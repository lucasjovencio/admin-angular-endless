import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../shared/services/firebase/auth.service';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastrService } from 'ngx-toastr';


type UserFields = 'email' | 'password';
type FormErrors = { [u in UserFields]: string };


@Component({
	selector: 'app-login-api',
	templateUrl: './login-api.component.html',
	styleUrls: ['./login-api.component.scss']
})
export class LoginApiComponent implements OnInit {

	public showExternalLogin = false;
	public newUser = false;
	public user: firebase.User;
	public loginForm: FormGroup;
	public formErrors: FormErrors = {
		'email': '',
		'password': '',
	};
	public errorMessage: any;

	constructor(
		public authService: AuthService,
		private userService:UserService,
		private afauth: AngularFireAuth, 
		private fb: FormBuilder,
		private router: Router,
		private toastr:ToastrService
	) 
	{
		this.loginForm = fb.group({
			email: ['cliente@teste.com', [Validators.required, Validators.email]],
			password: ['123123', Validators.required]
		});
	}

	ngOnInit() {
	}

	// Login With Google
	loginGoogle() {
		this.authService.GoogleAuth();
	}
	// Login With Twitter
	loginTwitter(): void {
		this.authService.signInTwitter();
	}
	// Login With Facebook
	loginFacebook() {
		this.authService.signInFacebok();
	}
	// Simple Login
	login() {
		this.userService.login(this.loginForm.getRawValue()).subscribe(res => {
			this.showSuccess()
			// this.router.navigate(['/dashboard/default']);
		}, error => {
			console.log(error)
			this.showError();
		})
	}
	showSuccess() {
		this.toastr.success('Login', 'Conectado com sucesso!');
	}
	showError() {
		this.toastr.error('Login', 'Ops... Tente novamente mais tarde.');
	}
}
