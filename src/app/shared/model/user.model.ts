export interface User {
    uid?: string;
    name?: string;
    email_verified_at?: string;
    lastname?: string;
    email: string;
    displayName?: string;
    photoURL?: string;
    emailVerified?: boolean;
    id:string;
}