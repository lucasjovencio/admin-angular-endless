import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpService } from './http.service';
import { tap } from 'rxjs/operators';
import { User } from '../model/user.model';

import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);
	token$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public showLoader: boolean = false;

	constructor(private http: HttpService,
				public afs: AngularFirestore,
				public router: Router,
				public ngZone: NgZone,
				private cookieService: CookieService
	) {
	}

	async hasToken() {
		const token: any = await localStorage.getItem('token');
		if (token) {
			return await true;
		} else {
			return await false
		}
	}
	
	get isLoggedIn(): boolean {
		const user:User = JSON.parse(localStorage.getItem('user_api'));
		const token: any = localStorage.getItem('token');
		return (((user != null && user.emailVerified != false)) && (token)) ? true : false;
	}

	setLocalToken(token) {
		localStorage.setItem("token", token);
	}
	cleanToken() {
		this.token$.next(null);
		localStorage.setItem("token", null);
	}
	setLocalUser(user) {
		localStorage.setItem('user_api', JSON.stringify(user));
	}
	cleanUser() {
		this.user$.next(null);
		localStorage.setItem('user_api', null);
	}
	cleanStorageData() {
		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
		this.cleanToken();
		this.cleanUser();
		this.cookieService.deleteAll('user_api', '/auth/login');
		this.router.navigate(['/auth/login']);
	}
	login(data: any) {
		this.showLoader = true;
		return this.http.post(`user/credenciais/login/login`, data).pipe(tap((res: any) => {
			this.token$.next(res['token'])
			this.setLocalToken(res['token']);
			this.setUserData(res['user']);
			this.showLoader = false;
			this.ngZone.run(() => {
				this.router.navigate(['/dashboard/default']);
			});
		}))
	}
	//Set user
	setUserData(user) {
		// const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
		const userData: User = {
			email: user.email,
			displayName: user.displayName,
			photoURL: user.photoURL || 'assets/images/user/1.jpg',
			emailVerified: user.email_verified_at || false,
			uid: user.id,
			id: user.id,
		};
		this.user$.next(userData)
		this.setLocalUser(userData);
	}
	get getUser() : User {
		const user:User = JSON.parse(localStorage.getItem('user_api'));
		return user;
	}
	getToken() {
		return this.token$.asObservable();
	}
	SignOut() {
		this.cleanStorageData();
	}
	verifyEmail(email: string) {
		return this.http.get(`user/verify/email`, { 'email': email });
	}
	verifyCpf(cpf: string) {
		return this.http.get(`user/verify/cpf`, { 'cpf': cpf });
	}
	verifyRg(rg: string) {
		return this.http.get(`user/verify/rg`, { 'rg': rg });
	}
	verifyCnpj(cnpj: string) {
		return this.http.get(`user/verify/cnpj`, { 'cnpj': cnpj });
	}
	autocompleteCep(cep: string) {
		let url = "https://viacep.com.br/ws/" + cep + "/json";
		return this.http.getExternal(url);
	}
	recoverPassword(data: any) {
		return this.http.post(`user/recover/password`, data);
	}
}
