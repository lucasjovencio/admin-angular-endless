
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavService, Menu } from '../../services/nav.service';
import { TranslateService } from '@ngx-translate/core';
// import { AuthService, User } from '../../services/firebase/auth.service';
import { UserService } from '../../services/user.service';
import { User } from '../../model/user.model';

var body = document.getElementsByTagName("body")[0];

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	public menuItems: Menu[];
	public items: Menu[];
	public searchResult: boolean = false;
	public searchResultEmpty: boolean = false;
	public openNav: boolean = false
	public right_sidebar: boolean = false
	public text: string
	public isOpenMobile: boolean = false
	public user:User = {
		email: '',
		displayName: '',
		photoURL: '',
		id:''
	};
	public changeLenguageTxt:string='PT';
	@Output() rightSidebarEvent = new EventEmitter<boolean>();

	constructor(public navServices: NavService,
		private translate: TranslateService,
		// public authService: AuthService,
		public userService:UserService
	) {
		translate.setDefaultLang('pt');
	}


	ngOnDestroy() {
		this.removeFix();
	}


	right_side_bar() {
		this.right_sidebar = !this.right_sidebar
		this.rightSidebarEvent.emit(this.right_sidebar)
	}

	collapseSidebar() {
		this.navServices.collapseSidebar = !this.navServices.collapseSidebar
	}

	openMobileNav() {
		this.openNav = !this.openNav;
	}

	public changeLanguage(lang) {
		this.translate.use(lang)
		switch(lang){
			case 'pt':
				this.changeLenguageTxt='PT';
			case 'en':
				this.changeLenguageTxt='EN';
			case 'es':
				this.changeLenguageTxt='ES';
			case 'fr':
				this.changeLenguageTxt='FR';
			default:
				this.changeLenguageTxt='PT';
		}
	}

	searchTerm(term: any) {
		term ? this.addFix() : this.removeFix();
		if (!term) return this.menuItems = [];
		let items = [];
		term = term.toLowerCase();
		this.items.filter(menuItems => {
			if (menuItems.title.toLowerCase().includes(term) && menuItems.type === 'link') {
				items.push(menuItems);
			}
			if (!menuItems.children) return false
			menuItems.children.filter(subItems => {
				if (subItems.title.toLowerCase().includes(term) && subItems.type === 'link') {
					subItems.icon = menuItems.icon
					items.push(subItems);
				}
				if (!subItems.children) return false
				subItems.children.filter(suSubItems => {
					if (suSubItems.title.toLowerCase().includes(term)) {
						suSubItems.icon = menuItems.icon
						items.push(suSubItems);
					}
				})
			})
			this.checkSearchResultEmpty(items)
			this.menuItems = items
		});
	}

	checkSearchResultEmpty(items) {
		if (!items.length)
			this.searchResultEmpty = true;
		else
			this.searchResultEmpty = false;
	}

	addFix() {
		this.searchResult = true;
		body.classList.add("offcanvas");
	}

	removeFix() {
		this.searchResult = false;
		body.classList.remove("offcanvas");
		this.text = "";
	}
	ngOnInit() {
		this.navServices.items.subscribe(menuItems => {
			this.items = menuItems
		});
		this.user = this.userService.getUser;
	}

}
