import { Component, OnInit, Input, ViewChild, ɵConsole, SimpleChanges } from '@angular/core';
import { companyDB } from '../../../../shared/data/tables/company';
import { HttpService } from 'src/app/shared/services/http.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
@Component({
	selector: 'app-datatable-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
	@ViewChild('DatatableComponent',{static: true}) table: DatatableComponent;
	public data = [];
	private dataBackup = [];
	public columns = [];
	private params = {};
	@Input() url: string = '';
	@Input() title: string = 'Paging Table';
	@Input() limit: number = 10;
	@Input() headerHeight: number = 50;
	@Input() footerHeight: number = 50;
	@Input() rowHeight: string = "auto";
	@Input() columnMode: string = "force";
	@Input() sarchString: string = "";
	@Input() sarchColumn: string = "";
	constructor(
		private http: HttpService
	) {
		if (this.url == '') {
			this.data = companyDB.data;
			this.columns = [{ name: 'Name' }, { name: 'Gender' }, { name: 'Company' }]
		} else {
			this.loadData();
		}
		console.log(this.data)
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
		if(changes.sarchString && changes.sarchString.currentValue){
			this.sarchString = changes.sarchString.currentValue;
		}
		if(changes.sarchColumn && changes.sarchColumn.currentValue){
			this.sarchColumn = changes.sarchColumn.currentValue;
		}
		if(changes.url && changes.url.currentValue){
			this.url = changes.url.currentValue;
			this.loadData();
		}
		
		this.updateFilter(this.sarchString,this.sarchColumn)
	}

	loadData()
	{
		this.http.get(this.url,this.params).subscribe(res=>{
			const data = res['data'];
			const columns = res['columns'];
			const columnsName = res['columnsName'];
			this.createColumns(columns,columnsName).then(res=>{
				this.columns = res;
				this.data = data;
				this.dataBackup = data;
			})
		},error=>{

		})
	}

	async createColumns(columns,columnsName){
		let cols = await [];
		for(let key in columns)
		{
			await cols.push(
				{ 
					name:columnsName[key],
					prop:columns[key],
					sort: true,
					filtering:true
				}
			)
		}
		return await cols;
	}

	async updateFilter(sarchString,sarchColumn) {
		const val = await sarchString.toLowerCase();
		try{
			if(val.length > 0){
				// filter our data
				const temp = this.data.filter(function (d) {
					return d[sarchColumn].toLowerCase().indexOf(val) !== -1 || !val;
				});
				// update the rows
				this.data = temp;
				// Whenever the filter changes, always go back to the first page
				// this.table.offset = 0;
			}else{
				this.data = this.dataBackup;
			}
		}
		catch(err) {
			console.log(err)
		}
		
	}
}
