import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ChartsModule } from "ng2-charts";
import { AppPieChartComponent } from "./app-piechart.component";

@NgModule({
	declarations: [
		AppPieChartComponent,
	],
	exports: [
		AppPieChartComponent,
	],
	imports: [
		CommonModule,
		ChartsModule
	]
})
export class AppPieChartModule
{
	
}