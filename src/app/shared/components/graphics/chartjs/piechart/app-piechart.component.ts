import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as chartData from '../../../../../shared/data/chart/chartjs';
import { ChartJsPieOptions } from "./Chartjs-pie-options";
import { ChartJsPieColors } from "./Chartjs-pie-colors";

@Component({
	selector: 'app-chartjs-pie',
	templateUrl: './app-piechart.component.html',
	styleUrls: ['./app-piechart.component.scss']
})
export class AppPieChartComponent
{
	@Input() labels: Array<string>;
	@Input() data: Array<number>;
	@Input() colors: ChartJsPieColors;
	@Input() legend: boolean;
	@Input() type: string;

	@Output() clicked = new EventEmitter();
	@Output() hovered = new EventEmitter();

	private options: ChartJsPieOptions = {
		animation: false,
		responsive: true,
		maintainAspectRatio: false
	}
	private doughnutChartType: string = 'doughnut';
	private doughnutChartColors: ChartJsPieColors;
	private defaultColors: ChartJsPieColors;
	private showLegend: boolean;

	constructor() {

	}

	ngOnInit() {
		// this.setDefaultColors();
		this.showLegend = this.legend ? this.legend : true;
		this.doughnutChartType = this.type ? this.type : 'doughnut';
		this.doughnutChartColors = (this.colors) ? this.colors : this.defaultColors;
	}

	// setDefaultColors() {
	// 	let colors: Array<string> = [];
	// 	this.data.map(() => {
	// 		let color = '#'+Math.floor(Math.random() * 16777215).toString(16);
	// 		colors.push(color) 
	// 	})
	// 	// console.log(colors);
	// 	// this.defaultColors.backgroundColor = colors;
	// }

	// events
	public chartClicked(e:any):void {
		this.clicked.emit(e);
	}

	public chartHovered(e:any):void { 
		this.hovered.emit(e);
	}
}