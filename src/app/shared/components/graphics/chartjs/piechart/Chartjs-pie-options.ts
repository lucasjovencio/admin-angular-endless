export interface ChartJsPieOptions {
	animation: boolean,
	responsive: boolean,
	maintainAspectRatio: boolean
};