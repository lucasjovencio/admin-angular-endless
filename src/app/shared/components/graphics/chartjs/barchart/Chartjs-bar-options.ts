export interface ChartJsBarOptions {
	scaleShowVerticalLines: boolean,
	responsive: boolean
}