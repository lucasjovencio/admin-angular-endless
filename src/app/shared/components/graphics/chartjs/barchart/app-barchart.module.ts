import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AppBarChartComponent } from "./app-barchart.component";
import { ChartsModule } from "ng2-charts";

@NgModule({
	declarations: [
		AppBarChartComponent,
	],
	exports: [
		AppBarChartComponent,
	],
	imports: [
		CommonModule,
		ChartsModule,
	]
})
export class AppBarChartModule
{
	
}