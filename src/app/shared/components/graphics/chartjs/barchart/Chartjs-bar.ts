export interface ChartJsBarData {
	data: Array<number>,
	label: string,
}