export interface ChartJsBarColors {
	backgroundColor: string,
    borderColor: string,
    borderWidth: number,
}