import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ChartJsBarColors } from "./Chartjs-bar-colors";
import { ChartJsBarData } from "./Chartjs-bar";
import { ChartJsBarOptions } from "./Chartjs-bar-options";


@Component({
	selector: 'app-chartjs-bar',
	templateUrl: './app-barchart.component.html',
	styleUrls: ['./app-barchart.component.scss']
})
export class AppBarChartComponent
{
	@Input() labels: Array<string>;
	@Input() legend: boolean;
	@Input() data: Array<ChartJsBarData>;
	@Input() colors: Array<ChartJsBarColors>;

	@Output() clicked = new EventEmitter();
	@Output() hovered = new EventEmitter();

	public barChartOptions: ChartJsBarOptions = {
		scaleShowVerticalLines: false,
		responsive: true
	};
	public barChartType:string = 'bar';
	private barChartColors: Array<ChartJsBarColors>;
	private defaultColors: Array<ChartJsBarColors> = [];
	private showLegend:boolean;

	constructor() {
		
	}

	ngOnInit() {
		this.setDefaultColors();
		this.barChartColors = (this.colors) ? this.colors : this.defaultColors;
		this.showLegend = this.legend ? this.legend : true;
	}
	
	setDefaultColors() {
		this.data.map(() => {
			let color = '#'+Math.floor(Math.random()*16777215).toString(16);
			let rgbaColor1 = Math.floor(Math.random() * 255);
			let rgbaColor2 = Math.floor(Math.random() * 255);
			let rgbaColor3 = Math.floor(Math.random() * 255);
			let colorRgba = 'rgba('+rgbaColor1+','+rgbaColor2+','+rgbaColor3+',0.8)'; //rgba(68, 102, 242, 0.3)
			this.defaultColors.push({
				backgroundColor: colorRgba,
				borderColor: color,
				borderWidth: 1,
				
			})
		})
	}

	// events
	public chartClicked(e:any):void {
		this.clicked.emit(e);
	}

	public chartHovered(e:any):void { 
		this.hovered.emit(e);
	}
}