export interface ChartJsLineOptions {
	scaleShowGridLines: boolean,
	scaleGridLineColor: string,
	scaleGridLineWidth: number,
	scaleShowHorizontalLines: boolean,
	scaleShowVerticalLines: boolean,
	bezierCurve: boolean,
	bezierCurveTension: number,
	pointDot: boolean,
	pointDotRadius: number,
	pointDotStrokeWidth: number,
	pointHitDetectionRadius: number,
	datasetStroke: boolean,
	datasetStrokeWidth: number,
	datasetFill: boolean,
	legendTemplate: string
};