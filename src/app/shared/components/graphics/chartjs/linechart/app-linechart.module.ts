import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AppLineChartComponent } from "./app-linechart.component";
import { ChartsModule } from "ng2-charts";

@NgModule({
	declarations: [
		AppLineChartComponent,
	],
	exports: [
		AppLineChartComponent,
	],
	imports: [
		CommonModule,
		ChartsModule,
	]
})
export class AppLineChartModule
{
	
}