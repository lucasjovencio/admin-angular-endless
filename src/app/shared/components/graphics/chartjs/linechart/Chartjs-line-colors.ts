export interface ChartJsLineColors {
	backgroundColor: string,
    borderColor: string,
    borderWidth: number,
}