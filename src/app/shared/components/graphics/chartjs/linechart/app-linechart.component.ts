import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as chartData from '../../../../../shared/data/chart/chartjs';
import { ChartJsLineOptions } from "./Chartjs-line-options";
import { ChartLineOptions } from "chart.js";
import { ChartJsLineData } from "./Chartjs-line.data";
import { ChartJsLineColors } from "./Chartjs-line-colors";

@Component({
	selector: 'app-chartjs-line',
	templateUrl: './app-linechart.component.html',
	styleUrls: ['./app-linechart.component.scss']
})
export class AppLineChartComponent
{
	@Input() options: ChartJsLineOptions;
	@Input() labels: Array<string>;
	@Input() legend: boolean;
	@Input() data: Array<ChartJsLineData>;
	@Input() colors: Array<ChartJsLineColors>;

	@Output() clicked = new EventEmitter();
	@Output() hovered = new EventEmitter();

	private showLegend:boolean;
	private type: string = 'line';
	private lineChartColors: Array<ChartJsLineColors> = [];
	private defaultColors: Array<ChartJsLineColors> = [];

	constructor() {

	}

	ngOnInit() {
		this.setDefaultColors();
		this.showLegend = this.legend ? this.legend : true;
		this.lineChartColors = (this.colors) ? this.colors : this.defaultColors;
	}

	setDefaultColors() {
		this.data.map(() => {
			let color = '#'+Math.floor(Math.random() * 16777215).toString(16);
			let rgbaColor1 = Math.floor(Math.random() * 255);
			let rgbaColor2 = Math.floor(Math.random() * 255);
			let rgbaColor3 = Math.floor(Math.random() * 255);
			let colorRgba = 'rgba('+rgbaColor1+','+rgbaColor2+','+rgbaColor3+',0.3)'; //rgba(68, 102, 242, 0.3)
			this.defaultColors.push({
				backgroundColor: colorRgba,
				borderColor: color,
				borderWidth: 2,
				
			})
		})
	}

	// events
	public chartClicked(e:any):void {
		this.clicked.emit(e);
	}

	public chartHovered(e:any):void { 
		this.hovered.emit(e);
	}
}