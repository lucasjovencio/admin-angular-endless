export interface ChartJsLineData {
	data: Array<number>,
	label: string,
}