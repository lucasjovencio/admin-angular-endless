import { Component, Input } from "@angular/core";

import { ChatUsers } from "../../model/chat.model";

@Component({
	selector: 'app-chat-component',
	templateUrl: './app-chat.component.html',
	styleUrls: ['./app-chat.component.scss']
})
export class AppChatComponent {
	@Input() contactList : ChatUsers[];
	@Input() loggedUser: any;

	public users : ChatUsers[] = []
	public profile : any

	constructor() {   
		
	}

	ngOnInit() {
		this.users = this.contactList;
		this.profile = this.loggedUser;
	}
}