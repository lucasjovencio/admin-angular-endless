import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({providedIn: 'root'})
export class ConversationService 
{
	private chats$ = new BehaviorSubject<any>(null);
	
	constructor() {
		
	}

	setChats(chats) {
		this.chats$.next(chats);
	}

	getChats() {
		return this.chats$.asObservable();
	}
}