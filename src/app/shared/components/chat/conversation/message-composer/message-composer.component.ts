import { Component, Input } from "@angular/core";
import { ChatService } from "src/app/shared/services/chat.service";
import { Observable } from "rxjs";
import { ContactListService } from "../../contact-list/contact-list.service";

@Component({
	selector: 'app-chat-messagecomposer',
	templateUrl: './message-composer.component.html',
	styleUrls: ['./message-composer.component.scss']
})
export class AppChatMessageComposer
{
	@Input() loggedUser:any;
	@Input() chatWith:any;

	id$: Observable<any>;

	public chatText : string;
	public profile: any;
	public error : boolean = false
	public chatUser: any;
	
	constructor(private chatService: ChatService,
				private contactListService: ContactListService) {

	}

	ngOnInit() {
		this.profile = this.loggedUser;
		this.id$ = this.contactListService.getId();
		this.id$.subscribe(id => {
			if(id) {
				console.log('chegou',id)
				this.userChat(id)
			}
		})
	}

	public userChat(id:number =1){
		this.chatService.chatToUser(id).subscribe(chatUser => {
			this.chatUser = chatUser
		})
	}

	// Send Message to User
	public sendMessage(form) {
		if(!form.value.message){
		  this.error = true
		  return false
		}
		console.log(this.chatUser);
		this.error = false
		let chat = {
			sender: this.profile.id,
			receiver: this.chatUser.id,
			receiver_name: this.chatUser.name,
			message: form.value.message
		}
		this.chatService.sendMessage(chat) 
		this.chatText = ''
		this.chatUser.seen = 'online'
		this.chatUser.online = true
	}
}