import { Component, Input } from "@angular/core";
import { Observable } from "rxjs";

import { ChatService } from "src/app/shared/services/chat.service";
import { ContactListService } from "../contact-list/contact-list.service";
import { ConversationService } from "./conversation.service";

@Component({
	selector: 'app-chat-conversation',
	templateUrl: './conversation.component.html',
	styleUrls: ['./conversation.component.scss']
})
export class AppChatConversation
{
	@Input() loggedUser: any;
	id$: Observable<any>;
	public chats : any
	public chatUser: any;
	public profile: any;

	constructor(private chatService: ChatService, 
				private contactListService: ContactListService,
				private conversationService: ConversationService
	) {   
		
	}

	ngOnInit() {
		this.profile = this.loggedUser;
		this.id$ = this.contactListService.getId();
		this.id$.subscribe(id => {
			if(id) {
				console.log('chegou',id)
				this.userChat(id)
			}
		})		
	}

	public sendMessage() {

	}

	// User Chat
	public userChat(id:number =1){
		this.chatService.chatToUser(id).subscribe(chatUser => {
			this.chatUser = chatUser
			console.log(this.chatUser)
		})
		this.chatService.getChatHistory(id).subscribe(chats => {
			this.conversationService.setChats(chats)
			// this.chats = chats
		})
	}
	

}