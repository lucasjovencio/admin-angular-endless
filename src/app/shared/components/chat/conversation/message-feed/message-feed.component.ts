import { Component, Input } from "@angular/core";
import { ChatService } from "src/app/shared/services/chat.service";
import { Observable } from "rxjs";
import { ConversationService } from "../conversation.service";

@Component({
	selector: 'app-chat-messagefeed',
	templateUrl: './message-feed.component.html',
	styleUrls: ['./message-feed.component.scss']
})
export class AppChatMessageFeed
{
	@Input() loggedUser: any;
	
	chats$: Observable<any>;

	public chats : any
	public id : any;
	public profile : any

	constructor(private chatService: ChatService, private conversationService: ConversationService) {   
		
	}

	ngOnInit() {  
		this.chats$ = this.conversationService.getChats();
		this.chats$.subscribe(chats => {
			if(chats) {
				console.log('veio 2', chats);
				this.chats = chats;
			}
		})
		this.profile = this.loggedUser;
		// console.log('chats',this.chats)
	}
}