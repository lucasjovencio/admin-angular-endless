import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppChatConversation } from "./conversation/conversation.component";
import { AppChatMessageComposer } from "./conversation/message-composer/message-composer.component";
import { AppChatMessageFeed } from "./conversation/message-feed/message-feed.component";
import { AppChatContactList } from "./contact-list/contact-list.component";
import { AppChatMessageList } from "./conversation/message-feed/message-list/message-list.component";
import { AppChatComponent } from "./app-chat.component";

@NgModule({
	declarations: [
		AppChatConversation,
		AppChatMessageComposer,
		AppChatMessageFeed,
		AppChatContactList,
		AppChatMessageList,
		AppChatComponent
	],
	exports: [
		AppChatComponent
	],
	imports : [
		CommonModule,
		FormsModule,
    	ReactiveFormsModule,
	]
})
export class AppChatModule {
	
}