import { Component, Input, Output, EventEmitter } from "@angular/core";

import { ChatUsers } from "src/app/shared/model/chat.model";
import { ContactListService } from "./contact-list.service";

@Component({
	selector: 'app-chat-contactlist',
	templateUrl: './contact-list.component.html',
	styleUrls: ['./contact-list.component.scss']
})
export class AppChatContactList
{
	@Input() contacts: ChatUsers[];
	@Input() loggedUser: any;

	public users : ChatUsers[] = []
	public searchUsers : ChatUsers[] = []
	public profile : any
	

	constructor(private contactListService: ContactListService) {
		
	}

	ngOnInit() {  
		this.users = this.contacts;
		this.searchUsers = this.contacts;
		this.profile = this.loggedUser; 
	}

	userChat(id){
		this.contactListService.setId(id);
	}

	searchTerm(term: any) {
		if(!term) return this.searchUsers = this.users
		term = term.toLowerCase();
		let user = []
		this.users.filter(users => {
			if(users.name.toLowerCase().includes(term)) {
				user.push(users)
			} 
		})
		this.searchUsers = user
	}
}