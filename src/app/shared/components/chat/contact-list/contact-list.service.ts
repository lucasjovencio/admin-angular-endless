import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({providedIn: 'root'})
export class ContactListService
{
	private id$ = new BehaviorSubject<any>(null);

	constructor() {

	}
	
	setId(id) {
		this.id$.next(id);
	}

	getId() {
		return this.id$.asObservable();
	}
}