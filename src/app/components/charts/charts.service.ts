import { Injectable } from "@angular/core";
import { HttpService } from "src/app/shared/services/http.service";

@Injectable({providedIn: 'root'})
export class ChartService 
{
	constructor(private http: HttpService) {

	}

	getData() {
		return this.http.get(`financeiro/get`);
	}
}