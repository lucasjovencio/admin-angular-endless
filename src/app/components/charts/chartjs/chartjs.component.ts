import { Component, ViewEncapsulation } from '@angular/core';

import * as chartData from '../../../shared/data/chart/chartjs';
import { ChartJsBarData } from 'src/app/shared/components/graphics/chartjs/barchart/Chartjs-bar';
import { ChartJsLineOptions } from 'src/app/shared/components/graphics/chartjs/linechart/Chartjs-line-options';
import { ChartJsLineData } from 'src/app/shared/components/graphics/chartjs/linechart/Chartjs-line.data';
import { ChartService } from '../charts.service';

@Component({
	selector: 'app-chartjs',
	templateUrl: './chartjs.component.html',
	styleUrls: ['./chartjs.component.scss'],
	encapsulation:ViewEncapsulation.None
})
export class ChartjsComponent {
	
	private barChartLabels: Array<string> = ["January", "February", "March", "April", "May", "June", "July"];
	private barChartData: Array<ChartJsBarData> = [
		{ data: [35, 59, 80, 81, 56, 55, 40], label: 'Series A' },
		// { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
	];

	private lineChartOptions: ChartJsLineOptions = {
		scaleShowGridLines: true,
		scaleGridLineColor: "rgba(0,0,0,.05)",
		scaleGridLineWidth: 1,
		scaleShowHorizontalLines: true,
		scaleShowVerticalLines: true,
		bezierCurve: true,
		bezierCurveTension: 0.4,
		pointDot: true,
		pointDotRadius: 4,
		pointDotStrokeWidth: 1,
		pointHitDetectionRadius: 20,
		datasetStroke: true,
		datasetStrokeWidth: 2,
		datasetFill: true,
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	}

	private lineChartLabels: Array<string> = ["January", "February", "March", "April", "May", "June", "July"];
	private lineChartData: Array<ChartJsLineData> = [
		{ data: [10, 59, 80, 81, 56, 55, 40], label: 'Series A' },
		// { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
	];

	private pieChartLabels: Array<string> = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
	private pieChartData: Array<number> = [350, 450, 100];
	private pieChartType: string = 'doughnut';

	constructor(private chartService: ChartService) { }

	ngOnInit() {
		this.chartService.getData().subscribe(res => {
			this.barChartData[0].data = res.data;
			this.lineChartData[0].data = res.data;
			this.barChartLabels = res.legend;
			this.lineChartLabels = res.legend;
			this.pieChartLabels = res.pieYears;
			this.pieChartData = res.pieValues;
		})
	}

}
