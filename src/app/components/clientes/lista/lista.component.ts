import { Component, OnInit } from '@angular/core';
import { companyDB } from '../../../shared/data/tables/company';

@Component({
	selector: 'app-lista',
	templateUrl: './lista.component.html',
	styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
	company: any = [];
	sarchString:string;
	sarchColumn:string;
	url="usuarios/get";
	title='Clientes';
	constructor() {
		this.company = companyDB.data;
		console.log(this.company)
	}

	ngOnInit() {
	}

	updateFilter(event,name){
		this.sarchString = event.target.value.toLowerCase();
		this.sarchColumn = name;
	}

}
