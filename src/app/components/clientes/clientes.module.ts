import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'src/app/shared/components/datatable/table/table.module';
import { ClientesRoutingModule } from './clientes-routing.module';
import { ListaComponent } from './lista/lista.component';

@NgModule({
	declarations: [
		ListaComponent
	],
	imports: [
		CommonModule,
		TableModule,
		ClientesRoutingModule
	]
})
export class ClientesModule { }
