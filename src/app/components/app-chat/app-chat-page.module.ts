import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppChatPageRoutingModule } from './app-chat-page-routing.module';
import { ChatComponent } from './chat/chat.component';
import { AppChatModule } from 'src/app/shared/components/chat/app-chat.module';

@NgModule({
	declarations: [ChatComponent],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AppChatPageRoutingModule,
		AppChatModule
	]
})
export class AppChatPageModule { }
