import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { HttpService } from "src/app/shared/services/http.service";

@Injectable({providedIn: 'root'})
export class AppChatService
{
	private contacts$ = new BehaviorSubject<any>(null);

	constructor(private http: HttpService) {

	}

	getAllContacts() {
		return this.contacts$.asObservable();
	}

	getContacts() {
		return this.http.get(`messages/contacts`).subscribe(contacts => this.contacts$.next(contacts));
	}

	getMessages(id) {
		return this.http.get(`messages/${id}`);
	}

	sendMessage(data) {
		return this.http.post(`messages/send`,data);
	}
}