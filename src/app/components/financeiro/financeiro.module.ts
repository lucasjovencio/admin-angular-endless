import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceiroRoutingModule } from './financeiro-routing.module';
import { TableModule } from 'src/app/shared/components/datatable/table/table.module';
import { RelatorioComponent } from './relatorio/relatorio.component';

@NgModule({
  declarations: [
    RelatorioComponent
  ],
  imports: [
    CommonModule,
    FinanceiroRoutingModule,
		TableModule,
  ]
})
export class FinanceiroModule { }

