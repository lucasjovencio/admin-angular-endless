import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})
export class RelatorioComponent implements OnInit {

	sarchString:string;
	sarchColumn:string;
	url="financeiro/tabela";
  	title='Transações financeiras';
  
	constructor() {
	}

	ngOnInit() {
	}

	updateFilter(event,name){
		this.sarchString = event.target.value.toLowerCase();
		this.sarchColumn = name;
	}

}
