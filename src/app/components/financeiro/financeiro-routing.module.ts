import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatorioComponent } from './relatorio/relatorio.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'transacoes',
				component: RelatorioComponent,
				data: {
					title: "Transações feitas",
					breadcrumb: "Transações"
				}
			},
		]
	}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceiroRoutingModule { }
