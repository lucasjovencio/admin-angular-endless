import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-ngx-dropzone',
  templateUrl: './ngx-dropzone.component.html',
  styleUrls: ['./ngx-dropzone.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NgxDropzoneComponent implements OnInit {

  constructor(private toastrService: ToastrService) { }

  public config1: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
	cancelReset: null,
	addRemoveLinks:true
  };

  public config2: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 5,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  public config3: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 5,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
    acceptedFiles: '.pdf'
  };

	public onSelect(event) {
		console.log('veio',event)
	}

  public onUploadError(args: any): void { 
	this.toastrService.error('Ocorreu um erro ao fazer o upload.', 'Erro!');
   }

  public onUploadSuccess(args: any): void { 
		this.toastrService.success('Arquivo enviado', 'Sucesso!');
	  console.log(args);
   }

  ngOnInit() {
  }

}
