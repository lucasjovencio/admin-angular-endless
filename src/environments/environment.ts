// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDpVqEKM9tmxSTlHOB7FYAnPYavFSrIxHo",
    authDomain: "endless-8e68b.firebaseapp.com",
    databaseURL: "https://endless-8e68b.firebaseio.com",
    projectId: "endless-8e68b",
    storageBucket: "endless-8e68b.appspot.com",
    messagingSenderId: "937844193298",
    appId: "1:937844193298:web:70446323e0858a7a2c80cb",
    measurementId: "G-XSK1E70V8H"
  },
//   api_production:'http://127.0.0.1:8000/bind-angular/public/',
    api_dev:'http://18.228.152.103/bind-angular/public/',
    menu_dev: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
