(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-e-commerce-e-commerce-module"],{

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/color-sets.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/color-sets.js ***!
  \***********************************************************************/
/*! exports provided: colorSets */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "colorSets", function() { return colorSets; });
var colorSets = [
    {
        name: 'vivid',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514'
        ]
    },
    {
        name: 'natural',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3'
        ]
    },
    {
        name: 'cool',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886'
        ]
    },
    {
        name: 'fire',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#ff3d00', '#bf360c', '#ff8f00', '#ff6f00', '#ff5722', '#e65100', '#ffca28', '#ffab00'
        ]
    },
    {
        name: 'solar',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#fff8e1', '#ffecb3', '#ffe082', '#ffd54f', '#ffca28', '#ffc107', '#ffb300', '#ffa000', '#ff8f00', '#ff6f00'
        ]
    },
    {
        name: 'air',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#e1f5fe', '#b3e5fc', '#81d4fa', '#4fc3f7', '#29b6f6', '#03a9f4', '#039be5', '#0288d1', '#0277bd', '#01579b'
        ]
    },
    {
        name: 'aqua',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#e0f7fa', '#b2ebf2', '#80deea', '#4dd0e1', '#26c6da', '#00bcd4', '#00acc1', '#0097a7', '#00838f', '#006064'
        ]
    },
    {
        name: 'flame',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738'
        ]
    },
    {
        name: 'ocean',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#1D68FB', '#33C0FC', '#4AFFFE', '#AFFFFF', '#FFFC63', '#FDBD2D', '#FC8A25', '#FA4F1E', '#FA141B', '#BA38D1'
        ]
    },
    {
        name: 'forest',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32'
        ]
    },
    {
        name: 'horizon',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#2597FB', '#65EBFD', '#99FDD0', '#FCEE4B', '#FEFCFA', '#FDD6E3', '#FCB1A8', '#EF6F7B', '#CB96E8', '#EFDEE0'
        ]
    },
    {
        name: 'neons',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#FF3333', '#FF33FF', '#CC33FF', '#0000FF', '#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600'
        ]
    },
    {
        name: 'picnic',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#FAC51D', '#66BD6D', '#FAA026', '#29BB9C', '#E96B56', '#55ACD2', '#B7332F', '#2C83C9', '#9166B8', '#92E7E8'
        ]
    },
    {
        name: 'night',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#2B1B5A', '#501356', '#183356', '#28203F', '#391B3C', '#1E2B3C', '#120634',
            '#2D0432', '#051932', '#453080', '#75267D', '#2C507D', '#4B3880', '#752F7D', '#35547D'
        ]
    },
    {
        name: 'nightLights',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
            '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
        ]
    }
];
//# sourceMappingURL=color-sets.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/color-utils.js":
/*!************************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/color-utils.js ***!
  \************************************************************************/
/*! exports provided: hexToRgb, invertColor, shadeRGBColor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToRgb", function() { return hexToRgb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invertColor", function() { return invertColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shadeRGBColor", function() { return shadeRGBColor; });
/* harmony import */ var d3_color__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! d3-color */ "./node_modules/d3-color/src/index.js");

/**
 * Converts a hex to RGB
 *
 * @export
 * @param {string} hex
 * @returns {*}
 */
function hexToRgb(value) {
    // deprecated, use d3.color()
    return d3_color__WEBPACK_IMPORTED_MODULE_0__["rgb"](value);
}
/**
 * Accepts a color (string) and returns a inverted hex color (string)
 * http://stackoverflow.com/questions/9600295/automatically-change-text-color-to-assure-readability
 *
 * @export
 * @param {any} value
 * @returns {string}
 */
function invertColor(value) {
    var color = d3_color__WEBPACK_IMPORTED_MODULE_0__["rgb"](value);
    var r = color.r, g = color.g, b = color.b, opacity = color.opacity;
    if (opacity === 0) {
        return color.toString();
    }
    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    var depth = (yiq >= 128) ? -.8 : .8;
    return shadeRGBColor(color, depth);
}
/**
 * Given a rgb, it will darken/lighten
 * http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
 *
 * @export
 * @param {any} { r, g, b }
 * @param {any} percent
 * @returns
 */
function shadeRGBColor(_a, percent) {
    var r = _a.r, g = _a.g, b = _a.b;
    var t = percent < 0 ? 0 : 255;
    var p = percent < 0 ? percent * -1 : percent;
    r = (Math.round((t - r) * p) + r);
    g = (Math.round((t - g) * p) + g);
    b = (Math.round((t - b) * p) + b);
    return "rgb(" + r + ", " + g + ", " + b + ")";
}
//# sourceMappingURL=color-utils.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/id.js":
/*!***************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/id.js ***!
  \***************************************************************/
/*! exports provided: id */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "id", function() { return id; });
var cache = {};
/**
 * Generates a short id.
 *
 * Description:
 *   A 4-character alphanumeric sequence (364 = 1.6 million)
 *   This should only be used for JavaScript specific models.
 *   http://stackoverflow.com/questions/6248666/how-to-generate-short-uid-like-ax4j9z-in-js
 *
 *   Example: `ebgf`
 */
function id() {
    var newId = ('0000' + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
    // append a 'a' because neo gets mad
    newId = "a" + newId;
    // ensure not already used
    if (!cache[newId]) {
        cache[newId] = true;
        return newId;
    }
    return id();
}
//# sourceMappingURL=id.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/index.js ***!
  \******************************************************************/
/*! exports provided: id, colorSets, sortLinear, sortByDomain, sortByTime, throttle, throttleable, hexToRgb, invertColor, shadeRGBColor, VisibilityObserver, isDate, isNumber */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _id__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./id */ "./node_modules/@swimlane/ngx-charts/release/utils/id.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "id", function() { return _id__WEBPACK_IMPORTED_MODULE_0__["id"]; });

/* harmony import */ var _color_sets__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./color-sets */ "./node_modules/@swimlane/ngx-charts/release/utils/color-sets.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "colorSets", function() { return _color_sets__WEBPACK_IMPORTED_MODULE_1__["colorSets"]; });

/* harmony import */ var _sort__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sort */ "./node_modules/@swimlane/ngx-charts/release/utils/sort.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sortLinear", function() { return _sort__WEBPACK_IMPORTED_MODULE_2__["sortLinear"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sortByDomain", function() { return _sort__WEBPACK_IMPORTED_MODULE_2__["sortByDomain"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sortByTime", function() { return _sort__WEBPACK_IMPORTED_MODULE_2__["sortByTime"]; });

/* harmony import */ var _throttle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./throttle */ "./node_modules/@swimlane/ngx-charts/release/utils/throttle.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "throttle", function() { return _throttle__WEBPACK_IMPORTED_MODULE_3__["throttle"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "throttleable", function() { return _throttle__WEBPACK_IMPORTED_MODULE_3__["throttleable"]; });

/* harmony import */ var _color_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./color-utils */ "./node_modules/@swimlane/ngx-charts/release/utils/color-utils.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hexToRgb", function() { return _color_utils__WEBPACK_IMPORTED_MODULE_4__["hexToRgb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "invertColor", function() { return _color_utils__WEBPACK_IMPORTED_MODULE_4__["invertColor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "shadeRGBColor", function() { return _color_utils__WEBPACK_IMPORTED_MODULE_4__["shadeRGBColor"]; });

/* harmony import */ var _visibility_observer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./visibility-observer */ "./node_modules/@swimlane/ngx-charts/release/utils/visibility-observer.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VisibilityObserver", function() { return _visibility_observer__WEBPACK_IMPORTED_MODULE_5__["VisibilityObserver"]; });

/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./types */ "./node_modules/@swimlane/ngx-charts/release/utils/types.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isDate", function() { return _types__WEBPACK_IMPORTED_MODULE_6__["isDate"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return _types__WEBPACK_IMPORTED_MODULE_6__["isNumber"]; });








//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/sort.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/sort.js ***!
  \*****************************************************************/
/*! exports provided: sortLinear, sortByDomain, sortByTime */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortLinear", function() { return sortLinear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortByDomain", function() { return sortByDomain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortByTime", function() { return sortByTime; });
function sortLinear(data, property, direction) {
    if (direction === void 0) { direction = 'asc'; }
    return data.sort(function (a, b) {
        if (direction === 'asc') {
            return a[property] - b[property];
        }
        else {
            return b[property] - a[property];
        }
    });
}
function sortByDomain(data, property, direction, domain) {
    if (direction === void 0) { direction = 'asc'; }
    return data.sort(function (a, b) {
        var aVal = a[property];
        var bVal = b[property];
        var aIdx = domain.indexOf(aVal);
        var bIdx = domain.indexOf(bVal);
        if (direction === 'asc') {
            return aIdx - bIdx;
        }
        else {
            return bIdx - aIdx;
        }
    });
}
function sortByTime(data, property, direction) {
    if (direction === void 0) { direction = 'asc'; }
    return data.sort(function (a, b) {
        var aDate = a[property].getTime();
        var bDate = b[property].getTime();
        if (direction === 'asc') {
            if (aDate > bDate)
                return 1;
            if (bDate > aDate)
                return -1;
            return 0;
        }
        else {
            if (aDate > bDate)
                return -1;
            if (bDate > aDate)
                return 1;
            return 0;
        }
    });
}
//# sourceMappingURL=sort.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/throttle.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/throttle.js ***!
  \*********************************************************************/
/*! exports provided: throttle, throttleable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throttle", function() { return throttle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throttleable", function() { return throttleable; });
/**
 * Throttle a function
 *
 * @export
 * @param {*}      func
 * @param {number} wait
 * @param {*}      [options]
 * @returns
 */
function throttle(func, wait, options) {
    options = options || {};
    var context;
    var args;
    var result;
    var timeout = null;
    var previous = 0;
    function later() {
        previous = options.leading === false ? 0 : +new Date();
        timeout = null;
        result = func.apply(context, args);
    }
    return function () {
        var now = +new Date();
        if (!previous && options.leading === false) {
            previous = now;
        }
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0) {
            clearTimeout(timeout);
            timeout = null;
            previous = now;
            result = func.apply(context, args);
        }
        else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };
}
/**
 * Throttle decorator
 *
 *  class MyClass {
 *    throttleable(10)
 *    myFn() { ... }
 *  }
 *
 * @export
 * @param {number} duration
 * @param {*} [options]
 * @returns
 */
function throttleable(duration, options) {
    return function innerDecorator(target, key, descriptor) {
        return {
            configurable: true,
            enumerable: descriptor.enumerable,
            get: function getter() {
                Object.defineProperty(this, key, {
                    configurable: true,
                    enumerable: descriptor.enumerable,
                    value: throttle(descriptor.value, duration, options)
                });
                return this[key];
            }
        };
    };
}
//# sourceMappingURL=throttle.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/types.js":
/*!******************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/types.js ***!
  \******************************************************************/
/*! exports provided: isDate, isNumber */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDate", function() { return isDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return isNumber; });
function isDate(value) {
    return toString.call(value) === '[object Date]';
}
function isNumber(value) {
    return typeof value === 'number';
}
//# sourceMappingURL=types.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-charts/release/utils/visibility-observer.js":
/*!********************************************************************************!*\
  !*** ./node_modules/@swimlane/ngx-charts/release/utils/visibility-observer.js ***!
  \********************************************************************************/
/*! exports provided: VisibilityObserver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisibilityObserver", function() { return VisibilityObserver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Visibility Observer
 */
var VisibilityObserver = /** @class */ (function () {
    function VisibilityObserver(element, zone) {
        this.element = element;
        this.zone = zone;
        this.visible = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.isVisible = false;
        this.runCheck();
    }
    VisibilityObserver.prototype.destroy = function () {
        clearTimeout(this.timeout);
    };
    VisibilityObserver.prototype.onVisibilityChange = function () {
        var _this = this;
        // trigger zone recalc for columns
        this.zone.run(function () {
            _this.isVisible = true;
            _this.visible.emit(true);
        });
    };
    VisibilityObserver.prototype.runCheck = function () {
        var _this = this;
        var check = function () {
            if (!_this.element) {
                return;
            }
            // https://davidwalsh.name/offsetheight-visibility
            var _a = _this.element.nativeElement, offsetHeight = _a.offsetHeight, offsetWidth = _a.offsetWidth;
            if (offsetHeight && offsetWidth) {
                clearTimeout(_this.timeout);
                _this.onVisibilityChange();
            }
            else {
                clearTimeout(_this.timeout);
                _this.zone.runOutsideAngular(function () {
                    _this.timeout = setTimeout(function () { return check(); }, 100);
                });
            }
        };
        this.zone.runOutsideAngular(function () {
            _this.timeout = setTimeout(function () { return check(); });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], VisibilityObserver.prototype, "visible", void 0);
    return VisibilityObserver;
}());

//# sourceMappingURL=visibility-observer.js.map

/***/ }),

/***/ "./node_modules/ngx-print/fesm5/ngx-print.js":
/*!***************************************************!*\
  !*** ./node_modules/ngx-print/fesm5/ngx-print.js ***!
  \***************************************************/
/*! exports provided: NgxPrintDirective, NgxPrintModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxPrintDirective", function() { return NgxPrintDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxPrintModule", function() { return NgxPrintModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgxPrintDirective = /** @class */ (function () {
    function NgxPrintDirective() {
        this._printStyle = [];
        /**
         *
         *
         * \@memberof NgxPrintDirective
         */
        this.useExistingCss = false;
        /**
         *
         *
         * @return html for the given tag
         *
         * \@memberof NgxPrintDirective
         */
        this._styleSheetFile = '';
    }
    Object.defineProperty(NgxPrintDirective.prototype, "printStyle", {
        /**
         *
         *
         * @memberof NgxPrintDirective
         */
        set: /**
         *
         *
         * \@memberof NgxPrintDirective
         * @param {?} values
         * @return {?}
         */
        function (values) {
            for (var key in values) {
                if (values.hasOwnProperty(key)) {
                    this._printStyle.push((key + JSON.stringify(values[key])).replace(/['"]+/g, ''));
                }
            }
            this.returnStyleValues();
        },
        enumerable: true,
        configurable: true
    });
    /**
     *
     *
     * @returns the string that create the stylesheet which will be injected
     * later within <style></style> tag.
     *
     * -join/replace to transform an array objects to css-styled string
     *
     * @memberof NgxPrintDirective
     */
    /**
     *
     *
     * \@memberof NgxPrintDirective
     * @return {?} the string that create the stylesheet which will be injected
     * later within <style></style> tag.
     *
     * -join/replace to transform an array objects to css-styled string
     *
     */
    NgxPrintDirective.prototype.returnStyleValues = /**
     *
     *
     * \@memberof NgxPrintDirective
     * @return {?} the string that create the stylesheet which will be injected
     * later within <style></style> tag.
     *
     * -join/replace to transform an array objects to css-styled string
     *
     */
    function () {
        return "<style> " + this._printStyle.join(' ').replace(/,/g, ';') + " </style>";
    };
    Object.defineProperty(NgxPrintDirective.prototype, "styleSheetFile", {
        /**
         * @memberof NgxPrintDirective
         * @param cssList
         */
        set: /**
         * \@memberof NgxPrintDirective
         * @param {?} cssList
         * @return {?}
         */
        function (cssList) {
            var e_1, _a;
            /** @type {?} */
            var linkTagFn = (/**
             * @param {?} cssFileName
             * @return {?}
             */
            function (cssFileName) {
                return "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + cssFileName + "\">";
            });
            if (cssList.indexOf(',') !== -1) {
                /** @type {?} */
                var valueArr = cssList.split(',');
                try {
                    for (var valueArr_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__values"])(valueArr), valueArr_1_1 = valueArr_1.next(); !valueArr_1_1.done; valueArr_1_1 = valueArr_1.next()) {
                        var val = valueArr_1_1.value;
                        this._styleSheetFile = this._styleSheetFile + linkTagFn(val);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (valueArr_1_1 && !valueArr_1_1.done && (_a = valueArr_1.return)) _a.call(valueArr_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            else {
                this._styleSheetFile = linkTagFn(cssList);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @returns string which contains the link tags containing the css which will
     * be injected later within <head></head> tag.
     *
     */
    /**
     * @private
     * @return {?} string which contains the link tags containing the css which will
     * be injected later within <head></head> tag.
     *
     */
    NgxPrintDirective.prototype.returnStyleSheetLinkTags = /**
     * @private
     * @return {?} string which contains the link tags containing the css which will
     * be injected later within <head></head> tag.
     *
     */
    function () {
        return this._styleSheetFile;
    };
    /**
     * @private
     * @param {?} tag
     * @return {?}
     */
    NgxPrintDirective.prototype.getElementTag = /**
     * @private
     * @param {?} tag
     * @return {?}
     */
    function (tag) {
        /** @type {?} */
        var html = [];
        /** @type {?} */
        var elements = document.getElementsByTagName(tag);
        for (var index = 0; index < elements.length; index++) {
            html.push(elements[index].outerHTML);
        }
        return html.join('\r\n');
    };
    /**
     *
     *
     * @memberof NgxPrintDirective
     */
    /**
     *
     *
     * \@memberof NgxPrintDirective
     * @return {?}
     */
    NgxPrintDirective.prototype.print = /**
     *
     *
     * \@memberof NgxPrintDirective
     * @return {?}
     */
    function () {
        /** @type {?} */
        var printContents;
        /** @type {?} */
        var popupWin;
        /** @type {?} */
        var styles = '';
        /** @type {?} */
        var links = '';
        if (this.useExistingCss) {
            styles = this.getElementTag('style');
            links = this.getElementTag('link');
        }
        printContents = document.getElementById(this.printSectionId).innerHTML;
        popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
        popupWin.document.open();
        popupWin.document.write("\n      <html>\n        <head>\n          <title>" + (this.printTitle ? this.printTitle : "") + "</title>\n          " + this.returnStyleValues() + "\n          " + this.returnStyleSheetLinkTags() + "\n          " + styles + "\n          " + links + "\n        </head>\n    <body onload=\"window.print();window.close()\">" + printContents + "</body>\n      </html>");
        popupWin.document.close();
    };
    NgxPrintDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"], args: [{
                    selector: "button[ngxPrint]"
                },] }
    ];
    NgxPrintDirective.propDecorators = {
        printSectionId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        printTitle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        useExistingCss: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        printStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        styleSheetFile: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        print: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['click',] }]
    };
    return NgxPrintDirective;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgxPrintModule = /** @class */ (function () {
    function NgxPrintModule() {
    }
    NgxPrintModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"], args: [{
                    declarations: [NgxPrintDirective],
                    imports: [],
                    exports: [NgxPrintDirective]
                },] }
    ];
    return NgxPrintModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */



//# sourceMappingURL=ngx-print.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/add-cart/add-cart.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/add-cart/add-cart.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"col-sm-12 empty-cart-cls text-center\" *ngIf=\"!selectCartItems.length\">\r\n    <img src=\"assets/images/icon-empty-cart.png\" class=\"img-fluid mb-4\">\r\n    <h3><strong>Your Cart is Empty</strong></h3>\r\n    <h4><strong>Add something to make me happy :)</strong></h4>\r\n    <a class=\"btn btn-primary cart-btn-transform\" [routerLink]=\"['/ecommerce/products']\">continue shopping</a>\r\n  </div>\r\n  <div class=\"row\" *ngIf='selectCartItems.length'>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Add To Cart</h5>\r\n        </div>\r\n        <div class=\"card-body cart\">\r\n          <div class=\"order-history table-responsive wishlist\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th>Prdouct</th>\r\n                  <th>Prdouct Name</th>\r\n                  <th>Price</th>\r\n                  <th>Quantity</th>\r\n                  <th>Action</th>\r\n                  <th>Total</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr class=\"title-orders\">\r\n                  <td colspan=\"12\">New Orders</td>\r\n                </tr>\r\n                <tr *ngFor=\"let item of selectCartItems\">\r\n                  <td><img class=\"img-fluid img-60\" [src]=\"[item.product.img]\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a\r\n                        [routerLink]=\"['/ecommerce/product-details', item.product.id]\">{{item.product.name}}</a></div>\r\n                  </td>\r\n                  <td>{{item.product.price | currency:productsService?.currency:'symbol'}}</td>\r\n                  <td>\r\n                    <div class=\"qty-box\">\r\n\r\n                      <div class=\"input-group\">\r\n                        <i class=\"fa fa-minus btnGtr\" (click)=\"decrement(item.product)\"></i>\r\n                        <input class=\"touchspin1 text-center\" name=\"quantity\" type=\"text\" disabled\r\n                          [value]=\"item.quantity\">\r\n                        <i class=\"fa fa-plus btnLess\" (click)=\"increment(item.product)\"></i>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <app-feather-icons class=\"remove\" [icon]=\"'x-circle'\" (click)=remove(item)></app-feather-icons>\r\n                  </td>\r\n\r\n                  <td>{{ item.product.price * item.quantity | currency:productsService?.currency:'symbol'}}</td>\r\n                <tr>\r\n                  <td class=\"total-amount\" colspan=\"5\">\r\n                    <h6 class=\"m-0\"> <span class=\"f-w-600\">Total Price:</span></h6>\r\n                  </td>\r\n                  <td><span>{{ getTotal() | async | currency:productsService?.currency:'symbol'}} </span></td>\r\n                </tr>\r\n                <tr>\r\n                  <td colspan=\"5\"><a class=\"btn btn-primary cart-btn-transform\"\r\n                      [routerLink]=\"['/ecommerce/products']\">continue shopping</a></td>\r\n                  <td><a class=\"btn btn-primary cart-btn-transform\" [routerLink]=\"['/ecommerce/check-out']\">check\r\n                      out</a></td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/check-out/check-out.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/check-out/check-out.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card checkout\">\r\n    <div class=\"card-header\">\r\n      <h5>Billing Details</h5>\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-6 col-sm-12\">\r\n          <form [formGroup]=\"checkoutForm\" novalidate>\r\n            <div class=\"form-row\">\r\n              <div class=\"form-group col-sm-6\">\r\n                <div class=\"field-label\">First Name</div>\r\n                <input type=\"text\" name=\"firstname\" formControlName=\"firstname\" class=\"form-control\" placeholder=\"\"\r\n                  [class.border-danger]=\"red_border\">\r\n                <div\r\n                  *ngIf=\"checkoutForm.controls['firstname'].touched && checkoutForm.controls['firstname'].errors?.required\"\r\n                  class=\"text text-danger\">\r\n                  First Name is required.\r\n                </div>\r\n                <div\r\n                  *ngIf=\"checkoutForm.controls['firstname'].touched && checkoutForm.controls['firstname'].errors?.pattern\"\r\n                  class=\"text text-danger\">\r\n                  First Name must be an alphabates.\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group col-sm-6\">\r\n                <div class=\"field-label\">Last Name </div>\r\n                <input type=\"text\" name=\"lastname\" formControlName=\"lastname\" class=\"form-control\" placeholder=\"\">\r\n                <div\r\n                  *ngIf=\"checkoutForm.controls['lastname'].touched && checkoutForm.controls['lastname'].errors?.required\"\r\n                  class=\"text text-danger\">\r\n                  Last Name is required.\r\n                </div>\r\n                <div\r\n                  *ngIf=\"checkoutForm.controls['lastname'].touched && checkoutForm.controls['lastname'].errors?.pattern\"\r\n                  class=\"text text-danger\">\r\n                  Last Name must be an alphabates.\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-row\">\r\n              <div class=\"form-group col-sm-6\">\r\n                <label for=\"inputEmail4\">Phone</label>\r\n                <input class=\"form-control\" type=\"number\" formControlName=\"phone\" class=\"form-control\">\r\n                <div *ngIf=\"checkoutForm.controls['phone'].touched && checkoutForm.controls['phone'].errors?.required\"\r\n                  class=\"text text-danger\">\r\n                  Phone No is required.\r\n                </div>\r\n                <div *ngIf=\"checkoutForm.controls['phone'].touched && checkoutForm.controls['phone'].errors?.pattern\"\r\n                  class=\"text text-danger\">\r\n                  Phone No is must be number.\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group col-sm-6\">\r\n                <label for=\"inputPassword4\">Email Address</label>\r\n                <input class=\"form-control\" type=\"email\" formControlName=\"email\" class=\"form-control\">\r\n                <div *ngIf=\"checkoutForm.controls['email'].touched && checkoutForm.controls['email'].errors?.required\"\r\n                  class=\"text text-danger\">\r\n                  Email is required\r\n                </div>\r\n                <div *ngIf=\"checkoutForm.controls['email'].touched && checkoutForm.controls['email'].errors?.email\"\r\n                  class=\"text text-danger\">\r\n                  Invalid Email\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"inputState\">Country</label>\r\n              <select name=\"country\" formControlName=\"country\" class=\"form-control\">\r\n                <option value=\"\" selected>Select Country</option>\r\n                <option value=\"India\">India</option>\r\n                <option value=\"USA\">USA</option>\r\n                <option value=\"Australia\">Australia</option>\r\n              </select>\r\n              <div *ngIf=\"checkoutForm.controls['country'].touched && checkoutForm.controls['country'].errors?.required\"\r\n                class=\"text text-danger\">\r\n                Country is required\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"inputAddress2\">Address</label>\r\n              <input class=\"form-control\" type=\"text\" formControlName=\"address\" class=\"form-control\">\r\n              <div *ngIf=\"checkoutForm.controls['address'].touched && checkoutForm.controls['address'].errors?.required\"\r\n                class=\"text text-danger\">\r\n                Address is required\r\n              </div>\r\n              <div\r\n                *ngIf=\"checkoutForm.controls['address'].touched && checkoutForm.controls['address'].errors?.maxlength\"\r\n                class=\"text text-danger\">\r\n                Maximum 50 character\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"inputCity\">Town/City</label>\r\n              <input class=\"form-control\" id=\"inputCity\" type=\"text\" formControlName=\"town\" class=\"form-control\">\r\n              <div *ngIf=\"checkoutForm.controls['town'].touched && checkoutForm.controls['town'].errors?.required\"\r\n                class=\"text text-danger\">\r\n                City is required\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"inputAddress2\">State/Country</label>\r\n              <input class=\"form-control\" id=\"inputAddress2\" type=\"text\" formControlName=\"state\" class=\"form-control\">\r\n              <div *ngIf=\"checkoutForm.controls['state'].touched && checkoutForm.controls['state'].errors?.required\"\r\n                class=\"text text-danger\">\r\n                State is required\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"inputAddress2\">Postal Code</label>\r\n              <input class=\"form-control\" id=\"inputAddress2\" type=\"number\" formControlName=\"postalcode\"\r\n                class=\"form-control\">\r\n              <div\r\n                *ngIf=\"checkoutForm.controls['postalcode'].touched && checkoutForm.controls['postalcode'].errors?.required\"\r\n                class=\"text text-danger\">\r\n                Postalcode is required\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <div class=\"form-check\">\r\n                <input class=\"form-check-input\" id=\"gridCheck\" type=\"checkbox\">\r\n                <label class=\"form-check-label\" for=\"gridCheck\">Check me out</label>\r\n              </div>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"col-lg-6 col-sm-12\">\r\n          <div class=\"checkout-details\">\r\n            <div class=\"order-box\">\r\n              <div class=\"title-box\">\r\n                <div class=\"checkbox-title\">\r\n                  <h4>Product </h4><span>Total</span>\r\n                </div>\r\n              </div>\r\n              <ul class=\"qty\" *ngIf='checkOutItems.length'>\r\n                <li *ngFor=\"let item of checkOutItems\"> {{ item.product.name }} × {{ item.quantity }}\r\n                  <span> {{ item.product.price * item.quantity | currency:productService?.currency:'symbol' }}</span>\r\n                </li>\r\n              </ul>\r\n              <ul class=\"qty\" *ngIf='!checkOutItems.length'>\r\n                <li class=\"empty-checkout\"> There are no products in cart </li>\r\n              </ul>\r\n              <ul class=\"sub-total\">\r\n                <li>Subtotal <span\r\n                    class=\"count\">{{getTotal() | async | currency:productService?.currency:'symbol'}}</span></li>\r\n              </ul>\r\n              <ul class=\"total\">\r\n                <li>Total <span\r\n                    class=\"count\">{{ getTotal() | async | currency:productService?.currency:'symbol'}}</span></li>\r\n              </ul>\r\n              <div class=\"animate-chk\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <label class=\"d-block\" for=\"edo-ani\">\r\n                      <input class=\"radio_animated\" id=\"edo-ani\" type=\"radio\" name=\"rdo-ani\" checked=\"\"\r\n                        data-original-title=\"\" title=\"\">Check Payments\r\n                    </label>\r\n                    <label class=\"d-block\" for=\"edo-ani1\">\r\n                      <input class=\"radio_animated\" id=\"edo-ani1\" type=\"radio\" name=\"rdo-ani\" data-original-title=\"\"\r\n                        title=\"\">Cash On Delivery\r\n                    </label>\r\n                    <label class=\"d-block\" for=\"edo-ani2\">\r\n                      <input class=\"radio_animated\" id=\"edo-ani2\" type=\"radio\" name=\"rdo-ani\" checked=\"\"\r\n                        data-original-title=\"\" title=\"\">PayPal\r\n                    </label>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"text-right\">\r\n                <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!checkoutForm.valid\"\r\n                  (click)=\"onSubmit()\">Place Order\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-right m-t-20\">\r\n            <button class=\"btn btn-primary cart-btn-transform\" [routerLink]=\"['/ecommerce/products']\">continue\r\n              shopping</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/brand/brand.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/colection/filter/brand/brand.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h6 class=\"f-w-600\">Brand</h6>\r\n<div class=\"checkbox-animated mt-0\">\r\n  <label class=\"d-block\" for=\"{{filter.brand}}\" *ngFor=\"let filter of tagsFilters\">\r\n    <input class=\"checkbox_animated\" type=\"checkbox\" [value]=\"filter.brand\" (change)=checkedFilter($event)\r\n      id=\"{{filter.brand}}\"> {{filter.brand}}\r\n  </label>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/color/color.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/colection/filter/color/color.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h6 class=\"f-w-600\">Colors</h6>\n<div class=\"color-selector\">\n  <ul>\n    <li [ngClass]=\"filter.color\" [class.active]=\"activeItem == filter.color\"\n      (click)=\"changeColor(activeItem == filter.color ? {} : filter)\" *ngFor=\"let filter of colorsFilters\"></li>\n  </ul>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/price/price.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/colection/filter/price/price.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng5-slider [(value)]=\"minValue\" [(highValue)]=\"maxValue\" [options]=\"options\" (userChangeStart)=\"onUserChangeStart($event)\" (userChange)=\"onUserChange($event)\"\n(userChangeEnd)=\"onUserChangeEnd($event)\"></ng5-slider>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/invoice/invoice.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/invoice/invoice.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"invoice\">\r\n            <div id=\"print-section\">\r\n              <div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-sm-6\">\r\n                    <div class=\"media\">\r\n                      <div class=\"media-left\"><img class=\"media-object img-60\"\r\n                          src=\"assets/images/other-images/logo-login.png\" alt=\"\"></div>\r\n                      <div class=\"media-body m-l-20\">\r\n                        <h4 class=\"media-heading\">Endless</h4>\r\n                        <p>hello@endless.in<br><span class=\"digits\">289-335-6503</span></p>\r\n                      </div>\r\n                    </div>\r\n                    <!-- End Info-->\r\n                  </div>\r\n                  <div class=\"col-sm-6\">\r\n                    <div class=\"text-md-right\">\r\n                      <h3>Invoice #<span class=\"digits counter\">1069</span></h3>\r\n                      <p>Issued: May<span class=\"digits\"> 27, 2015</span><br>\r\n                        Payment Due: <span class=\"digits\">{{date | date:'MMMM d , y'}}</span></p>\r\n                    </div>\r\n                    <!-- End Title-->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <hr>\r\n              <!-- End InvoiceTop-->\r\n              <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                  <div class=\"media\">\r\n                    <div class=\"media-left\"><img class=\"media-object rounded-circle img-60\"\r\n                        src=\"assets/images/user/1.jpg\" alt=\"\"></div>\r\n                    <div class=\"media-body m-l-20\">\r\n                      <h4 class=\"media-heading\">{{orderDetails.shippingDetails.firstname}}</h4>\r\n                      <p>{{orderDetails.shippingDetails.email}}<br><span\r\n                          class=\"digits\">{{orderDetails.shippingDetails.phone}}</span></p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                  <div class=\"text-md-right\" id=\"project\">\r\n                    <h6>Project Description</h6>\r\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.It is a long\r\n                      established fact that a reader will be distracted by the readable content of a page when looking\r\n                      at its layout.</p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!-- End Invoice Mid-->\r\n              <div *ngIf=\"orderDetails\">\r\n                <div class=\"table-responsive invoice-table\" id=\"table\">\r\n                  <table class=\"table table-bordered table-striped\">\r\n                    <tbody>\r\n                      <tr>\r\n                        <td class=\"item\">\r\n                          <h6 class=\"p-2 mb-0\">Product Name</h6>\r\n                        </td>\r\n                        <td class=\"Hours\">\r\n                          <h6 class=\"p-2 mb-0\">Quantity</h6>\r\n                        </td>\r\n                        <td class=\"Rate\">\r\n                          <h6 class=\"p-2 mb-0\">Price</h6>\r\n                        </td>\r\n                        <td class=\"subtotal\">\r\n                          <h6 class=\"p-2 mb-0\">Sub-total</h6>\r\n                        </td>\r\n                      </tr>\r\n                      <tr *ngFor=\"let item of orderDetails.product\">\r\n                        <td>\r\n                          <label>{{item.product.name}}</label>\r\n                        </td>\r\n                        <td>\r\n                          <p class=\"itemtext digits\">{{item.quantity}}</p>\r\n                        </td>\r\n                        <td>\r\n                          <p class=\"itemtext digits\">\r\n                            {{item.product.price | currency:productsService?.currency:'symbol'}}</p>\r\n                        </td>\r\n                        <td>\r\n                          <p class=\"itemtext digits\"></p>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td></td>\r\n                        <td></td>\r\n                        <td class=\"Rate\">\r\n                          <h6 class=\"mb-0 p-2\">Total</h6>\r\n                        </td>\r\n                        <td class=\"payment digits\">\r\n                          <h6 class=\"mb-0 p-2\">\r\n                            {{orderDetails.totalAmount | currency:productsService?.currency:'symbol'}}</h6>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n                <!-- End Table-->\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-8\">\r\n                    <div>\r\n                      <p class=\"legal\"><strong>Thank you for your business!</strong>  Payment is expected within 31\r\n                        days; please process this invoice within that time. There will be a 5% interest charge per month\r\n                        on late invoices.</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!-- End InvoiceBot-->\r\n            </div>\r\n            <div class=\"col-sm-12 text-center mt-3\">\r\n              <button class=\"btn btn btn-primary mr-2\"\r\n                [printStyle]=\"{h1 : {'color': 'red'}, h2 : {'border': 'solid 1px'}}\" printSectionId=\"print-section\"\r\n                ngxPrint>print</button>\r\n              <button class=\"btn btn-secondary\" type=\"button\" [routerLink]=\"['/ecommerce/check-out']\">Cancel</button>\r\n            </div>\r\n            <!-- End Invoice-->\r\n            <!-- End Invoice Holder-->\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/order-history/order-history.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/order-history/order-history.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Orders History</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"order-history table-responsive\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Prdouct</th>\r\n                  <th scope=\"col\">Prdouct name</th>\r\n                  <th scope=\"col\">Size</th>\r\n                  <th scope=\"col\">Color</th>\r\n                  <th scope=\"col\">Article number</th>\r\n                  <th scope=\"col\">Units</th>\r\n                  <th scope=\"col\">Price</th>\r\n                  <th scope=\"col\"><i class=\"fa fa-angle-down\"></i></th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr class=\"title-orders\">\r\n                  <td>New Orders</td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/1.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Long Top</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>M</td>\r\n                  <td>Lavander</td>\r\n                  <td>4215738</td>\r\n                  <td>1</td>\r\n                  <td>$21</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/13.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Fancy watch</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>35mm</td>\r\n                  <td>Blue</td>\r\n                  <td>5476182</td>\r\n                  <td>1</td>\r\n                  <td>$10</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/4.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Man shoes</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>8</td>\r\n                  <td>Black & white</td>\r\n                  <td>1756457</td>\r\n                  <td>1</td>\r\n                  <td>$18</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr class=\"title-orders\">\r\n                  <td>Shipped Orders</td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/10.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis side bag</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>22cm x 18cm</td>\r\n                  <td>Brown</td>\r\n                  <td>7451725</td>\r\n                  <td>1</td>\r\n                  <td>$13</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/12.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis Slipper</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>6</td>\r\n                  <td>Brown & white</td>\r\n                  <td>4127421</td>\r\n                  <td>1</td>\r\n                  <td>$6</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/3.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Fancy ledis Jacket</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Xl</td>\r\n                  <td>Light gray</td>\r\n                  <td>3581714</td>\r\n                  <td>1</td>\r\n                  <td>$24</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/2.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis Handbag</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>25cm x 20cm</td>\r\n                  <td>Black</td>\r\n                  <td>6748142</td>\r\n                  <td>1</td>\r\n                  <td>$14</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr class=\"title-orders\">\r\n                  <td>Cancelled Orders</td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                  <td></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/15.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Iphone6 mobile</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>10cm x 15cm</td>\r\n                  <td>Black</td>\r\n                  <td>5748214</td>\r\n                  <td>1</td>\r\n                  <td>$25</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/14.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Watch</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>27mm</td>\r\n                  <td>Brown</td>\r\n                  <td>2471254</td>\r\n                  <td>1</td>\r\n                  <td>$12</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/11.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Slipper</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>6</td>\r\n                  <td>Blue</td>\r\n                  <td>8475112</td>\r\n                  <td>1</td>\r\n                  <td>$6</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Datatable order history</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"order-history table-responsive\">\r\n            <table class=\"table table-bordernone display\" id=\"basic-1\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Prdouct</th>\r\n                  <th scope=\"col\">Prdouct name</th>\r\n                  <th scope=\"col\">Size</th>\r\n                  <th scope=\"col\">Color</th>\r\n                  <th scope=\"col\">Article number</th>\r\n                  <th scope=\"col\">Units</th>\r\n                  <th scope=\"col\">Price</th>\r\n                  <th scope=\"col\"><i class=\"fa fa-angle-down\"></i></th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/1.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Long Top</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>M</td>\r\n                  <td>Lavander</td>\r\n                  <td>4215738</td>\r\n                  <td>1</td>\r\n                  <td>$21</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/13.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Fancy watch</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>35mm</td>\r\n                  <td>Blue</td>\r\n                  <td>5476182</td>\r\n                  <td>1</td>\r\n                  <td>$10</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/4.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Man shoes</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle\"></span> Processing</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>8</td>\r\n                  <td>Black & white</td>\r\n                  <td>1756457</td>\r\n                  <td>1</td>\r\n                  <td>$18</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/10.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis side bag</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>22cm x 18cm</td>\r\n                  <td>Brown</td>\r\n                  <td>7451725</td>\r\n                  <td>1</td>\r\n                  <td>$13</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/12.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis Slipper</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>6</td>\r\n                  <td>Brown & white</td>\r\n                  <td>4127421</td>\r\n                  <td>1</td>\r\n                  <td>$6</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/3.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Fancy ledis Jacket</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Xl</td>\r\n                  <td>Light gray</td>\r\n                  <td>3581714</td>\r\n                  <td>1</td>\r\n                  <td>$24</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/2.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Ledis Handbag</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle shipped-order\"></span> Shipped</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>25cm x 20cm</td>\r\n                  <td>Black</td>\r\n                  <td>6748142</td>\r\n                  <td>1</td>\r\n                  <td>$14</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/15.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Iphone6 mobile</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>10cm x 15cm</td>\r\n                  <td>Black</td>\r\n                  <td>5748214</td>\r\n                  <td>1</td>\r\n                  <td>$25</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/14.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Watch</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>27mm</td>\r\n                  <td>Brown</td>\r\n                  <td>2471254</td>\r\n                  <td>1</td>\r\n                  <td>$12</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><img class=\"img-fluid img-60\" src=\"assets/images/product/11.png\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a href=\"javascript:void(0)\">Slipper</a>\r\n                      <div class=\"order-process\"><span class=\"order-process-circle cancel-order\"></span> Cancelled</div>\r\n                    </div>\r\n                  </td>\r\n                  <td>6</td>\r\n                  <td>Blue</td>\r\n                  <td>8475112</td>\r\n                  <td>1</td>\r\n                  <td>$6</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'more-vertical'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/payment-detail/payment-detail.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/payment-detail/payment-detail.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid credit-card\">\r\n  <div class=\"row\">\r\n    <!-- Individual column searching (text inputs) Starts-->\r\n    <div class=\"col-xl-8\">\r\n      <div class=\"card height-equal\">\r\n        <div class=\"card-header\">\r\n          <h5>Credit card </h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-4 text-center\"><img class=\"img-fluid\" src=\"assets/images/ecommerce/card.png\" alt=\"\">\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <form class=\"theme-form mega-form\">\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Card number\">\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"First Name\">\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" type=\"date\">\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Full Name\">\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Individual column searching (text inputs) Ends-->\r\n    <!-- Debit Card Starts-->\r\n    <div class=\"col-xl-4 col-lg-6 debit-card\">\r\n      <div class=\"card height-equal\">\r\n        <div class=\"card-header\">\r\n          <h5>Debit card </h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"theme-form e-commerce-form row\">\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Full name here\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Card number\">\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"CVV number\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"CVC\">\r\n            </div>\r\n            <div class=\"col-12\">\r\n              <label class=\"col-form-label p-t-0\">Expiration Date</label>\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Select Month</option>\r\n                <option>Jan</option>\r\n                <option>Fab</option>\r\n                <option>March</option>\r\n                <option>April</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Select Year</option>\r\n                <option>2015</option>\r\n                <option>2016</option>\r\n                <option>2017</option>\r\n                <option>2018</option>\r\n                <option>2019</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"col-12\">\r\n              <button class=\"btn btn-primary-gradien btn-block\" type=\"button\">Submit</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Debit Card Ends-->\r\n    <!-- COD Starts-->\r\n    <div class=\"col-xl-4 col-lg-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>COD</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"theme-form row\">\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"First Name\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Last name\">\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Pincode\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"number\" placeholder=\"Enter mobile number\">\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"State\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"City\">\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <textarea class=\"form-control\" rows=\"3\" placeholder=\"Address\"></textarea>\r\n            </div>\r\n            <div class=\"col-12\">\r\n              <button class=\"btn btn-primary-gradien btn-block\" type=\"button\">Submit</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- COD Ends-->\r\n    <!-- EMI Starts-->\r\n    <div class=\"col-xl-4 col-lg-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>EMI</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"theme-form row\">\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"First Name\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Last name\">\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Pincode\">\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Bank Name</option>\r\n                <option>SBI</option>\r\n                <option>ICICI</option>\r\n                <option>KOTAK</option>\r\n                <option>BOB</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Select Card</option>\r\n                <option>2</option>\r\n                <option>3</option>\r\n                <option>4</option>\r\n                <option>5</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Select Duration</option>\r\n                <option>2015-2016</option>\r\n                <option>2016-2017</option>\r\n                <option>2017-2018</option>\r\n                <option>2018-2019</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <ul class=\"payment-opt\">\r\n                <li><img src=\"assets/images/ecommerce/mastercard.png\" alt=\"\"></li>\r\n                <li><img src=\"assets/images/ecommerce/visa.png\" alt=\"\"></li>\r\n                <li><img src=\"assets/images/ecommerce/paypal.png\" alt=\"\"></li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"col-12\">\r\n              <button class=\"btn btn-primary-gradien btn-block\" type=\"button\">Submit</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- EMI Ends-->\r\n    <!-- EMI Starts-->\r\n    <div class=\"col-xl-4 col-lg-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Net Banking</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"theme-form row\">\r\n            <div class=\"form-group col-12\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"AC Holder name\">\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Account number\">\r\n            </div>\r\n            <div class=\"form-group col-6 p-r-0\">\r\n              <select class=\"form-control\" size=\"1\">\r\n                <option>Select Bank</option>\r\n                <option>SBI</option>\r\n                <option>ICICI</option>\r\n                <option>KOTAK</option>\r\n                <option>BOB</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-6\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"ICFC code\">\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <input class=\"form-control\" type=\"number\" placeholder=\"Enter mobile number\">\r\n            </div>\r\n            <div class=\"form-group col-12\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Other Details\">\r\n            </div>\r\n            <div class=\"col-12\">\r\n              <button class=\"btn btn-primary-gradien btn-block\" type=\"button\">Submit</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- EMI Ends-->\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/product-detail/product-detail.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/product-detail/product-detail.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"card\">\r\n        <div class=\"row product-page-main\">\r\n            <div class=\"col-xl-4\">\r\n                <section>\r\n                    <ks-carousel [id]=\"100\" [images]=\"imagesRect\" [dotsConfig]=\"{visible: false}\"\r\n                        [carouselConfig]=\"{maxHeight: '600px', keyboardEnable: true}\"\r\n                        [previewConfig]=\"{maxHeight: '125px'}\">\r\n                    </ks-carousel>\r\n                </section>\r\n            </div>\r\n            <div class=\"col-xl-8\">\r\n                <div class=\"product-page-details\">\r\n                    <h5>{{product.name}}</h5>\r\n                    <div class=\"d-flex\">\r\n                        <ngb-rating [rate]=\"3\" class=\"rating-size-product\"></ngb-rating>\r\n                        <span>{{product.review}}</span>\r\n                    </div>\r\n                </div>\r\n                <hr>\r\n                <p>{{product.discription}}</p>\r\n                <div class=\"product-price digits\">\r\n                    <del>{{product.discountPrice | currency:productService?.currency:'symbol'}}\r\n                    </del>{{product.price | currency:productService?.currency:'symbol'}}\r\n                </div>\r\n                <hr>\r\n                <div>\r\n                    <table class=\"product-page-width\">\r\n                        <tbody>\r\n                            <tr>\r\n                                <td><b>Brand : </b>{{product.tags}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td><b>Availability: </b>{{product.stock}}</td>\r\n\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <hr>\r\n                <ul class=\"product-color m-t-15\">\r\n                    <li class=\"bg-primary\"></li>\r\n                    <li class=\"bg-secondary\"></li>\r\n                    <li class=\"bg-success\"></li>\r\n                    <li class=\"bg-info\"></li>\r\n                    <li class=\"bg-warning\"></li>\r\n                </ul>\r\n                <div class=\"m-t-15\">\r\n                    <button class=\"btn btn-primary-gradien m-r-10\" type=\"button\"\r\n                        data-original-title=\"btn btn-info-gradien\" [routerLink]=\"['/ecommerce/add-cart']\"\r\n                        (click)=addToCart(product) title=\"\">\r\n                        Add To Cart</button>\r\n                    <button class=\"btn btn-success-gradien\" type=\"button\" data-original-title=\"btn btn-info-gradien\"\r\n                        (click)=buyNow(product) title=\"\">Buy Now</button>\r\n                    <button class=\"btn btn-primary-gradien m-r-10 m-l-10\" type=\"button\"\r\n                        data-original-title=\"btn btn-info-gradien\" [routerLink]=\"['/ecommerce/products']\" title=\"\">\r\n                        Back To Product</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"card\">\r\n        <div class=\"row product-page-main\">\r\n            <div class=\"col-sm-12\">\r\n                <ul class=\"nav nav-tabs nav-material mb-0\" id=\"top-tab\" role=\"tablist\">\r\n                    <li class=\"nav-item\"><a class=\"nav-link\" [ngClass]=\"{'active': type == 'Febric'}\" id=\"top-home-tab\"\r\n                            data-toggle=\"tab\" (click)=\"getOption(type = 'Febric')\" role=\"tab\" aria-controls=\"top-home\"\r\n                            aria-selected=\"false\">Febric</a>\r\n                        <div class=\"material-border\"></div>\r\n                    </li>\r\n                    <li class=\"nav-item\"><a class=\"nav-link\" [ngClass]=\"{'active': type == 'Video'}\"\r\n                            id=\"profile-top-tab\" data-toggle=\"tab\" (click)=\"getOption(type = 'Video')\" role=\"tab\"\r\n                            aria-controls=\"top-profile\" aria-selected=\"false\">Video</a>\r\n                        <div class=\"material-border\"></div>\r\n                    </li>\r\n                    <li class=\"nav-item\"><a class=\"nav-link\" [ngClass]=\"{'active': type == 'Details'}\"\r\n                            id=\"contact-top-tab\" data-toggle=\"tab\" (click)=\"getOption(type = 'Details')\" role=\"tab\"\r\n                            aria-controls=\"top-contact\" aria-selected=\"true\">Details</a>\r\n                        <div class=\"material-border\"></div>\r\n                    </li>\r\n                    <li class=\"nav-item\"><a class=\"nav-link\" [ngClass]=\"{'active': type == 'Brand'}\" id=\"brand-top-tab\"\r\n                            data-toggle=\"tab\" (click)=\"getOption(type = 'Brand')\" role=\"tab\" aria-controls=\"top-brand\"\r\n                            aria-selected=\"true\">Brand</a>\r\n                        <div class=\"material-border\"></div>\r\n                    </li>\r\n                </ul>\r\n                <div class=\"tab-content\" id=\"top-tabContent\" *ngFor=\"let item of contents\">\r\n                    <div class=\"tab-pane fade active show\" id=\"top-home\" role=\"tabpanel\" aria-labelledby=\"top-home-tab\">\r\n                        <p class=\"mb-0 m-t-20\">{{item.content_1}}</p>\r\n                        <p class=\"mb-0 m-t-20\">{{item.content_2}}\"</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/product-list/product-list.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/product-list/product-list.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <!-- Individual column searching (text inputs) Starts-->\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Individual column searching (text inputs) </h5><span>Search your product from here..</span>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"table-responsive product-list-custom server-datatable\">\r\n            <div class=\"table-responsive\">\r\n              <ng2-smart-table [settings]=\"settings\" [source]=\"products\"></ng2-smart-table>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Individual column searching (text inputs) Ends-->\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/products/products.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/products/products.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid product-wrapper\" [class.sidebaron]=\"sidebaron\">\r\n  <div class=\"product-grid\">\r\n    <div class=\"feature-products\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 products-total\">\r\n\r\n          <div class=\"square-product-setting d-inline-block\"><a class=\"icon-grid grid-layout-view\" (click)=\"gridOpen()\"\r\n              data-original-title=\"\" title=\"\">\r\n              <svg class=\"feather feather-grid\" xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\r\n                viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\r\n                stroke-linejoin=\"round\">\r\n                <rect x=\"3\" y=\"3\" width=\"7\" height=\"7\"></rect>\r\n                <rect x=\"14\" y=\"3\" width=\"7\" height=\"7\"></rect>\r\n                <rect x=\"14\" y=\"14\" width=\"7\" height=\"7\"></rect>\r\n                <rect x=\"3\" y=\"14\" width=\"7\" height=\"7\"></rect>\r\n              </svg></a></div>\r\n          <div class=\"square-product-setting d-inline-block\"><a class=\"icon-grid m-0 list-layout-view\"\r\n              (click)=\"listOpen()\" data-original-title=\"\" title=\"\">\r\n              <svg class=\"feather feather-list\" xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\"\r\n                viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\r\n                stroke-linejoin=\"round\">\r\n                <line x1=\"8\" y1=\"6\" x2=\"21\" y2=\"6\"></line>\r\n                <line x1=\"8\" y1=\"12\" x2=\"21\" y2=\"12\"></line>\r\n                <line x1=\"8\" y1=\"18\" x2=\"21\" y2=\"18\"></line>\r\n                <line x1=\"3\" y1=\"6\" x2=\"3\" y2=\"6\"></line>\r\n                <line x1=\"3\" y1=\"12\" x2=\"3\" y2=\"12\"></line>\r\n                <line x1=\"3\" y1=\"18\" x2=\"3\" y2=\"18\"></line>\r\n              </svg></a></div>\r\n          <span class=\"d-none-productlist filter-toggle\">\r\n            <a (click)=\"openMediaFilter()\">\r\n              <h6 class=\"mb-0\">Filters\r\n                <span class=\"ml-2\">\r\n                  <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\r\n                    stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\"\r\n                    class=\"feather feather-chevron-down toggle-data\">\r\n                    <polyline points=\"6 9 12 15 18 9\"></polyline>\r\n                  </svg>\r\n                </span>\r\n              </h6>\r\n            </a>\r\n          </span>\r\n          <div class=\"grid-options d-inline-block\" *ngIf=\"gridOptions\">\r\n            <ul>\r\n              <li><a class=\"product-2-layout-view\" (click)=\"grid2()\" data-original-title=\"\" title=\"\"><span\r\n                    class=\"line-grid line-grid-1 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-2 bg-primary\"></span></a> </li>\r\n              <li><a class=\"product-3-layout-view\" (click)=\"grid3()\" data-original-title=\"\" title=\"\"><span\r\n                    class=\"line-grid line-grid-3 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-4 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-5 bg-primary\"></span></a></li>\r\n              <li><a class=\"product-4-layout-view\" (click)=\"gridOpen()\" data-original-title=\"\" title=\"\"><span\r\n                    class=\"line-grid line-grid-6 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-7 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-8 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-9 bg-primary\"></span></a></li>\r\n              <li><a class=\"product-6-layout-view\" (click)=\"grid6()\" data-original-title=\"\" title=\"\"><span\r\n                    class=\"line-grid line-grid-10 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-11 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-12 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-13 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-14 bg-primary\"></span><span\r\n                    class=\"line-grid line-grid-15 bg-primary\"></span></a></li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6 text-right\"><span class=\"f-w-600 m-r-10\">Showing Products 1 - 24 Of 200 Results</span>\r\n          <div class=\"select2-drpdwn-product select-options d-inline-block\">\r\n            <select class=\"form-control btn-square\" name=\"select\" (change)=\"onChangeSorting($event.target.value)\">\r\n              <option value=\"low\">Lowest Prices</option>\r\n              <option value=\"high\">Highest Prices</option>\r\n            </select>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"product-sidebar\" [class.open]=\"open\">\r\n            <div class=\"filter-section\">\r\n              <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                  <h6 class=\"mb-0 f-w-600\">Filters<span class=\"pull-right\"><i class=\"fa fa-chevron-down toggle-data\"\r\n                        (click)=\"openFilter()\"></i></span></h6>\r\n                </div>\r\n                <div class=\"left-filter\" *ngIf=\"show\">\r\n                  <div class=\"card-body filter-cards-view animate-chk\">\r\n                    <div class=\"product-filter\">\r\n                    </div>\r\n                    <div class=\"product-filter\">\r\n                      <app-brand [tagsFilters]=\"tags\" (tagFilters)=\"updateTagFilters($event)\" *ngIf=\"tags.length\">\r\n                      </app-brand>\r\n                    </div>\r\n                    <div class=\"product-filter slider-product\">\r\n                      <app-color [colorsFilters]=\"colors\" (colorFilters)=\"updateColor($event)\" [class.active]=\"active\"\r\n                        *ngIf=\"colors.length\"></app-color>\r\n                    </div>\r\n                    <div class=\"product-filter pb-0\">\r\n                      <app-price (priceFilters)=\"updatePriceFilters($event)\"></app-price>\r\n                    </div>\r\n                    <div class=\"product-filter new-products\">\r\n                      <h6 class=\"f-w-600\">New Products</h6>\r\n                      <div class=\"item\">\r\n                        <owl-carousel-o [options]=\"customOptions\" style=\"width: 310.5px; margin-right: 30px;\">\r\n                          <ng-container *ngFor=\"let slide of products\">\r\n                            <ng-template carouselSlide>\r\n                              <div class=\"product-box row\">\r\n                                <div class=\"product-img col-md-6\">\r\n                                  <div class=\"item\">\r\n                                    <img src=\"assets/images/ecommerce/01.jpg\">\r\n                                  </div>\r\n                                </div>\r\n                                <div class=\"product-details col-md-6 text-left\"><span><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning\"></i></span>\r\n                                  <p class=\"mb-0\">{{slide.name}}</p>\r\n                                  <div class=\"product-price\">${{slide.price}}</div>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"product-box row\">\r\n                                <div class=\"product-img col-md-6\">\r\n                                  <div class=\"item\">\r\n                                    <img src=\"assets/images/ecommerce/03.jpg\">\r\n                                  </div>\r\n                                </div>\r\n                                <div class=\"product-details col-md-6 text-left\"><span><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning\"></i></span>\r\n                                  <p class=\"mb-0\">{{slide.name}}</p>\r\n                                  <div class=\"product-price\">${{slide.price}}</div>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"product-box row\">\r\n                                <div class=\"product-img col-md-6\">\r\n                                  <div class=\"item\">\r\n                                    <img src=\"assets/images/ecommerce/02.jpg\">\r\n                                  </div>\r\n                                </div>\r\n                                <div class=\"product-details col-md-6 text-left\"><span><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning mr-1\"></i><i\r\n                                      class=\"fa fa-star font-warning\"></i></span>\r\n                                  <p class=\"mb-0\">{{slide.name}}</p>\r\n                                  <div class=\"product-price\">${{slide.price}}</div>\r\n                                </div>\r\n                              </div>\r\n                            </ng-template>\r\n                          </ng-container>\r\n                        </owl-carousel-o>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"product-filter text-center\"><img class=\"img-fluid banner-product\"\r\n                        src=\"assets/images/ecommerce/banner.jpg\" alt=\"\" data-original-title=\"\" title=\"\"></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-9\">\r\n          <form>\r\n            <div class=\"form-group m-0\">\r\n              <input class=\"form-control\" type=\"search\" [(ngModel)]=\"term\" name=\"search\" placeholder=\"Search..\"><i\r\n                class=\"fa fa-search\"></i>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"product-wrapper-grid\" [class.list-view]=\"listView\" style=\"opacity: 1;\">\r\n      <div class=\"row\">\r\n        <div [class.col-xl-3]=\"col_xl_3\" [class.xl-4]=\"xl_4\" [class.col-sm-3]=\"col_sm_3\"\r\n          [class.col-xl-4]=\"col_xl_4\" [class.col-sm-4]=\"col_sm_4\" [class.col-xl-6]=\"col_xl_6\"\r\n          [class.col-sm-6]=\"col_sm_6\" [class.col-xl-12]=\"col_xl_12\" [class.col-xl-2]=\"col_xl_2\"\r\n          *ngFor=\"let product of filterItems() | orderBy:sortByOrder | filter : term\">\r\n          <div class=\"card\">\r\n            <div class=\"product-box\">\r\n              <div class=\"product-img\">\r\n                <div class=\"ribbon ribbon-danger\" *ngIf=\"product.status == 'sale'\">{{product.status}}</div>\r\n                <div class=\"ribbon ribbon-success ribbon-right\" *ngIf=\"product.status == '50%'  \">{{product.status}}\r\n                </div>\r\n                <div class=\"ribbon ribbon-secondary ribbon-vertical-left\" *ngIf=\"product.status == 'gift'\"><i\r\n                    class=\"icon-gift\"></i></div>\r\n                <div class=\"ribbon ribbon-bookmark ribbon-vertical-right ribbon-info\"\r\n                  *ngIf=\"product.status == 'love'  \"><i class=\"icofont icofont-love\"></i>\r\n                </div>\r\n                <div class=\"ribbon ribbon ribbon-clip ribbon-warning\" *ngIf=\"product.status == 'Hot' \">\r\n                  {{product.status}}</div>\r\n                <a [href]=\"[product.img]\"> <img class=\"img-fluid\" [src]=\"[product.img]\" alt=\"\"></a>\r\n                <div class=\"product-hover\">\r\n                  <ul>\r\n                    <li><i class=\"icon-shopping-cart\" [routerLink]=\"['/ecommerce/add-cart']\"\r\n                        (click)=addToCart(product)></i></li>\r\n                    <li><i class=\"icon-eye\" (click)=\"openProductDetail(content,product.id)\"></i></li>\r\n                    <ng-template #content let-modal>\r\n                      <div class=\"modal-header\" style=\"border-bottom: 0px;\">\r\n                        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss('Cross click')\">\r\n                          <span aria-hidden=\"true\">&times;</span>\r\n                        </button>\r\n                      </div>\r\n                      <div class=\"modal-body\" style=\"padding: 0%;\">\r\n                        <app-quick-view [productDetail]=\"productDetail\"></app-quick-view>\r\n                      </div>\r\n                    </ng-template>\r\n                    <li>\r\n                      <button class=\"btn\" type=\"button\" data-original-title=\"\" title=\"\"><i\r\n                          class=\"icofont icofont-heart\" [routerLink]=\"['/ecommerce/wish-list']\"\r\n                          (click)=addToWishlist(product)></i></button>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n              <div class=\"product-details\">\r\n                <a [routerLink]=\"['/ecommerce/product-details', product.id]\">\r\n                  <h5>{{product.name}}</h5>\r\n                </a>\r\n                <div class=\"product-price\">\r\n                  <del>{{product.discountPrice | currency:productsService?.currency:'symbol'}}</del>{{product.price | currency:productsService?.currency:'symbol'}}\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n          <div *ngIf=\"(filterItems() | filter:term).length == 0\">\r\n            <div class=\"search-not-found text-center\">\r\n              <img src=\"assets/images/search-not-found.png\" alt=\"\" class=\"second-search\">\r\n              <p>Sorry, We didn't find any results matching this search</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/quick-view/quick-view.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/quick-view/quick-view.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <div class=\"product-box row\">\r\n    <div class=\"product-img col-md-6\"><img class=\"img-fluid\" [src]=\"[productDetail?.img]\" alt=\"\"></div>\r\n    <div class=\"product-details col-md-6 text-left\">\r\n      <h1>{{productDetail?.category}}</h1>\r\n      <div class=\"product-price\">\r\n        <del>{{productDetail?.discountPrice | currency:productService?.currency:'symbol'}}</del>{{productDetail?.price | currency:productService?.currency:'symbol'}}\r\n      </div>\r\n      <div class=\"product-view\">\r\n        <h6 class=\"f-w-600\">Product Details</h6>\r\n        <p class=\"mb-0\">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque\r\n          laudantium, totam rem aperiam eaque ipsa, quae ab illo.</p>\r\n      </div>\r\n      <div class=\"product-size\">\r\n        <ul>\r\n          <li>\r\n            <button class=\"btn btn-outline-light\" type=\"button\">M</button>\r\n          </li>\r\n          <li>\r\n            <button class=\"btn btn-outline-light\" type=\"button\">L</button>\r\n          </li>\r\n          <li>\r\n            <button class=\"btn btn-outline-light\" type=\"button\">Xl</button>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"product-qnty\">\r\n        <h6 class=\"f-w-600\">Quantity</h6>\r\n        <div class=\"qty-box1\">\r\n          <div class=\"input-group\">\r\n            <i class=\"fa fa-minus btnGtr1\" (click)=\"decrement()\"></i>\r\n            <input class=\"touchspin1 text-center\" #qty name=\"quantity\" type=\"text\" value=\"{{counter}}\">\r\n            <i class=\"fa fa-plus btnLess1\" (click)=\"increment()\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"addcart-btn\">\r\n          <button class=\"btn btn-primary-gradien m-r-10\" type=\"button\" data-original-title=\"btn btn-info-gradien\"\r\n            [routerLink]=\"['/ecommerce/add-cart']\" (click)=addToCart(productDetail,qty.value) title=\"\">\r\n            Add To Cart</button>\r\n          <button class=\"btn btn-success-gradien\" type=\"button\" data-original-title=\"btn btn-info-gradien\"\r\n            (click)=buyNow(productDetail,qty.value) title=\"\">Buy Now</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/wish-list/wish-list.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/e-commerce/wish-list/wish-list.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Wishlist</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"order-history table-responsive wishlist\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th>Prdouct</th>\r\n                  <th>Prdouct Name</th>\r\n                  <th>Price</th>\r\n                  <th>Availability</th>\r\n                  <th>Action</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr class=\"title-orders\">\r\n                  <td colspan=\"12\">New Orders</td>\r\n                </tr>\r\n                <tr *ngFor=\"let item of selectCartItems\">\r\n                  <td><img class=\"img-fluid img-60\" [src]=\"[item.product.img]\" alt=\"#\"></td>\r\n                  <td>\r\n                    <div class=\"product-name\"><a\r\n                        [routerLink]=\"['/ecommerce/product-details', item.product.id]\">{{item.product.name}}</a></div>\r\n                  </td>\r\n                  <td>{{item.product.price | currency:productsService?.currency:'symbol'}}</td>\r\n                  <td>{{item.product.stock}}</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'x-circle'\" (click)=remove(item)></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td colspan=\"5\"><a class=\"btn btn-primary cart-btn-transform\"\r\n                      [routerLink]=\"['/ecommerce/products']\">continue shopping</a></td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/map.js":
/*!************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/map.js ***!
  \************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/map */ "./node_modules/rxjs-compat/_esm5/operator/map.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.map = _operator_map__WEBPACK_IMPORTED_MODULE_1__["map"];
//# sourceMappingURL=map.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/map.js":
/*!********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/map.js ***!
  \********************************************************/
/*! exports provided: map */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

function map(project, thisArg) {
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(project, thisArg)(this);
}
//# sourceMappingURL=map.js.map

/***/ }),

/***/ "./src/app/components/e-commerce/add-cart/add-cart.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/e-commerce/add-cart/add-cart.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9hZGQtY2FydC9hZGQtY2FydC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/e-commerce/add-cart/add-cart.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/e-commerce/add-cart/add-cart.component.ts ***!
  \**********************************************************************/
/*! exports provided: AddCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCartComponent", function() { return AddCartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/e-commerce/cart.service */ "./src/app/shared/services/e-commerce/cart.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddCartComponent = /** @class */ (function () {
    function AddCartComponent(route, cartService) {
        this.route = route;
        this.cartService = cartService;
        this.cartItems = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])([]);
        this.selectCartItems = [];
    }
    //remove cart
    AddCartComponent.prototype.remove = function (item) {
        this.cartService.removeCartItem(item);
    };
    //get total amount
    AddCartComponent.prototype.getTotal = function () {
        return this.cartService.getTotalAmount();
    };
    //product quentity decrement
    AddCartComponent.prototype.decrement = function (product, quantity) {
        if (quantity === void 0) { quantity = -1; }
        this.cartService.updateCartQuantity(product, quantity);
    };
    //product quentity increment
    AddCartComponent.prototype.increment = function (product, quantity) {
        if (quantity === void 0) { quantity = +1; }
        this.cartService.updateCartQuantity(product, quantity);
    };
    AddCartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cartItems = this.cartService.getAll();
        this.cartItems.subscribe(function (selectCartItems) { return _this.selectCartItems = selectCartItems; });
    };
    AddCartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-cart',
            template: __webpack_require__(/*! raw-loader!./add-cart.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/add-cart/add-cart.component.html"),
            styles: [__webpack_require__(/*! ./add-cart.component.scss */ "./src/app/components/e-commerce/add-cart/add-cart.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"]])
    ], AddCartComponent);
    return AddCartComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/check-out/check-out.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/components/e-commerce/check-out/check-out.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9jaGVjay1vdXQvY2hlY2stb3V0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/check-out/check-out.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/e-commerce/check-out/check-out.component.ts ***!
  \************************************************************************/
/*! exports provided: CheckOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckOutComponent", function() { return CheckOutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/e-commerce/cart.service */ "./src/app/shared/services/e-commerce/cart.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_e_commerce_invoice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/e-commerce/invoice.service */ "./src/app/shared/services/e-commerce/invoice.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CheckOutComponent = /** @class */ (function () {
    function CheckOutComponent(fb, productService, cartService, invoiceService) {
        this.fb = fb;
        this.productService = productService;
        this.cartService = cartService;
        this.invoiceService = invoiceService;
        this.red_border = false;
        this.cartItems = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])([]);
        this.checkOutItems = [];
        this.submitted = false;
        this.createForm();
    }
    CheckOutComponent.prototype.createForm = function () {
        this.checkoutForm = this.fb.group({
            firstname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
            lastname: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
            phone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9]+')]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            address: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(50)]],
            town: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            postalcode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    };
    CheckOutComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.checkoutForm.invalid) {
            this.red_border = true;
            return;
        }
        this.userInfo = this.checkoutForm.value;
        this.invoiceService.createOrder(this.checkOutItems, this.userInfo, this.amount);
    };
    CheckOutComponent.prototype.getTotal = function () {
        return this.cartService.getTotalAmount();
    };
    CheckOutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cartItems = this.cartService.getAll();
        this.cartItems.subscribe(function (products) { return _this.checkOutItems = products; });
        this.getTotal().subscribe(function (amount) { return _this.amount = amount; });
    };
    CheckOutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-check-out',
            template: __webpack_require__(/*! raw-loader!./check-out.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/check-out/check-out.component.html"),
            styles: [__webpack_require__(/*! ./check-out.component.scss */ "./src/app/components/e-commerce/check-out/check-out.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsService"], _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _shared_services_e_commerce_invoice_service__WEBPACK_IMPORTED_MODULE_5__["InvoiceService"]])
    ], CheckOutComponent);
    return CheckOutComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/brand/brand.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/brand/brand.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9jb2xlY3Rpb24vZmlsdGVyL2JyYW5kL2JyYW5kLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/brand/brand.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/brand/brand.component.ts ***!
  \*********************************************************************************/
/*! exports provided: BrandComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrandComponent", function() { return BrandComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BrandComponent = /** @class */ (function () {
    function BrandComponent() {
        this.tagsFilters = [];
        this.tagFilters = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // Array
        this.checkedTagsArray = [];
    }
    // value checked call this function
    BrandComponent.prototype.checkedFilter = function (event) {
        var index = this.checkedTagsArray.indexOf(event.target.value); // checked and unchecked value
        if (event.target.checked)
            this.checkedTagsArray.push(event.target.value); // push in array cheked value
        else
            this.checkedTagsArray.splice(index, 1); // removed in array unchecked value           
    };
    BrandComponent.prototype.ngOnInit = function () {
        this.tagFilters.emit(this.checkedTagsArray); // Pass value Using emit 
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], BrandComponent.prototype, "tagsFilters", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], BrandComponent.prototype, "tagFilters", void 0);
    BrandComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-brand',
            template: __webpack_require__(/*! raw-loader!./brand.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/brand/brand.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./brand.component.scss */ "./src/app/components/e-commerce/colection/filter/brand/brand.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BrandComponent);
    return BrandComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/color/color.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/color/color.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9jb2xlY3Rpb24vZmlsdGVyL2NvbG9yL2NvbG9yLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/color/color.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/color/color.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ColorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColorComponent", function() { return ColorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ColorComponent = /** @class */ (function () {
    function ColorComponent(productService) {
        this.productService = productService;
        this.activeItem = '';
        this.colorsFilters = [];
        this.colorFilters = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    // Click to call function 
    ColorComponent.prototype.changeColor = function (colors) {
        this.activeItem = colors.color;
        if (colors.color) {
            this.colorFilters.emit([colors]);
        }
        else {
            this.colorFilters.emit([]);
        }
    };
    ColorComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ColorComponent.prototype, "colorsFilters", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ColorComponent.prototype, "colorFilters", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ColorComponent.prototype, "uniqueProductColor", void 0);
    ColorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-color',
            template: __webpack_require__(/*! raw-loader!./color.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/color/color.component.html"),
            styles: [__webpack_require__(/*! ./color.component.scss */ "./src/app/components/e-commerce/colection/filter/color/color.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsService"]])
    ], ColorComponent);
    return ColorComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/price/price.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/price/price.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9jb2xlY3Rpb24vZmlsdGVyL3ByaWNlL3ByaWNlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/colection/filter/price/price.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/e-commerce/colection/filter/price/price.component.ts ***!
  \*********************************************************************************/
/*! exports provided: PriceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceComponent", function() { return PriceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PriceComponent = /** @class */ (function () {
    function PriceComponent() {
        this.priceFilters = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.minValue = 100;
        this.maxValue = 1000;
        this.options = {
            floor: 100,
            ceil: 1000,
            translate: function (value, label) {
                switch (label) {
                    case ng5_slider__WEBPACK_IMPORTED_MODULE_1__["LabelType"].Low:
                        return '$' + value;
                    case ng5_slider__WEBPACK_IMPORTED_MODULE_1__["LabelType"].High:
                        return '$' + value;
                    default:
                        return '$' + value;
                }
            }
        };
        this.logText = '';
    }
    PriceComponent.prototype.onUserChangeStart = function (changeContext) {
        this.logText += "onUserChangeStart(" + this.getChangeContextString(changeContext) + ")\n";
    };
    PriceComponent.prototype.onUserChange = function (changeContext) {
        this.logText += "onUserChange(" + this.getChangeContextString(changeContext) + ")\n";
    };
    PriceComponent.prototype.onUserChangeEnd = function (changeContext) {
        this.logText += "onUserChangeEnd(" + this.getChangeContextString(changeContext) + ")\n";
    };
    PriceComponent.prototype.getChangeContextString = function (changeContext) {
        this.min = changeContext.value;
        this.max = changeContext.highValue;
        this.priceFilters.emit(changeContext);
        return "{pointerType: " + (changeContext.pointerType === ng5_slider__WEBPACK_IMPORTED_MODULE_1__["PointerType"].Min ? 'Min' : 'Max') + ", " +
            ("value: " + changeContext.value + ", ") +
            ("highValue: " + changeContext.highValue + "}");
    };
    PriceComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PriceComponent.prototype, "priceFilters", void 0);
    PriceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-price',
            template: __webpack_require__(/*! raw-loader!./price.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/colection/filter/price/price.component.html"),
            styles: [__webpack_require__(/*! ./price.component.scss */ "./src/app/components/e-commerce/colection/filter/price/price.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PriceComponent);
    return PriceComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/e-commerce-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/e-commerce/e-commerce-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: ECommerceRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECommerceRoutingModule", function() { return ECommerceRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _order_history_order_history_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order-history/order-history.component */ "./src/app/components/e-commerce/order-history/order-history.component.ts");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/components/e-commerce/product-list/product-list.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products/products.component */ "./src/app/components/e-commerce/products/products.component.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-detail/product-detail.component */ "./src/app/components/e-commerce/product-detail/product-detail.component.ts");
/* harmony import */ var _swimlane_ngx_charts_release_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-charts/release/utils */ "./node_modules/@swimlane/ngx-charts/release/utils/index.js");
/* harmony import */ var _quick_view_quick_view_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./quick-view/quick-view.component */ "./src/app/components/e-commerce/quick-view/quick-view.component.ts");
/* harmony import */ var _add_cart_add_cart_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-cart/add-cart.component */ "./src/app/components/e-commerce/add-cart/add-cart.component.ts");
/* harmony import */ var _wish_list_wish_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./wish-list/wish-list.component */ "./src/app/components/e-commerce/wish-list/wish-list.component.ts");
/* harmony import */ var _check_out_check_out_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./check-out/check-out.component */ "./src/app/components/e-commerce/check-out/check-out.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/components/e-commerce/invoice/invoice.component.ts");
/* harmony import */ var _payment_detail_payment_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./payment-detail/payment-detail.component */ "./src/app/components/e-commerce/payment-detail/payment-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var routes = [
    {
        path: '',
        children: [
            {
                path: 'products',
                component: _products_products_component__WEBPACK_IMPORTED_MODULE_4__["ProductsComponent"],
                pathMatch: 'full',
                data: {
                    title: "Product",
                    breadcrumb: "Product"
                }
            },
            {
                path: "product-details/:id",
                component: _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailComponent"],
                data: {
                    some_data: _swimlane_ngx_charts_release_utils__WEBPACK_IMPORTED_MODULE_6__["id"],
                    title: "Product Detail",
                    breadcrumb: "Product Detail"
                },
            },
            {
                path: "quick-view/:id",
                component: _quick_view_quick_view_component__WEBPACK_IMPORTED_MODULE_7__["QuickViewComponent"],
                data: {
                    some_data: _swimlane_ngx_charts_release_utils__WEBPACK_IMPORTED_MODULE_6__["id"],
                    title: "Quick View",
                    breadcrumb: "Quick View"
                },
            },
            {
                path: "add-cart",
                component: _add_cart_add_cart_component__WEBPACK_IMPORTED_MODULE_8__["AddCartComponent"],
                data: {
                    title: "Add To Cart",
                    breadcrumb: "Add To Cart"
                }
            },
            {
                path: "wish-list",
                component: _wish_list_wish_list_component__WEBPACK_IMPORTED_MODULE_9__["WishListComponent"],
                data: {
                    title: "Wish List",
                    breadcrumb: "Wish List"
                }
            },
            {
                path: "check-out",
                component: _check_out_check_out_component__WEBPACK_IMPORTED_MODULE_10__["CheckOutComponent"],
                data: {
                    title: "Check Out",
                    breadcrumb: "Check Out"
                }
            },
            {
                path: "invoice",
                component: _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__["InvoiceComponent"],
                data: {
                    title: "Invoice",
                    breadcrumb: "Invoice"
                }
            },
            {
                path: 'payment/detail',
                component: _payment_detail_payment_detail_component__WEBPACK_IMPORTED_MODULE_12__["PaymentDetailComponent"],
                data: {
                    title: "Payment Details",
                    breadcrumb: "Payment Details"
                }
            },
            {
                path: 'order',
                component: _order_history_order_history_component__WEBPACK_IMPORTED_MODULE_2__["OrderHistoryComponent"],
                data: {
                    title: "Order History",
                    breadcrumb: "Order History"
                }
            },
            {
                path: 'product/list',
                component: _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_3__["ProductListComponent"],
                data: {
                    title: "product list",
                    breadcrumb: "product list"
                }
            },
        ]
    }
];
var ECommerceRoutingModule = /** @class */ (function () {
    function ECommerceRoutingModule() {
    }
    ECommerceRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ECommerceRoutingModule);
    return ECommerceRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/e-commerce.module.ts":
/*!************************************************************!*\
  !*** ./src/app/components/e-commerce/e-commerce.module.ts ***!
  \************************************************************/
/*! exports provided: ECommerceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECommerceModule", function() { return ECommerceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _e_commerce_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./e-commerce-routing.module */ "./src/app/components/e-commerce/e-commerce-routing.module.ts");
/* harmony import */ var _order_history_order_history_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-history/order-history.component */ "./src/app/components/e-commerce/order-history/order-history.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/components/e-commerce/product-list/product-list.component.ts");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _quick_view_quick_view_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./quick-view/quick-view.component */ "./src/app/components/e-commerce/quick-view/quick-view.component.ts");
/* harmony import */ var _add_cart_add_cart_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-cart/add-cart.component */ "./src/app/components/e-commerce/add-cart/add-cart.component.ts");
/* harmony import */ var _wish_list_wish_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./wish-list/wish-list.component */ "./src/app/components/e-commerce/wish-list/wish-list.component.ts");
/* harmony import */ var _check_out_check_out_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./check-out/check-out.component */ "./src/app/components/e-commerce/check-out/check-out.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/components/e-commerce/invoice/invoice.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./products/products.component */ "./src/app/components/e-commerce/products/products.component.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./product-detail/product-detail.component */ "./src/app/components/e-commerce/product-detail/product-detail.component.ts");
/* harmony import */ var _colection_filter_price_price_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./colection/filter/price/price.component */ "./src/app/components/e-commerce/colection/filter/price/price.component.ts");
/* harmony import */ var _colection_filter_color_color_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./colection/filter/color/color.component */ "./src/app/components/e-commerce/colection/filter/color/color.component.ts");
/* harmony import */ var _colection_filter_brand_brand_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./colection/filter/brand/brand.component */ "./src/app/components/e-commerce/colection/filter/brand/brand.component.ts");
/* harmony import */ var _shared_services_e_commerce_order_by_pipe__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../shared/services/e-commerce/order-by.pipe */ "./src/app/shared/services/e-commerce/order-by.pipe.ts");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-owl-carousel-o */ "./node_modules/ngx-owl-carousel-o/fesm5/ngx-owl-carousel-o.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var ngx_print__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-print */ "./node_modules/ngx-print/fesm5/ngx-print.js");
/* harmony import */ var _payment_detail_payment_detail_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./payment-detail/payment-detail.component */ "./src/app/components/e-commerce/payment-detail/payment-detail.component.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! mousetrap */ "./node_modules/mousetrap/mousetrap.js");
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(mousetrap__WEBPACK_IMPORTED_MODULE_30__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};































var ECommerceModule = /** @class */ (function () {
    function ECommerceModule() {
    }
    ECommerceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _order_history_order_history_component__WEBPACK_IMPORTED_MODULE_3__["OrderHistoryComponent"],
                _quick_view_quick_view_component__WEBPACK_IMPORTED_MODULE_7__["QuickViewComponent"],
                _add_cart_add_cart_component__WEBPACK_IMPORTED_MODULE_8__["AddCartComponent"],
                _wish_list_wish_list_component__WEBPACK_IMPORTED_MODULE_9__["WishListComponent"],
                _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_5__["ProductListComponent"],
                _check_out_check_out_component__WEBPACK_IMPORTED_MODULE_10__["CheckOutComponent"],
                _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__["InvoiceComponent"],
                _products_products_component__WEBPACK_IMPORTED_MODULE_12__["ProductsComponent"],
                _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_13__["ProductDetailComponent"],
                _add_cart_add_cart_component__WEBPACK_IMPORTED_MODULE_8__["AddCartComponent"],
                _check_out_check_out_component__WEBPACK_IMPORTED_MODULE_10__["CheckOutComponent"],
                _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__["InvoiceComponent"],
                _colection_filter_price_price_component__WEBPACK_IMPORTED_MODULE_14__["PriceComponent"],
                _colection_filter_color_color_component__WEBPACK_IMPORTED_MODULE_15__["ColorComponent"],
                _colection_filter_brand_brand_component__WEBPACK_IMPORTED_MODULE_16__["BrandComponent"],
                _shared_services_e_commerce_order_by_pipe__WEBPACK_IMPORTED_MODULE_17__["OrderByPipe"],
                _wish_list_wish_list_component__WEBPACK_IMPORTED_MODULE_9__["WishListComponent"],
                _quick_view_quick_view_component__WEBPACK_IMPORTED_MODULE_7__["QuickViewComponent"],
                _payment_detail_payment_detail_component__WEBPACK_IMPORTED_MODULE_26__["PaymentDetailComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _e_commerce_routing_module__WEBPACK_IMPORTED_MODULE_2__["ECommerceRoutingModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_20__["CarouselModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["NgbModule"].forRoot(),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_21__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_22__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_22__["ReactiveFormsModule"],
                ngx_print__WEBPACK_IMPORTED_MODULE_25__["NgxPrintModule"],
                ng5_slider__WEBPACK_IMPORTED_MODULE_23__["Ng5SliderModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_24__["Ng2SearchPipeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_27__["Ng2SmartTableModule"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_28__["GalleryModule"].forRoot()
            ],
            providers: [_shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_18__["ProductsService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["NgbActiveModal"]],
            entryComponents: [_products_products_component__WEBPACK_IMPORTED_MODULE_12__["ProductsComponent"]],
        })
    ], ECommerceModule);
    return ECommerceModule;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/invoice/invoice.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/e-commerce/invoice/invoice.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9pbnZvaWNlL2ludm9pY2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/e-commerce/invoice/invoice.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/e-commerce/invoice/invoice.component.ts ***!
  \********************************************************************/
/*! exports provided: InvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceComponent", function() { return InvoiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_e_commerce_invoice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/e-commerce/invoice.service */ "./src/app/shared/services/e-commerce/invoice.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InvoiceComponent = /** @class */ (function () {
    function InvoiceComponent(invoiceService, elRef) {
        this.invoiceService = invoiceService;
        this.elRef = elRef;
        this.date = new Date();
        this.orderDetails = {};
    }
    InvoiceComponent.prototype.ngOnInit = function () {
        this.orderDetails = this.invoiceService.getOrderItems();
    };
    InvoiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice',
            template: __webpack_require__(/*! raw-loader!./invoice.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/invoice/invoice.component.html"),
            styles: [__webpack_require__(/*! ./invoice.component.scss */ "./src/app/components/e-commerce/invoice/invoice.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_e_commerce_invoice_service__WEBPACK_IMPORTED_MODULE_1__["InvoiceService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], InvoiceComponent);
    return InvoiceComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/order-history/order-history.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/e-commerce/order-history/order-history.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9vcmRlci1oaXN0b3J5L29yZGVyLWhpc3RvcnkuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/e-commerce/order-history/order-history.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/e-commerce/order-history/order-history.component.ts ***!
  \********************************************************************************/
/*! exports provided: OrderHistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistoryComponent", function() { return OrderHistoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OrderHistoryComponent = /** @class */ (function () {
    function OrderHistoryComponent() {
    }
    OrderHistoryComponent.prototype.ngOnInit = function () { };
    OrderHistoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-history',
            template: __webpack_require__(/*! raw-loader!./order-history.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/order-history/order-history.component.html"),
            styles: [__webpack_require__(/*! ./order-history.component.scss */ "./src/app/components/e-commerce/order-history/order-history.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], OrderHistoryComponent);
    return OrderHistoryComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/payment-detail/payment-detail.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/components/e-commerce/payment-detail/payment-detail.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9wYXltZW50LWRldGFpbC9wYXltZW50LWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/e-commerce/payment-detail/payment-detail.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/e-commerce/payment-detail/payment-detail.component.ts ***!
  \**********************************************************************************/
/*! exports provided: PaymentDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentDetailComponent", function() { return PaymentDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaymentDetailComponent = /** @class */ (function () {
    function PaymentDetailComponent() {
    }
    PaymentDetailComponent.prototype.ngOnInit = function () { };
    PaymentDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payment-detail',
            template: __webpack_require__(/*! raw-loader!./payment-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/payment-detail/payment-detail.component.html"),
            styles: [__webpack_require__(/*! ./payment-detail.component.scss */ "./src/app/components/e-commerce/payment-detail/payment-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PaymentDetailComponent);
    return PaymentDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/product-detail/product-detail.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/components/e-commerce/product-detail/product-detail.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9wcm9kdWN0LWRldGFpbC9wcm9kdWN0LWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/e-commerce/product-detail/product-detail.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/e-commerce/product-detail/product-detail.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ProductDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailComponent", function() { return ProductDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/services/e-commerce/cart.service */ "./src/app/shared/services/e-commerce/cart.service.ts");
/* harmony import */ var _shared_model_e_commerce_content__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/model/e-commerce/content */ "./src/app/shared/model/e-commerce/content.ts");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProductDetailComponent = /** @class */ (function () {
    function ProductDetailComponent(router, route, config, productService, cartService) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.productService = productService;
        this.cartService = cartService;
        this.product = {};
        this.products = [];
        this.detailCnt = [];
        this.slidesPerPage = 4;
        this.syncedSecondary = true;
        this.allContent = [];
        this.contents = [];
        this.active = false;
        this.type = "Febric";
        this.imagesRect = [
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_6__["Image"](0, { img: 'assets/images/ecommerce/01.jpg' }, { img: 'assets/images/ecommerce/01.jpg' }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_6__["Image"](1, { img: 'assets/images/ecommerce/04.jpg' }, { img: 'assets/images/ecommerce/04.jpg' }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_6__["Image"](2, { img: 'assets/images/ecommerce/03.jpg' }, { img: 'assets/images/ecommerce/03.jpg' }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_6__["Image"](3, { img: 'assets/images/ecommerce/02.jpg' }, { img: 'assets/images/ecommerce/02.jpg' })
        ];
        this.allContent = _shared_model_e_commerce_content__WEBPACK_IMPORTED_MODULE_5__["ContentDetail"].ContentDetails;
        //for rating 
        this.allContent.filter(function (opt) {
            if (_this.type == opt.type) {
                _this.contents.push(opt);
            }
        });
        config.max = 5;
        config.readonly = false;
        this.route.params.subscribe(function (params) {
            var id = +params['id'];
            _this.productService.getProduct(id).subscribe(function (product) {
                _this.product = product;
            });
        });
    }
    ProductDetailComponent.prototype.getOption = function (type) {
        var _this = this;
        this.contents = [];
        return this.allContent.filter(function (data) {
            if (type == data.type) {
                _this.active = true;
                return _this.contents.push(data);
            }
            else {
                return false;
            }
        });
    };
    ProductDetailComponent.prototype.buyNow = function (product, quantity) {
        if (quantity === void 0) { quantity = 1; }
        if (quantity > 0)
            this.cartService.addToCart(product, quantity);
        this.router.navigate(['/ecommerce/check-out']);
    };
    ProductDetailComponent.prototype.addToCart = function (product, quantity) {
        if (quantity === void 0) { quantity = 1; }
        if (quantity == 0)
            return false;
        this.cartService.addToCart(product, quantity);
    };
    ProductDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productService.getProducts().subscribe(function (product) {
            _this.products = product;
            product.filter(function (ele) {
                _this.nav = ele.img;
            });
        });
    };
    ProductDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! raw-loader!./product-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/product-detail/product-detail.component.html"),
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbRatingConfig"]],
            styles: [__webpack_require__(/*! ./product-detail.component.scss */ "./src/app/components/e-commerce/product-detail/product-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbRatingConfig"], _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_3__["ProductsService"], _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_4__["CartService"]])
    ], ProductDetailComponent);
    return ProductDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/product-list/product-list.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/components/e-commerce/product-list/product-list.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9wcm9kdWN0LWxpc3QvcHJvZHVjdC1saXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/product-list/product-list.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/components/e-commerce/product-list/product-list.component.ts ***!
  \******************************************************************************/
/*! exports provided: ProductListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListComponent", function() { return ProductListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_product_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/tables/product-list */ "./src/app/shared/data/tables/product-list.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductListComponent = /** @class */ (function () {
    function ProductListComponent() {
        this.products = [];
        this.settings = {
            columns: {
                img: {
                    title: 'Image',
                    type: 'html',
                },
                product_name: {
                    title: 'Product_name'
                },
                product_desc: {
                    title: 'Product_desc'
                },
                amount: {
                    title: 'Amount'
                },
                stock: {
                    title: 'Stock',
                    type: 'html',
                },
                start_date: {
                    title: 'Start_date'
                }
            },
        };
        this.products = _shared_data_tables_product_list__WEBPACK_IMPORTED_MODULE_1__["productDB"].product;
    }
    ProductListComponent.prototype.ngOnInit = function () { };
    ProductListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-list',
            template: __webpack_require__(/*! raw-loader!./product-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/product-list/product-list.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./product-list.component.scss */ "./src/app/components/e-commerce/product-list/product-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProductListComponent);
    return ProductListComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/products/products.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/e-commerce/products/products.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9wcm9kdWN0cy9wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/e-commerce/products/products.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/e-commerce/products/products.component.ts ***!
  \**********************************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/e-commerce/cart.service */ "./src/app/shared/services/e-commerce/cart.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _shared_services_e_commerce_wish_list_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/services/e-commerce/wish-list.service */ "./src/app/shared/services/e-commerce/wish-list.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(productService, cartService, modalService, wishService) {
        this.productService = productService;
        this.cartService = cartService;
        this.modalService = modalService;
        this.wishService = wishService;
        this.items = [];
        this.products = [];
        this.colorsFilters = [];
        this.allItems = [];
        this.tags = [];
        this.tagsFilters = [];
        this.sortByOrder = '';
        this.check = false;
        //image set
        this.detailCnt = [];
        this.slidesPerPage = 1;
        this.customOptions = {
            slider: 1,
            items: 1,
            margin: 30,
            loop: false,
            pagination: false,
            nav: true,
            dots: false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        };
        this.sidebaron = false;
        this.show = false;
        this.open = false;
        this.listView = false;
        this.col_xl_12 = false;
        this.col_xl_2 = false;
        this.col_sm_3 = false;
        this.col_xl_3 = true;
        this.xl_4 = true;
        this.col_sm_4 = false;
        this.col_xl_4 = false;
        this.col_sm_6 = true;
        this.col_xl_6 = false;
        this.gridOptions = true;
        this.active = false;
    }
    ProductsComponent.prototype.onChangeSorting = function (val) {
        this.sortByOrder = val;
    };
    ProductsComponent.prototype.filterItems = function () {
        var _this = this;
        return this.products.filter(function (item) {
            var Colors = _this.colorsFilters.reduce(function (prev, curr) {
                if (item.colors) {
                    if (item.colors.includes(curr.color)) {
                        _this.active = true;
                        _this.check = true;
                        return prev && true;
                    }
                }
            }, true);
            var Tags = _this.tagsFilters.reduce(function (prev, curr) {
                if (item.tags) {
                    if (item.tags.includes(curr)) {
                        return prev && true;
                    }
                }
            }, true);
            return Colors && Tags; // return true
        });
    };
    ProductsComponent.prototype.getColors = function (products) {
        var uniqueColors = [];
        var itemColor = Array();
        products.map(function (product, index) {
            if (product.colors) {
                product.colors.map(function (color) {
                    var index = uniqueColors.indexOf(color);
                    if (index === -1)
                        uniqueColors.push(color);
                });
            }
        });
        for (var i = 0; i < uniqueColors.length; i++) {
            itemColor.push({ color: uniqueColors[i] });
        }
        this.colors = itemColor;
    };
    ProductsComponent.prototype.updateColor = function (colors) {
        this.colorsFilters = colors;
    };
    ProductsComponent.prototype.getTags = function (products) {
        var uniqueBrands = [];
        var itemBrand = Array();
        products.map(function (product, index) {
            if (product.tags) {
                product.tags.map(function (tag) {
                    var index = uniqueBrands.indexOf(tag);
                    if (index === -1)
                        uniqueBrands.push(tag);
                });
            }
        });
        for (var i = 0; i < uniqueBrands.length; i++) {
            itemBrand.push({ brand: uniqueBrands[i] });
        }
        this.tags = itemBrand;
    };
    ProductsComponent.prototype.updateTagFilters = function (tags) {
        this.tagsFilters = tags;
    };
    ProductsComponent.prototype.updatePriceFilters = function (price) {
        var _this = this;
        var pricemin = price.value;
        var maxPrice = price.highValue;
        var items = [];
        this.productService.getProducts().subscribe(function (product) {
            product.filter(function (item) {
                if (item.price >= pricemin && item.price <= maxPrice) {
                    items.push(item); // push in array
                }
            });
            _this.products = items;
        });
    };
    ProductsComponent.prototype.openFilter = function () {
        if (this.show == true && this.sidebaron == true) {
            this.show = false;
            this.sidebaron = false;
        }
        else {
            this.show = true;
            this.sidebaron = true;
        }
    };
    ProductsComponent.prototype.openMediaFilter = function () {
        if (this.show == false && this.sidebaron == false && this.open == false) {
            this.show = true;
            this.sidebaron = true;
            this.open = true;
        }
        else {
            this.show = false;
            this.sidebaron = false;
            this.open = false;
        }
    };
    ProductsComponent.prototype.gridOpen = function () {
        this.gridOptions = true;
        this.listView = false;
        this.col_xl_3 = true;
        this.xl_4 = true;
        this.col_xl_4 = false;
        this.col_sm_4 = false;
        this.col_xl_6 = false;
        this.col_sm_6 = true;
        this.col_xl_2 = false;
        this.col_xl_12 = false;
    };
    ProductsComponent.prototype.listOpen = function () {
        this.gridOptions = false;
        this.listView = true;
        this.col_xl_3 = true;
        this.xl_4 = true;
        this.col_xl_12 = true;
        this.col_xl_2 = false;
        this.col_xl_4 = false;
        this.col_sm_4 = false;
        this.col_xl_6 = false;
        this.col_sm_6 = true;
    };
    ProductsComponent.prototype.grid2 = function () {
        this.listView = false;
        this.col_xl_3 = false;
        this.col_sm_3 = false;
        this.col_xl_2 = false;
        this.col_xl_4 = false;
        this.col_sm_4 = false;
        this.col_xl_6 = true;
        this.col_sm_6 = true;
        this.col_xl_12 = false;
    };
    ProductsComponent.prototype.grid3 = function () {
        this.listView = false;
        this.col_xl_3 = false;
        this.col_sm_3 = false;
        this.col_xl_2 = false;
        this.col_xl_4 = true;
        this.col_sm_4 = true;
        this.col_xl_6 = false;
        this.col_sm_6 = false;
        this.col_xl_12 = false;
    };
    ProductsComponent.prototype.grid6 = function () {
        this.listView = false;
        this.col_xl_3 = false;
        this.col_sm_3 = false;
        this.col_xl_2 = true;
        this.col_xl_4 = false;
        this.col_sm_4 = false;
        this.col_xl_6 = false;
        this.col_sm_6 = false;
        this.col_xl_12 = false;
    };
    // add to cart service
    ProductsComponent.prototype.addToCart = function (product, quantity) {
        if (quantity === void 0) { quantity = 1; }
        this.cartService.addToCart(product, quantity);
    };
    //add to wish list service
    ProductsComponent.prototype.addToWishlist = function (product, quantity) {
        if (quantity === void 0) { quantity = 1; }
        this.wishService.addToWishList(product, quantity);
    };
    // open single product detail
    ProductsComponent.prototype.openProductDetail = function (content, id) {
        var _this = this;
        this.modalService.open(content, { centered: true, size: 'lg' });
        this.productService.getProduct(id).subscribe(function (product) {
            _this.productDetail = product;
        });
    };
    //decrement product quentity
    ProductsComponent.prototype.decrement = function (product, quantity) {
        if (quantity === void 0) { quantity = -1; }
        this.cartService.updateCartQuantity(product, quantity);
    };
    //increment product quentity
    ProductsComponent.prototype.increment = function (product, quantity) {
        if (quantity === void 0) { quantity = +1; }
        this.cartService.updateCartQuantity(product, quantity);
    };
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productService.getProducts().subscribe(function (product) {
            _this.products = product;
            _this.getColors(product);
            _this.getTags(product);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductsComponent.prototype, "productDetail", void 0);
    ProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! raw-loader!./products.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/products/products.component.html"),
            styles: [__webpack_require__(/*! ./products.component.scss */ "./src/app/components/e-commerce/products/products.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsService"], _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], _shared_services_e_commerce_wish_list_service__WEBPACK_IMPORTED_MODULE_4__["WishListService"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/quick-view/quick-view.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/components/e-commerce/quick-view/quick-view.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS9xdWljay12aWV3L3F1aWNrLXZpZXcuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/e-commerce/quick-view/quick-view.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/components/e-commerce/quick-view/quick-view.component.ts ***!
  \**************************************************************************/
/*! exports provided: QuickViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuickViewComponent", function() { return QuickViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/services/e-commerce/products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/e-commerce/cart.service */ "./src/app/shared/services/e-commerce/cart.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var QuickViewComponent = /** @class */ (function () {
    function QuickViewComponent(router, route, config, productService, cartService, ngb) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.productService = productService;
        this.cartService = cartService;
        this.ngb = ngb;
        this.cartItems = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])([]);
        this.selectCartItems = [];
        this.counter = 1;
        this.product = {};
        this.detailCnt = [];
        this.slidesPerPage = 4;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.ngb.dismissAll();
            }
        });
        this.route.params.subscribe(function (params) {
            var id = +params['id'];
            _this.productService.getProduct(id).subscribe(function (product) {
                _this.product = product;
            });
        });
    }
    QuickViewComponent.prototype.increment = function () {
        this.counter += 1;
    };
    QuickViewComponent.prototype.decrement = function () {
        if (this.counter > 1) {
            this.counter -= 1;
        }
    };
    QuickViewComponent.prototype.addToCart = function (product, quantity) {
        if (quantity == 0)
            return false;
        this.cartService.addToCart(product, parseInt(quantity));
    };
    QuickViewComponent.prototype.buyNow = function (product, quantity) {
        if (quantity > 0)
            this.cartService.addToCart(product, parseInt(quantity));
        this.router.navigate(['/ecommerce/check-out']);
    };
    QuickViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cartItems = this.cartService.getAll();
        this.cartItems.subscribe(function (selectCartItems) { return _this.selectCartItems = selectCartItems; });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], QuickViewComponent.prototype, "productDetail", void 0);
    QuickViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-quick-view',
            template: __webpack_require__(/*! raw-loader!./quick-view.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/quick-view/quick-view.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./quick-view.component.scss */ "./src/app/components/e-commerce/quick-view/quick-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbRatingConfig"], _shared_services_e_commerce_products_service__WEBPACK_IMPORTED_MODULE_4__["ProductsService"], _shared_services_e_commerce_cart_service__WEBPACK_IMPORTED_MODULE_5__["CartService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], QuickViewComponent);
    return QuickViewComponent;
}());



/***/ }),

/***/ "./src/app/components/e-commerce/wish-list/wish-list.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/components/e-commerce/wish-list/wish-list.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZS1jb21tZXJjZS93aXNoLWxpc3Qvd2lzaC1saXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/e-commerce/wish-list/wish-list.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/e-commerce/wish-list/wish-list.component.ts ***!
  \************************************************************************/
/*! exports provided: WishListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishListComponent", function() { return WishListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_e_commerce_wish_list_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/services/e-commerce/wish-list.service */ "./src/app/shared/services/e-commerce/wish-list.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WishListComponent = /** @class */ (function () {
    function WishListComponent(route, wishService) {
        this.route = route;
        this.wishService = wishService;
        this.cartItems = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])([]);
        this.selectCartItems = [];
    }
    WishListComponent.prototype.remove = function (item) {
        this.wishService.removeWishItem(item);
    };
    WishListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cartItems = this.wishService.getAll();
        this.cartItems.subscribe(function (selectCartItems) { return _this.selectCartItems = selectCartItems; });
    };
    WishListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wish-list',
            template: __webpack_require__(/*! raw-loader!./wish-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/e-commerce/wish-list/wish-list.component.html"),
            styles: [__webpack_require__(/*! ./wish-list.component.scss */ "./src/app/components/e-commerce/wish-list/wish-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _shared_services_e_commerce_wish_list_service__WEBPACK_IMPORTED_MODULE_3__["WishListService"]])
    ], WishListComponent);
    return WishListComponent;
}());



/***/ }),

/***/ "./src/app/shared/data/tables/product-list.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/data/tables/product-list.ts ***!
  \****************************************************/
/*! exports provided: productDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productDB", function() { return productDB; });
var productDB = /** @class */ (function () {
    function productDB() {
    }
    productDB.product = [
        {
            img: "<img src='assets/images/ecommerce/product-table-1.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$10",
            stock: "<div class='font-success'>In Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-2.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$10",
            stock: "<div class='font-danger'>Out of Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-3.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$10",
            stock: "<div class='font-danger'>Out of Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-4.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$20",
            stock: "<div class='font-primary'>Low Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-5.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$30",
            stock: "<div class='font-success'>In Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-6.png'>",
            product_name: "Brown Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$40",
            stock: "<div class='font-success'>In Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-1.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$10",
            stock: "<div class='font-success'>In Stock</div>",
            start_date: "2011/4/19"
        },
        {
            img: "<img src='assets/images/ecommerce/product-table-2.png'>",
            product_name: "Red Lipstick",
            product_desc: "Interchargebla lens Digital Camera with APS-C-X Trans CMOS Sens",
            amount: "$10",
            stock: "<div class='font-success'>In Stock</div>",
            start_date: "2011/4/19"
        }
    ];
    return productDB;
}());



/***/ }),

/***/ "./src/app/shared/model/e-commerce/content.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/model/e-commerce/content.ts ***!
  \****************************************************/
/*! exports provided: ContentDetail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentDetail", function() { return ContentDetail; });
var ContentDetail = /** @class */ (function () {
    function ContentDetail() {
    }
    ContentDetail.ContentDetails = [
        {
            type: "Febric",
            content_1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            content_2: "Lorem ipsum dolor sit amet, incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            active: "febric"
        },
        {
            type: "Video",
            content_1: "Lorem ipsum dolor sit amet,  anim id est laborum.",
            content_2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            active: "video"
        },
        {
            type: "Details",
            content_1: "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            content_2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            active: "details"
        },
        {
            type: "Brand",
            content_1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            content_2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            active: "brand"
        }
    ];
    return ContentDetail;
}());



/***/ }),

/***/ "./src/app/shared/services/e-commerce/cart.service.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/services/e-commerce/cart.service.ts ***!
  \************************************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var products = JSON.parse(localStorage.getItem("cartItem")) || [];
var CartService = /** @class */ (function () {
    function CartService(route, productService, toastrService) {
        this.route = route;
        this.productService = productService;
        this.toastrService = toastrService;
        this.cartItems = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]([]);
        this.itemsInCart = [];
        this.cartItems.subscribe(function (products) { return products = products; });
        this.itemList = [];
    }
    CartService.prototype.getAll = function () {
        var itemsList = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
            observer.next(products);
            observer.complete();
        });
        return itemsList;
    };
    CartService.prototype.addToCart = function (product, quantity) {
        var _this = this;
        var item = false;
        var hashItem = products.find(function (items, index) {
            if (items.product.id == product.id) {
                var qty = products[index].quantity + quantity;
                var stock = _this.calculateStockCounts(products[index], quantity);
                if (qty != 0 && stock) {
                    products[index]['quantity'] = qty;
                    _this.toastrService.success('This product has been already added to cart.');
                    localStorage.setItem('cartItem', JSON.stringify(products));
                }
                return true;
            }
        });
        if (!hashItem) {
            item = { product: product, quantity: quantity };
            products.push(item);
            this.toastrService.success('This product has been added to cart.');
        }
        localStorage.setItem('cartItem', JSON.stringify(products));
        return item;
    };
    CartService.prototype.calculateStockCounts = function (product, quantity) {
        var qty = product.quantity + quantity;
        var stock = product.product.stock;
        if (stock < qty) {
            this.toastrService.error('You can not add more items than available. In stock ' + stock + ' items.');
            return false;
        }
        return true;
    };
    CartService.prototype.removeCartItem = function (item) {
        if (item === undefined)
            return false;
        var index = products.indexOf(item);
        products.splice(index, 1);
        localStorage.setItem('cartItem', JSON.stringify(products));
    };
    CartService.prototype.updateCartQuantity = function (product, quantity) {
        var _this = this;
        return products.find(function (items, index) {
            if (items.product.id == product.id) {
                var qty = products[index].quantity + quantity;
                var stock = _this.calculateStockCounts(products[index], quantity);
                if (qty != 0 && stock)
                    products[index]['quantity'] = qty;
                localStorage.setItem("cartItem", JSON.stringify(products));
                return true;
            }
        });
    };
    CartService.prototype.getTotalAmount = function () {
        return this.cartItems.map(function (product) {
            return products.reduce(function (prev, curr) {
                return prev + curr.product.price * curr.quantity;
            }, 0);
        });
    };
    CartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/shared/services/e-commerce/invoice.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/services/e-commerce/invoice.service.ts ***!
  \***************************************************************/
/*! exports provided: InvoiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceService", function() { return InvoiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InvoiceService = /** @class */ (function () {
    function InvoiceService(router) {
        this.router = router;
        this.invoiceDetails = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this.invoice = [];
    }
    InvoiceService.prototype.getOrderItems = function () {
        return this.OrderDetails;
    };
    InvoiceService.prototype.createOrder = function (product, details, amount) {
        var item = {
            shippingDetails: details,
            product: product,
            totalAmount: amount
        };
        this.OrderDetails = item;
        this.router.navigate(['/ecommerce/invoice']);
    };
    InvoiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], InvoiceService);
    return InvoiceService;
}());



/***/ }),

/***/ "./src/app/shared/services/e-commerce/order-by.pipe.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/services/e-commerce/order-by.pipe.ts ***!
  \*************************************************************/
/*! exports provided: OrderByPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderByPipe", function() { return OrderByPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (array, val) {
        var _this = this;
        if (val === void 0) { val = 'desc'; }
        if (!val || val.trim() == "") {
            return array;
        }
        //ascending
        if (val == 'asc') {
            return Array.from(array).sort(function (item1, item2) {
                return _this.orderByComparator(item1['id'], item2['id']);
            });
        }
        else if (val == 'desc') { // desc
            return Array.from(array).sort(function (item1, item2) {
                return _this.orderByComparator(item2['id'], item1['id']);
            });
        }
        else if (val == 'a-z') { // a-z
            return Array.from(array).sort(function (a, b) {
                if (a['name'] < b['name']) {
                    return -1;
                }
                else if (a['name'] > b['name']) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (val == 'z-a') { // z-a
            return Array.from(array).sort(function (a, b) {
                if (a['name'] > b['name']) {
                    return -1;
                }
                else if (a['name'] < b['name']) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (val == 'low') { // low to high
            return Array.from(array).sort(function (a, b) {
                if (a['price'] < b['price']) {
                    return -1;
                }
                else if (a['price'] > b['price']) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (val == 'high') { // high to low
            return Array.from(array).sort(function (a, b) {
                if (a['price'] > b['price']) {
                    return -1;
                }
                else if (a['price'] < b['price']) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
    };
    OrderByPipe.prototype.orderByComparator = function (a, b) {
        if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
            //Isn't a number so lowercase the string to properly compare
            if (a.toLowerCase() < b.toLowerCase())
                return -1;
            if (a.toLowerCase() > b.toLowerCase())
                return 1;
        }
        else {
            //Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b))
                return -1;
            if (parseFloat(a) > parseFloat(b))
                return 1;
        }
        return 0; //equal each other
    };
    OrderByPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'orderBy'
        })
    ], OrderByPipe);
    return OrderByPipe;
}());



/***/ }),

/***/ "./src/app/shared/services/e-commerce/products.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/services/e-commerce/products.service.ts ***!
  \****************************************************************/
/*! exports provided: ProductsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsService", function() { return ProductsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductsService = /** @class */ (function () {
    function ProductsService(http) {
        this.http = http;
        this.currency = 'USD';
        this._http = null;
        this._http = http;
    }
    ProductsService.prototype.products = function () {
        return this.http.get('assets/data/ecommerce/products.json').map(function (res) {
            return res;
        });
    };
    ProductsService.prototype.getProducts = function () {
        return this.products();
    };
    ProductsService.prototype.getProduct = function (id) {
        return this.products().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (items) {
            return items.find(function (item) {
                return item.id === id;
            });
        }));
    };
    ProductsService.prototype.getProductByColor = function (color) {
        return this.products().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (items) {
            return items.filter(function (item) {
                if (color == item.colors) {
                    return item.colors;
                }
                else {
                    return item;
                }
            });
        }));
    };
    ProductsService.prototype.checkDuplicateInObject = function (tag, Products) {
        var seenDuplicate = false, testObject = {};
        Products.map(function (item) {
            var itemPropertyName = item[tag];
            if (itemPropertyName in testObject) {
                testObject[itemPropertyName].duplicate = true;
                item.duplicate = true;
                seenDuplicate = true;
            }
            else {
                testObject[itemPropertyName] = item;
                delete item.duplicate;
            }
        });
        return seenDuplicate;
    };
    ProductsService.prototype.getProductByCategory = function (category) {
        return this.products().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (items) {
            return items.filter(function (item) {
                if (category == 'all') {
                    return item;
                }
                else {
                    return item.category === category;
                }
            });
        }));
    };
    ProductsService.prototype.tag = function () {
        return this.http.get('assets/data/products.json').map(function (res) {
            return res;
        });
    };
    ProductsService.prototype.getTags = function () {
        return this.products();
    };
    ProductsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProductsService);
    return ProductsService;
}());



/***/ }),

/***/ "./src/app/shared/services/e-commerce/wish-list.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/shared/services/e-commerce/wish-list.service.ts ***!
  \*****************************************************************/
/*! exports provided: WishListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishListService", function() { return WishListService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _products_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products.service */ "./src/app/shared/services/e-commerce/products.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var products = JSON.parse(localStorage.getItem("cartItem")) || [];
var WishListService = /** @class */ (function () {
    function WishListService(route, productService, toastrService) {
        this.route = route;
        this.productService = productService;
        this.toastrService = toastrService;
        this.wishItems = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this.itemsInCart = [];
        this.wishItems.subscribe(function (products) { return products = products; });
        this.itemList = [];
    }
    WishListService.prototype.getAll = function () {
        var itemsList = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
            observer.next(products);
            observer.complete();
        });
        return itemsList;
    };
    WishListService.prototype.addToWishList = function (product, quantity) {
        var _this = this;
        var item = false;
        var hashItem = products.find(function (items, index) {
            if (items.product.id == product.id) {
                var qty = products[index].quantity + quantity;
                var stock = _this.calculateStockCounts(products[index], quantity);
                if (qty != 0 && stock) {
                    products[index]['quantity'] = qty;
                    _this.toastrService.success('This product has been already added to cart.');
                    localStorage.setItem('cartItem', JSON.stringify(products));
                }
                return true;
            }
        });
        if (!hashItem) {
            item = { product: product, quantity: quantity };
            products.push(item);
            this.toastrService.success('This product has been added to cart.');
        }
        localStorage.setItem('cartItem', JSON.stringify(products));
        return item;
    };
    WishListService.prototype.calculateStockCounts = function (product, quantity) {
        var qty = product.quantity + quantity;
        var stock = product.product.stock;
        if (stock < qty) {
            this.toastrService.error('You can not add more items than available. In stock ' + stock + ' items.');
            return false;
        }
        return true;
    };
    WishListService.prototype.removeWishItem = function (item) {
        if (item === undefined)
            return false;
        var index = products.indexOf(item);
        products.splice(index, 1);
        localStorage.setItem('cartItem', JSON.stringify(products));
    };
    WishListService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _products_service__WEBPACK_IMPORTED_MODULE_3__["ProductsService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], WishListService);
    return WishListService;
}());



/***/ })

}]);
//# sourceMappingURL=components-e-commerce-e-commerce-module.js.map