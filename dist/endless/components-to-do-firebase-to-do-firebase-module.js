(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-to-do-firebase-to-do-firebase-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/to-do-firebase/to-do-firebase.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/to-do-firebase/to-do-firebase.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>To-Do</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"todo todo-database\">\r\n            <div class=\"todo-list-wrapper\">\r\n              <div class=\"todo-list-container\">\r\n                <div class=\"mark-all-tasks mark-all-firebase\">\r\n                  <div class=\"mark-all-tasks-container d-inline-block\">\r\n                    <span class=\"mark-all-btn\" [ngClass]=\"{'move-up':completed}\" id=\"mark-all-finished\" role=\"button\">\r\n                      <span class=\"btn-label\">Mark all as finished</span>\r\n                      <span class=\"action-box completed\" (click)=\"markAllAction(true);\">\r\n                        <i class=\"icon\"><i class=\"icon-check\"></i></i>\r\n                      </span>\r\n                    </span>\r\n                    <span class=\"mark-all-btn\" [ngClass]=\"{'move-down':!completed}\" id=\"mark-all-incomplete\"\r\n                      role=\"button\">\r\n                      <span class=\"btn-label\">Mark all as completed</span>\r\n                      <span class=\"action-box\" (click)=\"markAllAction(false);\">\r\n                        <i class=\"icon\"><i class=\"icon-check\"></i></i>\r\n                      </span>\r\n                    </span>\r\n                  </div>\r\n                </div>\r\n                <div class=\"todo-list-body custom-scrollbar\">\r\n                  <ul id=\"todo-list\">\r\n                    <li class=\"task\" [ngClass]=\"{'completed':todo.payload.doc.data().completed}\"\r\n                      *ngFor=\"let todo of items\">\r\n                      <div class=\"task-container\">\r\n                        <h4 class=\"task-label\">{{todo.payload.doc.data().task}}</h4>\r\n                        <span class=\"task-action-btn\">\r\n                          <span class=\"action-box large delete-btn\" title=\"Delete Task\"\r\n                            (click)=\"taskDeleted(todo.payload.doc.id)\"><i class=\"icon\"><i\r\n                                class=\"icon-trash\"></i></i></span>\r\n                          <span class=\"action-box large complete-btn\" title=\"Mark Complete\"\r\n                            (click)=\"taskCompleted(todo)\"><i class=\"icon\"><i class=\"icon-check\"></i></i></span>\r\n                        </span>\r\n                      </div>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n                <div class=\"todo-list-footer\">\r\n                  <div class=\"add-task-btn-wrapper\">\r\n                    <span class=\"add-task-btn\" [ngClass]=\"{'hide':visible}\">\r\n                      <button class=\"btn btn-primary\" (click)=\"visible=true\">\r\n                        <i class=\"icon-plus mr-1\"></i>Add new task\r\n                      </button>\r\n                    </span>\r\n                  </div>\r\n                  <div class=\"new-task-wrapper\" [ngClass]=\"{'visible':visible}\">\r\n                    <textarea id=\"new-task\" placeholder=\"Enter new task here. . .\" [(ngModel)]=\"text\"\r\n                      [class.border-danger]=\"redBorder\"></textarea>\r\n                    <button class=\"btn btn-danger cancel-btn\" id=\"close-task-panel\"\r\n                      (click)=\"visible=false\">Close</button>\r\n                    <button class=\"btn btn-success ml-3 add-new-task-btn\" id=\"add-task\" (click)=\"addTask(text)\">Add\r\n                      Task</button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"notification-popup hide\">\r\n              <p><span class=\"task\"></span><span class=\"notification-text\"></span></p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->\r\n"

/***/ }),

/***/ "./src/app/components/to-do-firebase/to-do-firebase-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/to-do-firebase/to-do-firebase-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ToDoFirebaseRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoFirebaseRoutingModule", function() { return ToDoFirebaseRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _to_do_firebase_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./to-do-firebase.component */ "./src/app/components/to-do-firebase/to-do-firebase.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _to_do_firebase_component__WEBPACK_IMPORTED_MODULE_2__["ToDoFirebaseComponent"],
                data: {
                    title: "To-Do-Firebase",
                    breadcrumb: ""
                }
            }
        ]
    }
];
var ToDoFirebaseRoutingModule = /** @class */ (function () {
    function ToDoFirebaseRoutingModule() {
    }
    ToDoFirebaseRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ToDoFirebaseRoutingModule);
    return ToDoFirebaseRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/to-do-firebase/to-do-firebase.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/to-do-firebase/to-do-firebase.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdG8tZG8tZmlyZWJhc2UvdG8tZG8tZmlyZWJhc2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/to-do-firebase/to-do-firebase.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/to-do-firebase/to-do-firebase.component.ts ***!
  \***********************************************************************/
/*! exports provided: ToDoFirebaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoFirebaseComponent", function() { return ToDoFirebaseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_todo_todo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/data/todo/todo */ "./src/app/shared/data/todo/todo.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shared_services_firebase_todo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/firebase/todo.service */ "./src/app/shared/services/firebase/todo.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ToDoFirebaseComponent = /** @class */ (function () {
    function ToDoFirebaseComponent(toastrService, router, firebaseService) {
        this.toastrService = toastrService;
        this.router = router;
        this.firebaseService = firebaseService;
        this.todos = _shared_data_todo_todo__WEBPACK_IMPORTED_MODULE_1__["task"];
        this.redBorder = false;
    }
    //Add new task
    ToDoFirebaseComponent.prototype.addTask = function (text) {
        var _this = this;
        if (!text) {
            this.redBorder = true;
            return false;
        }
        this.firebaseService.createTask(text)
            .then(function (res) {
            _this.resetFields();
            _this.router.navigate(['/to-do-firebase']);
        });
        this.redBorder = false;
    };
    ToDoFirebaseComponent.prototype.resetFields = function () {
        this.text = '';
    };
    //For completing your task
    ToDoFirebaseComponent.prototype.taskCompleted = function (todo) {
        var _this = this;
        todo.task = todo.payload.doc.data().task;
        todo.completed = todo.payload.doc.data().completed;
        todo.completed = !todo.completed;
        todo.nameToSearch = todo.task.toLowerCase();
        todo.completed ? this.toastrService.success(todo.text, "Mark as completed") : this.toastrService.error(todo.text, "Mark as Incompleted");
        this.firebaseService.updateTask(todo.payload.doc.id, todo)
            .then(function (res) {
            _this.router.navigate(['/to-do-firebase']);
        });
    };
    //For deleting a task
    ToDoFirebaseComponent.prototype.taskDeleted = function (taskid) {
        var _this = this;
        this.firebaseService.deleteTask(taskid)
            .then(function (res) {
            _this.router.navigate(['/to-do-firebase']);
        }, function (err) {
        });
    };
    //Mark and unmark all task
    ToDoFirebaseComponent.prototype.markAllAction = function (action) {
        this.firebaseService.markAll(action);
        this.completed = action;
        action ? this.toastrService.success("All Task as Completed") : this.toastrService.error("All Task as Incompleted");
    };
    ToDoFirebaseComponent.prototype.ngOnInit = function () {
        var _this = this;
        //Get complete list of task
        this.firebaseService.getTasks()
            .subscribe(function (result) {
            _this.items = result;
        });
    };
    ToDoFirebaseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-to-do-firebase',
            template: __webpack_require__(/*! raw-loader!./to-do-firebase.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/to-do-firebase/to-do-firebase.component.html"),
            styles: [__webpack_require__(/*! ./to-do-firebase.component.scss */ "./src/app/components/to-do-firebase/to-do-firebase.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _shared_services_firebase_todo_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"]])
    ], ToDoFirebaseComponent);
    return ToDoFirebaseComponent;
}());



/***/ }),

/***/ "./src/app/components/to-do-firebase/to-do-firebase.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/to-do-firebase/to-do-firebase.module.ts ***!
  \********************************************************************/
/*! exports provided: ToDoFirebaseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoFirebaseModule", function() { return ToDoFirebaseModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _to_do_firebase_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./to-do-firebase-routing.module */ "./src/app/components/to-do-firebase/to-do-firebase-routing.module.ts");
/* harmony import */ var _to_do_firebase_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./to-do-firebase.component */ "./src/app/components/to-do-firebase/to-do-firebase.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ToDoFirebaseModule = /** @class */ (function () {
    function ToDoFirebaseModule() {
    }
    ToDoFirebaseModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_to_do_firebase_component__WEBPACK_IMPORTED_MODULE_5__["ToDoFirebaseComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _to_do_firebase_routing_module__WEBPACK_IMPORTED_MODULE_4__["ToDoFirebaseRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"]
            ]
        })
    ], ToDoFirebaseModule);
    return ToDoFirebaseModule;
}());



/***/ }),

/***/ "./src/app/shared/services/firebase/todo.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/services/firebase/todo.service.ts ***!
  \**********************************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FirebaseService = /** @class */ (function () {
    function FirebaseService(db) {
        this.db = db;
    }
    //For Creating a new task
    FirebaseService.prototype.createTask = function (value) {
        return this.db.collection('todo').add({
            task: value,
            completed: false,
            nameToSearch: value.toLowerCase()
        });
    };
    //Display complete list of task
    FirebaseService.prototype.getTasks = function () {
        return this.db.collection('todo').snapshotChanges();
    };
    //For deleting particular task
    FirebaseService.prototype.deleteTask = function (taskKey) {
        return this.db.collection('todo').doc(taskKey).delete();
    };
    //For updating particular task
    FirebaseService.prototype.updateTask = function (taskKey, value) {
        return this.db.collection('todo').doc(taskKey).set({
            task: value.task,
            completed: value.completed,
            nameToSearch: value.nameToSearch
        });
    };
    FirebaseService.prototype.markAll = function (action) {
        var _this = this;
        this.db.collection('todo').get().forEach(function (item) {
            return item.docs.map(function (m) {
                return _this.db.doc("todo/" + m.id).update({ completed: action });
            });
        });
    };
    FirebaseService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ })

}]);
//# sourceMappingURL=components-to-do-firebase-to-do-firebase-module.js.map