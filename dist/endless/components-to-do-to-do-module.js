(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-to-do-to-do-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/to-do/to-do.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/to-do/to-do.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>To-Do</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"todo\">\r\n            <div class=\"todo-list-wrapper\">\r\n              <div class=\"todo-list-container\">\r\n                <div class=\"mark-all-tasks\">\r\n                  <div class=\"mark-all-tasks-container\">\r\n                    <span class=\"mark-all-btn\" [ngClass]=\"{'move-up':completed}\" id=\"mark-all-finished\" role=\"button\">\r\n                      <span class=\"btn-label\">Mark all as finished</span>\r\n                      <span class=\"action-box completed\" (click)=\"markAllAction(true);\">\r\n                        <i class=\"icon\"><i class=\"icon-check\"></i></i>\r\n                      </span>\r\n                    </span>\r\n                    <span class=\"mark-all-btn\" [ngClass]=\"{'move-down':!completed}\" id=\"mark-all-incomplete\"\r\n                      role=\"button\">\r\n                      <span class=\"btn-label\">Mark all as completed</span>\r\n                      <span class=\"action-box\" (click)=\"markAllAction(false);\">\r\n                        <i class=\"icon\"><i class=\"icon-check\"></i></i>\r\n                      </span>\r\n                    </span>\r\n                  </div>\r\n                </div>\r\n                <div class=\"todo-list-body\">\r\n                  <ul id=\"todo-list\">\r\n                    <li class=\"task\" [ngClass]=\"{'completed':todo.completed}\" *ngFor=\"let todo of todos;let i=index\">\r\n                      <div class=\"task-container\">\r\n                        <h4 class=\"task-label\">{{todo.text}}</h4>\r\n                        <span class=\"task-action-btn\">\r\n                          <span class=\"action-box large delete-btn\" title=\"Delete Task\" (click)=\"taskDeleted(i)\"><i\r\n                              class=\"icon\"><i class=\"icon-trash\"></i></i></span>\r\n                          <span class=\"action-box large complete-btn\" title=\"Mark Complete\"\r\n                            (click)=\"taskCompleted(todo)\"><i class=\"icon\"><i class=\"icon-check\"></i></i></span>\r\n                        </span>\r\n                      </div>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n                <div class=\"todo-list-footer\">\r\n                  <div class=\"add-task-btn-wrapper\">\r\n                    <span class=\"add-task-btn\" [ngClass]=\"{'hide':visible}\">\r\n                      <button class=\"btn btn-primary\" (click)=\"visible=true\">\r\n                        <i class=\"icon-plus mr-1\"></i>Add new task\r\n                      </button>\r\n                    </span>\r\n                  </div>\r\n                  <div class=\"new-task-wrapper\" [ngClass]=\"{'visible':visible}\">\r\n                    <textarea id=\"new-task\" placeholder=\"Enter new task here. . .\" [(ngModel)]=\"text\"\r\n                      [class.border-danger]=\"red_border\"></textarea>\r\n                    <button class=\"btn btn-danger cancel-btn\" id=\"close-task-panel\"\r\n                      (click)=\"visible=false\">Close</button>\r\n                    <button class=\"btn btn-success ml-3 add-new-task-btn\" id=\"add-task\" (click)=\"addTask(text)\">Add\r\n                      Task</button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"notification-popup hide\">\r\n              <p><span class=\"task\"></span><span class=\"notification-text\"></span></p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./src/app/components/to-do/to-do-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/to-do/to-do-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: ToDoRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoRoutingModule", function() { return ToDoRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _to_do_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./to-do.component */ "./src/app/components/to-do/to-do.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _to_do_component__WEBPACK_IMPORTED_MODULE_2__["ToDoComponent"],
                data: {
                    title: "To-Do",
                    breadcrumb: ""
                }
            }
        ]
    }
];
var ToDoRoutingModule = /** @class */ (function () {
    function ToDoRoutingModule() {
    }
    ToDoRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ToDoRoutingModule);
    return ToDoRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/to-do/to-do.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/to-do/to-do.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdG8tZG8vdG8tZG8uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/to-do/to-do.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/to-do/to-do.component.ts ***!
  \*****************************************************/
/*! exports provided: ToDoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoComponent", function() { return ToDoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_todo_todo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/data/todo/todo */ "./src/app/shared/data/todo/todo.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ToDoComponent = /** @class */ (function () {
    function ToDoComponent(toastrService) {
        this.toastrService = toastrService;
        this.todos = _shared_data_todo_todo__WEBPACK_IMPORTED_MODULE_1__["task"];
        this.red_border = false;
    }
    ToDoComponent.prototype.ngOnInit = function () { };
    ToDoComponent.prototype.addTask = function (text) {
        if (!text) {
            this.red_border = true;
            return false;
        }
        var task = { text: text, completed: false };
        this.todos.push(task);
        this.text = '';
        this.red_border = false;
    };
    ToDoComponent.prototype.taskCompleted = function (task) {
        task.completed = !task.completed;
        task.completed ? this.toastrService.success(task.text, "Mark as completed") : this.toastrService.error(task.text, "Mark as Incompleted");
    };
    ToDoComponent.prototype.taskDeleted = function (index) {
        this.todos.splice(index, 1);
    };
    ToDoComponent.prototype.markAllAction = function (action) {
        this.todos.filter(function (task) {
            task.completed = action;
        });
        this.completed = action;
        action ? this.toastrService.success("All Task as Completed") : this.toastrService.error("All Task as Incompleted");
    };
    ToDoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-to-do',
            template: __webpack_require__(/*! raw-loader!./to-do.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/to-do/to-do.component.html"),
            styles: [__webpack_require__(/*! ./to-do.component.scss */ "./src/app/components/to-do/to-do.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]])
    ], ToDoComponent);
    return ToDoComponent;
}());



/***/ }),

/***/ "./src/app/components/to-do/to-do.module.ts":
/*!**************************************************!*\
  !*** ./src/app/components/to-do/to-do.module.ts ***!
  \**************************************************/
/*! exports provided: ToDoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToDoModule", function() { return ToDoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _to_do_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./to-do-routing.module */ "./src/app/components/to-do/to-do-routing.module.ts");
/* harmony import */ var _to_do_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./to-do.component */ "./src/app/components/to-do/to-do.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ToDoModule = /** @class */ (function () {
    function ToDoModule() {
    }
    ToDoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_to_do_component__WEBPACK_IMPORTED_MODULE_4__["ToDoComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _to_do_routing_module__WEBPACK_IMPORTED_MODULE_3__["ToDoRoutingModule"]
            ]
        })
    ], ToDoModule);
    return ToDoModule;
}());



/***/ })

}]);
//# sourceMappingURL=components-to-do-to-do-module.js.map