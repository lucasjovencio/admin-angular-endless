(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/angular-count-to/modules/angular-count-to.es5.js":
/*!***********************************************************************!*\
  !*** ./node_modules/angular-count-to/modules/angular-count-to.es5.js ***!
  \***********************************************************************/
/*! exports provided: CountToDirective, CountToModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountToDirective", function() { return CountToDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountToModule", function() { return CountToModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var CountToDirective = (function () {
    /**
     * @param {?} el
     */
    function CountToDirective(el) {
        this.el = el;
        this.from = 0;
        this.duration = 4;
        this.e = this.el.nativeElement;
        this.refreshInterval = 30;
        this.step = 0;
    }
    /**
     * @return {?}
     */
    CountToDirective.prototype.ngOnInit = function () {
    };
    /**
     * @return {?}
     */
    CountToDirective.prototype.ngOnChanges = function () {
        if (this.CountTo) {
            this.start();
        }
    };
    /**
     * @return {?}
     */
    CountToDirective.prototype.calculate = function () {
        this.duration = this.duration * 1000;
        this.steps = Math.ceil(this.duration / this.refreshInterval);
        this.increment = ((this.CountTo - this.from) / this.steps);
        this.num = this.from;
    };
    /**
     * @return {?}
     */
    CountToDirective.prototype.tick = function () {
        var _this = this;
        setTimeout(function () {
            _this.num += _this.increment;
            _this.step++;
            if (_this.step >= _this.steps) {
                _this.num = _this.CountTo;
                _this.e.textContent = _this.CountTo;
            }
            else {
                _this.e.textContent = Math.round(_this.num);
                _this.tick();
            }
        }, this.refreshInterval);
    };
    /**
     * @return {?}
     */
    CountToDirective.prototype.start = function () {
        this.calculate();
        this.tick();
    };
    return CountToDirective;
}());
CountToDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[CountTo]'
            },] },
];
/**
 * @nocollapse
 */
CountToDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
]; };
CountToDirective.propDecorators = {
    'CountTo': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'from': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'duration': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var CountToModule = (function () {
    function CountToModule() {
    }
    /**
     * Use in AppModule
     * @return {?}
     */
    CountToModule.forRoot = function () {
        return {
            ngModule: CountToModule,
            providers: []
        };
    };
    /**
     * Use in features modules with lazy loading
     * @return {?}
     */
    CountToModule.forChild = function () {
        return {
            ngModule: CountToModule,
            providers: []
        };
    };
    return CountToModule;
}());
CountToModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [
                    CountToDirective
                    // Pipes.
                    // Directives.
                ],
                exports: [
                    CountToDirective
                    // Pipes.
                    // Directives.
                ]
            },] },
];
/**
 * @nocollapse
 */
CountToModule.ctorParameters = function () { return []; };
// Public classes.
/**
 * Angular library starter.
 * Build an Angular library compatible with AoT compilation & Tree shaking.
 * Written by Roberto Simonetti.
 * MIT license.
 * https://github.com/robisim74/angular-count-to
 */
/**
 * Entry point for all public APIs of the package.
 */
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=angular-count-to.es5.js.map


/***/ }),

/***/ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js":
/*!*****************************************************************!*\
  !*** ./node_modules/ng2-search-filter/ng2-search-filter.es5.js ***!
  \*****************************************************************/
/*! exports provided: Ng2SearchPipeModule, Ng2SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipeModule", function() { return Ng2SearchPipeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipe", function() { return Ng2SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var Ng2SearchPipe = (function () {
    function Ng2SearchPipe() {
    }
    /**
     * @param {?} items object from array
     * @param {?} term term's search
     * @return {?}
     */
    Ng2SearchPipe.prototype.transform = function (items, term) {
        if (!term || !items)
            return items;
        return Ng2SearchPipe.filter(items, term);
    };
    /**
     *
     * @param {?} items List of items to filter
     * @param {?} term  a string term to compare with every property of the list
     *
     * @return {?}
     */
    Ng2SearchPipe.filter = function (items, term) {
        var /** @type {?} */ toCompare = term.toLowerCase();
        /**
         * @param {?} item
         * @param {?} term
         * @return {?}
         */
        function checkInside(item, term) {
            for (var /** @type {?} */ property in item) {
                if (item[property] === null || item[property] == undefined) {
                    continue;
                }
                if (typeof item[property] === 'object') {
                    if (checkInside(item[property], term)) {
                        return true;
                    }
                }
                if (item[property].toString().toLowerCase().includes(toCompare)) {
                    return true;
                }
            }
            return false;
        }
        return items.filter(function (item) {
            return checkInside(item, term);
        });
    };
    return Ng2SearchPipe;
}());
Ng2SearchPipe.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                name: 'filter',
                pure: false
            },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
Ng2SearchPipe.ctorParameters = function () { return []; };
var Ng2SearchPipeModule = (function () {
    function Ng2SearchPipeModule() {
    }
    return Ng2SearchPipeModule;
}());
Ng2SearchPipeModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [Ng2SearchPipe],
                exports: [Ng2SearchPipe]
            },] },
];
/**
 * @nocollapse
 */
Ng2SearchPipeModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=ng2-search-filter.es5.js.map


/***/ }),

/***/ "./src/app/shared/data/todo/todo.ts":
/*!******************************************!*\
  !*** ./src/app/shared/data/todo/todo.ts ***!
  \******************************************/
/*! exports provided: task */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "task", function() { return task; });
var task = [{
        text: "Weekly Bigbazar Shopping",
        completed: false
    },
    {
        text: "Go Outside Picnic on Sunday",
        completed: false
    },
    {
        text: "Write a blog post",
        completed: true
    },
    {
        text: "Do the chicken dance",
        completed: true
    },
    {
        text: "Pay the electricity bills",
        completed: false
    }];


/***/ })

}]);
//# sourceMappingURL=common.js.map