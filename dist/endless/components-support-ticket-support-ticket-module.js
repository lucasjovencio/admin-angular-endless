(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-support-ticket-support-ticket-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/support-ticket/support-ticket.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/support-ticket/support-ticket.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Support Ticket List</h5><span>List of ticket opend by customers</span>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Order</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"2563\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Pending</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"8943\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-secondary\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Running</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"2500\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase mt-4\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-warning\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Smooth</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"2060\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase mt-4\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-info\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Done</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"5600\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase mt-4\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-4 col-sm-6\">\r\n              <div class=\"card ecommerce-widget\">\r\n                <div class=\"card-body support-ticket-font\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-5\"><span>Cancle</span>\r\n                      <h3 class=\"total-num counter\" class=\"counter\" [CountTo]=\"2560\" [from]=\"0\" [duration]=\"2\"></h3>\r\n                    </div>\r\n                    <div class=\"col-7\">\r\n                      <div class=\"text-md-right\">\r\n                        <ul>\r\n                          <li>Profit<span class=\"product-stts txt-success ml-2\">8989<i\r\n                                class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                          <li>Loss<span class=\"product-stts txt-danger ml-2\">2560<i\r\n                                class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"progress-showcase\">\r\n                    <div class=\"progress sm-progress-bar\">\r\n                      <div class=\"progress-bar bg-danger\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                        aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"table-responsive\">\r\n            <div class=\"server-datatable\">\r\n              <div class=\"table-responsive\">\r\n                <ng2-smart-table [settings]=\"settings\" [source]=\"support\"></ng2-smart-table>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./src/app/components/support-ticket/support-ticket-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/support-ticket/support-ticket-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: SupportTicketRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportTicketRoutingModule", function() { return SupportTicketRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _support_ticket_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./support-ticket.component */ "./src/app/components/support-ticket/support-ticket.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _support_ticket_component__WEBPACK_IMPORTED_MODULE_2__["SupportTicketComponent"],
                data: {
                    title: "Support Ticket",
                    breadcrumb: ""
                }
            }
        ]
    }
];
var SupportTicketRoutingModule = /** @class */ (function () {
    function SupportTicketRoutingModule() {
    }
    SupportTicketRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SupportTicketRoutingModule);
    return SupportTicketRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/support-ticket/support-ticket.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/support-ticket/support-ticket.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc3VwcG9ydC10aWNrZXQvc3VwcG9ydC10aWNrZXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/support-ticket/support-ticket.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/support-ticket/support-ticket.component.ts ***!
  \***********************************************************************/
/*! exports provided: SupportTicketComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportTicketComponent", function() { return SupportTicketComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_support_ticket__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/data/tables/support-ticket */ "./src/app/shared/data/tables/support-ticket.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SupportTicketComponent = /** @class */ (function () {
    function SupportTicketComponent() {
        this.support = [];
        this.settings = {
            columns: {
                img: {
                    title: 'Image',
                    type: 'html',
                },
                position: {
                    title: 'Position'
                },
                salary: {
                    title: 'Salary'
                },
                office: {
                    title: 'Office'
                },
                skill: {
                    title: 'Skill',
                    type: 'html',
                },
                extn: {
                    title: 'Extn'
                },
                email: {
                    title: 'Email'
                }
            },
        };
        this.support = _shared_data_tables_support_ticket__WEBPACK_IMPORTED_MODULE_1__["supportDB"].ticket;
    }
    SupportTicketComponent.prototype.ngOnInit = function () { };
    SupportTicketComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-support-ticket',
            template: __webpack_require__(/*! raw-loader!./support-ticket.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/support-ticket/support-ticket.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./support-ticket.component.scss */ "./src/app/components/support-ticket/support-ticket.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SupportTicketComponent);
    return SupportTicketComponent;
}());



/***/ }),

/***/ "./src/app/components/support-ticket/support-ticket.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/support-ticket/support-ticket.module.ts ***!
  \********************************************************************/
/*! exports provided: SupportTicketModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportTicketModule", function() { return SupportTicketModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var angular_count_to__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-count-to */ "./node_modules/angular-count-to/modules/angular-count-to.es5.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _support_ticket_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./support-ticket-routing.module */ "./src/app/components/support-ticket/support-ticket-routing.module.ts");
/* harmony import */ var _support_ticket_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./support-ticket.component */ "./src/app/components/support-ticket/support-ticket.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var SupportTicketModule = /** @class */ (function () {
    function SupportTicketModule() {
    }
    SupportTicketModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_support_ticket_component__WEBPACK_IMPORTED_MODULE_5__["SupportTicketComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _support_ticket_routing_module__WEBPACK_IMPORTED_MODULE_4__["SupportTicketRoutingModule"],
                angular_count_to__WEBPACK_IMPORTED_MODULE_2__["CountToModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["Ng2SmartTableModule"]
            ]
        })
    ], SupportTicketModule);
    return SupportTicketModule;
}());



/***/ }),

/***/ "./src/app/shared/data/tables/support-ticket.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/data/tables/support-ticket.ts ***!
  \******************************************************/
/*! exports provided: supportDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportDB", function() { return supportDB; });
var supportDB = /** @class */ (function () {
    function supportDB() {
    }
    supportDB.ticket = [
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/1.jpg'><div class='media-body align-self-center'> <div>Tiger</div></div></div>",
            position: "System Architect",
            salary: "$320,800",
            office: "Edinburgh",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-danger' role='progressbar'></div></div></div>",
            extn: 5421,
            email: "t.nixon@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/2.png'><div class='media-body align-self-center'> <div>Tiger</div></div></div>",
            position: "System Architect",
            salary: "$320,800",
            office: "Edinburgh",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-secondary' role='progressbar'></div></div></div>",
            extn: 5421,
            email: "t.nixon@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/3.jpg'><div class='media-body align-self-center'> <div>Garrett Winters</div></div></div>",
            position: "Accountant",
            salary: "$170,750",
            office: "Tokyo",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 8422,
            email: "g.winters@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/4.jpg'><div class='media-body align-self-center'> <div>Garrett Winters</div></div></div>",
            position: "Accountant",
            salary: "$170,750",
            office: "Tokyo",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 8422,
            email: "g.winters@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/5.jpg'><div class='media-body align-self-center'> <div>Garrett Winters</div></div></div>",
            position: "Accountant",
            salary: "$170,750",
            office: "Tokyo",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 8422,
            email: "g.winters@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/6.jpg'><div class='media-body align-self-center'> <div>Ashton Cox</div></div></div>",
            position: "Junior Technical Author",
            salary: "$86,000",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 1562,
            email: "a.cox@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/7.jpg'><div class='media-body align-self-center'> <div>Cedric Kelly</div></div></div>",
            position: "Senior Javascript Developer",
            salary: "$433,060",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-secondary' role='progressbar'></div></div></div>",
            extn: 6224,
            email: "c.kelly@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/8.jpg'><div class='media-body align-self-center'> <div>Cedric Kelly</div></div></div>",
            position: "Senior Javascript Developer",
            salary: "$433,060",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-secondary' role='progressbar'></div></div></div>",
            extn: 6224,
            email: "c.kelly@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/9.jpg'><div class='media-body align-self-center'> <div>Airi Satou</div></div></div>",
            position: "Accountant",
            salary: "$162,700",
            office: "Tokyo",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 5407,
            email: "a.satou@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/10.jpg'><div class='media-body align-self-center'> <div>Brielle Williamson</div></div></div>",
            position: "Integration Specialist",
            salary: "$372,000",
            office: "New York",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-info' role='progressbar'></div></div></div>",
            extn: 4804,
            email: "b.williamson@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/1.jpg'><div class='media-body align-self-center'> <div>Herrod Chandler</div></div></div>",
            position: "Sales Assistant",
            salary: "$137,500",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-warning' role='progressbar'></div></div></div>",
            extn: 9608,
            email: "h.chandler@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/5.jpg'><div class='media-body align-self-center'> <div>Rhona Davidson</div></div></div>",
            position: "Integration Specialist",
            salary: "$327,900",
            office: "Tokyo",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-secondary' role='progressbar'></div></div></div>",
            extn: 6200,
            email: "r.davidson@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/1.jpg'><div class='media-body align-self-center'> <div>Colleen Hurst</div></div></div>",
            position: "Javascript Developer",
            salary: "$205,500",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 2360,
            email: "c.hurst@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/2.png'><div class='media-body align-self-center'> <div>Sonya Frost</div></div></div>",
            position: "Software Engineer",
            salary: "$103,600",
            office: "Edinburgh",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-primary' role='progressbar'></div></div></div>",
            extn: 1667,
            email: "s.frost@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/3.jpg'><div class='media-body align-self-center'> <div>Jena Gaines</div></div></div>",
            position: "Office Manager",
            salary: "$90,560",
            office: "London",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-warning' role='progressbar'></div></div></div>",
            extn: 3814,
            email: "j.gaines@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/4.jpg'><div class='media-body align-self-center'> <div>Quinn Flynn</div></div></div>",
            position: "Support Lead",
            salary: "$342,000",
            office: "Edinburgh",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-danger' role='progressbar'></div></div></div>",
            extn: 9497,
            email: "q.flynn@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/5.jpg'><div class='media-body align-self-center'> <div>Charde Marshall</div></div></div>",
            position: "Regional Director",
            salary: "$470,600",
            office: "San Francisco",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-info' role='progressbar'></div></div></div>",
            extn: 6741,
            email: "c.marshall@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/6.jpg'><div class='media-body align-self-center'> <div>Haley Kennedy</div></div></div>",
            position: "Senior Marketing Designer",
            salary: "$313,500",
            office: "London",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 3597,
            email: "h.kennedy@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/7.jpg'><div class='media-body align-self-center'> <div>Tatyana Fitzpatrick</div></div></div>",
            position: "Regional Director",
            salary: "$385,750",
            office: "London",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-warning' role='progressbar'></div></div></div>",
            extn: 1965,
            email: "t.fitzpatrick@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/8.jpg'><div class='media-body align-self-center'> <div>Michael Silva</div></div></div>",
            position: "Marketing Designer",
            salary: "$198,500",
            office: "London",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-primary' role='progressbar'></div></div></div>",
            extn: 1581,
            email: "m.silva@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/9.jpg'><div class='media-body align-self-center'> <div>Michael Silva</div></div></div>",
            position: "Paul Byrd",
            salary: "Chief Financial Officer (CFO)",
            office: "$725,000",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-ress-bar bg-success' role='progressbar'></div></div></div>",
            extn: 3059,
            email: "p.byrd@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/10.jpg'><div class='media-body align-self-center'> <div>Gloria Little</div></div></div>",
            position: "Systems Administrator",
            salary: "$237,500",
            office: "New York",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 1721,
            email: "g.little@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/2.png'><div class='media-body align-self-center'> <div>Bradley Greer</div></div></div>",
            position: "Software Engineer",
            salary: "$132,000",
            office: "London",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-primary' role='progressbar'></div></div></div>",
            extn: 2558,
            email: "b.greer@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/5.jpg'><div class='media-body align-self-center'> <div>Michael Silva</div></div></div>",
            position: "Dai Rios",
            salary: "Personnel Lead",
            office: "$217,500",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-gress-bar bg-warning' role='progressbar'></div></div></div>",
            extn: 2290,
            email: "d.rios@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/1.jpg'><div class='media-body align-self-center'> <div>Jenette Caldwell</div></div></div>",
            position: "Development Lead",
            salary: "$345,000",
            office: "New York",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-primary' role='progressbar'></div></div></div>",
            extn: 1937,
            email: "j.caldwell@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/2.png'><div class='media-body align-self-center'> <div>Yuri Berry</div></div></div>",
            position: "Chief Marketing Officer (CMO)",
            salary: "$675,000",
            office: "New York",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-danger' role='progressbar'></div></div></div>",
            extn: 6154,
            email: "y.berry@datatables.net"
        },
        {
            img: "<div class='media'><img class='rounded-circle img-30 mr-3' src='assets/images/user/3.jpg'><div class='media-body align-self-center'> <div>C</div></div></div>",
            position: "Pre-Sales Support",
            salary: "$106,450",
            office: "New York",
            skill: "<div class='progress-showcase'><div class='progress sm-progress-bar'><div class='progress-bar custom-progress-width bg-success' role='progressbar'></div></div></div>",
            extn: 8330,
            email: "c.vance@datatables.net"
        }
    ];
    return supportDB;
}());



/***/ })

}]);
//# sourceMappingURL=components-support-ticket-support-ticket-module.js.map