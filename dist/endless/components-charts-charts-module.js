(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-charts-charts-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/charts/chartist/chartist.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/charts/chartist/chartist.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Advanced SMIL Animations</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart1.type\" [data]=\"chart1.data\" [options]=\"chart1.options\"\r\n                        [events]=\"chart1.events\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>SVG Path animation</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart2.type\" [data]=\"chart2.data\" [options]=\"chart2.options\"\r\n                        [events]=\"chart2.events\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Animating a Donut with Svg.animate</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart3.type\" [data]=\"chart3.data\" [options]=\"chart3.options\"\r\n                        [events]=\"chart3.events\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Bi-polar Line chart with area only</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart4.type\" [data]=\"chart4.data\" [options]=\"chart4.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line chart with area</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart5.type\" [data]=\"chart5.data\" [options]=\"chart5.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Bi-polar bar chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart6.type\" [data]=\"chart6.data\" [options]=\"chart6.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Stacked bar chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart7.type\" [data]=\"chart7.data\" [options]=\"chart7.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Horizontal bar chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart8.type\" [data]=\"chart8.data\" [options]=\"chart8.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Extreme responsive configuration</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart9.type\" [data]=\"chart9.data\" [options]=\"chart9.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Simple line chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart10.type\" [data]=\"chart10.data\" [options]=\"chart10.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Holes in data</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart11.type\" [data]=\"chart11.data\" [options]=\"chart11.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12 col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Filled holes in data</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <x-chartist [type]=\"chart12.type\" [data]=\"chart12.data\" [options]=\"chart12.options\"></x-chartist>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/charts/chartjs/chartjs.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/charts/chartjs/chartjs.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Bar Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart\" [datasets]=\"barChartData\" [labels]=\"barChartLabels\"\r\n                        [options]=\"barChartOptions\" [colors]=\"barChartColors\" [legend]=\"barChartLegend\"\r\n                        [chartType]=\"barChartType\" (chartHover)=\"chartHovered($event)\"\r\n                        (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line Graph</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineGraphData\" [labels]=\"lineGraphLabels\"\r\n                        [options]=\"lineGraphOptions\" [colors]=\"lineGraphColors\" [legend]=\"lineGraphLegend\"\r\n                        [chartType]=\"lineGraphType\" (chartHover)=\"chartHovered($event)\"\r\n                        (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Radar Graph</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart\" [datasets]=\"radarGraphData\" [labels]=\"radarGraphLabels\"\r\n                        [options]=\"radarGraphOptions\" [colors]=\"radarGraphColors\" [legend]=\"radarGraphLegend\"\r\n                        [chartType]=\"radarGraphType\" (chartHover)=\"chartHovered($event)\"\r\n                        (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart\" [datasets]=\"lineChartData\" [labels]=\"lineChartLabels\"\r\n                        [options]=\"lineChartOptions\" [colors]=\"lineChartColors\" [legend]=\"lineChartLegend\"\r\n                        [chartType]=\"lineChartType\" (chartHover)=\"chartHovered($event)\"\r\n                        (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Doughnut Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart\" [data]=\"doughnutChartData\" [labels]=\"doughnutChartLabels\"\r\n                        [options]=\"doughnutChartOptions\" [chartType]=\"doughnutChartType\" [colors]=\"doughnutChartColors\"\r\n                        (chartHover)=\"chartHovered($event)\" (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 col-md-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Polar Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <canvas baseChart class=\"chart chartjs-custom\" [data]=\"polarAreaChartData\"\r\n                        [options]=\"polarChartOptions\" [labels]=\"polarAreaChartLabels\" [legend]=\"polarAreaLegend\"\r\n                        [chartType]=\"polarAreaChartType\" [colors]=\"ploarChartColors\" (chartHover)=\"chartHovered($event)\"\r\n                        (chartClick)=\"chartClicked($event)\"></canvas>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/charts/google/google.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/charts/google/google.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Pie Chart <span class=\"digits\">1</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"pieChart1\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Pie Chart <span class=\"digits\">2</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"pieChart2\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Pie Chart <span class=\"digits\">3</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"pieChart3\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Pie Chart <span class=\"digits\">4</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"pieChart4\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Area Chart <span class=\"digits\">1</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"areaChart1\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Area Chart <span class=\"digits\">2</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"areaChart2\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Column Chart <span class=\"digits\">1</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"columnChart1\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Column Chart <span class=\"digits\">2</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"columnChart2\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Bars Chart <span class=\"digits\">1</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"barChart1\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Bars Chart <span class=\"digits\">2</span></h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"barChart2\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"lineChart\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Combo Chart</h5>\r\n                </div>\r\n                <div class=\"card-body chart-block\">\r\n                    <google-chart [data]=\"comboChart\"></google-chart>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/charts/ngx-chart/ngx-chart.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/charts/ngx-chart/ngx-chart.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-6 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Single Vertical Bar Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-bar-vertical [scheme]=\"barChartColorScheme\" [results]=\"barChartsingle\"\r\n                                [gradient]=\"barChartGradient\" [xAxis]=\"barChartShowXAxis\" [yAxis]=\"barChartShowYAxis\"\r\n                                [legend]=\"barChartShowLegend\" [showXAxisLabel]=\"barChartShowXAxisLabel\"\r\n                                [showYAxisLabel]=\"barChartShowYAxisLabel\" [xAxisLabel]=\"barChartXAxisLabel\"\r\n                                [yAxisLabel]=\"barChartYAxisLabel\" [roundEdges]=\"false\" (select)=\"onSelect($event)\">\r\n                            </ngx-charts-bar-vertical>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-6 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Multiple Vertical Bar Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-bar-vertical-2d [scheme]=\"barChartColorScheme\" [results]=\"multiData\"\r\n                                [gradient]=\"barChartGradient\" [xAxis]=\"barChartShowXAxis\" [yAxis]=\"barChartShowYAxis\"\r\n                                [legend]=\"barChartShowLegend\" [showXAxisLabel]=\"barChartShowXAxisLabel\"\r\n                                [showYAxisLabel]=\"barChartShowYAxisLabel\" [xAxisLabel]=\"barChartXAxisLabel\"\r\n                                [yAxisLabel]=\"barChartYAxisLabel\" [roundEdges]=\"false\" (select)=\"onSelect($event)\">\r\n                            </ngx-charts-bar-vertical-2d>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 xl-100\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Single Pie Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-pie-chart [scheme]=\"pieChartColorScheme\" [results]=\"pieChart\"\r\n                                [explodeSlices]=\"true\" [labels]=\"pieChartShowLabels\" [doughnut]=\"false\" arcWidth=0.30\r\n                                [gradient]=\"pieChartGradient\" (select)=\"onSelect($event)\">\r\n                            </ngx-charts-pie-chart>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 xl-100\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Grid Pie Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container grid-pie-chart\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-pie-grid [scheme]=\"pieChartColorScheme\" [results]=\"pieChart\"\r\n                                (select)=\"onSelect($event)\">\r\n                            </ngx-charts-pie-grid>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-6 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-line-chart [scheme]=\"areaChartcolorScheme\" [results]=\"multiData\"\r\n                                [gradient]=\"areaChartgradient\" [xAxis]=\"areaChartshowXAxis\" [yAxis]=\"areaChartshowYAxis\"\r\n                                [showXAxisLabel]=\"areaChartshowXAxisLabel\" [showYAxisLabel]=\"areaChartshowYAxisLabel\"\r\n                                [xAxisLabel]=\"areaChartxAxisLabel\" [curve]=lineChartcurve (select)=\"onSelect($event)\">\r\n                            </ngx-charts-line-chart>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-6 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Line Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-line-chart [scheme]=\"areaChartcolorScheme\" [results]=\"multiData\"\r\n                                [gradient]=\"areaChartgradient\" [xAxis]=\"areaChartshowXAxis\" [yAxis]=\"areaChartshowYAxis\"\r\n                                [showXAxisLabel]=\"areaChartshowXAxisLabel\" [showYAxisLabel]=\"areaChartshowYAxisLabel\"\r\n                                [xAxisLabel]=\"areaChartxAxisLabel\" [curve]=\"lineChartcurve1\"\r\n                                (select)=\"onSelect($event)\">\r\n                            </ngx-charts-line-chart>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 xl-100\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Area Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-area-chart [scheme]=\"areaChartcolorScheme\" [results]=\"multiData\"\r\n                                [gradient]=\"areaChartgradient\" [xAxis]=\"areaChartshowXAxis\" [yAxis]=\"areaChartshowYAxis\"\r\n                                [showXAxisLabel]=\"areaChartshowXAxisLabel\" [showYAxisLabel]=\"areaChartshowYAxisLabel\"\r\n                                [xAxisLabel]=\"areaChartxAxisLabel\" (select)=\"onSelect($event)\">\r\n                            </ngx-charts-area-chart>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xl-6 xl-100\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Gauge Chart</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <div class=\"flot-chart-container\">\r\n                        <div class=\"flot-chart-placeholder ngx-chart-direction\" id=\"github-issues\">\r\n                            <ngx-charts-number-card [scheme]=\"barChartColorScheme\" [results]=\"single\">\r\n                            </ngx-charts-number-card>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./src/app/components/charts/chartist/chartist.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/components/charts/chartist/chartist.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhcnRzL2NoYXJ0aXN0L2NoYXJ0aXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/charts/chartist/chartist.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/charts/chartist/chartist.component.ts ***!
  \******************************************************************/
/*! exports provided: ChartistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartistComponent", function() { return ChartistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/chart/chartist */ "./src/app/shared/data/chart/chartist.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartistComponent = /** @class */ (function () {
    function ChartistComponent() {
        // Charts
        this.chart1 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart1"];
        this.chart2 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart2"];
        this.chart3 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart3"];
        this.chart4 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart4"];
        this.chart5 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart5"];
        this.chart6 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart6"];
        this.chart7 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart7"];
        this.chart8 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart8"];
        this.chart9 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart9"];
        this.chart10 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart10"];
        this.chart11 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart11"];
        this.chart12 = _shared_data_chart_chartist__WEBPACK_IMPORTED_MODULE_1__["chart12"];
    }
    ChartistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chartist',
            template: __webpack_require__(/*! raw-loader!./chartist.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/charts/chartist/chartist.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./chartist.component.scss */ "./src/app/components/charts/chartist/chartist.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ChartistComponent);
    return ChartistComponent;
}());



/***/ }),

/***/ "./src/app/components/charts/chartjs/chartjs.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/charts/chartjs/chartjs.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhcnRzL2NoYXJ0anMvY2hhcnRqcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/charts/chartjs/chartjs.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/charts/chartjs/chartjs.component.ts ***!
  \****************************************************************/
/*! exports provided: ChartjsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartjsComponent", function() { return ChartjsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/chart/chartjs */ "./src/app/shared/data/chart/chartjs.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartjsComponent = /** @class */ (function () {
    function ChartjsComponent() {
        // barChart
        this.barChartOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartOptions"];
        this.barChartLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartLabels"];
        this.barChartType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartType"];
        this.barChartLegend = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartLegend"];
        this.barChartData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartData"];
        this.barChartColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["barChartColors"];
        // lineGraph Chart
        this.lineGraphOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphOptions"];
        this.lineGraphLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphLabels"];
        this.lineGraphType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphType"];
        this.lineGraphLegend = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphLegend"];
        this.lineGraphData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphData"];
        this.lineGraphColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineGraphColors"];
        // radarGraph Chart
        this.radarGraphOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphOptions"];
        this.radarGraphLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphLabels"];
        this.radarGraphType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphType"];
        this.radarGraphLegend = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphLegend"];
        this.radarGraphData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphData"];
        this.radarGraphColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["radarGraphColors"];
        // lineChart
        this.lineChartData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartData"];
        this.lineChartLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartLabels"];
        this.lineChartOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartOptions"];
        this.lineChartColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartColors"];
        this.lineChartLegend = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartLegend"];
        this.lineChartType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["lineChartType"];
        // Doughnut
        this.doughnutChartLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["doughnutChartLabels"];
        this.doughnutChartData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["doughnutChartData"];
        this.doughnutChartType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["doughnutChartType"];
        this.doughnutChartColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["doughnutChartColors"];
        this.doughnutChartOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["doughnutChartOptions"];
        // PolarArea
        this.polarAreaChartLabels = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["polarAreaChartLabels"];
        this.polarAreaChartData = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["polarAreaChartData"];
        this.polarAreaLegend = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["polarAreaLegend"];
        this.ploarChartColors = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["ploarChartColors"];
        this.polarAreaChartType = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["polarAreaChartType"];
        this.polarChartOptions = _shared_data_chart_chartjs__WEBPACK_IMPORTED_MODULE_1__["polarChartOptions"];
    }
    // events
    ChartjsComponent.prototype.chartClicked = function (e) { };
    ChartjsComponent.prototype.chartHovered = function (e) { };
    ChartjsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chartjs',
            template: __webpack_require__(/*! raw-loader!./chartjs.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/charts/chartjs/chartjs.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./chartjs.component.scss */ "./src/app/components/charts/chartjs/chartjs.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ChartjsComponent);
    return ChartjsComponent;
}());



/***/ }),

/***/ "./src/app/components/charts/charts-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/components/charts/charts-routing.module.ts ***!
  \************************************************************/
/*! exports provided: ChartsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartsRoutingModule", function() { return ChartsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _google_google_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./google/google.component */ "./src/app/components/charts/google/google.component.ts");
/* harmony import */ var _chartjs_chartjs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chartjs/chartjs.component */ "./src/app/components/charts/chartjs/chartjs.component.ts");
/* harmony import */ var _chartist_chartist_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chartist/chartist.component */ "./src/app/components/charts/chartist/chartist.component.ts");
/* harmony import */ var _ngx_chart_ngx_chart_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ngx-chart/ngx-chart.component */ "./src/app/components/charts/ngx-chart/ngx-chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        children: [
            {
                path: 'google',
                component: _google_google_component__WEBPACK_IMPORTED_MODULE_2__["GoogleComponent"],
                data: {
                    title: "Google",
                    breadcrumb: "Google"
                }
            },
            {
                path: 'chartjs',
                component: _chartjs_chartjs_component__WEBPACK_IMPORTED_MODULE_3__["ChartjsComponent"],
                data: {
                    title: "ChartJS",
                    breadcrumb: "ChartJS"
                }
            },
            {
                path: 'chartist',
                component: _chartist_chartist_component__WEBPACK_IMPORTED_MODULE_4__["ChartistComponent"],
                data: {
                    title: "Chartist",
                    breadcrumb: "Chartist"
                }
            },
            {
                path: 'ngx-chart',
                component: _ngx_chart_ngx_chart_component__WEBPACK_IMPORTED_MODULE_5__["NgxChartComponent"],
                data: {
                    title: "Ngx-Chart",
                    breadcrumb: "Ngx-Chart"
                }
            }
        ]
    }
];
var ChartsRoutingModule = /** @class */ (function () {
    function ChartsRoutingModule() {
    }
    ChartsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChartsRoutingModule);
    return ChartsRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/charts/charts.module.ts":
/*!****************************************************!*\
  !*** ./src/app/components/charts/charts.module.ts ***!
  \****************************************************/
/*! exports provided: ChartModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartModule", function() { return ChartModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _charts_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./charts-routing.module */ "./src/app/components/charts/charts-routing.module.ts");
/* harmony import */ var ng2_google_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-google-charts */ "./node_modules/ng2-google-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/fesm5/ng-chartist.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _google_google_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./google/google.component */ "./src/app/components/charts/google/google.component.ts");
/* harmony import */ var _chartjs_chartjs_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./chartjs/chartjs.component */ "./src/app/components/charts/chartjs/chartjs.component.ts");
/* harmony import */ var _chartist_chartist_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./chartist/chartist.component */ "./src/app/components/charts/chartist/chartist.component.ts");
/* harmony import */ var _ngx_chart_ngx_chart_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ngx-chart/ngx-chart.component */ "./src/app/components/charts/ngx-chart/ngx-chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var ChartModule = /** @class */ (function () {
    function ChartModule() {
    }
    ChartModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_google_google_component__WEBPACK_IMPORTED_MODULE_7__["GoogleComponent"], _chartjs_chartjs_component__WEBPACK_IMPORTED_MODULE_8__["ChartjsComponent"], _chartist_chartist_component__WEBPACK_IMPORTED_MODULE_9__["ChartistComponent"], _ngx_chart_ngx_chart_component__WEBPACK_IMPORTED_MODULE_10__["NgxChartComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _charts_routing_module__WEBPACK_IMPORTED_MODULE_2__["ChartsRoutingModule"],
                ng2_google_charts__WEBPACK_IMPORTED_MODULE_3__["Ng2GoogleChartsModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_4__["ChartsModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_5__["ChartistModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_6__["NgxChartsModule"]
            ]
        })
    ], ChartModule);
    return ChartModule;
}());



/***/ }),

/***/ "./src/app/components/charts/google/google.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/components/charts/google/google.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhcnRzL2dvb2dsZS9nb29nbGUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/charts/google/google.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/charts/google/google.component.ts ***!
  \**************************************************************/
/*! exports provided: GoogleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoogleComponent", function() { return GoogleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/chart/google-chart */ "./src/app/shared/data/chart/google-chart.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GoogleComponent = /** @class */ (function () {
    function GoogleComponent() {
        // Pie Chart
        this.pieChart1 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart1"];
        this.pieChart2 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart2"];
        this.pieChart3 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart3"];
        this.pieChart4 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart4"];
        // Area Chart
        this.areaChart1 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["areaChart1"];
        this.areaChart2 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["areaChart2"];
        // Column Chart
        this.columnChart1 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["columnChart1"];
        this.columnChart2 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["columnChart2"];
        // Bar Chart
        this.barChart1 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["barChart1"];
        this.barChart2 = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["barChart2"];
        // Line Chart
        this.lineChart = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["lineChart"];
        // Combo Chart
        this.comboChart = _shared_data_chart_google_chart__WEBPACK_IMPORTED_MODULE_1__["comboChart"];
        this.geoChartData = {
            chartType: 'GeoChart',
            dataTable: [
                ['City', 'Population'],
                ['Melbourne', 456789]
            ],
            options: {
                'region': 'IT',
                'displayMode': 'markers',
                colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#FF5370"]
            }
        };
    }
    GoogleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-google',
            template: __webpack_require__(/*! raw-loader!./google.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/charts/google/google.component.html"),
            styles: [__webpack_require__(/*! ./google.component.scss */ "./src/app/components/charts/google/google.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GoogleComponent);
    return GoogleComponent;
}());



/***/ }),

/***/ "./src/app/components/charts/ngx-chart/ngx-chart.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/charts/ngx-chart/ngx-chart.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhcnRzL25neC1jaGFydC9uZ3gtY2hhcnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/charts/ngx-chart/ngx-chart.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/charts/ngx-chart/ngx-chart.component.ts ***!
  \********************************************************************/
/*! exports provided: NgxChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxChartComponent", function() { return NgxChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/chart/ngx-chart */ "./src/app/shared/data/chart/ngx-chart.ts");
/* harmony import */ var _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/data/chart/config */ "./src/app/shared/data/chart/config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NgxChartComponent = /** @class */ (function () {
    function NgxChartComponent() {
        this.barChartsingle = _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["barChartSingle"];
        this.pieChart = _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart"];
        this.multiData = _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["multiData"];
        this.single = _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["single"];
        // Bar-chart options
        this.barChartShowYAxis = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartShowYAxis"];
        this.barChartShowXAxis = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartShowXAxis"];
        this.barChartGradient = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartGradient"];
        this.barChartShowLegend = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartShowLegend"];
        this.barChartShowXAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartShowXAxisLabel"];
        this.barChartXAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartXAxisLabel"];
        this.barChartShowYAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartShowYAxisLabel"];
        this.barChartYAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartYAxisLabel"];
        this.barChartColorScheme = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartColorScheme"];
        this.barChartshowGridLines = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["barChartshowGridLines"];
        // pie-chart options
        this.pieChartColorScheme = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["pieChartcolorScheme"];
        this.pieChartShowLabels = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["pieChartShowLabels"];
        this.pieChartGradient = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["pieChartGradient"];
        this.chartOptions = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["chartOptions"];
        //Area-chart options
        this.areaChartshowXAxis = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartshowXAxis"];
        this.areaChartshowYAxis = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartshowYAxis"];
        this.areaChartgradient = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartgradient"];
        this.areaChartshowXAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartshowXAxisLabel"];
        this.areaChartxAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartxAxisLabel"];
        this.areaChartshowYAxisLabel = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartshowYAxisLabel"];
        this.areaChartcolorScheme = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["areaChartcolorScheme"];
        this.lineChartcurve = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["lineChartcurve"];
        this.lineChartcurve1 = _shared_data_chart_config__WEBPACK_IMPORTED_MODULE_2__["lineChartcurve1"];
        Object.assign(this, { multiData: _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["multiData"], barChartSingle: _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["barChartSingle"], pieChart: _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["pieChart"], single: _shared_data_chart_ngx_chart__WEBPACK_IMPORTED_MODULE_1__["single"] });
    }
    NgxChartComponent.prototype.ngOnInit = function () { };
    NgxChartComponent.prototype.onSelect = function (e) { };
    NgxChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ngx-chart',
            template: __webpack_require__(/*! raw-loader!./ngx-chart.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/charts/ngx-chart/ngx-chart.component.html"),
            styles: [__webpack_require__(/*! ./ngx-chart.component.scss */ "./src/app/components/charts/ngx-chart/ngx-chart.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NgxChartComponent);
    return NgxChartComponent;
}());



/***/ }),

/***/ "./src/app/shared/data/chart/chartist.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/data/chart/chartist.ts ***!
  \***********************************************/
/*! exports provided: chart1, chart2, chart3, chart4, chart5, chart6, chart7, chart8, chart9, chart10, chart11, chart12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart1", function() { return chart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart2", function() { return chart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart3", function() { return chart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart4", function() { return chart4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart5", function() { return chart5; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart6", function() { return chart6; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart7", function() { return chart7; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart8", function() { return chart8; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart9", function() { return chart9; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart10", function() { return chart10; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart11", function() { return chart11; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart12", function() { return chart12; });
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_0__);

var seq = 0;
var delays = 80;
var durations = 500;
// Chart 1 Advanced SMIL Animations
var chart1 = {
    type: 'Line',
    data: {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
        series: [
            [12, 9, 7, 8, 5, 4, 6, 2, 3, 3, 4, 6],
            [4, 5, 3, 7, 3, 5, 5, 3, 4, 4, 5, 5],
            [5, 3, 4, 5, 6, 3, 3, 4, 5, 6, 3, 4],
            [3, 4, 5, 6, 7, 6, 4, 5, 6, 7, 6, 3]
        ]
    },
    options: {
        low: 0,
        showArea: false,
        fullWidth: true,
        height: 450,
    },
    events: {
        draw: function (data) {
            seq++;
            if (data.type === 'line') {
                data.element.animate({
                    opacity: {
                        begin: seq * delays + 1000,
                        dur: durations,
                        from: 0,
                        to: 1
                    }
                });
            }
            else if (data.type === 'label' && data.axis === 'x') {
                data.element.animate({
                    y: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.y + 100,
                        to: data.y,
                        easing: 'easeOutQuart'
                    }
                });
            }
            else if (data.type === 'label' && data.axis === 'y') {
                data.element.animate({
                    x: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 100,
                        to: data.x,
                        easing: 'easeOutQuart'
                    }
                });
            }
            else if (data.type === 'point') {
                data.element.animate({
                    x1: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 10,
                        to: data.x,
                        easing: 'easeOutQuart'
                    },
                    x2: {
                        begin: seq * delays,
                        dur: durations,
                        from: data.x - 10,
                        to: data.x,
                        easing: 'easeOutQuart'
                    },
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'easeOutQuart'
                    }
                });
            }
            else if (data.type === 'grid') {
                var pos1Animation = {
                    begin: seq * delays,
                    dur: durations,
                    from: data[data.axis.units.pos + '1'] - 30,
                    to: data[data.axis.units.pos + '1'],
                    easing: 'easeOutQuart'
                };
                var pos2Animation = {
                    begin: seq * delays,
                    dur: durations,
                    from: data[data.axis.units.pos + '2'] - 100,
                    to: data[data.axis.units.pos + '2'],
                    easing: 'easeOutQuart'
                };
                var animations = {};
                animations[data.axis.units.pos + '1'] = pos1Animation;
                animations[data.axis.units.pos + '2'] = pos2Animation;
                animations['opacity'] = {
                    begin: seq * delays,
                    dur: durations,
                    from: 0,
                    to: 1,
                    easing: 'easeOutQuart'
                };
                data.element.animate(animations);
            }
        }
    }
};
// Chart 2 SVG Path animation
var chart2 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [
            [1, 5, 2, 5, 4, 3],
            [2, 3, 4, 8, 1, 2],
            [5, 4, 3, 2, 1, 0.5]
        ]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        height: 450,
    },
    events: {
        draw: function (data) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 2000 * data.index,
                        dur: 2000,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: chartist__WEBPACK_IMPORTED_MODULE_0__["Svg"].Easing.easeOutQuint
                    }
                });
            }
        }
    }
};
// Chart 3 Animating a Donut with Svg.animate
var chart3 = {
    type: 'Pie',
    data: {
        series: [10, 20, 50, 20, 5, 50, 15],
        labels: [1, 2, 3, 4, 5, 6, 7]
    },
    options: {
        donut: true,
        showLabel: false,
        height: 450,
    },
    events: {
        draw: function (data) {
            if (data.type === 'slice') {
                var pathLength = data.element._node.getTotalLength();
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: chartist__WEBPACK_IMPORTED_MODULE_0__["Svg"].Easing.easeOutQuint,
                        fill: 'freeze'
                    }
                };
                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px'
                });
                data.element.animate(animationDefinition, false);
            }
        }
    }
};
// Chart 4 Bi-polar Line chart with area only
var chart4 = {
    type: 'Line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [
            [1, 2, 3, 1, -2, 0, 1, 0],
            [-2, -1, -2, -1, -2.5, -1, -2, -1],
            [0, 0, 0, 1, 2, 2.5, 2, 1],
            [2.5, 2, 1, 0.5, 1, 0.5, -1, -2.5]
        ]
    },
    options: {
        high: 3,
        low: -3,
        showArea: true,
        showLine: false,
        showPoint: false,
        fullWidth: true,
        axisX: {
            showLabel: false,
            showGrid: false
        },
        height: 450,
    }
};
// Chart 5 Line chart with area
var chart5 = {
    type: 'Line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [
            [5, 9, 7, 8, 5, 3, 5, 4]
        ]
    },
    options: {
        low: 0,
        showArea: true,
        height: 450,
    }
};
// Chart 6 Bi-polar bar chart
var chart6 = {
    type: 'Bar',
    data: {
        labels: ['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10'],
        series: [
            [1, 2, 4, 8, 6, -2, -1, -4, -6, -2]
        ]
    },
    options: {
        high: 10,
        low: -10,
        axisX: {
            labelInterpolationFnc: function (value, index) {
                return index % 2 === 0 ? value : null;
            }
        },
        height: 450,
    }
};
// Chart 7 Stacked bar chart
var chart7 = {
    type: 'Bar',
    data: {
        labels: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9', 'Q10', 'Q11', 'Q13', 'Q14'],
        series: [
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300],
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300],
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300]
        ]
    },
    options: {
        stackBars: true,
        axisY: {
            labelInterpolationFnc: function (value) {
                return (value / 1000) + 'k';
            }
        },
        height: 450,
    }
};
// Chart 8 Horizontal bar chart
var chart8 = {
    type: 'Bar',
    data: {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
        series: [
            [5, 4, 3, 7, 5, 10, 3],
            [3, 2, 9, 5, 4, 6, 4]
        ]
    },
    options: {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: true,
        axisY: {
            offset: 70
        },
        height: 450,
    }
};
// Chart 9 Extreme responsive configuration
var chart9 = {
    type: 'Bar',
    data: {
        labels: ['2010-11', '2011-12', '2012-13', '2013-13', '2014-15', '2015-16', '2016-17', '2017-18'],
        series: [
            [0.9, 0.4, 1.5, 4.9, 3, 3.8, 3.8, 1.9],
            [6.4, 5.7, 7, 4.95, 3, 3.8, 3.8, 1.9],
            [4.3, 2.3, 3.6, 4.5, 5, 2.8, 3.3, 4.3],
            [3.8, 4.1, 2.8, 1.8, 2.2, 1.9, 6.7, 2.9]
        ]
    },
    options: {
        height: 450,
        seriesBarDistance: 15,
        horizontalBars: false,
        axisY: {
            offset: 20
        }
    },
};
// Chart 10 Simple line chart
var chart10 = {
    type: 'Line',
    data: {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
        series: [
            [12, 9, 7, 8, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
        ]
    },
    options: {
        height: 450,
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    },
};
//Chart11: Holes in data
var chart11 = {
    type: 'Line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
        series: [
            [5, 5, 10, 8, 7, 5, 4, null, null, null, 10, 10, 7, 8, 6, 9],
            [10, 15, null, 12, null, 10, 12, 15, null, null, 12, null, 14, null, null, null],
            [null, null, null, null, 3, 4, 1, 3, 4, 6, 7, 9, 5, null, null, null]
            //	[{x:3, y: 3},{x: 4, y: 3}, {x: 5, y: undefined}, {x: 6, y: 4}, {x: 7, y: null}, {x: 8, y: 4}, {x: 9, y: 4}]
        ]
    },
    options: {
        height: 450,
        fullWidth: true,
        chartPadding: {
            right: 10
        },
        low: 0
    },
};
// // Chart 12 Filled holes in data
var chart12 = {
    type: 'Line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
        series: [
            [5, 5, 10, 8, 7, 5, 4, null, null, null, 10, 10, 7, 8, 6, 9],
            [10, 15, null, 12, null, 10, 12, 15, null, null, 12, null, 14, null, null, null],
            [null, null, null, null, 3, 4, 1, 3, 4, 6, 7, 9, 5, null, null, null]
            //	[{x:3, y: 3},{x: 4, y: 3}, {x: 5, y: undefined}, {x: 6, y: 4}, {x: 7, y: null}, {x: 8, y: 4}, {x: 9, y: 4}]
        ]
    },
    options: {
        height: 450,
        fullWidth: true,
        chartPadding: {
            right: 10
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            fillHoles: true,
        }),
        low: 0
    },
};


/***/ }),

/***/ "./src/app/shared/data/chart/chartjs.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/data/chart/chartjs.ts ***!
  \**********************************************/
/*! exports provided: barChartOptions, barChartLabels, barChartType, barChartLegend, barChartData, barChartColors, lineGraphOptions, lineGraphLabels, lineGraphType, lineGraphLegend, lineGraphData, lineGraphColors, radarGraphOptions, radarGraphLabels, radarGraphType, radarGraphLegend, radarGraphData, radarGraphColors, lineChartData, lineChartLabels, lineChartOptions, lineChartColors, lineChartLegend, lineChartType, doughnutChartLabels, doughnutChartData, doughnutChartColors, doughnutChartType, doughnutChartOptions, polarAreaChartLabels, polarAreaChartData, polarAreaLegend, ploarChartColors, polarAreaChartType, polarChartOptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartOptions", function() { return barChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartLabels", function() { return barChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartType", function() { return barChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartLegend", function() { return barChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartData", function() { return barChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartColors", function() { return barChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphOptions", function() { return lineGraphOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphLabels", function() { return lineGraphLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphType", function() { return lineGraphType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphLegend", function() { return lineGraphLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphData", function() { return lineGraphData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineGraphColors", function() { return lineGraphColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphOptions", function() { return radarGraphOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphLabels", function() { return radarGraphLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphType", function() { return radarGraphType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphLegend", function() { return radarGraphLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphData", function() { return radarGraphData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radarGraphColors", function() { return radarGraphColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartData", function() { return lineChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLabels", function() { return lineChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartOptions", function() { return lineChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartColors", function() { return lineChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLegend", function() { return lineChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartType", function() { return lineChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartLabels", function() { return doughnutChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartData", function() { return doughnutChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartColors", function() { return doughnutChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartType", function() { return doughnutChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartOptions", function() { return doughnutChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "polarAreaChartLabels", function() { return polarAreaChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "polarAreaChartData", function() { return polarAreaChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "polarAreaLegend", function() { return polarAreaLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ploarChartColors", function() { return ploarChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "polarAreaChartType", function() { return polarAreaChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "polarChartOptions", function() { return polarChartOptions; });
// barChart
var barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
};
var barChartLabels = ["January", "February", "March", "April", "May", "June", "July"];
var barChartType = 'bar';
var barChartLegend = false;
var barChartData = [
    { data: [35, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
];
var barChartColors = [
    {
        backgroundColor: '#4466f2',
        borderColor: "rgba(30, 166, 236, 0.8)",
        borderWidth: 1,
    },
    {
        backgroundColor: '#1ea6ec',
        borderColor: "rgba(68, 102, 242, 0.8)",
        borderWidth: 1,
    },
];
// LineGraph Chart
var lineGraphOptions = {
    scaleShowGridLines: true,
    scaleGridLineColor: "rgba(0,0,0,.05)",
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: true,
    bezierCurve: true,
    bezierCurveTension: 0.4,
    pointDot: true,
    pointDotRadius: 4,
    pointDotStrokeWidth: 1,
    pointHitDetectionRadius: 20,
    datasetStroke: true,
    datasetStrokeWidth: 2,
    datasetFill: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};
var lineGraphLabels = ["January", "February", "March", "April", "May", "June", "July"];
var lineGraphType = 'line';
var lineGraphLegend = false;
var lineGraphData = [
    { data: [10, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
];
var lineGraphColors = [
    {
        backgroundColor: 'rgba(68, 102, 242, 0.3)',
        borderColor: "#4466f2",
        borderWidth: 2,
    },
    {
        backgroundColor: 'rgba(30, 166, 236, 0.3)',
        borderColor: "#1ea6ec",
        borderWidth: 2,
    },
];
// RadarGraph Chart
var radarGraphOptions = {
    scaleShowGridLines: true,
    scaleGridLineColor: "rgba(0,0,0,.2)",
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: true,
    bezierCurve: true,
    bezierCurveTension: 0.4,
    pointDot: true,
    pointDotRadius: 3,
    pointDotStrokeWidth: 1,
    pointHitDetectionRadius: 20,
    datasetStroke: true,
    datasetStrokeWidth: 2,
    datasetFill: true,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};
var radarGraphLabels = ["Ford", "Chevy", "Toyota", "Honda", "Mazda"];
var radarGraphType = 'radar';
var radarGraphLegend = false;
var radarGraphData = [
    { data: [12, 3, 5, 18, 7] }
];
var radarGraphColors = [{
        backgroundColor: 'rgba(68, 102, 242, 0.4)',
        borderColor: "#4466f2",
        borderWidth: 2,
    }];
//line chart
var lineChartData = [
    { data: [10, 20, 40, 30, 0, 20, 10, 30, 10] },
    { data: [20, 40, 10, 20, 40, 30, 40, 10, 20] },
    { data: [60, 10, 40, 30, 80, 30, 20, 90] }
];
var lineChartLabels = ["", "10", "20", "30", "40", "50", "60", "70", "80"];
var lineChartOptions = {
    responsive: true,
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
};
var lineChartColors = [
    {
        backgroundColor: 'rgba(68, 102, 242, 0.3)',
        borderColor: "#4466f2",
        borderWidth: 2,
        lineTension: 0,
    },
    {
        backgroundColor: 'rgba(30, 166, 236, 0.3)',
        borderColor: "#1ea6ec",
        borderWidth: 2,
        lineTension: 0,
    },
    {
        backgroundColor: 'rgba(68, 102, 242, 0.4)',
        borderColor: "#4466f2",
        borderWidth: 2,
        lineTension: 0,
    }
];
var lineChartLegend = false;
var lineChartType = 'line';
// Doughnut
var doughnutChartLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
var doughnutChartData = [350, 450, 100];
var doughnutChartColors = [{ backgroundColor: ["#4466f2", "#1ea6ec", "#FF5370"] }];
var doughnutChartType = 'doughnut';
var doughnutChartOptions = {
    animation: false,
    responsive: true,
    maintainAspectRatio: false
};
// PolarArea
var polarAreaChartLabels = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
var polarAreaChartData = [300, 500, 100, 40, 120];
var polarAreaLegend = false;
var ploarChartColors = [{ backgroundColor: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#ff2046"] }];
var polarAreaChartType = 'polarArea';
var polarChartOptions = {
    animation: false,
    responsive: true,
    maintainAspectRatio: false
};


/***/ }),

/***/ "./src/app/shared/data/chart/config.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/data/chart/config.ts ***!
  \*********************************************/
/*! exports provided: barChartShowXAxis, barChartShowYAxis, barChartGradient, barChartShowLegend, barChartShowXAxisLabel, barChartXAxisLabel, barChartShowYAxisLabel, barChartYAxisLabel, roundEdges, barChartshowGridLines, barChartColorScheme, pieChartShowLabels, pieChartGradient, pieChartcolorScheme, chartOptions, areaChartshowXAxis, areaChartshowYAxis, areaChartgradient, areaChartshowXAxisLabel, areaChartxAxisLabel, areaChartshowYAxisLabel, areaChartyAxisLabel, areaChartcolorScheme, lineChartcurve, lineChartcurve1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartShowXAxis", function() { return barChartShowXAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartShowYAxis", function() { return barChartShowYAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartGradient", function() { return barChartGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartShowLegend", function() { return barChartShowLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartShowXAxisLabel", function() { return barChartShowXAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartXAxisLabel", function() { return barChartXAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartShowYAxisLabel", function() { return barChartShowYAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartYAxisLabel", function() { return barChartYAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "roundEdges", function() { return roundEdges; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartshowGridLines", function() { return barChartshowGridLines; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartColorScheme", function() { return barChartColorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChartShowLabels", function() { return pieChartShowLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChartGradient", function() { return pieChartGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChartcolorScheme", function() { return pieChartcolorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartOptions", function() { return chartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartshowXAxis", function() { return areaChartshowXAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartshowYAxis", function() { return areaChartshowYAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartgradient", function() { return areaChartgradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartshowXAxisLabel", function() { return areaChartshowXAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartxAxisLabel", function() { return areaChartxAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartshowYAxisLabel", function() { return areaChartshowYAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartyAxisLabel", function() { return areaChartyAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChartcolorScheme", function() { return areaChartcolorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartcurve", function() { return lineChartcurve; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartcurve1", function() { return lineChartcurve1; });
/* harmony import */ var d3_shape__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! d3-shape */ "./node_modules/d3-shape/src/index.js");

//BarChart
// options
var barChartShowXAxis = true;
var barChartShowYAxis = true;
var barChartGradient = true;
var barChartShowLegend = false;
var barChartShowXAxisLabel = true;
var barChartXAxisLabel = 'Country';
var barChartShowYAxisLabel = true;
var barChartYAxisLabel = 'Population';
var roundEdges = true;
var barChartshowGridLines = false;
var barChartColorScheme = {
    domain: ["#007bff", "#ff9f40", "#ff5370", "#1ea6ec"]
};
//Pie-Chart
//Options
var pieChartShowLabels = true;
var pieChartGradient = false;
var pieChartcolorScheme = {
    domain: ["#143fef", "#1ea6ec", "#1a8436", "#0062cc", "#ff850d", "#ff2046"]
};
var chartOptions = { responsive: true };
//Area Chart
var areaChartshowXAxis = true;
var areaChartshowYAxis = true;
var areaChartgradient = false;
var areaChartshowXAxisLabel = true;
var areaChartxAxisLabel = 'Country';
var areaChartshowYAxisLabel = true;
var areaChartyAxisLabel = 'Population';
var areaChartcolorScheme = {
    domain: ["#007bff", "#ff9f40", "#ff5370"]
};
var lineChartcurve = d3_shape__WEBPACK_IMPORTED_MODULE_0__["curveStep"];
var lineChartcurve1 = d3_shape__WEBPACK_IMPORTED_MODULE_0__["curveLinear"];


/***/ }),

/***/ "./src/app/shared/data/chart/google-chart.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/data/chart/google-chart.ts ***!
  \***************************************************/
/*! exports provided: pieChart1, pieChart2, pieChart3, pieChart4, areaChart1, areaChart2, columnChart1, columnChart2, barChart1, barChart2, lineChart, comboChart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart1", function() { return pieChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart2", function() { return pieChart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart3", function() { return pieChart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart4", function() { return pieChart4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChart1", function() { return areaChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "areaChart2", function() { return areaChart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "columnChart1", function() { return columnChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "columnChart2", function() { return columnChart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChart1", function() { return barChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChart2", function() { return barChart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChart", function() { return lineChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "comboChart", function() { return comboChart; });
// Pie Chart 1
var pieChart1 = {
    chartType: 'PieChart',
    dataTable: [
        ['Task', 'Hours per Day'],
        ['Work', 5],
        ['Eat', 10],
        ['Commute', 15],
        ['Watch TV', 20],
        ['Sleep', 25]
    ],
    options: {
        title: 'My Daily Activities',
        width: '100%',
        height: 400,
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#FF5370"],
        backgroundColor: 'transparent'
    },
};
// Pie Chart 2
var pieChart2 = {
    chartType: 'PieChart',
    dataTable: [
        ['Task', 'Hours per Day'],
        ['Work', 5],
        ['Eat', 10],
        ['Commute', 15],
        ['Watch TV', 20],
        ['Sleep', 25]
    ],
    options: {
        title: 'My Daily Activities',
        is3D: true,
        width: '100%',
        height: 400,
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#FF5370"],
        backgroundColor: 'transparent'
    },
};
// Pie Chart 3
var pieChart3 = {
    chartType: 'PieChart',
    dataTable: [
        ['Task', 'Hours per Day'],
        ['Work', 2],
        ['Eat', 2],
        ['Commute', 11],
        ['Watch TV', 2],
        ['Sleep', 7]
    ],
    options: {
        title: 'My Daily Activities',
        pieHole: 0.4,
        width: '100%',
        height: 400,
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#FF5370", "#FF5370"],
        backgroundColor: 'transparent'
    },
};
// Pie Chart 4
var pieChart4 = {
    chartType: 'PieChart',
    dataTable: [
        ['Language', 'Speakers (in millions)'],
        ['Assamese', 13],
        ['Bengali', 83],
        ['Bodo', 1.4],
        ['Dogri', 2.3],
        ['Gujarati', 46],
        ['Hindi', 300],
        ['Kannada', 38],
        ['Kashmiri', 5.5],
        ['Konkani', 5],
        ['Maithili', 20],
        ['Malayalam', 33],
        ['Manipuri', 1.5],
        ['Marathi', 72],
        ['Nepali', 2.9],
        ['Oriya', 33],
        ['Punjabi', 29],
        ['Sanskrit', 0.01],
        ['Santhali', 6.5],
        ['Sindhi', 2.5],
        ['Tamil', 61],
        ['Telugu', 74],
        ['Urdu', 52]
    ],
    options: {
        title: 'Indian Language Use',
        legend: 'none',
        width: '100%',
        height: 400,
        pieSliceText: 'label',
        slices: {
            4: { offset: 0.2 },
            12: { offset: 0.3 },
            14: { offset: 0.4 },
            15: { offset: 0.5 },
        },
        // colors: ["#ab8ce4", "#26c6da"]
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff", "#ff9f40", "#FF5370", "#4466f2", "#1ea6ec", "#22af47", "#007bff", "#ff9f40", "#FF5370", "#4466f2", "#1ea6ec", "#22af47", "#007bff", "#ff9f40", "#FF5370", "#fd7b6c", "#22af47", "#007bff", "#ff9f40"],
        backgroundColor: 'transparent'
    },
};
// Area Chart 1
var areaChart1 = {
    chartType: 'AreaChart',
    dataTable: [
        ['Year', 'Sales', 'Expenses'],
        ['2013', 1000, 400],
        ['2014', 1170, 460],
        ['2015', 660, 1120],
        ['2016', 1030, 540]
    ],
    options: {
        title: 'Company Performance',
        hAxis: { title: 'Year', titleTextStyle: { color: '#333' } },
        vAxis: { minValue: 0 },
        width: '100%',
        height: 400,
        colors: ["#4466f2", "#1ea6ec"],
        backgroundColor: 'transparent'
    },
};
// Area Chart 2
var areaChart2 = {
    chartType: 'AreaChart',
    dataTable: [
        ['Year', 'Cars', 'Trucks', 'Drones', 'Segways'],
        ['2013', 100, 400, 2000, 400],
        ['2014', 500, 700, 530, 800],
        ['2015', 2000, 1000, 620, 120],
        ['2016', 120, 201, 2501, 540]
    ],
    options: {
        title: 'Company Performance',
        hAxis: { title: 'Year', titleTextStyle: { color: '#333' } },
        vAxis: { minValue: 0 },
        width: '100%',
        height: 400,
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#007bff"],
        backgroundColor: 'transparent'
    },
};
// Column Chart 1
var columnChart1 = {
    chartType: 'ColumnChart',
    dataTable: [
        ["Year", "Sales", "Expenses", "Profit"],
        ["2014", 1e3, 400, 250],
        ["2015", 1170, 460, 300],
        ["2016", 660, 1120, 400],
        ["2017", 1030, 540, 450]
    ],
    options: {
        chart: {
            title: "Company Performance",
            subtitle: "Sales, Expenses, and Profit: 2014-2017"
        },
        bars: "vertical",
        vAxis: {
            format: "decimal"
        },
        height: 400,
        width: '100%',
        colors: ["#4466f2", "#1ea6ec", "#22af47"],
        backgroundColor: 'transparent'
    },
};
// Column-BarChart Chart 2
var columnChart2 = {
    chartType: 'BarChart',
    dataTable: [
        ["Year", "Sales", "Expenses", "Profit"],
        ["2014", 1e3, 400, 250],
        ["2015", 1170, 460, 300],
        ["2016", 660, 1120, 400],
        ["2017", 1030, 540, 450]
    ],
    options: {
        chart: {
            title: "Company Performance",
            subtitle: "Sales, Expenses, and Profit: 2014-2017"
        },
        bars: "horizontal",
        vAxis: {
            format: "decimal"
        },
        height: 400,
        width: '100%',
        colors: ["#4466f2", "#1ea6ec", "#22af47"],
        backgroundColor: 'transparent'
    },
};
// ColumnChart-BarChart Chart 1
var barChart1 = {
    chartType: 'ColumnChart',
    dataTable: [
        ["Element", "Density", { role: "style" }],
        ["Copper", 10, "#4466f2"],
        ["Silver", 12, "#1ea6ec"],
        ["Gold", 14, "#22af47"],
        ["Platinum", 16, "color: #007bff"]
    ],
    options: {
        title: "Density of Precious Metals, in g/cm^3",
        width: '100%',
        height: 400,
        bar: { groupWidth: "95%" },
        legend: { position: "none" },
        backgroundColor: 'transparent'
    },
};
// BarChart Chart 2
var barChart2 = {
    chartType: 'BarChart',
    dataTable: [
        ["Element", "Density", {
                role: "style"
            }],
        ["Copper", 10, "#4466f2"],
        ["Silver", 12, "#1ea6ec"],
        ["Gold", 14, "#22af47"],
        ["Platinum", 16, "color: #007bff"]
    ],
    options: {
        title: "Density of Precious Metals, in g/cm^3",
        width: '100%',
        height: 400,
        bar: { groupWidth: "95%" },
        legend: { position: "none" },
        backgroundColor: 'transparent'
    },
};
// Line Chart 
var lineChart = {
    chartType: 'LineChart',
    dataTable: [
        ['Month', 'Guardians of the Galaxy', 'The Avengers', 'Transformers: Age of Extinction'],
        [1, 37.8, 80.8, 41.8],
        [2, 30.9, 10.5, 32.4],
        [3, 40.4, 57, 25.7],
        [4, 11.7, 18.8, 10.5],
        [5, 20, 17.6, 10.4],
        [6, 8.8, 13.6, 7.7],
        [7, 7.6, 12.3, 9.6],
        [8, 12.3, 29.2, 10.6],
        [9, 16.9, 42.9, 14.8],
        [10, 12.8, 30.9, 11.6],
        [11, 5.3, 7.9, 4.7],
        [12, 6.6, 8.4, 5.2],
    ],
    options: {
        chart: {
            title: 'Box Office Earnings in First Two Weeks of Opening',
            subtitle: 'in millions of dollars (USD)'
        },
        colors: ["#4466f2", "#1ea6ec", "#22af47"],
        height: 500,
        width: '100%',
        backgroundColor: 'transparent'
    },
};
// Combo Chart
var comboChart = {
    chartType: 'ComboChart',
    dataTable: [
        ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
        ['2004/05', 165, 938, 522, 998, 450, 614.6],
        ['2005/06', 135, 1120, 599, 1268, 288, 682],
        ['2006/07', 157, 1167, 587, 807, 397, 623],
        ['2007/08', 139, 1110, 615, 968, 215, 609.4],
        ['2008/09', 136, 691, 629, 1026, 366, 569.6]
    ],
    options: {
        title: 'Monthly Coffee Production by Country',
        vAxis: { title: 'Cups' },
        hAxis: { title: 'Month' },
        seriesType: 'bars',
        series: { 5: { type: 'line' } },
        height: 500,
        fullWidth: true,
        colors: ["#4466f2", "#1ea6ec", "#22af47", "#FF5370", "#007bff"],
        backgroundColor: 'transparent'
    },
};


/***/ }),

/***/ "./src/app/shared/data/chart/ngx-chart.ts":
/*!************************************************!*\
  !*** ./src/app/shared/data/chart/ngx-chart.ts ***!
  \************************************************/
/*! exports provided: barChartSingle, single, pieChart, multiData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartSingle", function() { return barChartSingle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "single", function() { return single; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart", function() { return pieChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "multiData", function() { return multiData; });
var barChartSingle = [
    {
        "name": "Germany",
        "value": 894
    },
    {
        "name": "USA",
        "value": 500
    },
    {
        "name": "France",
        "value": 720
    },
    {
        "name": "Australia",
        "value": 650
    }
];
var single = [
    {
        "name": "Germany",
        "value": 8940000
    },
    {
        "name": "USA",
        "value": 5000000
    },
    {
        "name": "France",
        "value": 7200000
    }
];
var pieChart = [
    {
        name: "Germany",
        value: 8940000
    },
    {
        name: "USA",
        value: 6523140
    },
    {
        name: "France",
        value: 7200000
    },
    {
        name: "India",
        value: 5458796
    },
    {
        name: "NZ",
        value: 6145687
    },
    {
        name: "UK",
        value: 5234567
    }
];
var multiData = [
    {
        "name": "Germany",
        "series": [
            {
                "name": "2010",
                "value": 200
            },
            {
                "name": "2011",
                "value": 500
            },
            {
                "name": "2012",
                "value": 275
            },
            {
                "name": "2013",
                "value": 1000
            },
            {
                "name": "2014",
                "value": 650
            },
            {
                "name": "2015",
                "value": 1900
            },
            {
                "name": "2016",
                "value": 860
            }
        ]
    },
    {
        "name": "USA",
        "series": [
            {
                "name": "2010",
                "value": 100
            },
            {
                "name": "2011",
                "value": 300
            },
            {
                "name": "2012",
                "value": 1350
            },
            {
                "name": "2013",
                "value": 650
            },
            {
                "name": "2014",
                "value": 250
            },
            {
                "name": "2015",
                "value": 775
            },
            {
                "name": "2016",
                "value": 730
            }
        ]
    },
    {
        "name": "France",
        "series": [
            {
                "name": "2010",
                "value": 660
            },
            {
                "name": "2011",
                "value": 900
            },
            {
                "name": "2012",
                "value": 680
            },
            {
                "name": "2013",
                "value": 675
            },
            {
                "name": "2014",
                "value": 1500
            },
            {
                "name": "2015",
                "value": 680
            },
            {
                "name": "2016",
                "value": 690
            }
        ]
    }
];


/***/ })

}]);
//# sourceMappingURL=components-charts-charts-module.js.map