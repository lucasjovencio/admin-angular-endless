(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-learning-learning-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-detail/learning-detail.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/learning/learning-detail/learning-detail.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-9 xl-60\">\r\n      <div class=\"blog-single\">\r\n        <div class=\"blog-box blog-details\"><img class=\"img-fluid w-100\" src=\"assets/images/faq/learning-1.png\"\r\n            alt=\"blog-main\">\r\n          <div class=\"blog-details\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">{{arr.date}}</li>\r\n              <li><i class=\"icofont icofont-user\"></i>Mark <span>Jecno </span></li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>{{arr.hits}}<span>Hits</span></li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>{{arr.comments}} Comments</li>\r\n            </ul>\r\n            <h4>\r\n              {{arr.des1}}\r\n            </h4>\r\n            <div class=\"single-blog-content-top\" [innerHTML]=\"arr.des2\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <section class=\"comment-box\">\r\n          <h4>Comment</h4>\r\n          <hr>\r\n          <ul>\r\n            <li>\r\n              <div class=\"media align-self-center\"><img class=\"align-self-center\" [src]='arr.student_img'\r\n                  alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4 xl-100\">\r\n                      <h6 class=\"mt-0\">{{arr.student_name}}<span> ( {{arr.student_designation}} )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8 xl-100\">\r\n                      <ul class=\"comment-social float-left float-md-right learning-comment\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>{{arr.hits}} Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>{{arr.comments}} Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>{{arr.student_comment}}</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li>\r\n              <ul>\r\n                <li>\r\n                  <div class=\"media\"><img class=\"align-self-center\" [src]='arr.writer_img'\r\n                      alt=\"Generic placeholder image\">\r\n                    <div class=\"media-body\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-xl-12\">\r\n                          <h6 class=\"mt-0\">{{arr.writer_name}}<span> ( {{arr.student_designation}} )</span></h6>\r\n                        </div>\r\n                      </div>\r\n                      <p>{{arr.writer_reply}}</p>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n              </ul>\r\n            </li>\r\n            <li>\r\n              <div class=\"media\"><img class=\"align-self-center\" [src]='arr.student_img2'\r\n                  alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4 xl-100\">\r\n                      <h6 class=\"mt-0\">{{arr.student_name}}<span> ( {{arr.student_designation}} )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8 xl-100\">\r\n                      <ul class=\"comment-social float-left float-md-right learning-comment\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>{{arr.hits}} Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>{{arr.comments}} Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>{{arr.student_comment}}</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </section>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-40\">\r\n      <app-learning-filter></app-learning-filter>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-filter/learning-filter.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/learning/learning-filter/learning-filter.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"default-according style-1 faq-accordion job-accordion\" id=\"accordionoc\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5 class=\"mb-0\">\r\n            <button class=\"btn btn-link pl-0\" (click)=\"isCourse = !isCourse\" [attr.aria-expanded]=\"!isCourse\"\r\n              aria-controls=\"collapse\">Find Course</button>\r\n          </h5>\r\n        </div>\r\n        <div class=\"collapse show\" [ngbCollapse]=\"isCourse\">\r\n          <div class=\"card-body filter-cards-view animate-chk\">\r\n            <div class=\"job-filter\">\r\n              <div class=\"faq-form\">\r\n                <input class=\"form-control\" type=\"text\" placeholder=\"Search..\">\r\n                <app-feather-icons class=\"search-icon\" [icon]=\"'search'\"></app-feather-icons>\r\n              </div>\r\n            </div>\r\n            <div class=\"checkbox-animated\">\r\n              <div class=\"learning-header\"><span class=\"f-w-600\">Categories</span></div>\r\n              <label class=\"d-block\" for=\"chk-ani\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani\" type=\"checkbox\"> Accounting\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani1\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani1\" type=\"checkbox\"> Design\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani2\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani2\" type=\"checkbox\"> Development\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani3\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani3\" type=\"checkbox\"> Management\r\n              </label>\r\n            </div>\r\n            <div class=\"checkbox-animated mt-0\">\r\n              <div class=\"learning-header\"><span class=\"f-w-600\">Duration</span></div>\r\n              <label class=\"d-block\" for=\"chk-ani4\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani4\" type=\"checkbox\"> 0-50 hours\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani5\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani5\" type=\"checkbox\"> 50-100 hours\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani6\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani6\" type=\"checkbox\"> 100+ hours\r\n              </label>\r\n            </div>\r\n            <div class=\"checkbox-animated mt-0\">\r\n              <div class=\"learning-header\"><span class=\"f-w-600\">Price</span></div>\r\n              <label class=\"d-block\" for=\"edo-ani\">\r\n                <input class=\"radio_animated\" id=\"edo-ani\" type=\"radio\" name=\"rdo-ani\" checked=\"\"> All Courses\r\n              </label>\r\n              <label class=\"d-block\" for=\"edo-ani1\">\r\n                <input class=\"radio_animated\" id=\"edo-ani1\" type=\"radio\" name=\"rdo-ani\" checked=\"\"> Paid Courses\r\n              </label>\r\n              <label class=\"d-block\" for=\"edo-ani2\">\r\n                <input class=\"radio_animated\" id=\"edo-ani2\" type=\"radio\" name=\"rdo-ani\" checked=\"\"> Free Courses\r\n              </label>\r\n            </div>\r\n            <div class=\"checkbox-animated mt-0\">\r\n              <div class=\"learning-header\"><span class=\"f-w-600\">Status</span></div>\r\n              <label class=\"d-block\" for=\"chk-ani7\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani7\" type=\"checkbox\"> Registration\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani8\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani8\" type=\"checkbox\"> Progress\r\n              </label>\r\n              <label class=\"d-block\" for=\"chk-ani9\">\r\n                <input class=\"checkbox_animated\" id=\"chk-ani9\" type=\"checkbox\"> Completed\r\n              </label>\r\n            </div>\r\n            <button class=\"btn btn-primary text-center\" type=\"button\">Filter</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5 class=\"mb-0\">\r\n            <button class=\"btn btn-link pl-0\" (click)=\"isCategories = !isCategories\"\r\n              [attr.aria-expanded]=\"!isCategories\" aria-controls=\"collapse\">Categories</button>\r\n          </h5>\r\n        </div>\r\n        <div class=\"collapse show\" id=\"collapseicon1\" [ngbCollapse]=\"isCategories\">\r\n          <div class=\"categories\">\r\n            <div class=\"learning-header\"><span class=\"f-w-600\">Design</span></div>\r\n            <ul>\r\n              <li><a  href=\"javascript:void(0)\">Graphic Design</a><span class=\"badge badge-primary pull-right\">20</span></li>\r\n              <li><a  href=\"javascript:void(0)\">Web Design</a><span class=\"badge badge-primary pull-right\">42</span></li>\r\n              <li><a  href=\"javascript:void(0)\">Interface Design</a><span class=\"badge badge-primary pull-right\">15</span></li>\r\n              <li><a  href=\"javascript:void(0)\">User Experience</a><span class=\"badge badge-primary pull-right\">63</span></li>\r\n            </ul>\r\n          </div>\r\n          <div class=\"categories pt-0\">\r\n            <div class=\"learning-header\"><span class=\"f-w-600\">Development</span></div>\r\n            <ul>\r\n              <li><a  href=\"javascript:void(0)\">Frontend Development</a><span class=\"badge badge-primary pull-right\">74</span></li>\r\n              <li><a  href=\"javascript:void(0)\">Backend Development</a><span class=\"badge badge-primary pull-right\">53</span></li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5 class=\"mb-0\">\r\n            <button class=\"btn btn-link pl-0\" (click)=\"isUpcomimngCourse = !isUpcomimngCourse\"\r\n              [attr.aria-expanded]=\"!isUpcomimngCourse\" aria-controls=\"collapse\">Upcoming Courses</button>\r\n          </h5>\r\n        </div>\r\n        <div class=\"collapse show\" [ngbCollapse]=\"isUpcomimngCourse\">\r\n          <div class=\"upcoming-course card-body\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body\"><span class=\"f-w-600\">Business Development</span><span class=\"d-block\">Course By\r\n                  <a  href=\"javascript:void(0)\">Badi Zaman</a></span><span class=\"d-block\"><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star-half-o font-warning\"></i></span></div>\r\n              <div>\r\n                <h5 class=\"mb-0 font-primary\">09</h5><span class=\"d-block\">Nov</span>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\"><span class=\"f-w-600\">Web design strategy</span><span class=\"d-block\">Course By <a\r\n                     href=\"javascript:void(0)\">Zaky Hanania</a></span><span class=\"d-block\"><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star font-warning\"></i></span></div>\r\n              <div>\r\n                <h5 class=\"mb-0 font-primary\">18</h5><span class=\"d-block\">Dec</span>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\"><span class=\"f-w-600\">User experience design</span><span class=\"d-block\">Course By\r\n                  <a  href=\"javascript:void(0)\">Majida Handal</a></span><span class=\"d-block\"><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star font-warning\"></i><i\r\n                    class=\"fa fa-star font-warning\"></i><i class=\"fa fa-star-o font-warning\"></i></span></div>\r\n              <div>\r\n                <h5 class=\"mb-0 font-primary\">24</h5><span class=\"d-block\">Dec</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-list/learning-list.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/learning/learning-list/learning-list.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-9 xl-60\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-4 xl-50 col-sm-6\" *ngFor=\"let course of courses\">\r\n          <div class=\"card\">\r\n            <div class=\"blog-box blog-grid text-center product-box\">\r\n              <div class=\"product-img\"><img class=\"img-fluid top-radius-blog\" [src]='course.img' alt=\"\">\r\n                <div class=\"product-hover\">\r\n                  <ul>\r\n                    <li><i class=\"icon-link\" (click)=\"detailview(course)\"></i></li>\r\n                    <li><i class=\"icon-import\"></i></li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n              <div class=\"blog-details-main\">\r\n                <ul class=\"blog-social\">\r\n                  <li class=\"digits\">{{ course.date }}</li>\r\n                  <li class=\"digits\">by:{{ course.writer}}</li>\r\n                  <li class=\"digits\">{{course.hits}} Hits</li>\r\n                </ul>\r\n                <hr>\r\n                <h6 class=\"blog-bottom-details\">{{course.short_description}}</h6>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-40\">\r\n      <app-learning-filter></app-learning-filter>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/learning/learning-detail/learning-detail.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/components/learning/learning-detail/learning-detail.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGVhcm5pbmcvbGVhcm5pbmctZGV0YWlsL2xlYXJuaW5nLWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/learning/learning-detail/learning-detail.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/learning/learning-detail/learning-detail.component.ts ***!
  \**********************************************************************************/
/*! exports provided: LearningDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningDetailComponent", function() { return LearningDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_data_learning_learning__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/data/learning/learning */ "./src/app/shared/data/learning/learning.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LearningDetailComponent = /** @class */ (function () {
    function LearningDetailComponent(route, router) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.courses = _shared_data_learning_learning__WEBPACK_IMPORTED_MODULE_2__["LearningDB"].lang;
        this.route.params.subscribe(function (params) {
            var id = +params['id'];
            _this.courses.filter(function (items) {
                if (items.Id === id) {
                    _this.arr = items;
                }
            });
        });
    }
    LearningDetailComponent.prototype.ngOnInit = function () { };
    LearningDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-learning-detail',
            template: __webpack_require__(/*! raw-loader!./learning-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-detail/learning-detail.component.html"),
            styles: [__webpack_require__(/*! ./learning-detail.component.scss */ "./src/app/components/learning/learning-detail/learning-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LearningDetailComponent);
    return LearningDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/learning/learning-filter/learning-filter.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/components/learning/learning-filter/learning-filter.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGVhcm5pbmcvbGVhcm5pbmctZmlsdGVyL2xlYXJuaW5nLWZpbHRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/learning/learning-filter/learning-filter.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/components/learning/learning-filter/learning-filter.component.ts ***!
  \**********************************************************************************/
/*! exports provided: LearningFilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningFilterComponent", function() { return LearningFilterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LearningFilterComponent = /** @class */ (function () {
    function LearningFilterComponent() {
        this.isCourse = false;
        this.isIndustry = false;
        this.isUpcomimngCourse = false;
    }
    LearningFilterComponent.prototype.ngOnInit = function () { };
    LearningFilterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-learning-filter',
            template: __webpack_require__(/*! raw-loader!./learning-filter.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-filter/learning-filter.component.html"),
            styles: [__webpack_require__(/*! ./learning-filter.component.scss */ "./src/app/components/learning/learning-filter/learning-filter.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LearningFilterComponent);
    return LearningFilterComponent;
}());



/***/ }),

/***/ "./src/app/components/learning/learning-list/learning-list.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/components/learning/learning-list/learning-list.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGVhcm5pbmcvbGVhcm5pbmctbGlzdC9sZWFybmluZy1saXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/learning/learning-list/learning-list.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/components/learning/learning-list/learning-list.component.ts ***!
  \******************************************************************************/
/*! exports provided: LearningListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningListComponent", function() { return LearningListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_data_learning_learning__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/data/learning/learning */ "./src/app/shared/data/learning/learning.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LearningListComponent = /** @class */ (function () {
    function LearningListComponent(route, router) {
        this.route = route;
        this.router = router;
        this.courses = [];
        this.courses = _shared_data_learning_learning__WEBPACK_IMPORTED_MODULE_2__["LearningDB"].lang;
    }
    LearningListComponent.prototype.detailview = function (course) {
        this.router.navigate(['/learning/learning-detail', course.Id]);
    };
    LearningListComponent.prototype.ngOnInit = function () { };
    LearningListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-learning-list',
            template: __webpack_require__(/*! raw-loader!./learning-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/learning/learning-list/learning-list.component.html"),
            styles: [__webpack_require__(/*! ./learning-list.component.scss */ "./src/app/components/learning/learning-list/learning-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LearningListComponent);
    return LearningListComponent;
}());



/***/ }),

/***/ "./src/app/components/learning/learning-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/learning/learning-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: LearningRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningRoutingModule", function() { return LearningRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _learning_detail_learning_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./learning-detail/learning-detail.component */ "./src/app/components/learning/learning-detail/learning-detail.component.ts");
/* harmony import */ var _learning_list_learning_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./learning-list/learning-list.component */ "./src/app/components/learning/learning-list/learning-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        children: [
            {
                path: 'learninglist',
                component: _learning_list_learning_list_component__WEBPACK_IMPORTED_MODULE_3__["LearningListComponent"],
                data: {
                    title: "Learning List",
                    breadcrumb: "learning"
                }
            },
            {
                path: 'learning-detail/:id',
                component: _learning_detail_learning_detail_component__WEBPACK_IMPORTED_MODULE_2__["LearningDetailComponent"],
                data: {
                    title: "Detail Course",
                    breadcrumb: "Detail Course"
                }
            }
        ]
    }
];
var LearningRoutingModule = /** @class */ (function () {
    function LearningRoutingModule() {
    }
    LearningRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LearningRoutingModule);
    return LearningRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/learning/learning.module.ts":
/*!********************************************************!*\
  !*** ./src/app/components/learning/learning.module.ts ***!
  \********************************************************/
/*! exports provided: LearningModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningModule", function() { return LearningModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _learning_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./learning-routing.module */ "./src/app/components/learning/learning-routing.module.ts");
/* harmony import */ var _learning_filter_learning_filter_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./learning-filter/learning-filter.component */ "./src/app/components/learning/learning-filter/learning-filter.component.ts");
/* harmony import */ var _learning_detail_learning_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./learning-detail/learning-detail.component */ "./src/app/components/learning/learning-detail/learning-detail.component.ts");
/* harmony import */ var _learning_list_learning_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./learning-list/learning-list.component */ "./src/app/components/learning/learning-list/learning-list.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var LearningModule = /** @class */ (function () {
    function LearningModule() {
    }
    LearningModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_learning_filter_learning_filter_component__WEBPACK_IMPORTED_MODULE_4__["LearningFilterComponent"], _learning_detail_learning_detail_component__WEBPACK_IMPORTED_MODULE_5__["LearningDetailComponent"], _learning_list_learning_list_component__WEBPACK_IMPORTED_MODULE_6__["LearningListComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _learning_routing_module__WEBPACK_IMPORTED_MODULE_3__["LearningRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
            ]
        })
    ], LearningModule);
    return LearningModule;
}());



/***/ }),

/***/ "./src/app/shared/data/learning/learning.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/data/learning/learning.ts ***!
  \**************************************************/
/*! exports provided: LearningDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearningDB", function() { return LearningDB; });
var LearningDB = /** @class */ (function () {
    function LearningDB() {
    }
    LearningDB.lang = [
        {
            Id: 1,
            img: 'assets/images/faq/1.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 2,
            img: 'assets/images/faq/2.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 3,
            img: 'assets/images/faq/3.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 4,
            img: 'assets/images/faq/3.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 5,
            img: 'assets/images/faq/1.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 6,
            img: 'assets/images/faq/2.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 7,
            img: 'assets/images/faq/2.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 8,
            img: 'assets/images/faq/1.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 9,
            img: 'assets/images/faq/3.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 10,
            img: 'assets/images/faq/1.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 11,
            img: 'assets/images/faq/2.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        },
        {
            Id: 12,
            img: 'assets/images/faq/3.jpg',
            date: '15 April 2019',
            writer: 'Admin',
            hits: '0',
            short_description: [
                'Perspiciatis unde omnis iste natus error sit.Dummy text'
            ],
            comments: 598,
            des1: ['All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.'],
            des2: ["<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>"],
            student_name: 'Jolio Mark',
            writer_name: 'Molina Mark',
            student_designation: 'Designer',
            student_comment: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            writer_reply: ['There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text.'],
            student_img: 'assets/images/blog/comment.jpg',
            writer_img: 'assets/images/blog/9.jpg',
            student_img2: 'assets/images/blog/4.jpg'
        }
    ];
    return LearningDB;
}());



/***/ })

}]);
//# sourceMappingURL=components-learning-learning-module.js.map