(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~components-editor-editor-module~components-email-email-module"],{

/***/ "./node_modules/ngx-ckeditor/fesm5/ngx-ckeditor.js":
/*!*********************************************************!*\
  !*** ./node_modules/ngx-ckeditor/fesm5/ngx-ckeditor.js ***!
  \*********************************************************/
/*! exports provided: CKEditorComponent, CKEditorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CKEditorComponent", function() { return CKEditorComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CKEditorModule", function() { return CKEditorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var defaults = {
    contentsCss: [''],
    customConfig: ''
};
var CKEditorComponent = /** @class */ (function () {
    function CKEditorComponent(ngZone, hostEl) {
        this.ngZone = ngZone;
        this.hostEl = hostEl;
        this.innerValue = '';
        this.disabled = false;
        this.editorInitialized = false;
        /**
         * Is readonly mode, default:false
         */
        this.readonly = false;
        /**
         * The ck-editor config object.
         */
        this.config = {};
        /**
         * The special skin, default: moono-lisa
         */
        this.skin = 'moono-lisa';
        /**
         * The special language, default: en
         */
        this.language = 'en';
        /**
         * Use fullpage mode, default:false
         */
        this.fullPage = false;
        /**
         * Use inline mode, default: false
         */
        this.inline = false;
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.ready = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.blur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.focus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onChange = function (value) { };
        this.onTouched = function () { };
        this.identifier = CKEditorComponent.getRandomIdentifier(this.id);
    }
    /**
     * @private
     * @param {?=} id
     * @return {?}
     */
    CKEditorComponent.getRandomIdentifier = /**
     * @private
     * @param {?=} id
     * @return {?}
     */
    function (id) {
        if (id === void 0) { id = ''; }
        return 'editor-' + (id !== '' ? id : String(CKEditorComponent.idx++));
    };
    Object.defineProperty(CKEditorComponent.prototype, "instance", {
        get: /**
         * @return {?}
         */
        function () {
            return this.ckIns;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} changes
     * @return {?}
     */
    CKEditorComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (this.editorInitialized) {
            this.destroyEditor();
            this.initEditor(this.identifier);
        }
    };
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngAfterViewChecked = /**
     * @return {?}
     */
    function () {
        if (!this.editorInitialized && this.documentContains(this.textareaRef.nativeElement)) {
            this.editorInitialized = true;
            this.initEditor(this.identifier);
        }
        else if (this.editorInitialized && !this.documentContains(this.textareaRef.nativeElement)) {
            this.editorInitialized = false;
            this.destroyEditor();
        }
    };
    /**
     * @return {?}
     */
    CKEditorComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.destroyEditor();
    };
    /**
     * @private
     * @param {?} identifier
     * @return {?}
     */
    CKEditorComponent.prototype.initEditor = /**
     * @private
     * @param {?} identifier
     * @return {?}
     */
    function (identifier) {
        var _this = this;
        if (typeof CKEDITOR === 'undefined') {
            return console.warn('CKEditor 4.x is missing (http://ckeditor.com/)');
        }
        /** @type {?} */
        var textareaEl = this.textareaRef.nativeElement;
        this.identifier = identifier;
        textareaEl.setAttribute('name', this.identifier);
        if (this.ckIns || !this.documentContains(this.textareaRef.nativeElement)) {
            return;
        }
        /** @type {?} */
        var opt = Object.assign({}, defaults, this.config, {
            readOnly: this.readonly,
            skin: this.skin,
            language: this.language,
            fullPage: this.fullPage,
            inline: this.inline
        });
        this.ckIns = this.inline ? CKEDITOR.inline(textareaEl, opt) : CKEDITOR.replace(textareaEl, opt);
        this.ckIns.setData(this.innerValue);
        this.ckIns.on('change', function () {
            /** @type {?} */
            var val = _this.ckIns.getData();
            _this.updateValue(val);
        });
        this.ckIns.on('instanceReady', function (evt) {
            _this.ngZone.run(function () {
                _this.ready.emit(evt);
            });
        });
        this.ckIns.on('blur', function (evt) {
            _this.ngZone.run(function () {
                _this.blur.emit(evt);
                _this.onTouched();
            });
        });
        this.ckIns.on('focus', function (evt) {
            _this.ngZone.run(function () {
                _this.focus.emit(evt);
            });
        });
    };
    /**
     * @private
     * @return {?}
     */
    CKEditorComponent.prototype.destroyEditor = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.ckIns) {
            // If use destroy, will fire 'Error code: editor-destroy-iframe'
            // this.ckIns.destroy();
            if (CKEDITOR.instances.hasOwnProperty(this.ckIns.name)) {
                CKEDITOR.remove(CKEDITOR.instances[this.ckIns.name]);
            }
            this.ckIns = null;
            /** @type {?} */
            var editorEl = this.hostEl.nativeElement.querySelector('#cke_' + this.identifier);
            if (editorEl != null && editorEl.parentElement) {
                editorEl.parentElement.removeChild(editorEl);
            }
        }
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    CKEditorComponent.prototype.updateValue = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        this.ngZone.run(function () {
            _this.innerValue = value;
            _this.onChange(value);
            _this.onTouched();
            _this.change.emit(value);
        });
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    CKEditorComponent.prototype.documentContains = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        return document.contains ? document.contains(node) : document.body.contains(node);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CKEditorComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.innerValue = value || '';
        if (this.ckIns) {
            // Fix bug that can't emit change event when set non-html tag value twice in fullpage mode.
            this.ckIns.setData(this.innerValue);
            /** @type {?} */
            var val = this.ckIns.getData();
            this.ckIns.setData(val);
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    CKEditorComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    CKEditorComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        this.disabled = isDisabled;
    };
    CKEditorComponent.idx = 1;
    CKEditorComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'ck-editor',
                    template: "\n    <textarea #textarea></textarea>\n  ",
                    providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return CKEditorComponent; }), multi: true }],
                    exportAs: 'ckEditor'
                },] },
    ];
    /** @nocollapse */
    CKEditorComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }
    ]; };
    CKEditorComponent.propDecorators = {
        readonly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        config: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        skin: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        language: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        fullPage: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        inline: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        change: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        ready: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        blur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        focus: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        textareaRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['textarea',] }]
    };
    return CKEditorComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CKEditorModule = /** @class */ (function () {
    function CKEditorModule() {
    }
    CKEditorModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [],
                    exports: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], CKEditorComponent],
                    declarations: [CKEditorComponent],
                    providers: []
                },] },
    ];
    return CKEditorModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */



//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNrZWRpdG9yLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gtY2tlZGl0b3IvbGliL2NrLWVkaXRvci5jb21wb25lbnQudHMiLCJuZzovL25neC1ja2VkaXRvci9saWIvY2stZWRpdG9yLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIEFmdGVyVmlld0luaXQsXHJcbiAgQWZ0ZXJWaWV3Q2hlY2tlZCxcclxuICBDb21wb25lbnQsXHJcbiAgRWxlbWVudFJlZixcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgSW5wdXQsXHJcbiAgTmdab25lLFxyXG4gIE9uQ2hhbmdlcyxcclxuICBPbkRlc3Ryb3ksXHJcbiAgT25Jbml0LFxyXG4gIE91dHB1dCxcclxuICBTaW1wbGVDaGFuZ2VzLFxyXG4gIFZpZXdDaGlsZCxcclxuICBmb3J3YXJkUmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmRlY2xhcmUgdmFyIENLRURJVE9SOiBhbnk7XHJcblxyXG5jb25zdCBkZWZhdWx0cyA9IHtcclxuICBjb250ZW50c0NzczogWycnXSxcclxuICBjdXN0b21Db25maWc6ICcnXHJcbn07XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2NrLWVkaXRvcicsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDx0ZXh0YXJlYSAjdGV4dGFyZWE+PC90ZXh0YXJlYT5cclxuICBgLFxyXG4gIHByb3ZpZGVyczogW3sgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IENLRWRpdG9yQ29tcG9uZW50KSwgbXVsdGk6IHRydWUgfV0sXHJcbiAgZXhwb3J0QXM6ICdja0VkaXRvcidcclxufSlcclxuZXhwb3J0IGNsYXNzIENLRWRpdG9yQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcywgQWZ0ZXJWaWV3Q2hlY2tlZCwgQ29udHJvbFZhbHVlQWNjZXNzb3Ige1xyXG4gIHByaXZhdGUgY2tJbnM6IGFueTtcclxuICBwcml2YXRlIGlubmVyVmFsdWUgPSAnJztcclxuICBwcml2YXRlIGlkZW50aWZpZXI6IHN0cmluZztcclxuICBwcml2YXRlIGRpc2FibGVkID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSBlZGl0b3JJbml0aWFsaXplZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBJcyByZWFkb25seSBtb2RlLCBkZWZhdWx0OmZhbHNlXHJcbiAgICovXHJcbiAgQElucHV0KCkgcHVibGljIHJlYWRvbmx5ID0gZmFsc2U7XHJcbiAgLyoqXHJcbiAgICogVGhlIGNrLWVkaXRvciBjb25maWcgb2JqZWN0LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBjb25maWc6IGFueSA9IHt9O1xyXG4gIC8qKlxyXG4gICAqIFRoZSBzcGVjaWFsIHNraW4sIGRlZmF1bHQ6IG1vb25vLWxpc2FcclxuICAgKi9cclxuICBASW5wdXQoKSBwdWJsaWMgc2tpbiA9ICdtb29uby1saXNhJztcclxuICAvKipcclxuICAgKiBUaGUgc3BlY2lhbCBsYW5ndWFnZSwgZGVmYXVsdDogZW5cclxuICAgKi9cclxuICBASW5wdXQoKSBwdWJsaWMgbGFuZ3VhZ2UgPSAnZW4nO1xyXG4gIC8qKlxyXG4gICAqIFVzZSBmdWxscGFnZSBtb2RlLCBkZWZhdWx0OmZhbHNlXHJcbiAgICovXHJcbiAgQElucHV0KCkgcHVibGljIGZ1bGxQYWdlID0gZmFsc2U7XHJcbiAgLyoqXHJcbiAgICogVXNlIGlubGluZSBtb2RlLCBkZWZhdWx0OiBmYWxzZVxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBpbmxpbmUgPSBmYWxzZTtcclxuICAvKipcclxuICAgKiBUaGUgZWRpdG9yIGlkXHJcbiAgICovXHJcbiAgQElucHV0KCkgcHVibGljIGlkOiBzdHJpbmc7XHJcblxyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgcmVhZHkgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBibHVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgZm9jdXMgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3RleHRhcmVhJykgcHVibGljIHRleHRhcmVhUmVmOiBFbGVtZW50UmVmO1xyXG5cclxuICBwcml2YXRlIHN0YXRpYyBpZHggPSAxO1xyXG4gIHByaXZhdGUgc3RhdGljIGdldFJhbmRvbUlkZW50aWZpZXIoaWQ6IHN0cmluZyA9ICcnKSB7XHJcbiAgICByZXR1cm4gJ2VkaXRvci0nICsgKGlkICE9PSAnJyA/IGlkIDogU3RyaW5nKENLRWRpdG9yQ29tcG9uZW50LmlkeCsrKSk7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZSA9ICh2YWx1ZTogc3RyaW5nKSA9PiB7fTtcclxuICBvblRvdWNoZWQgPSAoKSA9PiB7fTtcclxuXHJcbiAgcHVibGljIGdldCBpbnN0YW5jZSgpIHtcclxuICAgIHJldHVybiB0aGlzLmNrSW5zO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSwgcHJpdmF0ZSBob3N0RWw6IEVsZW1lbnRSZWYpIHtcclxuICAgIHRoaXMuaWRlbnRpZmllciA9IENLRWRpdG9yQ29tcG9uZW50LmdldFJhbmRvbUlkZW50aWZpZXIodGhpcy5pZCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHt9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmVkaXRvckluaXRpYWxpemVkKSB7XHJcbiAgICAgIHRoaXMuZGVzdHJveUVkaXRvcigpO1xyXG4gICAgICB0aGlzLmluaXRFZGl0b3IodGhpcy5pZGVudGlmaWVyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3Q2hlY2tlZCgpIHtcclxuICAgIGlmICghdGhpcy5lZGl0b3JJbml0aWFsaXplZCAmJiB0aGlzLmRvY3VtZW50Q29udGFpbnModGhpcy50ZXh0YXJlYVJlZi5uYXRpdmVFbGVtZW50KSkge1xyXG4gICAgICB0aGlzLmVkaXRvckluaXRpYWxpemVkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5pbml0RWRpdG9yKHRoaXMuaWRlbnRpZmllcik7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZWRpdG9ySW5pdGlhbGl6ZWQgJiYgIXRoaXMuZG9jdW1lbnRDb250YWlucyh0aGlzLnRleHRhcmVhUmVmLm5hdGl2ZUVsZW1lbnQpKSB7XHJcbiAgICAgIHRoaXMuZWRpdG9ySW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5kZXN0cm95RWRpdG9yKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuZGVzdHJveUVkaXRvcigpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpbml0RWRpdG9yKGlkZW50aWZpZXI6IHN0cmluZykge1xyXG4gICAgaWYgKHR5cGVvZiBDS0VESVRPUiA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgcmV0dXJuIGNvbnNvbGUud2FybignQ0tFZGl0b3IgNC54IGlzIG1pc3NpbmcgKGh0dHA6Ly9ja2VkaXRvci5jb20vKScpO1xyXG4gICAgfVxyXG4gICAgY29uc3QgdGV4dGFyZWFFbCA9IHRoaXMudGV4dGFyZWFSZWYubmF0aXZlRWxlbWVudDtcclxuICAgIHRoaXMuaWRlbnRpZmllciA9IGlkZW50aWZpZXI7XHJcbiAgICB0ZXh0YXJlYUVsLnNldEF0dHJpYnV0ZSgnbmFtZScsIHRoaXMuaWRlbnRpZmllcik7XHJcbiAgICBpZiAodGhpcy5ja0lucyB8fCAhdGhpcy5kb2N1bWVudENvbnRhaW5zKHRoaXMudGV4dGFyZWFSZWYubmF0aXZlRWxlbWVudCkpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IG9wdCA9IE9iamVjdC5hc3NpZ24oe30sIGRlZmF1bHRzLCB0aGlzLmNvbmZpZywge1xyXG4gICAgICByZWFkT25seTogdGhpcy5yZWFkb25seSxcclxuICAgICAgc2tpbjogdGhpcy5za2luLFxyXG4gICAgICBsYW5ndWFnZTogdGhpcy5sYW5ndWFnZSxcclxuICAgICAgZnVsbFBhZ2U6IHRoaXMuZnVsbFBhZ2UsXHJcbiAgICAgIGlubGluZTogdGhpcy5pbmxpbmVcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY2tJbnMgPSB0aGlzLmlubGluZSA/IENLRURJVE9SLmlubGluZSh0ZXh0YXJlYUVsLCBvcHQpIDogQ0tFRElUT1IucmVwbGFjZSh0ZXh0YXJlYUVsLCBvcHQpO1xyXG4gICAgdGhpcy5ja0lucy5zZXREYXRhKHRoaXMuaW5uZXJWYWx1ZSk7XHJcblxyXG4gICAgdGhpcy5ja0lucy5vbignY2hhbmdlJywgKCkgPT4ge1xyXG4gICAgICBjb25zdCB2YWwgPSB0aGlzLmNrSW5zLmdldERhdGEoKTtcclxuICAgICAgdGhpcy51cGRhdGVWYWx1ZSh2YWwpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5ja0lucy5vbignaW5zdGFuY2VSZWFkeScsIChldnQ6IGFueSkgPT4ge1xyXG4gICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xyXG4gICAgICAgIHRoaXMucmVhZHkuZW1pdChldnQpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuY2tJbnMub24oJ2JsdXInLCAoZXZ0OiBhbnkpID0+IHtcclxuICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICB0aGlzLmJsdXIuZW1pdChldnQpO1xyXG4gICAgICAgIHRoaXMub25Ub3VjaGVkKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5ja0lucy5vbignZm9jdXMnLCAoZXZ0OiBhbnkpID0+IHtcclxuICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICB0aGlzLmZvY3VzLmVtaXQoZXZ0KTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZGVzdHJveUVkaXRvcigpIHtcclxuICAgIGlmICh0aGlzLmNrSW5zKSB7XHJcbiAgICAgIC8vIElmIHVzZSBkZXN0cm95LCB3aWxsIGZpcmUgJ0Vycm9yIGNvZGU6IGVkaXRvci1kZXN0cm95LWlmcmFtZSdcclxuICAgICAgLy8gdGhpcy5ja0lucy5kZXN0cm95KCk7XHJcbiAgICAgIGlmIChDS0VESVRPUi5pbnN0YW5jZXMuaGFzT3duUHJvcGVydHkodGhpcy5ja0lucy5uYW1lKSkge1xyXG4gICAgICAgIENLRURJVE9SLnJlbW92ZShDS0VESVRPUi5pbnN0YW5jZXNbdGhpcy5ja0lucy5uYW1lXSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5ja0lucyA9IG51bGw7XHJcbiAgICAgIGNvbnN0IGVkaXRvckVsID0gdGhpcy5ob3N0RWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcjY2tlXycgKyB0aGlzLmlkZW50aWZpZXIpO1xyXG4gICAgICBpZiAoZWRpdG9yRWwgIT0gbnVsbCAmJiBlZGl0b3JFbC5wYXJlbnRFbGVtZW50KSB7XHJcbiAgICAgICAgZWRpdG9yRWwucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChlZGl0b3JFbCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgdXBkYXRlVmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcclxuICAgICAgdGhpcy5pbm5lclZhbHVlID0gdmFsdWU7XHJcbiAgICAgIHRoaXMub25DaGFuZ2UodmFsdWUpO1xyXG4gICAgICB0aGlzLm9uVG91Y2hlZCgpO1xyXG4gICAgICB0aGlzLmNoYW5nZS5lbWl0KHZhbHVlKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBkb2N1bWVudENvbnRhaW5zKG5vZGU6IE5vZGUpIHtcclxuICAgIHJldHVybiBkb2N1bWVudC5jb250YWlucyA/IGRvY3VtZW50LmNvbnRhaW5zKG5vZGUpIDogZG9jdW1lbnQuYm9keS5jb250YWlucyhub2RlKTtcclxuICB9XHJcblxyXG4gIHdyaXRlVmFsdWUodmFsdWU6IGFueSk6IHZvaWQge1xyXG4gICAgdGhpcy5pbm5lclZhbHVlID0gdmFsdWUgfHwgJyc7XHJcbiAgICBpZiAodGhpcy5ja0lucykge1xyXG4gICAgICAvLyBGaXggYnVnIHRoYXQgY2FuJ3QgZW1pdCBjaGFuZ2UgZXZlbnQgd2hlbiBzZXQgbm9uLWh0bWwgdGFnIHZhbHVlIHR3aWNlIGluIGZ1bGxwYWdlIG1vZGUuXHJcbiAgICAgIHRoaXMuY2tJbnMuc2V0RGF0YSh0aGlzLmlubmVyVmFsdWUpO1xyXG4gICAgICBjb25zdCB2YWwgPSB0aGlzLmNrSW5zLmdldERhdGEoKTtcclxuICAgICAgdGhpcy5ja0lucy5zZXREYXRhKHZhbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiAodmFsdWU6IHN0cmluZykgPT4gdm9pZCk6IHZvaWQge1xyXG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xyXG4gIH1cclxuXHJcbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm46ICgpID0+IHZvaWQpOiB2b2lkIHtcclxuICAgIHRoaXMub25Ub3VjaGVkID0gZm47XHJcbiAgfVxyXG5cclxuICBzZXREaXNhYmxlZFN0YXRlPyhpc0Rpc2FibGVkOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkID0gaXNEaXNhYmxlZDtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENLRWRpdG9yQ29tcG9uZW50IH0gZnJvbSAnLi9jay1lZGl0b3IuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZXhwb3J0czogW0Zvcm1zTW9kdWxlLCBDS0VkaXRvckNvbXBvbmVudF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbQ0tFZGl0b3JDb21wb25lbnRdLFxyXG4gIHByb3ZpZGVyczogW11cclxufSlcclxuZXhwb3J0IGNsYXNzIENLRWRpdG9yTW9kdWxlIHt9XHJcbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7SUFvQk0sUUFBUSxHQUFHO0lBQ2YsV0FBVyxFQUFFLENBQUMsRUFBRSxDQUFDO0lBQ2pCLFlBQVksRUFBRSxFQUFFO0NBQ2pCO0FBRUQ7SUErREUsMkJBQW9CLE1BQWMsRUFBVSxNQUFrQjtRQUExQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQXJEdEQsZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVoQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLHNCQUFpQixHQUFHLEtBQUssQ0FBQzs7OztRQUtsQixhQUFRLEdBQUcsS0FBSyxDQUFDOzs7O1FBSWpCLFdBQU0sR0FBUSxFQUFFLENBQUM7Ozs7UUFJakIsU0FBSSxHQUFHLFlBQVksQ0FBQzs7OztRQUlwQixhQUFRLEdBQUcsSUFBSSxDQUFDOzs7O1FBSWhCLGFBQVEsR0FBRyxLQUFLLENBQUM7Ozs7UUFJakIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQU1kLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzVCLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNCLFNBQUksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzFCLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBUzVDLGFBQVEsR0FBRyxVQUFDLEtBQWEsS0FBTyxDQUFDO1FBQ2pDLGNBQVMsR0FBRyxlQUFRLENBQUM7UUFPbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDbEU7Ozs7OztJQWJjLHFDQUFtQjs7Ozs7SUFBbEMsVUFBbUMsRUFBZTtRQUFmLG1CQUFBLEVBQUEsT0FBZTtRQUNoRCxPQUFPLFNBQVMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ3ZFO0lBS0Qsc0JBQVcsdUNBQVE7Ozs7UUFBbkI7WUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDbkI7OztPQUFBOzs7O0lBTUQsb0NBQVE7OztJQUFSLGVBQWE7Ozs7O0lBRWIsdUNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNsQztLQUNGOzs7O0lBRUQsOENBQWtCOzs7SUFBbEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQ3BGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbEM7YUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQzNGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO0tBQ0Y7Ozs7SUFFRCx1Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7S0FDdEI7Ozs7OztJQUVPLHNDQUFVOzs7OztJQUFsQixVQUFtQixVQUFrQjtRQUFyQyxpQkE2Q0M7UUE1Q0MsSUFBSSxPQUFPLFFBQVEsS0FBSyxXQUFXLEVBQUU7WUFDbkMsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7U0FDdkU7O1lBQ0ssVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixVQUFVLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDeEUsT0FBTztTQUNSOztZQUVLLEdBQUcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNuRCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDcEIsQ0FBQztRQUVGLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFOztnQkFDaEIsR0FBRyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2hDLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDdkIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQUMsR0FBUTtZQUN0QyxLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDZCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN0QixDQUFDLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBQyxHQUFRO1lBQzdCLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNkLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7YUFDbEIsQ0FBQyxDQUFDO1NBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsR0FBUTtZQUM5QixLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDZCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN0QixDQUFDLENBQUM7U0FDSixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFTyx5Q0FBYTs7OztJQUFyQjtRQUNFLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7O1lBR2QsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN0RCxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7O2dCQUNaLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDbkYsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsQ0FBQyxhQUFhLEVBQUU7Z0JBQzlDLFFBQVEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzlDO1NBQ0Y7S0FDRjs7Ozs7O0lBRU8sdUNBQVc7Ozs7O0lBQW5CLFVBQW9CLEtBQWE7UUFBakMsaUJBT0M7UUFOQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNkLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2pCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pCLENBQUMsQ0FBQztLQUNKOzs7Ozs7SUFFTyw0Q0FBZ0I7Ozs7O0lBQXhCLFVBQXlCLElBQVU7UUFDakMsT0FBTyxRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDbkY7Ozs7O0lBRUQsc0NBQVU7Ozs7SUFBVixVQUFXLEtBQVU7UUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQzlCLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7WUFFZCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7O2dCQUM5QixHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDekI7S0FDRjs7Ozs7SUFFRCw0Q0FBZ0I7Ozs7SUFBaEIsVUFBaUIsRUFBMkI7UUFDMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7S0FDcEI7Ozs7O0lBRUQsNkNBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQWM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7S0FDckI7Ozs7O0lBRUQsNENBQWdCOzs7O0lBQWhCLFVBQWtCLFVBQW1CO1FBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO0tBQzVCO0lBdEljLHFCQUFHLEdBQUcsQ0FBQyxDQUFDOztnQkFuRHhCLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLDJDQUVUO29CQUNELFNBQVMsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsY0FBTSxPQUFBLGlCQUFpQixHQUFBLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7b0JBQzFHLFFBQVEsRUFBRSxVQUFVO2lCQUNyQjs7OztnQkF6QkMsTUFBTTtnQkFITixVQUFVOzs7MkJBdUNULEtBQUs7eUJBSUwsS0FBSzt1QkFJTCxLQUFLOzJCQUlMLEtBQUs7MkJBSUwsS0FBSzt5QkFJTCxLQUFLO3FCQUlMLEtBQUs7eUJBRUwsTUFBTTt3QkFDTixNQUFNO3VCQUNOLE1BQU07d0JBQ04sTUFBTTs4QkFFTixTQUFTLFNBQUMsVUFBVTs7SUF5SXZCLHdCQUFDO0NBQUE7Ozs7OztBQ25ORDtJQUlBO0tBTThCOztnQkFON0IsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxFQUFFO29CQUNYLE9BQU8sRUFBRSxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQztvQkFDekMsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7b0JBQ2pDLFNBQVMsRUFBRSxFQUFFO2lCQUNkOztJQUM0QixxQkFBQztDQUFBOzs7Ozs7Ozs7Ozs7OzsifQ==

/***/ })

}]);
//# sourceMappingURL=default~components-editor-editor-module~components-email-email-module.js.map