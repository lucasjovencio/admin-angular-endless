(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-base-base-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/accordion/accordion.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/accordion/accordion.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Accordion</h5>\r\n        </div>\r\n        <div class=\"card-body default-accordion\">\r\n          <ngb-accordion #acc=\"ngbAccordion\" activeIds=\"ngb-panel-0\">\r\n            <ngb-panel title=\"Simple\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel>\r\n              <ng-template ngbPanelTitle>\r\n                <span>&#9733; <b>Fancy</b> title &#9733;</span>\r\n              </ng-template>\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel title=\"Disabled\" [disabled]=\"true\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n          </ngb-accordion>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>One open panel at a time</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-accordion [closeOthers]=\"true\" activeIds=\"static-1\">\r\n            <ngb-panel id=\"static-1\" title=\"Simple\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"static-2\">\r\n              <ng-template ngbPanelTitle>\r\n                <span>&#9733; <b>Fancy</b> title &#9733;</span>\r\n              </ng-template>\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"static-3\" title=\"Disabled\" [disabled]=\"true\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n          </ngb-accordion>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Toggle panels</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-accordion #acc=\"ngbAccordion\">\r\n            <ngb-panel id=\"toggle-1\" title=\"First panel\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"toggle-2\" title=\"Second\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n          </ngb-accordion>\r\n\r\n          <hr />\r\n          <div class=\"btn-showcase\">\r\n            <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"acc.toggle('toggle-1')\">Toggle first</button>\r\n            <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"acc.toggle('toggle-2')\">Toggle second</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Prevent panel toggle</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-accordion (panelChange)=\"beforeChange($event)\">\r\n            <ngb-panel id=\"preventchange-1\" title=\"Simple\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"preventchange-2\" title=\"I can't be toggled...\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"preventchange-3\" title=\"I can be opened, but not closed...\">\r\n              <ng-template ngbPanelContent>\r\n                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf\r\n                moon officia\r\n                aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf\r\n                moon tempor,\r\n                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim\r\n                keffiyeh helvetica,\r\n                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.\r\n                Leggings\r\n                occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them\r\n                accusamus\r\n                labore sustainable VHS.\r\n              </ng-template>\r\n            </ngb-panel>\r\n          </ngb-accordion>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/alert/alert.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/alert/alert.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Alert</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-alert [type]=\"'success'\" [dismissible]=\"false\">\r\n            This is an success alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'info'\" [dismissible]=\"false\">\r\n            This is an info alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'warning'\" [dismissible]=\"false\">\r\n            This is a warning alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'danger'\" [dismissible]=\"false\">\r\n            This is a danger alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'primary'\" [dismissible]=\"false\">\r\n            This is a primary alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'secondary'\" [dismissible]=\"false\">\r\n            This is a secondary alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'light'\" [dismissible]=\"false\">\r\n            This is a light alert\r\n          </ngb-alert>\r\n          <ngb-alert [type]=\"'dark'\" [dismissible]=\"false\">\r\n            This is a dark alert\r\n          </ngb-alert>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Closable Alert</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p *ngFor=\"let alert of alerts\">\r\n            <ngb-alert [type]=\"alert.type\" [dismissible]=\"true\" (close)=\"close(alert)\">{{ alert.message }}</ngb-alert>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Self closing alert</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            Static self-closing alert that disappears after 20 seconds (refresh the page if it has already disappeared)\r\n          </p>\r\n          <ngb-alert *ngIf=\"!staticAlertClosed\" (close)=\"staticAlertClosed = true\">Check out our awesome new features!\r\n          </ngb-alert>\r\n          <hr/>\r\n          <p>\r\n            Show a self-closing success message that disappears after 5 seconds.\r\n          </p>\r\n          <ngb-alert *ngIf=\"successMessage\" type=\"success\" (close)=\"successMessage = null\">{{ successMessage }}\r\n          </ngb-alert>\r\n          <p>\r\n            <button class=\"btn btn-primary\" (click)=\"changeSuccessMessage()\">Change message</button>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom alert</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-alert type=\"custom\" class=\"alert-custom\" [dismissible]=\"false\"><strong>Whoa!</strong> This is a custom\r\n              alert.</ngb-alert>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/buttons/buttons.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/buttons/buttons.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Checkbox buttons</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"btn-group btn-group-toggle\">\r\n            <label class=\"btn-primary\" ngbButtonLabel>\r\n              <input type=\"checkbox\" ngbButton [(ngModel)]=\"model.left\"> Left (pre-checked)\r\n            </label>\r\n            <label class=\"btn-primary\" ngbButtonLabel>\r\n              <input type=\"checkbox\" ngbButton [(ngModel)]=\"model.middle\"> Middle\r\n            </label>\r\n            <label class=\"btn-primary\" ngbButtonLabel>\r\n              <input type=\"checkbox\" ngbButton [(ngModel)]=\"model.right\"> Right\r\n            </label>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">{{model | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Checkbox buttons (Reactive Forms)</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form [formGroup]=\"checkboxGroupForm\">\r\n            <div class=\"btn-group btn-group-toggle\">\r\n              <label class=\"btn-primary\" ngbButtonLabel>\r\n                <input type=\"checkbox\" formControlName=\"left\" ngbButton> Left (pre-checked)\r\n              </label>\r\n              <label class=\"btn-primary\" ngbButtonLabel>\r\n                <input type=\"checkbox\" formControlName=\"middle\" ngbButton> Middle\r\n              </label>\r\n              <label class=\"btn-primary\" ngbButtonLabel>\r\n                <input type=\"checkbox\" formControlName=\"right\" ngbButton> Right\r\n              </label>\r\n            </div>\r\n          </form>\r\n          <hr>\r\n          <pre class=\"mb-0\">{{checkboxGroupForm.value | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Radio buttons</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"btn-group btn-group-toggle\" ngbRadioGroup name=\"radioBasic\" [(ngModel)]=\"modelRadio\">\r\n            <label ngbButtonLabel class=\"btn-primary\">\r\n              <input ngbButton type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-primary\">\r\n              <input ngbButton type=\"radio\" value=\"middle\"> Middle\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-primary\">\r\n              <input ngbButton type=\"radio\" [value]=\"false\"> Right\r\n            </label>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">{{modelRadio}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Radio buttons (Reactive Forms)</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form [formGroup]=\"radioGroupForm\">\r\n            <div class=\"btn-group btn-group-toggle\" ngbRadioGroup name=\"radioBasic\" formControlName=\"model\">\r\n              <label ngbButtonLabel class=\"btn-primary\">\r\n                <input ngbButton type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n              </label>\r\n              <label ngbButtonLabel class=\"btn-primary\">\r\n                <input ngbButton type=\"radio\" value=\"middle\"> Middle\r\n              </label>\r\n              <label ngbButtonLabel class=\"btn-primary\">\r\n                <input ngbButton type=\"radio\" [value]=\"false\"> Right\r\n              </label>\r\n            </div>\r\n          </form>\r\n          <hr>\r\n          <pre class=\"mb-0\">{{radioGroupForm.value['model']}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/carousel/carousel.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/carousel/carousel.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Carousel</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-carousel *ngIf=\"images\">\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[0]\" alt=\"Random first slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>First slide label</h3>\r\n                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\r\n              </div>\r\n            </ng-template>\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[1]\" alt=\"Random second slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>Second slide label</h3>\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n              </div>\r\n            </ng-template>\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[2]\" alt=\"Random third slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>Third slide label</h3>\r\n                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\r\n              </div>\r\n            </ng-template>\r\n          </ngb-carousel>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Global configuration of carousels</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-carousel *ngIf=\"images\">\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[0]\" alt=\"Random first slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>10 seconds between slides...</h3>\r\n                <p>This carousel uses customized default values.</p>\r\n              </div>\r\n            </ng-template>\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[1]\" alt=\"Random second slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>No mouse events...</h3>\r\n                <p>This carousel doesn't pause or resume on mouse events</p>\r\n              </div>\r\n            </ng-template>\r\n            <ng-template ngbSlide>\r\n              <img [src]=\"images[2]\" alt=\"Random third slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>No keyboard...</h3>\r\n                <p>This carousel uses customized default values.</p>\r\n              </div>\r\n            </ng-template>\r\n          </ngb-carousel>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Navigation arrows and indicators</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-carousel *ngIf=\"images\" [showNavigationArrows]=\"showNavigationArrows\"\r\n            [showNavigationIndicators]=\"showNavigationIndicators\">\r\n            <ng-template ngbSlide *ngFor=\"let image of images\">\r\n              <img [src]=\"image\" alt=\"Random slide\">\r\n              <div class=\"carousel-caption\">\r\n                <h3>No mouse navigation</h3>\r\n                <p>This carousel hides navigation arrows and indicators.</p>\r\n              </div>\r\n            </ng-template>\r\n          </ngb-carousel>\r\n          <hr>\r\n          <div class=\"btn-showcase\" role=\"group\" aria-label=\"Carousel toggle controls\">\r\n            <button type=\"button\" (click)=\"showNavigationArrows = !showNavigationArrows\"\r\n              class=\"btn btn-outline-primary btn-sm mr-1\">Toggle navigation arrows</button>\r\n            <button type=\"button\" (click)=\"showNavigationIndicators = !showNavigationIndicators\"\r\n              class=\"btn btn-outline-primary btn-sm\">Toggle navigation indicators</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/collapse/collapse.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/collapse/collapse.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Collapse</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"isCollapsed = !isCollapsed\"\r\n              [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"collapseExample\">\r\n              Toggle\r\n            </button>\r\n          </p>\r\n          <div id=\"collapseExample\" [ngbCollapse]=\"isCollapsed\">\r\n            <div class=\"card mb-0\">\r\n              <div class=\"card-body\">\r\n                You can collapse this card by clicking Toggle\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/datepicker/datepicker.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/datepicker/datepicker.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Datepicker</h5>\r\n        </div>\r\n        <div class=\"card-body custom-datepicker\">\r\n          <ngb-datepicker #dp [(ngModel)]=\"model\" (navigate)=\"date = $event.next\">\r\n          </ngb-datepicker>\r\n          <hr />\r\n          <div class=\"btn-showcase\">\r\n            <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"selectToday()\">Select Today</button>\r\n            <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"dp.navigateTo()\">To current month</button>\r\n            <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"dp.navigateTo({year: 2013, month: 2})\">To Feb\r\n              2013</button>\r\n          </div>\r\n          <hr />\r\n          <span class=\"d-block\">Month: {{ date.month }}.{{ date.year }}</span>\r\n          <span class=\"d-block\">Model: {{ model | json }}</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Disabled datepicker</h5>\r\n        </div>\r\n        <div class=\"card-body custom-datepicker\">\r\n          <ngb-datepicker [(ngModel)]=\"modelDisabled\" [disabled]=\"disabled\"></ngb-datepicker>\r\n          <hr />\r\n          <button class=\"btn btn-sm btn-outline-{{disabled ? 'danger' : 'primary'}}\" (click)=\"disabled = !disabled\">\r\n            {{ disabled ? \"disabled\" : \"enabled\"}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Multiple months</h5>\r\n        </div>\r\n        <div class=\"card-body mutliple-datepicker\">\r\n          <ngb-datepicker [displayMonths]=\"displayMonths\" [navigation]=\"navigation\" [showWeekNumbers]=\"showWeekNumbers\"\r\n            [outsideDays]=\"outsideDays\">\r\n          </ngb-datepicker>\r\n          <hr />\r\n          <form class=\"form-inline custom-datepicker-dropdown\">\r\n            <div class=\"form-group mb-0\">\r\n              <div class=\"input-group\">\r\n                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dp\" [displayMonths]=\"displayMonths\"\r\n                  [navigation]=\"navigation\" [outsideDays]=\"outsideDays\" [showWeekNumbers]=\"showWeekNumbers\"\r\n                  ngbDatepicker #k=\"ngbDatepicker\">\r\n                <div class=\"input-group-append\">\r\n                  <button class=\"btn calendar\" (click)=\"k.toggle()\" type=\"button\"><i\r\n                      class=\"fa fa-calendar\"></i></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </form>\r\n          <hr />\r\n          <div class=\"d-flex flex-wrap align-content-between p-2\">\r\n            <select class=\"custom-select\" [(ngModel)]=\"displayMonths\">\r\n              <option [ngValue]=\"1\">One month</option>\r\n              <option [ngValue]=\"2\">Two months</option>\r\n            </select>\r\n            <select class=\"custom-select\" [(ngModel)]=\"navigation\">\r\n              <option value=\"none\">Without navigation</option>\r\n              <option value=\"select\">With select boxes</option>\r\n              <option value=\"arrows\">Without select boxes</option>\r\n            </select>\r\n            <select class=\"custom-select\" [(ngModel)]=\"showWeekNumbers\">\r\n              <option [ngValue]=\"true\">Week numbers</option>\r\n              <option [ngValue]=\"false\">No week numbers</option>\r\n            </select>\r\n            <select class=\"custom-select\" [(ngModel)]=\"outsideDays\">\r\n              <option value=\"visible\">Visible outside days</option>\r\n              <option value=\"hidden\">Hidden outside days</option>\r\n              <option value=\"collapsed\">Collapsed outside days</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Range selection</h5>\r\n        </div>\r\n        <div class=\"card-body mutliple-datepicker\">\r\n          <ngb-datepicker #dp (select)=\"onDateSelection($event)\" [displayMonths]=\"2\" [dayTemplate]=\"t\"\r\n            outsideDays=\"hidden\">\r\n          </ngb-datepicker>\r\n          <ng-template #t let-date let-focused=\"focused\">\r\n            <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n              [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n              (mouseleave)=\"hoveredDate = null\">\r\n              {{ date.day }}\r\n            </span>\r\n          </ng-template>\r\n          <hr>\r\n          <span class=\"d-block\">From: {{ fromDate | json }} </span>\r\n          <span class=\"d-block\">To: {{ toDate | json }} </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Footer template</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"form-inline custom-datepicker custom-datepicker-dropdown\">\r\n            <div class=\"form-group mb-0\">\r\n              <div class=\"input-group\">\r\n                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dp\" [(ngModel)]=\"modelFooter\" ngbDatepicker\r\n                  [footerTemplate]=\"footerTemplate\" #f=\"ngbDatepicker\">\r\n                <div class=\"input-group-append\">\r\n                  <button class=\"btn calendar\" (click)=\"f.toggle()\" type=\"button\"><i\r\n                      class=\"fa fa-calendar\"></i></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </form>\r\n          <ng-template #footerTemplate>\r\n            <hr class=\"my-0\">\r\n            <button class=\"btn btn-primary btn-sm m-2 float-left\" (click)=\"model = today; f.close()\">Today</button>\r\n            <button class=\"btn btn-secondary btn-sm m-2 float-right\" (click)=\"f.close()\">Close</button>\r\n          </ng-template>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"colsm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom day view</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form class=\"form-inline custom-datepicker-dropdown custom-datepicker\">\r\n            <div class=\"form-group mb-0\">\r\n              <div class=\"input-group\">\r\n                <input class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dp\" [(ngModel)]=\"modelCustom\" ngbDatepicker\r\n                  [dayTemplate]=\"customDay\" [markDisabled]=\"isDisabled\" #c=\"ngbDatepicker\">\r\n                <div class=\"input-group-append\">\r\n                  <button class=\"btn calendar\" (click)=\"c.toggle()\" type=\"button\"><i\r\n                      class=\"fa fa-calendar\"></i></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </form>\r\n          <ng-template #customDay let-date let-currentMonth=\"currentMonth\" let-selected=\"selected\"\r\n            let-disabled=\"disabled\" let-focused=\"focused\">\r\n            <span class=\"custom-day\" [class.weekend]=\"isWeekend(date)\" [class.focused]=\"focused\"\r\n              [class.bg-primary]=\"selected\" [class.hidden]=\"date.month !== currentMonth\" [class.text-muted]=\"disabled\">\r\n              {{ date.day }}\r\n            </span>\r\n          </ng-template>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/dropdown/dropdown.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/dropdown/dropdown.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Dropdown</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row btn-showcase\">\r\n            <div class=\"col\">\r\n              <div ngbDropdown class=\"d-inline-block\" placement=\"bottom\">\r\n                <button class=\"btn btn-outline-primary\" id=\"dropdownBasic1\" ngbDropdownToggle >Toggle dropdown</button>\r\n                <div ngbDropdownMenu aria-labelledby=\"dropdownBasic1\">\r\n                  <button class=\"dropdown-item\">Action - 1</button>\r\n                  <button class=\"dropdown-item\">Another Action</button>\r\n                  <button class=\"dropdown-item\">Something else is here</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col text-right\">\r\n              <div ngbDropdown placement=\"top-right\" class=\"d-inline-block\">\r\n                <button class=\"btn btn-outline-primary mr-0\" id=\"dropdownBasic2\" ngbDropdownToggle>Toggle\r\n                  dropup</button>\r\n                <div ngbDropdownMenu aria-labelledby=\"dropdownBasic2\">\r\n                  <button class=\"dropdown-item\">Action - 1</button>\r\n                  <button class=\"dropdown-item\">Another Action</button>\r\n                  <button class=\"dropdown-item\">Something else is here</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Manual and custom triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"d-inline-block btn-showcase\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n            <button class=\"btn btn-outline-primary mr-2\" id=\"dropdownManual\" ngbDropdownAnchor\r\n              (focus)=\"myDrop.open()\">Toggle dropdown</button>\r\n            <div ngbDropdownMenu aria-labelledby=\"dropdownManual\">\r\n              <button class=\"dropdown-item\">Action - 1</button>\r\n              <button class=\"dropdown-item\">Another Action</button>\r\n              <button class=\"dropdown-item\">Something else is here</button>\r\n            </div>\r\n            <button class=\"btn btn-outline-primary mr-2\" (click)=\"$event.stopPropagation(); myDrop.open();\">Open from\r\n              outside</button>\r\n            <button class=\"btn btn-outline-primary mr-2\" (click)=\"$event.stopPropagation(); myDrop.close();\">Close from\r\n              outside</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Manual and custom triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"btn-showcase\">\r\n            <div class=\"btn-group\">\r\n              <button type=\"button\" class=\"btn btn-outline-primary\">Plain ol' button</button>\r\n              <div class=\"btn-group\" ngbDropdown role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Drop me</button>\r\n                <div class=\"dropdown-menu\" ngbDropdownMenu>\r\n                  <button class=\"dropdown-item\">One</button>\r\n                  <button class=\"dropdown-item\">Two</button>\r\n                  <button class=\"dropdown-item\">Four!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"btn-group\">\r\n              <button type=\"button\" class=\"btn btn-primary mr-0\">Split me</button>\r\n              <div class=\"btn-group\" ngbDropdown role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                <button class=\"btn btn-primary dropdown-toggle-split\" ngbDropdownToggle></button>\r\n                <div class=\"dropdown-menu\" ngbDropdownMenu>\r\n                  <button class=\"dropdown-item\">One</button>\r\n                  <button class=\"dropdown-item\">Two</button>\r\n                  <button class=\"dropdown-item\">Four!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"btn-group mr-3\">\r\n              <div class=\"btn-group\" ngbDropdown role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Select me</button>\r\n                <div class=\"dropdown-menu\" ngbDropdownMenu>\r\n                  <button class=\"dropdown-item\">One</button>\r\n                  <button class=\"dropdown-item\">Two</button>\r\n                  <button class=\"dropdown-item\">Four!</button>\r\n                </div>\r\n              </div>\r\n              <div class=\"btn-group\" ngbDropdown role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                <button class=\"btn btn-outline-primary\" ngbDropdownToggle>Or me</button>\r\n                <div class=\"dropdown-menu\" ngbDropdownMenu>\r\n                  <button class=\"dropdown-item\">One</button>\r\n                  <button class=\"dropdown-item\">Two</button>\r\n                  <button class=\"dropdown-item\">Four!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Global configuration of dropdowns</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div ngbDropdown>\r\n            <button class=\"btn btn-outline-primary\" id=\"dropdownConfig\" ngbDropdownToggle>Toggle</button>\r\n            <div ngbDropdownMenu aria-labelledby=\"dropdownConfig\">\r\n              <button class=\"dropdown-item\">Action - 1</button>\r\n              <button class=\"dropdown-item\">Another Action</button>\r\n              <button class=\"dropdown-item\">Something else is here</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/modal/modal.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/modal/modal.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Modal with default options</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ng-template #content let-modal>\r\n            <div class=\"modal-header\">\r\n              <h4 class=\"modal-title\" id=\"modal-basic-title\">Profile update</h4>\r\n              <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss('Cross click')\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n              </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n              <form>\r\n                <div class=\"form-group\">\r\n                  <label for=\"dateOfBirth\">Date of birth</label>\r\n                  <div class=\"input-group\">\r\n                    <input id=\"dateOfBirth\" class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dp\" ngbDatepicker\r\n                      #dp=\"ngbDatepicker\">\r\n                    <div class=\"input-group-append\">\r\n                      <button class=\"btn btn-outline-secondary calendar\" (click)=\"dp.toggle()\" type=\"button\"></button>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n              <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Save</button>\r\n            </div>\r\n          </ng-template>\r\n          <button class=\"btn btn-lg btn-outline-primary\" (click)=\"open(content)\">Launch demo modal</button>\r\n          <hr>\r\n          <pre class=\"mb-0\">Result : {{closeResult}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>\r\n            Components as content</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>You can pass an existing component as content of the modal window. In this case remember to add content\r\n            component\r\n            as an <code>entryComponents</code> section of your <code>NgModule</code>.</p>\r\n          <button class=\"btn btn-lg btn-outline-primary\" (click)=\"openModal()\">Launch demo modal</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Stacked modals</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <button class=\"btn btn-lg btn-outline-primary\" (click)=\"openStackedModal()\">Launch demo modal</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Global configuration of modals</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n            <div class=\"modal-header\">\r\n              <h4 class=\"modal-title\" id=\"modal-basic-title\">Hi there!</h4>\r\n              <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n              </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n              <p>Hello, World!</p>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n              <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"c('Save click')\">Save</button>\r\n            </div>\r\n          </ng-template>\r\n          <button class=\"btn btn-lg btn-outline-primary\" (click)=\"openCustomModal(content)\">Launch demo modal</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Modal with options</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ng-template #content let-modal>\r\n            <div class=\"modal-header\">\r\n              <h4 class=\"modal-title\">Modal title</h4>\r\n              <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss('Cross click')\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n              </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n              <p>One fine body&hellip;</p>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n              <button type=\"button\" class=\"btn btn-light\" (click)=\"modal.close('Close click')\">Close</button>\r\n            </div>\r\n          </ng-template>\r\n          <div class=\"btn-showcase modal-options-p-xs\">\r\n            <button class=\"btn btn-outline-primary\" (click)=\"openWindowCustomClass(content)\">Modal with window custom\r\n              class</button>\r\n            <button class=\"btn btn-outline-primary\" (click)=\"openBackDropCustomClass(content)\">Modal with backdrop\r\n              custom class</button>\r\n            <button class=\"btn btn-outline-primary\" (click)=\"openSm(content)\">Small modal</button>\r\n            <button class=\"btn btn-outline-primary\" (click)=\"openLg(content)\">Large modal</button>\r\n            <button class=\"btn btn-outline-primary\" (click)=\"openVerticallyCentered(content)\">Modal vertically\r\n              centered</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/pagination/pagination.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/pagination/pagination.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid Ends -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic pagination</h5>\r\n        </div>\r\n        <div class=\"card-body pagination-space\">\r\n          <p>Default pagination:</p>\r\n          <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" aria-label=\"Default pagination\"></ngb-pagination>\r\n\r\n          <div class=\"pagination-top\">\r\n            <p>No direction links:</p>\r\n            <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [directionLinks]=\"false\"></ngb-pagination>\r\n          </div>\r\n          <div class=\"pagination-top\">\r\n            <p>With boundary links:</p>\r\n            <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">Current page: {{page}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Advanced pagination</h5>\r\n        </div>\r\n        <div class=\"card-body pagination-space\">\r\n          <p>Restricted size, no rotation:</p>\r\n          <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancePage\" [maxSize]=\"5\" [boundaryLinks]=\"true\">\r\n          </ngb-pagination>\r\n          <div class=\"pagination-top\">\r\n            <p>Restricted size with rotation:</p>\r\n            <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancePage\" [maxSize]=\"5\" [rotate]=\"true\"\r\n              [boundaryLinks]=\"true\"></ngb-pagination>\r\n          </div>\r\n          <div class=\"pagination-top\">\r\n            <p>Restricted size with rotation and no ellipses:</p>\r\n            <ngb-pagination [collectionSize]=\"120\" [(page)]=\"advancePage\" [maxSize]=\"5\" [rotate]=\"true\"\r\n              [ellipses]=\"false\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">Current page: {{advancePage}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Pagination size</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\" size=\"lg\"></ngb-pagination>\r\n          <br>\r\n          <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\"></ngb-pagination>\r\n          <br>\r\n          <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\" size=\"sm\"></ngb-pagination>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Pagination alignment</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-pagination class=\"d-flex justify-content-start\" [collectionSize]=\"70\" [(page)]=\"page\"></ngb-pagination>\r\n          <br>\r\n          <ngb-pagination class=\"d-flex justify-content-center\" [collectionSize]=\"70\" [(page)]=\"page\"></ngb-pagination>\r\n          <br>\r\n          <ngb-pagination class=\"d-flex justify-content-end\" [collectionSize]=\"70\" [(page)]=\"page\"></ngb-pagination>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Disabled pagination</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>Pagination control can be disabled:</p>\r\n          <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [disabled]='isDisabled'></ngb-pagination>\r\n          <hr>\r\n          <button class=\"btn btn-sm btn-outline-primary\" (click)=\"toggleDisabled()\">\r\n            Toggle disabled\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Global configuration</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>This pagination uses custom default values</p>\r\n          <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\"></ngb-pagination>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/popover/popover.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/popover/popover.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Quick and easy popovers</h5>\r\n        </div>\r\n        <div class=\"card-body btn-showcase popover-mr\">\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"top\"\r\n            ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on top\">\r\n            Popover on top\r\n          </button>\r\n\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"right\"\r\n            ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on right\">\r\n            Popover on right\r\n          </button>\r\n\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"bottom\"\r\n            ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on bottom\">\r\n            Popover on bottom\r\n          </button>\r\n\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"left\"\r\n            ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on left\">\r\n            Popover on left\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Global configuration of popovers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <button type=\"button\" class=\"btn btn-outline-primary\"\r\n            ngbPopover=\"This popover gets its inputs from the customized configuration\"\r\n            popoverTitle=\"Customized popover\">\r\n            Customized popover\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom and manual triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can easily override open and close triggers by specifying event names (separated by <code>:</code>) in\r\n            the <code>triggers</code> property.\r\n          </p>\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" ngbPopover=\"You see, I show up on hover!\"\r\n            triggers=\"mouseenter:mouseleave\" popoverTitle=\"Pop title\">\r\n            Hover over me!\r\n          </button>\r\n          <hr>\r\n          <p>\r\n            Alternatively you can take full manual control over popover opening / closing events.\r\n          </p>\r\n          <div class=\"btn-showcase\">\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" ngbPopover=\"What a great tip!\"\r\n              [autoClose]=\"false\" triggers=\"manual\" #p=\"ngbPopover\" (click)=\"p.open()\" popoverTitle=\"Pop title\">\r\n              Click me to open a popover\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"p.close()\">\r\n              Click me to close a popover\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Automatic closing with keyboard and mouse</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>As for some other popup-based widgets, you can set the popover to close automatically upon some events.</p>\r\n          <p>In the following examples, they will all close on <code>Escape</code> as well as:</p>\r\n          <ul>\r\n            <li class=\"mb-2\">\r\n              click inside:\r\n              <button type=\"button\" class=\"btn btn-outline-primary\" popoverTitle=\"Pop title\" [autoClose]=\"'inside'\"\r\n                ngbPopover=\"Click inside or press Escape to close\">\r\n                Click to toggle\r\n              </button>\r\n            </li>\r\n            <li class=\"mb-2\">\r\n              click outside:\r\n              <button type=\"button\" class=\"btn btn-outline-primary\" popoverTitle=\"Pop title\" [autoClose]=\"'outside'\"\r\n                ngbPopover=\"Click outside or press Escape to close\">\r\n                Click to toggle\r\n              </button>\r\n            </li>\r\n            <li>\r\n              all clicks:\r\n              <div class=\"btn-showcase d-inline-block\">\r\n                <button type=\"button\" class=\"btn btn-outline-primary mr-0\" popoverTitle=\"Pop title\" [autoClose]=\"true\"\r\n                  ngbPopover=\"Click anywhere or press Escape to close (try the toggling element too)\"\r\n                  #popover3=\"ngbPopover\">\r\n                  Click to toggle\r\n                </button>\r\n                &nbsp;\r\n                <button type=\"button\" class=\"btn btn-outline-primary mr-0 btn-xs-eplisis\" (click)=\"popover3.toggle()\">\r\n                  Click to toggle the external popover\r\n                </button>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Popover with custom class</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can optionally pass in a custom class via <code>popoverClass</code>\r\n          </p>\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" ngbPopover=\"Nice class!\" popoverClass=\"my-custom-class\">\r\n            Popover with custom class\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Context and manual triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can optionally pass in a context when manually triggering a popover.\r\n          </p>\r\n          <ng-template #popContent let-greeting=\"greeting\">{{greeting}}, <b>{{name}}</b>!</ng-template>\r\n          <ng-template #popTitle let-language=\"language\">Greeting in {{language}}</ng-template>\r\n          <div class=\"btn-showcase\">\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbPopover]=\"popContent\"\r\n              [popoverTitle]=\"popTitle\" triggers=\"manual\" #p1=\"ngbPopover\"\r\n              (click)=\"toggleWithGreeting(p1, 'Bonjour', 'French')\">\r\n              French\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbPopover]=\"popContent\"\r\n              [popoverTitle]=\"popTitle\" triggers=\"manual\" #p2=\"ngbPopover\"\r\n              (click)=\"toggleWithGreeting(p2, 'Gutentag', 'German')\">\r\n              German\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbPopover]=\"popContent\"\r\n              [popoverTitle]=\"popTitle\" triggers=\"manual\" #p3=\"ngbPopover\"\r\n              (click)=\"toggleWithGreeting(p3, 'Hello', 'English')\">\r\n              English\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Popover visibility events</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"top\"\r\n            ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on top\"\r\n            #popover=\"ngbPopover\" (shown)=\"recordShown()\" (hidden)=\"recordHidden()\">\r\n            Open Popover\r\n          </button>\r\n          <hr>\r\n          <ul>\r\n            <li>Popover is currently: <code>{{ popover.isOpen() ? 'open' : 'closed' }}</code></li>\r\n            <li>Last shown at: <code>{{lastShown | date:'h:mm:ss'}}</code></li>\r\n            <li>Last hidden at: <code>{{lastHidden | date:'h:mm:ss'}}</code></li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>HTML and bindings in popovers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            Popovers can contain any arbitrary HTML, Angular bindings and even directives!\r\n            Simply enclose desired content or title in a <code>&lt;ng-template&gt;</code> element.\r\n          </p>\r\n          <ng-template #popContent>Hello, <b>{{name}}</b>!</ng-template>\r\n          <ng-template #popTitle>Fancy <b>content!!</b></ng-template>\r\n          <button type=\"button\" class=\"btn btn-outline-primary btn-xs-eplisis\" [ngbPopover]=\"popContent\"\r\n            [popoverTitle]=\"popTitle\">\r\n            I've got markup and bindings in my popover!\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/progressbar/progressbar.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/progressbar/progressbar.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Contextual progress bars</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-progressbar type=\"success\" [value]=\"25\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"info\" [value]=\"50\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"warning\" [value]=\"75\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"danger\" [value]=\"100\"></ngb-progressbar>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Progress bars with current value labels</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-progressbar showValue=\"true\" type=\"success\" [value]=\"25\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar [showValue]=\"true\" type=\"info\" [value]=\"50\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar showValue=\"true\" type=\"warning\" [value]=\"150\" [max]=\"200\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar [showValue]=\"true\" type=\"danger\" [value]=\"150\" [max]=\"150\"></ngb-progressbar>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Striped progress bars</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-progressbar type=\"success\" [value]=\"25\" [striped]=\"true\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"info\" [value]=\"50\" [striped]=\"true\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"warning\" [value]=\"75\" [striped]=\"true\"></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\"></ngb-progressbar>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Progress bars with custom labels</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-progressbar type=\"success\" [value]=\"25\">25</ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"info\" [value]=\"50\">Copying file <b>2 of 4</b>...</ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"warning\" [value]=\"75\" [striped]=\"true\" [animated]=\"true\"><i>50%</i></ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\">Completed!</ngb-progressbar>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Progress bars with height</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            <ngb-progressbar type=\"success\" [value]=\"25\">default</ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"info\" [value]=\"50\" height=\"10px\">10px</ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"warning\" [value]=\"75\" height=\".5rem\">.5rem</ngb-progressbar>\r\n          </p>\r\n          <p>\r\n            <ngb-progressbar type=\"danger\" [value]=\"100\" height=\".3rem\">.3rem</ngb-progressbar>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/rating/rating.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/rating/rating.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Rating</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-rating [(rate)]=\"currentRate\" class=\"rating-size\"></ngb-rating>\r\n          <hr>\r\n          <pre class=\"mb-0\">Rate: <b>{{currentRate}}</b></pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Events and readonly ratings</h5>\r\n        </div>\r\n        <div class=\"card-body editable-rating\">\r\n          <ngb-rating [(rate)]=\"selected\" (hover)=\"hovered=$event\" (leave)=\"hovered=0\" [readonly]=\"readonly\"\r\n            class=\"rating-size\"></ngb-rating>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected: <b>{{selected}}</b> Hovered: <b>{{hovered}}</b></pre>\r\n          <button class=\"btn btn-sm btn-position btn-outline-{{readonly ? 'danger' : 'success'}}\"\r\n            (click)=\"readonly = !readonly\">\r\n            {{ readonly ? \"readonly\" : \"editable\"}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Form integration</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>NgModel and reactive forms can be used without the 'rate' binding</p>\r\n          <div class=\"form-group\">\r\n            <ngb-rating [formControl]=\"ctrl\" class=\"rating-size\"></ngb-rating>\r\n            <div class=\"form-text small\">\r\n              <div *ngIf=\"ctrl.valid\" class=\"text-success\">Thanks!</div>\r\n              <div *ngIf=\"ctrl.invalid\" class=\"text-danger\">Please rate us</div>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <pre>Model: <b>{{ ctrl.value }}</b></pre>\r\n          <button class=\"btn btn-sm btn-outline-{{ ctrl.disabled ? 'danger' : 'success'}} mr-2\" (click)=\"toggle()\">\r\n            {{ ctrl.disabled ? \"control disabled\" : \" control enabled\" }}\r\n          </button>\r\n          <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"ctrl.setValue(null)\">Clear</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom decimal rating</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>Custom rating template provided via a variable. Shows fine-grained rating display</p>\r\n          <ng-template #t let-fill=\"fill\">\r\n            <span class=\"star\" [class.full]=\"fill === 100\">\r\n              <span class=\"half\" [style.width.%]=\"fill\">&hearts;</span>&hearts;\r\n            </span>\r\n          </ng-template>\r\n          <ngb-rating [(rate)]=\"heartRate\" [starTemplate]=\"t\" [readonly]=\"true\" max=\"5\"></ngb-rating>\r\n          <hr>\r\n          <pre>Rate: <b>{{heartRate}}</b></pre>\r\n          <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"heartRate = 1.35\">1.35</button>\r\n          <button class=\"btn btn-sm btn-outline-primary mr-2\" (click)=\"heartRate = 4.72\">4.72</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom star template</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>Custom rating template provided as child element</p>\r\n          <ngb-rating [(rate)]=\"currentRate\">\r\n            <ng-template let-fill=\"fill\" let-index=\"index\">\r\n              <span class=\"star\" [class.filled]=\"fill === 100\" [class.bad]=\"index < 3\">&#9733;</span>\r\n            </ng-template>\r\n          </ngb-rating>\r\n          <hr>\r\n          <pre class=\"mb-0\">Rate: <b>{{currentRate}}</b></pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/tabset/tabset.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/tabset/tabset.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Tabset</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset>\r\n            <ngb-tab title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n              <ng-template ngbTabContent>\r\n                <p>Sed commodo, leo at suscipit dictum, quam est porttitor sapien, eget sodales nibh elit id diam. Nulla\r\n                  facilisi. Donec egestas ligula vitae odio interdum aliquet. Duis lectus turpis, luctus eget tincidunt\r\n                  eu, congue et odio. Duis pharetra et nisl at faucibus. Quisque luctus pulvinar arcu, et molestie\r\n                  lectus ultrices et. Sed diam urna, egestas ut ipsum vel, volutpat volutpat neque. Praesent fringilla\r\n                  tortor arcu. Vivamus faucibus nisl enim, nec tristique ipsum euismod facilisis. Morbi ut bibendum est,\r\n                  eu tincidunt odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus\r\n                  mus. Mauris aliquet odio ac lorem aliquet ultricies in eget neque. Phasellus nec tortor vel tellus\r\n                  pulvinar feugiat.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Pills</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset type=\"pills\">\r\n            <ngb-tab title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n              <ng-template ngbTabContent>\r\n                <p>Sed commodo, leo at suscipit dictum, quam est porttitor sapien, eget sodales nibh elit id diam. Nulla\r\n                  facilisi. Donec egestas ligula vitae odio interdum aliquet. Duis lectus turpis, luctus eget tincidunt\r\n                  eu, congue et odio. Duis pharetra et nisl at faucibus. Quisque luctus pulvinar arcu, et molestie\r\n                  lectus ultrices et. Sed diam urna, egestas ut ipsum vel, volutpat volutpat neque. Praesent fringilla\r\n                  tortor arcu. Vivamus faucibus nisl enim, nec tristique ipsum euismod facilisis. Morbi ut bibendum est,\r\n                  eu tincidunt odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus\r\n                  mus. Mauris aliquet odio ac lorem aliquet ultricies in eget neque. Phasellus nec tortor vel tellus\r\n                  pulvinar feugiat.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Select an active tab by id</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset #t=\"ngbTabset\">\r\n            <ngb-tab id=\"tab-selectbyid1\" title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab id=\"tab-selectbyid2\">\r\n              <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n\r\n          <p class=\"mt-2 tabset-group-btn-xs\">\r\n            <button class=\"btn btn-outline-primary btn-position\" (click)=\"t.select('tab-selectbyid2')\">Selected tab with\r\n              \"tab-selectbyid2\" id</button>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Prevent tab change</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset (tabChange)=\"beforeChange($event)\">\r\n            <ngb-tab id=\"tab-preventchange1\" title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab id=\"tab-preventchange2\" title=\"I can't be selected...\">\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab title=\"But I can!\">\r\n              <ng-template ngbTabContent>\r\n                <p>Sed commodo, leo at suscipit dictum, quam est porttitor sapien, eget sodales nibh elit id diam. Nulla\r\n                  facilisi. Donec egestas ligula vitae odio interdum aliquet. Duis lectus turpis, luctus eget tincidunt\r\n                  eu, congue et odio. Duis pharetra et nisl at faucibus. Quisque luctus pulvinar arcu, et molestie\r\n                  lectus ultrices et. Sed diam urna, egestas ut ipsum vel, volutpat volutpat neque. Praesent fringilla\r\n                  tortor arcu. Vivamus faucibus nisl enim, nec tristique ipsum euismod facilisis. Morbi ut bibendum est,\r\n                  eu tincidunt odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus\r\n                  mus. Mauris aliquet odio ac lorem aliquet ultricies in eget neque. Phasellus nec tortor vel tellus\r\n                  pulvinar feugiat.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Nav justification</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset [justify]=\"currentJustify\">\r\n            <ngb-tab title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab title=\"A very long nav title\">\r\n              <ng-template ngbTabContent>\r\n                <p>Sed commodo, leo at suscipit dictum, quam est porttitor sapien, eget sodales nibh elit id diam. Nulla\r\n                  facilisi. Donec egestas ligula vitae odio interdum aliquet. Duis lectus turpis, luctus eget tincidunt\r\n                  eu, congue et odio. Duis pharetra et nisl at faucibus. Quisque luctus pulvinar arcu, et molestie\r\n                  lectus ultrices et. Sed diam urna, egestas ut ipsum vel, volutpat volutpat neque. Praesent fringilla\r\n                  tortor arcu. Vivamus faucibus nisl enim, nec tristique ipsum euismod facilisis. Morbi ut bibendum est,\r\n                  eu tincidunt odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus\r\n                  mus. Mauris aliquet odio ac lorem aliquet ultricies in eget neque. Phasellus nec tortor vel tellus\r\n                  pulvinar feugiat.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n          <div class=\"btn-group btn-group-toggle mt-2 tabset-group-btn-xs\" ngbRadioGroup [(ngModel)]=\"currentJustify\">\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm btn-position\">\r\n              <input ngbButton type=\"radio\" value=\"start\">Start\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm btn-position\">\r\n              <input ngbButton type=\"radio\" value=\"center\">Center\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm btn-position\">\r\n              <input ngbButton type=\"radio\" value=\"end\">End\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm btn-position\">\r\n              <input ngbButton type=\"radio\" value=\"fill\">Fill\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm btn-position\">\r\n              <input ngbButton type=\"radio\" value=\"justified\">Justified\r\n            </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Nav justification</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-tabset type=\"pills\" [orientation]=\"currentOrientation\">\r\n            <ngb-tab title=\"Simple\">\r\n              <ng-template ngbTabContent>\r\n                <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,\r\n                  retro synth\r\n                  master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher\r\n                  retro keffiyeh\r\n                  dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat\r\n                  salvia cillum\r\n                  iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n              <ng-template ngbTabContent>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin\r\n                coffee squid.\r\n                <p>Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko\r\n                  farm-to-table\r\n                  craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts\r\n                  ullamco ad vinyl\r\n                  cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica\r\n                  VHS salvia\r\n                  yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit,\r\n                  sustainable jean\r\n                  shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr\r\n                  butcher vero\r\n                  sint qui sapiente accusamus tattooed echo park.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n              <ng-template ngbTabContent>\r\n                <p>Sed commodo, leo at suscipit dictum, quam est porttitor sapien, eget sodales nibh elit id diam. Nulla\r\n                  facilisi. Donec egestas ligula vitae odio interdum aliquet. Duis lectus turpis, luctus eget tincidunt\r\n                  eu, congue et odio. Duis pharetra et nisl at faucibus. Quisque luctus pulvinar arcu, et molestie\r\n                  lectus ultrices et. Sed diam urna, egestas ut ipsum vel, volutpat volutpat neque. Praesent fringilla\r\n                  tortor arcu. Vivamus faucibus nisl enim, nec tristique ipsum euismod facilisis. Morbi ut bibendum est,\r\n                  eu tincidunt odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus\r\n                  mus. Mauris aliquet odio ac lorem aliquet ultricies in eget neque. Phasellus nec tortor vel tellus\r\n                  pulvinar feugiat.</p>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n          <div class=\"btn-group btn-group-toggle mt-2\" ngbRadioGroup [(ngModel)]=\"currentOrientation\">\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm\">\r\n              <input ngbButton type=\"radio\" value=\"horizontal\">Horizontal\r\n            </label>\r\n            <label ngbButtonLabel class=\"btn-outline-primary btn-sm\">\r\n              <input ngbButton type=\"radio\" value=\"vertical\">Vertical\r\n            </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/timepicker/timepicker.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/timepicker/timepicker.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Basic Timepicker</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-timepicker [(ngModel)]=\"time\" class=\"time-picker-custom\"></ngb-timepicker>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected time: {{time | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Meridian</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-timepicker [(ngModel)]=\"time\" [meridian]=\"meridian\" class=\"time-picker-custom\"></ngb-timepicker>\r\n          <button class=\"btn btn-sm btn-outline-{{meridian ? 'primary' : 'danger'}}\" (click)=\"toggleMeridian()\">\r\n            Meridian - {{meridian ? \"ON\" : \"OFF\"}}\r\n          </button>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected time: {{time | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Seconds</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-timepicker [(ngModel)]=\"timeSeccond\" [seconds]=\"seconds\" class=\"time-picker-custom\"></ngb-timepicker>\r\n          <button class=\"btn btn-sm btn-outline-{{seconds ? 'primary' : 'danger'}}\" (click)=\"toggleSeconds()\">\r\n            Seconds - {{seconds ? \"ON\" : \"OFF\"}}\r\n          </button>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected time: {{timeSeccond | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Spinners</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-timepicker [(ngModel)]=\"timeSpinners\" [spinners]=\"spinners\" class=\"time-picker-custom\"></ngb-timepicker>\r\n          <hr />\r\n          <button class=\"m-t-1 btn btn-sm btn-outline-{{spinners ? 'primary' : 'danger'}}\" (click)=\"toggleSpinners()\">\r\n            Spinners - {{spinners ? \"ON\" : \"OFF\"}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom steps</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <ngb-timepicker [(ngModel)]=\"timeCustom\" [seconds]=\"true\" [hourStep]=\"hourStep\" [minuteStep]=\"minuteStep\"\r\n            [secondStep]=\"secondStep\" class=\"time-picker-custom\"></ngb-timepicker>\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-3\">\r\n              <label for=\"changeHourStep\">Hour Step</label>\r\n              <input id=\"changeHourStep\" type=\"number\" class=\"form-control form-control-sm\" [(ngModel)]=\"hourStep\" />\r\n            </div>\r\n            <div class=\"col-sm-3 xs-mt-timer-steps\">\r\n              <label for=\"changeMinuteStep\">Minute Step</label>\r\n              <input id=\"changeMinuteStep\" type=\"number\" class=\"form-control form-control-sm\"\r\n                [(ngModel)]=\"minuteStep\" />\r\n            </div>\r\n            <div class=\"col-sm-3 xs-mt-timer-steps\">\r\n              <label for=\"changeSecondStep\">Second Step</label>\r\n              <input id=\"changeSecondStep\" type=\"number\" class=\"form-control form-control-sm\"\r\n                [(ngModel)]=\"secondStep\" />\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected time: {{timeCustom | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom validation</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>Illustrates custom validation, you have to select time between 12:00 and 13:59</p>\r\n          <div class=\"form-group\">\r\n            <ngb-timepicker [formControl]=\"ctrl\" class=\"time-picker-custom\" required></ngb-timepicker>\r\n            <div *ngIf=\"ctrl.valid\" class=\"small form-text text-primary\">Great choice</div>\r\n            <div class=\"small form-text text-danger\" *ngIf=\"!ctrl.valid\">\r\n              <div *ngIf=\"ctrl.errors['required']\">Select some time during lunchtime</div>\r\n              <div *ngIf=\"ctrl.errors['tooLate']\">Oh no, it's way too late</div>\r\n              <div *ngIf=\"ctrl.errors['tooEarly']\">It's a bit too early</div>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">Selected time: {{ctrl.value | json}}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/tooltip/tooltip.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/tooltip/tooltip.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Quick and easy tooltips</h5>\r\n        </div>\r\n        <div class=\"card-body btn-showcase\">\r\n          <button type=\"button\" class=\"btn btn-outline-primary mr-2\" placement=\"top\" ngbTooltip=\"Tooltip on top\">\r\n            Tooltip on top\r\n          </button>\r\n          <button type=\"button\" class=\"btn btn-outline-primary mr-2\" placement=\"right\" ngbTooltip=\"Tooltip on right\">\r\n            Tooltip on right\r\n          </button>\r\n          <button type=\"button\" class=\"btn btn-outline-primary mr-2\" placement=\"bottom\" ngbTooltip=\"Tooltip on bottom\">\r\n            Tooltip on bottom\r\n          </button>\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" placement=\"left\" ngbTooltip=\"Tooltip on left\">\r\n            Tooltip on left\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>HTML and bindings in tooltips</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            Tooltips can contain any arbitrary HTML, Angular bindings and even directives!\r\n            Simply enclose desired content in a <code>&lt;ng-template&gt;</code> element.\r\n          </p>\r\n\r\n          <ng-template #tipContent>Hello, <b>{{name}}</b>!</ng-template>\r\n          <button type=\"button\" class=\"btn btn-outline-primary btn-xs-eplisis\" [ngbTooltip]=\"tipContent\">\r\n            I've got markup and bindings in my tooltip!\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Custom and manual triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can easily override open and close triggers by specifying event names (separated by <code>:</code>) in\r\n            the <code>triggers</code> property.\r\n          </p>\r\n\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" ngbTooltip=\"You see, I show up on click!\"\r\n            triggers=\"click:blur\">\r\n            Click me!\r\n          </button>\r\n\r\n          <hr>\r\n          <p>\r\n            Alternatively you can take full manual control over tooltip opening / closing events.\r\n          </p>\r\n          <div class=\"btn-showcase\">\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" ngbTooltip=\"What a great tip!\"\r\n              [autoClose]=\"false\" triggers=\"manual\" #t=\"ngbTooltip\" (click)=\"t.open()\">\r\n              Click me to open a tooltip\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"t.close()\">\r\n              Click me to close a tooltip\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Automatic closing with keyboard and mouse</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>As for some other popup-based widgets, you can set the tooltip to close automatically upon some events.</p>\r\n          <p>In the following examples, they will all close on <code>Escape</code> as well as:</p>\r\n\r\n          <ul>\r\n            <li class=\"mb-2\">\r\n              click inside:\r\n              <button type=\"button\" class=\"btn btn-outline-primary\" triggers=\"click\" [autoClose]=\"'inside'\"\r\n                ngbTooltip=\"Click inside or press Escape to close\">\r\n                Click to toggle\r\n              </button>\r\n            </li>\r\n\r\n            <li class=\"mb-2\">\r\n              click outside:\r\n              <button type=\"button\" class=\"btn btn-outline-primary\" triggers=\"click\" [autoClose]=\"'outside'\"\r\n                ngbTooltip=\"Click outside or press Escape to close\">\r\n                Click to toggle\r\n              </button>\r\n            </li>\r\n\r\n            <li class=\"mb-2\">\r\n              all clicks:\r\n              <div class=\"d-inline-block btn-showcase\">\r\n                <button type=\"button\" class=\"btn btn-outline-primary mr-0\" triggers=\"click\" [autoClose]=\"true\"\r\n                  ngbTooltip=\"Click anywhere or press Escape to close (try the toggling element too)\"\r\n                  #tooltip3=\"ngbTooltip\">\r\n                  Click to toggle\r\n                </button>\r\n\r\n                &nbsp;\r\n\r\n                <button type=\"button\" class=\"btn btn-outline-primary btn-xs-eplisis\" (click)=\"tooltip3.toggle()\">\r\n                  Click to toggle the external tooltip\r\n                </button>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Context and manual triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can optionally pass in a context when manually triggering a tooltip.\r\n          </p>\r\n\r\n          <ng-template #tipContent let-greeting=\"greeting\">{{greeting}}, <b>{{name}}</b>!</ng-template>\r\n          <div class=\"btn-showcase\">\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbTooltip]=\"tipContent\" triggers=\"manual\"\r\n              #t1=\"ngbTooltip\" (click)=\"toggleWithGreeting(t1, 'Bonjour')\">\r\n              French\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbTooltip]=\"tipContent\" triggers=\"manual\"\r\n              #t2=\"ngbTooltip\" (click)=\"toggleWithGreeting(t2, 'Gutentag')\">\r\n              German\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-outline-primary mr-2\" [ngbTooltip]=\"tipContent\" triggers=\"manual\"\r\n              #t3=\"ngbTooltip\" (click)=\"toggleWithGreeting(t3, 'Hello')\">\r\n              English\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Context and manual triggers</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>\r\n            You can optionally pass in a custom class via <code>tooltipClass</code>\r\n          </p>\r\n          <button type=\"button\" class=\"btn btn-outline-primary\" ngbTooltip=\"Nice class!\" tooltipClass=\"my-custom-class\">\r\n            Tooltip with custom class\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/base/typeahead/typeahead.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/base/typeahead/typeahead.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Simple Typeahead</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          A typeahead example that gets values from a static <code>string[]</code>\r\n          <ul>\r\n            <li><code>debounceTime</code> operator</li>\r\n            <li>kicks in only if 2+ characters typed</li>\r\n            <li>limits to 10 results</li>\r\n          </ul>\r\n          <label for=\"typeahead-basic\">Search for a state:</label>\r\n          <input id=\"typeahead-basic\" type=\"text\" class=\"form-control\" [(ngModel)]=\"model\" [ngbTypeahead]=\"search\" />\r\n          <hr>\r\n          <pre class=\"mb-0\">Model: {{ model | json }}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Open on focus</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          A typeahead example that gets values from a static <code>string[]</code>\r\n          <ul>\r\n            <li><code>debounceTime</code> operator</li>\r\n            <li>kicks in only if 2+ characters typed</li>\r\n            <li>limits to 10 results</li>\r\n          </ul>\r\n          <label for=\"typeahead-basic\">Search for a state:</label>\r\n          <input id=\"typeahead-basic\" type=\"text\" class=\"form-control\" [(ngModel)]=\"modelFocus\"\r\n            [ngbTypeahead]=\"searchOnFocus\" />\r\n          <hr>\r\n          <pre class=\"mb-0\">Model: {{ modelFocus | json }}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Wikipedia search</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          A typeahead example that gets values from the <code>WikipediaService</code>\r\n          <ul>\r\n            <li>remote data retrieval</li>\r\n            <li><code>debounceTime</code> operator</li>\r\n            <li><code>do</code> operator</li>\r\n            <li><code>distinctUntilChanged</code> operator</li>\r\n            <li><code>switchMap</code> operator</li>\r\n            <li><code>catch</code> operator to display an error message in case of connectivity issue</li>\r\n          </ul>\r\n          <div class=\"form-group\">\r\n            <label for=\"typeahead-http\">Search for a wiki page:</label>\r\n            <input id=\"typeahead-http\" type=\"text\" class=\"form-control\" [class.is-invalid]=\"searchFailed\"\r\n              [(ngModel)]=\"modelWiki\" [ngbTypeahead]=\"searchWikipedia\" placeholder=\"Wikipedia search\" />\r\n            <span *ngIf=\"searching\">searching...</span>\r\n            <div class=\"invalid-feedback\" *ngIf=\"searchFailed\">Sorry, suggestions could not be loaded.</div>\r\n          </div>\r\n          <hr>\r\n          <pre class=\"mb-0\">Model: {{ modelWiki | json }}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Template for results</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <p>A typeahead example that uses a custom template for results display, an object as the model,\r\n            and the highlight directive to highlight the term inside the custom template.\r\n          </p>\r\n          <ng-template #rt let-r=\"result\" let-t=\"term\">\r\n            <img [src]=\"'https://upload.wikimedia.org/wikipedia/commons/thumb/' + r['flag']\" class=\"mr-1\"\r\n              style=\"width: 16px\">\r\n            <ngb-highlight [result]=\"r.name\" [term]=\"t\"></ngb-highlight>\r\n          </ng-template>\r\n          <label for=\"typeahead-template\">Search for a state:</label>\r\n          <input id=\"typeahead-template\" type=\"text\" class=\"form-control\" [(ngModel)]=\"modelTemp\"\r\n            [ngbTypeahead]=\"searchTemp\" [resultTemplate]=\"rt\" [inputFormatter]=\"formatter\" />\r\n          <hr>\r\n          <pre class=\"mb-0\">Model: {{ modelTemp | json }}</pre>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/base/accordion/accordion.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/components/base/accordion/accordion.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9hY2NvcmRpb24vYWNjb3JkaW9uLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/base/accordion/accordion.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/base/accordion/accordion.component.ts ***!
  \******************************************************************/
/*! exports provided: AccordionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccordionComponent", function() { return AccordionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AccordionComponent = /** @class */ (function () {
    function AccordionComponent() {
    }
    AccordionComponent.prototype.ngOnInit = function () { };
    AccordionComponent.prototype.beforeChange = function (e) { };
    AccordionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-accordion',
            template: __webpack_require__(/*! raw-loader!./accordion.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/accordion/accordion.component.html"),
            styles: [__webpack_require__(/*! ./accordion.component.scss */ "./src/app/components/base/accordion/accordion.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AccordionComponent);
    return AccordionComponent;
}());



/***/ }),

/***/ "./src/app/components/base/alert/alert.component.scss":
/*!************************************************************!*\
  !*** ./src/app/components/base/alert/alert.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9hbGVydC9hbGVydC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/alert/alert.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/base/alert/alert.component.ts ***!
  \**********************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ALERTS = [{
        type: 'success',
        message: 'This is an success alert',
    },
    {
        type: 'info',
        message: 'This is an info alert',
    },
    {
        type: 'warning',
        message: 'This is a warning alert',
    },
    {
        type: 'danger',
        message: 'This is a danger alert',
    },
    {
        type: 'primary',
        message: 'This is a primary alert',
    },
    {
        type: 'secondary',
        message: 'This is a secondary alert',
    },
    {
        type: 'light',
        message: 'This is a light alert',
    },
    {
        type: 'dark',
        message: 'This is a dark alert',
    }
];
var AlertComponent = /** @class */ (function () {
    function AlertComponent() {
        this._success = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.staticAlertClosed = false;
        this.reset();
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.staticAlertClosed = true; }, 20000);
        this._success.subscribe(function (message) { return _this.successMessage = message; });
        this._success.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(5000)).subscribe(function () { return _this.successMessage = null; });
    };
    AlertComponent.prototype.changeSuccessMessage = function () {
        this._success.next(new Date() + " - Message successfully changed.");
    };
    AlertComponent.prototype.close = function (alert) {
        this.alerts.splice(this.alerts.indexOf(alert), 1);
    };
    AlertComponent.prototype.reset = function () {
        this.alerts = Array.from(ALERTS);
    };
    AlertComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__(/*! raw-loader!./alert.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/alert/alert.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./alert.component.scss */ "./src/app/components/base/alert/alert.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/components/base/base-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/components/base/base-routing.module.ts ***!
  \********************************************************/
/*! exports provided: BaseRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseRoutingModule", function() { return BaseRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _accordion_accordion_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accordion/accordion.component */ "./src/app/components/base/accordion/accordion.component.ts");
/* harmony import */ var _alert_alert_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./alert/alert.component */ "./src/app/components/base/alert/alert.component.ts");
/* harmony import */ var _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./buttons/buttons.component */ "./src/app/components/base/buttons/buttons.component.ts");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/components/base/carousel/carousel.component.ts");
/* harmony import */ var _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./collapse/collapse.component */ "./src/app/components/base/collapse/collapse.component.ts");
/* harmony import */ var _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./datepicker/datepicker.component */ "./src/app/components/base/datepicker/datepicker.component.ts");
/* harmony import */ var _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dropdown/dropdown.component */ "./src/app/components/base/dropdown/dropdown.component.ts");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./modal/modal.component */ "./src/app/components/base/modal/modal.component.ts");
/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/components/base/pagination/pagination.component.ts");
/* harmony import */ var _popover_popover_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./popover/popover.component */ "./src/app/components/base/popover/popover.component.ts");
/* harmony import */ var _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./progressbar/progressbar.component */ "./src/app/components/base/progressbar/progressbar.component.ts");
/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/components/base/rating/rating.component.ts");
/* harmony import */ var _tabset_tabset_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./tabset/tabset.component */ "./src/app/components/base/tabset/tabset.component.ts");
/* harmony import */ var _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./timepicker/timepicker.component */ "./src/app/components/base/timepicker/timepicker.component.ts");
/* harmony import */ var _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./tooltip/tooltip.component */ "./src/app/components/base/tooltip/tooltip.component.ts");
/* harmony import */ var _typeahead_typeahead_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./typeahead/typeahead.component */ "./src/app/components/base/typeahead/typeahead.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var routes = [
    {
        path: '',
        children: [
            {
                path: 'accordion',
                component: _accordion_accordion_component__WEBPACK_IMPORTED_MODULE_2__["AccordionComponent"],
                data: {
                    title: "Accordion",
                    breadcrumb: "Accordion"
                }
            },
            {
                path: 'alert',
                component: _alert_alert_component__WEBPACK_IMPORTED_MODULE_3__["AlertComponent"],
                data: {
                    title: "Alert",
                    breadcrumb: "Alert"
                }
            },
            {
                path: 'buttons',
                component: _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_4__["ButtonsComponent"],
                data: {
                    title: "Buttons",
                    breadcrumb: "Buttons"
                }
            },
            {
                path: 'carousel',
                component: _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselComponent"],
                data: {
                    title: "Carousel",
                    breadcrumb: "Carousel"
                }
            },
            {
                path: 'collapse',
                component: _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_6__["CollapseComponent"],
                data: {
                    title: "Collapse",
                    breadcrumb: "Collapse"
                }
            },
            {
                path: 'datepicker',
                component: _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_7__["DatepickerComponent"],
                data: {
                    title: "Datepicker",
                    breadcrumb: "Datepicker"
                }
            },
            {
                path: 'dropdown',
                component: _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_8__["DropdownComponent"],
                data: {
                    title: "Dropdown",
                    breadcrumb: "Dropdown"
                }
            },
            {
                path: 'modal',
                component: _modal_modal_component__WEBPACK_IMPORTED_MODULE_9__["ModalComponent"],
                data: {
                    title: "Modal",
                    breadcrumb: "Modal"
                }
            },
            {
                path: 'pagination',
                component: _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_10__["PaginationComponent"],
                data: {
                    title: "Pagination",
                    breadcrumb: "Pagination"
                }
            },
            {
                path: 'popover',
                component: _popover_popover_component__WEBPACK_IMPORTED_MODULE_11__["PopoverComponent"],
                data: {
                    title: "Popover",
                    breadcrumb: "Popover"
                }
            },
            {
                path: 'progressbar',
                component: _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_12__["ProgressbarComponent"],
                data: {
                    title: "Progressbar",
                    breadcrumb: "Progressbar"
                }
            },
            {
                path: 'rating',
                component: _rating_rating_component__WEBPACK_IMPORTED_MODULE_13__["RatingComponent"],
                data: {
                    title: "Rating",
                    breadcrumb: "Rating"
                }
            },
            {
                path: 'tabset',
                component: _tabset_tabset_component__WEBPACK_IMPORTED_MODULE_14__["TabsetComponent"],
                data: {
                    title: "Tabset",
                    breadcrumb: "Tabset"
                }
            },
            {
                path: 'timepicker',
                component: _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_15__["TimepickerComponent"],
                data: {
                    title: "TimePicker",
                    breadcrumb: "TimePicker"
                }
            },
            {
                path: 'tooltip',
                component: _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_16__["TooltipComponent"],
                data: {
                    title: "Tooltip",
                    breadcrumb: "Tooltip"
                }
            },
            {
                path: 'typeahead',
                component: _typeahead_typeahead_component__WEBPACK_IMPORTED_MODULE_17__["TypeaheadComponent"],
                data: {
                    title: "Typeahead",
                    breadcrumb: "Typeahead"
                }
            }
        ]
    }
];
var BaseRoutingModule = /** @class */ (function () {
    function BaseRoutingModule() {
    }
    BaseRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BaseRoutingModule);
    return BaseRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/base/base.module.ts":
/*!************************************************!*\
  !*** ./src/app/components/base/base.module.ts ***!
  \************************************************/
/*! exports provided: BaseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseModule", function() { return BaseModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _base_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./base-routing.module */ "./src/app/components/base/base-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _accordion_accordion_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./accordion/accordion.component */ "./src/app/components/base/accordion/accordion.component.ts");
/* harmony import */ var _alert_alert_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./alert/alert.component */ "./src/app/components/base/alert/alert.component.ts");
/* harmony import */ var _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./buttons/buttons.component */ "./src/app/components/base/buttons/buttons.component.ts");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/components/base/carousel/carousel.component.ts");
/* harmony import */ var _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./collapse/collapse.component */ "./src/app/components/base/collapse/collapse.component.ts");
/* harmony import */ var _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./datepicker/datepicker.component */ "./src/app/components/base/datepicker/datepicker.component.ts");
/* harmony import */ var _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dropdown/dropdown.component */ "./src/app/components/base/dropdown/dropdown.component.ts");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./modal/modal.component */ "./src/app/components/base/modal/modal.component.ts");
/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/components/base/pagination/pagination.component.ts");
/* harmony import */ var _popover_popover_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./popover/popover.component */ "./src/app/components/base/popover/popover.component.ts");
/* harmony import */ var _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./progressbar/progressbar.component */ "./src/app/components/base/progressbar/progressbar.component.ts");
/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/components/base/rating/rating.component.ts");
/* harmony import */ var _tabset_tabset_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./tabset/tabset.component */ "./src/app/components/base/tabset/tabset.component.ts");
/* harmony import */ var _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./timepicker/timepicker.component */ "./src/app/components/base/timepicker/timepicker.component.ts");
/* harmony import */ var _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./tooltip/tooltip.component */ "./src/app/components/base/tooltip/tooltip.component.ts");
/* harmony import */ var _typeahead_typeahead_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./typeahead/typeahead.component */ "./src/app/components/base/typeahead/typeahead.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var BaseModule = /** @class */ (function () {
    function BaseModule() {
    }
    BaseModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_accordion_accordion_component__WEBPACK_IMPORTED_MODULE_5__["AccordionComponent"], _alert_alert_component__WEBPACK_IMPORTED_MODULE_6__["AlertComponent"], _buttons_buttons_component__WEBPACK_IMPORTED_MODULE_7__["ButtonsComponent"], _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_8__["CarouselComponent"], _collapse_collapse_component__WEBPACK_IMPORTED_MODULE_9__["CollapseComponent"], _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_10__["DatepickerComponent"], _dropdown_dropdown_component__WEBPACK_IMPORTED_MODULE_11__["DropdownComponent"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["ModalComponent"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModalContent"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModal1Content"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModal2Content"], _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_13__["PaginationComponent"], _popover_popover_component__WEBPACK_IMPORTED_MODULE_14__["PopoverComponent"], _progressbar_progressbar_component__WEBPACK_IMPORTED_MODULE_15__["ProgressbarComponent"], _rating_rating_component__WEBPACK_IMPORTED_MODULE_16__["RatingComponent"], _tabset_tabset_component__WEBPACK_IMPORTED_MODULE_17__["TabsetComponent"], _timepicker_timepicker_component__WEBPACK_IMPORTED_MODULE_18__["TimepickerComponent"], _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_19__["TooltipComponent"], _typeahead_typeahead_component__WEBPACK_IMPORTED_MODULE_20__["TypeaheadComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _base_routing_module__WEBPACK_IMPORTED_MODULE_2__["BaseRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"]
            ],
            entryComponents: [_modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModalContent"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModal1Content"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_12__["NgbdModal2Content"]]
        })
    ], BaseModule);
    return BaseModule;
}());



/***/ }),

/***/ "./src/app/components/base/buttons/buttons.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/components/base/buttons/buttons.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9idXR0b25zL2J1dHRvbnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/buttons/buttons.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/base/buttons/buttons.component.ts ***!
  \**************************************************************/
/*! exports provided: ButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonsComponent", function() { return ButtonsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ButtonsComponent = /** @class */ (function () {
    function ButtonsComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.model = {
            left: true,
            middle: false,
            right: false
        };
        this.modelRadio = 1;
    }
    ButtonsComponent.prototype.ngOnInit = function () {
        this.checkboxGroupForm = this.formBuilder.group({
            left: true,
            middle: false,
            right: false
        });
        this.radioGroupForm = this.formBuilder.group({
            'model': 1
        });
    };
    ButtonsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-buttons',
            template: __webpack_require__(/*! raw-loader!./buttons.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/buttons/buttons.component.html"),
            styles: [__webpack_require__(/*! ./buttons.component.scss */ "./src/app/components/base/buttons/buttons.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], ButtonsComponent);
    return ButtonsComponent;
}());



/***/ }),

/***/ "./src/app/components/base/carousel/carousel.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/base/carousel/carousel.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9jYXJvdXNlbC9jYXJvdXNlbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/carousel/carousel.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/base/carousel/carousel.component.ts ***!
  \****************************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CarouselComponent = /** @class */ (function () {
    function CarouselComponent(config) {
        this.showNavigationArrows = false;
        this.showNavigationIndicators = false;
        this.images = ['assets/images/c1.jpg', 'assets/images/c2.jpg', 'assets/images/c3.jpg'];
        // customize default values of carousels used by this component tree
        config.showNavigationArrows = true;
        config.showNavigationIndicators = true;
        // customize default values of carousels used by this component tree
        config.interval = 10000;
        config.wrap = false;
        config.keyboard = false;
        config.pauseOnHover = false;
    }
    CarouselComponent.prototype.ngOnInit = function () { };
    CarouselComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__(/*! raw-loader!./carousel.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/carousel/carousel.component.html"),
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbCarouselConfig"]] // add NgbCarouselConfig to the component providers
            ,
            styles: [__webpack_require__(/*! ./carousel.component.scss */ "./src/app/components/base/carousel/carousel.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbCarouselConfig"]])
    ], CarouselComponent);
    return CarouselComponent;
}());



/***/ }),

/***/ "./src/app/components/base/collapse/collapse.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/base/collapse/collapse.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9jb2xsYXBzZS9jb2xsYXBzZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/collapse/collapse.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/base/collapse/collapse.component.ts ***!
  \****************************************************************/
/*! exports provided: CollapseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollapseComponent", function() { return CollapseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CollapseComponent = /** @class */ (function () {
    function CollapseComponent() {
        this.isCollapsed = false;
    }
    CollapseComponent.prototype.ngOnInit = function () { };
    CollapseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-collapse',
            template: __webpack_require__(/*! raw-loader!./collapse.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/collapse/collapse.component.html"),
            styles: [__webpack_require__(/*! ./collapse.component.scss */ "./src/app/components/base/collapse/collapse.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CollapseComponent);
    return CollapseComponent;
}());



/***/ }),

/***/ "./src/app/components/base/datepicker/datepicker.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/base/datepicker/datepicker.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9kYXRlcGlja2VyL2RhdGVwaWNrZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/datepicker/datepicker.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/base/datepicker/datepicker.component.ts ***!
  \********************************************************************/
/*! exports provided: DatepickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatepickerComponent", function() { return DatepickerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DatepickerComponent = /** @class */ (function () {
    function DatepickerComponent(calendar) {
        var _this = this;
        this.calendar = calendar;
        this.disabled = true;
        this.displayMonths = 2;
        this.navigation = 'select';
        this.showWeekNumbers = false;
        this.outsideDays = 'visible';
        this.today = this.calendar.getToday();
        this.isDisabled = function (date, current) { return date.month !== current.month; };
        this.isWeekend = function (date) { return _this.calendar.getWeekday(date) >= 6; };
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    DatepickerComponent.prototype.ngOnInit = function () { };
    DatepickerComponent.prototype.selectToday = function () {
        this.model = this.calendar.getToday();
    };
    DatepickerComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    DatepickerComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    };
    DatepickerComponent.prototype.isInside = function (date) {
        return date.after(this.fromDate) && date.before(this.toDate);
    };
    DatepickerComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    };
    DatepickerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-datepicker',
            template: __webpack_require__(/*! raw-loader!./datepicker.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/datepicker/datepicker.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./datepicker.component.scss */ "./src/app/components/base/datepicker/datepicker.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbCalendar"]])
    ], DatepickerComponent);
    return DatepickerComponent;
}());



/***/ }),

/***/ "./src/app/components/base/dropdown/dropdown.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/base/dropdown/dropdown.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9kcm9wZG93bi9kcm9wZG93bi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/dropdown/dropdown.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/base/dropdown/dropdown.component.ts ***!
  \****************************************************************/
/*! exports provided: DropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownComponent", function() { return DropdownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DropdownComponent = /** @class */ (function () {
    function DropdownComponent(config) {
        // customize default values of dropdowns used by this component tree
        config.placement = 'top-left';
        config.autoClose = false;
    }
    DropdownComponent.prototype.ngOnInit = function () { };
    DropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dropdown',
            template: __webpack_require__(/*! raw-loader!./dropdown.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/dropdown/dropdown.component.html"),
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]] // add NgbDropdownConfig to the component providers
            ,
            styles: [__webpack_require__(/*! ./dropdown.component.scss */ "./src/app/components/base/dropdown/dropdown.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]])
    ], DropdownComponent);
    return DropdownComponent;
}());



/***/ }),

/***/ "./src/app/components/base/modal/modal.component.scss":
/*!************************************************************!*\
  !*** ./src/app/components/base/modal/modal.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9tb2RhbC9tb2RhbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/modal/modal.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/base/modal/modal.component.ts ***!
  \**********************************************************/
/*! exports provided: NgbdModalContent, NgbdModal1Content, NgbdModal2Content, ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalContent", function() { return NgbdModalContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModal1Content", function() { return NgbdModal1Content; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModal2Content", function() { return NgbdModal2Content; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NgbdModalContent = /** @class */ (function () {
    function NgbdModalContent(activeModal) {
        this.activeModal = activeModal;
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NgbdModalContent.prototype, "name", void 0);
    NgbdModalContent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ngbd-modal-content',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Hi there!</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <p>Hello, {{name}}!</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"activeModal.close('Close click')\">Close</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"]])
    ], NgbdModalContent);
    return NgbdModalContent;
}());

var NgbdModal1Content = /** @class */ (function () {
    function NgbdModal1Content(modalService, activeModal) {
        this.modalService = modalService;
        this.activeModal = activeModal;
    }
    NgbdModal1Content.prototype.open = function () {
        this.modalService.open(NgbdModal2Content, {
            size: 'lg'
        });
    };
    NgbdModal1Content = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Hi there!</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <p>Hello, World!</p>\n      <p><button class=\"btn btn-lg btn-outline-primary\" (click)=\"open()\">Launch demo modal</button></p>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"activeModal.close('Close click')\">Close</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"]])
    ], NgbdModal1Content);
    return NgbdModal1Content;
}());

var NgbdModal2Content = /** @class */ (function () {
    function NgbdModal2Content(activeModal) {
        this.activeModal = activeModal;
    }
    NgbdModal2Content = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Hi there!</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <p>Hello, World!</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"activeModal.close('Close click')\">Close</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"]])
    ], NgbdModal2Content);
    return NgbdModal2Content;
}());

var ModalComponent = /** @class */ (function () {
    function ModalComponent(config, modalService) {
        this.modalService = modalService;
        // customize default values of modals used by this component tree
        config.backdrop = 'static';
        config.keyboard = false;
    }
    ModalComponent.prototype.ngOnInit = function () { };
    ModalComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ModalComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ModalComponent.prototype.openModal = function () {
        var modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.name = 'World';
    };
    ModalComponent.prototype.openBackDropCustomClass = function (content) {
        this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
    };
    ModalComponent.prototype.openWindowCustomClass = function (content) {
        this.modalService.open(content, { windowClass: 'dark-modal' });
    };
    ModalComponent.prototype.openSm = function (content) {
        this.modalService.open(content, { size: 'sm' });
    };
    ModalComponent.prototype.openLg = function (content) {
        this.modalService.open(content, { size: 'lg' });
    };
    ModalComponent.prototype.openVerticallyCentered = function (content) {
        this.modalService.open(content, { centered: true });
    };
    ModalComponent.prototype.openStackedModal = function () {
        this.modalService.open(NgbdModal1Content);
    };
    ModalComponent.prototype.openCustomModal = function (content) {
        this.modalService.open(content);
    };
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! raw-loader!./modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/modal/modal.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModalConfig"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"]],
            styles: [__webpack_require__(/*! ./modal.component.scss */ "./src/app/components/base/modal/modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModalConfig"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"]])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/components/base/pagination/pagination.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/base/pagination/pagination.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9wYWdpbmF0aW9uL3BhZ2luYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/pagination/pagination.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/base/pagination/pagination.component.ts ***!
  \********************************************************************/
/*! exports provided: PaginationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationComponent", function() { return PaginationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaginationComponent = /** @class */ (function () {
    function PaginationComponent(config) {
        this.page = 4;
        this.advancePage = 1;
        this.currentPage = 3;
        this.isDisabled = true;
        // customize default values of paginations used by this component tree
        config.size = 'sm';
        config.boundaryLinks = true;
    }
    PaginationComponent.prototype.ngOnInit = function () { };
    PaginationComponent.prototype.toggleDisabled = function () {
        this.isDisabled = !this.isDisabled;
    };
    PaginationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagination',
            template: __webpack_require__(/*! raw-loader!./pagination.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/pagination/pagination.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbPaginationConfig"]] // add NgbPaginationConfig to the component providers
            ,
            styles: [__webpack_require__(/*! ./pagination.component.scss */ "./src/app/components/base/pagination/pagination.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbPaginationConfig"]])
    ], PaginationComponent);
    return PaginationComponent;
}());



/***/ }),

/***/ "./src/app/components/base/popover/popover.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/components/base/popover/popover.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9wb3BvdmVyL3BvcG92ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/popover/popover.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/base/popover/popover.component.ts ***!
  \**************************************************************/
/*! exports provided: PopoverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopoverComponent", function() { return PopoverComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopoverComponent = /** @class */ (function () {
    function PopoverComponent(config) {
        this.name = 'World';
        // customize default values of popovers used by this component tree
        config.placement = 'top';
        config.triggers = 'click';
    }
    PopoverComponent.prototype.ngOnInit = function () { };
    //Tooltip greeting on click as well as hover
    PopoverComponent.prototype.toggleWithGreeting = function (popover, greeting, language) {
        if (popover.isOpen()) {
            popover.close();
        }
        else {
            popover.open({ greeting: greeting, language: language });
        }
    };
    PopoverComponent.prototype.recordShown = function () {
        this.lastShown = new Date();
    };
    PopoverComponent.prototype.recordHidden = function () {
        this.lastHidden = new Date();
    };
    PopoverComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-popover',
            template: __webpack_require__(/*! raw-loader!./popover.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/popover/popover.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbPopoverConfig"]] // add NgbPopoverConfig to the component providers
            ,
            styles: [__webpack_require__(/*! ./popover.component.scss */ "./src/app/components/base/popover/popover.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbPopoverConfig"]])
    ], PopoverComponent);
    return PopoverComponent;
}());



/***/ }),

/***/ "./src/app/components/base/progressbar/progressbar.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/base/progressbar/progressbar.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9wcm9ncmVzc2Jhci9wcm9ncmVzc2Jhci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/base/progressbar/progressbar.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/base/progressbar/progressbar.component.ts ***!
  \**********************************************************************/
/*! exports provided: ProgressbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressbarComponent", function() { return ProgressbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProgressbarComponent = /** @class */ (function () {
    function ProgressbarComponent() {
    }
    ProgressbarComponent.prototype.ngOnInit = function () { };
    ProgressbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-progressbar',
            template: __webpack_require__(/*! raw-loader!./progressbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/progressbar/progressbar.component.html"),
            styles: [__webpack_require__(/*! ./progressbar.component.scss */ "./src/app/components/base/progressbar/progressbar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProgressbarComponent);
    return ProgressbarComponent;
}());



/***/ }),

/***/ "./src/app/components/base/rating/rating.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/base/rating/rating.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS9yYXRpbmcvcmF0aW5nLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/base/rating/rating.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/base/rating/rating.component.ts ***!
  \************************************************************/
/*! exports provided: RatingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return RatingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RatingComponent = /** @class */ (function () {
    function RatingComponent() {
        this.currentRate = 6;
        this.selected = 0;
        this.hovered = 0;
        this.readonly = false;
        this.heartRate = 5;
        this.ctrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
    }
    RatingComponent.prototype.ngOnInit = function () { };
    RatingComponent.prototype.toggle = function () {
        if (this.ctrl.disabled) {
            this.ctrl.enable();
        }
        else {
            this.ctrl.disable();
        }
    };
    RatingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rating',
            template: __webpack_require__(/*! raw-loader!./rating.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/rating/rating.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./rating.component.scss */ "./src/app/components/base/rating/rating.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RatingComponent);
    return RatingComponent;
}());



/***/ }),

/***/ "./src/app/components/base/tabset/tabset.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/base/tabset/tabset.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS90YWJzZXQvdGFic2V0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/base/tabset/tabset.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/base/tabset/tabset.component.ts ***!
  \************************************************************/
/*! exports provided: TabsetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsetComponent", function() { return TabsetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TabsetComponent = /** @class */ (function () {
    function TabsetComponent() {
        this.currentJustify = 'start';
        this.currentOrientation = 'horizontal';
    }
    TabsetComponent.prototype.ngOnInit = function () { };
    TabsetComponent.prototype.beforeChange = function ($event) {
        if ($event.nextId === 'tab-preventchange2') {
            $event.preventDefault();
        }
    };
    TabsetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tabset',
            template: __webpack_require__(/*! raw-loader!./tabset.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/tabset/tabset.component.html"),
            styles: [__webpack_require__(/*! ./tabset.component.scss */ "./src/app/components/base/tabset/tabset.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TabsetComponent);
    return TabsetComponent;
}());



/***/ }),

/***/ "./src/app/components/base/timepicker/timepicker.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/base/timepicker/timepicker.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS90aW1lcGlja2VyL3RpbWVwaWNrZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/timepicker/timepicker.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/base/timepicker/timepicker.component.ts ***!
  \********************************************************************/
/*! exports provided: TimepickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimepickerComponent", function() { return TimepickerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TimepickerComponent = /** @class */ (function () {
    function TimepickerComponent() {
        this.time = { hour: 13, minute: 30 };
        this.meridian = true;
        this.timeSeccond = { hour: 13, minute: 30, second: 30 };
        this.seconds = true;
        this.timeSpinners = { hour: 13, minute: 30 };
        this.spinners = true;
        this.timeCustom = { hour: 13, minute: 30, second: 0 };
        this.hourStep = 1;
        this.minuteStep = 15;
        this.secondStep = 30;
        this.ctrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', function (control) {
            var value = control.value;
            if (!value) {
                return null;
            }
            if (value.hour < 12) {
                return { tooEarly: true };
            }
            if (value.hour > 13) {
                return { tooLate: true };
            }
            return null;
        });
    }
    TimepickerComponent.prototype.ngOnInit = function () { };
    TimepickerComponent.prototype.toggleMeridian = function () {
        this.meridian = !this.meridian;
    };
    TimepickerComponent.prototype.toggleSeconds = function () {
        this.seconds = !this.seconds;
    };
    TimepickerComponent.prototype.toggleSpinners = function () {
        this.spinners = !this.spinners;
    };
    TimepickerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timepicker',
            template: __webpack_require__(/*! raw-loader!./timepicker.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/timepicker/timepicker.component.html"),
            styles: [__webpack_require__(/*! ./timepicker.component.scss */ "./src/app/components/base/timepicker/timepicker.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TimepickerComponent);
    return TimepickerComponent;
}());



/***/ }),

/***/ "./src/app/components/base/tooltip/tooltip.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/components/base/tooltip/tooltip.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS90b29sdGlwL3Rvb2x0aXAuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/base/tooltip/tooltip.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/base/tooltip/tooltip.component.ts ***!
  \**************************************************************/
/*! exports provided: TooltipComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipComponent", function() { return TooltipComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TooltipComponent = /** @class */ (function () {
    function TooltipComponent() {
        this.name = 'World';
    }
    TooltipComponent.prototype.ngOnInit = function () { };
    //tooltip toggle
    TooltipComponent.prototype.toggleWithGreeting = function (tooltip, greeting) {
        if (tooltip.isOpen()) {
            tooltip.close();
        }
        else {
            tooltip.open({ greeting: greeting });
        }
    };
    TooltipComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tooltip',
            template: __webpack_require__(/*! raw-loader!./tooltip.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/tooltip/tooltip.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./tooltip.component.scss */ "./src/app/components/base/tooltip/tooltip.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TooltipComponent);
    return TooltipComponent;
}());



/***/ }),

/***/ "./src/app/components/base/typeahead/typeahead.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/components/base/typeahead/typeahead.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmFzZS90eXBlYWhlYWQvdHlwZWFoZWFkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/base/typeahead/typeahead.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/base/typeahead/typeahead.component.ts ***!
  \******************************************************************/
/*! exports provided: WikipediaService, TypeaheadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikipediaService", function() { return WikipediaService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeaheadComponent", function() { return TypeaheadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
    'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
    'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
    'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
    'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
    'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
var statesWithFlags = [
    { 'name': 'Alabama', 'flag': '5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png' },
    { 'name': 'Alaska', 'flag': 'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png' },
    { 'name': 'Arizona', 'flag': '9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png' },
    { 'name': 'Arkansas', 'flag': '9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png' },
    { 'name': 'California', 'flag': '0/01/Flag_of_California.svg/45px-Flag_of_California.svg.png' },
    { 'name': 'Colorado', 'flag': '4/46/Flag_of_Colorado.svg/45px-Flag_of_Colorado.svg.png' },
    { 'name': 'Connecticut', 'flag': '9/96/Flag_of_Connecticut.svg/39px-Flag_of_Connecticut.svg.png' },
    { 'name': 'Delaware', 'flag': 'c/c6/Flag_of_Delaware.svg/45px-Flag_of_Delaware.svg.png' },
    { 'name': 'Florida', 'flag': 'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png' },
    { 'name': 'Georgia', 'flag': '5/54/Flag_of_Georgia_%28U.S._state%29.svg/46px-Flag_of_Georgia_%28U.S._state%29.svg.png' },
    { 'name': 'Hawaii', 'flag': 'e/ef/Flag_of_Hawaii.svg/46px-Flag_of_Hawaii.svg.png' },
    { 'name': 'Idaho', 'flag': 'a/a4/Flag_of_Idaho.svg/38px-Flag_of_Idaho.svg.png' },
    { 'name': 'Illinois', 'flag': '0/01/Flag_of_Illinois.svg/46px-Flag_of_Illinois.svg.png' },
    { 'name': 'Indiana', 'flag': 'a/ac/Flag_of_Indiana.svg/45px-Flag_of_Indiana.svg.png' },
    { 'name': 'Iowa', 'flag': 'a/aa/Flag_of_Iowa.svg/44px-Flag_of_Iowa.svg.png' },
    { 'name': 'Kansas', 'flag': 'd/da/Flag_of_Kansas.svg/46px-Flag_of_Kansas.svg.png' },
    { 'name': 'Kentucky', 'flag': '8/8d/Flag_of_Kentucky.svg/46px-Flag_of_Kentucky.svg.png' },
    { 'name': 'Louisiana', 'flag': 'e/e0/Flag_of_Louisiana.svg/46px-Flag_of_Louisiana.svg.png' },
    { 'name': 'Maine', 'flag': '3/35/Flag_of_Maine.svg/45px-Flag_of_Maine.svg.png' },
    { 'name': 'Maryland', 'flag': 'a/a0/Flag_of_Maryland.svg/45px-Flag_of_Maryland.svg.png' },
    { 'name': 'Massachusetts', 'flag': 'f/f2/Flag_of_Massachusetts.svg/46px-Flag_of_Massachusetts.svg.png' },
    { 'name': 'Michigan', 'flag': 'b/b5/Flag_of_Michigan.svg/45px-Flag_of_Michigan.svg.png' },
    { 'name': 'Minnesota', 'flag': 'b/b9/Flag_of_Minnesota.svg/46px-Flag_of_Minnesota.svg.png' },
    { 'name': 'Mississippi', 'flag': '4/42/Flag_of_Mississippi.svg/45px-Flag_of_Mississippi.svg.png' },
    { 'name': 'Missouri', 'flag': '5/5a/Flag_of_Missouri.svg/46px-Flag_of_Missouri.svg.png' },
    { 'name': 'Montana', 'flag': 'c/cb/Flag_of_Montana.svg/45px-Flag_of_Montana.svg.png' },
    { 'name': 'Nebraska', 'flag': '4/4d/Flag_of_Nebraska.svg/46px-Flag_of_Nebraska.svg.png' },
    { 'name': 'Nevada', 'flag': 'f/f1/Flag_of_Nevada.svg/45px-Flag_of_Nevada.svg.png' },
    { 'name': 'New Hampshire', 'flag': '2/28/Flag_of_New_Hampshire.svg/45px-Flag_of_New_Hampshire.svg.png' },
    { 'name': 'New Jersey', 'flag': '9/92/Flag_of_New_Jersey.svg/45px-Flag_of_New_Jersey.svg.png' },
    { 'name': 'New Mexico', 'flag': 'c/c3/Flag_of_New_Mexico.svg/45px-Flag_of_New_Mexico.svg.png' },
    { 'name': 'New York', 'flag': '1/1a/Flag_of_New_York.svg/46px-Flag_of_New_York.svg.png' },
    { 'name': 'North Carolina', 'flag': 'b/bb/Flag_of_North_Carolina.svg/45px-Flag_of_North_Carolina.svg.png' },
    { 'name': 'North Dakota', 'flag': 'e/ee/Flag_of_North_Dakota.svg/38px-Flag_of_North_Dakota.svg.png' },
    { 'name': 'Ohio', 'flag': '4/4c/Flag_of_Ohio.svg/46px-Flag_of_Ohio.svg.png' },
    { 'name': 'Oklahoma', 'flag': '6/6e/Flag_of_Oklahoma.svg/45px-Flag_of_Oklahoma.svg.png' },
    { 'name': 'Oregon', 'flag': 'b/b9/Flag_of_Oregon.svg/46px-Flag_of_Oregon.svg.png' },
    { 'name': 'Pennsylvania', 'flag': 'f/f7/Flag_of_Pennsylvania.svg/45px-Flag_of_Pennsylvania.svg.png' },
    { 'name': 'Rhode Island', 'flag': 'f/f3/Flag_of_Rhode_Island.svg/32px-Flag_of_Rhode_Island.svg.png' },
    { 'name': 'South Carolina', 'flag': '6/69/Flag_of_South_Carolina.svg/45px-Flag_of_South_Carolina.svg.png' },
    { 'name': 'South Dakota', 'flag': '1/1a/Flag_of_South_Dakota.svg/46px-Flag_of_South_Dakota.svg.png' },
    { 'name': 'Tennessee', 'flag': '9/9e/Flag_of_Tennessee.svg/46px-Flag_of_Tennessee.svg.png' },
    { 'name': 'Texas', 'flag': 'f/f7/Flag_of_Texas.svg/45px-Flag_of_Texas.svg.png' },
    { 'name': 'Utah', 'flag': 'f/f6/Flag_of_Utah.svg/45px-Flag_of_Utah.svg.png' },
    { 'name': 'Vermont', 'flag': '4/49/Flag_of_Vermont.svg/46px-Flag_of_Vermont.svg.png' },
    { 'name': 'Virginia', 'flag': '4/47/Flag_of_Virginia.svg/44px-Flag_of_Virginia.svg.png' },
    { 'name': 'Washington', 'flag': '5/54/Flag_of_Washington.svg/46px-Flag_of_Washington.svg.png' },
    { 'name': 'West Virginia', 'flag': '2/22/Flag_of_West_Virginia.svg/46px-Flag_of_West_Virginia.svg.png' },
    { 'name': 'Wisconsin', 'flag': '2/22/Flag_of_Wisconsin.svg/45px-Flag_of_Wisconsin.svg.png' },
    { 'name': 'Wyoming', 'flag': 'b/bc/Flag_of_Wyoming.svg/43px-Flag_of_Wyoming.svg.png' }
];
var WIKI_URL = 'https://en.wikipedia.org/w/api.php';
var PARAMS = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]({
    fromObject: {
        action: 'opensearch',
        format: 'json',
        origin: '*'
    }
});
var WikipediaService = /** @class */ (function () {
    function WikipediaService(http) {
        this.http = http;
    }
    WikipediaService.prototype.search = function (term) {
        if (term === '') {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])([]);
        }
        return this.http
            .get(WIKI_URL, { params: PARAMS.set('search', term) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response[1]; }));
    };
    WikipediaService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], WikipediaService);
    return WikipediaService;
}());

var TypeaheadComponent = /** @class */ (function () {
    function TypeaheadComponent(_service) {
        var _this = this;
        this._service = _service;
        this.searching = false;
        this.searchFailed = false;
        this.focus$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.click$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.search = function (text$) {
            return text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (term) { return term.length < 2 ? []
                : states.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; }).slice(0, 10); }));
        };
        this.searchOnFocus = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.click$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focus$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (term) { return (term === '' ? states
                : states.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
        this.searchWikipedia = function (text$) {
            return text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(300), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function () { return _this.searching = true; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (term) {
                return _this._service.search(term).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function () { return _this.searchFailed = false; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function () {
                    _this.searchFailed = true;
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])([]);
                }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function () { return _this.searching = false; }));
        };
        this.searchTemp = function (text$) {
            return text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (term) { return term === '' ? []
                : statesWithFlags.filter(function (v) { return v.name.toLowerCase().indexOf(term.toLowerCase()) > -1; }).slice(0, 10); }));
        };
        this.formatter = function (x) { return x.name; };
    }
    TypeaheadComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('instance', { static: false }),
        __metadata("design:type", _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbTypeahead"])
    ], TypeaheadComponent.prototype, "instance", void 0);
    TypeaheadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-typeahead',
            template: __webpack_require__(/*! raw-loader!./typeahead.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/base/typeahead/typeahead.component.html"),
            providers: [WikipediaService],
            styles: [__webpack_require__(/*! ./typeahead.component.scss */ "./src/app/components/base/typeahead/typeahead.component.scss")]
        }),
        __metadata("design:paramtypes", [WikipediaService])
    ], TypeaheadComponent);
    return TypeaheadComponent;
}());



/***/ })

}]);
//# sourceMappingURL=components-base-base-module.js.map