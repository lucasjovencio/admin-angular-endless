(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-widgets-widgets-module"],{

/***/ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js":
/*!************************************************************************!*\
  !*** ./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js"), __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js"), __webpack_require__(/*! date-fns */ "./node_modules/date-fns/index.js"), __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js"), __webpack_require__(/*! ngx-slimscroll */ "./node_modules/ngx-slimscroll/dist/bundles/ngx-slimscroll.umd.js")) :
	undefined;
}(this, (function (exports,core,forms,dateFns,common,ngxSlimscroll) { 'use strict';

var counter = 0;
var isNil = function (value) {
    return (typeof value === 'undefined') || (value === null);
};
var NgDatepickerComponent = /** @class */ (function () {
    function NgDatepickerComponent(elementRef) {
        this.elementRef = elementRef;
        this.headless = false;
        this.isOpened = false;
        this.position = 'bottom-right';
        this.positions = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
        this.onTouchedCallback = function () { };
        this.onChangeCallback = function () { };
        this.scrollOptions = {
            barBackground: '#DFE3E9',
            gridBackground: '#FFFFFF',
            barBorderRadius: '3',
            gridBorderRadius: '3',
            barWidth: '6',
            gridWidth: '6',
            barMargin: '0',
            gridMargin: '0'
        };
    }
    NgDatepickerComponent.prototype.setDisabledState = function (isDisabled) {
        this.disabled = isDisabled;
    };
    Object.defineProperty(NgDatepickerComponent.prototype, "value", {
        get: function () {
            return this.innerValue;
        },
        set: function (val) {
            this.innerValue = val;
            this.onChangeCallback(this.innerValue);
        },
        enumerable: true,
        configurable: true
    });
    NgDatepickerComponent.prototype.ngOnInit = function () {
        this.view = 'days';
        this.date = new Date();
        this.setOptions();
        this.initDayNames();
        this.initYears();
        if (this.positions.indexOf(this.position) === -1) {
            throw new TypeError("ng-datepicker: invalid position property value '" + this.position + "' (expected: " + this.positions.join(', ') + ")");
        }
    };
    NgDatepickerComponent.prototype.ngOnChanges = function (changes) {
        if ('options' in changes) {
            this.setOptions();
            this.initDayNames();
            this.init();
            this.initYears();
        }
    };
    Object.defineProperty(NgDatepickerComponent.prototype, "defaultFieldId", {
        get: function () {
            var value = "datepicker-" + counter++;
            Object.defineProperty(this, 'defaultFieldId', { value: value });
            return value;
        },
        enumerable: true,
        configurable: true
    });
    NgDatepickerComponent.prototype.setOptions = function () {
        var today = new Date();
        this.minYear = this.options && this.options.minYear || dateFns.getYear(today) - 30;
        this.maxYear = this.options && this.options.maxYear || dateFns.getYear(today) + 30;
        this.displayFormat = this.options && this.options.displayFormat || 'MMM D[,] YYYY';
        this.barTitleFormat = this.options && this.options.barTitleFormat || 'MMMM YYYY';
        this.dayNamesFormat = this.options && this.options.dayNamesFormat || 'ddd';
        this.barTitleIfEmpty = this.options && this.options.barTitleIfEmpty || 'Click to select a date';
        this.firstCalendarDay = this.options && this.options.firstCalendarDay || 0;
        this.locale = this.options && { locale: this.options.locale } || {};
        this.placeholder = this.options && this.options.placeholder || '';
        this.addClass = this.options && this.options.addClass || {};
        this.addStyle = this.options && this.options.addStyle || {};
        this.fieldId = this.options && this.options.fieldId || this.defaultFieldId;
        this.useEmptyBarTitle = this.options && 'useEmptyBarTitle' in this.options ? this.options.useEmptyBarTitle : true;
    };
    NgDatepickerComponent.prototype.nextMonth = function () {
        this.date = dateFns.addMonths(this.date, 1);
        this.init();
    };
    NgDatepickerComponent.prototype.prevMonth = function () {
        this.date = dateFns.subMonths(this.date, 1);
        this.init();
    };
    NgDatepickerComponent.prototype.setDate = function (i) {
        this.date = this.days[i].date;
        this.value = this.date;
        this.init();
        this.close();
    };
    NgDatepickerComponent.prototype.setYear = function (i) {
        this.date = dateFns.setYear(this.date, this.years[i].year);
        this.init();
        this.initYears();
        this.view = 'days';
    };
    NgDatepickerComponent.prototype.isDateSelectable = function (date) {
        if (isNil(this.options)) {
            return true;
        }
        var minDateSet = !isNil(this.options.minDate);
        var maxDateSet = !isNil(this.options.maxDate);
        var timestamp = date.valueOf();
        if (minDateSet && (timestamp < this.options.minDate.valueOf())) {
            return false;
        }
        if (maxDateSet && (timestamp > this.options.maxDate.valueOf())) {
            return false;
        }
        return true;
    };
    NgDatepickerComponent.prototype.init = function () {
        var _this = this;
        var actualDate = this.date || new Date();
        var start = dateFns.startOfMonth(actualDate);
        var end = dateFns.endOfMonth(actualDate);
        this.days = dateFns.eachDay(start, end).map(function (date) {
            return {
                date: date,
                day: dateFns.getDate(date),
                month: dateFns.getMonth(date),
                year: dateFns.getYear(date),
                inThisMonth: true,
                isToday: dateFns.isToday(date),
                isSelected: dateFns.isSameDay(date, _this.innerValue) && dateFns.isSameMonth(date, _this.innerValue) && dateFns.isSameYear(date, _this.innerValue),
                isSelectable: _this.isDateSelectable(date)
            };
        });
        var tmp = dateFns.getDay(start) - this.firstCalendarDay;
        var prevDays = tmp < 0 ? 7 - this.firstCalendarDay : tmp;
        for (var i = 1; i <= prevDays; i++) {
            var date = dateFns.subDays(start, i);
            this.days.unshift({
                date: date,
                day: dateFns.getDate(date),
                month: dateFns.getMonth(date),
                year: dateFns.getYear(date),
                inThisMonth: false,
                isToday: dateFns.isToday(date),
                isSelected: dateFns.isSameDay(date, this.innerValue) && dateFns.isSameMonth(date, this.innerValue) && dateFns.isSameYear(date, this.innerValue),
                isSelectable: this.isDateSelectable(date)
            });
        }
        if (this.innerValue) {
            this.displayValue = dateFns.format(this.innerValue, this.displayFormat, this.locale);
            this.barTitle = dateFns.format(start, this.barTitleFormat, this.locale);
        }
        else {
            this.displayValue = '';
            this.barTitle = this.useEmptyBarTitle ? this.barTitleIfEmpty : dateFns.format(start, this.barTitleFormat, this.locale);
        }
    };
    NgDatepickerComponent.prototype.initYears = function () {
        var _this = this;
        var range = this.maxYear - this.minYear;
        this.years = Array.from(new Array(range), function (x, i) { return i + _this.minYear; }).map(function (year) {
            return { year: year, isThisYear: year === dateFns.getYear(_this.date) };
        });
    };
    NgDatepickerComponent.prototype.initDayNames = function () {
        this.dayNames = [];
        var start = this.firstCalendarDay;
        for (var i = start; i <= 6 + start; i++) {
            var date = dateFns.setDay(new Date(), i);
            this.dayNames.push(dateFns.format(date, this.dayNamesFormat, this.locale));
        }
    };
    NgDatepickerComponent.prototype.toggleView = function () {
        this.view = this.view === 'days' ? 'years' : 'days';
    };
    NgDatepickerComponent.prototype.toggle = function () {
        this.isOpened = !this.isOpened;
        if (!this.isOpened && this.view === 'years') {
            this.toggleView();
        }
    };
    NgDatepickerComponent.prototype.close = function () {
        this.isOpened = false;
        if (this.view === 'years') {
            this.toggleView();
        }
    };
    NgDatepickerComponent.prototype.reset = function (fireValueChangeEvent) {
        if (fireValueChangeEvent === void 0) { fireValueChangeEvent = false; }
        this.date = null;
        this.innerValue = null;
        this.init();
        if (fireValueChangeEvent && this.onChangeCallback) {
            this.onChangeCallback(this.innerValue);
        }
    };
    NgDatepickerComponent.prototype.writeValue = function (val) {
        if (val) {
            this.date = val;
            this.innerValue = val;
            this.init();
            this.displayValue = dateFns.format(this.innerValue, this.displayFormat, this.locale);
            this.barTitle = dateFns.format(dateFns.startOfMonth(val), this.barTitleFormat, this.locale);
        }
    };
    NgDatepickerComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    NgDatepickerComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    NgDatepickerComponent.prototype.onBlur = function (e) {
        if (!this.isOpened) {
            return;
        }
        var input = this.elementRef.nativeElement.querySelector('.ngx-datepicker-input');
        if (input == null) {
            return;
        }
        if (e.target === input || input.contains((e.target))) {
            return;
        }
        var container = this.elementRef.nativeElement.querySelector('.ngx-datepicker-calendar-container');
        if (container && container !== e.target && !container.contains((e.target)) && !((e.target)).classList.contains('year-unit')) {
            this.close();
        }
    };
    return NgDatepickerComponent;
}());
NgDatepickerComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ng-datepicker',
                template: "<div class=\"ngx-datepicker-container\">\n  <input type=\"text\" *ngIf=\"!headless\" class=\"ngx-datepicker-input\" [(ngModel)]=\"displayValue\" readonly [placeholder]=\"placeholder\"\n    [ngClass]=\"addClass\" [ngStyle]=\"addStyle\" [id]=\"fieldId\" [disabled]=\"disabled\" (click)=\"toggle()\" />\n  <ng-content></ng-content>\n  <div class=\"ngx-datepicker-calendar-container ngx-datepicker-position-{{position}}\" *ngIf=\"isOpened\">\n    <div class=\"topbar-container\">\n      <svg width=\"7px\" height=\"10px\" viewBox=\"0 0 7 10\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n        (click)=\"prevMonth()\">\n        <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n          <g transform=\"translate(-923.000000, -1882.000000)\" fill=\"#CED0DA\">\n            <g transform=\"translate(80.000000, 1361.000000)\">\n              <g transform=\"translate(0.000000, 430.000000)\">\n                <g transform=\"translate(825.000000, 0.000000)\">\n                  <g transform=\"translate(0.000000, 72.000000)\">\n                    <g transform=\"translate(18.000000, 15.000000)\">\n                      <polygon id=\"Back\" points=\"6.015 4 0 9.013 6.015 14.025\"></polygon>\n                    </g>\n                  </g>\n                </g>\n              </g>\n            </g>\n          </g>\n        </g>\n      </svg>\n      <span class=\"topbar-title\" (click)=\"toggleView()\">{{ barTitle }}</span>\n      <svg width=\"7px\" height=\"10px\" viewBox=\"0 0 6 10\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n        (click)=\"nextMonth()\">\n        <g id=\"Source-Sans---UI-Elements-Kit\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n          <g id=\"White-Layout\" transform=\"translate(-1182.000000, -1882.000000)\" fill=\"#CED0DA\">\n            <g id=\"Dropdowns-&amp;-Selector\" transform=\"translate(80.000000, 1361.000000)\">\n              <g id=\"Dropdowns\" transform=\"translate(0.000000, 430.000000)\">\n                <g id=\"Calendar\" transform=\"translate(825.000000, 0.000000)\">\n                  <g transform=\"translate(0.000000, 72.000000)\" id=\"Top-Bar-Nav\">\n                    <g transform=\"translate(18.000000, 15.000000)\">\n                      <polygon id=\"Forward\" transform=\"translate(262.007500, 9.012500) scale(-1, 1) translate(-262.007500, -9.012500) \" points=\"265.015 4 259 9.013 265.015 14.025\"></polygon>\n                    </g>\n                  </g>\n                </g>\n              </g>\n            </g>\n          </g>\n        </g>\n      </svg>\n    </div>\n    <div class=\"main-calendar-container\" *ngIf=\"view === 'days'\">\n      <div class=\"main-calendar-day-names\">\n        <span class=\"day-name-unit\" *ngFor=\"let name of dayNames\">{{ name }}</span>\n      </div>\n      <div class=\"main-calendar-days\">\n        <span class=\"day-unit\" *ngFor=\"let day of days; let i = index;\" [ngClass]=\"{ 'is-prev-month': !day.inThisMonth, 'is-today': day.isToday, 'is-selected': day.isSelected, 'is-disabled': !day.isSelectable }\"\n          (click)=\"day.isSelectable && setDate(i)\">\n          {{ day.day }}\n        </span>\n      </div>\n    </div>\n    <div class=\"main-calendar-container\" *ngIf=\"view === 'years'\">\n      <div class=\"main-calendar-years\" slimScroll [options]=\"scrollOptions\">\n        <span class=\"year-unit\" *ngFor=\"let year of years; let i = index;\" [ngClass]=\"{ 'is-selected': year.isThisYear }\" (click)=\"setYear(i)\">{{ year.year }}</span>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".ngx-datepicker-position-bottom-left{top:40px;right:0}.ngx-datepicker-position-bottom-right{top:40px;left:0}.ngx-datepicker-position-top-left{bottom:40px;right:0}.ngx-datepicker-position-top-right{bottom:40px;left:0}.ngx-datepicker-container{position:relative}.ngx-datepicker-container .ngx-datepicker-input{padding:5px 10px;font-size:14px;width:200px;outline:0;border:1px solid #dfe3e9}.ngx-datepicker-container .ngx-datepicker-calendar-container{position:absolute;width:300px;background:#fff;-webkit-box-shadow:0 1px 4px 0 rgba(0,0,0,.08);box-shadow:0 1px 4px 0 rgba(0,0,0,.08);border:1px solid #dfe3e9;border-radius:4px}.ngx-datepicker-container .ngx-datepicker-calendar-container .topbar-container{width:100%;height:50px;padding:15px;border-bottom:1px solid #dfe3e9;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.ngx-datepicker-container .ngx-datepicker-calendar-container .topbar-container svg{cursor:pointer}.ngx-datepicker-container .ngx-datepicker-calendar-container .topbar-container svg g{fill:#ced0da}.ngx-datepicker-container .ngx-datepicker-calendar-container .topbar-container .topbar-title{color:#3d495c;font-size:14px;font-weight:600;cursor:pointer}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container{width:100%;height:100%;padding:15px 10px 0;font-size:12px;font-weight:500}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-day-names{color:#a4a9b1;width:100%;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-day-names .day-name-unit{width:calc(100% / 7);text-transform:uppercase;text-align:center}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years{padding:15px 0;width:100%;display:inline-block;max-height:275px;overflow:hidden}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit{width:calc(100% / 7);height:40px;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;float:left;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border-radius:50%;color:#3d495c}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit.is-prev-month,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit.is-prev-month,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit.is-prev-month,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit.is-prev-month{color:#a4a9b1}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit.is-today,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit.is-today,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit.is-today,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit.is-today,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit:hover{background:#a4a9b1;color:#fff}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit.is-selected,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit.is-selected,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit.is-selected,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit.is-selected{background:#1a91eb;color:#fff}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit.is-disabled,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit.is-disabled,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit.is-disabled,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit.is-disabled{cursor:not-allowed;color:#a4a9b1}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .day-unit.is-disabled:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-days .year-unit.is-disabled:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .day-unit.is-disabled:hover,.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit.is-disabled:hover{background:0 0}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years{height:210px;display:block;padding:0}.ngx-datepicker-container .ngx-datepicker-calendar-container .main-calendar-container .main-calendar-years .year-unit{width:calc(100% / 3);border-radius:10px}"],
                providers: [
                    { provide: forms.NG_VALUE_ACCESSOR, useExisting: core.forwardRef(function () { return NgDatepickerComponent; }), multi: true }
                ]
            },] },
];
NgDatepickerComponent.ctorParameters = function () { return [
    { type: core.ElementRef, },
]; };
NgDatepickerComponent.propDecorators = {
    "options": [{ type: core.Input },],
    "headless": [{ type: core.Input },],
    "isOpened": [{ type: core.Input },],
    "position": [{ type: core.Input },],
    "onBlur": [{ type: core.HostListener, args: ['document:click', ['$event'],] },],
};
var NgDatepickerModule = /** @class */ (function () {
    function NgDatepickerModule() {
    }
    return NgDatepickerModule;
}());
NgDatepickerModule.decorators = [
    { type: core.NgModule, args: [{
                declarations: [NgDatepickerComponent],
                imports: [common.CommonModule, forms.FormsModule, ngxSlimscroll.NgSlimScrollModule],
                exports: [NgDatepickerComponent, common.CommonModule, forms.FormsModule, ngxSlimscroll.NgSlimScrollModule]
            },] },
];

exports.NgDatepickerModule = NgDatepickerModule;
exports.NgDatepickerComponent = NgDatepickerComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ng2-datepicker.umd.js.map


/***/ }),

/***/ "./node_modules/ngx-slimscroll/dist/bundles/ngx-slimscroll.umd.js":
/*!************************************************************************!*\
  !*** ./node_modules/ngx-slimscroll/dist/bundles/ngx-slimscroll.umd.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
     true ? factory(exports, __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js"), __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js"), __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js"), __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js")) :
    undefined;
}(this, (function (exports,core,common,rxjs,operators) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SlimScrollEvent = (function () {
        function SlimScrollEvent(obj) {
            this.type = obj.type;
            this.y = obj && obj.y ? obj.y : 0;
            this.percent = obj && obj.percent ? obj.percent : 0;
            this.duration = obj && obj.duration ? obj.duration : 0;
            this.easing = obj && obj.easing ? obj.easing : 'linear';
        }
        return SlimScrollEvent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ SLIMSCROLL_DEFAULTS = new core.InjectionToken('NGX_SLIMSCROLL_DEFAULTS');
    var SlimScrollOptions = (function () {
        function SlimScrollOptions(obj) {
            this.position = obj && obj.position ? obj.position : 'right';
            this.barBackground = obj && obj.barBackground ? obj.barBackground : '#343a40';
            this.barOpacity = obj && obj.barOpacity ? obj.barOpacity : '1';
            this.barWidth = obj && obj.barWidth ? obj.barWidth : '12';
            this.barBorderRadius = obj && obj.barBorderRadius ? obj.barBorderRadius : '5';
            this.barMargin = obj && obj.barMargin ? obj.barMargin : '1px 0';
            this.gridBackground = obj && obj.gridBackground ? obj.gridBackground : '#adb5bd';
            this.gridOpacity = obj && obj.gridOpacity ? obj.gridOpacity : '1';
            this.gridWidth = obj && obj.gridWidth ? obj.gridWidth : '8';
            this.gridBorderRadius = obj && obj.gridBorderRadius ? obj.gridBorderRadius : '10';
            this.gridMargin = obj && obj.gridMargin ? obj.gridMargin : '1px 2px';
            this.alwaysVisible = obj && typeof obj.alwaysVisible !== 'undefined' ? obj.alwaysVisible : true;
            this.visibleTimeout = obj && obj.visibleTimeout ? obj.visibleTimeout : 1000;
        }
        /**
         * @param {?=} obj
         * @return {?}
         */
        SlimScrollOptions.prototype.merge = /**
         * @param {?=} obj
         * @return {?}
         */
            function (obj) {
                var /** @type {?} */ result = new SlimScrollOptions();
                result.position = obj && obj.position ? obj.position : this.position;
                result.barBackground = obj && obj.barBackground ? obj.barBackground : this.barBackground;
                result.barOpacity = obj && obj.barOpacity ? obj.barOpacity : this.barOpacity;
                result.barWidth = obj && obj.barWidth ? obj.barWidth : this.barWidth;
                result.barBorderRadius = obj && obj.barBorderRadius ? obj.barBorderRadius : this.barBorderRadius;
                result.barMargin = obj && obj.barMargin ? obj.barMargin : this.barMargin;
                result.gridBackground = obj && obj.gridBackground ? obj.gridBackground : this.gridBackground;
                result.gridOpacity = obj && obj.gridOpacity ? obj.gridOpacity : this.gridBackground;
                result.gridWidth = obj && obj.gridWidth ? obj.gridWidth : this.gridWidth;
                result.gridBorderRadius = obj && obj.gridBorderRadius ? obj.gridBorderRadius : this.gridBorderRadius;
                result.gridMargin = obj && obj.gridMargin ? obj.gridMargin : this.gridMargin;
                result.alwaysVisible = obj && typeof obj.alwaysVisible !== 'undefined' ? obj.alwaysVisible : this.alwaysVisible;
                result.visibleTimeout = obj && obj.visibleTimeout ? obj.visibleTimeout : this.visibleTimeout;
                return result;
            };
        return SlimScrollOptions;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SlimScrollState = (function () {
        function SlimScrollState(obj) {
            this.scrollPosition = obj && obj.scrollPosition ? obj.scrollPosition : 0;
            this.isScrollAtStart = obj && typeof obj.isScrollAtStart !== 'undefined' ? obj.isScrollAtStart : true;
            this.isScrollAtEnd = obj && typeof obj.isScrollAtEnd !== 'undefined' ? obj.isScrollAtEnd : false;
        }
        return SlimScrollState;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ easing = {
        linear: function (t) { return t; },
        inQuad: function (t) { return t * t; },
        outQuad: function (t) { return t * (2 - t); },
        inOutQuad: function (t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t; },
        inCubic: function (t) { return t * t * t; },
        outCubic: function (t) { return (--t) * t * t + 1; },
        inOutCubic: function (t) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1; },
        inQuart: function (t) { return t * t * t * t; },
        outQuart: function (t) { return 1 - (--t) * t * t * t; },
        inOutQuart: function (t) { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; },
        inQuint: function (t) { return t * t * t * t * t; },
        outQuint: function (t) { return 1 + (--t) * t * t * t * t; },
        inOutQuint: function (t) { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t; }
    };
    var SlimScrollDirective = (function () {
        function SlimScrollDirective(viewContainer, renderer, document, optionsDefaults) {
            var _this = this;
            this.viewContainer = viewContainer;
            this.renderer = renderer;
            this.document = document;
            this.optionsDefaults = optionsDefaults;
            this.enabled = true;
            this.scrollChanged = new core.EventEmitter();
            this.barVisibilityChange = new core.EventEmitter();
            this.initWheel = function () {
                var /** @type {?} */ dommousescroll = rxjs.fromEvent(_this.el, 'DOMMouseScroll');
                var /** @type {?} */ mousewheel = rxjs.fromEvent(_this.el, 'mousewheel');
                var /** @type {?} */ wheelSubscription = rxjs.merge.apply(void 0, __spread([dommousescroll, mousewheel])).subscribe(function (e) {
                    var /** @type {?} */ delta = 0;
                    if (e.wheelDelta) {
                        delta = -e.wheelDelta / 120;
                    }
                    if (e.detail) {
                        delta = e.detail / 3;
                    }
                    _this.scrollContent(delta, true, false);
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                });
                _this.interactionSubscriptions.add(wheelSubscription);
            };
            this.initDrag = function () {
                var /** @type {?} */ bar = _this.bar;
                var /** @type {?} */ mousemove = rxjs.fromEvent(_this.document.documentElement, 'mousemove');
                var /** @type {?} */ touchmove = rxjs.fromEvent(_this.document.documentElement, 'touchmove');
                var /** @type {?} */ mousedown = rxjs.fromEvent(bar, 'mousedown');
                var /** @type {?} */ touchstart = rxjs.fromEvent(_this.el, 'touchstart');
                var /** @type {?} */ mouseup = rxjs.fromEvent(_this.document.documentElement, 'mouseup');
                var /** @type {?} */ touchend = rxjs.fromEvent(_this.document.documentElement, 'touchend');
                var /** @type {?} */ mousedrag = mousedown
                    .pipe(operators.mergeMap(function (e) {
                    _this.pageY = e.pageY;
                    _this.top = parseFloat(getComputedStyle(bar).top);
                    return mousemove
                        .pipe(operators.map(function (emove) {
                        emove.preventDefault();
                        return _this.top + emove.pageY - _this.pageY;
                    }), operators.takeUntil(mouseup));
                }));
                var /** @type {?} */ touchdrag = touchstart
                    .pipe(operators.mergeMap(function (e) {
                    _this.pageY = e.targetTouches[0].pageY;
                    _this.top = -parseFloat(getComputedStyle(bar).top);
                    return touchmove
                        .pipe(operators.map(function (tmove) {
                        return -(_this.top + tmove.targetTouches[0].pageY - _this.pageY);
                    }), operators.takeUntil(touchend));
                }));
                var /** @type {?} */ dragSubscription = rxjs.merge.apply(void 0, __spread([mousedrag, touchdrag])).subscribe(function (top) {
                    _this.body.addEventListener('selectstart', _this.preventDefaultEvent, false);
                    _this.renderer.setStyle(_this.body, 'touch-action', 'pan-y');
                    _this.renderer.setStyle(_this.body, 'user-select', 'none');
                    _this.renderer.setStyle(_this.bar, 'top', top + "px");
                    var /** @type {?} */ over = _this.scrollContent(0, true, false);
                    var /** @type {?} */ maxTop = _this.el.offsetHeight - _this.bar.offsetHeight;
                    if (over && over < 0 && -over <= maxTop) {
                        _this.renderer.setStyle(_this.el, 'paddingTop', -over + 'px');
                    }
                    else if (over && over > 0 && over <= maxTop) {
                        _this.renderer.setStyle(_this.el, 'paddingBottom', over + 'px');
                    }
                });
                var /** @type {?} */ dragEndSubscription = rxjs.merge.apply(void 0, __spread([mouseup, touchend])).subscribe(function () {
                    _this.body.removeEventListener('selectstart', _this.preventDefaultEvent, false);
                    var /** @type {?} */ paddingTop = parseInt(_this.el.style.paddingTop, 10);
                    var /** @type {?} */ paddingBottom = parseInt(_this.el.style.paddingBottom, 10);
                    _this.renderer.setStyle(_this.body, 'touch-action', 'unset');
                    _this.renderer.setStyle(_this.body, 'user-select', 'default');
                    if (paddingTop > 0) {
                        _this.scrollTo(0, 300, 'linear');
                    }
                    else if (paddingBottom > 0) {
                        _this.scrollTo(0, 300, 'linear');
                    }
                });
                _this.interactionSubscriptions.add(dragSubscription);
                _this.interactionSubscriptions.add(dragEndSubscription);
            };
            this.preventDefaultEvent = function (e) {
                e.preventDefault();
                e.stopPropagation();
            };
            this.viewContainer = viewContainer;
            this.el = viewContainer.element.nativeElement;
            this.body = this.document.querySelector('body');
            this.mutationThrottleTimeout = 50;
        }
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                // setup if no changes for enabled for the first time
                if (!this.interactionSubscriptions && this.enabled) {
                    this.setup();
                }
            };
        /**
         * @param {?} changes
         * @return {?}
         */
        SlimScrollDirective.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes["enabled"]) {
                    if (this.enabled) {
                        this.setup();
                    }
                    else {
                        this.destroy();
                    }
                }
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.destroy();
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.setup = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.interactionSubscriptions = new rxjs.Subscription();
                if (this.optionsDefaults) {
                    this.options = new SlimScrollOptions(this.optionsDefaults).merge(this.options);
                }
                else {
                    this.options = new SlimScrollOptions(this.options);
                }
                this.setStyle();
                this.wrapContainer();
                this.initGrid();
                this.initBar();
                this.getBarHeight();
                this.initWheel();
                this.initDrag();
                if (!this.options.alwaysVisible) {
                    this.hideBarAndGrid();
                }
                if (MutationObserver) {
                    if (this.mutationObserver) {
                        this.mutationObserver.disconnect();
                    }
                    this.mutationObserver = new MutationObserver(function () {
                        if (_this.mutationThrottleTimeout) {
                            clearTimeout(_this.mutationThrottleTimeout);
                            _this.mutationThrottleTimeout = setTimeout(_this.onMutation.bind(_this), 50);
                        }
                    });
                    this.mutationObserver.observe(this.el, { subtree: true, childList: true });
                }
                if (this.scrollEvents && this.scrollEvents instanceof core.EventEmitter) {
                    var /** @type {?} */ scrollSubscription = this.scrollEvents.subscribe(function (event) { return _this.handleEvent(event); });
                    this.interactionSubscriptions.add(scrollSubscription);
                }
            };
        /**
         * @param {?} e
         * @return {?}
         */
        SlimScrollDirective.prototype.handleEvent = /**
         * @param {?} e
         * @return {?}
         */
            function (e) {
                if (e.type === 'scrollToBottom') {
                    var /** @type {?} */ y = this.el.scrollHeight - this.el.clientHeight;
                    this.scrollTo(y, e.duration, e.easing);
                }
                else if (e.type === 'scrollToTop') {
                    var /** @type {?} */ y = 0;
                    this.scrollTo(y, e.duration, e.easing);
                }
                else if (e.type === 'scrollToPercent' && (e.percent >= 0 && e.percent <= 100)) {
                    var /** @type {?} */ y = Math.round(((this.el.scrollHeight - this.el.clientHeight) / 100) * e.percent);
                    this.scrollTo(y, e.duration, e.easing);
                }
                else if (e.type === 'scrollTo') {
                    var /** @type {?} */ y = e.y;
                    if (y <= this.el.scrollHeight - this.el.clientHeight && y >= 0) {
                        this.scrollTo(y, e.duration, e.easing);
                    }
                }
                else if (e.type === 'recalculate') {
                    this.getBarHeight();
                }
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.setStyle = /**
         * @return {?}
         */
            function () {
                var /** @type {?} */ el = this.el;
                this.renderer.setStyle(el, 'overflow', 'hidden');
                this.renderer.setStyle(el, 'position', 'relative');
                this.renderer.setStyle(el, 'display', 'block');
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.onMutation = /**
         * @return {?}
         */
            function () {
                this.getBarHeight();
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.wrapContainer = /**
         * @return {?}
         */
            function () {
                this.wrapper = this.renderer.createElement('div');
                var /** @type {?} */ wrapper = this.wrapper;
                var /** @type {?} */ el = this.el;
                this.renderer.addClass(wrapper, 'slimscroll-wrapper');
                this.renderer.setStyle(wrapper, 'position', 'relative');
                this.renderer.setStyle(wrapper, 'overflow', 'hidden');
                this.renderer.setStyle(wrapper, 'display', 'inline-block');
                this.renderer.setStyle(wrapper, 'margin', getComputedStyle(el).margin);
                this.renderer.setStyle(wrapper, 'width', '100%');
                this.renderer.setStyle(wrapper, 'height', getComputedStyle(el).height);
                this.renderer.insertBefore(el.parentNode, wrapper, el);
                this.renderer.appendChild(wrapper, el);
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.initGrid = /**
         * @return {?}
         */
            function () {
                this.grid = this.renderer.createElement('div');
                var /** @type {?} */ grid = this.grid;
                this.renderer.addClass(grid, 'slimscroll-grid');
                this.renderer.setStyle(grid, 'position', 'absolute');
                this.renderer.setStyle(grid, 'top', '0');
                this.renderer.setStyle(grid, 'bottom', '0');
                this.renderer.setStyle(grid, this.options.position, '0');
                this.renderer.setStyle(grid, 'width', this.options.gridWidth + "px");
                this.renderer.setStyle(grid, 'background', this.options.gridBackground);
                this.renderer.setStyle(grid, 'opacity', this.options.gridOpacity);
                this.renderer.setStyle(grid, 'display', 'block');
                this.renderer.setStyle(grid, 'cursor', 'pointer');
                this.renderer.setStyle(grid, 'z-index', '99');
                this.renderer.setStyle(grid, 'border-radius', this.options.gridBorderRadius + "px");
                this.renderer.setStyle(grid, 'margin', this.options.gridMargin);
                this.renderer.appendChild(this.wrapper, grid);
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.initBar = /**
         * @return {?}
         */
            function () {
                this.bar = this.renderer.createElement('div');
                var /** @type {?} */ bar = this.bar;
                this.renderer.addClass(bar, 'slimscroll-bar');
                this.renderer.setStyle(bar, 'position', 'absolute');
                this.renderer.setStyle(bar, 'top', '0');
                this.renderer.setStyle(bar, this.options.position, '0');
                this.renderer.setStyle(bar, 'width', this.options.barWidth + "px");
                this.renderer.setStyle(bar, 'background', this.options.barBackground);
                this.renderer.setStyle(bar, 'opacity', this.options.barOpacity);
                this.renderer.setStyle(bar, 'display', 'block');
                this.renderer.setStyle(bar, 'cursor', 'pointer');
                this.renderer.setStyle(bar, 'z-index', '100');
                this.renderer.setStyle(bar, 'border-radius', this.options.barBorderRadius + "px");
                this.renderer.setStyle(bar, 'margin', this.options.barMargin);
                this.renderer.appendChild(this.wrapper, bar);
                this.barVisibilityChange.emit(true);
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.getBarHeight = /**
         * @return {?}
         */
            function () {
                var /** @type {?} */ elHeight = this.el.offsetHeight;
                var /** @type {?} */ barHeight = Math.max((elHeight / this.el.scrollHeight) * elHeight, 30) + 'px';
                var /** @type {?} */ display = parseInt(barHeight, 10) === elHeight ? 'none' : 'block';
                if (this.wrapper.offsetHeight !== elHeight) {
                    this.renderer.setStyle(this.wrapper, 'height', elHeight + 'px');
                }
                this.renderer.setStyle(this.bar, 'height', barHeight);
                this.renderer.setStyle(this.bar, 'display', display);
                this.renderer.setStyle(this.grid, 'display', display);
                this.barVisibilityChange.emit(display !== 'none');
            };
        /**
         * @param {?} y
         * @param {?} duration
         * @param {?} easingFunc
         * @return {?}
         */
        SlimScrollDirective.prototype.scrollTo = /**
         * @param {?} y
         * @param {?} duration
         * @param {?} easingFunc
         * @return {?}
         */
            function (y, duration, easingFunc) {
                var _this = this;
                var /** @type {?} */ start = Date.now();
                var /** @type {?} */ from = this.el.scrollTop;
                var /** @type {?} */ maxTop = this.el.offsetHeight - this.bar.offsetHeight;
                var /** @type {?} */ maxElScrollTop = this.el.scrollHeight - this.el.clientHeight;
                var /** @type {?} */ barHeight = Math.max((this.el.offsetHeight / this.el.scrollHeight) * this.el.offsetHeight, 30);
                var /** @type {?} */ paddingTop = parseInt(this.el.style.paddingTop, 10) || 0;
                var /** @type {?} */ paddingBottom = parseInt(this.el.style.paddingBottom, 10) || 0;
                var /** @type {?} */ scroll = function (timestamp) {
                    var /** @type {?} */ currentTime = Date.now();
                    var /** @type {?} */ time = Math.min(1, ((currentTime - start) / duration));
                    var /** @type {?} */ easedTime = easing[easingFunc](time);
                    if (paddingTop > 0 || paddingBottom > 0) {
                        var /** @type {?} */ fromY = null;
                        if (paddingTop > 0) {
                            fromY = -paddingTop;
                            fromY = -((easedTime * (y - fromY)) + fromY);
                            _this.renderer.setStyle(_this.el, 'paddingTop', fromY + "px");
                        }
                        if (paddingBottom > 0) {
                            fromY = paddingBottom;
                            fromY = ((easedTime * (y - fromY)) + fromY);
                            _this.renderer.setStyle(_this.el, 'paddingBottom', fromY + "px");
                        }
                    }
                    else {
                        _this.el.scrollTop = (easedTime * (y - from)) + from;
                    }
                    var /** @type {?} */ percentScroll = _this.el.scrollTop / maxElScrollTop;
                    if (paddingBottom === 0) {
                        var /** @type {?} */ delta = Math.round(Math.round(_this.el.clientHeight * percentScroll) - barHeight);
                        if (delta > 0) {
                            _this.renderer.setStyle(_this.bar, 'top', delta + "px");
                        }
                    }
                    if (time < 1) {
                        requestAnimationFrame(scroll);
                    }
                };
                requestAnimationFrame(scroll);
            };
        /**
         * @param {?} y
         * @param {?} isWheel
         * @param {?} isJump
         * @return {?}
         */
        SlimScrollDirective.prototype.scrollContent = /**
         * @param {?} y
         * @param {?} isWheel
         * @param {?} isJump
         * @return {?}
         */
            function (y, isWheel, isJump) {
                var _this = this;
                var /** @type {?} */ delta = y;
                var /** @type {?} */ maxTop = this.el.offsetHeight - this.bar.offsetHeight;
                var /** @type {?} */ hiddenContent = this.el.scrollHeight - this.el.offsetHeight;
                var /** @type {?} */ percentScroll;
                var /** @type {?} */ over = null;
                if (isWheel) {
                    delta = parseInt(getComputedStyle(this.bar).top, 10) + y * 20 / 100 * this.bar.offsetHeight;
                    if (delta < 0 || delta > maxTop) {
                        over = delta > maxTop ? delta - maxTop : delta;
                    }
                    delta = Math.min(Math.max(delta, 0), maxTop);
                    delta = (y > 0) ? Math.ceil(delta) : Math.floor(delta);
                    this.renderer.setStyle(this.bar, 'top', delta + 'px');
                }
                percentScroll = parseInt(getComputedStyle(this.bar).top, 10) / (this.el.offsetHeight - this.bar.offsetHeight);
                delta = percentScroll * hiddenContent;
                this.el.scrollTop = delta;
                this.showBarAndGrid();
                if (!this.options.alwaysVisible) {
                    if (this.visibleTimeout) {
                        clearTimeout(this.visibleTimeout);
                    }
                    this.visibleTimeout = setTimeout(function () {
                        _this.hideBarAndGrid();
                    }, this.options.visibleTimeout);
                }
                var /** @type {?} */ isScrollAtStart = delta === 0;
                var /** @type {?} */ isScrollAtEnd = delta === hiddenContent;
                var /** @type {?} */ scrollPosition = Math.ceil(delta);
                var /** @type {?} */ scrollState = new SlimScrollState({ scrollPosition: scrollPosition, isScrollAtStart: isScrollAtStart, isScrollAtEnd: isScrollAtEnd });
                this.scrollChanged.emit(scrollState);
                return over;
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.showBarAndGrid = /**
         * @return {?}
         */
            function () {
                this.renderer.setStyle(this.grid, 'background', this.options.gridBackground);
                this.renderer.setStyle(this.bar, 'background', this.options.barBackground);
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.hideBarAndGrid = /**
         * @return {?}
         */
            function () {
                this.renderer.setStyle(this.grid, 'background', 'transparent');
                this.renderer.setStyle(this.bar, 'background', 'transparent');
            };
        /**
         * @return {?}
         */
        SlimScrollDirective.prototype.destroy = /**
         * @return {?}
         */
            function () {
                if (this.mutationObserver) {
                    this.mutationObserver.disconnect();
                    this.mutationObserver = null;
                }
                if (this.el.parentElement.classList.contains('slimscroll-wrapper')) {
                    var /** @type {?} */ wrapper = this.el.parentElement;
                    var /** @type {?} */ bar = wrapper.querySelector('.slimscroll-bar');
                    wrapper.removeChild(bar);
                    var /** @type {?} */ grid = wrapper.querySelector('.slimscroll-grid');
                    wrapper.removeChild(grid);
                    this.unwrap(wrapper);
                }
                if (this.interactionSubscriptions) {
                    this.interactionSubscriptions.unsubscribe();
                }
            };
        /**
         * @param {?} wrapper
         * @return {?}
         */
        SlimScrollDirective.prototype.unwrap = /**
         * @param {?} wrapper
         * @return {?}
         */
            function (wrapper) {
                var /** @type {?} */ docFrag = document.createDocumentFragment();
                while (wrapper.firstChild) {
                    var /** @type {?} */ child = wrapper.removeChild(wrapper.firstChild);
                    docFrag.appendChild(child);
                }
                wrapper.parentNode.replaceChild(docFrag, wrapper);
            };
        /**
         * @param {?} $event
         * @return {?}
         */
        SlimScrollDirective.prototype.onResize = /**
         * @param {?} $event
         * @return {?}
         */
            function ($event) {
                this.getBarHeight();
            };
        SlimScrollDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[slimScroll]',
                        // tslint:disable-line
                        exportAs: 'slimScroll'
                    },] },
        ];
        /** @nocollapse */
        SlimScrollDirective.ctorParameters = function () {
            return [
                { type: core.ViewContainerRef, decorators: [{ type: core.Inject, args: [core.ViewContainerRef,] },] },
                { type: core.Renderer2, decorators: [{ type: core.Inject, args: [core.Renderer2,] },] },
                { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] },] },
                { type: undefined, decorators: [{ type: core.Inject, args: [SLIMSCROLL_DEFAULTS,] }, { type: core.Optional },] },
            ];
        };
        SlimScrollDirective.propDecorators = {
            "enabled": [{ type: core.Input },],
            "options": [{ type: core.Input },],
            "scrollEvents": [{ type: core.Input },],
            "scrollChanged": [{ type: core.Output, args: ['scrollChanged',] },],
            "barVisibilityChange": [{ type: core.Output, args: ['barVisibilityChange',] },],
            "onResize": [{ type: core.HostListener, args: ['window:resize', ['$event'],] },],
        };
        return SlimScrollDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var NgSlimScrollModule = (function () {
        function NgSlimScrollModule() {
        }
        NgSlimScrollModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            SlimScrollDirective
                        ],
                        exports: [
                            SlimScrollDirective
                        ]
                    },] },
        ];
        return NgSlimScrollModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.SlimScrollEvent = SlimScrollEvent;
    exports.SLIMSCROLL_DEFAULTS = SLIMSCROLL_DEFAULTS;
    exports.SlimScrollOptions = SlimScrollOptions;
    exports.easing = easing;
    exports.SlimScrollDirective = SlimScrollDirective;
    exports.NgSlimScrollModule = NgSlimScrollModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXNsaW1zY3JvbGwudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gtc2xpbXNjcm9sbC9hcHAvbmd4LXNsaW1zY3JvbGwvY2xhc3Nlcy9zbGltc2Nyb2xsLWV2ZW50LmNsYXNzLnRzIiwibmc6Ly9uZ3gtc2xpbXNjcm9sbC9hcHAvbmd4LXNsaW1zY3JvbGwvY2xhc3Nlcy9zbGltc2Nyb2xsLW9wdGlvbnMuY2xhc3MudHMiLG51bGwsIm5nOi8vbmd4LXNsaW1zY3JvbGwvYXBwL25neC1zbGltc2Nyb2xsL2NsYXNzZXMvc2xpbXNjcm9sbC1zdGF0ZS5jbGFzcy50cyIsIm5nOi8vbmd4LXNsaW1zY3JvbGwvYXBwL25neC1zbGltc2Nyb2xsL2RpcmVjdGl2ZXMvc2xpbXNjcm9sbC5kaXJlY3RpdmUudHMiLCJuZzovL25neC1zbGltc2Nyb2xsL2FwcC9uZ3gtc2xpbXNjcm9sbC9tb2R1bGUvbmd4LXNsaW1zY3JvbGwubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgSVNsaW1TY3JvbGxFdmVudCB7XG4gIHR5cGU6ICdzY3JvbGxUb0JvdHRvbScgfCAnc2Nyb2xsVG9Ub3AnIHwgJ3Njcm9sbFRvUGVyY2VudCcgfCAnc2Nyb2xsVG8nIHwgJ3JlY2FsY3VsYXRlJztcbiAgeT86IG51bWJlcjtcbiAgcGVyY2VudD86IG51bWJlcjtcbiAgZHVyYXRpb24/OiBudW1iZXI7XG4gIGVhc2luZz86ICdsaW5lYXInIHwgJ2luUXVhZCcgfCAnb3V0UXVhZCcgfCAnaW5PdXRRdWFkJyB8ICdpbkN1YmljJyB8XG4gICAgJ291dEN1YmljJyB8ICdpbk91dEN1YmljJyB8ICdpblF1YXJ0JyB8ICdvdXRRdWFydCcgfCAnaW5PdXRRdWFydCcgfFxuICAgICdpblF1aW50JyB8ICdvdXRRdWludCcgfCAnaW5PdXRRdWludCc7XG59XG5cbmV4cG9ydCBjbGFzcyBTbGltU2Nyb2xsRXZlbnQgaW1wbGVtZW50cyBJU2xpbVNjcm9sbEV2ZW50IHtcbiAgdHlwZTogJ3Njcm9sbFRvQm90dG9tJyB8ICdzY3JvbGxUb1RvcCcgfCAnc2Nyb2xsVG9QZXJjZW50JyB8ICdzY3JvbGxUbycgfCAncmVjYWxjdWxhdGUnO1xuICB5PzogbnVtYmVyO1xuICBwZXJjZW50PzogbnVtYmVyO1xuICBkdXJhdGlvbj86IG51bWJlcjtcbiAgZWFzaW5nOiAnbGluZWFyJyB8ICdpblF1YWQnIHwgJ291dFF1YWQnIHwgJ2luT3V0UXVhZCcgfCAnaW5DdWJpYycgfFxuICAgICdvdXRDdWJpYycgfCAnaW5PdXRDdWJpYycgfCAnaW5RdWFydCcgfCAnb3V0UXVhcnQnIHwgJ2luT3V0UXVhcnQnIHxcbiAgICAnaW5RdWludCcgfCAnb3V0UXVpbnQnIHwgJ2luT3V0UXVpbnQnO1xuXG4gIGNvbnN0cnVjdG9yKG9iaj86IElTbGltU2Nyb2xsRXZlbnQpIHtcbiAgICB0aGlzLnR5cGUgPSBvYmoudHlwZTtcbiAgICB0aGlzLnkgPSBvYmogJiYgb2JqLnkgPyBvYmoueSA6IDA7XG4gICAgdGhpcy5wZXJjZW50ID0gb2JqICYmIG9iai5wZXJjZW50ID8gb2JqLnBlcmNlbnQgOiAwO1xuICAgIHRoaXMuZHVyYXRpb24gPSBvYmogJiYgb2JqLmR1cmF0aW9uID8gb2JqLmR1cmF0aW9uIDogMDtcbiAgICB0aGlzLmVhc2luZyA9IG9iaiAmJiBvYmouZWFzaW5nID8gb2JqLmVhc2luZyA6ICdsaW5lYXInO1xuICB9XG59XG4iLCJpbXBvcnQgeyBJU2xpbVNjcm9sbE9wdGlvbnMgfSBmcm9tICcuL3NsaW1zY3JvbGwtb3B0aW9ucy5jbGFzcyc7XG5pbXBvcnQgeyBJU2xpbVNjcm9sbEV2ZW50IH0gZnJvbSAnLi9zbGltc2Nyb2xsLWV2ZW50LmNsYXNzJztcbmltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNsaW1TY3JvbGxPcHRpb25zIHtcbiAgcG9zaXRpb24/OiBzdHJpbmc7XG4gIGJhckJhY2tncm91bmQ/OiBzdHJpbmc7XG4gIGJhck9wYWNpdHk/OiBzdHJpbmc7XG4gIGJhcldpZHRoPzogc3RyaW5nO1xuICBiYXJCb3JkZXJSYWRpdXM/OiBzdHJpbmc7XG4gIGJhck1hcmdpbj86IHN0cmluZztcbiAgZ3JpZEJhY2tncm91bmQ/OiBzdHJpbmc7XG4gIGdyaWRPcGFjaXR5Pzogc3RyaW5nO1xuICBncmlkV2lkdGg/OiBzdHJpbmc7XG4gIGdyaWRCb3JkZXJSYWRpdXM/OiBzdHJpbmc7XG4gIGdyaWRNYXJnaW4/OiBzdHJpbmc7XG4gIGFsd2F5c1Zpc2libGU/OiBib29sZWFuO1xuICB2aXNpYmxlVGltZW91dD86IG51bWJlcjtcbn1cblxuZXhwb3J0IGNvbnN0IFNMSU1TQ1JPTExfREVGQVVMVFM6IEluamVjdGlvblRva2VuPElTbGltU2Nyb2xsT3B0aW9ucz5cbiAgICA9IG5ldyBJbmplY3Rpb25Ub2tlbignTkdYX1NMSU1TQ1JPTExfREVGQVVMVFMnKTtcblxuZXhwb3J0IGNsYXNzIFNsaW1TY3JvbGxPcHRpb25zIGltcGxlbWVudHMgSVNsaW1TY3JvbGxPcHRpb25zIHtcbiAgcG9zaXRpb24/OiBzdHJpbmc7XG4gIGJhckJhY2tncm91bmQ/OiBzdHJpbmc7XG4gIGJhck9wYWNpdHk/OiBzdHJpbmc7XG4gIGJhcldpZHRoPzogc3RyaW5nO1xuICBiYXJCb3JkZXJSYWRpdXM/OiBzdHJpbmc7XG4gIGJhck1hcmdpbj86IHN0cmluZztcbiAgZ3JpZEJhY2tncm91bmQ/OiBzdHJpbmc7XG4gIGdyaWRPcGFjaXR5Pzogc3RyaW5nO1xuICBncmlkV2lkdGg/OiBzdHJpbmc7XG4gIGdyaWRCb3JkZXJSYWRpdXM/OiBzdHJpbmc7XG4gIGdyaWRNYXJnaW4/OiBzdHJpbmc7XG4gIGFsd2F5c1Zpc2libGU/OiBib29sZWFuO1xuICB2aXNpYmxlVGltZW91dD86IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcihvYmo/OiBJU2xpbVNjcm9sbE9wdGlvbnMpIHtcbiAgICB0aGlzLnBvc2l0aW9uID0gb2JqICYmIG9iai5wb3NpdGlvbiA/IG9iai5wb3NpdGlvbiA6ICdyaWdodCc7XG4gICAgdGhpcy5iYXJCYWNrZ3JvdW5kID0gb2JqICYmIG9iai5iYXJCYWNrZ3JvdW5kID8gb2JqLmJhckJhY2tncm91bmQgOiAnIzM0M2E0MCc7XG4gICAgdGhpcy5iYXJPcGFjaXR5ID0gb2JqICYmIG9iai5iYXJPcGFjaXR5ID8gb2JqLmJhck9wYWNpdHkgOiAnMSc7XG4gICAgdGhpcy5iYXJXaWR0aCA9IG9iaiAmJiBvYmouYmFyV2lkdGggPyBvYmouYmFyV2lkdGggOiAnMTInO1xuICAgIHRoaXMuYmFyQm9yZGVyUmFkaXVzID0gb2JqICYmIG9iai5iYXJCb3JkZXJSYWRpdXMgPyBvYmouYmFyQm9yZGVyUmFkaXVzIDogJzUnO1xuICAgIHRoaXMuYmFyTWFyZ2luID0gb2JqICYmIG9iai5iYXJNYXJnaW4gPyBvYmouYmFyTWFyZ2luIDogJzFweCAwJztcbiAgICB0aGlzLmdyaWRCYWNrZ3JvdW5kID0gb2JqICYmIG9iai5ncmlkQmFja2dyb3VuZCA/IG9iai5ncmlkQmFja2dyb3VuZCA6ICcjYWRiNWJkJztcbiAgICB0aGlzLmdyaWRPcGFjaXR5ID0gb2JqICYmIG9iai5ncmlkT3BhY2l0eSA/IG9iai5ncmlkT3BhY2l0eSA6ICcxJztcbiAgICB0aGlzLmdyaWRXaWR0aCA9IG9iaiAmJiBvYmouZ3JpZFdpZHRoID8gb2JqLmdyaWRXaWR0aCA6ICc4JztcbiAgICB0aGlzLmdyaWRCb3JkZXJSYWRpdXMgPSBvYmogJiYgb2JqLmdyaWRCb3JkZXJSYWRpdXMgPyBvYmouZ3JpZEJvcmRlclJhZGl1cyA6ICcxMCc7XG4gICAgdGhpcy5ncmlkTWFyZ2luID0gb2JqICYmIG9iai5ncmlkTWFyZ2luID8gb2JqLmdyaWRNYXJnaW4gOiAnMXB4IDJweCc7XG4gICAgdGhpcy5hbHdheXNWaXNpYmxlID0gb2JqICYmIHR5cGVvZiBvYmouYWx3YXlzVmlzaWJsZSAhPT0gJ3VuZGVmaW5lZCcgPyBvYmouYWx3YXlzVmlzaWJsZSA6IHRydWU7XG4gICAgdGhpcy52aXNpYmxlVGltZW91dCA9IG9iaiAmJiBvYmoudmlzaWJsZVRpbWVvdXQgPyBvYmoudmlzaWJsZVRpbWVvdXQgOiAxMDAwO1xuICB9XG5cbiAgcHVibGljIG1lcmdlKG9iaj86IElTbGltU2Nyb2xsT3B0aW9ucyk6IFNsaW1TY3JvbGxPcHRpb25zIHtcbiAgICBjb25zdCByZXN1bHQgPSBuZXcgU2xpbVNjcm9sbE9wdGlvbnMoKTtcblxuICAgIHJlc3VsdC5wb3NpdGlvbiA9IG9iaiAmJiBvYmoucG9zaXRpb24gPyBvYmoucG9zaXRpb24gOiB0aGlzLnBvc2l0aW9uO1xuICAgIHJlc3VsdC5iYXJCYWNrZ3JvdW5kID0gb2JqICYmIG9iai5iYXJCYWNrZ3JvdW5kID8gb2JqLmJhckJhY2tncm91bmQgOiB0aGlzLmJhckJhY2tncm91bmQ7XG4gICAgcmVzdWx0LmJhck9wYWNpdHkgPSBvYmogJiYgb2JqLmJhck9wYWNpdHkgPyBvYmouYmFyT3BhY2l0eSA6IHRoaXMuYmFyT3BhY2l0eTtcbiAgICByZXN1bHQuYmFyV2lkdGggPSBvYmogJiYgb2JqLmJhcldpZHRoID8gb2JqLmJhcldpZHRoIDogdGhpcy5iYXJXaWR0aDtcbiAgICByZXN1bHQuYmFyQm9yZGVyUmFkaXVzID0gb2JqICYmIG9iai5iYXJCb3JkZXJSYWRpdXMgPyBvYmouYmFyQm9yZGVyUmFkaXVzIDogdGhpcy5iYXJCb3JkZXJSYWRpdXM7XG4gICAgcmVzdWx0LmJhck1hcmdpbiA9IG9iaiAmJiBvYmouYmFyTWFyZ2luID8gb2JqLmJhck1hcmdpbiA6IHRoaXMuYmFyTWFyZ2luO1xuICAgIHJlc3VsdC5ncmlkQmFja2dyb3VuZCA9IG9iaiAmJiBvYmouZ3JpZEJhY2tncm91bmQgPyBvYmouZ3JpZEJhY2tncm91bmQgOiB0aGlzLmdyaWRCYWNrZ3JvdW5kO1xuICAgIHJlc3VsdC5ncmlkT3BhY2l0eSA9IG9iaiAmJiBvYmouZ3JpZE9wYWNpdHkgPyBvYmouZ3JpZE9wYWNpdHkgOiB0aGlzLmdyaWRCYWNrZ3JvdW5kO1xuICAgIHJlc3VsdC5ncmlkV2lkdGggPSBvYmogJiYgb2JqLmdyaWRXaWR0aCA/IG9iai5ncmlkV2lkdGggOiB0aGlzLmdyaWRXaWR0aDtcbiAgICByZXN1bHQuZ3JpZEJvcmRlclJhZGl1cyA9IG9iaiAmJiBvYmouZ3JpZEJvcmRlclJhZGl1cyA/IG9iai5ncmlkQm9yZGVyUmFkaXVzIDogdGhpcy5ncmlkQm9yZGVyUmFkaXVzO1xuICAgIHJlc3VsdC5ncmlkTWFyZ2luID0gb2JqICYmIG9iai5ncmlkTWFyZ2luID8gb2JqLmdyaWRNYXJnaW4gOiB0aGlzLmdyaWRNYXJnaW47XG4gICAgcmVzdWx0LmFsd2F5c1Zpc2libGUgPSBvYmogJiYgdHlwZW9mIG9iai5hbHdheXNWaXNpYmxlICE9PSAndW5kZWZpbmVkJyA/IG9iai5hbHdheXNWaXNpYmxlIDogdGhpcy5hbHdheXNWaXNpYmxlO1xuICAgIHJlc3VsdC52aXNpYmxlVGltZW91dCA9IG9iaiAmJiBvYmoudmlzaWJsZVRpbWVvdXQgPyBvYmoudmlzaWJsZVRpbWVvdXQgOiB0aGlzLnZpc2libGVUaW1lb3V0O1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufVxuIiwiLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuLyogZ2xvYmFsIFJlZmxlY3QsIFByb21pc2UgKi9cclxuXHJcbnZhciBleHRlbmRTdGF0aWNzID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8XHJcbiAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICBmdW5jdGlvbiAoZCwgYikgeyBmb3IgKHZhciBwIGluIGIpIGlmIChiLmhhc093blByb3BlcnR5KHApKSBkW3BdID0gYltwXTsgfTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiBfX2Fzc2lnbih0KSB7XHJcbiAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSkgdFtwXSA9IHNbcF07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVzdChzLCBlKSB7XHJcbiAgICB2YXIgdCA9IHt9O1xyXG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXHJcbiAgICAgICAgdFtwXSA9IHNbcF07XHJcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDApXHJcbiAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xyXG4gICAgcmV0dXJuIHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2RlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19wYXJhbShwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSkge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXRlcih0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcclxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUocmVzdWx0LnZhbHVlKTsgfSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxyXG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSB5W29wWzBdICYgMiA/IFwicmV0dXJuXCIgOiBvcFswXSA/IFwidGhyb3dcIiA6IFwibmV4dFwiXSkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbMCwgdC52YWx1ZV07XHJcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxyXG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19leHBvcnRTdGFyKG0sIGV4cG9ydHMpIHtcclxuICAgIGZvciAodmFyIHAgaW4gbSkgaWYgKCFleHBvcnRzLmhhc093blByb3BlcnR5KHApKSBleHBvcnRzW3BdID0gbVtwXTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fdmFsdWVzKG8pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXSwgaSA9IDA7XHJcbiAgICBpZiAobSkgcmV0dXJuIG0uY2FsbChvKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgbmV4dDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAobyAmJiBpID49IG8ubGVuZ3RoKSBvID0gdm9pZCAwO1xyXG4gICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogbyAmJiBvW2krK10sIGRvbmU6ICFvIH07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVhZChvLCBuKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl07XHJcbiAgICBpZiAoIW0pIHJldHVybiBvO1xyXG4gICAgdmFyIGkgPSBtLmNhbGwobyksIHIsIGFyID0gW10sIGU7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIHdoaWxlICgobiA9PT0gdm9pZCAwIHx8IG4tLSA+IDApICYmICEociA9IGkubmV4dCgpKS5kb25lKSBhci5wdXNoKHIudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgY2F0Y2ggKGVycm9yKSB7IGUgPSB7IGVycm9yOiBlcnJvciB9OyB9XHJcbiAgICBmaW5hbGx5IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAociAmJiAhci5kb25lICYmIChtID0gaVtcInJldHVyblwiXSkpIG0uY2FsbChpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmluYWxseSB7IGlmIChlKSB0aHJvdyBlLmVycm9yOyB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3NwcmVhZCgpIHtcclxuICAgIGZvciAodmFyIGFyID0gW10sIGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIGFyID0gYXIuY29uY2F0KF9fcmVhZChhcmd1bWVudHNbaV0pKTtcclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXQodikge1xyXG4gICAgcmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBfX2F3YWl0ID8gKHRoaXMudiA9IHYsIHRoaXMpIDogbmV3IF9fYXdhaXQodik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jR2VuZXJhdG9yKHRoaXNBcmcsIF9hcmd1bWVudHMsIGdlbmVyYXRvcikge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBnID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pLCBpLCBxID0gW107XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaWYgKGdbbl0pIGlbbl0gPSBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKGEsIGIpIHsgcS5wdXNoKFtuLCB2LCBhLCBiXSkgPiAxIHx8IHJlc3VtZShuLCB2KTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHJlc3VtZShuLCB2KSB7IHRyeSB7IHN0ZXAoZ1tuXSh2KSk7IH0gY2F0Y2ggKGUpIHsgc2V0dGxlKHFbMF1bM10sIGUpOyB9IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAocikgeyByLnZhbHVlIGluc3RhbmNlb2YgX19hd2FpdCA/IFByb21pc2UucmVzb2x2ZShyLnZhbHVlLnYpLnRoZW4oZnVsZmlsbCwgcmVqZWN0KSA6IHNldHRsZShxWzBdWzJdLCByKTsgIH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlmIChvW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9OyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jVmFsdWVzKG8pIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgbSA9IG9bU3ltYm9sLmFzeW5jSXRlcmF0b3JdO1xyXG4gICAgcmV0dXJuIG0gPyBtLmNhbGwobykgOiB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ha2VUZW1wbGF0ZU9iamVjdChjb29rZWQsIHJhdykge1xyXG4gICAgaWYgKE9iamVjdC5kZWZpbmVQcm9wZXJ0eSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29va2VkLCBcInJhd1wiLCB7IHZhbHVlOiByYXcgfSk7IH0gZWxzZSB7IGNvb2tlZC5yYXcgPSByYXc7IH1cclxuICAgIHJldHVybiBjb29rZWQ7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnRTdGFyKG1vZCkge1xyXG4gICAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcclxuICAgIHZhciByZXN1bHQgPSB7fTtcclxuICAgIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XHJcbiAgICByZXN1bHQuZGVmYXVsdCA9IG1vZDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydERlZmF1bHQobW9kKSB7XHJcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IGRlZmF1bHQ6IG1vZCB9O1xyXG59XHJcbiIsImV4cG9ydCBpbnRlcmZhY2UgSVNsaW1TY3JvbGxTdGF0ZSB7XG4gICAgc2Nyb2xsUG9zaXRpb246IG51bWJlcjtcbiAgICBpc1Njcm9sbEF0U3RhcnQ6IGJvb2xlYW47XG4gICAgaXNTY3JvbGxBdEVuZDogYm9vbGVhbjtcbn1cblxuZXhwb3J0IGNsYXNzIFNsaW1TY3JvbGxTdGF0ZSBpbXBsZW1lbnRzIElTbGltU2Nyb2xsU3RhdGUge1xuICAgIHNjcm9sbFBvc2l0aW9uOiBudW1iZXI7XG4gICAgaXNTY3JvbGxBdFN0YXJ0OiBib29sZWFuO1xuICAgIGlzU2Nyb2xsQXRFbmQ6IGJvb2xlYW47XG4gICAgY29uc3RydWN0b3Iob2JqPzogSVNsaW1TY3JvbGxTdGF0ZSkge1xuICAgICAgICB0aGlzLnNjcm9sbFBvc2l0aW9uID0gb2JqICYmIG9iai5zY3JvbGxQb3NpdGlvbiA/IG9iai5zY3JvbGxQb3NpdGlvbiA6IDA7XG4gICAgICAgIHRoaXMuaXNTY3JvbGxBdFN0YXJ0ID0gb2JqICYmIHR5cGVvZiBvYmouaXNTY3JvbGxBdFN0YXJ0ICE9PSAndW5kZWZpbmVkJyA/IG9iai5pc1Njcm9sbEF0U3RhcnQgOiB0cnVlO1xuICAgICAgICB0aGlzLmlzU2Nyb2xsQXRFbmQgPSBvYmogJiYgdHlwZW9mIG9iai5pc1Njcm9sbEF0RW5kICE9PSAndW5kZWZpbmVkJyA/IG9iai5pc1Njcm9sbEF0RW5kIDogZmFsc2U7XG4gICAgfVxufVxuIiwiaW1wb3J0IHtcbiAgRGlyZWN0aXZlLFxuICBWaWV3Q29udGFpbmVyUmVmLFxuICBIb3N0TGlzdGVuZXIsXG4gIE9uQ2hhbmdlcyxcbiAgT25EZXN0cm95LFxuICBPbkluaXQsXG4gIFJlbmRlcmVyMixcbiAgSW5qZWN0LFxuICBPcHRpb25hbCxcbiAgSW5wdXQsXG4gIEV2ZW50RW1pdHRlcixcbiAgT3V0cHV0LFxuICBTaW1wbGVDaGFuZ2VzXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgU2xpbVNjcm9sbE9wdGlvbnMsIElTbGltU2Nyb2xsT3B0aW9ucywgU0xJTVNDUk9MTF9ERUZBVUxUUyB9IGZyb20gJy4uL2NsYXNzZXMvc2xpbXNjcm9sbC1vcHRpb25zLmNsYXNzJztcbmltcG9ydCB7IElTbGltU2Nyb2xsRXZlbnQsIFNsaW1TY3JvbGxFdmVudCB9IGZyb20gJy4uL2NsYXNzZXMvc2xpbXNjcm9sbC1ldmVudC5jbGFzcyc7XG5pbXBvcnQgeyBTbGltU2Nyb2xsU3RhdGUsIElTbGltU2Nyb2xsU3RhdGUgfSBmcm9tICcuLi9jbGFzc2VzL3NsaW1zY3JvbGwtc3RhdGUuY2xhc3MnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3Vic2NyaXB0aW9uLCBmcm9tRXZlbnQsIG1lcmdlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtZXJnZU1hcCwgbWFwLCB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBjb25zdCBlYXNpbmc6IHsgW2tleTogc3RyaW5nXTogRnVuY3Rpb24gfSA9IHtcbiAgbGluZWFyOiAodDogbnVtYmVyKSA9PiB0LFxuICBpblF1YWQ6ICh0OiBudW1iZXIpID0+IHQgKiB0LFxuICBvdXRRdWFkOiAodDogbnVtYmVyKSA9PiB0ICogKDIgLSB0KSxcbiAgaW5PdXRRdWFkOiAodDogbnVtYmVyKSA9PiB0IDwgLjUgPyAyICogdCAqIHQgOiAtMSArICg0IC0gMiAqIHQpICogdCxcbiAgaW5DdWJpYzogKHQ6IG51bWJlcikgPT4gdCAqIHQgKiB0LFxuICBvdXRDdWJpYzogKHQ6IG51bWJlcikgPT4gKC0tdCkgKiB0ICogdCArIDEsXG4gIGluT3V0Q3ViaWM6ICh0OiBudW1iZXIpID0+IHQgPCAuNSA/IDQgKiB0ICogdCAqIHQgOiAodCAtIDEpICogKDIgKiB0IC0gMikgKiAoMiAqIHQgLSAyKSArIDEsXG4gIGluUXVhcnQ6ICh0OiBudW1iZXIpID0+IHQgKiB0ICogdCAqIHQsXG4gIG91dFF1YXJ0OiAodDogbnVtYmVyKSA9PiAxIC0gKC0tdCkgKiB0ICogdCAqIHQsXG4gIGluT3V0UXVhcnQ6ICh0OiBudW1iZXIpID0+IHQgPCAuNSA/IDggKiB0ICogdCAqIHQgKiB0IDogMSAtIDggKiAoLS10KSAqIHQgKiB0ICogdCxcbiAgaW5RdWludDogKHQ6IG51bWJlcikgPT4gdCAqIHQgKiB0ICogdCAqIHQsXG4gIG91dFF1aW50OiAodDogbnVtYmVyKSA9PiAxICsgKC0tdCkgKiB0ICogdCAqIHQgKiB0LFxuICBpbk91dFF1aW50OiAodDogbnVtYmVyKSA9PiB0IDwgLjUgPyAxNiAqIHQgKiB0ICogdCAqIHQgKiB0IDogMSArIDE2ICogKC0tdCkgKiB0ICogdCAqIHQgKiB0XG59O1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbc2xpbVNjcm9sbF0nLCAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lXG4gIGV4cG9ydEFzOiAnc2xpbVNjcm9sbCdcbn0pXG5leHBvcnQgY2xhc3MgU2xpbVNjcm9sbERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBlbmFibGVkID0gdHJ1ZTtcbiAgQElucHV0KCkgb3B0aW9uczogU2xpbVNjcm9sbE9wdGlvbnM7XG4gIEBJbnB1dCgpIHNjcm9sbEV2ZW50czogRXZlbnRFbWl0dGVyPElTbGltU2Nyb2xsRXZlbnQ+O1xuICBAT3V0cHV0KCdzY3JvbGxDaGFuZ2VkJykgc2Nyb2xsQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8SVNsaW1TY3JvbGxTdGF0ZT4oKTtcbiAgQE91dHB1dCgnYmFyVmlzaWJpbGl0eUNoYW5nZScpIGJhclZpc2liaWxpdHlDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgZWw6IEhUTUxFbGVtZW50O1xuICB3cmFwcGVyOiBIVE1MRWxlbWVudDtcbiAgZ3JpZDogSFRNTEVsZW1lbnQ7XG4gIGJhcjogSFRNTEVsZW1lbnQ7XG4gIGJvZHk6IEhUTUxFbGVtZW50O1xuICBwYWdlWTogbnVtYmVyO1xuICB0b3A6IG51bWJlcjtcbiAgZHJhZ2dpbmc6IGJvb2xlYW47XG4gIG11dGF0aW9uVGhyb3R0bGVUaW1lb3V0OiBudW1iZXI7XG4gIG11dGF0aW9uT2JzZXJ2ZXI6IE11dGF0aW9uT2JzZXJ2ZXI7XG4gIGxhc3RUb3VjaFBvc2l0aW9uWTogbnVtYmVyO1xuICB2aXNpYmxlVGltZW91dDogYW55O1xuICBpbnRlcmFjdGlvblN1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbjtcbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChWaWV3Q29udGFpbmVyUmVmKSBwcml2YXRlIHZpZXdDb250YWluZXI6IFZpZXdDb250YWluZXJSZWYsXG4gICAgQEluamVjdChSZW5kZXJlcjIpIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnksXG4gICAgQEluamVjdChTTElNU0NST0xMX0RFRkFVTFRTKSBAT3B0aW9uYWwoKSBwcml2YXRlIG9wdGlvbnNEZWZhdWx0czogSVNsaW1TY3JvbGxPcHRpb25zXG4gICkge1xuICAgIHRoaXMudmlld0NvbnRhaW5lciA9IHZpZXdDb250YWluZXI7XG4gICAgdGhpcy5lbCA9IHZpZXdDb250YWluZXIuZWxlbWVudC5uYXRpdmVFbGVtZW50O1xuICAgIHRoaXMuYm9keSA9IHRoaXMuZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpO1xuICAgIHRoaXMubXV0YXRpb25UaHJvdHRsZVRpbWVvdXQgPSA1MDtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIC8vIHNldHVwIGlmIG5vIGNoYW5nZXMgZm9yIGVuYWJsZWQgZm9yIHRoZSBmaXJzdCB0aW1lXG4gICAgaWYgKCF0aGlzLmludGVyYWN0aW9uU3Vic2NyaXB0aW9ucyAmJiB0aGlzLmVuYWJsZWQpIHtcbiAgICAgIHRoaXMuc2V0dXAoKTtcbiAgICB9XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgaWYgKGNoYW5nZXMuZW5hYmxlZCkge1xuICAgICAgaWYgKHRoaXMuZW5hYmxlZCkge1xuICAgICAgICB0aGlzLnNldHVwKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmRlc3Ryb3koKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLmRlc3Ryb3koKTtcbiAgfVxuXG4gIHNldHVwKCkge1xuICAgIHRoaXMuaW50ZXJhY3Rpb25TdWJzY3JpcHRpb25zID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuICAgIGlmICh0aGlzLm9wdGlvbnNEZWZhdWx0cykge1xuICAgICAgdGhpcy5vcHRpb25zID0gbmV3IFNsaW1TY3JvbGxPcHRpb25zKHRoaXMub3B0aW9uc0RlZmF1bHRzKS5tZXJnZSh0aGlzLm9wdGlvbnMpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLm9wdGlvbnMgPSBuZXcgU2xpbVNjcm9sbE9wdGlvbnModGhpcy5vcHRpb25zKTtcbiAgICB9XG5cbiAgICB0aGlzLnNldFN0eWxlKCk7XG4gICAgdGhpcy53cmFwQ29udGFpbmVyKCk7XG4gICAgdGhpcy5pbml0R3JpZCgpO1xuICAgIHRoaXMuaW5pdEJhcigpO1xuICAgIHRoaXMuZ2V0QmFySGVpZ2h0KCk7XG4gICAgdGhpcy5pbml0V2hlZWwoKTtcbiAgICB0aGlzLmluaXREcmFnKCk7XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucy5hbHdheXNWaXNpYmxlKSB7XG4gICAgICB0aGlzLmhpZGVCYXJBbmRHcmlkKCk7XG4gICAgfVxuXG4gICAgaWYgKE11dGF0aW9uT2JzZXJ2ZXIpIHtcbiAgICAgIGlmICh0aGlzLm11dGF0aW9uT2JzZXJ2ZXIpIHtcbiAgICAgICAgdGhpcy5tdXRhdGlvbk9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbiAgICAgIH1cbiAgICAgIHRoaXMubXV0YXRpb25PYnNlcnZlciA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKCgpID0+IHtcbiAgICAgICAgaWYgKHRoaXMubXV0YXRpb25UaHJvdHRsZVRpbWVvdXQpIHtcbiAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5tdXRhdGlvblRocm90dGxlVGltZW91dCk7XG4gICAgICAgICAgdGhpcy5tdXRhdGlvblRocm90dGxlVGltZW91dCA9IHNldFRpbWVvdXQodGhpcy5vbk11dGF0aW9uLmJpbmQodGhpcyksIDUwKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICB0aGlzLm11dGF0aW9uT2JzZXJ2ZXIub2JzZXJ2ZSh0aGlzLmVsLCB7IHN1YnRyZWU6IHRydWUsIGNoaWxkTGlzdDogdHJ1ZSB9KTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5zY3JvbGxFdmVudHMgJiYgdGhpcy5zY3JvbGxFdmVudHMgaW5zdGFuY2VvZiBFdmVudEVtaXR0ZXIpIHtcbiAgICAgIGNvbnN0IHNjcm9sbFN1YnNjcmlwdGlvbiA9IHRoaXMuc2Nyb2xsRXZlbnRzLnN1YnNjcmliZSgoZXZlbnQ6IFNsaW1TY3JvbGxFdmVudCkgPT4gdGhpcy5oYW5kbGVFdmVudChldmVudCkpO1xuICAgICAgdGhpcy5pbnRlcmFjdGlvblN1YnNjcmlwdGlvbnMuYWRkKHNjcm9sbFN1YnNjcmlwdGlvbik7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlRXZlbnQoZTogU2xpbVNjcm9sbEV2ZW50KTogdm9pZCB7XG4gICAgaWYgKGUudHlwZSA9PT0gJ3Njcm9sbFRvQm90dG9tJykge1xuICAgICAgY29uc3QgeSA9IHRoaXMuZWwuc2Nyb2xsSGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQ7XG4gICAgICB0aGlzLnNjcm9sbFRvKHksIGUuZHVyYXRpb24sIGUuZWFzaW5nKTtcbiAgICB9IGVsc2UgaWYgKGUudHlwZSA9PT0gJ3Njcm9sbFRvVG9wJykge1xuICAgICAgY29uc3QgeSA9IDA7XG4gICAgICB0aGlzLnNjcm9sbFRvKHksIGUuZHVyYXRpb24sIGUuZWFzaW5nKTtcbiAgICB9IGVsc2UgaWYgKGUudHlwZSA9PT0gJ3Njcm9sbFRvUGVyY2VudCcgJiYgKGUucGVyY2VudCA+PSAwICYmIGUucGVyY2VudCA8PSAxMDApKSB7XG4gICAgICBjb25zdCB5ID0gTWF0aC5yb3VuZCgoKHRoaXMuZWwuc2Nyb2xsSGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQpIC8gMTAwKSAqIGUucGVyY2VudCk7XG4gICAgICB0aGlzLnNjcm9sbFRvKHksIGUuZHVyYXRpb24sIGUuZWFzaW5nKTtcbiAgICB9IGVsc2UgaWYgKGUudHlwZSA9PT0gJ3Njcm9sbFRvJykge1xuICAgICAgY29uc3QgeSA9IGUueTtcbiAgICAgIGlmICh5IDw9IHRoaXMuZWwuc2Nyb2xsSGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQgJiYgeSA+PSAwKSB7XG4gICAgICAgIHRoaXMuc2Nyb2xsVG8oeSwgZS5kdXJhdGlvbiwgZS5lYXNpbmcpO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoZS50eXBlID09PSAncmVjYWxjdWxhdGUnKSB7XG4gICAgICB0aGlzLmdldEJhckhlaWdodCgpO1xuICAgIH1cbiAgfVxuXG4gIHNldFN0eWxlKCk6IHZvaWQge1xuICAgIGNvbnN0IGVsID0gdGhpcy5lbDtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGVsLCAnb3ZlcmZsb3cnLCAnaGlkZGVuJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShlbCwgJ3Bvc2l0aW9uJywgJ3JlbGF0aXZlJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShlbCwgJ2Rpc3BsYXknLCAnYmxvY2snKTtcbiAgfVxuXG4gIG9uTXV0YXRpb24oKSB7XG4gICAgdGhpcy5nZXRCYXJIZWlnaHQoKTtcbiAgfVxuXG4gIHdyYXBDb250YWluZXIoKTogdm9pZCB7XG4gICAgdGhpcy53cmFwcGVyID0gdGhpcy5yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBjb25zdCB3cmFwcGVyID0gdGhpcy53cmFwcGVyO1xuICAgIGNvbnN0IGVsID0gdGhpcy5lbDtcblxuICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3Mod3JhcHBlciwgJ3NsaW1zY3JvbGwtd3JhcHBlcicpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUod3JhcHBlciwgJ3Bvc2l0aW9uJywgJ3JlbGF0aXZlJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh3cmFwcGVyLCAnb3ZlcmZsb3cnLCAnaGlkZGVuJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh3cmFwcGVyLCAnZGlzcGxheScsICdpbmxpbmUtYmxvY2snKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHdyYXBwZXIsICdtYXJnaW4nLCBnZXRDb21wdXRlZFN0eWxlKGVsKS5tYXJnaW4pO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUod3JhcHBlciwgJ3dpZHRoJywgJzEwMCUnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHdyYXBwZXIsICdoZWlnaHQnLCBnZXRDb21wdXRlZFN0eWxlKGVsKS5oZWlnaHQpO1xuXG4gICAgdGhpcy5yZW5kZXJlci5pbnNlcnRCZWZvcmUoZWwucGFyZW50Tm9kZSwgd3JhcHBlciwgZWwpO1xuICAgIHRoaXMucmVuZGVyZXIuYXBwZW5kQ2hpbGQod3JhcHBlciwgZWwpO1xuICB9XG5cbiAgaW5pdEdyaWQoKTogdm9pZCB7XG4gICAgdGhpcy5ncmlkID0gdGhpcy5yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBjb25zdCBncmlkID0gdGhpcy5ncmlkO1xuXG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhncmlkLCAnc2xpbXNjcm9sbC1ncmlkJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAncG9zaXRpb24nLCAnYWJzb2x1dGUnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGdyaWQsICd0b3AnLCAnMCcpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZ3JpZCwgJ2JvdHRvbScsICcwJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCB0aGlzLm9wdGlvbnMucG9zaXRpb24sICcwJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAnd2lkdGgnLCBgJHt0aGlzLm9wdGlvbnMuZ3JpZFdpZHRofXB4YCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAnYmFja2dyb3VuZCcsIHRoaXMub3B0aW9ucy5ncmlkQmFja2dyb3VuZCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAnb3BhY2l0eScsIHRoaXMub3B0aW9ucy5ncmlkT3BhY2l0eSk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAnZGlzcGxheScsICdibG9jaycpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZ3JpZCwgJ2N1cnNvcicsICdwb2ludGVyJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShncmlkLCAnei1pbmRleCcsICc5OScpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZ3JpZCwgJ2JvcmRlci1yYWRpdXMnLCBgJHt0aGlzLm9wdGlvbnMuZ3JpZEJvcmRlclJhZGl1c31weGApO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZ3JpZCwgJ21hcmdpbicsIHRoaXMub3B0aW9ucy5ncmlkTWFyZ2luKTtcblxuICAgIHRoaXMucmVuZGVyZXIuYXBwZW5kQ2hpbGQodGhpcy53cmFwcGVyLCBncmlkKTtcbiAgfVxuXG4gIGluaXRCYXIoKTogdm9pZCB7XG4gICAgdGhpcy5iYXIgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgIGNvbnN0IGJhciA9IHRoaXMuYmFyO1xuXG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhiYXIsICdzbGltc2Nyb2xsLWJhcicpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYmFyLCAncG9zaXRpb24nLCAnYWJzb2x1dGUnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJhciwgJ3RvcCcsICcwJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShiYXIsIHRoaXMub3B0aW9ucy5wb3NpdGlvbiwgJzAnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJhciwgJ3dpZHRoJywgYCR7dGhpcy5vcHRpb25zLmJhcldpZHRofXB4YCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShiYXIsICdiYWNrZ3JvdW5kJywgdGhpcy5vcHRpb25zLmJhckJhY2tncm91bmQpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYmFyLCAnb3BhY2l0eScsIHRoaXMub3B0aW9ucy5iYXJPcGFjaXR5KTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJhciwgJ2Rpc3BsYXknLCAnYmxvY2snKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJhciwgJ2N1cnNvcicsICdwb2ludGVyJyk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShiYXIsICd6LWluZGV4JywgJzEwMCcpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoYmFyLCAnYm9yZGVyLXJhZGl1cycsIGAke3RoaXMub3B0aW9ucy5iYXJCb3JkZXJSYWRpdXN9cHhgKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGJhciwgJ21hcmdpbicsIHRoaXMub3B0aW9ucy5iYXJNYXJnaW4pO1xuXG4gICAgdGhpcy5yZW5kZXJlci5hcHBlbmRDaGlsZCh0aGlzLndyYXBwZXIsIGJhcik7XG4gICAgdGhpcy5iYXJWaXNpYmlsaXR5Q2hhbmdlLmVtaXQodHJ1ZSk7XG4gIH1cblxuICBnZXRCYXJIZWlnaHQoKTogdm9pZCB7XG4gICAgY29uc3QgZWxIZWlnaHQgPSB0aGlzLmVsLm9mZnNldEhlaWdodDtcbiAgICBjb25zdCBiYXJIZWlnaHQgPSBNYXRoLm1heCgoZWxIZWlnaHQgLyB0aGlzLmVsLnNjcm9sbEhlaWdodCkgKiBlbEhlaWdodCwgMzApICsgJ3B4JztcbiAgICBjb25zdCBkaXNwbGF5ID0gcGFyc2VJbnQoYmFySGVpZ2h0LCAxMCkgPT09IGVsSGVpZ2h0ID8gJ25vbmUnIDogJ2Jsb2NrJztcblxuICAgIGlmICh0aGlzLndyYXBwZXIub2Zmc2V0SGVpZ2h0ICE9PSBlbEhlaWdodCkge1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLndyYXBwZXIsICdoZWlnaHQnLCBlbEhlaWdodCArICdweCcpO1xuICAgIH1cblxuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5iYXIsICdoZWlnaHQnLCBiYXJIZWlnaHQpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5iYXIsICdkaXNwbGF5JywgZGlzcGxheSk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmdyaWQsICdkaXNwbGF5JywgZGlzcGxheSk7XG4gICAgdGhpcy5iYXJWaXNpYmlsaXR5Q2hhbmdlLmVtaXQoZGlzcGxheSAhPT0gJ25vbmUnKTtcbiAgfVxuXG4gIHNjcm9sbFRvKHk6IG51bWJlciwgZHVyYXRpb246IG51bWJlciwgZWFzaW5nRnVuYzogc3RyaW5nKTogdm9pZCB7XG4gICAgY29uc3Qgc3RhcnQgPSBEYXRlLm5vdygpO1xuICAgIGNvbnN0IGZyb20gPSB0aGlzLmVsLnNjcm9sbFRvcDtcbiAgICBjb25zdCBtYXhUb3AgPSB0aGlzLmVsLm9mZnNldEhlaWdodCAtIHRoaXMuYmFyLm9mZnNldEhlaWdodDtcbiAgICBjb25zdCBtYXhFbFNjcm9sbFRvcCA9IHRoaXMuZWwuc2Nyb2xsSGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQ7XG4gICAgY29uc3QgYmFySGVpZ2h0ID0gTWF0aC5tYXgoKHRoaXMuZWwub2Zmc2V0SGVpZ2h0IC8gdGhpcy5lbC5zY3JvbGxIZWlnaHQpICogdGhpcy5lbC5vZmZzZXRIZWlnaHQsIDMwKTtcbiAgICBjb25zdCBwYWRkaW5nVG9wID0gcGFyc2VJbnQodGhpcy5lbC5zdHlsZS5wYWRkaW5nVG9wLCAxMCkgfHwgMDtcbiAgICBjb25zdCBwYWRkaW5nQm90dG9tID0gcGFyc2VJbnQodGhpcy5lbC5zdHlsZS5wYWRkaW5nQm90dG9tLCAxMCkgfHwgMDtcblxuICAgIGNvbnN0IHNjcm9sbCA9ICh0aW1lc3RhbXA6IG51bWJlcikgPT4ge1xuICAgICAgY29uc3QgY3VycmVudFRpbWUgPSBEYXRlLm5vdygpO1xuICAgICAgY29uc3QgdGltZSA9IE1hdGgubWluKDEsICgoY3VycmVudFRpbWUgLSBzdGFydCkgLyBkdXJhdGlvbikpO1xuICAgICAgY29uc3QgZWFzZWRUaW1lID0gZWFzaW5nW2Vhc2luZ0Z1bmNdKHRpbWUpO1xuXG4gICAgICBpZiAocGFkZGluZ1RvcCA+IDAgfHwgcGFkZGluZ0JvdHRvbSA+IDApIHtcbiAgICAgICAgbGV0IGZyb21ZID0gbnVsbDtcblxuICAgICAgICBpZiAocGFkZGluZ1RvcCA+IDApIHtcbiAgICAgICAgICBmcm9tWSA9IC1wYWRkaW5nVG9wO1xuICAgICAgICAgIGZyb21ZID0gLSgoZWFzZWRUaW1lICogKHkgLSBmcm9tWSkpICsgZnJvbVkpO1xuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5lbCwgJ3BhZGRpbmdUb3AnLCBgJHtmcm9tWX1weGApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBhZGRpbmdCb3R0b20gPiAwKSB7XG4gICAgICAgICAgZnJvbVkgPSBwYWRkaW5nQm90dG9tO1xuICAgICAgICAgIGZyb21ZID0gKChlYXNlZFRpbWUgKiAoeSAtIGZyb21ZKSkgKyBmcm9tWSk7XG4gICAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmVsLCAncGFkZGluZ0JvdHRvbScsIGAke2Zyb21ZfXB4YCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZWwuc2Nyb2xsVG9wID0gKGVhc2VkVGltZSAqICh5IC0gZnJvbSkpICsgZnJvbTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgcGVyY2VudFNjcm9sbCA9IHRoaXMuZWwuc2Nyb2xsVG9wIC8gbWF4RWxTY3JvbGxUb3A7XG4gICAgICBpZiAocGFkZGluZ0JvdHRvbSA9PT0gMCkge1xuICAgICAgICBjb25zdCBkZWx0YSA9IE1hdGgucm91bmQoTWF0aC5yb3VuZCh0aGlzLmVsLmNsaWVudEhlaWdodCAqIHBlcmNlbnRTY3JvbGwpIC0gYmFySGVpZ2h0KTtcbiAgICAgICAgaWYgKGRlbHRhID4gMCkge1xuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5iYXIsICd0b3AnLCBgJHtkZWx0YX1weGApO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmICh0aW1lIDwgMSkge1xuICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc2Nyb2xsKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHNjcm9sbCk7XG4gIH1cblxuICBzY3JvbGxDb250ZW50KHk6IG51bWJlciwgaXNXaGVlbDogYm9vbGVhbiwgaXNKdW1wOiBib29sZWFuKTogbnVsbCB8IG51bWJlciB7XG4gICAgbGV0IGRlbHRhID0geTtcbiAgICBjb25zdCBtYXhUb3AgPSB0aGlzLmVsLm9mZnNldEhlaWdodCAtIHRoaXMuYmFyLm9mZnNldEhlaWdodDtcbiAgICBjb25zdCBoaWRkZW5Db250ZW50ID0gdGhpcy5lbC5zY3JvbGxIZWlnaHQgLSB0aGlzLmVsLm9mZnNldEhlaWdodDtcbiAgICBsZXQgcGVyY2VudFNjcm9sbDogbnVtYmVyO1xuICAgIGxldCBvdmVyID0gbnVsbDtcblxuICAgIGlmIChpc1doZWVsKSB7XG4gICAgICBkZWx0YSA9IHBhcnNlSW50KGdldENvbXB1dGVkU3R5bGUodGhpcy5iYXIpLnRvcCwgMTApICsgeSAqIDIwIC8gMTAwICogdGhpcy5iYXIub2Zmc2V0SGVpZ2h0O1xuXG4gICAgICBpZiAoZGVsdGEgPCAwIHx8IGRlbHRhID4gbWF4VG9wKSB7XG4gICAgICAgIG92ZXIgPSBkZWx0YSA+IG1heFRvcCA/IGRlbHRhIC0gbWF4VG9wIDogZGVsdGE7XG4gICAgICB9XG5cbiAgICAgIGRlbHRhID0gTWF0aC5taW4oTWF0aC5tYXgoZGVsdGEsIDApLCBtYXhUb3ApO1xuICAgICAgZGVsdGEgPSAoeSA+IDApID8gTWF0aC5jZWlsKGRlbHRhKSA6IE1hdGguZmxvb3IoZGVsdGEpO1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJhciwgJ3RvcCcsIGRlbHRhICsgJ3B4Jyk7XG4gICAgfVxuXG4gICAgcGVyY2VudFNjcm9sbCA9IHBhcnNlSW50KGdldENvbXB1dGVkU3R5bGUodGhpcy5iYXIpLnRvcCwgMTApIC8gKHRoaXMuZWwub2Zmc2V0SGVpZ2h0IC0gdGhpcy5iYXIub2Zmc2V0SGVpZ2h0KTtcbiAgICBkZWx0YSA9IHBlcmNlbnRTY3JvbGwgKiBoaWRkZW5Db250ZW50O1xuXG4gICAgdGhpcy5lbC5zY3JvbGxUb3AgPSBkZWx0YTtcblxuICAgIHRoaXMuc2hvd0JhckFuZEdyaWQoKTtcblxuICAgIGlmICghdGhpcy5vcHRpb25zLmFsd2F5c1Zpc2libGUpIHtcbiAgICAgIGlmICh0aGlzLnZpc2libGVUaW1lb3V0KSB7XG4gICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnZpc2libGVUaW1lb3V0KTtcbiAgICAgIH1cblxuICAgICAgdGhpcy52aXNpYmxlVGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmhpZGVCYXJBbmRHcmlkKCk7XG4gICAgICB9LCB0aGlzLm9wdGlvbnMudmlzaWJsZVRpbWVvdXQpO1xuICAgIH1cbiAgICBjb25zdCBpc1Njcm9sbEF0U3RhcnQgPSBkZWx0YSA9PT0gMDtcbiAgICBjb25zdCBpc1Njcm9sbEF0RW5kID0gZGVsdGEgPT09IGhpZGRlbkNvbnRlbnQ7XG4gICAgY29uc3Qgc2Nyb2xsUG9zaXRpb24gPSBNYXRoLmNlaWwoZGVsdGEpO1xuICAgIGNvbnN0IHNjcm9sbFN0YXRlID0gbmV3IFNsaW1TY3JvbGxTdGF0ZSh7IHNjcm9sbFBvc2l0aW9uLCBpc1Njcm9sbEF0U3RhcnQsIGlzU2Nyb2xsQXRFbmQgfSk7XG4gICAgdGhpcy5zY3JvbGxDaGFuZ2VkLmVtaXQoc2Nyb2xsU3RhdGUpO1xuXG4gICAgcmV0dXJuIG92ZXI7XG4gIH1cblxuICBpbml0V2hlZWwgPSAoKSA9PiB7XG4gICAgY29uc3QgZG9tbW91c2VzY3JvbGwgPSBmcm9tRXZlbnQodGhpcy5lbCwgJ0RPTU1vdXNlU2Nyb2xsJyk7XG4gICAgY29uc3QgbW91c2V3aGVlbCA9IGZyb21FdmVudCh0aGlzLmVsLCAnbW91c2V3aGVlbCcpO1xuXG4gICAgY29uc3Qgd2hlZWxTdWJzY3JpcHRpb24gPSBtZXJnZSguLi5bZG9tbW91c2VzY3JvbGwsIG1vdXNld2hlZWxdKS5zdWJzY3JpYmUoKGU6IE1vdXNlV2hlZWxFdmVudCkgPT4ge1xuICAgICAgbGV0IGRlbHRhID0gMDtcblxuICAgICAgaWYgKGUud2hlZWxEZWx0YSkge1xuICAgICAgICBkZWx0YSA9IC1lLndoZWVsRGVsdGEgLyAxMjA7XG4gICAgICB9XG5cbiAgICAgIGlmIChlLmRldGFpbCkge1xuICAgICAgICBkZWx0YSA9IGUuZGV0YWlsIC8gMztcbiAgICAgIH1cblxuICAgICAgdGhpcy5zY3JvbGxDb250ZW50KGRlbHRhLCB0cnVlLCBmYWxzZSk7XG5cbiAgICAgIGlmIChlLnByZXZlbnREZWZhdWx0KSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuaW50ZXJhY3Rpb25TdWJzY3JpcHRpb25zLmFkZCh3aGVlbFN1YnNjcmlwdGlvbik7XG4gIH1cblxuICBpbml0RHJhZyA9ICgpID0+IHtcbiAgICBjb25zdCBiYXIgPSB0aGlzLmJhcjtcblxuICAgIGNvbnN0IG1vdXNlbW92ZSA9IGZyb21FdmVudCh0aGlzLmRvY3VtZW50LmRvY3VtZW50RWxlbWVudCwgJ21vdXNlbW92ZScpO1xuICAgIGNvbnN0IHRvdWNobW92ZSA9IGZyb21FdmVudCh0aGlzLmRvY3VtZW50LmRvY3VtZW50RWxlbWVudCwgJ3RvdWNobW92ZScpO1xuXG4gICAgY29uc3QgbW91c2Vkb3duID0gZnJvbUV2ZW50KGJhciwgJ21vdXNlZG93bicpO1xuICAgIGNvbnN0IHRvdWNoc3RhcnQgPSBmcm9tRXZlbnQodGhpcy5lbCwgJ3RvdWNoc3RhcnQnKTtcbiAgICBjb25zdCBtb3VzZXVwID0gZnJvbUV2ZW50KHRoaXMuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCAnbW91c2V1cCcpO1xuICAgIGNvbnN0IHRvdWNoZW5kID0gZnJvbUV2ZW50KHRoaXMuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCAndG91Y2hlbmQnKTtcblxuICAgIGNvbnN0IG1vdXNlZHJhZyA9IG1vdXNlZG93blxuICAgICAgLnBpcGUoXG4gICAgICAgIG1lcmdlTWFwKChlOiBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICAgICAgdGhpcy5wYWdlWSA9IGUucGFnZVk7XG4gICAgICAgICAgdGhpcy50b3AgPSBwYXJzZUZsb2F0KGdldENvbXB1dGVkU3R5bGUoYmFyKS50b3ApO1xuXG4gICAgICAgICAgcmV0dXJuIG1vdXNlbW92ZVxuICAgICAgICAgICAgLnBpcGUoXG4gICAgICAgICAgICAgIG1hcCgoZW1vdmU6IE1vdXNlRXZlbnQpID0+IHtcbiAgICAgICAgICAgICAgICBlbW92ZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRvcCArIGVtb3ZlLnBhZ2VZIC0gdGhpcy5wYWdlWTtcbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIHRha2VVbnRpbChtb3VzZXVwKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSlcbiAgICAgICk7XG5cbiAgICBjb25zdCB0b3VjaGRyYWcgPSB0b3VjaHN0YXJ0XG4gICAgICAucGlwZShcbiAgICAgICAgbWVyZ2VNYXAoKGU6IFRvdWNoRXZlbnQpID0+IHtcbiAgICAgICAgICB0aGlzLnBhZ2VZID0gZS50YXJnZXRUb3VjaGVzWzBdLnBhZ2VZO1xuICAgICAgICAgIHRoaXMudG9wID0gLXBhcnNlRmxvYXQoZ2V0Q29tcHV0ZWRTdHlsZShiYXIpLnRvcCk7XG5cbiAgICAgICAgICByZXR1cm4gdG91Y2htb3ZlXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgbWFwKCh0bW92ZTogVG91Y2hFdmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiAtKHRoaXMudG9wICsgdG1vdmUudGFyZ2V0VG91Y2hlc1swXS5wYWdlWSAtIHRoaXMucGFnZVkpO1xuICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgdGFrZVVudGlsKHRvdWNoZW5kKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSlcbiAgICAgICk7XG5cbiAgICBjb25zdCBkcmFnU3Vic2NyaXB0aW9uID0gbWVyZ2UoLi4uW21vdXNlZHJhZywgdG91Y2hkcmFnXSkuc3Vic2NyaWJlKCh0b3A6IG51bWJlcikgPT4ge1xuICAgICAgdGhpcy5ib2R5LmFkZEV2ZW50TGlzdGVuZXIoJ3NlbGVjdHN0YXJ0JywgdGhpcy5wcmV2ZW50RGVmYXVsdEV2ZW50LCBmYWxzZSk7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYm9keSwgJ3RvdWNoLWFjdGlvbicsICdwYW4teScpO1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJvZHksICd1c2VyLXNlbGVjdCcsICdub25lJyk7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYmFyLCAndG9wJywgYCR7dG9wfXB4YCk7XG4gICAgICBjb25zdCBvdmVyID0gdGhpcy5zY3JvbGxDb250ZW50KDAsIHRydWUsIGZhbHNlKTtcbiAgICAgIGNvbnN0IG1heFRvcCA9IHRoaXMuZWwub2Zmc2V0SGVpZ2h0IC0gdGhpcy5iYXIub2Zmc2V0SGVpZ2h0O1xuXG4gICAgICBpZiAob3ZlciAmJiBvdmVyIDwgMCAmJiAtb3ZlciA8PSBtYXhUb3ApIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmVsLCAncGFkZGluZ1RvcCcsIC1vdmVyICsgJ3B4Jyk7XG4gICAgICB9IGVsc2UgaWYgKG92ZXIgJiYgb3ZlciA+IDAgJiYgb3ZlciA8PSBtYXhUb3ApIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmVsLCAncGFkZGluZ0JvdHRvbScsIG92ZXIgKyAncHgnKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGNvbnN0IGRyYWdFbmRTdWJzY3JpcHRpb24gPSBtZXJnZSguLi5bbW91c2V1cCwgdG91Y2hlbmRdKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdGhpcy5ib2R5LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3NlbGVjdHN0YXJ0JywgdGhpcy5wcmV2ZW50RGVmYXVsdEV2ZW50LCBmYWxzZSk7XG4gICAgICBjb25zdCBwYWRkaW5nVG9wID0gcGFyc2VJbnQodGhpcy5lbC5zdHlsZS5wYWRkaW5nVG9wLCAxMCk7XG4gICAgICBjb25zdCBwYWRkaW5nQm90dG9tID0gcGFyc2VJbnQodGhpcy5lbC5zdHlsZS5wYWRkaW5nQm90dG9tLCAxMCk7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYm9keSwgJ3RvdWNoLWFjdGlvbicsICd1bnNldCcpO1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJvZHksICd1c2VyLXNlbGVjdCcsICdkZWZhdWx0Jyk7XG5cbiAgICAgIGlmIChwYWRkaW5nVG9wID4gMCkge1xuICAgICAgICB0aGlzLnNjcm9sbFRvKDAsIDMwMCwgJ2xpbmVhcicpO1xuICAgICAgfSBlbHNlIGlmIChwYWRkaW5nQm90dG9tID4gMCkge1xuICAgICAgICB0aGlzLnNjcm9sbFRvKDAsIDMwMCwgJ2xpbmVhcicpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5pbnRlcmFjdGlvblN1YnNjcmlwdGlvbnMuYWRkKGRyYWdTdWJzY3JpcHRpb24pO1xuICAgIHRoaXMuaW50ZXJhY3Rpb25TdWJzY3JpcHRpb25zLmFkZChkcmFnRW5kU3Vic2NyaXB0aW9uKTtcbiAgfVxuXG4gIHNob3dCYXJBbmRHcmlkKCk6IHZvaWQge1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5ncmlkLCAnYmFja2dyb3VuZCcsIHRoaXMub3B0aW9ucy5ncmlkQmFja2dyb3VuZCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJhciwgJ2JhY2tncm91bmQnLCB0aGlzLm9wdGlvbnMuYmFyQmFja2dyb3VuZCk7XG4gIH1cblxuICBoaWRlQmFyQW5kR3JpZCgpOiB2b2lkIHtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZ3JpZCwgJ2JhY2tncm91bmQnLCAndHJhbnNwYXJlbnQnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYmFyLCAnYmFja2dyb3VuZCcsICd0cmFuc3BhcmVudCcpO1xuICB9XG5cbiAgcHJldmVudERlZmF1bHRFdmVudCA9IChlOiBNb3VzZUV2ZW50KSA9PiB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gIH1cblxuICBkZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLm11dGF0aW9uT2JzZXJ2ZXIpIHtcbiAgICAgIHRoaXMubXV0YXRpb25PYnNlcnZlci5kaXNjb25uZWN0KCk7XG4gICAgICB0aGlzLm11dGF0aW9uT2JzZXJ2ZXIgPSBudWxsO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmVsLnBhcmVudEVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdzbGltc2Nyb2xsLXdyYXBwZXInKSkge1xuICAgICAgY29uc3Qgd3JhcHBlciA9IHRoaXMuZWwucGFyZW50RWxlbWVudDtcbiAgICAgIGNvbnN0IGJhciA9IHdyYXBwZXIucXVlcnlTZWxlY3RvcignLnNsaW1zY3JvbGwtYmFyJyk7XG4gICAgICB3cmFwcGVyLnJlbW92ZUNoaWxkKGJhcik7XG4gICAgICBjb25zdCBncmlkID0gd3JhcHBlci5xdWVyeVNlbGVjdG9yKCcuc2xpbXNjcm9sbC1ncmlkJyk7XG4gICAgICB3cmFwcGVyLnJlbW92ZUNoaWxkKGdyaWQpO1xuICAgICAgdGhpcy51bndyYXAod3JhcHBlcik7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaW50ZXJhY3Rpb25TdWJzY3JpcHRpb25zKSB7XG4gICAgICB0aGlzLmludGVyYWN0aW9uU3Vic2NyaXB0aW9ucy51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHVud3JhcCh3cmFwcGVyOiBIVE1MRWxlbWVudCk6IHZvaWQge1xuICAgIGNvbnN0IGRvY0ZyYWcgPSBkb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCk7XG4gICAgd2hpbGUgKHdyYXBwZXIuZmlyc3RDaGlsZCkge1xuICAgICAgY29uc3QgY2hpbGQgPSB3cmFwcGVyLnJlbW92ZUNoaWxkKHdyYXBwZXIuZmlyc3RDaGlsZCk7XG4gICAgICBkb2NGcmFnLmFwcGVuZENoaWxkKGNoaWxkKTtcbiAgICB9XG4gICAgd3JhcHBlci5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChkb2NGcmFnLCB3cmFwcGVyKTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbJyRldmVudCddKVxuICBvblJlc2l6ZSgkZXZlbnQ6IGFueSkge1xuICAgIHRoaXMuZ2V0QmFySGVpZ2h0KCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTbGltU2Nyb2xsRGlyZWN0aXZlIH0gZnJvbSAnLi4vZGlyZWN0aXZlcy9zbGltc2Nyb2xsLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFNsaW1TY3JvbGxEaXJlY3RpdmVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIFNsaW1TY3JvbGxEaXJlY3RpdmVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBOZ1NsaW1TY3JvbGxNb2R1bGUgeyB9XG4iXSwibmFtZXMiOlsiSW5qZWN0aW9uVG9rZW4iLCJFdmVudEVtaXR0ZXIiLCJmcm9tRXZlbnQiLCJtZXJnZSIsIm1lcmdlTWFwIiwibWFwIiwidGFrZVVudGlsIiwiU3Vic2NyaXB0aW9uIiwiRGlyZWN0aXZlIiwiVmlld0NvbnRhaW5lclJlZiIsIkluamVjdCIsIlJlbmRlcmVyMiIsIkRPQ1VNRU5UIiwiT3B0aW9uYWwiLCJJbnB1dCIsIk91dHB1dCIsIkhvc3RMaXN0ZW5lciIsIk5nTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O1FBVUE7UUFTRSx5QkFBWSxHQUFzQjtZQUNoQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDckIsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQztTQUN6RDs4QkF6Qkg7UUEwQkM7Ozs7OztBQ3hCRCx5QkFrQmEsbUJBQW1CLEdBQzFCLElBQUlBLG1CQUFjLENBQUMseUJBQXlCLENBQUMsQ0FBQztBQUVwRCxRQUFBO1FBZUUsMkJBQVksR0FBd0I7WUFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztZQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1lBQzlFLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7WUFDL0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUMxRCxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDO1lBQzlFLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7WUFDaEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQztZQUNqRixJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7WUFDNUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUNsRixJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxJQUFJLE9BQU8sR0FBRyxDQUFDLGFBQWEsS0FBSyxXQUFXLEdBQUcsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDaEcsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM3RTs7Ozs7UUFFTSxpQ0FBSzs7OztzQkFBQyxHQUF3QjtnQkFDbkMscUJBQU0sTUFBTSxHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQztnQkFFdkMsTUFBTSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JFLE1BQU0sQ0FBQyxhQUFhLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUN6RixNQUFNLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDN0UsTUFBTSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JFLE1BQU0sQ0FBQyxlQUFlLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO2dCQUNqRyxNQUFNLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDekUsTUFBTSxDQUFDLGNBQWMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGNBQWMsR0FBRyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzdGLE1BQU0sQ0FBQyxXQUFXLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2dCQUNwRixNQUFNLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDekUsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDckcsTUFBTSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQzdFLE1BQU0sQ0FBQyxhQUFhLEdBQUcsR0FBRyxJQUFJLE9BQU8sR0FBRyxDQUFDLGFBQWEsS0FBSyxXQUFXLEdBQUcsR0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUNoSCxNQUFNLENBQUMsY0FBYyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFFN0YsT0FBTyxNQUFNLENBQUM7O2dDQXZFbEI7UUF5RUM7O0lDekVEOzs7Ozs7Ozs7Ozs7OztBQWNBLG9CQWlHdUIsQ0FBQyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLEdBQUcsT0FBTyxNQUFNLEtBQUssVUFBVSxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLENBQUM7WUFBRSxPQUFPLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNqQyxJQUFJO1lBQ0EsT0FBTyxDQUFDLENBQUMsS0FBSyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsSUFBSTtnQkFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5RTtRQUNELE9BQU8sS0FBSyxFQUFFO1lBQUUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDO1NBQUU7Z0JBQy9CO1lBQ0osSUFBSTtnQkFDQSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3BEO29CQUNPO2dCQUFFLElBQUksQ0FBQztvQkFBRSxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFBRTtTQUNwQztRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztBQUVEO1FBQ0ksS0FBSyxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7WUFDOUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekMsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7Ozs7SUM5SEQsSUFBQTtRQUlJLHlCQUFZLEdBQXNCO1lBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7WUFDekUsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLElBQUksT0FBTyxHQUFHLENBQUMsZUFBZSxLQUFLLFdBQVcsR0FBRyxHQUFHLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUN0RyxJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsSUFBSSxPQUFPLEdBQUcsQ0FBQyxhQUFhLEtBQUssV0FBVyxHQUFHLEdBQUcsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1NBQ3BHOzhCQWRMO1FBZUMsQ0FBQTs7Ozs7O3lCQ09ZLE1BQU0sR0FBZ0M7UUFDakQsTUFBTSxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxHQUFBO1FBQ3hCLE1BQU0sRUFBRSxVQUFDLENBQVMsSUFBSyxPQUFBLENBQUMsR0FBRyxDQUFDLEdBQUE7UUFDNUIsT0FBTyxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBQTtRQUNuQyxTQUFTLEVBQUUsVUFBQyxDQUFTLElBQUssT0FBQSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFBO1FBQ25FLE9BQU8sRUFBRSxVQUFDLENBQVMsSUFBSyxPQUFBLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFBO1FBQ2pDLFFBQVEsRUFBRSxVQUFDLENBQVMsSUFBSyxPQUFBLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUE7UUFDMUMsVUFBVSxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUE7UUFDM0YsT0FBTyxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFBO1FBQ3JDLFFBQVEsRUFBRSxVQUFDLENBQVMsSUFBSyxPQUFBLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFBO1FBQzlDLFVBQVUsRUFBRSxVQUFDLENBQVMsSUFBSyxPQUFBLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUE7UUFDakYsT0FBTyxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBQTtRQUN6QyxRQUFRLEVBQUUsVUFBQyxDQUFTLElBQUssT0FBQSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUE7UUFDbEQsVUFBVSxFQUFFLFVBQUMsQ0FBUyxJQUFLLE9BQUEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFBO0tBQzVGLENBQUM7O1FBMEJBLDZCQUNvQyxlQUNQLFVBQ0QsVUFDdUI7WUFKbkQsaUJBVUM7WUFUbUMsa0JBQWEsR0FBYixhQUFhO1lBQ3BCLGFBQVEsR0FBUixRQUFRO1lBQ1QsYUFBUSxHQUFSLFFBQVE7WUFDZSxvQkFBZSxHQUFmLGVBQWU7MkJBdkIvQyxJQUFJO2lDQUdrQixJQUFJQyxpQkFBWSxFQUFvQjt1Q0FDeEIsSUFBSUEsaUJBQVksRUFBVzs2QkE0UnBFO2dCQUNWLHFCQUFNLGNBQWMsR0FBR0MsY0FBUyxDQUFDLEtBQUksQ0FBQyxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztnQkFDNUQscUJBQU0sVUFBVSxHQUFHQSxjQUFTLENBQUMsS0FBSSxDQUFDLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFFcEQscUJBQU0saUJBQWlCLEdBQUdDLFVBQUssd0JBQUksQ0FBQyxjQUFjLEVBQUUsVUFBVSxDQUFDLEdBQUUsU0FBUyxDQUFDLFVBQUMsQ0FBa0I7b0JBQzVGLHFCQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7b0JBRWQsSUFBSSxDQUFDLENBQUMsVUFBVSxFQUFFO3dCQUNoQixLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQztxQkFDN0I7b0JBRUQsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFO3dCQUNaLEtBQUssR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztxQkFDdEI7b0JBRUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUV2QyxJQUFJLENBQUMsQ0FBQyxjQUFjLEVBQUU7d0JBQ3BCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztxQkFDcEI7aUJBQ0YsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUN0RDs0QkFFVTtnQkFDVCxxQkFBTSxHQUFHLEdBQUcsS0FBSSxDQUFDLEdBQUcsQ0FBQztnQkFFckIscUJBQU0sU0FBUyxHQUFHRCxjQUFTLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3hFLHFCQUFNLFNBQVMsR0FBR0EsY0FBUyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUV4RSxxQkFBTSxTQUFTLEdBQUdBLGNBQVMsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQzlDLHFCQUFNLFVBQVUsR0FBR0EsY0FBUyxDQUFDLEtBQUksQ0FBQyxFQUFFLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ3BELHFCQUFNLE9BQU8sR0FBR0EsY0FBUyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUNwRSxxQkFBTSxRQUFRLEdBQUdBLGNBQVMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFFdEUscUJBQU0sU0FBUyxHQUFHLFNBQVM7cUJBQ3hCLElBQUksQ0FDSEUsa0JBQVEsQ0FBQyxVQUFDLENBQWE7b0JBQ3JCLEtBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDckIsS0FBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRWpELE9BQU8sU0FBUzt5QkFDYixJQUFJLENBQ0hDLGFBQUcsQ0FBQyxVQUFDLEtBQWlCO3dCQUNwQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7d0JBQ3ZCLE9BQU8sS0FBSSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUM7cUJBQzVDLENBQUMsRUFDRkMsbUJBQVMsQ0FBQyxPQUFPLENBQUMsQ0FDbkIsQ0FBQztpQkFDTCxDQUFDLENBQ0gsQ0FBQztnQkFFSixxQkFBTSxTQUFTLEdBQUcsVUFBVTtxQkFDekIsSUFBSSxDQUNIRixrQkFBUSxDQUFDLFVBQUMsQ0FBYTtvQkFDckIsS0FBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDdEMsS0FBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFFbEQsT0FBTyxTQUFTO3lCQUNiLElBQUksQ0FDSEMsYUFBRyxDQUFDLFVBQUMsS0FBaUI7d0JBQ3BCLE9BQU8sRUFBRSxLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDaEUsQ0FBQyxFQUNGQyxtQkFBUyxDQUFDLFFBQVEsQ0FBQyxDQUNwQixDQUFDO2lCQUNMLENBQUMsQ0FDSCxDQUFDO2dCQUVKLHFCQUFNLGdCQUFnQixHQUFHSCxVQUFLLHdCQUFJLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxHQUFFLFNBQVMsQ0FBQyxVQUFDLEdBQVc7b0JBQzlFLEtBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDM0UsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQzNELEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUN6RCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBSyxHQUFHLE9BQUksQ0FBQyxDQUFDO29CQUNwRCxxQkFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNoRCxxQkFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUM7b0JBRTVELElBQUksSUFBSSxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxFQUFFO3dCQUN2QyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQztxQkFDN0Q7eUJBQU0sSUFBSSxJQUFJLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO3dCQUM3QyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsRUFBRSxFQUFFLGVBQWUsRUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUM7cUJBQy9EO2lCQUNGLENBQUMsQ0FBQztnQkFFSCxxQkFBTSxtQkFBbUIsR0FBR0EsVUFBSyx3QkFBSSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsR0FBRSxTQUFTLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDOUUscUJBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzFELHFCQUFNLGFBQWEsR0FBRyxRQUFRLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUNoRSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDM0QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBRTVELElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTt3QkFDbEIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO3FCQUNqQzt5QkFBTSxJQUFJLGFBQWEsR0FBRyxDQUFDLEVBQUU7d0JBQzVCLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztxQkFDakM7aUJBQ0YsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDcEQsS0FBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2FBQ3hEO3VDQVlxQixVQUFDLENBQWE7Z0JBQ2xDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3JCO1lBMVhDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO1lBQ25DLElBQUksQ0FBQyxFQUFFLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7WUFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsRUFBRSxDQUFDO1NBQ25DOzs7O1FBRUQsc0NBQVE7OztZQUFSOztnQkFFRSxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2xELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDZDthQUNGOzs7OztRQUVELHlDQUFXOzs7O1lBQVgsVUFBWSxPQUFzQjtnQkFDaEMsSUFBSSxPQUFPLGFBQVU7b0JBQ25CLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTt3QkFDaEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNkO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFDaEI7aUJBQ0Y7YUFDRjs7OztRQUVELHlDQUFXOzs7WUFBWDtnQkFDRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDaEI7Ozs7UUFFRCxtQ0FBSzs7O1lBQUw7Z0JBQUEsaUJBcUNDO2dCQXBDQyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSUksaUJBQVksRUFBRSxDQUFDO2dCQUNuRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDaEY7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDcEQ7Z0JBRUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNmLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDcEIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNqQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBRWhCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTtvQkFDL0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2lCQUN2QjtnQkFFRCxJQUFJLGdCQUFnQixFQUFFO29CQUNwQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxDQUFDO3FCQUNwQztvQkFDRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQzt3QkFDM0MsSUFBSSxLQUFJLENBQUMsdUJBQXVCLEVBQUU7NEJBQ2hDLFlBQVksQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQzs0QkFDM0MsS0FBSSxDQUFDLHVCQUF1QixHQUFHLFVBQVUsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzt5QkFDM0U7cUJBQ0YsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQzVFO2dCQUVELElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxZQUFZTixpQkFBWSxFQUFFO29CQUNsRSxxQkFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQXNCLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFBLENBQUMsQ0FBQztvQkFDNUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2lCQUN2RDthQUNGOzs7OztRQUVELHlDQUFXOzs7O1lBQVgsVUFBWSxDQUFrQjtnQkFDNUIsSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLGdCQUFnQixFQUFFO29CQUMvQixxQkFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUN4QztxQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssYUFBYSxFQUFFO29CQUNuQyxxQkFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUN4QztxQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLEtBQUssQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sSUFBSSxHQUFHLENBQUMsRUFBRTtvQkFDL0UscUJBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3hGLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUN4QztxQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO29CQUNoQyxxQkFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZCxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDeEM7aUJBQ0Y7cUJBQU0sSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLGFBQWEsRUFBRTtvQkFDbkMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUNyQjthQUNGOzs7O1FBRUQsc0NBQVE7OztZQUFSO2dCQUNFLHFCQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUNuRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2hEOzs7O1FBRUQsd0NBQVU7OztZQUFWO2dCQUNFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzthQUNyQjs7OztRQUVELDJDQUFhOzs7WUFBYjtnQkFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsRCxxQkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDN0IscUJBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7Z0JBRW5CLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUV2RSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ3hDOzs7O1FBRUQsc0NBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQy9DLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUV2QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxPQUFJLENBQUMsQ0FBQztnQkFDckUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2xFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxlQUFlLEVBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsT0FBSSxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFFaEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUMvQzs7OztRQUVELHFDQUFPOzs7WUFBUDtnQkFDRSxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM5QyxxQkFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztnQkFFckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBSyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsT0FBSSxDQUFDLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsZUFBZSxFQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxPQUFJLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUU5RCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3JDOzs7O1FBRUQsMENBQVk7OztZQUFaO2dCQUNFLHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdEMscUJBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLElBQUksUUFBUSxFQUFFLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQztnQkFDcEYscUJBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLEtBQUssUUFBUSxHQUFHLE1BQU0sR0FBRyxPQUFPLENBQUM7Z0JBRXhFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEtBQUssUUFBUSxFQUFFO29CQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7aUJBQ2pFO2dCQUVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3RELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLE1BQU0sQ0FBQyxDQUFDO2FBQ25EOzs7Ozs7O1FBRUQsc0NBQVE7Ozs7OztZQUFSLFVBQVMsQ0FBUyxFQUFFLFFBQWdCLEVBQUUsVUFBa0I7Z0JBQXhELGlCQThDQztnQkE3Q0MscUJBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDekIscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUMvQixxQkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVELHFCQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDbkUscUJBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckcscUJBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvRCxxQkFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRXJFLHFCQUFNLE1BQU0sR0FBRyxVQUFDLFNBQWlCO29CQUMvQixxQkFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUMvQixxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsS0FBSyxJQUFJLFFBQVEsRUFBRSxDQUFDO29CQUM3RCxxQkFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUUzQyxJQUFJLFVBQVUsR0FBRyxDQUFDLElBQUksYUFBYSxHQUFHLENBQUMsRUFBRTt3QkFDdkMscUJBQUksS0FBSyxHQUFHLElBQUksQ0FBQzt3QkFFakIsSUFBSSxVQUFVLEdBQUcsQ0FBQyxFQUFFOzRCQUNsQixLQUFLLEdBQUcsQ0FBQyxVQUFVLENBQUM7NEJBQ3BCLEtBQUssR0FBRyxFQUFFLENBQUMsU0FBUyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQzs0QkFDN0MsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUssS0FBSyxPQUFJLENBQUMsQ0FBQzt5QkFDN0Q7d0JBRUQsSUFBSSxhQUFhLEdBQUcsQ0FBQyxFQUFFOzRCQUNyQixLQUFLLEdBQUcsYUFBYSxDQUFDOzRCQUN0QixLQUFLLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDOzRCQUM1QyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsRUFBRSxFQUFFLGVBQWUsRUFBSyxLQUFLLE9BQUksQ0FBQyxDQUFDO3lCQUNoRTtxQkFDRjt5QkFBTTt3QkFDTCxLQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLFNBQVMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDO3FCQUNyRDtvQkFFRCxxQkFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDO29CQUN6RCxJQUFJLGFBQWEsS0FBSyxDQUFDLEVBQUU7d0JBQ3ZCLHFCQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsYUFBYSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUM7d0JBQ3ZGLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTs0QkFDYixLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBSyxLQUFLLE9BQUksQ0FBQyxDQUFDO3lCQUN2RDtxQkFDRjtvQkFFRCxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUU7d0JBQ1oscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQy9CO2lCQUNGLENBQUM7Z0JBRUYscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDL0I7Ozs7Ozs7UUFFRCwyQ0FBYTs7Ozs7O1lBQWIsVUFBYyxDQUFTLEVBQUUsT0FBZ0IsRUFBRSxNQUFlO2dCQUExRCxpQkEwQ0M7Z0JBekNDLHFCQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7Z0JBQ2QscUJBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDO2dCQUM1RCxxQkFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUM7Z0JBQ2xFLHFCQUFJLGFBQXFCLENBQUM7Z0JBQzFCLHFCQUFJLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBRWhCLElBQUksT0FBTyxFQUFFO29CQUNYLEtBQUssR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQztvQkFFNUYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxNQUFNLEVBQUU7d0JBQy9CLElBQUksR0FBRyxLQUFLLEdBQUcsTUFBTSxHQUFHLEtBQUssR0FBRyxNQUFNLEdBQUcsS0FBSyxDQUFDO3FCQUNoRDtvQkFFRCxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0MsS0FBSyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FBQztpQkFDdkQ7Z0JBRUQsYUFBYSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzlHLEtBQUssR0FBRyxhQUFhLEdBQUcsYUFBYSxDQUFDO2dCQUV0QyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBRTFCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFFdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFO29CQUMvQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7d0JBQ3ZCLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7cUJBQ25DO29CQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsVUFBVSxDQUFDO3dCQUMvQixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDakM7Z0JBQ0QscUJBQU0sZUFBZSxHQUFHLEtBQUssS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLHFCQUFNLGFBQWEsR0FBRyxLQUFLLEtBQUssYUFBYSxDQUFDO2dCQUM5QyxxQkFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEMscUJBQU0sV0FBVyxHQUFHLElBQUksZUFBZSxDQUFDLEVBQUUsY0FBYyxnQkFBQSxFQUFFLGVBQWUsaUJBQUEsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQUM7Z0JBQzVGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUVyQyxPQUFPLElBQUksQ0FBQzthQUNiOzs7O1FBd0dELDRDQUFjOzs7WUFBZDtnQkFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUM3RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQzVFOzs7O1FBRUQsNENBQWM7OztZQUFkO2dCQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUMvRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFlBQVksRUFBRSxhQUFhLENBQUMsQ0FBQzthQUMvRDs7OztRQU9ELHFDQUFPOzs7WUFBUDtnQkFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2lCQUM5QjtnQkFFRCxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsRUFBRTtvQkFDbEUscUJBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDO29CQUN0QyxxQkFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUNyRCxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN6QixxQkFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2RCxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN0QjtnQkFFRCxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtvQkFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUM3QzthQUNGOzs7OztRQUVELG9DQUFNOzs7O1lBQU4sVUFBTyxPQUFvQjtnQkFDekIscUJBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUNsRCxPQUFPLE9BQU8sQ0FBQyxVQUFVLEVBQUU7b0JBQ3pCLHFCQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDdEQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDNUI7Z0JBQ0QsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ25EOzs7OztRQUdELHNDQUFROzs7O3NCQUFDLE1BQVc7Z0JBQ2xCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzs7O29CQXpidkJPLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsY0FBYzs7d0JBQ3hCLFFBQVEsRUFBRSxZQUFZO3FCQUN2Qjs7Ozs7d0JBdkNDQyxxQkFBZ0IsdUJBNkRiQyxXQUFNLFNBQUNELHFCQUFnQjt3QkF4RDFCRSxjQUFTLHVCQXlETkQsV0FBTSxTQUFDQyxjQUFTO3dEQUNoQkQsV0FBTSxTQUFDRSxlQUFRO3dEQUNmRixXQUFNLFNBQUMsbUJBQW1CLGNBQUdHLGFBQVE7Ozs7Z0NBdkJ2Q0MsVUFBSztnQ0FDTEEsVUFBSztxQ0FDTEEsVUFBSztzQ0FDTEMsV0FBTSxTQUFDLGVBQWU7NENBQ3RCQSxXQUFNLFNBQUMscUJBQXFCO2lDQThhNUJDLGlCQUFZLFNBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxDQUFDOztrQ0E3ZDNDOzs7Ozs7O0FDQUE7Ozs7b0JBR0NDLGFBQVEsU0FBQzt3QkFDUixZQUFZLEVBQUU7NEJBQ1osbUJBQW1CO3lCQUNwQjt3QkFDRCxPQUFPLEVBQUU7NEJBQ1AsbUJBQW1CO3lCQUNwQjtxQkFDRjs7aUNBVkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/widgets/chart/chart.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/widgets/chart/chart.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <!-- Chart widget top Start-->\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"chart-widget-top\">\r\n          <div class=\"row card-body\">\r\n            <div class=\"col-5\">\r\n              <h5 class=\"font-primary\">SALE</h5><span class=\"num\"><span class=\"counter\">90</span>%<i\r\n                  class=\"icon-angle-up f-12 ml-1\"></i></span>\r\n            </div>\r\n            <div class=\"col-7 text-right\">\r\n              <h4 class=\"num total-value\">$ <span class=\"counter\" [CountTo]=\"3654\" [from]=\"0\" [duration]=\"1\"></span>.00\r\n              </h4>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"flot-chart-placeholder\" id=\"chart-widget-top-first\">\r\n              <x-chartist [data]=\"chart1.data\" [type]=\"chart1.type\" [options]=\"chart1.options\" [events]=\"chart1.events\">\r\n              </x-chartist>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"chart-widget-top\">\r\n          <div class=\"row card-body\">\r\n            <div class=\"col-7\">\r\n              <h5 class=\"font-primary\">PROJECTS</h5><span class=\"num\"><span class=\"counter\">30</span>%<i\r\n                  class=\"icon-angle-up f-12 ml-1\"></i></span>\r\n            </div>\r\n            <div class=\"col-5 text-right\">\r\n              <h4 class=\"num total-value counter\" [CountTo]=\"12569\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"flot-chart-placeholder\" id=\"chart-widget-top-second\">\r\n              <x-chartist [data]=\"chart2.data\" [type]=\"chart2.type\" [options]=\"chart2.options\" [events]=\"chart2.events\">\r\n              </x-chartist>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"chart-widget-top\">\r\n          <div class=\"row card-body\">\r\n            <div class=\"col-8\">\r\n              <h5 class=\"font-primary\">PRODUCTS</h5><span class=\"num\"><span class=\"counter\">68</span>%<i\r\n                  class=\"icon-angle-up f-12 ml-1\"></i></span>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <h4 class=\"num total-value\"><span class=\"counter\" [CountTo]=\"93\" [from]=\"0\" [duration]=\"1\"></span>M</h4>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"flot-chart-placeholder\" id=\"chart-widget-top-third\">\r\n              <x-chartist [data]=\"chart3.data\" [type]=\"chart3.type\" [options]=\"chart3.options\" [events]=\"chart3.events\">\r\n              </x-chartist>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Chart widget top Ends-->\r\n  <!-- Chart widget with bar chart Start-->\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-6 col-md-12\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bar-chart-widget\">\r\n          <div class=\"top-content bg-primary\">\r\n            <div class=\"row\">\r\n              <div class=\"col-7\">\r\n                <div class=\"card-body pb-0\">\r\n                  <x-chartist [data]=\"WidgetBarChart1.data\" [type]=\"WidgetBarChart1.type\"\r\n                    [options]=\"WidgetBarChart1.options\">\r\n                  </x-chartist>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-5\">\r\n                <div class=\"earning-details\">\r\n                  <div class=\"text-left\"><span>Marketing Expenses</span>\r\n                    <h4 class=\"mt-1 num mb-0\">$ <span class=\"counter\" [CountTo]=\"3654\" [from]=\"0\" [duration]=\"1\"></span>\r\n                    </h4>\r\n                  </div><i class=\"icon-announcement\"></i>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-content card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-4 b-r-light\">\r\n                <div><span class=\"num font-primary\">12%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Marketing</span>\r\n                  <h4 class=\"num m-0\">$<span class=\"counter color-bottom\" [CountTo]=\"9313\" [from]=\"0\"\r\n                      [duration]=\"1\"></span></h4>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-4 b-r-light\">\r\n                <div><span class=\"num font-primary\">15%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Affiliate</span>\r\n                  <h4 class=\"num m-0\">$<span class=\"counter color-bottom\" [CountTo]=\"2314\" [from]=\"0\"\r\n                      [duration]=\"1\"></span></h4>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-4\">\r\n                <div><span class=\"num font-primary\">34%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Advertise</span>\r\n                  <h4 class=\"num m-0\">$<span class=\"counter color-bottom\" [CountTo]=\"4219\" [from]=\"0\"\r\n                      [duration]=\"1\"></span></h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 col-md-12\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bar-chart-widget\">\r\n          <div class=\"top-content bg-primary\">\r\n            <div class=\"row\">\r\n              <div class=\"col-7\">\r\n                <div class=\"card-body pb-0\">\r\n                  <x-chartist [data]=\"WidgetBarChart2.data\" [type]=\"WidgetBarChart2.type\"\r\n                    [options]=\"WidgetBarChart2.options\">\r\n                  </x-chartist>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-5\">\r\n                <div class=\"earning-details\">\r\n                  <div class=\"text-left\"><span>Total Earning</span>\r\n                    <h4 class=\"mt-1 num mb-0\"><span class=\"counter\" [CountTo]=\"3653\" [from]=\"0\" [duration]=\"1\"></span> M\r\n                    </h4>\r\n                  </div><i class=\"icofont icofont-coins\"></i>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-content card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-4 b-r-light\">\r\n                <div><span class=\"num font-primary\">12%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Year</span>\r\n                  <h4 class=\"num m-0\"><span class=\"counter color-bottom\" [CountTo]=\"3659\" [from]=\"0\"\r\n                      [duration]=\"1\"></span>M</h4>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-4 b-r-light\">\r\n                <div><span class=\"num font-primary\">15%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Month</span>\r\n                  <h4 class=\"num m-0\"><span class=\"counter color-bottom\" [CountTo]=\"698\" [from]=\"0\"\r\n                      [duration]=\"1\"></span>M</h4>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-4\">\r\n                <div><span class=\"num font-primary\">34%<i class=\"icon-angle-up f-12 ml-1\"></i></span><span\r\n                    class=\"text-muted mt-2 mb-2 block-bottom\">Today</span>\r\n                  <h4 class=\"num m-0\"><span class=\"counter color-bottom\" [CountTo]=\"9361\" [from]=\"0\"\r\n                      [duration]=\"1\"></span>M</h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Chart widget with bar chart Ends-->\r\n  <!-- small widgets Start-->\r\n  <div class=\"row\">\r\n    <!-- Live Product chart widget Start-->\r\n    <div class=\"xl-50 col-md-6\">\r\n      <div class=\"small-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Live Products</h5>\r\n          </div>\r\n          <div class=\"card-body bg-primary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"live-products\">\r\n                <x-chartist class=\"live-products\" [data]=\"liveProductChart.data\" [type]=\"liveProductChart.type\"\r\n                  [options]=\"liveProductChart.options\" [events]=\"liveProductChart.events\"></x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Live Product chart widget Ends-->\r\n    <!-- Turnover chart widget Start-->\r\n    <div class=\"xl-50 col-md-6\">\r\n      <div class=\"small-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Turnover</h5>\r\n          </div>\r\n          <div class=\"card-body bg-secondary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"turnover\">\r\n                <x-chartist [data]=\"turnOverChart.data\" [type]=\"turnOverChart.type\" [options]=\"turnOverChart.options\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Turnover chart widget Ends-->\r\n    <!-- Monthly Sale chart widget Start-->\r\n    <div class=\"xl-50 col-md-6\">\r\n      <div class=\"small-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Monthly Sales</h5>\r\n          </div>\r\n          <div class=\"card-body bg-secondary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"monthly\">\r\n                <x-chartist [type]=\"monthlyChart.type\" [data]=\"monthlyChart.data\" [options]=\"monthlyChart.options\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Monthly Sale chart widget Ends-->\r\n    <!-- Uses chart widget Start-->\r\n    <div class=\"xl-50 col-md-6\">\r\n      <div class=\"small-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Uses</h5>\r\n          </div>\r\n          <div class=\"card-body bg-primary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"uses\">\r\n                <x-chartist class=\"uses\" [data]=\"usesChart.data\" [type]=\"usesChart.type\" [options]=\"usesChart.options\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- Uses chart widget Ends-->\r\n  </div>\r\n  <!-- small widgets Ends-->\r\n  <!-- status widget Start-->\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 col-lg-12\">\r\n      <div class=\"status-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <div class=\"row\">\r\n              <div class=\"col-9\">\r\n                <h5>Finance</h5>\r\n              </div>\r\n              <div class=\"col-3 text-sm-right\"><i class=\"text-muted\" data-feather=\"navigation\"></i></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"status-details\">\r\n              <div class=\"row\">\r\n                <div class=\"col-4 text-center\"><span>Investor</span>\r\n                  <h4 class=\"counter mb-0\" [CountTo]=\"3659\" [from]=\"0\" [duration]=\"1\"></h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Money</span>\r\n                  <h4 class=\"mb-0\">$<span class=\"counter\" [CountTo]=\"362\" [from]=\"0\" [duration]=\"1\"></span></h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Earning</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"86\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"status-chart bg-primary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"flot-chart-placeholder\" id=\"finance-graph\">\r\n                <x-chartist [data]=\"financeWidget.data\" [type]=\"financeWidget.type\" [options]=\"financeWidget.options\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 col-lg-12\">\r\n      <div class=\"status-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <div class=\"row\">\r\n              <div class=\"col-9\">\r\n                <h5>Order Status</h5>\r\n              </div>\r\n              <div class=\"col-3 text-sm-right\"><i class=\"text-muted\" data-feather=\"shopping-bag\"></i></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"status-details\">\r\n              <div class=\"row\">\r\n                <div class=\"col-4 text-center\"><span>Complete</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"62\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Pending</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"36\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Cancle</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"20\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"status-chart bg-secondary\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"flot-chart-placeholder\" id=\"order-graph\">\r\n                <x-chartist [data]=\"orderStatusWidget.data\" [type]=\"orderStatusWidget.type\"\r\n                  [options]=\"orderStatusWidget.options\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 col-lg-12\">\r\n      <div class=\"status-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <div class=\"row\">\r\n              <div class=\"col-9\">\r\n                <h5>Skill Status</h5>\r\n              </div>\r\n              <div class=\"col-3 text-sm-right\"><i class=\"text-muted\" data-feather=\"trending-up\"></i></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"status-details\">\r\n              <div class=\"row\">\r\n                <div class=\"col-4 text-center\"><span>Design</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"25\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Market</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"40\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n                <div class=\"col-4 text-center\"><span>Converse</span>\r\n                  <h4 class=\"mb-0\"><span class=\"counter\" [CountTo]=\"35\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"status-chart bg-primary\">\r\n            <div class=\"chart-container\">\r\n              <x-chartist class=\"flot-chart-placeholder\" [data]=\"skillWidget.data\" [type]=\"skillWidget.type\"\r\n                [options]=\"skillWidget.options\">\r\n              </x-chartist>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- status widget Ends-->\r\n  <!-- Browser uses and website visiter widget Start-->\r\n  <div class=\"row\">\r\n    <!-- browser uses widget chart Start-->\r\n    <div class=\"col-sm-6\">\r\n      <div class=\"donut-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Browser Uses</h5>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"chart-container\">\r\n              <ngx-charts-pie-chart [scheme]=\"monthlydoughnutChartColorScheme\" [results]=\"monthlydoughnutData\"\r\n                [explodeSlices]=\"true\" [labels]=\" monthlydoughnutChartShowLabels\" [arcWidth]=0.30 [doughnut]=\"true\"\r\n                [gradient]=\" monthlydoughnutChartGradient\">\r\n              </ngx-charts-pie-chart>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- browser uses widget chart Ends-->\r\n    \r\n    <!-- Website visiter widget Start-->\r\n    <div class=\"col-sm-6\">\r\n      <div class=\"donut-chart-widget\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Website Visiter</h5>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"chart-container\">\r\n              <div class=\"flot-chart-placeholder\" id=\"website-visiter-chart\">\r\n                <ngx-charts-pie-chart [scheme]=\"dailydoughnutChartColorScheme\" [results]=\"dailydoughnutData\"\r\n                  [explodeSlices]=\"true\" [labels]=\"dailydoughnutChartShowLabels\" [arcWidth]=0.30 [doughnut]=\"true\"\r\n                  [gradient]=\"dailydoughnutChartGradient\">\r\n                </ngx-charts-pie-chart>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/widgets/general/general.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/widgets/general/general.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-6 col-xl-3 col-lg-6\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bg-primary b-r-4 card-body\">\r\n          <div class=\"media static-top-widget\">\r\n            <div class=\"align-self-center text-center\">\r\n              <app-feather-icons [icon]=\"'navigation'\"></app-feather-icons>\r\n            </div>\r\n            <div class=\"media-body\"><span class=\"m-0\">Earnings</span>\r\n              <h4 class=\"mb-0 counter\" [CountTo]=\"6659\" [from]=\"0\" [duration]=\"1\"></h4><i class=\"icon-bg\"\r\n                data-feather=\"navigation\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 col-lg-6\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bg-secondary b-r-4 card-body\">\r\n          <div class=\"media static-top-widget\">\r\n            <div class=\"align-self-center text-center\">\r\n              <app-feather-icons [icon]=\"'box'\"></app-feather-icons>\r\n            </div>\r\n            <div class=\"media-body\"><span class=\"m-0\">Products</span>\r\n              <h4 class=\"mb-0 counter\" [CountTo]=\"9856\" [from]=\"0\" [duration]=\"1\">9856</h4><i class=\"icon-bg\"\r\n                data-feather=\"box\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 col-lg-6\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bg-primary b-r-4 card-body\">\r\n          <div class=\"media static-top-widget\">\r\n            <div class=\"align-self-center text-center\">\r\n              <app-feather-icons [icon]=\"'message-square'\"></app-feather-icons>\r\n            </div>\r\n            <div class=\"media-body\"><span class=\"m-0\">Messages</span>\r\n              <h4 class=\"mb-0 counter\" [CountTo]=\"893\" [from]=\"0\" [duration]=\"1\">893</h4><i class=\"icon-bg\"\r\n                data-feather=\"message-square\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 col-lg-6\">\r\n      <div class=\"card o-hidden\">\r\n        <div class=\"bg-primary b-r-4 card-body\">\r\n          <div class=\"media static-top-widget\">\r\n            <div class=\"align-self-center text-center\">\r\n              <app-feather-icons [icon]=\"'users'\"></app-feather-icons>\r\n            </div>\r\n            <div class=\"media-body\"><span class=\"m-0\">New User</span>\r\n              <h4 class=\"mb-0 counter\" [CountTo]=\"45631\" [from]=\"0\" [duration]=\"1\">45631</h4><i class=\"icon-bg\"\r\n                data-feather=\"users\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"widget-joins card\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-6 pr-0\">\r\n            <div class=\"media border-after-xs\">\r\n              <div class=\"align-self-center mr-3\">68%<i class=\"fa fa-angle-up ml-2\"></i></div>\r\n              <div class=\"media-body details pl-3\"><span class=\"mb-1\">New</span>\r\n                <h4 class=\"mb-0 counter digits\" [CountTo]=\"6982\" [from]=\"0\" [duration]=\"1\">6982</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary float-left ml-2\"\r\n                  data-feather=\"shopping-bag\"></i></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pl-0\">\r\n            <div class=\"media\">\r\n              <div class=\"align-self-center mr-3 digits\">12%<i class=\"fa fa-angle-down ml-2\"></i></div>\r\n              <div class=\"media-body details pl-3\"><span class=\"mb-1\">Pending</span>\r\n                <h4 class=\"mb-0 counter digits\" [CountTo]=\"783\" [from]=\"0\" [duration]=\"1\">783</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary float-left ml-3\"\r\n                  data-feather=\"layers\"></i>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pr-0\">\r\n            <div class=\"media border-after-xs\">\r\n              <div class=\"align-self-center mr-3\">68%<i class=\"fa fa-angle-up ml-2\"></i></div>\r\n              <div class=\"media-body details pl-3 pt-0\"><span class=\"mb-1\">Done</span>\r\n                <h4 class=\"mb-0 counter digits\" [CountTo]=\"3674\" [from]=\"0\" [duration]=\"1\">3674</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary float-left ml-2\"\r\n                  data-feather=\"shopping-cart\"></i></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pl-0\">\r\n            <div class=\"media\">\r\n              <div class=\"align-self-center mr-3\">68%<i class=\"fa fa-angle-up ml-2\"></i></div>\r\n              <div class=\"media-body details pl-3 pt-0\"><span class=\"mb-1\">Cancel</span>\r\n                <h4 class=\"mb-0 counter digits\" [CountTo]=\"69\" [from]=\"0\" [duration]=\"1\"></h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary float-left ml-2\"\r\n                  data-feather=\"dollar-sign\"></i></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"widget-joins card widget-arrow\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-6 pr-0\">\r\n            <div class=\"media border-after-xs\">\r\n              <div class=\"align-self-center mr-3 text-left\">\r\n                <h6 class=\"mb-1\">Sale</h6>\r\n                <h4 class=\"mb-0\">Today</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary\" data-feather=\"arrow-down\"></i></div>\r\n              <div class=\"media-body\">\r\n                <h5 class=\"mb-0\">$<span class=\"counter\" [CountTo]=\"25698\" [from]=\"0\" [duration]=\"1\"></span></h5><span\r\n                  class=\"mb-1\">-$2658(36%)</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pl-0\">\r\n            <div class=\"media\">\r\n              <div class=\"align-self-center mr-3 text-left\">\r\n                <h6 class=\"mb-1\">Sale</h6>\r\n                <h4 class=\"mb-0\">Month</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary\" data-feather=\"arrow-up\"></i></div>\r\n              <div class=\"media-body pl-2\">\r\n                <h5 class=\"mb-0\">$<span class=\"counter\" [CountTo]=\"6954\" [from]=\"0\" [duration]=\"1\"></span></h5><span\r\n                  class=\"mb-1\">+$369(15%)</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pr-0\">\r\n            <div class=\"media border-after-xs\">\r\n              <div class=\"align-self-center mr-3 text-left\">\r\n                <h6 class=\"mb-1\">Sale</h6>\r\n                <h4 class=\"mb-0\">Week</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center\"><i class=\"font-primary\" data-feather=\"arrow-up\"></i></div>\r\n              <div class=\"media-body\">\r\n                <h5 class=\"mb-0\">$<span class=\"counter\" [CountTo]=\"63147\" [from]=\"0\" [duration]=\"1\"></span></h5><span\r\n                  class=\"mb-1\">+$69(65%)</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-6 pl-0\">\r\n            <div class=\"media\">\r\n              <div class=\"align-self-center mr-3 text-left\">\r\n                <h6 class=\"mb-1\">Sale</h6>\r\n                <h4 class=\"mb-0\">Year</h4>\r\n              </div>\r\n              <div class=\"media-body align-self-center pl-3\"><i class=\"font-primary\" data-feather=\"arrow-up\"></i></div>\r\n              <div class=\"media-body pl-2\">\r\n                <h5 class=\"mb-0\">$<span class=\"counter\" [CountTo]=\"963198\" [from]=\"0\" [duration]=\"1\"></span></h5><span\r\n                  class=\"mb-1\">+$3654(90%)</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"cal-date-widget card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xl-6 col-xs-12 col-md-6 col-sm-6\">\r\n              <div class=\"cal-info text-center\">\r\n                <h2>24</h2>\r\n                <div class=\"d-inline-block mt-2\"><span class=\"b-r-dark pr-3\">March</span><span class=\"pl-3\">2018</span>\r\n                </div>\r\n                <p class=\"mt-4 f-16 text-muted\">There is no minimum donation, any sum is appreciated</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-6 col-xs-12 col-md-6 col-sm-6\">\r\n              <div class=\"cal-datepicker custom-datepicker pull-right\">\r\n                <div class=\"datepicker-here\" data-language=\"en\">\r\n                  <ngb-datepicker [(ngModel)]=\"model\" #dp (navigate)=\"date = $event.next\"></ngb-datepicker>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"weather-widget-two\">\r\n          <div class=\"card-body\">\r\n            <div class=\"media\">\r\n              <svg class=\"climacon climacon_cloudDrizzleMoonAlt\" id=\"cloudDrizzleMoonAlt\" version=\"1.1\"\r\n                viewBox=\"15 15 70 70\">\r\n                <clippath id=\"cloudFillClip\">\r\n                  <path\r\n                    d=\"M15,15v70h70V15H15z M59.943,61.639c-3.02,0-12.381,0-15.999,0c-6.626,0-11.998-5.371-11.998-11.998c0-6.627,5.372-11.999,11.998-11.999c5.691,0,10.434,3.974,11.665,9.29c1.252-0.81,2.733-1.291,4.334-1.291c4.418,0,8,3.582,8,8C67.943,58.057,64.361,61.639,59.943,61.639z\">\r\n                  </path>\r\n                </clippath>\r\n                <clippath id=\"moonCloudFillClip\">\r\n                  <path\r\n                    d=\"M0,0v100h100V0H0z M60.943,46.641c-4.418,0-7.999-3.582-7.999-7.999c0-3.803,2.655-6.979,6.211-7.792c0.903,4.854,4.726,8.676,9.579,9.58C67.922,43.986,64.745,46.641,60.943,46.641z\">\r\n                  </path>\r\n                </clippath>\r\n                <g class=\"climacon_iconWrap climacon_iconWrap-cloudDrizzleMoonAlt\">\r\n                  <g clip-path=\"url(#cloudFillClip)\">\r\n                    <g class=\"climacon_wrapperComponent climacon_wrapperComponent-moon climacon_componentWrap-moon_cloud\"\r\n                      clip-path=\"url(#moonCloudFillClip)\">\r\n                      <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_sunBody\"\r\n                        d=\"M61.023,50.641c-6.627,0-11.999-5.372-11.999-11.998c0-6.627,5.372-11.999,11.999-11.999c0.755,0,1.491,0.078,2.207,0.212c-0.132,0.576-0.208,1.173-0.208,1.788c0,4.418,3.582,7.999,8,7.999c0.614,0,1.212-0.076,1.788-0.208c0.133,0.717,0.211,1.452,0.211,2.208C73.021,45.269,67.649,50.641,61.023,50.641z\">\r\n                      </path>\r\n                    </g>\r\n                  </g>\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-drizzle\">\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-left\"\r\n                      id=\"Drizzle-Left_1_\"\r\n                      d=\"M56.969,57.672l-2.121,2.121c-1.172,1.172-1.172,3.072,0,4.242c1.17,1.172,3.07,1.172,4.24,0c1.172-1.17,1.172-3.07,0-4.242L56.969,57.672z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-middle\"\r\n                      d=\"M50.088,57.672l-2.119,2.121c-1.174,1.172-1.174,3.07,0,4.242c1.17,1.172,3.068,1.172,4.24,0s1.172-3.07,0-4.242L50.088,57.672z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-right\"\r\n                      d=\"M43.033,57.672l-2.121,2.121c-1.172,1.172-1.172,3.07,0,4.242s3.07,1.172,4.244,0c1.172-1.172,1.172-3.07,0-4.242L43.033,57.672z\">\r\n                    </path>\r\n                  </g>\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-cloud\" clip-path=\"url(#cloudFillClip)\">\r\n                    <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_cloud\"\r\n                      d=\"M59.943,41.642c-0.696,0-1.369,0.092-2.033,0.205c-2.736-4.892-7.961-8.203-13.965-8.203c-8.835,0-15.998,7.162-15.998,15.997c0,5.992,3.3,11.207,8.177,13.947c0.276-1.262,0.892-2.465,1.873-3.445l0.057-0.057c-3.644-2.061-6.106-5.963-6.106-10.445c0-6.626,5.372-11.998,11.998-11.998c5.691,0,10.433,3.974,11.666,9.29c1.25-0.81,2.732-1.291,4.332-1.291c4.418,0,8,3.581,8,7.999c0,3.443-2.182,6.371-5.235,7.498c0.788,1.146,1.194,2.471,1.222,3.807c4.666-1.645,8.014-6.077,8.014-11.305C71.941,47.014,66.57,41.642,59.943,41.642z\">\r\n                    </path>\r\n                  </g>\r\n                </g>\r\n              </svg>\r\n              <!-- cloudDrizzleMoonAlt-->\r\n              <div class=\"media-body align-self-center text-white\">\r\n                <h4 class=\"m-0 f-w-600 num\">25°C</h4>\r\n                <p class=\"m-0 f-14\">Newyork</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <svg class=\"climacon climacon_cloudRainMoon\" id=\"cloudRainMoon\" version=\"1.1\" viewBox=\"15 15 70 70\">\r\n                <clippath id=\"cloudFillClip1\">\r\n                  <path\r\n                    d=\"M15,15v70h70V15H15z M59.943,61.639c-3.02,0-12.381,0-15.999,0c-6.626,0-11.998-5.371-11.998-11.998c0-6.627,5.372-11.999,11.998-11.999c5.691,0,10.434,3.974,11.665,9.29c1.252-0.81,2.733-1.291,4.334-1.291c4.418,0,8,3.582,8,8C67.943,58.057,64.361,61.639,59.943,61.639z\">\r\n                  </path>\r\n                </clippath>\r\n                <clippath id=\"moonCloudFillClip1\">\r\n                  <path\r\n                    d=\"M0,0v100h100V0H0z M60.943,46.641c-4.418,0-7.999-3.582-7.999-7.999c0-3.803,2.655-6.979,6.211-7.792c0.903,4.854,4.726,8.676,9.579,9.58C67.922,43.986,64.745,46.641,60.943,46.641z\">\r\n                  </path>\r\n                </clippath>\r\n                <g class=\"climacon_iconWrap climacon_iconWrap-cloudRainMoon\">\r\n                  <g clip-path=\"url(#cloudFillClip1)\">\r\n                    <g class=\"climacon_wrapperComponent climacon_wrapperComponent-moon climacon_componentWrap-moon_cloud\"\r\n                      clip-path=\"url(#moonCloudFillClip1)\">\r\n                      <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_sunBody\"\r\n                        d=\"M61.023,50.641c-6.627,0-11.999-5.372-11.999-11.998c0-6.627,5.372-11.999,11.999-11.999c0.755,0,1.491,0.078,2.207,0.212c-0.132,0.576-0.208,1.173-0.208,1.788c0,4.418,3.582,7.999,8,7.999c0.614,0,1.212-0.076,1.788-0.208c0.133,0.717,0.211,1.452,0.211,2.208C73.021,45.269,67.649,50.641,61.023,50.641z\">\r\n                      </path>\r\n                    </g>\r\n                  </g>\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-rain\">\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- left\"\r\n                      d=\"M41.946,53.641c1.104,0,1.999,0.896,1.999,2v15.998c0,1.105-0.895,2-1.999,2s-2-0.895-2-2V55.641C39.946,54.537,40.842,53.641,41.946,53.641z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- middle\"\r\n                      d=\"M49.945,57.641c1.104,0,2,0.896,2,2v15.998c0,1.104-0.896,2-2,2s-2-0.896-2-2V59.641C47.945,58.535,48.841,57.641,49.945,57.641z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- right\"\r\n                      d=\"M57.943,53.641c1.104,0,2,0.896,2,2v15.998c0,1.105-0.896,2-2,2c-1.104,0-2-0.895-2-2V55.641C55.943,54.537,56.84,53.641,57.943,53.641z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- left\"\r\n                      d=\"M41.946,53.641c1.104,0,1.999,0.896,1.999,2v15.998c0,1.105-0.895,2-1.999,2s-2-0.895-2-2V55.641C39.946,54.537,40.842,53.641,41.946,53.641z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- middle\"\r\n                      d=\"M49.945,57.641c1.104,0,2,0.896,2,2v15.998c0,1.104-0.896,2-2,2s-2-0.896-2-2V59.641C47.945,58.535,48.841,57.641,49.945,57.641z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_rain climacon_component-stroke_rain- right\"\r\n                      d=\"M57.943,53.641c1.104,0,2,0.896,2,2v15.998c0,1.105-0.896,2-2,2c-1.104,0-2-0.895-2-2V55.641C55.943,54.537,56.84,53.641,57.943,53.641z\">\r\n                    </path>\r\n                  </g>\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-cloud\" clip-path=\"url(#cloudFillClip1)\">\r\n                    <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_cloud\"\r\n                      d=\"M59.943,41.642c-0.696,0-1.369,0.092-2.033,0.205c-2.736-4.892-7.961-8.203-13.965-8.203c-8.835,0-15.998,7.162-15.998,15.997c0,5.992,3.3,11.207,8.177,13.947c0.276-1.262,0.892-2.465,1.873-3.445l0.057-0.057c-3.644-2.061-6.106-5.963-6.106-10.445c0-6.626,5.372-11.998,11.998-11.998c5.691,0,10.433,3.974,11.666,9.29c1.25-0.81,2.732-1.291,4.332-1.291c4.418,0,8,3.581,8,7.999c0,3.443-2.182,6.371-5.235,7.498c0.788,1.146,1.194,2.471,1.222,3.807c4.666-1.645,8.014-6.077,8.014-11.305C71.941,47.014,66.57,41.642,59.943,41.642z\">\r\n                    </path>\r\n                  </g>\r\n                </g>\r\n              </svg>\r\n              <!-- cloudRainMoon-->\r\n              <div class=\"media-body align-self-center text-white\">\r\n                <h4 class=\"m-0 f-w-600 num\">95°F</h4>\r\n                <p class=\"m-0 f-14\">Peris</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <svg class=\"climacon climacon_cloudDrizzle\" id=\"cloudDrizzle\" version=\"1.1\" viewBox=\"15 15 70 70\">\r\n                <g class=\"climacon_iconWrap climacon_iconWrap-cloudDrizzle\">\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-drizzle\">\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-left\"\r\n                      d=\"M42.001,53.644c1.104,0,2,0.896,2,2v3.998c0,1.105-0.896,2-2,2c-1.105,0-2.001-0.895-2.001-2v-3.998C40,54.538,40.896,53.644,42.001,53.644z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-middle\"\r\n                      d=\"M49.999,53.644c1.104,0,2,0.896,2,2v4c0,1.104-0.896,2-2,2s-1.998-0.896-1.998-2v-4C48.001,54.54,48.896,53.644,49.999,53.644z\">\r\n                    </path>\r\n                    <path\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_drizzle climacon_component-stroke_drizzle-right\"\r\n                      d=\"M57.999,53.644c1.104,0,2,0.896,2,2v3.998c0,1.105-0.896,2-2,2c-1.105,0-2-0.895-2-2v-3.998C55.999,54.538,56.894,53.644,57.999,53.644z\">\r\n                    </path>\r\n                  </g>\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-cloud\">\r\n                    <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_cloud\"\r\n                      d=\"M63.999,64.944v-4.381c2.387-1.386,3.998-3.961,3.998-6.92c0-4.418-3.58-8-7.998-8c-1.603,0-3.084,0.481-4.334,1.291c-1.232-5.316-5.973-9.29-11.664-9.29c-6.628,0-11.999,5.372-11.999,12c0,3.549,1.55,6.729,3.998,8.926v4.914c-4.776-2.769-7.998-7.922-7.998-13.84c0-8.836,7.162-15.999,15.999-15.999c6.004,0,11.229,3.312,13.965,8.203c0.664-0.113,1.336-0.205,2.033-0.205c6.627,0,11.998,5.373,11.998,12C71.997,58.864,68.655,63.296,63.999,64.944z\">\r\n                    </path>\r\n                  </g>\r\n                </g>\r\n              </svg>\r\n              <!-- cloudDrizzle-->\r\n              <div class=\"media-body align-self-center text-white\">\r\n                <h4 class=\"m-0 f-w-600 num\">50°C</h4>\r\n                <p class=\"m-0 f-14\">India</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"top-bg-whether\">\r\n              <svg class=\"climacon climacon_cloudHailAltFill\" id=\"cloudHailAltFill\" version=\"1.1\" viewBox=\"15 15 70 70\">\r\n                <g class=\"climacon_iconWrap climacon_iconWrap-cloudHailAltFill\">\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-hailAlt\">\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-left\">\r\n                      <circle cx=\"42\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-middle\">\r\n                      <circle cx=\"49.999\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-right\">\r\n                      <circle cx=\"57.998\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-left\">\r\n                      <circle cx=\"42\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-middle\">\r\n                      <circle cx=\"49.999\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                    <g\r\n                      class=\"climacon_component climacon_component-stroke climacon_component-stroke_hailAlt climacon_component-stroke_hailAlt-right\">\r\n                      <circle cx=\"57.998\" cy=\"65.498\" r=\"2\"></circle>\r\n                    </g>\r\n                  </g>\r\n                  <g class=\"climacon_componentWrap climacon_componentWrap_cloud\">\r\n                    <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_cloud\"\r\n                      d=\"M43.945,65.639c-8.835,0-15.998-7.162-15.998-15.998c0-8.836,7.163-15.998,15.998-15.998c6.004,0,11.229,3.312,13.965,8.203c0.664-0.113,1.338-0.205,2.033-0.205c6.627,0,11.998,5.373,11.998,12c0,6.625-5.371,11.998-11.998,11.998C57.168,65.639,47.143,65.639,43.945,65.639z\">\r\n                    </path>\r\n                    <path class=\"climacon_component climacon_component-fill climacon_component-fill_cloud\"\r\n                      fill=\"#FFFFFF\"\r\n                      d=\"M59.943,61.639c4.418,0,8-3.582,8-7.998c0-4.417-3.582-8-8-8c-1.601,0-3.082,0.481-4.334,1.291c-1.23-5.316-5.973-9.29-11.665-9.29c-6.626,0-11.998,5.372-11.998,11.999c0,6.626,5.372,11.998,11.998,11.998C47.562,61.639,56.924,61.639,59.943,61.639z\">\r\n                    </path>\r\n                  </g>\r\n                </g>\r\n              </svg>\r\n            </div>\r\n            <div class=\"bottom-whetherinfo\">\r\n              <div class=\"row\">\r\n                <div class=\"col-6\">\r\n                  <app-feather-icons [icon]=\"'cloud-drizzle'\"></app-feather-icons>\r\n                </div>\r\n                <div class=\"col-6\">\r\n                  <div class=\"whether-content\"><span>India, Surat</span>\r\n                    <h4 class=\"num mb-0\">36°F</h4>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"mobile-clock-widget\">\r\n          <div class=\"bg-svg\">\r\n            <svg class=\"climacon climacon_cloudLightningMoon\" id=\"cloudLightningMoon\" version=\"1.1\"\r\n              viewBox=\"15 15 70 70\">\r\n              <clippath id=\"moonCloudFillClipfill11\">\r\n                <path\r\n                  d=\"M0,0v100h100V0H0z M60.943,46.641c-4.418,0-7.999-3.582-7.999-7.999c0-3.803,2.655-6.979,6.211-7.792c0.903,4.854,4.726,8.676,9.579,9.58C67.922,43.986,64.745,46.641,60.943,46.641z\">\r\n                </path>\r\n              </clippath>\r\n              <clippath id=\"cloudFillClipfill19\">\r\n                <path\r\n                  d=\"M15,15v70h70V15H15z M59.943,61.639c-3.02,0-12.381,0-15.999,0c-6.626,0-11.998-5.371-11.998-11.998c0-6.627,5.372-11.999,11.998-11.999c5.691,0,10.434,3.974,11.665,9.29c1.252-0.81,2.733-1.291,4.334-1.291c4.418,0,8,3.582,8,8C67.943,58.057,64.361,61.639,59.943,61.639z\">\r\n                </path>\r\n              </clippath>\r\n              <g class=\"climacon_iconWrap climacon_iconWrap-cloudLightningMoon\">\r\n                <g clip-path=\"url(#cloudFillClip)\">\r\n                  <g class=\"climacon_wrapperComponent climacon_wrapperComponent-moon climacon_componentWrap-moon_cloud\"\r\n                    clip-path=\"url(#moonCloudFillClip)\">\r\n                    <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_sunBody\"\r\n                      d=\"M61.023,50.641c-6.627,0-11.999-5.372-11.999-11.998c0-6.627,5.372-11.999,11.999-11.999c0.755,0,1.491,0.078,2.207,0.212c-0.132,0.576-0.208,1.173-0.208,1.788c0,4.418,3.582,7.999,8,7.999c0.614,0,1.212-0.076,1.788-0.208c0.133,0.717,0.211,1.452,0.211,2.208C73.021,45.269,67.649,50.641,61.023,50.641z\">\r\n                    </path>\r\n                  </g>\r\n                </g>\r\n                <g class=\"climacon_wrapperComponent climacon_wrapperComponent-lightning\">\r\n                  <polygon class=\"climacon_component climacon_component-stroke climacon_component-stroke_lightning\"\r\n                    points=\"48.001,51.641 57.999,51.641 52,61.641 58.999,61.641 46.001,77.639 49.601,65.641 43.001,65.641 \">\r\n                  </polygon>\r\n                </g>\r\n                <g class=\"climacon_wrapperComponent climacon_wrapperComponent-cloud\">\r\n                  <path class=\"climacon_component climacon_component-stroke climacon_component-stroke_cloud\"\r\n                    d=\"M59.999,65.641c-0.28,0-0.649,0-1.062,0l3.584-4.412c3.182-1.057,5.478-4.053,5.478-7.588c0-4.417-3.581-7.998-7.999-7.998c-1.602,0-3.083,0.48-4.333,1.29c-1.231-5.316-5.974-9.29-11.665-9.29c-6.626,0-11.998,5.372-11.998,12c0,5.446,3.632,10.039,8.604,11.503l-1.349,3.777c-6.52-2.021-11.255-8.098-11.255-15.282c0-8.835,7.163-15.999,15.998-15.999c6.004,0,11.229,3.312,13.965,8.204c0.664-0.114,1.338-0.205,2.033-0.205c6.627,0,11.999,5.371,11.999,11.999C71.999,60.268,66.626,65.641,59.999,65.641z\">\r\n                  </path>\r\n                </g>\r\n              </g>\r\n            </svg>\r\n          </div>\r\n          <div>\r\n            <ul class=\"clock\" id=\"clock\">\r\n              <li class=\"hour\" id=\"hour\"></li>\r\n              <li class=\"min\" id=\"min\"></li>\r\n              <li class=\"sec\" id=\"sec\"></li>\r\n            </ul>\r\n            <div class=\"date f-24 mb-2\" id=\"date\"> {{today | date}}</div>\r\n            <div>\r\n              <p class=\"m-0 f-14 text-light\">kolkata, India</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 xl-50 col-lg-6\">\r\n      <div class=\"card social-widget-card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"redial-social-widget radial-bar-70\" data-label=\"50%\"><i class=\"fa fa-facebook font-primary\"></i>\r\n          </div>\r\n          <h5 class=\"b-b-light\">Facebook</h5>\r\n          <div class=\"row\">\r\n            <div class=\"col text-center b-r-light\"><span>Post</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"6589\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n            <div class=\"col text-center\"><span>Like</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"75269\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 xl-50 col-lg-6\">\r\n      <div class=\"card social-widget-card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"redial-social-widget radial-bar-70\" data-label=\"50%\"><i class=\"fa fa-twitter font-primary\"></i>\r\n          </div>\r\n          <h5 class=\"b-b-light\">Twitter</h5>\r\n          <div class=\"row\">\r\n            <div class=\"col text-center b-r-light\"><span>Post</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"6589\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n            <div class=\"col text-center\"><span>Follower</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"75269\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 xl-50 col-lg-6\">\r\n      <div class=\"card social-widget-card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"redial-social-widget radial-bar-70\" data-label=\"50%\"><i class=\"fa fa-linkedin font-primary\"></i>\r\n          </div>\r\n          <h5 class=\"b-b-light\">linkedin</h5>\r\n          <div class=\"row\">\r\n            <div class=\"col text-center b-r-light\"><span>Post</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"1234\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n            <div class=\"col text-center\"><span>linkedin</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"4369\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-6 col-xl-3 xl-50 col-lg-6\">\r\n      <div class=\"card social-widget-card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"redial-social-widget radial-bar-70\" data-label=\"50%\"><i\r\n              class=\"fa fa-google-plus font-primary\"></i></div>\r\n          <h5 class=\"b-b-light\">Google +</h5>\r\n          <div class=\"row\">\r\n            <div class=\"col text-center b-r-light\"><span>Post</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"369\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n            <div class=\"col text-center\"><span>Follower</span>\r\n              <h4 class=\"counter mb-0\" [CountTo]=\"3458\" [from]=\"0\" [duration]=\"1\"></h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <div class=\"card browser-widget\">\r\n        <div class=\"media card-body\">\r\n          <div class=\"media-img\"><img src=\"assets/images/dashboard/chrome.png\" alt=\"\"></div>\r\n          <div class=\"media-body align-self-center\">\r\n            <div>\r\n              <p>Daily </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"36\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Month </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"96\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Week </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"46\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <div class=\"card browser-widget\">\r\n        <div class=\"media card-body\">\r\n          <div class=\"media-img\"><img src=\"assets/images/dashboard/firefox.png\" alt=\"\"></div>\r\n          <div class=\"media-body align-self-center\">\r\n            <div>\r\n              <p>Daily </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"36\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Month </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"96\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Week </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"46\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <div class=\"card browser-widget\">\r\n        <div class=\"media card-body\">\r\n          <div class=\"media-img\"><img src=\"assets/images/dashboard/safari.png\" alt=\"\"></div>\r\n          <div class=\"media-body align-self-center\">\r\n            <div>\r\n              <p>Daily </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"36\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Month </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"96\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n            <div>\r\n              <p>Week </p>\r\n              <h4><span class=\"counter\" [CountTo]=\"46\" [from]=\"0\" [duration]=\"1\"></span>%</h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>PRODUCTS CART</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"user-status table-responsive\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Details</th>\r\n                  <th scope=\"col\">Quantity</th>\r\n                  <th scope=\"col\">Status</th>\r\n                  <th scope=\"col\">Price</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td>Simply dummy text of the printing</td>\r\n                  <td class=\"digits\">1</td>\r\n                  <td class=\"font-primary\">Pending</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>Long established</td>\r\n                  <td class=\"digits\">5</td>\r\n                  <td class=\"font-secondary\">Cancle</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>sometimes by accident</td>\r\n                  <td class=\"digits\">10</td>\r\n                  <td class=\"font-secondary\">Cancle</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>Classical Latin literature</td>\r\n                  <td class=\"digits\">9</td>\r\n                  <td class=\"font-primary\">Return</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>keep the site on the Internet</td>\r\n                  <td class=\"digits\">8</td>\r\n                  <td class=\"font-primary\">Pending</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>Molestiae consequatur</td>\r\n                  <td class=\"digits\">3</td>\r\n                  <td class=\"font-secondary\">Cancle</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n                <tr>\r\n                  <td>Pain can procure</td>\r\n                  <td class=\"digits\">8</td>\r\n                  <td class=\"font-primary\">Return</td>\r\n                  <td class=\"digits\">$6523</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>EMPLOYEE STATUS</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"user-status table-responsive\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Name</th>\r\n                  <th scope=\"col\">Designation</th>\r\n                  <th scope=\"col\">Skill Level</th>\r\n                  <th scope=\"col\">Experience</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td class=\"bd-t-none u-s-tb\">\r\n                    <div class=\"align-middle image-sm-size\"><img class=\"img-radius align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <h6>John Deo <span class=\"text-muted digits\">(14+ Online)</span></h6>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Designer</td>\r\n                  <td>\r\n                    <div class=\"progress-showcase\">\r\n                      <div class=\"progress\" style=\"height: 8px;\">\r\n                        <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 30%\" aria-valuenow=\"50\"\r\n                          aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"digits\">2 Year</td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"bd-t-none u-s-tb\">\r\n                    <div class=\"align-middle image-sm-size\"><img class=\"img-radius align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/1.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <h6>Holio Mako <span class=\"text-muted digits\">(250+ Online)</span></h6>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Developer</td>\r\n                  <td>\r\n                    <div class=\"progress-showcase\">\r\n                      <div class=\"progress\" style=\"height: 8px;\">\r\n                        <div class=\"progress-bar bg-secondary\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"50\"\r\n                          aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"digits\">3 Year</td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"bd-t-none u-s-tb\">\r\n                    <div class=\"align-middle image-sm-size\"><img class=\"img-radius align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/5.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <h6>Mohsib lara<span class=\"text-muted digits\">(99+ Online)</span></h6>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Tester</td>\r\n                  <td>\r\n                    <div class=\"progress-showcase\">\r\n                      <div class=\"progress\" style=\"height: 8px;\">\r\n                        <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 60%\" aria-valuenow=\"50\"\r\n                          aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"digits\">5 Month</td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"bd-t-none u-s-tb\">\r\n                    <div class=\"align-middle image-sm-size\"><img class=\"img-radius align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/6.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <h6>Hileri Soli <span class=\"text-muted digits\">(150+ Online)</span></h6>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Designer</td>\r\n                  <td>\r\n                    <div class=\"progress-showcase\">\r\n                      <div class=\"progress\" style=\"height: 8px;\">\r\n                        <div class=\"progress-bar bg-secondary\" role=\"progressbar\" style=\"width: 30%\" aria-valuenow=\"50\"\r\n                          aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"digits\">3 Month</td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"bd-t-none u-s-tb\">\r\n                    <div class=\"align-middle image-sm-size\"><img class=\"img-radius align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/7.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <h6>Pusiz bia <span class=\"text-muted digits\">(14+ Online)</span></h6>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>Designer</td>\r\n                  <td>\r\n                    <div class=\"progress-showcase\">\r\n                      <div class=\"progress\" style=\"height: 8px;\">\r\n                        <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 90%\" aria-valuenow=\"50\"\r\n                          aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"digits\">5 Year</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-lg-6 col-xl-4 xl-50 col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"calender-widget\">\r\n          <div class=\"cal-img\"></div>\r\n          <div class=\"cal-date\">\r\n            <h5>25<br>APRIL</h5>\r\n          </div>\r\n          <div class=\"cal-desc text-center card-body\">\r\n            <h6 class=\"f-w-600\">I must explain to you how all this mistaken idea truth</h6>\r\n            <p class=\"text-muted mt-3 mb-0\">Denouncing pleasure and praising pain was born and I will give you a\r\n              complete account of the system, and expound the actual teachings of the great explorer of the\r\n              truth,Letraset sheets containing Lorem Ipsum passages, and more recently.</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12 col-lg-6 col-xl-8 xl-50 col-md-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Contact Us</h5>\r\n\r\n        </div>\r\n        <div class=\"contact-form card-body\">\r\n          <form class=\"theme-form\">\r\n            <div class=\"form-icon\"><i class=\"icofont icofont-envelope-open\"></i></div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputName\">Your Name</label>\r\n              <input class=\"form-control\" id=\"exampleInputName\" type=\"text\" placeholder=\"John Dio\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"col-form-label\" for=\"exampleInputEmail1\">Email</label>\r\n              <input class=\"form-control\" id=\"exampleInputEmail1\" type=\"email\" placeholder=\"Demo@gmail.com\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"col-form-label\" for=\"exampleInputEmail1\">Message</label>\r\n              <textarea class=\"form-control textarea\" rows=\"3\" cols=\"50\" placeholder=\"Your Message\"></textarea>\r\n            </div>\r\n            <div class=\"text-sm-right\">\r\n              <button class=\"btn btn-primary-gradien\">SEND IT</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 col-lg-12\">\r\n      <div>\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5 class=\"text-uppercase\">Recent Activity</h5>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <ul class=\"crm-activity\">\r\n              <li class=\"media\"><span class=\"mr-3 font-primary\">E</span>\r\n                <div class=\"align-self-center media-body\">\r\n                  <h6 class=\"mt-0\">Established fact that a reader will be distracted </h6>\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n              <li class=\"media\"><span class=\"mr-3 font-secondary\">A</span>\r\n                <div class=\"align-self-center media-body\">\r\n                  <h6 class=\"mt-0\">Any desktop publishing packages and web page editors.</h6>\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n              <li class=\"media\"><span class=\"mr-3 font-primary\">T</span>\r\n                <div class=\"align-self-center media-body\">\r\n                  <h6 class=\"mt-0\">There isn't anything embarrassing hidden.</h6>\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n              <li class=\"media\"><span class=\"mr-3 font-secondary\">C</span>\r\n                <div class=\"align-self-center media-body\">\r\n                  <h6 class=\"mt-0\">Contrary to popular belief, Lorem Ipsum is not simply. </h6>\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n              <li class=\"media\"><span class=\"mr-3 font-primary\">H</span>\r\n                <div class=\"align-self-center media-body\">\r\n                  <h6 class=\"mt-0\">H-Code - A premium portfolio template from ThemeZaa </h6>\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n              <li class=\"media\">\r\n                <div class=\"align-self-center media-body\">\r\n                  <ul class=\"dates\">\r\n                    <li class=\"digits\">25 July 2017</li>\r\n                    <li class=\"digits\">20 hours ago</li>\r\n                  </ul>\r\n                </div>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/3.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/3.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a  href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a  href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a  href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a  href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a  href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Mark Jecno</h4>\r\n          <h6>Manager</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\" [CountTo]=\"9564\" [from]=\"0\" [duration]=\"1\">9564</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\" [CountTo]=\"49\" [from]=\"0\" [duration]=\"1\">49</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\" [CountTo]=\"96\" [from]=\"0\" [duration]=\"1\">96</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4\">\r\n      <div class=\"card testimonial text-center\">\r\n        <div class=\"card-body\">\r\n          <owl-carousel-o [options]=\"owlcarouselOptions\">\r\n            <ng-container *ngFor=\"let data of owlcarousel\">\r\n              <ng-template carouselSlide class=\"item\">\r\n                <i class=\"icon-quote-left\"></i>\r\n                <p>{{data.desc}}</p><img class=\"img-80\" [src]=\"data.img\" alt=\"\">\r\n                <h5 class=\"font-primary\">{{data.name}}</h5><span>{{data.designation}}</span>\r\n              </ng-template>\r\n            </ng-container>\r\n          </owl-carousel-o>\r\n        </div>\r\n      </div>\r\n      <div class=\"card xl-none\">\r\n        <div class=\"ecommerce-widget card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6\"><span>New Order</span>\r\n              <h3 class=\"total-num counter\" [CountTo]=\"16665\" [from]=\"0\" [duration]=\"1\">16665</h3>\r\n            </div>\r\n            <div class=\"col-6\">\r\n              <div class=\"text-md-right\">\r\n                <ul>\r\n                  <li>Profit<span class=\"product-stts font-primary ml-2\">8989<i\r\n                        class=\"icon-angle-up f-12 ml-1\"></i></span></li>\r\n                  <li>Loss<span class=\"product-stts font-primary ml-2\">2560<i\r\n                        class=\"icon-angle-down f-12 ml-1\"></i></span></li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-showcase\">\r\n            <div class=\"progress lg-progress-bar\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"25\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/widgets/chart/chart.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/widgets/chart/chart.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0cy9jaGFydC9jaGFydC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/widgets/chart/chart.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/widgets/chart/chart.component.ts ***!
  \*************************************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/widgets-chart/chart-widget */ "./src/app/shared/data/widgets-chart/chart-widget.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChartComponent = /** @class */ (function () {
    function ChartComponent() {
        this.isOpened = true;
        this.monthlydoughnutData = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlydoughnutData"];
        this.dailydoughnutData = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["dailydoughnutData"];
        this.chart1 = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["chart1"];
        this.chart2 = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["chart2"];
        this.chart3 = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["chart3"];
        this.WidgetBarChart1 = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["WidgetBarChart1"];
        this.WidgetBarChart2 = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["WidgetBarChart2"];
        this.liveProductChart = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["liveProductChart"];
        this.turnOverChart = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["turnOverChart"];
        this.monthlyChart = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlyChart"];
        this.usesChart = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["usesChart"];
        this.financeWidget = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["financeWidget"];
        this.orderStatusWidget = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["orderStatusWidget"];
        this.skillWidget = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["skillWidget"];
        // Doughnut Chart (Monthlt visitor chart)
        this.monthlydoughnutChartColorScheme = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlydoughnutChartcolorScheme"];
        this.monthlydoughnutChartShowLabels = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlydoughnutChartShowLabels"];
        this.monthlydoughnutChartGradient = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlydoughnutChartGradient"];
        // Doughnut Chart (Daily visitor chart)
        this.dailydoughnutChartColorScheme = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["dailydoughnutChartcolorScheme"];
        this.dailydoughnutChartShowLabels = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["dailydoughnutChartShowLabels"];
        this.dailydoughnutChartGradient = _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["dailydoughnutChartGradient"];
        Object.assign(this, { monthlydoughnutData: _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["monthlydoughnutData"], dailydoughnutData: _shared_data_widgets_chart_chart_widget__WEBPACK_IMPORTED_MODULE_1__["dailydoughnutData"] });
    }
    ChartComponent.prototype.ngOnInit = function () { };
    ChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! raw-loader!./chart.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/widgets/chart/chart.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./chart.component.scss */ "./src/app/components/widgets/chart/chart.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/general/general.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/widgets/general/general.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2lkZ2V0cy9nZW5lcmFsL2dlbmVyYWwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/widgets/general/general.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/widgets/general/general.component.ts ***!
  \*****************************************************************/
/*! exports provided: GeneralComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralComponent", function() { return GeneralComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeneralComponent = /** @class */ (function () {
    function GeneralComponent(calender) {
        this.calender = calender;
        this.time = new Date();
        this.jstoday = '';
        this.today = Date.now();
        this.owlcarousel = [
            {
                desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia",
                img: "assets/images/dashboard/boy-2.png",
                name: "Poio klot",
                designation: "Developer"
            },
            {
                desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia",
                img: "assets/images/dashboard/boy-2.png",
                name: "Poio klot",
                designation: "Developer"
            }
        ];
        this.owlcarouselOptions = {
            loop: true,
            margin: 10,
            items: 1,
            nav: false,
            dots: false
        };
        this.model = calender.getToday();
        this.jstoday = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(this.time, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
    }
    GeneralComponent.prototype.setTime = function () {
        this.intmin = setInterval(function () {
            var second = new Date().getSeconds();
            var sdegree = second * 6;
            var srotate = "rotate(" + sdegree + "deg)";
            var seconds = document.getElementById('sec').style.transform = srotate;
        }, 1000);
        this.intsec = setInterval(function () {
            var mins = new Date().getMinutes();
            var mdegree = mins * 6;
            var mrotate = "rotate(" + mdegree + "deg)";
            var minits = document.getElementById('min').style.transform = mrotate;
        }, 1000);
        this.inthour = setInterval(function () {
            var hour = new Date().getHours();
            var mints = new Date().getMinutes();
            var hdegree = hour * 30 + (mints / 2);
            var hrotate = "rotate(" + hdegree + "deg)";
            var hours = document.getElementById('hour').style.transform = hrotate;
        }, 1000);
    };
    GeneralComponent.prototype.ngOnInit = function () {
        var now = new Date();
        this.setTime();
    };
    GeneralComponent.prototype.ngOnDestroy = function () {
        if (this.intmin) {
            clearInterval(this.intmin);
        }
        if (this.intsec) {
            clearInterval(this.intsec);
        }
        if (this.inthour) {
            clearInterval(this.inthour);
        }
    };
    GeneralComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-general',
            template: __webpack_require__(/*! raw-loader!./general.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/widgets/general/general.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./general.component.scss */ "./src/app/components/widgets/general/general.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbCalendar"]])
    ], GeneralComponent);
    return GeneralComponent;
}());



/***/ }),

/***/ "./src/app/components/widgets/widgets-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/widgets/widgets-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: WidgetsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetsRoutingModule", function() { return WidgetsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _general_general_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./general/general.component */ "./src/app/components/widgets/general/general.component.ts");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/components/widgets/chart/chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        children: [
            {
                path: 'general',
                component: _general_general_component__WEBPACK_IMPORTED_MODULE_2__["GeneralComponent"],
                data: {
                    title: "General",
                    breadcrumb: "General"
                }
            },
            {
                path: 'chart',
                component: _chart_chart_component__WEBPACK_IMPORTED_MODULE_3__["ChartComponent"],
                data: {
                    title: "Chart",
                    breadcrumb: "Chart"
                }
            },
        ]
    }
];
var WidgetsRoutingModule = /** @class */ (function () {
    function WidgetsRoutingModule() {
    }
    WidgetsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], WidgetsRoutingModule);
    return WidgetsRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/widgets/widgets.module.ts":
/*!******************************************************!*\
  !*** ./src/app/components/widgets/widgets.module.ts ***!
  \******************************************************/
/*! exports provided: WidgetsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetsModule", function() { return WidgetsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _widgets_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./widgets-routing.module */ "./src/app/components/widgets/widgets-routing.module.ts");
/* harmony import */ var _general_general_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./general/general.component */ "./src/app/components/widgets/general/general.component.ts");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/components/widgets/chart/chart.component.ts");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-datepicker */ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_datepicker__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var angular_count_to__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-count-to */ "./node_modules/angular-count-to/modules/angular-count-to.es5.js");
/* harmony import */ var ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-owl-carousel-o */ "./node_modules/ngx-owl-carousel-o/fesm5/ngx-owl-carousel-o.js");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/fesm5/ng-chartist.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var WidgetsModule = /** @class */ (function () {
    function WidgetsModule() {
    }
    WidgetsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_general_general_component__WEBPACK_IMPORTED_MODULE_3__["GeneralComponent"], _chart_chart_component__WEBPACK_IMPORTED_MODULE_4__["ChartComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _widgets_routing_module__WEBPACK_IMPORTED_MODULE_2__["WidgetsRoutingModule"],
                ng2_datepicker__WEBPACK_IMPORTED_MODULE_5__["NgDatepickerModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                angular_count_to__WEBPACK_IMPORTED_MODULE_7__["CountToModule"],
                ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_8__["CarouselModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_9__["ChartistModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_11__["NgxChartsModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__["SharedModule"]
            ]
        })
    ], WidgetsModule);
    return WidgetsModule;
}());



/***/ }),

/***/ "./src/app/shared/data/widgets-chart/chart-widget.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/data/widgets-chart/chart-widget.ts ***!
  \***********************************************************/
/*! exports provided: chart1, chart2, chart3, WidgetBarChart1, WidgetBarChart2, liveProductChart, turnOverChart, monthlyChart, usesChart, financeWidget, orderStatusWidget, skillWidget, monthlydoughnutData, monthlydoughnutChartShowLabels, monthlydoughnutChartGradient, monthlydoughnutChartcolorScheme, dailydoughnutData, dailydoughnutChartShowLabels, dailydoughnutChartGradient, dailydoughnutChartcolorScheme */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart1", function() { return chart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart2", function() { return chart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart3", function() { return chart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetBarChart1", function() { return WidgetBarChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetBarChart2", function() { return WidgetBarChart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "liveProductChart", function() { return liveProductChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "turnOverChart", function() { return turnOverChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "monthlyChart", function() { return monthlyChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usesChart", function() { return usesChart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "financeWidget", function() { return financeWidget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "orderStatusWidget", function() { return orderStatusWidget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "skillWidget", function() { return skillWidget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "monthlydoughnutData", function() { return monthlydoughnutData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "monthlydoughnutChartShowLabels", function() { return monthlydoughnutChartShowLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "monthlydoughnutChartGradient", function() { return monthlydoughnutChartGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "monthlydoughnutChartcolorScheme", function() { return monthlydoughnutChartcolorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailydoughnutData", function() { return dailydoughnutData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailydoughnutChartShowLabels", function() { return dailydoughnutChartShowLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailydoughnutChartGradient", function() { return dailydoughnutChartGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailydoughnutChartcolorScheme", function() { return dailydoughnutChartcolorScheme; });
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_0__);

var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
//Line Chart
var chart1 = {
    type: 'Line',
    data: {
        series: [
            [25, 50, 30, 40, 60, 80, 50, 10, 50, 13, 0, 10, 30, 40, 10, 15, 20]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 100,
        showPoint: false,
    },
};
var chart2 = {
    type: 'Line',
    data: {
        series: [
            [25, 35, 70, 100, 90, 50, 60, 80, 40, 50, 60, 40, 80, 70, 60, 50, 100]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 100,
        showPoint: false,
    },
};
var chart3 = {
    type: 'Line',
    data: {
        series: [
            [50, 100, 80, 60, 50, 60, 40, 80, 40, 50, 60, 40, 60, 70, 40, 50, 20]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 100,
        showPoint: false,
    },
};
var WidgetBarChart1 = {
    type: 'Bar',
    data: {
        series: [
            [80.00, 80.00, 60.00, 20.00, 70.00, 0, 80.00, 60.00, 110.00, 20.00, 60.00, 100, 70, 30]
        ]
    },
    options: {
        labels: [80.00, 80.00, 60.00, 20.00, 70.00, 0, 80.00, 60.00, 110.00, 20.00, 60.00, 100, 70, 30],
        legend: {
            labels: { fontColor: 'white' }
        },
        axisX: {
            showGrid: false,
            showLabel: true,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: true,
            offset: 0,
        },
        tooltips: {
            disabled: true
        },
        toolTipContent: "<a href = {name}> {label}</a><hr/>Views: {y}",
        chartPadding: {
            bottom: 0,
            top: 0,
            left: 0
        },
        responsive: true,
        height: 200
    },
};
var WidgetBarChart2 = {
    type: 'Bar',
    data: {
        series: [
            [60.00, 110.00, 20.00, 60.00, 100.00, 70, 30.00, 80.00, 80.00, 60.00, 20.00, 70, 0, 80]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            bottom: 0,
            top: 0,
            left: 0
        },
        showArea: true,
        fullWidth: true,
        height: 200
    },
};
var liveProductChart = {
    type: 'Line',
    data: {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
            [1, 5, 2, 5, 4, 3, 6],
        ]
    },
    options: {
        low: 0,
        showArea: false,
        showPoint: false,
        fullWidth: true,
        height: 300,
    },
    events: {
        draw: function (data) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 2000 * data.index,
                        dur: 2000,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: chartist__WEBPACK_IMPORTED_MODULE_0__["Svg"].Easing.easeOutQuint
                    }
                });
            }
        }
    }
};
var turnOverChart = {
    type: 'Bar',
    data: {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
            [1.9, 4.4, 1.5, 5, 4.4, 3.4],
            [6.4, 5.7, 7, 4, 5.5, 3.5],
            [5, 2.3, 3.6, 6, 3.6, 2.3]
        ]
    },
    options: {
        height: 300,
    }
};
var monthlyChart = {
    type: 'Bar',
    data: {
        labels: ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10'],
        series: [
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000],
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000],
            [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
        ]
    },
    options: {
        stackBars: true,
        axisY: {
            labelInterpolationFnc: function (value) {
                return (value / 1000) + 'k';
            }
        },
        height: 300
    }
};
var usesChart = {
    type: 'Line',
    data: {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
            [1, 5, 2, 5, 4, 3],
            [2, 3, 4, 8, 1, 2],
            [5, 4, 3, 2, 1, 0.5]
        ]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        height: 300,
    }
};
var financeWidget = {
    type: 'Line',
    data: {
        series: [
            [5, 30, 27, 35, 30, 50, 70],
            [0, 5, 10, 7, 25, 20, 30]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 200,
        showPoint: false,
    },
};
var orderStatusWidget = {
    type: 'Line',
    data: {
        series: [
            [null],
            [40, 15, 25, 20, 15, 20, 10, 25, 35, 13, 35, 10, 30, 20, 10, 15, 20]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 200,
        showPoint: false,
    },
};
var skillWidget = {
    type: 'Line',
    data: {
        series: [
            [null],
            [null],
            [5, 10, 20, 14, 17, 21, 20, 10, 4, 13, 0, 10, 30, 40, 10, 15, 20]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
            top: 0
        },
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height: 200,
        showPoint: false,
    },
};
var monthlydoughnutData = [
    {
        value: 500,
        name: "Safari"
    },
    {
        value: 600,
        name: "Mozila Firefox"
    },
    {
        value: 400,
        name: "Google Crome"
    },
    {
        value: 700,
        name: "Opera Browser"
    }
];
//doughnut-Chart
//Monthly donught chart Options
var monthlydoughnutChartShowLabels = false;
var monthlydoughnutChartGradient = true;
var monthlydoughnutChartcolorScheme = {
    domain: [primary, secondary, primary, secondary],
};
var dailydoughnutData = [
    {
        value: 448,
        name: "India"
    },
    {
        value: 340,
        name: "USA"
    },
    {
        value: 270,
        name: "Canada"
    },
    {
        value: 359,
        name: "UK"
    }
];
//Monthly donught chart Options
var dailydoughnutChartShowLabels = false;
var dailydoughnutChartGradient = true;
var dailydoughnutChartcolorScheme = {
    domain: [primary, secondary, primary, secondary],
};


/***/ })

}]);
//# sourceMappingURL=components-widgets-widgets-module.js.map