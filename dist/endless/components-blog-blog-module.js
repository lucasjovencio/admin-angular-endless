(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-blog-blog-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/blog/blog-detail/blog-detail.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/blog/blog-detail/blog-detail.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-6 set-col-12\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-shadow\"><img class=\"img-fluid\" src=\"assets/images/blog/blog.jpg\" alt=\"\">\r\n          <div class=\"blog-details\">\r\n            <p class=\"digits\">25 July 2018</p>\r\n            <h4>Accusamus et iusto odio dignissimos ducimus qui blanditiis.</h4>\r\n            <ul class=\"blog-social\">\r\n              <li><i class=\"icofont icofont-user\"></i>Mark Jecno</li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 set-col-12\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-list row\">\r\n          <div class=\"col-sm-5\"><img class=\"img-fluid sm-100-w\" src=\"assets/images/blog/blog-2.jpg\" alt=\"\"></div>\r\n          <div class=\"col-sm-7\">\r\n            <div class=\"blog-details\">\r\n              <div class=\"blog-date digits\"><span>02</span> January 2018</div>\r\n              <h6>Perspiciatis unde omnis iste natus error sit voluptatem </h6>\r\n              <div class=\"blog-bottom-content\">\r\n                <ul class=\"blog-social\">\r\n                  <li>by: Admin</li>\r\n                  <li class=\"digits\">0 Hits</li>\r\n                </ul>\r\n                <hr>\r\n                <p class=\"mt-0\">inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim\r\n                  ipsam voluptatem quia voluptas sit.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-list row\">\r\n          <div class=\"col-sm-5\"><img class=\"img-fluid sm-100-w\" src=\"assets/images/blog/blog-3.jpg\" alt=\"\"></div>\r\n          <div class=\"col-sm-7\">\r\n            <div class=\"blog-details\">\r\n              <div class=\"blog-date digits\"><span>03</span> January 2018</div>\r\n              <h6>Perspiciatis unde omnis iste natus error sit voluptatem </h6>\r\n              <div class=\"blog-bottom-content\">\r\n                <ul class=\"blog-social\">\r\n                  <li>by: Admin</li>\r\n                  <li class=\"digits\">02 Hits</li>\r\n                </ul>\r\n                <hr>\r\n                <p class=\"mt-0\">inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim\r\n                  ipsam voluptatem quia voluptas sit.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-xl-3 set-col-6\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-grid text-center\"><img class=\"img-fluid top-radius-blog\"\r\n            src=\"assets/images/blog/blog-5.png\" alt=\"\">\r\n          <div class=\"blog-details-main\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">9 April 2018</li>\r\n              <li class=\"digits\">by: Admin</li>\r\n              <li class=\"digits\">0 Hits</li>\r\n            </ul>\r\n            <hr>\r\n            <h6 class=\"blog-bottom-details\">Perspiciatis unde omnis iste natus error sit.Dummy text</h6>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-xl-3 set-col-6\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-grid text-center\"><img class=\"img-fluid top-radius-blog\"\r\n            src=\"assets/images/blog/blog-6.png\" alt=\"\">\r\n          <div class=\"blog-details-main\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">9 April 2018</li>\r\n              <li class=\"digits\">by: Admin</li>\r\n              <li class=\"digits\">0 Hits</li>\r\n            </ul>\r\n            <hr>\r\n            <h6 class=\"blog-bottom-details\">Perspiciatis unde omnis iste natus error sit.Dummy text</h6>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-xl-3 set-col-6\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-grid text-center\"><img class=\"img-fluid top-radius-blog\"\r\n            src=\"assets/images/blog/blog-5.png\" alt=\"\">\r\n          <div class=\"blog-details-main\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">9 April 2018</li>\r\n              <li class=\"digits\">by: Admin</li>\r\n              <li class=\"digits\">0 Hits</li>\r\n            </ul>\r\n            <hr>\r\n            <h6 class=\"blog-bottom-details\">Perspiciatis unde omnis iste natus error sit.Dummy text</h6>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-xl-3 set-col-6\">\r\n      <div class=\"card\">\r\n        <div class=\"blog-box blog-grid text-center\"><img class=\"img-fluid top-radius-blog\"\r\n            src=\"assets/images/blog/blog-6.png\" alt=\"\">\r\n          <div class=\"blog-details-main\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">9 April 2018</li>\r\n              <li class=\"digits\">by: Admin</li>\r\n              <li class=\"digits\">0 Hits</li>\r\n            </ul>\r\n            <hr>\r\n            <h6 class=\"blog-bottom-details\">Perspiciatis unde omnis iste natus error sit.Dummy text</h6>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/blog/blog-single/blog-single.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/blog/blog-single/blog-single.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"blog-single\">\r\n        <div class=\"blog-box blog-details\"><img class=\"img-fluid w-100\" src=\"assets/images/blog/blog-single.jpg\"\r\n            alt=\"blog-main\">\r\n          <div class=\"blog-details\">\r\n            <ul class=\"blog-social\">\r\n              <li class=\"digits\">25 July 2018</li>\r\n              <li><i class=\"icofont icofont-user\"></i>Mark <span>Jecno </span></li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02<span>Hits</span></li>\r\n              <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n            </ul>\r\n            <h4>\r\n              All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this\r\n              the first true generator on the\r\n              Internet.\r\n            </h4>\r\n            <div class=\"single-blog-content-top\">\r\n              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the\r\n                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and\r\n                scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap\r\n                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the\r\n                release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing\r\n                software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n              <p>It is a long established fact that a reader will be distracted by the readable content of a page when\r\n                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution\r\n                of letters, as opposed to using 'Content here, content here', making it look like readable English. Many\r\n                desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a\r\n                search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have\r\n                evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <section class=\"comment-box\">\r\n          <h4>Comment</h4>\r\n          <hr>\r\n          <ul>\r\n            <li>\r\n              <div class=\"media align-self-center\"><img class=\"align-self-center\"\r\n                  src=\"assets/images/blog/comment.jpg\" alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4\">\r\n                      <h6 class=\"mt-0\">Jolio Mark<span> ( Designer )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8\">\r\n                      <ul class=\"comment-social float-left float-md-right\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered\r\n                    alteration in some form, by injected humour, or randomised words which don't look even slightly\r\n                    believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't\r\n                    anything embarrassing hidden in the middle of text.</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li>\r\n              <ul>\r\n                <li>\r\n                  <div class=\"media\"><img class=\"align-self-center\" src=\"assets/images/blog/9.jpg\"\r\n                      alt=\"Generic placeholder image\">\r\n                    <div class=\"media-body\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-xl-12\">\r\n                          <h6 class=\"mt-0\">Jolio Mark<span> ( Designer )</span></h6>\r\n                        </div>\r\n                      </div>\r\n                      <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered\r\n                        alteration in some form, by injected humour, or randomised words which don't look even slightly\r\n                        believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't\r\n                        anything embarrassing hidden in the middle of text.</p>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n              </ul>\r\n            </li>\r\n            <li>\r\n              <div class=\"media\"><img class=\"align-self-center\" src=\"assets/images/blog/4.jpg\"\r\n                  alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4\">\r\n                      <h6 class=\"mt-0\">Jolio Mark<span> ( Designer )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8\">\r\n                      <ul class=\"comment-social float-left float-md-right\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered\r\n                    alteration in some form, by injected humour, or randomised words which don't look even slightly\r\n                    believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't\r\n                    anything embarrassing hidden in the middle of text.</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li>\r\n              <div class=\"media\"><img class=\"align-self-center\" src=\"assets/images/blog/12.png\"\r\n                  alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4\">\r\n                      <h6 class=\"mt-0\">Jolio Mark<span> ( Designer )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8\">\r\n                      <ul class=\"comment-social float-left float-md-right\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered\r\n                    alteration in some form, by injected humour, or randomised words which don't look even slightly\r\n                    believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't\r\n                    anything embarrassing hidden in the middle of text.</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li>\r\n              <div class=\"media\"><img class=\"align-self-center\" src=\"assets/images/blog/14.png\"\r\n                  alt=\"Generic placeholder image\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-4\">\r\n                      <h6 class=\"mt-0\">Jolio Mark<span> ( Designer )</span></h6>\r\n                    </div>\r\n                    <div class=\"col-md-8\">\r\n                      <ul class=\"comment-social float-left float-md-right\">\r\n                        <li class=\"digits\"><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n                        <li class=\"digits\"><i class=\"icofont icofont-ui-chat\"></i>598 Comments</li>\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered\r\n                    alteration in some form, by injected humour, or randomised words which don't look even slightly\r\n                    believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't\r\n                    anything embarrassing hidden in the middle of text.</p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </section>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/blog/blog-detail/blog-detail.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/blog/blog-detail/blog-detail.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmxvZy9ibG9nLWRldGFpbC9ibG9nLWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/blog/blog-detail/blog-detail.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/blog/blog-detail/blog-detail.component.ts ***!
  \**********************************************************************/
/*! exports provided: BlogDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogDetailComponent", function() { return BlogDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlogDetailComponent = /** @class */ (function () {
    function BlogDetailComponent() {
    }
    BlogDetailComponent.prototype.ngOnInit = function () { };
    BlogDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blog-detail',
            template: __webpack_require__(/*! raw-loader!./blog-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/blog/blog-detail/blog-detail.component.html"),
            styles: [__webpack_require__(/*! ./blog-detail.component.scss */ "./src/app/components/blog/blog-detail/blog-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BlogDetailComponent);
    return BlogDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/blog/blog-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/components/blog/blog-routing.module.ts ***!
  \********************************************************/
/*! exports provided: BlogRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogRoutingModule", function() { return BlogRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _blog_detail_blog_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blog-detail/blog-detail.component */ "./src/app/components/blog/blog-detail/blog-detail.component.ts");
/* harmony import */ var _blog_single_blog_single_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blog-single/blog-single.component */ "./src/app/components/blog/blog-single/blog-single.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        children: [
            {
                path: 'details',
                component: _blog_detail_blog_detail_component__WEBPACK_IMPORTED_MODULE_2__["BlogDetailComponent"],
                data: {
                    title: "Blog-Detail",
                    breadcrumb: "Blog-Detail"
                }
            },
            {
                path: 'single',
                component: _blog_single_blog_single_component__WEBPACK_IMPORTED_MODULE_3__["BlogSingleComponent"],
                data: {
                    title: "Blog-Single",
                    breadcrumb: "Blog-Single"
                }
            }
        ]
    }
];
var BlogRoutingModule = /** @class */ (function () {
    function BlogRoutingModule() {
    }
    BlogRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BlogRoutingModule);
    return BlogRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/blog/blog-single/blog-single.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/blog/blog-single/blog-single.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYmxvZy9ibG9nLXNpbmdsZS9ibG9nLXNpbmdsZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/blog/blog-single/blog-single.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/blog/blog-single/blog-single.component.ts ***!
  \**********************************************************************/
/*! exports provided: BlogSingleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogSingleComponent", function() { return BlogSingleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlogSingleComponent = /** @class */ (function () {
    function BlogSingleComponent() {
    }
    BlogSingleComponent.prototype.ngOnInit = function () { };
    BlogSingleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blog-single',
            template: __webpack_require__(/*! raw-loader!./blog-single.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/blog/blog-single/blog-single.component.html"),
            styles: [__webpack_require__(/*! ./blog-single.component.scss */ "./src/app/components/blog/blog-single/blog-single.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BlogSingleComponent);
    return BlogSingleComponent;
}());



/***/ }),

/***/ "./src/app/components/blog/blog.module.ts":
/*!************************************************!*\
  !*** ./src/app/components/blog/blog.module.ts ***!
  \************************************************/
/*! exports provided: BlogModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogModule", function() { return BlogModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blog-routing.module */ "./src/app/components/blog/blog-routing.module.ts");
/* harmony import */ var _blog_detail_blog_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blog-detail/blog-detail.component */ "./src/app/components/blog/blog-detail/blog-detail.component.ts");
/* harmony import */ var _blog_single_blog_single_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./blog-single/blog-single.component */ "./src/app/components/blog/blog-single/blog-single.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var BlogModule = /** @class */ (function () {
    function BlogModule() {
    }
    BlogModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_blog_detail_blog_detail_component__WEBPACK_IMPORTED_MODULE_3__["BlogDetailComponent"], _blog_single_blog_single_component__WEBPACK_IMPORTED_MODULE_4__["BlogSingleComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlogRoutingModule"]
            ]
        })
    ], BlogModule);
    return BlogModule;
}());



/***/ })

}]);
//# sourceMappingURL=components-blog-blog-module.js.map