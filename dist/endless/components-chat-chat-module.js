(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-chat-chat-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/chat/chat/chat.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/chat/chat/chat.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col call-chat-sidebar col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body chat-body\">\r\n          <div class=\"chat-box\">\r\n            <!-- Chat left side Start-->\r\n            <div class=\"chat-left-aside\">\r\n              <div class=\"media\">\r\n                <img class=\"rounded-circle user-image\" [src]=\"profile?.profile\" alt=\"\">\r\n                <div class=\"about\">\r\n                  <div class=\"name f-w-600\">{{profile?.name}}</div>\r\n                  <div class=\"status\">{{profile?.status}}</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"people-list\" id=\"people-list\">\r\n                <div class=\"search\">\r\n                  <form class=\"theme-form\">\r\n                    <div class=\"form-group\">\r\n                      <input class=\"form-control\" [(ngModel)]=\"searchText\" [ngModelOptions]=\"{standalone: true}\"\r\n                        (keyup)=\"searchTerm(searchText)\" type=\"text\" placeholder=\"search\"><i class=\"fa fa-search\"></i>\r\n                    </div>\r\n                  </form>\r\n                </div>\r\n                <ul class=\"list\">\r\n                  <ng-container *ngFor=\"let user of searchUsers\">\r\n                    <li class=\"clearfix\" *ngIf=\"user.authenticate != 1\">\r\n                      <a href=\"javascript:void(0)\" (click)=\"userChat(user.id)\">\r\n                        <img class=\"rounded-circle user-image\" [src]=\"user?.profile\" alt=\"\">\r\n                        <div class=\"status-circle\" [ngClass]=\"{'online' : user.online, 'offline' : !user.online}\"></div>\r\n                        <div class=\"about\">\r\n                          <div class=\"name\">{{user?.name}}</div>\r\n                          <div class=\"status\">{{user?.status}}</div>\r\n                        </div>\r\n                      </a>\r\n                    </li>\r\n                  </ng-container>\r\n                  <ng-container *ngIf=\"!searchUsers.length\">\r\n                    <div class=\"search-not-found chat-search text-center\">\r\n                      <div>\r\n                        <img src=\"assets/images/search-not-found.png\" alt=\"\" class=\"second-search\">\r\n                        <p>Sorry, We didn't find any results matching this search</p>\r\n                      </div>\r\n                    </div>\r\n                  </ng-container>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n            <!-- Chat left side Ends-->\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col call-chat-body\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body p-0\">\r\n          <div class=\"row chat-box\">\r\n            <!-- Chat right side start-->\r\n            <div class=\"col pr-0 chat-right-aside\">\r\n              <!-- chat start-->\r\n              <div class=\"chat\">\r\n                <!-- chat-header start-->\r\n                <div class=\"chat-header clearfix\">\r\n                  <img class=\"rounded-circle\" [src]=\"chatUser?.profile\" alt=\"\">\r\n                  <div class=\"about\">\r\n                    <div class=\"name\">{{chatUser?.name}}  <span class=\"font-primary f-12\"\r\n                        *ngIf=\"chatUser?.typing\">Typing...</span></div>\r\n                    <div class=\"status digits\">{{chatUser?.seen}}</div>\r\n                  </div>\r\n                  <ul class=\"list-inline float-left float-sm-right chat-menu-icons\">\r\n                    <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"icon-search\"></i></a></li>\r\n                    <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"icon-clip\"></i></a></li>\r\n                    <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"icon-headphone-alt\"></i></a>\r\n                    </li>\r\n                    <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"icon-video-camera\"></i></a></li>\r\n                    <li class=\"list-inline-item toogle-bar\"><a href=\"javascript:void(0)\"><i class=\"icon-menu\"></i></a>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n                <!-- chat-header end-->\r\n                <div class=\"chat-history chat-msg-box custom-scrollbar\">\r\n                  <ul>\r\n                    <li *ngFor=\"let chat of chats?.message\" [ngClass]=\"{'clearfix': chat.sender == profile.id }\">\r\n                      <div class=\"message my-message mb-0\" *ngIf=\"chat.sender != profile.id; else myChat\">\r\n                        <img class=\"rounded-circle float-left chat-user-img img-30\" [src]=\"chatUser?.profile\" alt=\"\">\r\n                        <div class=\"message-data text-right\">\r\n                          <span class=\"message-data-time\">{{chat?.time}}</span>\r\n                        </div> {{chat?.text}}\r\n                      </div>\r\n                      <ng-template #myChat>\r\n                        <div class=\"message other-message pull-right\">\r\n                          <img class=\"rounded-circle float-right chat-user-img img-30\" [src]=\"profile?.profile\" alt=\"\">\r\n                          <div class=\"message-data\">\r\n                            <span class=\"message-data-time\">{{chat?.time}}</span>\r\n                          </div> {{chat?.text}}\r\n                        </div>\r\n                      </ng-template>\r\n                    </li>\r\n                    <ng-container *ngIf=\"!chats?.message.length\">\r\n                      <div class=\"image-not-found\">\r\n                        <div class=\"d-block start-conversion\">\r\n                          <img src=\"assets/images/start-conversion.jpg\">\r\n                        </div>\r\n                      </div>\r\n                    </ng-container>\r\n                  </ul>\r\n                </div>\r\n                <!-- end chat-history-->\r\n                <form #chatForm=\"ngForm\" (ngSubmit)=\"sendMessage(chatForm)\">\r\n                  <div class=\"chat-message clearfix\">\r\n                    <div class=\"row\">\r\n                      <div class=\"col-xl-12 d-flex\">\r\n                        <div class=\"smiley-box bg-primary\">\r\n                          <div class=\"picker\"><img src=\"assets/images/smiley.png\" alt=\"\"></div>\r\n                        </div>\r\n                        <div class=\"input-group text-box\">\r\n                          <input class=\"form-control input-txt-bx\" [class.border-danger]=\"error\" [(ngModel)]=\"chatText\"\r\n                            id=\"message-to-send\" ngModel type=\"text\" name=\"message\" placeholder=\"Type a message......\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-primary\" (click)=\"sendMessage(chatForm)\" type=\"button\">SEND</button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </form>\r\n                <!-- end chat-message-->\r\n                <!-- chat end-->\r\n                <!-- Chat right side ends-->\r\n              </div>\r\n            </div>\r\n            <div class=\"col pl-0 chat-menu\">\r\n              <ul class=\"nav nav-tabs nav-material nav-primary\" id=\"info-tab\" role=\"tablist\">\r\n                <li class=\"nav-item\">\r\n                  <a href=\"javascript:void(0)\" class=\"nav-link\" [ngClass]=\"{'active': openTab == 'call'}\"\r\n                    id=\"info-home-tab\" (click)=\"tabbed('call')\">CALL</a>\r\n                  <div class=\"material-border\"></div>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                  <a href=\"javascript:void(0)\" class=\"nav-link\" [ngClass]=\"{'active': openTab == 'status'}\"\r\n                    id=\"profile-info-tab\" (click)=\"tabbed('status')\">STATUS</a>\r\n                  <div class=\"material-border\"></div>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                  <a href=\"javascript:void(0)\" class=\"nav-link\" [ngClass]=\"{'active': openTab == 'profile'}\"\r\n                    id=\"contact-info-tab\" (click)=\"tabbed('profile')\">PROFILE</a>\r\n                  <div class=\"material-border\"></div>\r\n                </li>\r\n              </ul>\r\n              <div class=\"tab-content\" id=\"info-tabContent\">\r\n                <div class=\"tab-pane fade\" [ngClass]=\"{'show active': openTab == 'call'}\" id=\"info-home\" role=\"tabpanel\"\r\n                  aria-labelledby=\"info-home-tab\">\r\n                  <div class=\"people-list\">\r\n                    <ul class=\"list digits\">\r\n                      <ng-container *ngFor=\"let user of users\">\r\n                        <li class=\"clearfix\" *ngIf=\"user.authenticate != 1\">\r\n                          <img class=\"rounded-circle user-image\" [src]=\"user?.profile\" alt=\"\">\r\n                          <div class=\"about\">\r\n                            <div class=\"name\">{{user?.name}}</div>\r\n                            <div class=\"status\">\r\n                              <i class=\"fa fa-share font-success\" *ngIf=\"user?.call?.status == 'outgoing'\"></i> <i\r\n                                class=\"fa fa-reply font-danger\"\r\n                                *ngIf=\"user?.call?.status == 'incoming'\"></i>  {{user?.call.date_time}}\r\n                            </div>\r\n                          </div>\r\n                        </li>\r\n                      </ng-container>\r\n\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n                <div class=\"tab-pane fade\" [ngClass]=\"{'show active': openTab == 'status'}\" id=\"info-profile\"\r\n                  role=\"tabpanel\" aria-labelledby=\"profile-info-tab\">\r\n                  <div class=\"people-list\">\r\n                    <div class=\"search\">\r\n                      <form class=\"theme-form\">\r\n                        <div class=\"form-group\">\r\n                          <input class=\"form-control\" type=\"text\" placeholder=\"Write Status...\"><i\r\n                            class=\"fa fa-pencil\"></i>\r\n                        </div>\r\n                      </form>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"status\">\r\n                    <p class=\"font-dark\">Active</p>\r\n                    <hr>\r\n                    <p>\r\n                      Established fact that a reader will be\r\n                      distracted  <i class=\"icofont icofont-emo-heart-eyes font-danger f-20\"></i><i\r\n                        class=\"icofont icofont-emo-heart-eyes font-danger f-20 m-l-5\"></i>\r\n                    </p>\r\n                    <hr>\r\n                    <p>Dolore magna aliqua  <i class=\"icofont icofont-emo-rolling-eyes font-success f-20\"></i></p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"tab-pane fade\" [ngClass]=\"{'show active': openTab == 'profile'}\" id=\"info-contact\"\r\n                  role=\"tabpanel\" aria-labelledby=\"contact-info-tab\">\r\n                  <div class=\"user-profile\">\r\n                    <div class=\"image\">\r\n                      <div class=\"avatar text-center\">\r\n                        <img alt=\"\" [src]=\"profile?.profile\">\r\n                      </div>\r\n                      <div class=\"icon-wrapper\"><i class=\"icofont icofont-pencil-alt-5\"></i></div>\r\n                    </div>\r\n                    <div class=\"user-content text-center\">\r\n                      <h5 class=\"text-uppercase\">{{profile?.name}}</h5>\r\n                      <div class=\"social-media\">\r\n                        <ul class=\"list-inline\">\r\n                          <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a>\r\n                          </li>\r\n                          <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i\r\n                                class=\"fa fa-google-plus\"></i></a></li>\r\n                          <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a>\r\n                          </li>\r\n                          <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a>\r\n                          </li>\r\n                          <li class=\"list-inline-item\"><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n                        </ul>\r\n                      </div>\r\n                      <hr>\r\n                      <div class=\"follow text-center\">\r\n                        <div class=\"row\">\r\n                          <div class=\"col border-right\"><span>Following</span>\r\n                            <div class=\"follow-num\">236k</div>\r\n                          </div>\r\n                          <div class=\"col\"><span>Follower</span>\r\n                            <div class=\"follow-num\">3691k</div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <hr>\r\n                      <div class=\"text-center digits\">\r\n                        <p class=\"mb-0\">Mark.jecno23@gmail.com</p>\r\n                        <p class=\"mb-0\">+91 365 - 658 - 1236</p>\r\n                        <p class=\"mb-0\">Fax: 123-4560</p>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends -->\r\n"

/***/ }),

/***/ "./src/app/components/chat/chat-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/components/chat/chat-routing.module.ts ***!
  \********************************************************/
/*! exports provided: ChatRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatRoutingModule", function() { return ChatRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chat_chat_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat/chat.component */ "./src/app/components/chat/chat/chat.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [{
        path: '',
        children: [
            {
                path: '',
                component: _chat_chat_component__WEBPACK_IMPORTED_MODULE_2__["ChatComponent"],
                data: {
                    title: "Chat",
                    breadcrumb: ""
                }
            }
        ]
    }];
var ChatRoutingModule = /** @class */ (function () {
    function ChatRoutingModule() {
    }
    ChatRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChatRoutingModule);
    return ChatRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/chat/chat.module.ts":
/*!************************************************!*\
  !*** ./src/app/components/chat/chat.module.ts ***!
  \************************************************/
/*! exports provided: ChatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatModule", function() { return ChatModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat-routing.module */ "./src/app/components/chat/chat-routing.module.ts");
/* harmony import */ var _chat_chat_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chat/chat.component */ "./src/app/components/chat/chat/chat.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ChatModule = /** @class */ (function () {
    function ChatModule() {
    }
    ChatModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_chat_chat_component__WEBPACK_IMPORTED_MODULE_4__["ChatComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _chat_routing_module__WEBPACK_IMPORTED_MODULE_3__["ChatRoutingModule"]
            ]
        })
    ], ChatModule);
    return ChatModule;
}());



/***/ }),

/***/ "./src/app/components/chat/chat/chat.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/components/chat/chat/chat.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hhdC9jaGF0L2NoYXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/chat/chat/chat.component.ts":
/*!********************************************************!*\
  !*** ./src/app/components/chat/chat/chat.component.ts ***!
  \********************************************************/
/*! exports provided: ChatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatComponent", function() { return ChatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_chat_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/chat.service */ "./src/app/shared/services/chat.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChatComponent = /** @class */ (function () {
    function ChatComponent(chatService) {
        var _this = this;
        this.chatService = chatService;
        this.openTab = "call";
        this.users = [];
        this.searchUsers = [];
        this.error = false;
        this.notFound = false;
        this.chatService.getUsers().subscribe(function (users) {
            _this.searchUsers = users;
            _this.users = users;
        });
    }
    ChatComponent.prototype.ngOnInit = function () {
        this.userChat(this.id);
        this.getProfile();
    };
    ChatComponent.prototype.tabbed = function (val) {
        this.openTab = val;
    };
    // Get user Profile
    ChatComponent.prototype.getProfile = function () {
        var _this = this;
        this.chatService.getCurrentUser().subscribe(function (userProfile) { return _this.profile = userProfile; });
    };
    // User Chat
    ChatComponent.prototype.userChat = function (id) {
        var _this = this;
        if (id === void 0) { id = 1; }
        this.chatService.chatToUser(id).subscribe(function (chatUser) { return _this.chatUser = chatUser; });
        this.chatService.getChatHistory(id).subscribe(function (chats) { return _this.chats = chats; });
    };
    // Send Message to User
    ChatComponent.prototype.sendMessage = function (form) {
        if (!form.value.message) {
            this.error = true;
            return false;
        }
        this.error = false;
        var chat = {
            sender: this.profile.id,
            receiver: this.chatUser.id,
            receiver_name: this.chatUser.name,
            message: form.value.message
        };
        this.chatService.sendMessage(chat);
        this.chatText = '';
        this.chatUser.seen = 'online';
        this.chatUser.online = true;
    };
    ChatComponent.prototype.searchTerm = function (term) {
        if (!term)
            return this.searchUsers = this.users;
        term = term.toLowerCase();
        var user = [];
        this.users.filter(function (users) {
            if (users.name.toLowerCase().includes(term)) {
                user.push(users);
            }
        });
        this.searchUsers = user;
    };
    ChatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chat',
            template: __webpack_require__(/*! raw-loader!./chat.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/chat/chat/chat.component.html"),
            styles: [__webpack_require__(/*! ./chat.component.scss */ "./src/app/components/chat/chat/chat.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_chat_service__WEBPACK_IMPORTED_MODULE_1__["ChatService"]])
    ], ChatComponent);
    return ChatComponent;
}());



/***/ })

}]);
//# sourceMappingURL=components-chat-chat-module.js.map