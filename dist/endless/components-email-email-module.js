(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-email-email-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/email/email.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/email/email.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"email-wrap\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-3 col-md-6\">\r\n        <div class=\"email-left-aside\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"email-app-sidebar\">\r\n                <div class=\"media\">\r\n                  <div class=\"media-size-email\">\r\n                    <img class=\"mr-3 rounded-circle img-60\" src=\"assets/images/user/1.jpg\" alt=\"\">\r\n                  </div>\r\n                  <div class=\"media-body\">\r\n                    <h6 class=\"f-w-600\">MARKJENCO</h6>\r\n                    <p>Markjecno@gmail.com</p>\r\n                  </div>\r\n                </div>\r\n                <ul class=\"nav main-menu\" role=\"tablist\">\r\n                  <li class=\"nav-item\">\r\n                    <a href=\"javascript:void(0)\" class=\"btn-primary btn-block btn-mail\" id=\"pills-darkhome-tab\"\r\n                      data-toggle=\"pill\" role=\"tab\" aria-controls=\"pills-darkhome\" aria-selected=\"true\"\r\n                      (click)=\"compose=true\">\r\n                      <i class=\"icofont icofont-envelope mr-2\"></i> NEW MAIL\r\n                    </a>\r\n                  </li>\r\n                  <li class=\"nav-item\" (click)=\"getUserEmail(type='inbox')\">\r\n                    <a href=\"javascript:void(0)\" class=\"show\" id=\"pills-darkprofile-tab\" data-toggle=\"pill\" role=\"tab\"\r\n                      aria-controls=\"pills-darkprofile\" aria-selected=\"false\">\r\n                      <span class=\"title\"><i class=\"icon-import\"></i> Inbox</span>\r\n                      <span class=\"badge pull-right digits\">({{ getUserEmail('inbox').length }})</span>\r\n                    </a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='allmail')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-folder\"></i> All mail</span></a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='sent')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-new-window\"></i> Sent</span>\r\n                      <span class=\"badge pull-right digits\">({{ getUserEmail('sent').length }})</span>\r\n                    </a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='draft')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-pencil-alt\"></i> DRAFT</span>\r\n                      <span class=\"badge pull-right digits\">({{ getUserEmail('draft').length }})</span>\r\n                    </a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='trash')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-trash\"></i> TRASH</span>\r\n                      <span class=\"badge pull-right digits\">({{ getUserEmail('trash').length }})</span>\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-info-alt\"></i> IMPORTANT</span></a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='favourite')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-star\"></i> Starred</span></a>\r\n                  </li>\r\n                  <li>\r\n                    <hr>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='unread')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-email\"></i> UNREAD</span></a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='spam')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-export\"></i> SPAM</span></a>\r\n                  </li>\r\n                  <li (click)=\"getUserEmail(type='outbox')\">\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-share\"></i> OUTBOX</span></a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-file\"></i> UPDATE</span></a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-bell\"></i> ALERT</span></a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"javascript:void(0)\"><span class=\"title\"><i class=\"icon-notepad\"></i> NOTES</span>\r\n                      <span class=\"badge pull-right digits\">(20)</span>\r\n                    </a>\r\n                  </li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-3 col-md-6\">\r\n        <div class=\"email-right-aside\">\r\n          <div class=\"card email-body\">\r\n            <div class=\"pr-0 b-r-light\">\r\n              <div class=\"email-top\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <h5>{{type ? ( type | titlecase ) : 'Inbox' }}</h5>\r\n                  </div>\r\n                  <div class=\"col text-right\">\r\n                    <div ngbDropdown class=\"dropdown\">\r\n                      <button class=\"btn bg-transparent dropdown-toggle p-0 text-muted\" id=\"dropdownMenuButton\"\r\n                        ngbDropdownToggle>More</button>\r\n                      <div ngbDropdownMenu aria-labelledby=\"dropdownMenuButton\" class=\"email-option-position\">\r\n                        <a href=\"javascript:void(0)\" class=\"dropdown-item\" (click)=\"moveEmails('draft')\">Move to\r\n                          Draft</a>\r\n                        <a href=\"javascript:void(0)\" class=\"dropdown-item\" (click)=\"moveEmails('trash')\">Move to\r\n                          Trash</a>\r\n                        <a href=\"javascript:void(0)\" class=\"dropdown-item\" (click)=\"moveEmails('spam')\">Move to Spam</a>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"inbox\">\r\n                <a href=\"javascript:void(0)\" *ngFor=\"let userEmail of getUserEmail(type ? type : 'inbox') | slice:0:7\">\r\n                  <div class=\"media\">\r\n                    <label class=\"d-block\" for=\"chk-ani\">\r\n                      <input class=\"checkbox_animated\" id=\"chk-ani\" name=\"chk-ani\" type=\"checkbox\"\r\n                        (change)=selectedmail($event,userEmail)>\r\n                    </label>\r\n                    <div class=\"media-size-email\" (click)=\"selectedUserEmail(userEmail)\"><img\r\n                        class=\"mr-3 rounded-circle img-50\" [src]='userEmail.image' alt=\"\"></div>\r\n                    <div class=\"media-body\" (click)=\"selectedUserEmail(userEmail)\">\r\n                      <h6>{{userEmail.name}} <small>(<span class=\"digits\">{{userEmail.date}}</span>)</small></h6>\r\n                      <p>{{userEmail.cc}}</p>\r\n                    </div>\r\n                  </div>\r\n                </a>\r\n                <ng-container *ngIf=\"!getUserEmail(type ? type : 'inbox').length\">\r\n                    <div class=\"search-not-found text-center\">\r\n                      <div class=\"\">\r\n                        <img src=\"assets/images/search-not-found.png\" alt=\"\" class=\"second-search\">\r\n                        <p class=\"mb-0\">No mail found</p>\r\n                      </div>\r\n                    </div>\r\n                  </ng-container>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-6 col-md-12\">\r\n        <div class=\"email-right-aside\">\r\n          <div class=\"card email-body radius-left\">\r\n            <div class=\"pl-0\">\r\n              <div class=\"tab-content\">\r\n                <div class=\"tab-pane fade\" [ngClass]=\"{'active show':compose}\" id=\"pills-darkhome\" role=\"tabpanel\"\r\n                  aria-labelledby=\"pills-darkhome-tab\">\r\n                  <div class=\"email-compose\">\r\n                    <div class=\"email-top compose-border\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-8 xl-50\">\r\n                          <h4 class=\"mb-0\">New Message</h4>\r\n                        </div>\r\n                        <div class=\"col-sm-4 btn-middle xl-50\">\r\n                          <button class=\"btn btn-primary btn-block btn-mail text-center mb-0 mt-0\" type=\"button\"><i\r\n                              class=\"fa fa-paper-plane mr-2\"></i> SEND</button>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"email-wrapper\">\r\n                      <form class=\"theme-form\">\r\n                        <div class=\"form-group\">\r\n                          <label class=\"col-form-label pt-0\" for=\"exampleInputEmail1\">To</label>\r\n                          <input class=\"form-control\" id=\"exampleInputEmail1\" type=\"email\">\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"exampleInputPassword1\">Subject</label>\r\n                          <input class=\"form-control\" id=\"exampleInputPassword1\" type=\"text\">\r\n                        </div>\r\n                        <div class=\"form-group mb-0\">\r\n                          <label class=\"text-muted\">Message</label>\r\n                          <ck-editor name=\"editor\" id=\"text-box\" [(ngModel)]=\"editorValue\" skin=\"moono-lisa\"\r\n                            language=\"en\"></ck-editor>\r\n                        </div>\r\n                      </form>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"tab-pane fade\" [ngClass]=\"{'active show':!compose}\" id=\"pills-darkprofile\" role=\"tabpanel\"\r\n                  aria-labelledby=\"pills-darkprofile-tab\">\r\n                  <div class=\"email-content\">\r\n                    <div class=\"email-top\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-md-6 xl-100 col-sm-12\">\r\n                          <div class=\"media\">\r\n                            <img class=\"mr-3 rounded-circle img-50\" src=\"{{selectEmail?.image}}\" alt=\"image\">\r\n                            <div class=\"media-body\">\r\n                              <h6>{{selectEmail?.name}} <small><span class=\"digits\">({{selectEmail?.date}})</span> <span\r\n                                    class=\"digits\"> 6:00</span> AM</small></h6>\r\n                              <p>{{selectEmail?.cc}}</p>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col-md-6 col-sm-12\">\r\n                          <div class=\"float-right d-flex\">\r\n                            <p class=\"user-emailid\">{{selectEmail?.email}}</p>\r\n                            <i class=\"fa fa-star-o f-18 mt-1\" [class.starred]=\"selectEmail?.favourite\"\r\n                              (click)=\"addFavourite(selectEmail)\"></i>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"email-wrapper\">\r\n                      <span [innerHTML]=\"selectEmail?.text\"></span>\r\n                      <hr>\r\n                      <div class=\"d-inline-block\">\r\n                        <h6 class=\"text-muted\"><i class=\"icofont icofont-clip\"></i> ATTACHMENTS</h6><a\r\n                          class=\"text-muted text-right right-download\" href=\"javascript:void(0)\"><i\r\n                            class=\"fa fa-long-arrow-down mr-2\"></i>Download All</a>\r\n                        <div class=\"clearfix\"></div>\r\n                      </div>\r\n                      <div class=\"attachment\">\r\n                        <ul class=\"list-inline\">\r\n                          <li class=\"list-inline-item\"><img class=\"img-fluid\" src=\"assets/images/email/1.jpg\" alt=\"\">\r\n                          </li>\r\n                          <li class=\"list-inline-item\"><img class=\"img-fluid\" src=\"assets/images/email/2.jpg\" alt=\"\">\r\n                          </li>\r\n                          <li class=\"list-inline-item\"><img class=\"img-fluid\" src=\"assets/images/email/3.jpg\" alt=\"\">\r\n                          </li>\r\n                        </ul>\r\n                      </div>\r\n                      <hr>\r\n                      <div class=\"action-wrapper\">\r\n                        <ul class=\"actions\">\r\n                          <li><a class=\"text-muted\" href=\"javascript:void(0)\"><i class=\"fa fa-reply mr-2\"></i>Reply</a>\r\n                          </li>\r\n                          <li><a class=\"text-muted\" href=\"javascript:void(0)\"><i class=\"fa fa-reply-all mr-2\"></i>Reply\r\n                              All</a></li>\r\n                          <li><a class=\"text-muted\" href=\"javascript:void(0)\"><i\r\n                                class=\"fa fa-share mr-2\"></i></a>Forward</li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->\r\n"

/***/ }),

/***/ "./src/app/components/email/email-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/email/email-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: EmailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailRoutingModule", function() { return EmailRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _email_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./email.component */ "./src/app/components/email/email.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _email_component__WEBPACK_IMPORTED_MODULE_2__["EmailComponent"],
                data: {
                    title: "Email",
                    breadcrumb: ""
                }
            }
        ]
    }
];
var EmailRoutingModule = /** @class */ (function () {
    function EmailRoutingModule() {
    }
    EmailRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EmailRoutingModule);
    return EmailRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/email/email.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/email/email.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW1haWwvZW1haWwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/email/email.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/email/email.component.ts ***!
  \*****************************************************/
/*! exports provided: EmailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailComponent", function() { return EmailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_email_email__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/data/email/email */ "./src/app/shared/data/email/email.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmailComponent = /** @class */ (function () {
    function EmailComponent() {
        this.compose = true;
        this.allEmails = _shared_data_email_email__WEBPACK_IMPORTED_MODULE_1__["Mail"].Emails;
        this.selectedEmails = [];
    }
    EmailComponent.prototype.getUserEmail = function (type) {
        var emails = [];
        return this.allEmails.filter(function (email) {
            if (type == 'allmail') {
                return emails.push(email);
            }
            else if (type == 'favourite') {
                if (email.favourite) {
                    return emails.push(email);
                }
            }
            else if (email.type === type) {
                return emails.push(email);
            }
        });
    };
    EmailComponent.prototype.selectedUserEmail = function (email) {
        this.selectEmail = email;
        this.compose = false;
    };
    EmailComponent.prototype.selectedmail = function ($event, email) {
        var index = this.selectedEmails.indexOf(email);
        if ($event.target.checked === true && index === -1) {
            // val not found, pushing onto array
            this.selectedEmails.push(email);
        }
        else {
            // val is found, removing from array
            this.selectedEmails.splice(index, 1);
        }
    };
    EmailComponent.prototype.moveEmails = function (val) {
        if (!val)
            return;
        this.selectedEmails.filter(function (email) {
            return email.type = val;
        });
    };
    EmailComponent.prototype.addFavourite = function (email) {
        email.favourite = !email.favourite;
    };
    EmailComponent.prototype.ngOnInit = function () { };
    EmailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email',
            template: __webpack_require__(/*! raw-loader!./email.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/email/email.component.html"),
            styles: [__webpack_require__(/*! ./email.component.scss */ "./src/app/components/email/email.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EmailComponent);
    return EmailComponent;
}());



/***/ }),

/***/ "./src/app/components/email/email.module.ts":
/*!**************************************************!*\
  !*** ./src/app/components/email/email.module.ts ***!
  \**************************************************/
/*! exports provided: EmailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailModule", function() { return EmailModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_ckeditor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-ckeditor */ "./node_modules/ngx-ckeditor/fesm5/ngx-ckeditor.js");
/* harmony import */ var _email_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./email-routing.module */ "./src/app/components/email/email-routing.module.ts");
/* harmony import */ var _email_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email.component */ "./src/app/components/email/email.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var EmailModule = /** @class */ (function () {
    function EmailModule() {
    }
    EmailModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_email_component__WEBPACK_IMPORTED_MODULE_6__["EmailComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                ngx_ckeditor__WEBPACK_IMPORTED_MODULE_4__["CKEditorModule"],
                _email_routing_module__WEBPACK_IMPORTED_MODULE_5__["EmailRoutingModule"]
            ]
        })
    ], EmailModule);
    return EmailModule;
}());



/***/ }),

/***/ "./src/app/shared/data/email/email.ts":
/*!********************************************!*\
  !*** ./src/app/shared/data/email/email.ts ***!
  \********************************************/
/*! exports provided: Mail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Mail", function() { return Mail; });
var Mail = /** @class */ (function () {
    function Mail() {
    }
    Mail.Emails = [
        {
            id: 1,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "pork@company.com",
            date: "15 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 2,
            image: "assets/images/user/2.png",
            name: "Lorm lpsa",
            email: "lpsa@company.com",
            date: "16 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: true
        },
        {
            id: 3,
            image: "assets/images/user/3.jpg",
            name: "Vincent Porter",
            email: "vincent@company.com",
            date: "17 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 4,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "vincent@company.com",
            date: "18 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 5,
            image: "assets/images/user/2.png",
            name: "Lorm lpsa",
            email: "Lion@company.com",
            date: "19 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 6,
            image: "assets/images/user/3.jpg",
            name: "Vincent Porter",
            email: "solvn@company.com",
            date: "20 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: true
        },
        {
            id: 7,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "soft@company.com",
            date: "21 Feb 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "trash",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 8,
            image: "assets/images/user/2.png",
            name: "Lorm lpsa",
            email: "lorapasoft23@company.com",
            date: "1 March 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "trash",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 9,
            image: "assets/images/user/3.jpg",
            name: "Vincent Porter",
            email: "vincent@company.com",
            date: "2 March 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "trash",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 10,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "vincent@company.com",
            date: "15 March 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "draft",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 11,
            image: "assets/images/user/2.png",
            name: "Lorm lpsa",
            email: "herry@company.com",
            date: "16 March 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "draft",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 12,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "john@company.com",
            date: "21 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "outbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: true
        },
        {
            id: 13,
            image: "assets/images/user/2.png",
            name: "Lorm lpsa",
            email: "deojoseph@company.com",
            date: "22 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "outbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 14,
            image: "assets/images/user/6.jpg",
            name: "Charlie Porter",
            email: "charle21@company.com",
            date: "23 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "unread",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 15,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 16,
            image: "assets/images/user/8.jpg",
            name: "Ross Singh",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: true
        },
        {
            id: 17,
            image: "assets/images/user/14.png",
            name: "Sam Porter",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: true
        },
        {
            id: 18,
            image: "assets/images/user/7.jpg",
            name: "Jenisha Trivedi",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 19,
            image: "assets/images/user/8.jpg",
            name: "Ross Singh",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 20,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 21,
            image: "assets/images/user/14.png",
            name: "Sam Porter",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "inbox",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 22,
            image: "assets/images/user/5.jpg",
            name: "Ashiyana Oza",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 23,
            image: "assets/images/user/6.jpg",
            name: "Charlie Porter",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 24,
            image: "assets/images/user/7.jpg",
            name: "Jenisha Trivedi",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "draft",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 25,
            image: "assets/images/user/8.jpg",
            name: "Ross Singh",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "draft",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 26,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "sent",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 27,
            image: "assets/images/user/14.png",
            name: "Sam Porter",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "spam",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 28,
            image: "assets/images/user/8.jpg",
            name: "Ross Singh",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "spam",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 29,
            image: "assets/images/user/9.jpg",
            name: "Hileri makr",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "spam",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 30,
            image: "assets/images/user/14.png",
            name: "Sam Porter",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "spam",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
        {
            id: 32,
            image: "assets/images/user/5.jpg",
            name: "Ashiyana Oza",
            email: "sam1254@company.com",
            date: "30 Apr 2019",
            cc: "Mattis luctus. Donec nisi diam,",
            type: "spam",
            text: "<p>Hello</p><p>Dear Sir Good Morning,</p><h5>Elementum varius nisi vel tempus. Donec eleifend egestas viverra.</h5><p class='m-b-20'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non diam facilisis, commodo libero et, commodo sapien. Pellentesque sollicitudin massa sagittis dolor facilisis, sit amet vulputate nunc molestie. Pellentesque maximus nibh id luctus porta. Ut consectetur dui nec nulla mattis luctus. Donec nisi diam, congue vitae felis at, ullamcorper bibendum tortor. Vestibulum pellentesque felis felis. Etiam ac tortor felis. Ut elit arcu, rhoncus in laoreet vel, gravida sed tortor.</p><p>In elementum varius nisi vel tempus. Donec eleifend egestas viverra. Donec dapibus sollicitudin blandit. Donec scelerisque purus sit amet feugiat efficitur. Quisque feugiat semper sapien vel hendrerit. Mauris lacus felis, consequat nec pellentesque viverra, venenatis a lorem. Sed urna lectus.Quisque feugiat semper sapien vel hendrerit</p>",
            favourite: false
        },
    ];
    return Mail;
}());



/***/ })

}]);
//# sourceMappingURL=components-email-email-module.js.map