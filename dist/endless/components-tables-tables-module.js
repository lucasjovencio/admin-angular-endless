(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-tables-tables-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/basic/basic.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/bootstrap-tables/basic/basic.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Basic Table</h5>\r\n                    <span>Use a class<code>table</code> to any table.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First Name</th>\r\n                                <th scope=\"col\">Last Name</th>\r\n                                <th scope=\"col\">Username</th>\r\n                                <th scope=\"col\">Role</th>\r\n                                <th scope=\"col\">Country</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Alexander</td>\r\n                                <td>Orton</td>\r\n                                <td>@mdorton</td>\r\n                                <td>Admin</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>John Deo</td>\r\n                                <td>Deo</td>\r\n                                <td>@johndeo</td>\r\n                                <td>User</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td>Randy Orton</td>\r\n                                <td>the Bird</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>UK</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">4</th>\r\n                                <td>Randy Mark</td>\r\n                                <td>Ottandy</td>\r\n                                <td>@mdothe</td>\r\n                                <td>user</td>\r\n                                <td>AUS</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">5</th>\r\n                                <td>Ram Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>IND</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Inverse Table</h5>\r\n                    <span>Use a class <code>table-inverse</code> inside table element.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-inverse\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First Name</th>\r\n                                <th scope=\"col\">Last Name</th>\r\n                                <th scope=\"col\">Username</th>\r\n                                <th scope=\"col\">Role</th>\r\n                                <th scope=\"col\">Country</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Alexander</td>\r\n                                <td>Orton</td>\r\n                                <td>@mdorton</td>\r\n                                <td>Admin</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>John Deo</td>\r\n                                <td>Deo</td>\r\n                                <td>@johndeo</td>\r\n                                <td>User</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td>Randy Orton</td>\r\n                                <td>the Bird</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>UK</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">4</th>\r\n                                <td>Randy Mark</td>\r\n                                <td>Ottandy</td>\r\n                                <td>@mdothe</td>\r\n                                <td>user</td>\r\n                                <td>AUS</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">5</th>\r\n                                <td>Ram Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>IND</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Inverse Table with Primary background</h5>\r\n                    <span>Use a class <code>.bg-info, .bg-success, .bg-warning and .bg-danger classes.</code> with light\r\n                        text on dark backgrounds inside table element.<br>To set the light background color use\r\n                        .bg-[color] class where [color] is the value of your selected color from stack color palette. So\r\n                        for teal color background class will be .bg-teal</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-striped bg-primary\">\r\n                        <thead class=\"tbl-strip-thad-bdr\">\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First Name</th>\r\n                                <th scope=\"col\">Last Name</th>\r\n                                <th scope=\"col\">Username</th>\r\n                                <th scope=\"col\">Role</th>\r\n                                <th scope=\"col\">Country</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Alexander</td>\r\n                                <td>Orton</td>\r\n                                <td>@mdorton</td>\r\n                                <td>Admin</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>John Deo</td>\r\n                                <td>Deo</td>\r\n                                <td>@johndeo</td>\r\n                                <td>User</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td>Randy Orton</td>\r\n                                <td>the Bird</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>UK</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">4</th>\r\n                                <td>Randy Mark</td>\r\n                                <td>Ottandy</td>\r\n                                <td>@mdothe</td>\r\n                                <td>user</td>\r\n                                <td>AUS</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">5</th>\r\n                                <td>Ram Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>IND</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Hoverable rows</h5>\r\n                    <span>Use a class <code>table-hover</code> to enable a hover state on table rows within a\r\n                        <code>tbody</code>.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-hover\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First Name</th>\r\n                                <th scope=\"col\">Last Name</th>\r\n                                <th scope=\"col\">Username</th>\r\n                                <th scope=\"col\">Role</th>\r\n                                <th scope=\"col\">Country</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Alexander</td>\r\n                                <td>Orton</td>\r\n                                <td>@mdorton</td>\r\n                                <td>Admin</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>John Deo</td>\r\n                                <td>Deo</td>\r\n                                <td>@johndeo</td>\r\n                                <td>User</td>\r\n                                <td>USA</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td>Randy Orton</td>\r\n                                <td>the Bird</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>UK</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">4</th>\r\n                                <td>Randy Mark</td>\r\n                                <td>Ottandy</td>\r\n                                <td>@mdothe</td>\r\n                                <td>user</td>\r\n                                <td>AUS</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">5</th>\r\n                                <td>Ram Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@twitter</td>\r\n                                <td>admin</td>\r\n                                <td>IND</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Contextual classes</h5>\r\n                    <span>Use contextual classes to color table rows or individual cells. you may use Classes\r\n                        <code>table-primary</code>,<code>table-secondary</code>,<code>table-success</code>,<code>table-info</code>,<code>table-warning</code>,<code>table-danger</code>,<code>table-light</code>,<code>table-active</code>\r\n                        in<code>tr</code></span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">Class</th>\r\n                                <th scope=\"col\">Heading</th>\r\n                                <th scope=\"col\">Heading</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr class=\"table-primary\">\r\n                                <th scope=\"row\">Primary</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-secondary\">\r\n                                <th scope=\"row\">Secondary</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-success\">\r\n                                <th scope=\"row\">Success</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-info\">\r\n                                <th scope=\"row\">Info</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-warning\">\r\n                                <th scope=\"row\">Warning</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-danger\">\r\n                                <th scope=\"row\">Danger</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-light\">\r\n                                <th scope=\"row\">Light</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                            <tr class=\"table-active\">\r\n                                <th scope=\"row\">Active</th>\r\n                                <td>Cell</td>\r\n                                <td>Cell</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Text or background utilities</h5>\r\n                    <span>Regular table background variants are not available with the inverse table, however, you may\r\n                        use Classes\r\n                        <code>bg-dark</code>,<code>bg-warning</code>,<code>bg-success</code>,<code>bg-info</code>,<code>bg-danger</code>,<code>bg-primary</code>,<code>bg-secondary</code>,<code>bg-light</code>\r\n                        in<code>td</code></span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-borderedfor\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">Heading</th>\r\n                                <th scope=\"col\">Heading</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr class=\"table-active\">\r\n                                <td class=\"bg-primary\">1</td>\r\n                                <td class=\"bg-primary\">primary</td>\r\n                                <td class=\"bg-primary\">primary</td>\r\n                            </tr>\r\n                            <tr class=\"table-active\">\r\n                                <td class=\"bg-secondary\">2</td>\r\n                                <td class=\"bg-secondary\">secondary</td>\r\n                                <td class=\"bg-secondary\">secondary</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"bg-success\">3</td>\r\n                                <td class=\"bg-success\">success</td>\r\n                                <td class=\"bg-success\">success</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"bg-info\">4</td>\r\n                                <td class=\"bg-info\">info</td>\r\n                                <td class=\"bg-info\">info</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"bg-warning\">5</td>\r\n                                <td class=\"bg-warning\">warning</td>\r\n                                <td class=\"bg-warning\">warning</td>\r\n                            </tr>\r\n\r\n\r\n                            <tr>\r\n                                <td class=\"bg-danger\">6</td>\r\n                                <td class=\"bg-danger\">danger</td>\r\n                                <td class=\"bg-danger\">danger</td>\r\n                            </tr>\r\n                            <tr class=\"table-active\">\r\n                                <td class=\"bg-light\">7</td>\r\n                                <td class=\"bg-light\">light</td>\r\n                                <td class=\"bg-light\">light</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Table head options</h5>\r\n                    <span>Similar to tables and dark tables, use the modifier classes <code>.thead-dark</code> to make\r\n                        <code>thead</code> appear light or dark gray.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead class=\"thead-dark\">\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Table head options</h5>\r\n                    <span>Similar to tables and dark tables, use the modifier classes <code>.bg-*</code>and\r\n                        <code>.thead-light</code> to make <code>thead</code> appear light or dark gray.\r\n                    </span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead class=\"thead-light\">\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Striped Row </h5>\r\n                    <span>Use <code>.table-striped</code> to add zebra-striping to any table row within the\r\n                        <code></code>. This styling doesn't work in IE8 and below as :nth-child CSS selector isn't\r\n                        supported.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-striped\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Striped Row with Inverse Table</h5>\r\n                    <span>Use <code>.table-striped</code> to add zebra-striping to any table row within the\r\n                        <code></code>. This styling doesn't work in IE8 and below as :nth-child CSS selector isn't\r\n                        supported.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-inverse table-striped\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Caption</h5>\r\n                    <span>A <code>&lt;caption&gt;</code> functions like a heading for a table. It helps users with\r\n                        screen readers to find a table and understand what it’s about and decide if they want to read\r\n                        it.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <caption>List of users</caption>\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Responsive Tables</h5>\r\n                    <span>A <code>&lt;caption&gt;</code> functions like a heading for a table. It helps users with\r\n                        screen readers to find a table and understand what it’s about and decide if they want to read\r\n                        it.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Breckpoint Specific</h5>\r\n                    <span>Use <code>.table-responsive -sm|-md|-lg|-xl</code> functions like a heading for a table. It\r\n                        helps users with screen readers to find a table and understand what it’s about and decide if\r\n                        they want to read it.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-responsive-sm\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-responsive-sm\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-responsive-sm\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-responsive-sm\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                        <th scope=\"col\">Table heading</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                        <td>Table cell</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/border/border.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/bootstrap-tables/border/border.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Horizontal Borders</h5>\r\n                    <span>Example of <code>horizontal</code> table borders. This is a default table border style\r\n                        attached to <code> .table</code> class.All borders have the same grey color and style, table\r\n                        itself doesn't have a border, but you can add this border\r\n                        using<code> .table-border-horizontal</code>class added to the table with\r\n                        <code>.table</code>class.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table table-border-horizontal\">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                    <th scope=\"col\">Role</th>\r\n                                    <th scope=\"col\">Country</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Alexander</td>\r\n                                    <td>Orton</td>\r\n                                    <td>@mdorton</td>\r\n                                    <td>Admin</td>\r\n                                    <td>USA</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>John Deo</td>\r\n                                    <td>Deo</td>\r\n                                    <td>@johndeo</td>\r\n                                    <td>User</td>\r\n                                    <td>USA</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Randy Orton</td>\r\n                                    <td>the Bird</td>\r\n                                    <td>@twitter</td>\r\n                                    <td>admin</td>\r\n                                    <td>UK</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">4</th>\r\n                                    <td>Randy Mark</td>\r\n                                    <td>Ottandy</td>\r\n                                    <td>@mdothe</td>\r\n                                    <td>user</td>\r\n                                    <td>AUS</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">5</th>\r\n                                    <td>Ram Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@twitter</td>\r\n                                    <td>admin</td>\r\n                                    <td>IND</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Vertical Borders</h5>\r\n                    <span>Example of <code>Vertical </code> table borders. This is a default table border style attached\r\n                        to <code> .table</code> class.All borders have the same grey color and style, table itself\r\n                        doesn't have a border, but you can add this border\r\n                        using<code> .table-border-vertical</code>class added to the table with\r\n                        <code>.table</code>class.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table table-border-vertical\">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                    <th scope=\"col\">Role</th>\r\n                                    <th scope=\"col\">Country</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Alexander</td>\r\n                                    <td>Orton</td>\r\n                                    <td>@mdorton</td>\r\n                                    <td>Admin</td>\r\n                                    <td>USA</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>John Deo</td>\r\n                                    <td>Deo</td>\r\n                                    <td>@johndeo</td>\r\n                                    <td>User</td>\r\n                                    <td>USA</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Randy Orton</td>\r\n                                    <td>the Bird</td>\r\n                                    <td>@twitter</td>\r\n                                    <td>admin</td>\r\n                                    <td>UK</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">4</th>\r\n                                    <td>Randy Mark</td>\r\n                                    <td>Ottandy</td>\r\n                                    <td>@mdothe</td>\r\n                                    <td>user</td>\r\n                                    <td>AUS</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">5</th>\r\n                                    <td>Ram Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@twitter</td>\r\n                                    <td>admin</td>\r\n                                    <td>IND</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Both Bordeds</h5>\r\n                    <span>Example of <code>horizontal</code> table borders. This is a default table border style\r\n                        attached to <code> .table</code> class.All borders have the same grey color and style, table\r\n                        itself doesn't have a border, but you can add this border\r\n                        using<code> .table-bordered</code>class added to the table with <code>.table</code>class.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table table-bordered\">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Borderless Table</h5>\r\n                    <span>Example of <code>horizontal</code> table borders. This is a default table border style\r\n                        attached to <code> .table</code> class.All borders have the same grey color and style, table\r\n                        itself doesn't have a border, but you can add this border\r\n                        using<code> .table-bordernone</code>class added to the table with\r\n                        <code>.table</code>class.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table table-bordernone\">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th class=\"bd-t-none\" scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th class=\"bd-t-none\" scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th class=\"bd-t-none\" scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Default Table Border</h5>\r\n                    <span>Example of <code>Default Table Border</code>.This is a default table border style attached to\r\n                        <code>.table</code> class.All borders have the same grey color and style, table itself doesn't\r\n                        have a border, but you can add this border using<code> .table</code>class added to the table\r\n                        with <code>.table</code>class.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Solid Border</h5>\r\n                    <span>Example of a <code>solid</code> border inside table <code>thead</code>. This border inherits\r\n                        all styling options from the default border style, the only difference is it has\r\n                        <code>2px</code> width. Sometimes it could be useful for visual separation and addition\r\n                        highlight. To use this border add <code>.border-solid</code> to the table head row. This border\r\n                        style works only with row borders.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr class=\"border-solid\">\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr>\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Double Border</h5>\r\n                    <span>Example of a <code>double</code> border inside table head. This border has <code>3px</code>\r\n                        width, <code>double</code> style and inherits all styling options from the default border style.\r\n                        Visually it displays table <code>head</code> and <code>body</code> as 2 separated table areas.\r\n                        To use this border add <code>.border-double</code> to the table head row.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr class=\"double\">\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr class=\"double\">\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr class=\"double\">\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr class=\"double\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Dotted Border</h5>\r\n                    <span>Example of a <code>dotted</code> border inside table head. This border has <code>3px</code>\r\n                        width, <code>dotted</code> style and inherits all styling options from the default border style.\r\n                        Visually it displays table <code>head</code> and <code>body</code> as 2 separated table areas.\r\n                        To use this border add <code>.border-dotted</code> to the table head row.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr class=\"dotted\">\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr class=\"dotted\">\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr class=\"dotted\">\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr class=\"dotted\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Dashed Border</h5>\r\n                    <span>Example of a <code>dashed</code> border inside table head. This border has <code>3px</code>\r\n                        width, <code>dashed</code> style and inherits all styling options from the default border style.\r\n                        Visually it displays table <code>head</code> and <code>body</code> as 2 separated table areas.\r\n                        To use this border add <code>.border-dashed</code> to the table head row.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr class=\"dashed\">\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr class=\"dashed\">\r\n                                    <th scope=\"row\">1</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@mdo</td>\r\n                                </tr>\r\n                                <tr class=\"dashed\">\r\n                                    <th scope=\"row\">2</th>\r\n                                    <td>Mark</td>\r\n                                    <td>Otto</td>\r\n                                    <td>@TwBootstrap</td>\r\n                                </tr>\r\n                                <tr class=\"dashed\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Border Bottom Color </h5>\r\n                    <span>EExample of a table head border with<code>custom </code> color. According to the custom color\r\n                        system options, you can set any of predefined colors by adding <code>.border-bottom-*</code>\r\n                        class to the table head row. This technique works with all border styles demonstrated\r\n                        above.</span>\r\n                </div>\r\n                <div class=\"card-body p-0\">\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table\">\r\n                            <thead>\r\n                                <tr class=\"border-bottom-primary\">\r\n                                    <th scope=\"col\">#</th>\r\n                                    <th scope=\"col\">First Name</th>\r\n                                    <th scope=\"col\">Last Name</th>\r\n                                    <th scope=\"col\">Username</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr class=\"border-bottom-secondary\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                                <tr class=\"border-bottom-success\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                                <tr class=\"border-bottom-info\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                                <tr class=\"border-bottom-warning\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                                <tr class=\"border-bottom-danger\">\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <th scope=\"row\">3</th>\r\n                                    <td>Jacob</td>\r\n                                    <td>Thornton</td>\r\n                                    <td>@fat</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/sizing/sizing.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/bootstrap-tables/sizing/sizing.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Extra Large Tables</h5>\r\n                    <span>Example of Extra large table, Add <code>.table-xl</code> class to the <code>.table</code> to\r\n                        create a table with extra large spacing. Extra larger table all rows have <code>1.25rem</code>\r\n                        height.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-xl\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th>#</th>\r\n                                <th>First Name</th>\r\n                                <th>Last Name</th>\r\n                                <th>Username</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Mark</td>\r\n                                <td>Otto</td>\r\n                                <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td>Larry</td>\r\n                                <td>the Bird</td>\r\n                                <td>@twitter</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Large Table</h5>\r\n                    <span>Example of large table, Add <code>.table-lg</code>class to the <code>.table</code>to create a\r\n                        table with large spacing. larger table all rows have <code>0.9rem</code> height.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-lg\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First</th>\r\n                                <th scope=\"col\">Last</th>\r\n                                <th scope=\"col\">Handle</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Mark</td>\r\n                                <td>Otto</td>\r\n                                <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td colspan=\"2\">Larry the Bird</td>\r\n                                <td>@twitter</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Default Table</h5>\r\n                    <span>Example of large table, Add <code>.table-de</code>class to the <code>.table</code>to create a\r\n                        table with large spacing. larger table all rows have <code>0.75rem</code> height.</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-de\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First</th>\r\n                                <th scope=\"col\">Last</th>\r\n                                <th scope=\"col\">Handle</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Mark</td>\r\n                                <td>Otto</td>\r\n                                <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td colspan=\"2\">Larry the Bird</td>\r\n                                <td>@twitter</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Small table</h5>\r\n                    <span>Example of small table, Add <code>.table-sm</code> class to the <code>.table</code> to create\r\n                        a table with small spacing. Small table all rows have <code>0.3rem</code> height</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-sm\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First</th>\r\n                                <th scope=\"col\">Last</th>\r\n                                <th scope=\"col\">Handle</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Mark</td>\r\n                                <td>Otto</td>\r\n                                <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td colspan=\"2\">Larry the Bird</td>\r\n                                <td>@twitter</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Extra Small table</h5>\r\n                    <span>Example of small table, Add <code>.table-xs</code> class to the <code>.table</code> to create\r\n                        a table with extra small spacing. Small table all rows have <code>1.5rem</code> height</span>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-xs\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th scope=\"col\">#</th>\r\n                                <th scope=\"col\">First</th>\r\n                                <th scope=\"col\">Last</th>\r\n                                <th scope=\"col\">Handle</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr>\r\n                                <th scope=\"row\">1</th>\r\n                                <td>Mark</td>\r\n                                <td>Otto</td>\r\n                                <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">2</th>\r\n                                <td>Jacob</td>\r\n                                <td>Thornton</td>\r\n                                <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <th scope=\"row\">3</th>\r\n                                <td colspan=\"2\">Larry the Bird</td>\r\n                                <td>@twitter</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/styling/styling.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/bootstrap-tables/styling/styling.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Defult Styling</h5>\r\n                    <span>use class <code>table table-styling</code> inside table element</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table table-styling\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Table head options</h5>\r\n                    <span>Use class<code>.table-primary</code> inside thead tr element.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead class=\"table-primary\">\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Table head options With Primary Background</h5>\r\n                    <span>Use background classes<code>.bg-*</code>and\r\n                        <code>table-primary</code>,<code>table-secondary</code>,<code>table-success</code>,<code>table-danger</code>\r\n                        ,<code>table-info</code>,<code>table-warning</code> to make custom <code>thead</code>\r\n                        background. You can also use Stack Admin color palette classes for background.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead class=\"bg-primary\">\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive\">\r\n                            <table class=\"table\">\r\n                                <thead class=\"table-success\">\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Table Footer Styling</h5>\r\n                    <span>Use class\r\n                        <code>table-info</code>,<code>table-success</code>,<code>table-success</code>,<code>table-info</code>,<code>table-danger</code>,<code>table-primary</code>,<code>table-secondary</code>,<code>table-light</code>,<code>table-active</code>and\r\n                        also use <code>bg-*</code> inside tfoot element.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive table-border-radius\">\r\n                            <table class=\"table\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n\r\n                                </tbody>\r\n                                <tfoot class=\"table-success\">\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tfoot>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Custom Table Color</h5>\r\n                    <span>Use class<code>table-*</code> inside table element.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive table-border-radius\">\r\n                            <table class=\"table table-styling table-primary\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Custom Table Color with Hover and Stripped</h5>\r\n                    <span>Use class<code>table-hover, table-striped table-*</code>\r\n                        <code>table-info</code>,<code>table-success</code>,<code>table-success</code>,<code>table-info</code>,<code>table-danger</code>,<code>table-primary</code>,<code>table-secondary</code>,<code>table-light</code>,<code>table-active</code>\r\n                        inside table element.</span>\r\n                </div>\r\n                <div class=\"card-block row\">\r\n                    <div class=\"col-sm-12 col-lg-12 col-xl-12\">\r\n                        <div class=\"table-responsive table-border-radius\">\r\n                            <table class=\"table table-styling table-hover table-striped table-primary\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th scope=\"col\">#</th>\r\n                                        <th scope=\"col\">First Name</th>\r\n                                        <th scope=\"col\">Last Name</th>\r\n                                        <th scope=\"col\">Username</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <th scope=\"row\">1</th>\r\n                                        <td>Mark</td>\r\n                                        <td>Otto</td>\r\n                                        <td>@mdo</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">2</th>\r\n                                        <td>Jacob</td>\r\n                                        <td>Thornton</td>\r\n                                        <td>@fat</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <th scope=\"row\">3</th>\r\n                                        <td>Larry</td>\r\n                                        <td>the Bird</td>\r\n                                        <td>@twitter</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/basic/basic.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/basic/basic.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Basic Table</h5>\r\n                </div>\r\n                <div class=\"card-body custom-datatable\">\r\n                    <ngx-datatable class=\"bootstrap\" [rows]=\"company\" [loadingIndicator]=\"loadingIndicator\"\r\n                        [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"40\" [summaryRow]=\"true\"\r\n                        [summaryPosition]=\"'bottom'\" [footerHeight]=\"40\" [rowHeight]=\"'auto'\"\r\n                        [reorderable]=\"reorderable\">\r\n                    </ngx-datatable>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/editing/editing.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/editing/editing.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Editing Table</h5>\r\n        </div>\r\n        <div class=\"card-body custom-datatable\">\r\n          <ngx-datatable #mydatatable class=\"bootstrap\" [headerHeight]=\"50\" [limit]=\"5\" [columnMode]=\"'force'\"\r\n            [footerHeight]=\"50\" [rowHeight]=\"'auto'\" [rows]=\"company\">\r\n            <ngx-datatable-column name=\"Name\">\r\n              <ng-template ngx-datatable-cell-template let-rowIndex=\"rowIndex\" let-value=\"value\" let-row=\"row\">\r\n                <span title=\"Double click to edit\" (dblclick)=\"editing[rowIndex + '-name'] = true\"\r\n                  *ngIf=\"!editing[rowIndex + '-name']\">\r\n                  {{value}}\r\n                </span>\r\n                <input autofocus (blur)=\"updateValue($event, 'name', rowIndex)\" *ngIf=\"editing[rowIndex+ '-name']\"\r\n                  type=\"text\" [value]=\"value\" />\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Gender\">\r\n              <ng-template ngx-datatable-cell-template let-rowIndex=\"rowIndex\" let-row=\"row\" let-value=\"value\">\r\n                <span title=\"Double click to edit\" (dblclick)=\"editing[rowIndex + '-gender'] = true\"\r\n                  *ngIf=\"!editing[rowIndex + '-gender']\">\r\n                  {{value}}\r\n                </span>\r\n                <select *ngIf=\"editing[rowIndex + '-gender']\" (change)=\"updateValue($event, 'gender', rowIndex)\"\r\n                  [value]=\"value\">\r\n                  <option value=\"male\">Male</option>\r\n                  <option value=\"female\">Female</option>\r\n                </select>\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Age\">\r\n              <ng-template ngx-datatable-cell-template let-value=\"value\">\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n          </ngx-datatable>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/filter/filter.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/filter/filter.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Filter Table</h5>\r\n                </div>\r\n                <div class=\"card-body custom-datatable\">\r\n                    <input type='text' class=\"filter-ngx form-control\" placeholder='Type to filter the name column...'\r\n                        (keyup)='updateFilter($event)' />\r\n                    <ngx-datatable #table class='bootstrap' [columns]=\"columns\" [columnMode]=\"'force'\"\r\n                        [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"'auto'\" [limit]=\"10\" [rows]='company'>\r\n                    </ngx-datatable>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Fullscreen Table</h5>\r\n                </div>\r\n                <div class=\"card-body custom-datatable\">\r\n                    <ngx-datatable class=\"bootstrap fullscreen\" [columnMode]=\"'force'\" [headerHeight]=\"50\"\r\n                        [footerHeight]=\"0\" [rowHeight]=\"50\" [scrollbarV]=\"false\" [scrollbarH]=\"false\" [rows]=\"employee\">\r\n                        <ngx-datatable-column name=\"Id\" [width]=\"80\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Name\" [width]=\"300\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Gender\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Age\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"City\" [width]=\"300\" prop=\"address.city\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"State\" [width]=\"300\" prop=\"address.state\"></ngx-datatable-column>\r\n                    </ngx-datatable>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/paging/paging.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/paging/paging.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Paging Table</h5>\r\n                </div>\r\n                <div class=\"card-body custom-datatable\">\r\n                    <ngx-datatable class=\"bootstrap\" [rows]=\"company\"\r\n                        [columns]=\"[{name:'Name'},{name:'Gender'},{name:'Company'}]\" [columnMode]=\"'force'\"\r\n                        [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"'auto'\" [limit]=\"10\">\r\n                    </ngx-datatable>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/selection/selection.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/ngx-datatables/selection/selection.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Selection Table</h5>\r\n                </div>\r\n                <div class=\"card-body custom-datatable\">\r\n                    <ngx-datatable class=\"bootstrap\" [rows]=\"company\" [columnMode]=\"'force'\" [headerHeight]=\"50\"\r\n                        [footerHeight]=\"50\" [rowHeight]=\"'auto'\" [limit]=\"5\" [selected]=\"selected\"\r\n                        [selectionType]=\"'checkbox'\" [selectAllRowsOnPage]=\"false\" (select)='onSelect($event)'>\r\n                        <ngx-datatable-column [width]=\"30\" [sortable]=\"false\" [canAutoResize]=\"false\"\r\n                            [draggable]=\"false\" [resizeable]=\"false\" [headerCheckboxable]=\"true\" [checkboxable]=\"true\">\r\n                        </ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Name\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Gender\"></ngx-datatable-column>\r\n                        <ngx-datatable-column name=\"Company\"></ngx-datatable-column>\r\n                    </ngx-datatable>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/tables/smart-table/smart-table.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/tables/smart-table/smart-table.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Selection Table</h5>\r\n                </div>\r\n                <div class=\"card-body server-datatable selection-datatable\">\r\n                    <div class=\"table-responsive\">\r\n                        <ng2-smart-table [settings]=\"settings\" [source]=\"data\"></ng2-smart-table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/basic/basic.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/basic/basic.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL2Jvb3RzdHJhcC10YWJsZXMvYmFzaWMvYmFzaWMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/basic/basic.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/basic/basic.component.ts ***!
  \*****************************************************************************/
/*! exports provided: BasicComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicComponent", function() { return BasicComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BasicComponent = /** @class */ (function () {
    function BasicComponent() {
    }
    BasicComponent.prototype.ngOnInit = function () { };
    BasicComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basic',
            template: __webpack_require__(/*! raw-loader!./basic.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/basic/basic.component.html"),
            styles: [__webpack_require__(/*! ./basic.component.scss */ "./src/app/components/tables/bootstrap-tables/basic/basic.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BasicComponent);
    return BasicComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/border/border.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/border/border.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL2Jvb3RzdHJhcC10YWJsZXMvYm9yZGVyL2JvcmRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/border/border.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/border/border.component.ts ***!
  \*******************************************************************************/
/*! exports provided: BorderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BorderComponent", function() { return BorderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BorderComponent = /** @class */ (function () {
    function BorderComponent() {
    }
    BorderComponent.prototype.ngOnInit = function () { };
    BorderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-border',
            template: __webpack_require__(/*! raw-loader!./border.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/border/border.component.html"),
            styles: [__webpack_require__(/*! ./border.component.scss */ "./src/app/components/tables/bootstrap-tables/border/border.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BorderComponent);
    return BorderComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/sizing/sizing.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/sizing/sizing.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL2Jvb3RzdHJhcC10YWJsZXMvc2l6aW5nL3NpemluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/sizing/sizing.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/sizing/sizing.component.ts ***!
  \*******************************************************************************/
/*! exports provided: SizingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SizingComponent", function() { return SizingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SizingComponent = /** @class */ (function () {
    function SizingComponent() {
    }
    SizingComponent.prototype.ngOnInit = function () { };
    SizingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sizing',
            template: __webpack_require__(/*! raw-loader!./sizing.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/sizing/sizing.component.html"),
            styles: [__webpack_require__(/*! ./sizing.component.scss */ "./src/app/components/tables/bootstrap-tables/sizing/sizing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SizingComponent);
    return SizingComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/styling/styling.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/styling/styling.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL2Jvb3RzdHJhcC10YWJsZXMvc3R5bGluZy9zdHlsaW5nLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/tables/bootstrap-tables/styling/styling.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/tables/bootstrap-tables/styling/styling.component.ts ***!
  \*********************************************************************************/
/*! exports provided: StylingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StylingComponent", function() { return StylingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StylingComponent = /** @class */ (function () {
    function StylingComponent() {
    }
    StylingComponent.prototype.ngOnInit = function () { };
    StylingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-styling',
            template: __webpack_require__(/*! raw-loader!./styling.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/bootstrap-tables/styling/styling.component.html"),
            styles: [__webpack_require__(/*! ./styling.component.scss */ "./src/app/components/tables/bootstrap-tables/styling/styling.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StylingComponent);
    return StylingComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/basic/basic.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/basic/basic.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL2Jhc2ljL2Jhc2ljLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/basic/basic.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/basic/basic.component.ts ***!
  \***************************************************************************/
/*! exports provided: BasicNgxDatatableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicNgxDatatableComponent", function() { return BasicNgxDatatableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/data/tables/company */ "./src/app/shared/data/tables/company.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BasicNgxDatatableComponent = /** @class */ (function () {
    function BasicNgxDatatableComponent() {
        this.company = [];
        this.loadingIndicator = true;
        this.reorderable = true;
        this.columns = [
            { prop: 'name' },
            { name: 'Gender' },
            { name: 'Company' }
        ];
        this.company = _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__["companyDB"].data;
    }
    BasicNgxDatatableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basic',
            template: __webpack_require__(/*! raw-loader!./basic.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/basic/basic.component.html"),
            styles: [__webpack_require__(/*! ./basic.component.scss */ "./src/app/components/tables/ngx-datatables/basic/basic.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BasicNgxDatatableComponent);
    return BasicNgxDatatableComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/editing/editing.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/editing/editing.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL2VkaXRpbmcvZWRpdGluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/editing/editing.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/editing/editing.component.ts ***!
  \*******************************************************************************/
/*! exports provided: EditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditingComponent", function() { return EditingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/data/tables/company */ "./src/app/shared/data/tables/company.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditingComponent = /** @class */ (function () {
    function EditingComponent() {
        this.editing = {};
        this.company = [];
        this.company = _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__["companyDB"].data;
    }
    EditingComponent.prototype.updateValue = function (event, cell, rowIndex) {
        this.editing[rowIndex + '-' + cell] = false;
        this.company[rowIndex][cell] = event.target.value;
        this.company = this.company.slice();
    };
    EditingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-editing',
            template: __webpack_require__(/*! raw-loader!./editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/editing/editing.component.html"),
            styles: [__webpack_require__(/*! ./editing.component.scss */ "./src/app/components/tables/ngx-datatables/editing/editing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EditingComponent);
    return EditingComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/filter/filter.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/filter/filter.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL2ZpbHRlci9maWx0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/filter/filter.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/filter/filter.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FilterNgxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterNgxComponent", function() { return FilterNgxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @swimlane/ngx-datatable/release */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../shared/data/tables/company */ "./src/app/shared/data/tables/company.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FilterNgxComponent = /** @class */ (function () {
    function FilterNgxComponent() {
        this.company = [];
        this.temp = [];
        this.columns = [
            { prop: 'name' },
            { name: 'Company' },
            { name: 'Gender' }
        ];
        this.company = _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_2__["companyDB"].data;
    }
    FilterNgxComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        // filter our data
        var temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.company = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('DatatableComponent', { static: true }),
        __metadata("design:type", _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"])
    ], FilterNgxComponent.prototype, "table", void 0);
    FilterNgxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filter',
            template: __webpack_require__(/*! raw-loader!./filter.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/filter/filter.component.html"),
            styles: [__webpack_require__(/*! ./filter.component.scss */ "./src/app/components/tables/ngx-datatables/filter/filter.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FilterNgxComponent);
    return FilterNgxComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL2Z1bGxzY3JlZW4vZnVsbHNjcmVlbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FullscreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullscreenComponent", function() { return FullscreenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_employee__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/data/tables/employee */ "./src/app/shared/data/tables/employee.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FullscreenComponent = /** @class */ (function () {
    function FullscreenComponent() {
        this.employee = [];
        this.employee = _shared_data_tables_employee__WEBPACK_IMPORTED_MODULE_1__["employeeDB"].emp;
    }
    FullscreenComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-fullscreen',
            template: __webpack_require__(/*! raw-loader!./fullscreen.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.html"),
            styles: [__webpack_require__(/*! ./fullscreen.component.scss */ "./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FullscreenComponent);
    return FullscreenComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/paging/paging.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/paging/paging.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL3BhZ2luZy9wYWdpbmcuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/paging/paging.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/paging/paging.component.ts ***!
  \*****************************************************************************/
/*! exports provided: PagingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagingComponent", function() { return PagingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/data/tables/company */ "./src/app/shared/data/tables/company.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagingComponent = /** @class */ (function () {
    function PagingComponent() {
        this.company = [];
        this.company = _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__["companyDB"].data;
    }
    PagingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-paging',
            template: __webpack_require__(/*! raw-loader!./paging.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/paging/paging.component.html"),
            styles: [__webpack_require__(/*! ./paging.component.scss */ "./src/app/components/tables/ngx-datatables/paging/paging.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PagingComponent);
    return PagingComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/selection/selection.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/selection/selection.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL25neC1kYXRhdGFibGVzL3NlbGVjdGlvbi9zZWxlY3Rpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tables/ngx-datatables/selection/selection.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/tables/ngx-datatables/selection/selection.component.ts ***!
  \***********************************************************************************/
/*! exports provided: SelectionNgxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectionNgxComponent", function() { return SelectionNgxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/data/tables/company */ "./src/app/shared/data/tables/company.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SelectionNgxComponent = /** @class */ (function () {
    function SelectionNgxComponent() {
        this.company = [];
        this.selected = [];
        this.company = _shared_data_tables_company__WEBPACK_IMPORTED_MODULE_1__["companyDB"].data;
    }
    SelectionNgxComponent.prototype.onSelect = function (_a) {
        var _b;
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
    };
    SelectionNgxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-selection',
            template: __webpack_require__(/*! raw-loader!./selection.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/ngx-datatables/selection/selection.component.html"),
            styles: [__webpack_require__(/*! ./selection.component.scss */ "./src/app/components/tables/ngx-datatables/selection/selection.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SelectionNgxComponent);
    return SelectionNgxComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/smart-table/smart-table.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/components/tables/smart-table/smart-table.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFibGVzL3NtYXJ0LXRhYmxlL3NtYXJ0LXRhYmxlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/tables/smart-table/smart-table.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/tables/smart-table/smart-table.component.ts ***!
  \************************************************************************/
/*! exports provided: SmartTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableComponent", function() { return SmartTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SmartTableComponent = /** @class */ (function () {
    function SmartTableComponent() {
        this.settings = {
            columns: {
                id: {
                    title: 'ID'
                },
                name: {
                    title: 'Full Name'
                },
                username: {
                    title: 'User Name'
                },
                email: {
                    title: 'Email'
                }
            }
        };
        this.data = [
            {
                id: 1,
                name: "Leanne Graham",
                username: "Bret",
                email: "Sincere@april.biz"
            },
            {
                id: 2,
                name: "Ervin Howell",
                username: "Antonette",
                email: "Shanna@melissa.tv"
            },
            {
                id: 3,
                name: "Nicholas DuBuque",
                username: "Nicholas.Stanton",
                email: "Rey.Padberg@rosamond.biz"
            }
        ];
    }
    SmartTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-smart-table',
            template: __webpack_require__(/*! raw-loader!./smart-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/tables/smart-table/smart-table.component.html"),
            styles: [__webpack_require__(/*! ./smart-table.component.scss */ "./src/app/components/tables/smart-table/smart-table.component.scss")]
        })
    ], SmartTableComponent);
    return SmartTableComponent;
}());



/***/ }),

/***/ "./src/app/components/tables/tables-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/components/tables/tables-routing.module.ts ***!
  \************************************************************/
/*! exports provided: TablesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablesRoutingModule", function() { return TablesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _bootstrap_tables_basic_basic_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bootstrap-tables/basic/basic.component */ "./src/app/components/tables/bootstrap-tables/basic/basic.component.ts");
/* harmony import */ var _bootstrap_tables_border_border_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bootstrap-tables/border/border.component */ "./src/app/components/tables/bootstrap-tables/border/border.component.ts");
/* harmony import */ var _bootstrap_tables_sizing_sizing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bootstrap-tables/sizing/sizing.component */ "./src/app/components/tables/bootstrap-tables/sizing/sizing.component.ts");
/* harmony import */ var _bootstrap_tables_styling_styling_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bootstrap-tables/styling/styling.component */ "./src/app/components/tables/bootstrap-tables/styling/styling.component.ts");
/* harmony import */ var _ngx_datatables_basic_basic_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ngx-datatables/basic/basic.component */ "./src/app/components/tables/ngx-datatables/basic/basic.component.ts");
/* harmony import */ var _ngx_datatables_editing_editing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ngx-datatables/editing/editing.component */ "./src/app/components/tables/ngx-datatables/editing/editing.component.ts");
/* harmony import */ var _ngx_datatables_filter_filter_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ngx-datatables/filter/filter.component */ "./src/app/components/tables/ngx-datatables/filter/filter.component.ts");
/* harmony import */ var _ngx_datatables_fullscreen_fullscreen_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ngx-datatables/fullscreen/fullscreen.component */ "./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.ts");
/* harmony import */ var _ngx_datatables_paging_paging_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ngx-datatables/paging/paging.component */ "./src/app/components/tables/ngx-datatables/paging/paging.component.ts");
/* harmony import */ var _smart_table_smart_table_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./smart-table/smart-table.component */ "./src/app/components/tables/smart-table/smart-table.component.ts");
/* harmony import */ var _ngx_datatables_selection_selection_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ngx-datatables/selection/selection.component */ "./src/app/components/tables/ngx-datatables/selection/selection.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var routes = [
    {
        path: '',
        children: [
            {
                path: 'basic',
                component: _bootstrap_tables_basic_basic_component__WEBPACK_IMPORTED_MODULE_2__["BasicComponent"],
                data: {
                    title: "Basic",
                    breadcrumb: "Basic"
                }
            },
            {
                path: 'sizing',
                component: _bootstrap_tables_sizing_sizing_component__WEBPACK_IMPORTED_MODULE_4__["SizingComponent"],
                data: {
                    title: "Sizing",
                    breadcrumb: "Sizing"
                }
            },
            {
                path: 'border',
                component: _bootstrap_tables_border_border_component__WEBPACK_IMPORTED_MODULE_3__["BorderComponent"],
                data: {
                    title: "Border",
                    breadcrumb: "Border"
                }
            },
            {
                path: 'styling',
                component: _bootstrap_tables_styling_styling_component__WEBPACK_IMPORTED_MODULE_5__["StylingComponent"],
                data: {
                    title: "Styling",
                    breadcrumb: "Styling"
                }
            },
            {
                path: 'ngx-datatables/basic',
                component: _ngx_datatables_basic_basic_component__WEBPACK_IMPORTED_MODULE_6__["BasicNgxDatatableComponent"],
                data: {
                    title: "Basic",
                    breadcrumb: "Ngx-Datatables / Basic"
                }
            },
            {
                path: 'ngx-datatables/editing',
                component: _ngx_datatables_editing_editing_component__WEBPACK_IMPORTED_MODULE_7__["EditingComponent"],
                data: {
                    title: "Editing",
                    breadcrumb: "Ngx-Datatables / Editing"
                }
            },
            {
                path: 'ngx-datatables/filter',
                component: _ngx_datatables_filter_filter_component__WEBPACK_IMPORTED_MODULE_8__["FilterNgxComponent"],
                data: {
                    title: "Filter",
                    breadcrumb: "Ngx-Datatables / Filter"
                }
            },
            {
                path: 'ngx-datatables/fullscreen',
                component: _ngx_datatables_fullscreen_fullscreen_component__WEBPACK_IMPORTED_MODULE_9__["FullscreenComponent"],
                data: {
                    title: "FullScreen",
                    breadcrumb: "Ngx-Datatables / FullScreen"
                }
            },
            {
                path: 'ngx-datatables/paging',
                component: _ngx_datatables_paging_paging_component__WEBPACK_IMPORTED_MODULE_10__["PagingComponent"],
                data: {
                    title: "Paging",
                    breadcrumb: "Ngx-Datatables / Paging"
                }
            },
            {
                path: 'ngx-datatables/selection',
                component: _ngx_datatables_selection_selection_component__WEBPACK_IMPORTED_MODULE_12__["SelectionNgxComponent"],
                data: {
                    title: "Selection",
                    breadcrumb: "Ngx-Datatables / Selection"
                }
            },
            {
                path: 'smart-table',
                component: _smart_table_smart_table_component__WEBPACK_IMPORTED_MODULE_11__["SmartTableComponent"],
                data: {
                    title: "Smart Table",
                    breadcrumb: "Smart Table"
                }
            }
        ]
    }
];
var TablesRoutingModule = /** @class */ (function () {
    function TablesRoutingModule() {
    }
    TablesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TablesRoutingModule);
    return TablesRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/tables/tables.module.ts":
/*!****************************************************!*\
  !*** ./src/app/components/tables/tables.module.ts ***!
  \****************************************************/
/*! exports provided: TablesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablesModule", function() { return TablesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tables_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tables-routing.module */ "./src/app/components/tables/tables-routing.module.ts");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _bootstrap_tables_basic_basic_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bootstrap-tables/basic/basic.component */ "./src/app/components/tables/bootstrap-tables/basic/basic.component.ts");
/* harmony import */ var _bootstrap_tables_border_border_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bootstrap-tables/border/border.component */ "./src/app/components/tables/bootstrap-tables/border/border.component.ts");
/* harmony import */ var _bootstrap_tables_sizing_sizing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./bootstrap-tables/sizing/sizing.component */ "./src/app/components/tables/bootstrap-tables/sizing/sizing.component.ts");
/* harmony import */ var _bootstrap_tables_styling_styling_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./bootstrap-tables/styling/styling.component */ "./src/app/components/tables/bootstrap-tables/styling/styling.component.ts");
/* harmony import */ var _ngx_datatables_basic_basic_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ngx-datatables/basic/basic.component */ "./src/app/components/tables/ngx-datatables/basic/basic.component.ts");
/* harmony import */ var _ngx_datatables_editing_editing_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ngx-datatables/editing/editing.component */ "./src/app/components/tables/ngx-datatables/editing/editing.component.ts");
/* harmony import */ var _ngx_datatables_filter_filter_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ngx-datatables/filter/filter.component */ "./src/app/components/tables/ngx-datatables/filter/filter.component.ts");
/* harmony import */ var _ngx_datatables_fullscreen_fullscreen_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ngx-datatables/fullscreen/fullscreen.component */ "./src/app/components/tables/ngx-datatables/fullscreen/fullscreen.component.ts");
/* harmony import */ var _ngx_datatables_paging_paging_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./ngx-datatables/paging/paging.component */ "./src/app/components/tables/ngx-datatables/paging/paging.component.ts");
/* harmony import */ var _ngx_datatables_selection_selection_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ngx-datatables/selection/selection.component */ "./src/app/components/tables/ngx-datatables/selection/selection.component.ts");
/* harmony import */ var _smart_table_smart_table_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./smart-table/smart-table.component */ "./src/app/components/tables/smart-table/smart-table.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var TablesModule = /** @class */ (function () {
    function TablesModule() {
    }
    TablesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_bootstrap_tables_basic_basic_component__WEBPACK_IMPORTED_MODULE_5__["BasicComponent"], _bootstrap_tables_border_border_component__WEBPACK_IMPORTED_MODULE_6__["BorderComponent"], _bootstrap_tables_sizing_sizing_component__WEBPACK_IMPORTED_MODULE_7__["SizingComponent"], _bootstrap_tables_styling_styling_component__WEBPACK_IMPORTED_MODULE_8__["StylingComponent"], _ngx_datatables_basic_basic_component__WEBPACK_IMPORTED_MODULE_9__["BasicNgxDatatableComponent"], _ngx_datatables_editing_editing_component__WEBPACK_IMPORTED_MODULE_10__["EditingComponent"], _ngx_datatables_filter_filter_component__WEBPACK_IMPORTED_MODULE_11__["FilterNgxComponent"], _ngx_datatables_fullscreen_fullscreen_component__WEBPACK_IMPORTED_MODULE_12__["FullscreenComponent"], _ngx_datatables_paging_paging_component__WEBPACK_IMPORTED_MODULE_13__["PagingComponent"], _ngx_datatables_selection_selection_component__WEBPACK_IMPORTED_MODULE_14__["SelectionNgxComponent"], _smart_table_smart_table_component__WEBPACK_IMPORTED_MODULE_15__["SmartTableComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _tables_routing_module__WEBPACK_IMPORTED_MODULE_2__["TablesRoutingModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__["NgxDatatableModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["Ng2SmartTableModule"]
            ]
        })
    ], TablesModule);
    return TablesModule;
}());



/***/ }),

/***/ "./src/app/shared/data/tables/company.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/data/tables/company.ts ***!
  \***********************************************/
/*! exports provided: companyDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "companyDB", function() { return companyDB; });
var companyDB = /** @class */ (function () {
    function companyDB() {
    }
    companyDB.data = [
        {
            "name": "Ethel Price",
            "gender": "female",
            "company": "Johnson, Johnson and Partners, LLC CMP DDC",
            "age": 22
        },
        {
            "name": "Claudine Neal",
            "gender": "female",
            "company": "Sealoud",
            "age": 55
        },
        {
            "name": "Beryl Rice",
            "gender": "female",
            "company": "Velity",
            "age": 67
        },
        {
            "name": "Wilder Gonzales",
            "gender": "male",
            "company": "Geekko"
        },
        {
            "name": "Georgina Schultz",
            "gender": "female",
            "company": "Suretech"
        },
        {
            "name": "Carroll Buchanan",
            "gender": "male",
            "company": "Ecosys"
        },
        {
            "name": "Valarie Atkinson",
            "gender": "female",
            "company": "Hopeli"
        },
        {
            "name": "Schroeder Mathews",
            "gender": "male",
            "company": "Polarium"
        },
        {
            "name": "Lynda Mendoza",
            "gender": "female",
            "company": "Dogspa"
        },
        {
            "name": "Sarah Massey",
            "gender": "female",
            "company": "Bisba"
        },
        {
            "name": "Robles Boyle",
            "gender": "male",
            "company": "Comtract"
        },
        {
            "name": "Evans Hickman",
            "gender": "male",
            "company": "Parleynet"
        },
        {
            "name": "Dawson Barber",
            "gender": "male",
            "company": "Dymi"
        },
        {
            "name": "Bruce Strong",
            "gender": "male",
            "company": "Xyqag"
        },
        {
            "name": "Nellie Whitfield",
            "gender": "female",
            "company": "Exospace"
        },
        {
            "name": "Jackson Macias",
            "gender": "male",
            "company": "Aquamate"
        },
        {
            "name": "Pena Pena",
            "gender": "male",
            "company": "Quarx"
        },
        {
            "name": "Lelia Gates",
            "gender": "female",
            "company": "Proxsoft"
        },
        {
            "name": "Letitia Vasquez",
            "gender": "female",
            "company": "Slumberia"
        },
        {
            "name": "Trevino Moreno",
            "gender": "male",
            "company": "Conjurica"
        },
        {
            "name": "Barr Page",
            "gender": "male",
            "company": "Apex"
        },
        {
            "name": "Kirkland Merrill",
            "gender": "male",
            "company": "Utara"
        },
        {
            "name": "Blanche Conley",
            "gender": "female",
            "company": "Imkan"
        }
    ];
    return companyDB;
}());



/***/ }),

/***/ "./src/app/shared/data/tables/employee.ts":
/*!************************************************!*\
  !*** ./src/app/shared/data/tables/employee.ts ***!
  \************************************************/
/*! exports provided: employeeDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employeeDB", function() { return employeeDB; });
var employeeDB = /** @class */ (function () {
    function employeeDB() {
    }
    employeeDB.emp = [
        {
            "id": 0,
            "name": "Ramsey Cummings",
            "gender": "male",
            "age": 52,
            "address": {
                "state": "South Carolina",
                "city": "Glendale"
            }
        },
        {
            "id": 1,
            "name": "Stefanie Huff",
            "gender": "female",
            "age": 70,
            "address": {
                "state": "Arizona",
                "city": "Beaverdale"
            }
        },
        {
            "id": 2,
            "name": "Mabel David",
            "gender": "female",
            "age": 52,
            "address": {
                "state": "New Mexico",
                "city": "Grazierville"
            }
        },
        {
            "id": 3,
            "name": "Frank Bradford",
            "gender": "male",
            "age": 61,
            "address": {
                "state": "Wisconsin",
                "city": "Saranap"
            }
        },
        {
            "id": 4,
            "name": "Forbes Levine",
            "gender": "male",
            "age": 34,
            "address": {
                "state": "Vermont",
                "city": "Norris"
            }
        },
        {
            "id": 5,
            "name": "Santiago Mcclain",
            "gender": "male",
            "age": 38,
            "address": {
                "state": "Montana",
                "city": "Bordelonville"
            }
        },
        {
            "id": 6,
            "name": "Merritt Booker",
            "gender": "male",
            "age": 33,
            "address": {
                "state": "New Jersey",
                "city": "Aguila"
            }
        },
        {
            "id": 7,
            "name": "Oconnor Wade",
            "gender": "male",
            "age": 18,
            "address": {
                "state": "Virginia",
                "city": "Kenmar"
            }
        },
        {
            "id": 8,
            "name": "Leigh Beasley",
            "gender": "female",
            "age": 53,
            "address": {
                "state": "Texas",
                "city": "Alfarata"
            }
        },
        {
            "id": 9,
            "name": "Johns Wood",
            "gender": "male",
            "age": 52,
            "address": {
                "state": "Maine",
                "city": "Witmer"
            }
        },
        {
            "id": 10,
            "name": "Thompson Hays",
            "gender": "male",
            "age": 38,
            "address": {
                "state": "Nevada",
                "city": "Kipp"
            }
        },
        {
            "id": 11,
            "name": "Hallie Mack",
            "gender": "female",
            "age": 19,
            "address": {
                "state": "Minnesota",
                "city": "Darrtown"
            }
        },
        {
            "id": 12,
            "name": "Houston Santos",
            "gender": "male",
            "age": 24,
            "address": {
                "state": "Georgia",
                "city": "Crucible"
            }
        },
        {
            "id": 13,
            "name": "Brandy Savage",
            "gender": "female",
            "age": 65,
            "address": {
                "state": "Idaho",
                "city": "Nord"
            }
        },
        {
            "id": 14,
            "name": "Finch Barnett",
            "gender": "male",
            "age": 22,
            "address": {
                "state": "Ohio",
                "city": "Osmond"
            }
        },
        {
            "id": 15,
            "name": "Nicole Crosby",
            "gender": "female",
            "age": 77,
            "address": {
                "state": "Kentucky",
                "city": "Fairfield"
            }
        },
        {
            "id": 16,
            "name": "Carrie Mcconnell",
            "gender": "female",
            "age": 26,
            "address": {
                "state": "South Dakota",
                "city": "Waikele"
            }
        },
        {
            "id": 17,
            "name": "Ann James",
            "gender": "female",
            "age": 37,
            "address": {
                "state": "North Dakota",
                "city": "Siglerville"
            }
        },
        {
            "id": 18,
            "name": "Becky Sanford",
            "gender": "female",
            "age": 48,
            "address": {
                "state": "Massachusetts",
                "city": "Celeryville"
            }
        },
        {
            "id": 19,
            "name": "Kathryn Rios",
            "gender": "female",
            "age": 39,
            "address": {
                "state": "Delaware",
                "city": "Kylertown"
            }
        },
        {
            "id": 20,
            "name": "Dotson Vaughn",
            "gender": "male",
            "age": 68,
            "address": {
                "state": "Arkansas",
                "city": "Monument"
            }
        },
        {
            "id": 21,
            "name": "Wright Kline",
            "gender": "male",
            "age": 41,
            "address": {
                "state": "Missouri",
                "city": "Bynum"
            }
        },
        {
            "id": 22,
            "name": "Lula Morgan",
            "gender": "female",
            "age": 52,
            "address": {
                "state": "Oregon",
                "city": "Mapletown"
            }
        },
        {
            "id": 23,
            "name": "Kay Mendez",
            "gender": "female",
            "age": 50,
            "address": {
                "state": "Michigan",
                "city": "Twilight"
            }
        },
        {
            "id": 24,
            "name": "Mona Maddox",
            "gender": "female",
            "age": 35,
            "address": {
                "state": "Wyoming",
                "city": "Wilmington"
            }
        },
        {
            "id": 25,
            "name": "Fulton Velez",
            "gender": "male",
            "age": 66,
            "address": {
                "state": "Colorado",
                "city": "Loretto"
            }
        },
        {
            "id": 26,
            "name": "Ericka Craft",
            "gender": "female",
            "age": 80,
            "address": {
                "state": "Nebraska",
                "city": "Beaulieu"
            }
        },
        {
            "id": 27,
            "name": "Richmond Rodriguez",
            "gender": "male",
            "age": 62,
            "address": {
                "state": "Rhode Island",
                "city": "Vallonia"
            }
        },
        {
            "id": 28,
            "name": "Olsen Farmer",
            "gender": "male",
            "age": 45,
            "address": {
                "state": "Connecticut",
                "city": "Romeville"
            }
        },
        {
            "id": 29,
            "name": "Sophie Austin",
            "gender": "female",
            "age": 59,
            "address": {
                "state": "New Hampshire",
                "city": "Gorst"
            }
        },
        {
            "id": 30,
            "name": "Alta Olsen",
            "gender": "female",
            "age": 58,
            "address": {
                "state": "Florida",
                "city": "Drytown"
            }
        },
        {
            "id": 31,
            "name": "Katherine Chavez",
            "gender": "female",
            "age": 20,
            "address": {
                "state": "Mississippi",
                "city": "Trucksville"
            }
        },
        {
            "id": 32,
            "name": "Yvette Myers",
            "gender": "female",
            "age": 69,
            "address": {
                "state": "Washington",
                "city": "Bangor"
            }
        },
        {
            "id": 33,
            "name": "Nguyen Dean",
            "gender": "male",
            "age": 58,
            "address": {
                "state": "Alaska",
                "city": "Caroline"
            }
        },
        {
            "id": 34,
            "name": "Lauri Irwin",
            "gender": "female",
            "age": 23,
            "address": {
                "state": "Hawaii",
                "city": "Takilma"
            }
        },
        {
            "id": 35,
            "name": "David Mclean",
            "gender": "male",
            "age": 49,
            "address": {
                "state": "Louisiana",
                "city": "Harviell"
            }
        },
        {
            "id": 36,
            "name": "Tisha Cotton",
            "gender": "female",
            "age": 78,
            "address": {
                "state": "Illinois",
                "city": "Camas"
            }
        },
        {
            "id": 37,
            "name": "Eliza Conrad",
            "gender": "female",
            "age": 82,
            "address": {
                "state": "Utah",
                "city": "Gwynn"
            }
        },
        {
            "id": 38,
            "name": "Bolton Cooley",
            "gender": "male",
            "age": 44,
            "address": {
                "state": "Oklahoma",
                "city": "Glidden"
            }
        },
        {
            "id": 39,
            "name": "Crosby Osborn",
            "gender": "male",
            "age": 44,
            "address": {
                "state": "Alabama",
                "city": "Buxton"
            }
        },
        {
            "id": 40,
            "name": "Reese Tran",
            "gender": "male",
            "age": 53,
            "address": {
                "state": "Maryland",
                "city": "Kempton"
            }
        },
        {
            "id": 41,
            "name": "Evangeline Larson",
            "gender": "female",
            "age": 49,
            "address": {
                "state": "Pennsylvania",
                "city": "Mayfair"
            }
        },
        {
            "id": 42,
            "name": "Jimenez Frazier",
            "gender": "male",
            "age": 23,
            "address": {
                "state": "California",
                "city": "Ronco"
            }
        },
        {
            "id": 43,
            "name": "Conner Tate",
            "gender": "male",
            "age": 39,
            "address": {
                "state": "West Virginia",
                "city": "Eastvale"
            }
        },
        {
            "id": 44,
            "name": "Avery Rosales",
            "gender": "male",
            "age": 71,
            "address": {
                "state": "Tennessee",
                "city": "Cascades"
            }
        },
        {
            "id": 45,
            "name": "Burris Marquez",
            "gender": "male",
            "age": 32,
            "address": {
                "state": "North Carolina",
                "city": "Chamizal"
            }
        },
        {
            "id": 46,
            "name": "Hoover Cardenas",
            "gender": "male",
            "age": 65,
            "address": {
                "state": "Kansas",
                "city": "Joes"
            }
        },
        {
            "id": 47,
            "name": "Moran Gomez",
            "gender": "male",
            "age": 40,
            "address": {
                "state": "New York",
                "city": "Knowlton"
            }
        },
        {
            "id": 48,
            "name": "Boyd Juarez",
            "gender": "male",
            "age": 33,
            "address": {
                "state": "Iowa",
                "city": "Hemlock"
            }
        },
        {
            "id": 49,
            "name": "John Mooney",
            "gender": "female",
            "age": 73,
            "address": {
                "state": "Rhode Island",
                "city": "Gardners"
            }
        },
        {
            "id": 50,
            "name": "Avery Crawford",
            "gender": "male",
            "age": 39,
            "address": {
                "state": "Hawaii",
                "city": "Woodruff"
            }
        }
    ];
    return employeeDB;
}());



/***/ })

}]);
//# sourceMappingURL=components-tables-tables-module.js.map