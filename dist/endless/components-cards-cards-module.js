(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-cards-cards-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/cards/basic/basic.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/cards/basic/basic.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Basic Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the\r\n                        1500s, when an unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card b-r-0\">\r\n                <div class=\"card-header\">\r\n                    <h5>Flat Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the\r\n                        1500s, when an unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card shadow-0 border\">\r\n                <div class=\"card-header\">\r\n                    <h5>Without shadow Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5><i class=\"icofont icofont-truck mr-2\"></i> Icon in Heading</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card height-equal\">\r\n                <div class=\"card-header\">\r\n                    <h5>Card sub Title</h5>\r\n                    <span>Using the <a href=\"javascript:void(0)\">card</a> component, you can extend the default collapse\r\n                        behavior to create an accordion.</span>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card height-equal\">\r\n                <div class=\"card-header\">\r\n                    <h5>Card With Footer</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the the industry's standard dummy text ever. </p>\r\n                </div>\r\n                <div class=\"card-footer\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header bg-primary\">\r\n                    <h5>Primary Color Card</h5>\r\n                </div>\r\n                <div class=\"card-body bg-primary\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer bg-primary\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header bg-secondary\">\r\n                    <h5>Secondary Color Card</h5>\r\n                </div>\r\n                <div class=\"card-body bg-secondary\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer bg-secondary\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header bg-success\">\r\n                    <h5>Success color Card</h5>\r\n                </div>\r\n                <div class=\"card-body bg-success\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer bg-success\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header bg-primary\">\r\n                    <h5>Primary Color Header</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Primary Color Body</h5>\r\n                </div>\r\n                <div class=\"card-body bg-primary\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Primary Color Footer</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n                <div class=\"card-footer bg-primary\">\r\n                    <h6 class=\"mb-0\">Card Footer</h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/cards/creative/creative.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/cards/creative/creative.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n\t<div class=\"row\">\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card \">\r\n\t\t\t\t<div class=\"card-header b-l-primary\">\r\n\t\t\t\t\t<h5>Border left</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card \">\r\n\t\t\t\t<div class=\"card-header b-r-secondary\">\r\n\t\t\t\t\t<h5>Border right</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card \">\r\n\t\t\t\t<div class=\"card-header b-t-success\">\r\n\t\t\t\t\t<h5>Border top</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card \">\r\n\t\t\t\t<div class=\"card-header b-b-info\">\r\n\t\t\t\t\t<h5>Border bottom</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-warning\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-danger\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-light\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-primary\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-secondary border-2\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header b-l-primary border-3\">\r\n\t\t\t\t\t<h5>Border color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card card-absolute\">\r\n\t\t\t\t<div class=\"card-header bg-primary\">\r\n\t\t\t\t\t<h5>Absolute Style</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-sm-12 col-xl-6\">\r\n\t\t\t<div class=\"card card-absolute\">\r\n\t\t\t\t<div class=\"card-header bg-secondary\">\r\n\t\t\t\t\t<h5>Color state</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n\t\t\t\t\t\tthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n\t\t\t\t\t\tof type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n\t\t\t\t\t\tindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\tunknown printer took a galley of type and scrambled.</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/cards/draggable/draggable.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/cards/draggable/draggable.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row ui-sortable\" [dragula]='\"drag-card\"'>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Basic Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card b-r-0\">\r\n                <div class=\"card-header\">\r\n                    <h5>Flat Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card shadow-0 border\">\r\n                <div class=\"card-header\">\r\n                    <h5>Without shadow Card</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5><i class=\"icon-move mr-2\"></i> Icon in Heading</h5>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been\r\n                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n                        of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting\r\n                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an\r\n                        unknown printer took a galley of type and scrambled. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Card sub Title</h5>\r\n                    <span>Using the <a href=\"javascript:void(0)\">card</a> component, you can extend the default collapse\r\n                        behavior to create an accordion.</span>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-xl-6\">\r\n            <div class=\"card\">\r\n                <div class=\"card-header\">\r\n                    <h5>Card sub Title</h5>\r\n                    <span>Using the <a href=\"javascript:void(0)\">card</a> component, you can extend the default collapse\r\n                        behavior to create an accordion.</span>\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    <p class=\"mb-0\"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer\r\n                        took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and\r\n                        typesetting industry. Lorem Ipsum has been the. </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/cards/tabbed/tabbed.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/cards/tabbed/tabbed.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts -->\r\n<div class=\"container-fluid\">\r\n\t<div class=\"row\">\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Simple Tab</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset type=\"pills\" class=\"tabbed-card\">\r\n\t\t\t\t\t\t<ngb-tab title=\"Home\" class=\"pull-right nav nav-pills nav-primary\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever\r\n\t\t\t\t\t\t\t\t\t\tsince the 1500s, when an unknown printer took a galley of type and scrambled it\r\n\t\t\t\t\t\t\t\t\t\tto make a type specimen book. It has survived not only five centuries, but also\r\n\t\t\t\t\t\t\t\t\t\tthe leap into electronic typesetting, remaining essentially unchanged. It was\r\n\t\t\t\t\t\t\t\t\t\tpopularised in the 1960s with the release of Letraset sheets containing Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum passages, and more recently with desktop publishing software like Aldus\r\n\t\t\t\t\t\t\t\t\t\tPageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-primary\">Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It\r\n\t\t\t\t\t\t\t\t\t\thas survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab title=\"Contact\" ngbTabTitle class=\"pull-right nav nav-pills nav-primary\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen\r\n\t\t\t\t\t\t\t\t\t\tbook. It has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Tab With Icon</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset type=\"pills\" class=\"tabbed-card\">\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-primary\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-ui-home\"></i>Home</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever\r\n\t\t\t\t\t\t\t\t\t\tsince the 1500s, when an unknown printer took a galley of type and scrambled it\r\n\t\t\t\t\t\t\t\t\t\tto make a type specimen book. It has survived not only five centuries, but also\r\n\t\t\t\t\t\t\t\t\t\tthe leap into electronic typesetting, remaining essentially unchanged. It was\r\n\t\t\t\t\t\t\t\t\t\tpopularised in the 1960s with the release of Letraset sheets containing Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum passages, and more recently with desktop publishing software like Aldus\r\n\t\t\t\t\t\t\t\t\t\tPageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-primary\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-man-in-glasses\"></i>Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It\r\n\t\t\t\t\t\t\t\t\t\thas survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-primary\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-contacts\"></i>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen\r\n\t\t\t\t\t\t\t\t\t\tbook. It has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Color Tab</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset type=\"pills\" class=\"tabbed-card\">\r\n\t\t\t\t\t\t<ngb-tab title=\"Home\" class=\"pull-right nav nav-pills nav-primary\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever\r\n\t\t\t\t\t\t\t\t\t\tsince the 1500s, when an unknown printer took a galley of type and scrambled it\r\n\t\t\t\t\t\t\t\t\t\tto make a type specimen book. It has survived not only five centuries, but also\r\n\t\t\t\t\t\t\t\t\t\tthe leap into electronic typesetting, remaining essentially unchanged. It was\r\n\t\t\t\t\t\t\t\t\t\tpopularised in the 1960s with the release of Letraset sheets containing Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum passages, and more recently with desktop publishing software like Aldus\r\n\t\t\t\t\t\t\t\t\t\tPageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-primary\">Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It\r\n\t\t\t\t\t\t\t\t\t\thas survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab title=\"Contact\" ngbTabTitle class=\"pull-right nav nav-pills nav-primary\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen\r\n\t\t\t\t\t\t\t\t\t\tbook. It has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Color Option</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset type=\"pills\" class=\"tabbed-card nav-secondary\">\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-ui-home\"></i>Home</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever\r\n\t\t\t\t\t\t\t\t\t\tsince the 1500s, when an unknown printer took a galley of type and scrambled it\r\n\t\t\t\t\t\t\t\t\t\tto make a type specimen book. It has survived not only five centuries, but also\r\n\t\t\t\t\t\t\t\t\t\tthe leap into electronic typesetting, remaining essentially unchanged. It was\r\n\t\t\t\t\t\t\t\t\t\tpopularised in the 1960s with the release of Letraset sheets containing Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum passages, and more recently with desktop publishing software like Aldus\r\n\t\t\t\t\t\t\t\t\t\tPageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-secondary\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-man-in-glasses\"></i>Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It\r\n\t\t\t\t\t\t\t\t\t\thas survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"pull-right nav nav-pills nav-secondary\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-contacts\"></i>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent class=\"tab-content\">\r\n\t\t\t\t\t\t\t\t<div class=\"tab-pane fade show\">\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen\r\n\t\t\t\t\t\t\t\t\t\tbook. It has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s\r\n\t\t\t\t\t\t\t\t\t\twith the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including\r\n\t\t\t\t\t\t\t\t\t\tversions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Material tab</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset #t=\"ngbTabset\">\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid1\" title=\"Home\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever since\r\n\t\t\t\t\t\t\t\t\tthe 1500s, when an unknown printer took a galley of type and scrambled it to make a\r\n\t\t\t\t\t\t\t\t\ttype specimen book. It has survived not only five centuries, but also the leap into\r\n\t\t\t\t\t\t\t\t\telectronic typesetting, remaining essentially unchanged. It was popularised in the\r\n\t\t\t\t\t\t\t\t\t1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including versions of\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid2\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle>Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It has\r\n\t\t\t\t\t\t\t\t\tsurvived not only five centuries, but also the leap into electronic typesetting,\r\n\t\t\t\t\t\t\t\t\tremaining essentially unchanged. It was popularised in the 1960s with the release of\r\n\t\t\t\t\t\t\t\t\tLetraset sheets containing Lorem Ipsum passages, and more recently with desktop\r\n\t\t\t\t\t\t\t\t\tpublishing software like Aldus PageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid3\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\t\t\t\t\t\t\t\t\tIt has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s with\r\n\t\t\t\t\t\t\t\t\tthe release of Letraset sheets containing Lorem Ipsum passages, and more recently\r\n\t\t\t\t\t\t\t\t\twith desktop publishing software like Aldus PageMaker including versions of Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Material tab with icon</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset #t=\"ngbTabset\">\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid1\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav-material\"><i class=\"icofont icofont-ui-home\"></i>Home\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever since\r\n\t\t\t\t\t\t\t\t\tthe 1500s, when an unknown printer took a galley of type and scrambled it to make a\r\n\t\t\t\t\t\t\t\t\ttype specimen book. It has survived not only five centuries, but also the leap into\r\n\t\t\t\t\t\t\t\t\telectronic typesetting, remaining essentially unchanged. It was popularised in the\r\n\t\t\t\t\t\t\t\t\t1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including versions of\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid2\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav-material\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-man-in-glasses\"></i>Profile</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It has\r\n\t\t\t\t\t\t\t\t\tsurvived not only five centuries, but also the leap into electronic typesetting,\r\n\t\t\t\t\t\t\t\t\tremaining essentially unchanged. It was popularised in the 1960s with the release of\r\n\t\t\t\t\t\t\t\t\tLetraset sheets containing Lorem Ipsum passages, and more recently with desktop\r\n\t\t\t\t\t\t\t\t\tpublishing software like Aldus PageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid3\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav-material\"><i\r\n\t\t\t\t\t\t\t\t\tclass=\"icofont icofont-contacts\"></i>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\t\t\t\t\t\t\t\t\tIt has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s with\r\n\t\t\t\t\t\t\t\t\tthe release of Letraset sheets containing Lorem Ipsum passages, and more recently\r\n\t\t\t\t\t\t\t\t\twith desktop publishing software like Aldus PageMaker including versions of Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Material tab with color</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset #t=\"ngbTabset\" class=\"nav-secondary\">\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid1\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-ui-home\"></i>Home\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever since\r\n\t\t\t\t\t\t\t\t\tthe 1500s, when an unknown printer took a galley of type and scrambled it to make a\r\n\t\t\t\t\t\t\t\t\ttype specimen book. It has survived not only five centuries, but also the leap into\r\n\t\t\t\t\t\t\t\t\telectronic typesetting, remaining essentially unchanged. It was popularised in the\r\n\t\t\t\t\t\t\t\t\t1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including versions of\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid2\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-man-in-glasses\"></i>Profile\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It has\r\n\t\t\t\t\t\t\t\t\tsurvived not only five centuries, but also the leap into electronic typesetting,\r\n\t\t\t\t\t\t\t\t\tremaining essentially unchanged. It was popularised in the 1960s with the release of\r\n\t\t\t\t\t\t\t\t\tLetraset sheets containing Lorem Ipsum passages, and more recently with desktop\r\n\t\t\t\t\t\t\t\t\tpublishing software like Aldus PageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid3\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-contacts\"></i>Contact\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\t\t\t\t\t\t\t\t\tIt has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s with\r\n\t\t\t\t\t\t\t\t\tthe release of Letraset sheets containing Lorem Ipsum passages, and more recently\r\n\t\t\t\t\t\t\t\t\twith desktop publishing software like Aldus PageMaker including versions of Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"col-xl-6 xl-100 col-lg-12\">\r\n\t\t\t<div class=\"card\">\r\n\t\t\t\t<div class=\"card-header\">\r\n\t\t\t\t\t<h5 class=\"pull-left\">Material tab with color</h5>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t<ngb-tabset #t=\"ngbTabset\" class=\"nav-success\">\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid1\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-ui-home\"></i>Home\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum is simply dummy text of the printing and typesetting industry. text ever since\r\n\t\t\t\t\t\t\t\t\tthe 1500s, when an unknown printer took a galley of type and scrambled it to make a\r\n\t\t\t\t\t\t\t\t\ttype specimen book. It has survived not only five centuries, but also the leap into\r\n\t\t\t\t\t\t\t\t\telectronic typesetting, remaining essentially unchanged. It was popularised in the\r\n\t\t\t\t\t\t\t\t\t1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\r\n\t\t\t\t\t\t\t\t\trecently with desktop publishing software like Aldus PageMaker including versions of\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid2\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-man-in-glasses\"></i>Profile\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. dummy\r\n\t\t\t\t\t\t\t\t\ttext of the printing and typesetting industry. since the 1500s, when an unknown\r\n\t\t\t\t\t\t\t\t\tprinter took a galley of type and scrambled it to make a type specimen book. It has\r\n\t\t\t\t\t\t\t\t\tsurvived not only five centuries, but also the leap into electronic typesetting,\r\n\t\t\t\t\t\t\t\t\tremaining essentially unchanged. It was popularised in the 1960s with the release of\r\n\t\t\t\t\t\t\t\t\tLetraset sheets containing Lorem Ipsum passages, and more recently with desktop\r\n\t\t\t\t\t\t\t\t\tpublishing software like Aldus PageMaker including versions of Lorem Ipsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t\t<ngb-tab id=\"tab-selectbyid3\">\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle class=\"nav\"><i class=\"icofont icofont-contacts\"></i>Contact\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabTitle>Contact</ng-template>\r\n\t\t\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum has been the industry's standard dummy text ever since the 1500s, when an\r\n\t\t\t\t\t\t\t\t\tunknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\t\t\t\t\t\t\t\t\tIt has survived not only five centuries, but also the leap into electronic\r\n\t\t\t\t\t\t\t\t\ttypesetting, remaining essentially unchanged. It was popularised in the 1960s with\r\n\t\t\t\t\t\t\t\t\tthe release of Letraset sheets containing Lorem Ipsum passages, and more recently\r\n\t\t\t\t\t\t\t\t\twith desktop publishing software like Aldus PageMaker including versions of Lorem\r\n\t\t\t\t\t\t\t\t\tIpsum\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t</ngb-tab>\r\n\t\t\t\t\t</ngb-tabset>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<!-- Container-fluid Ends -->"

/***/ }),

/***/ "./src/app/components/cards/basic/basic.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/cards/basic/basic.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZHMvYmFzaWMvYmFzaWMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/cards/basic/basic.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/cards/basic/basic.component.ts ***!
  \***********************************************************/
/*! exports provided: BasicComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicComponent", function() { return BasicComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BasicComponent = /** @class */ (function () {
    function BasicComponent() {
    }
    BasicComponent.prototype.ngOnInit = function () { };
    BasicComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basic',
            template: __webpack_require__(/*! raw-loader!./basic.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/cards/basic/basic.component.html"),
            styles: [__webpack_require__(/*! ./basic.component.scss */ "./src/app/components/cards/basic/basic.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BasicComponent);
    return BasicComponent;
}());



/***/ }),

/***/ "./src/app/components/cards/cards-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/cards/cards-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: CardsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsRoutingModule", function() { return CardsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _basic_basic_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/basic.component */ "./src/app/components/cards/basic/basic.component.ts");
/* harmony import */ var _creative_creative_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./creative/creative.component */ "./src/app/components/cards/creative/creative.component.ts");
/* harmony import */ var _tabbed_tabbed_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tabbed/tabbed.component */ "./src/app/components/cards/tabbed/tabbed.component.ts");
/* harmony import */ var _draggable_draggable_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./draggable/draggable.component */ "./src/app/components/cards/draggable/draggable.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        children: [
            {
                path: 'basic',
                component: _basic_basic_component__WEBPACK_IMPORTED_MODULE_2__["BasicComponent"],
                data: {
                    title: "Basic",
                    breadcrumb: "Basic"
                }
            },
            {
                path: 'creative',
                component: _creative_creative_component__WEBPACK_IMPORTED_MODULE_3__["CreativeComponent"],
                data: {
                    title: "Creative",
                    breadcrumb: "Creative"
                }
            },
            {
                path: 'tabbed',
                component: _tabbed_tabbed_component__WEBPACK_IMPORTED_MODULE_4__["TabbedComponent"],
                data: {
                    title: "Tabbed",
                    breadcrumb: "Tabbed"
                }
            },
            {
                path: 'draggable',
                component: _draggable_draggable_component__WEBPACK_IMPORTED_MODULE_5__["DraggableComponent"],
                data: {
                    title: "Draggable",
                    breadcrumb: "Draggable"
                }
            }
        ]
    }
];
var CardsRoutingModule = /** @class */ (function () {
    function CardsRoutingModule() {
    }
    CardsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CardsRoutingModule);
    return CardsRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/cards/cards.module.ts":
/*!**************************************************!*\
  !*** ./src/app/components/cards/cards.module.ts ***!
  \**************************************************/
/*! exports provided: CardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsModule", function() { return CardsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _cards_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cards-routing.module */ "./src/app/components/cards/cards-routing.module.ts");
/* harmony import */ var ng2_dragula__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-dragula */ "./node_modules/ng2-dragula/dist/fesm5/ng2-dragula.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _basic_basic_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./basic/basic.component */ "./src/app/components/cards/basic/basic.component.ts");
/* harmony import */ var _creative_creative_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./creative/creative.component */ "./src/app/components/cards/creative/creative.component.ts");
/* harmony import */ var _tabbed_tabbed_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tabbed/tabbed.component */ "./src/app/components/cards/tabbed/tabbed.component.ts");
/* harmony import */ var _draggable_draggable_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./draggable/draggable.component */ "./src/app/components/cards/draggable/draggable.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var CardsModule = /** @class */ (function () {
    function CardsModule() {
    }
    CardsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_basic_basic_component__WEBPACK_IMPORTED_MODULE_5__["BasicComponent"], _creative_creative_component__WEBPACK_IMPORTED_MODULE_6__["CreativeComponent"], _tabbed_tabbed_component__WEBPACK_IMPORTED_MODULE_7__["TabbedComponent"], _draggable_draggable_component__WEBPACK_IMPORTED_MODULE_8__["DraggableComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _cards_routing_module__WEBPACK_IMPORTED_MODULE_2__["CardsRoutingModule"],
                ng2_dragula__WEBPACK_IMPORTED_MODULE_3__["DragulaModule"].forRoot(),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"]
            ]
        })
    ], CardsModule);
    return CardsModule;
}());



/***/ }),

/***/ "./src/app/components/cards/creative/creative.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/cards/creative/creative.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZHMvY3JlYXRpdmUvY3JlYXRpdmUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/cards/creative/creative.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/cards/creative/creative.component.ts ***!
  \*****************************************************************/
/*! exports provided: CreativeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreativeComponent", function() { return CreativeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CreativeComponent = /** @class */ (function () {
    function CreativeComponent() {
    }
    CreativeComponent.prototype.ngOnInit = function () { };
    CreativeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-creative',
            template: __webpack_require__(/*! raw-loader!./creative.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/cards/creative/creative.component.html"),
            styles: [__webpack_require__(/*! ./creative.component.scss */ "./src/app/components/cards/creative/creative.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CreativeComponent);
    return CreativeComponent;
}());



/***/ }),

/***/ "./src/app/components/cards/draggable/draggable.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/cards/draggable/draggable.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZHMvZHJhZ2dhYmxlL2RyYWdnYWJsZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/cards/draggable/draggable.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/cards/draggable/draggable.component.ts ***!
  \*******************************************************************/
/*! exports provided: DraggableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DraggableComponent", function() { return DraggableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DraggableComponent = /** @class */ (function () {
    function DraggableComponent() {
    }
    DraggableComponent.prototype.ngOnInit = function () { };
    DraggableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-draggable',
            template: __webpack_require__(/*! raw-loader!./draggable.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/cards/draggable/draggable.component.html"),
            styles: [__webpack_require__(/*! ./draggable.component.scss */ "./src/app/components/cards/draggable/draggable.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DraggableComponent);
    return DraggableComponent;
}());



/***/ }),

/***/ "./src/app/components/cards/tabbed/tabbed.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/cards/tabbed/tabbed.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZHMvdGFiYmVkL3RhYmJlZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/cards/tabbed/tabbed.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/cards/tabbed/tabbed.component.ts ***!
  \*************************************************************/
/*! exports provided: TabbedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabbedComponent", function() { return TabbedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TabbedComponent = /** @class */ (function () {
    function TabbedComponent() {
    }
    TabbedComponent.prototype.ngOnInit = function () { };
    TabbedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tabbed',
            template: __webpack_require__(/*! raw-loader!./tabbed.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/cards/tabbed/tabbed.component.html"),
            styles: [__webpack_require__(/*! ./tabbed.component.scss */ "./src/app/components/cards/tabbed/tabbed.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TabbedComponent);
    return TabbedComponent;
}());



/***/ })

}]);
//# sourceMappingURL=components-cards-cards-module.js.map