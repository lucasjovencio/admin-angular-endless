(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-users-users-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/users/user-cards/user-cards.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/users/user-cards/user-cards.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/1.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/3.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Mark Jecno</h4>\r\n          <h6>Manager</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">9564</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">49</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">96</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/2.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/16.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Johan Deo</h4>\r\n          <h6>Designer</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">2578</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">26</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">96</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/3.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/11.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Dev John</h4>\r\n          <h6>Devloper</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">6545</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">91</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">21</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/7.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/16.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Johan Deo</h4>\r\n          <h6>Designer</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">2578</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">26</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">96</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/5.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/11.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Dev John</h4>\r\n          <h6>Devloper</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">6545</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">91</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">21</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-6 col-xl-4\">\r\n      <div class=\"card custom-card\">\r\n        <div class=\"card-header\"><img class=\"img-fluid\" src=\"assets/images/user-card/6.jpg\" alt=\"\"></div>\r\n        <div class=\"card-profile\"><img class=\"rounded-circle\" src=\"assets/images/avtar/3.jpg\" alt=\"\"></div>\r\n        <ul class=\"card-social\">\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n        </ul>\r\n        <div class=\"text-center profile-details\">\r\n          <h4>Mark Jecno</h4>\r\n          <h6>Manager</h6>\r\n        </div>\r\n        <div class=\"card-footer row\">\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Follower</h6>\r\n            <h3 class=\"counter\">9564</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Following</h6>\r\n            <h3><span class=\"counter\">49</span>K</h3>\r\n          </div>\r\n          <div class=\"col-4 col-sm-4\">\r\n            <h6>Total Post</h6>\r\n            <h3><span class=\"counter\">96</span>M</h3>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/users/user-edit/user-edit.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/users/user-edit/user-edit.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"edit-profile\">\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-4\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h4 class=\"card-title mb-0\">My Profile</h4>\r\n            <div class=\"card-options\"><a class=\"card-options-collapse\" [routerLink]=\"\" data-toggle=\"card-collapse\"><i\r\n                  class=\"fe fe-chevron-up\"></i></a><a class=\"card-options-remove\" [routerLink]=\"\"\r\n                data-toggle=\"card-remove\"><i class=\"fe fe-x\"></i></a></div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <form>\r\n              <div class=\"row mb-2\">\r\n                <div class=\"col-auto\"><img class=\"img-70 rounded-circle\" alt=\"\" src=\"assets/images/user/7.jpg\"></div>\r\n                <div class=\"col\">\r\n                  <h3 class=\"mb-1\">MARK JECNO</h3>\r\n                  <p class=\"mb-4\">DESIGNER</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <h6 class=\"form-label\">Bio</h6>\r\n                <textarea class=\"form-control\"\r\n                  rows=\"5\">On the other hand, we denounce with righteous indignation</textarea>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"form-label\">Email-Address</label>\r\n                <input class=\"form-control\" placeholder=\"your-email@domain.com\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"form-label\">Password</label>\r\n                <input class=\"form-control\" type=\"password\" value=\"password\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"form-label\">Website</label>\r\n                <input class=\"form-control\" placeholder=\"http://Uplor.com\">\r\n              </div>\r\n              <div class=\"form-footer\">\r\n                <button class=\"btn btn-primary btn-block\">Save</button>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-8\">\r\n        <form class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h4 class=\"card-title mb-0\">Edit Profile</h4>\r\n            <div class=\"card-options\"><a class=\"card-options-collapse\" [routerLink]=\"\" data-toggle=\"card-collapse\"><i\r\n                  class=\"fe fe-chevron-up\"></i></a><a class=\"card-options-remove\" [routerLink]=\"\"\r\n                data-toggle=\"card-remove\"><i class=\"fe fe-x\"></i></a></div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-5\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Company</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Company\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-3\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Username</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Username\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-4\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Email address</label>\r\n                  <input class=\"form-control\" type=\"email\" placeholder=\"Email\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">First Name</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Company\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Last Name</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Last Name\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Address</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"Home Address\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-4\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">City</label>\r\n                  <input class=\"form-control\" type=\"text\" placeholder=\"City\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-md-3\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Postal Code</label>\r\n                  <input class=\"form-control\" type=\"number\" placeholder=\"ZIP Code\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-5\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-label\">Country</label>\r\n                  <select class=\"form-control btn-square\">\r\n                    <option value=\"0\">--Select--</option>\r\n                    <option value=\"1\">Germany</option>\r\n                    <option value=\"2\">Canada</option>\r\n                    <option value=\"3\">Usa</option>\r\n                    <option value=\"4\">Aus</option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12\">\r\n                <div class=\"form-group mb-0\">\r\n                  <label class=\"form-label\">About Me</label>\r\n                  <textarea class=\"form-control\" rows=\"5\" placeholder=\"Enter About your description\"></textarea>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-footer text-right\">\r\n            <button class=\"btn btn-primary\" type=\"submit\">Update Profile</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"col-md-12\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h4 class=\"card-title mb-0\">Add projects And Upload</h4>\r\n            <div class=\"card-options\"><a class=\"card-options-collapse\" [routerLink]=\"\" data-toggle=\"card-collapse\"><i\r\n                  class=\"fe fe-chevron-up\"></i></a><a class=\"card-options-remove\" [routerLink]=\"\"\r\n                data-toggle=\"card-remove\"><i class=\"fe fe-x\"></i></a></div>\r\n          </div>\r\n          <div class=\"table-responsive\">\r\n            <table class=\"table card-table table-vcenter text-nowrap\">\r\n              <thead>\r\n                <tr>\r\n                  <th>Project Name</th>\r\n                  <th>Date</th>\r\n                  <th>Status</th>\r\n                  <th>Price</th>\r\n                  <th></th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td><a class=\"text-inherit\" [routerLink]=\"\">Untrammelled prevents </a></td>\r\n                  <td>28 May 2018</td>\r\n                  <td><span class=\"status-icon bg-success\"></span> Completed</td>\r\n                  <td>$56,908</td>\r\n                  <td class=\"text-right\"><a class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-primary btn-sm\"\r\n                      [routerLink]=\"\"><i class=\"fa fa-pencil\"></i> Edit</a><a class=\"icon\" [routerLink]=\"\"></a><a\r\n                      class=\"btn btn-transparent btn-sm\" [routerLink]=\"\"><i class=\"fa fa-link\"></i> Update</a><a\r\n                      class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-danger btn-sm\" [routerLink]=\"\"><i\r\n                        class=\"fa fa-trash\"></i> Delete</a></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><a class=\"text-inherit\" [routerLink]=\"\">Untrammelled prevents</a></td>\r\n                  <td>12 June 2018</td>\r\n                  <td><span class=\"status-icon bg-danger\"></span> On going</td>\r\n                  <td>$45,087</td>\r\n                  <td class=\"text-right\"><a class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-primary btn-sm\"\r\n                      [routerLink]=\"\"><i class=\"fa fa-pencil\"></i> Edit</a><a class=\"icon\" [routerLink]=\"\"></a><a\r\n                      class=\"btn btn-transparent btn-sm\" [routerLink]=\"\"><i class=\"fa fa-link\"></i> Update</a><a\r\n                      class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-danger btn-sm\" [routerLink]=\"\"><i\r\n                        class=\"fa fa-trash\"></i> Delete</a></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><a class=\"text-inherit\" [routerLink]=\"\">Untrammelled prevents</a></td>\r\n                  <td>12 July 2018</td>\r\n                  <td><span class=\"status-icon bg-warning\"></span> Pending</td>\r\n                  <td>$60,123</td>\r\n                  <td class=\"text-right\"><a class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-primary btn-sm\"\r\n                      [routerLink]=\"\"><i class=\"fa fa-pencil\"></i> Edit</a><a class=\"icon\" [routerLink]=\"\"></a><a\r\n                      class=\"btn btn-transparent btn-sm\" [routerLink]=\"\"><i class=\"fa fa-link\"></i> Update</a><a\r\n                      class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-danger btn-sm\" [routerLink]=\"\"><i\r\n                        class=\"fa fa-trash\"></i> Delete</a></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><a class=\"text-inherit\" [routerLink]=\"\">Untrammelled prevents</a></td>\r\n                  <td>14 June 2018</td>\r\n                  <td><span class=\"status-icon bg-warning\"></span> Pending</td>\r\n                  <td>$70,435</td>\r\n                  <td class=\"text-right\"><a class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-primary btn-sm\"\r\n                      [routerLink]=\"\"><i class=\"fa fa-pencil\"></i> Edit</a><a class=\"icon\" [routerLink]=\"\"></a><a\r\n                      class=\"btn btn-transparent btn-sm\" [routerLink]=\"\"><i class=\"fa fa-link\"></i> Update</a><a\r\n                      class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-danger btn-sm\" [routerLink]=\"\"><i\r\n                        class=\"fa fa-trash\"></i> Delete</a></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><a class=\"text-inherit\" [routerLink]=\"\">Untrammelled prevents</a></td>\r\n                  <td>25 June 2018</td>\r\n                  <td><span class=\"status-icon bg-success\"></span> Completed</td>\r\n                  <td>$15,987</td>\r\n                  <td class=\"text-right\"><a class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-primary btn-sm\"\r\n                      [routerLink]=\"\"><i class=\"fa fa-pencil\"></i> Edit</a><a class=\"icon\" [routerLink]=\"\"></a><a\r\n                      class=\"btn btn-transparent btn-sm\" [routerLink]=\"\"><i class=\"fa fa-link\"></i> Update</a><a\r\n                      class=\"icon\" [routerLink]=\"\"></a><a class=\"btn btn-danger btn-sm\" [routerLink]=\"\"><i\r\n                        class=\"fa fa-trash\"></i> Delete</a></td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/users/users-profile/users-profile.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/users/users-profile/users-profile.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"user-profile\">\r\n    <div class=\"row\">\r\n      <!-- user profile first-style start-->\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card hovercard text-center\">\r\n          <div class=\"cardheader\"></div>\r\n          <div class=\"user-image\">\r\n            <div class=\"avatar\"><img alt=\"\" [src]=\"url? url:'assets/images/user/1.jpg'\"></div>\r\n            <div class=\"icon-wrapper\"><i class=\"icofont icofont-pencil-alt-5\"><input class=\"upload\" type=\"file\"\r\n                  (change)=\"readUrl($event)\" /></i></div>\r\n          </div>\r\n          <div class=\"info\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-6 col-lg-4 order-sm-1 order-xl-0\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"ttl-info text-left\">\r\n                      <h6><i class=\"fa fa-envelope\"></i>   Email</h6><span>Marekjecno@yahoo.com</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"ttl-info text-left\">\r\n                      <h6><i class=\"fa fa-calendar\"></i>   BOD</h6><span>02 January 1988</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-12 col-lg-4 order-sm-0 order-xl-1\">\r\n                <div class=\"user-designation\">\r\n                  <div class=\"title\"><a target=\"_blank\" href=\"\">Mark jecno</a></div>\r\n                  <div class=\"desc mt-2\">designer</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-6 col-lg-4 order-sm-2 order-xl-2\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"ttl-info text-left\">\r\n                      <h6><i class=\"fa fa-phone\"></i>   Contact Us</h6><span>India +91 123-456-7890</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"ttl-info text-left\">\r\n                      <h6><i class=\"fa fa-location-arrow\"></i>   Location</h6><span>B69 Near Schoool Demo Home</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"social-media\">\r\n              <ul class=\"list-inline\">\r\n                <li class=\"list-inline-item\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                <li class=\"list-inline-item\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n                <li class=\"list-inline-item\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                <li class=\"list-inline-item\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-instagram\"></i></a></li>\r\n                <li class=\"list-inline-item\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-rss\"></i></a></li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"follow\">\r\n              <div class=\"row\">\r\n                <div class=\"col-6 text-md-right border-right\">\r\n                  <div class=\"follow-num\" [CountTo]=\"25869\" [from]=\"0\" [duration]=\"5\"></div><span>Follower</span>\r\n                </div>\r\n                <div class=\"col-6 text-md-left\">\r\n                  <div class=\"follow-num counter\" [CountTo]=\"659887\" [from]=\"0\" [duration]=\"5\"></div>\r\n                  <span>Following</span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- user profile first-style end-->\r\n      <!-- user profile second-style start-->\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card\">\r\n          <div class=\"profile-img-style\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-8\">\r\n                <div class=\"media\"><img class=\"img-thumbnail rounded-circle mr-3\" src=\"assets/images/user/7.jpg\"\r\n                    alt=\"Generic placeholder image\">\r\n                  <div class=\"media-body align-self-center\">\r\n                    <h5 class=\"mt-0 user-name\">JOHAN DIO</h5>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4 align-self-center\">\r\n                <div class=\"float-sm-right\"><small>5 Hours ago</small></div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <p>you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing\r\n              hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined\r\n              chunks as necessary, making this the first true generator on the Internet.</p>\r\n            <div class=\"img-container\">\r\n              <div class=\"my-gallery\">\r\n                <ks-modal-gallery class=\"img-fluid rounded\" [id]=\"1\" [modalImages]=\"images\"\r\n                  [currentImageConfig]=\"{downloadable: true}\" [buttonsConfig]=\"buttonsConfigCustom\"\r\n                  (buttonAfterHook)=\"onButtonAfterHook($event)\">\r\n                </ks-modal-gallery>\r\n              </div>\r\n              <div class=\"like-comment mt-4\">\r\n                <ul class=\"list-inline\">\r\n                  <li class=\"list-inline-item border-right pr-3\">\r\n                    <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-heart\"></i></a>  Like</label><span class=\"ml-2 counter\"\r\n                                                                                                      [CountTo]=\"2659\" [from]=\"0\" [duration]=\"5\"></span>\r\n                  </li>\r\n                  <li class=\"list-inline-item ml-2\">\r\n                    <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>  Comment</label><span\r\n                          class=\"ml-2 counter\" [CountTo]=\"569\" [from]=\"0\" [duration]=\"5\"></span>\r\n\r\n                  </li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- user profile second-style end-->\r\n      <!-- user profile third-style start-->\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card\">\r\n          <div class=\"profile-img-style\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-8\">\r\n                <div class=\"media\"><img class=\"img-thumbnail rounded-circle mr-3\" src=\"assets/images/user/7.jpg\"\r\n                    alt=\"Generic placeholder image\">\r\n                  <div class=\"media-body align-self-center\">\r\n                    <h5 class=\"mt-0 user-name\">JOHAN DIO</h5>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4 align-self-center\">\r\n                <div class=\"float-sm-right\"><small>10 Hours ago</small></div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <p>you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing\r\n              hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined\r\n              chunks as necessary, making this the first true generator on the Internet.</p>\r\n            <div class=\"row mt-4 pictures my-gallery\" id=\"aniimated-thumbnials-2\">\r\n              <div class=\"col-sm-6\">\r\n                <ks-modal-gallery class=\"img-fluid rounded\" [id]=\"2\" [modalImages]=\"images\"\r\n                  [currentImageConfig]=\"{downloadable: true}\" [buttonsConfig]=\"buttonsConfigCustom\"\r\n                  (buttonAfterHook)=\"onButtonAfterHook($event)\">\r\n                </ks-modal-gallery>\r\n              </div>\r\n              <div class=\"col-sm-6\">\r\n                <ks-modal-gallery class=\"img-fluid rounded\" [id]=\"3\" [modalImages]=\"images\"\r\n                  [currentImageConfig]=\"{downloadable: true}\" [buttonsConfig]=\"buttonsConfigCustom\"\r\n                  (buttonAfterHook)=\"onButtonAfterHook($event)\">\r\n                </ks-modal-gallery>\r\n              </div>\r\n            </div>\r\n            <div class=\"like-comment mt-4\">\r\n              <ul class=\"list-inline\">\r\n                <li class=\"list-inline-item border-right pr-3\">\r\n                  <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-heart\"></i></a>  Like</label><span class=\"ml-2 counter\"\r\n                    [CountTo]=\"569\" [from]=\"0\" [duration]=\"5\"></span>\r\n                </li>\r\n                <li class=\"list-inline-item ml-2\">\r\n                  <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>  Comment</label><span\r\n                    class=\"ml-2 counter\" [CountTo]=\"569\" [from]=\"0\" [duration]=\"5\"></span>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- user profile third-style end-->\r\n      <!-- user profile fourth-style start-->\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card\">\r\n          <div class=\"profile-img-style\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-8\">\r\n                <div class=\"media\"><img class=\"img-thumbnail rounded-circle mr-3\" src=\"assets/images/user/7.jpg\"\r\n                    alt=\"Generic placeholder image\">\r\n                  <div class=\"media-body align-self-center\">\r\n                    <h5 class=\"mt-0 user-name\">JOHAN DIO</h5>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4 align-self-center\">\r\n                <div class=\"float-sm-right\"><small>10 Hours ago</small></div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical\r\n              Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at\r\n              Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a\r\n              Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the\r\n              undoubtable source .Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a\r\n              piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin\r\n              professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words,\r\n              consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature,\r\n              discovered the undoubtable source</p>\r\n            <div class=\"like-comment mt-4\">\r\n              <ul class=\"list-inline\">\r\n                <li class=\"list-inline-item border-right pr-3\">\r\n                  <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-heart\"></i></a>  Like</label><span class=\"ml-2 counter\"\r\n                    [CountTo]=\"569\" [from]=\"0\" [duration]=\"10\"></span>\r\n                </li>\r\n                <li class=\"list-inline-item ml-2\">\r\n                  <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>  Comment</label><span\r\n                    class=\"ml-2 counter\" [CountTo]=\"569\" [from]=\"0\" [duration]=\"10\"></span>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- user profile fourth-style end-->\r\n      <!-- user profile fifth-style start-->\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card\">\r\n          <div class=\"profile-img-style\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-8\">\r\n                <div class=\"media\"><img class=\"img-thumbnail rounded-circle mr-3\" src=\"assets/images/user/7.jpg\"\r\n                    alt=\"Generic placeholder image\">\r\n                  <div class=\"media-body align-self-center\">\r\n                    <h5 class=\"mt-0 user-name\">JOHAN DIO</h5>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4 align-self-center\">\r\n                <div class=\"float-sm-right\"><small>10 Hours ago</small></div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-12 col-xl-4\">\r\n                <div class=\"my-gallery\" id=\"aniimated-thumbnials-3\">\r\n                  <ks-modal-gallery class=\"img-fluid rounded\" [id]=\"4\" [modalImages]=\"images1\"\r\n                    [currentImageConfig]=\"{downloadable: true}\" [buttonsConfig]=\"buttonsConfigCustom\"\r\n                    (buttonAfterHook)=\"onButtonAfterHook($event)\">\r\n                  </ks-modal-gallery>\r\n                </div>\r\n                <div class=\"like-comment mt-4 like-comment-sm-mb\">\r\n                  <ul class=\"list-inline\">\r\n                    <li class=\"list-inline-item border-right pr-3\">\r\n                      <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-heart\"></i></a>  Like</label><span\r\n                        class=\"ml-2 counter\" [CountTo]=\"569\" [from]=\"0\" [duration]=\"5\"></span>\r\n                    </li>\r\n                    <li class=\"list-inline-item ml-2\">\r\n                      <label class=\"m-0\"><a  href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>  Comment</label><span\r\n                        class=\"ml-2 counter\" [CountTo]=\"569\" [from]=\"0\" [duration]=\"5\"></span>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-6\">\r\n                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of\r\n                  classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin\r\n                  professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words,\r\n                  consecteturContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece\r\n                  of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin\r\n                  professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words,\r\n                  consectetur</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- user profile fifth-style end-->\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./src/app/components/users/user-cards/user-cards.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/users/user-cards/user-cards.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcnMvdXNlci1jYXJkcy91c2VyLWNhcmRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/users/user-cards/user-cards.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/users/user-cards/user-cards.component.ts ***!
  \*********************************************************************/
/*! exports provided: UserCardsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserCardsComponent", function() { return UserCardsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserCardsComponent = /** @class */ (function () {
    function UserCardsComponent() {
    }
    UserCardsComponent.prototype.ngOnInit = function () { };
    UserCardsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-cards',
            template: __webpack_require__(/*! raw-loader!./user-cards.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/users/user-cards/user-cards.component.html"),
            styles: [__webpack_require__(/*! ./user-cards.component.scss */ "./src/app/components/users/user-cards/user-cards.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserCardsComponent);
    return UserCardsComponent;
}());



/***/ }),

/***/ "./src/app/components/users/user-edit/user-edit.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/users/user-edit/user-edit.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcnMvdXNlci1lZGl0L3VzZXItZWRpdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/users/user-edit/user-edit.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/users/user-edit/user-edit.component.ts ***!
  \*******************************************************************/
/*! exports provided: UserEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEditComponent", function() { return UserEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserEditComponent = /** @class */ (function () {
    function UserEditComponent() {
    }
    UserEditComponent.prototype.ngOnInit = function () { };
    UserEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-edit',
            template: __webpack_require__(/*! raw-loader!./user-edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/users/user-edit/user-edit.component.html"),
            styles: [__webpack_require__(/*! ./user-edit.component.scss */ "./src/app/components/users/user-edit/user-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserEditComponent);
    return UserEditComponent;
}());



/***/ }),

/***/ "./src/app/components/users/users-profile/users-profile.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/users/users-profile/users-profile.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcnMvdXNlcnMtcHJvZmlsZS91c2Vycy1wcm9maWxlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/users/users-profile/users-profile.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/users/users-profile/users-profile.component.ts ***!
  \***************************************************************************/
/*! exports provided: UsersProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersProfileComponent", function() { return UsersProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsersProfileComponent = /** @class */ (function () {
    function UsersProfileComponent(galleryService) {
        this.galleryService = galleryService;
        this.images = [
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](0, {
                img: 'assets/images/other-images/profile-style-img3.png',
                extUrl: 'http://www.google.com'
            })
        ];
        this.images1 = [
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](0, {
                img: 'assets/images/blog/img.png',
                extUrl: 'http://www.google.com'
            })
        ];
        this.buttonsConfigDefault = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].DEFAULT
        };
        this.buttonsConfigSimple = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].SIMPLE
        };
        this.buttonsConfigAdvanced = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].ADVANCED
        };
        this.buttonsConfigFull = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].FULL
        };
        this.buttonsConfigCustom = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].CUSTOM,
            buttons: [
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_FULL_SCREEN"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_EXTURL"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_DOWNLOAD"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_CLOSE"]
            ]
        };
        this.customPlainGalleryRowDescConfig = {
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["PlainGalleryStrategy"].CUSTOM,
            layout: new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["AdvancedLayout"](-1, true)
        };
    }
    UsersProfileComponent.prototype.openImageModalRowDescription = function (image) {
        var index = this.getCurrentIndexCustomLayout(image, this.images);
        this.customPlainGalleryRowDescConfig = Object.assign({}, this.customPlainGalleryRowDescConfig, { layout: new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["AdvancedLayout"](index, true) });
    };
    UsersProfileComponent.prototype.getCurrentIndexCustomLayout = function (image, images) {
        return image ? images.indexOf(image) : -1;
    };
    ;
    UsersProfileComponent.prototype.onButtonAfterHook = function (event) {
        if (!event || !event.button) {
            return;
        }
    };
    UsersProfileComponent.prototype.onCustomButtonBeforeHook = function (event, galleryId) {
        var _this = this;
        if (!event || !event.button) {
            return;
        }
        if (event.button.type === _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonType"].CUSTOM) {
            this.addRandomImage();
            setTimeout(function () {
                _this.galleryService.openGallery(galleryId, _this.images.length - 1);
            }, 0);
        }
    };
    UsersProfileComponent.prototype.onCustomButtonAfterHook = function (event, galleryId) {
        if (!event || !event.button) {
            return;
        }
    };
    UsersProfileComponent.prototype.addRandomImage = function () {
        var imageToCopy = this.images[Math.floor(Math.random() * this.images.length)];
        var newImage = new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](this.images.length - 1 + 1, imageToCopy.modal, imageToCopy.plain);
        this.images = this.images.concat([newImage]);
    };
    //FileUpload
    UsersProfileComponent.prototype.readUrl = function (event) {
        var _this = this;
        if (event.target.files.length === 0)
            return;
        //Image upload validation
        var mimeType = event.target.files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }
        // Image upload
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = function (_event) {
            _this.url = reader.result;
        };
    };
    UsersProfileComponent.prototype.ngOnInit = function () { };
    UsersProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users-profile',
            template: __webpack_require__(/*! raw-loader!./users-profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/users/users-profile/users-profile.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./users-profile.component.scss */ "./src/app/components/users/users-profile/users-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [_ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["GalleryService"]])
    ], UsersProfileComponent);
    return UsersProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/users/users-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/users/users-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: UsersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersRoutingModule", function() { return UsersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_profile_users_profile_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users-profile/users-profile.component */ "./src/app/components/users/users-profile/users-profile.component.ts");
/* harmony import */ var _user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-edit/user-edit.component */ "./src/app/components/users/user-edit/user-edit.component.ts");
/* harmony import */ var _user_cards_user_cards_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-cards/user-cards.component */ "./src/app/components/users/user-cards/user-cards.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        children: [
            {
                path: 'profile',
                component: _users_profile_users_profile_component__WEBPACK_IMPORTED_MODULE_2__["UsersProfileComponent"],
                data: {
                    title: "Profile",
                    breadcrumb: "Profile"
                }
            },
            {
                path: 'edit',
                component: _user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_3__["UserEditComponent"],
                data: {
                    title: "Edit",
                    breadcrumb: "Edit"
                }
            },
            {
                path: 'cards',
                component: _user_cards_user_cards_component__WEBPACK_IMPORTED_MODULE_4__["UserCardsComponent"],
                data: {
                    title: "Cards",
                    breadcrumb: "Cards"
                }
            }
        ]
    }
];
var UsersRoutingModule = /** @class */ (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UsersRoutingModule);
    return UsersRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/users/users.module.ts":
/*!**************************************************!*\
  !*** ./src/app/components/users/users.module.ts ***!
  \**************************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var angular_count_to__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-count-to */ "./node_modules/angular-count-to/modules/angular-count-to.es5.js");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! mousetrap */ "./node_modules/mousetrap/mousetrap.js");
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(mousetrap__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _users_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users-routing.module */ "./src/app/components/users/users-routing.module.ts");
/* harmony import */ var _users_profile_users_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./users-profile/users-profile.component */ "./src/app/components/users/users-profile/users-profile.component.ts");
/* harmony import */ var _user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user-edit/user-edit.component */ "./src/app/components/users/user-edit/user-edit.component.ts");
/* harmony import */ var _user_cards_user_cards_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user-cards/user-cards.component */ "./src/app/components/users/user-cards/user-cards.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_users_profile_users_profile_component__WEBPACK_IMPORTED_MODULE_7__["UsersProfileComponent"], _user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_8__["UserEditComponent"], _user_cards_user_cards_component__WEBPACK_IMPORTED_MODULE_9__["UserCardsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _users_routing_module__WEBPACK_IMPORTED_MODULE_6__["UsersRoutingModule"],
                angular_count_to__WEBPACK_IMPORTED_MODULE_2__["CountToModule"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_3__["GalleryModule"].forRoot()
            ]
        })
    ], UsersModule);
    return UsersModule;
}());



/***/ })

}]);
//# sourceMappingURL=components-users-users-module.js.map