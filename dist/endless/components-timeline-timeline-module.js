(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-timeline-timeline-module"],{

/***/ "./node_modules/angular-vertical-timeline/dist/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/angular-vertical-timeline/dist/index.js ***!
  \**************************************************************/
/*! exports provided: VerticalTimelineComponent, VerticalTimelineCardComponent, VerticalTimelineModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vertical_timeline_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vertical-timeline.component */ "./node_modules/angular-vertical-timeline/dist/vertical-timeline.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineComponent", function() { return _vertical_timeline_component__WEBPACK_IMPORTED_MODULE_0__["VerticalTimelineComponent"]; });

/* harmony import */ var _vertical_timeline_card_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vertical-timeline-card.component */ "./node_modules/angular-vertical-timeline/dist/vertical-timeline-card.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineCardComponent", function() { return _vertical_timeline_card_component__WEBPACK_IMPORTED_MODULE_1__["VerticalTimelineCardComponent"]; });

/* harmony import */ var _vertical_timeline_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vertical-timeline.module */ "./node_modules/angular-vertical-timeline/dist/vertical-timeline.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineModule", function() { return _vertical_timeline_module__WEBPACK_IMPORTED_MODULE_2__["VerticalTimelineModule"]; });




//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/angular-vertical-timeline/dist/vertical-timeline-card.component.js":
/*!*****************************************************************************************!*\
  !*** ./node_modules/angular-vertical-timeline/dist/vertical-timeline-card.component.js ***!
  \*****************************************************************************************/
/*! exports provided: VerticalTimelineCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineCardComponent", function() { return VerticalTimelineCardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var VerticalTimelineCardComponent = /** @class */ (function () {
    function VerticalTimelineCardComponent() {
        this.disabled = false;
        this.isATimelineItem = false;
    }
    VerticalTimelineCardComponent.prototype.ngOnInit = function () {
        this.isATimelineItem = true; // set class `timeline-item` on host `<div>`
        if (this.dateValue === null || this.dateValue === undefined) {
            this.dateValue = new Date();
        }
        if (this.color === null || this.color === undefined) {
            this.color = '#3F51B5';
        }
        if (this.isLight(this.color)) {
            this.textColor = '#000000';
        }
        else {
            this.textColor = '#FFFFFF';
        }
    };
    VerticalTimelineCardComponent.prototype.isLight = function (hexColor) {
        var R = parseInt(hexColor.slice(1, 3), 16);
        var G = parseInt(hexColor.slice(3, 5), 16);
        var B = parseInt(hexColor.slice(5, 7), 16);
        var maxBrightness = this.calculateBrightness(255, 255, 255);
        var brightness = this.calculateBrightness(R, G, B);
        var pBrightness = brightness / maxBrightness;
        return pBrightness > 0.5;
    };
    // HSP rule sqrt( .299 R2 + .587 G2 + .114 B2 ), see http://alienryderflex.com/hsp.html
    // HSP rule sqrt( .299 R2 + .587 G2 + .114 B2 ), see http://alienryderflex.com/hsp.html
    VerticalTimelineCardComponent.prototype.calculateBrightness = 
    // HSP rule sqrt( .299 R2 + .587 G2 + .114 B2 ), see http://alienryderflex.com/hsp.html
    function (R, G, B) {
        return Math.sqrt((0.299 * Math.pow(R, 2)) +
            (0.587 * Math.pow(G, 2)) +
            (0.114 * Math.pow(B, 2)));
    };
    VerticalTimelineCardComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'vertical-timeline-card',
                    template: "\n      <div>\n        <div class=\"timeline-img\" [style.background]=\"color\">\n          <p *ngIf=\"!timeString || timeString === ''\" [style.color]=\"textColor\">{{dateValue | date: \"HH:mm\"}}</p>\n          <p *ngIf=\"timeString && timeString != ''\" [style.color]=\"textColor\">{{timeString}}</p>\n        </div>\n\n        <div class=\"timeline-content\" [ngClass]=\"{'disabled-element': disabled}\">\n          <div class=\"date\" [style.background]=\"color\">\n            <p [style.color]=\"textColor\">{{dateValue | date:\"dd MMMM yyyy\"}}</p>\n          </div>\n\n          <div class=\"inner-content\">\n            <ng-content></ng-content>\n          </div>\n        </div>\n      </div>\n    ",
                },] },
    ];
    /** @nocollapse */
    VerticalTimelineCardComponent.propDecorators = {
        "dateValue": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "timeString": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "color": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "disabled": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
        "isATimelineItem": [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.timeline-item',] },],
    };
    return VerticalTimelineCardComponent;
}());

//# sourceMappingURL=vertical-timeline-card.component.js.map

/***/ }),

/***/ "./node_modules/angular-vertical-timeline/dist/vertical-timeline.component.js":
/*!************************************************************************************!*\
  !*** ./node_modules/angular-vertical-timeline/dist/vertical-timeline.component.js ***!
  \************************************************************************************/
/*! exports provided: VerticalTimelineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineComponent", function() { return VerticalTimelineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var VerticalTimelineComponent = /** @class */ (function () {
    function VerticalTimelineComponent() {
    }
    VerticalTimelineComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'vertical-timeline',
                    template: "\n    <section class=\"timeline\">\n      <div>\n        <ng-content></ng-content>\n      </div>\n    </section>\n  ",
                },] },
    ];
    return VerticalTimelineComponent;
}());

//# sourceMappingURL=vertical-timeline.component.js.map

/***/ }),

/***/ "./node_modules/angular-vertical-timeline/dist/vertical-timeline.module.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/angular-vertical-timeline/dist/vertical-timeline.module.js ***!
  \*********************************************************************************/
/*! exports provided: VerticalTimelineModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerticalTimelineModule", function() { return VerticalTimelineModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _vertical_timeline_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vertical-timeline.component */ "./node_modules/angular-vertical-timeline/dist/vertical-timeline.component.js");
/* harmony import */ var _vertical_timeline_card_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vertical-timeline-card.component */ "./node_modules/angular-vertical-timeline/dist/vertical-timeline-card.component.js");




var VerticalTimelineModule = /** @class */ (function () {
    function VerticalTimelineModule() {
    }
    VerticalTimelineModule.forRoot = function () {
        return {
            ngModule: VerticalTimelineModule,
        };
    };
    VerticalTimelineModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
                    ],
                    declarations: [
                        _vertical_timeline_component__WEBPACK_IMPORTED_MODULE_2__["VerticalTimelineComponent"],
                        _vertical_timeline_card_component__WEBPACK_IMPORTED_MODULE_3__["VerticalTimelineCardComponent"]
                    ],
                    exports: [
                        _vertical_timeline_component__WEBPACK_IMPORTED_MODULE_2__["VerticalTimelineComponent"],
                        _vertical_timeline_card_component__WEBPACK_IMPORTED_MODULE_3__["VerticalTimelineCardComponent"]
                    ]
                },] },
    ];
    return VerticalTimelineModule;
}());

//# sourceMappingURL=vertical-timeline.module.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/timeline/timeline1/timeline1.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/timeline/timeline1/timeline1.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Example</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <!-- cd-timeline Start-->\r\n          <vertical-timeline>\r\n            <section class=\"cd-container\" id=\"cd-timeline\">\r\n              <vertical-timeline-card [dateValue]=\"'jan 14 2019'\" [timeString]=\"'12:18'\">\r\n                <h4>Title of section<span class=\"digits\"> 1</span></h4>\r\n                <p class=\"m-0\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident\r\n                  rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus\r\n                  veritatis qui ut.</p>\r\n              </vertical-timeline-card>\r\n              <vertical-timeline-card [dateValue]=\"'jan 24 2019'\" [timeString]=\"'15:20'\">\r\n                <div class=\"cd-timeline-content\">\r\n                  <h4>Title of section<span class=\"digits\"> 2</span></h4>\r\n                  <div class=\"embed-responsive embed-responsive-21by9 m-t-20\">\r\n                    <iframe src=\"https://www.youtube.com/embed/wpmHZspl4EM\" allowfullscreen=\"\"></iframe>\r\n                  </div>\r\n                </div>\r\n              </vertical-timeline-card>\r\n              <vertical-timeline-card [dateValue]=\"'Feb 14 2019'\" [timeString]=\"'5:30'\">\r\n                <h4>Title of section<span class=\"digits\"> 3</span></h4><img class=\"img-fluid p-t-20\"\r\n                  src=\"assets/images/banner/1.jpg\" alt=\"\">\r\n              </vertical-timeline-card>\r\n              <vertical-timeline-card [dateValue]=\"'March 14 2019'\" [timeString]=\"'7:40'\">\r\n                <div class=\"cd-timeline-content\">\r\n                  <h4>Title of section<span class=\"digits\"> 4</span></h4>\r\n                  <audio class=\"m-t-20\" controls=\"\">\r\n                    <source type=\"audio/ogg\"> Your browser does not support the audio element.\r\n                  </audio>\r\n                </div>\r\n              </vertical-timeline-card>\r\n              <vertical-timeline-card [dateValue]=\"'April 18 2019'\" [timeString]=\"'18:15'\">\r\n                <h4>Title of section<span class=\"digits\"> 5</span></h4><img class=\"img-fluid p-t-20\"\r\n                  src=\"assets/images/banner/3.jpg\" alt=\"\">\r\n              </vertical-timeline-card>\r\n              <vertical-timeline-card>\r\n                <h4>Final Section</h4>\r\n                <p class=\"m-0\">This is the content of the last section</p>\r\n              </vertical-timeline-card>\r\n            </section>\r\n          </vertical-timeline>\r\n          <!-- cd-timeline Ends-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/timeline/timeline2/timeline2.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/timeline/timeline2/timeline2.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline Primary color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-primary\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-primary\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-primary\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-primary\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-primary\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline secondary color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-secondary\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-secondary\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-secondary\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-secondary\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-secondary\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline Success color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-success\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-success\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-success\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-success\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-success\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline Info color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-info\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-info\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-info\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-info\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-info\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline Warning color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-warning\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-warning\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-warning\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-warning\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-warning\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Timeline Danger color</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"timeline-small\">\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-danger\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-danger\">\r\n                <app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 small-line bg-danger\">\r\n                <app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 timeline-line-1 bg-danger\">\r\n                <app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"timeline-round m-r-30 medium-line bg-danger\">\r\n                <app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the\r\n                  industry.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid ends -->"

/***/ }),

/***/ "./src/app/components/timeline/timeline-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/timeline/timeline-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: TimelineRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelineRoutingModule", function() { return TimelineRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _timeline1_timeline1_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./timeline1/timeline1.component */ "./src/app/components/timeline/timeline1/timeline1.component.ts");
/* harmony import */ var _timeline2_timeline2_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./timeline2/timeline2.component */ "./src/app/components/timeline/timeline2/timeline2.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        children: [
            {
                path: 'timeline1',
                component: _timeline1_timeline1_component__WEBPACK_IMPORTED_MODULE_2__["Timeline1Component"],
                data: {
                    title: "Timeline1",
                    breadcrumb: "Timeline1"
                }
            },
            {
                path: 'timeline2',
                component: _timeline2_timeline2_component__WEBPACK_IMPORTED_MODULE_3__["Timeline2Component"],
                data: {
                    title: "Timeline2",
                    breadcrumb: "Timeline2"
                }
            },
        ]
    }
];
var TimelineRoutingModule = /** @class */ (function () {
    function TimelineRoutingModule() {
    }
    TimelineRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TimelineRoutingModule);
    return TimelineRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/timeline/timeline.module.ts":
/*!********************************************************!*\
  !*** ./src/app/components/timeline/timeline.module.ts ***!
  \********************************************************/
/*! exports provided: TimelineModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelineModule", function() { return TimelineModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var angular_vertical_timeline__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-vertical-timeline */ "./node_modules/angular-vertical-timeline/dist/index.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _timeline_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./timeline-routing.module */ "./src/app/components/timeline/timeline-routing.module.ts");
/* harmony import */ var _timeline1_timeline1_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./timeline1/timeline1.component */ "./src/app/components/timeline/timeline1/timeline1.component.ts");
/* harmony import */ var _timeline2_timeline2_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./timeline2/timeline2.component */ "./src/app/components/timeline/timeline2/timeline2.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var TimelineModule = /** @class */ (function () {
    function TimelineModule() {
    }
    TimelineModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_timeline1_timeline1_component__WEBPACK_IMPORTED_MODULE_5__["Timeline1Component"], _timeline2_timeline2_component__WEBPACK_IMPORTED_MODULE_6__["Timeline2Component"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _timeline_routing_module__WEBPACK_IMPORTED_MODULE_4__["TimelineRoutingModule"],
                angular_vertical_timeline__WEBPACK_IMPORTED_MODULE_2__["VerticalTimelineModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]
            ]
        })
    ], TimelineModule);
    return TimelineModule;
}());



/***/ }),

/***/ "./src/app/components/timeline/timeline1/timeline1.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/timeline/timeline1/timeline1.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".timeline {\n  padding: 5%;\n  position: relative; }\n  .timeline::before {\n    content: '';\n    background: #C5CAE9;\n    width: 5px;\n    height: 95%;\n    position: absolute;\n    left: 50%;\n    -webkit-transform: translateX(-50%);\n            transform: translateX(-50%); }\n  .timeline-item {\n  width: 100%;\n  margin-bottom: 70px; }\n  .timeline-item:nth-child(odd) .timeline-content .inner-content {\n    margin-right: 105px; }\n  .timeline-item:nth-child(even) .timeline-content {\n    float: right;\n    padding: 40px 30px 10px 30px; }\n  .timeline-item:nth-child(even) .timeline-content .date {\n      right: auto;\n      left: 0; }\n  .timeline-item:nth-child(even) .timeline-content::after {\n      content: '';\n      position: absolute;\n      border-style: solid;\n      width: 0;\n      height: 0;\n      top: 30px;\n      left: -15px;\n      border-width: 10px 15px 10px 0;\n      border-color: transparent #f5f5f5 transparent transparent; }\n  .timeline-item::after {\n    content: '';\n    display: block;\n    clear: both; }\n  .timeline-content {\n  position: relative;\n  width: 40%;\n  padding: 10px 30px;\n  border-radius: 4px;\n  background: #f5f5f5;\n  box-shadow: 0 20px 25px -15px rgba(0, 0, 0, 0.3);\n  min-height: 10em; }\n  .timeline-content::after {\n    content: '';\n    position: absolute;\n    border-style: solid;\n    width: 0;\n    height: 0;\n    top: 30px;\n    right: -15px;\n    border-width: 10px 0 10px 15px;\n    border-color: transparent transparent transparent #f5f5f5; }\n  .timeline-img {\n  background: #3F51B5;\n  border-radius: 50%;\n  position: absolute;\n  left: 50%;\n  width: 60px;\n  height: 60px;\n  margin-left: -30px;\n  margin-top: 45px;\n  display: flex;\n  align-items: center;\n  justify-content: center; }\n  .timeline-img p {\n    height: 0;\n    line-height: 1;\n    color: #FFFFFF; }\n  .timeline-card {\n  padding: 0 !important; }\n  .timeline-card p {\n    padding: 0 20px; }\n  .timeline-card a {\n    margin-left: 20px; }\n  .timeline-item:nth-child(2) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1458530970867-aaa3700e966d\") center center no-repeat;\n  background-size: cover; }\n  .timeline-item:nth-child(5) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1444093826349-9ce8c622f4f3\") center center no-repeat;\n  background-size: cover; }\n  .timeline-item:nth-child(6) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1471479917193-f00955256257\") center center no-repeat;\n  background-size: cover; }\n  .timeline-item:nth-child(8) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1466840787022-48e0ec048c8a\") center center no-repeat;\n  background-size: cover; }\n  .timeline-item:nth-child(10) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1447639703758-f525f36456bf\") center center no-repeat;\n  background-size: cover; }\n  .timeline-item:nth-child(11) .timeline-img-header {\n  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.4)), url(\"https://hd.unsplash.com/photo-1469429978400-082eec725ad5\") center center no-repeat;\n  background-size: cover; }\n  .timeline-img-header {\n  height: 200px;\n  position: relative;\n  margin-bottom: 20px; }\n  .timeline-img-header h2 {\n    color: #FFFFFF;\n    position: absolute;\n    bottom: 5px;\n    left: 20px; }\n  blockquote {\n  margin-top: 30px;\n  color: #757575;\n  border-left-color: #3F51B5;\n  padding: 0 20px; }\n  .date {\n  background: #3F51B5;\n  display: inline-block;\n  color: #FFFFFF;\n  padding: 10px;\n  position: absolute;\n  top: 0;\n  right: 0; }\n  @media screen and (max-width: 1024px) {\n  .timeline::before {\n    left: 50px; }\n  .timeline .timeline-img {\n    left: 50px; }\n  .timeline .timeline-content {\n    max-width: 100%;\n    width: auto;\n    margin-left: 70px; }\n  .timeline .timeline-item:nth-child(even) .timeline-content {\n    float: none;\n    margin-top: 35px; }\n    .timeline .timeline-item:nth-child(even) .timeline-content .date {\n      right: 0;\n      left: auto; }\n    .timeline .timeline-item:nth-child(even) .timeline-content .inner-content {\n      margin-top: 35px; }\n  .timeline .timeline-item:nth-child(odd) .timeline-content {\n    margin-top: 35px; }\n    .timeline .timeline-item:nth-child(odd) .timeline-content .inner-content {\n      margin-top: 65px;\n      margin-right: 0; }\n    .timeline .timeline-item:nth-child(odd) .timeline-content::after {\n      content: '';\n      position: absolute;\n      border-style: solid;\n      width: 0;\n      height: 0;\n      top: 30px;\n      left: -15px;\n      border-width: 10px 15px 10px 0;\n      border-color: transparent #f5f5f5 transparent transparent; } }\n  .inner-content {\n  margin-top: 35px;\n  text-align: justify; }\n  .disabled-element {\n  pointer-events: none;\n  -webkit-filter: opacity(35%);\n          filter: opacity(35%); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9hbmd1bGFyLXZlcnRpY2FsLXRpbWVsaW5lL2Rpc3QvdmVydGljYWwtdGltZWxpbmUuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLGtCQUFrQixFQUFFO0VBQ3BCO0lBQ0UsV0FBVztJQUNYLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsbUNBQTJCO1lBQTNCLDJCQUEyQixFQUFFO0VBRWpDO0VBQ0UsV0FBVztFQUNYLG1CQUFtQixFQUFFO0VBQ3JCO0lBQ0UsbUJBQW1CLEVBQUU7RUFDdkI7SUFDRSxZQUFZO0lBQ1osNEJBQTRCLEVBQUU7RUFDOUI7TUFDRSxXQUFXO01BQ1gsT0FBTyxFQUFFO0VBQ1g7TUFDRSxXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixRQUFRO01BQ1IsU0FBUztNQUNULFNBQVM7TUFDVCxXQUFXO01BQ1gsOEJBQThCO01BQzlCLHlEQUF5RCxFQUFFO0VBQy9EO0lBQ0UsV0FBVztJQUNYLGNBQWM7SUFDZCxXQUFXLEVBQUU7RUFFakI7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGdEQUFnRDtFQUNoRCxnQkFBZ0IsRUFBRTtFQUNsQjtJQUNFLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFFBQVE7SUFDUixTQUFTO0lBQ1QsU0FBUztJQUNULFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIseURBQXlELEVBQUU7RUFFL0Q7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCLEVBQUU7RUFDekI7SUFDRSxTQUFTO0lBQ1QsY0FBYztJQUNkLGNBQWMsRUFBRTtFQUVwQjtFQUNFLHFCQUFxQixFQUFFO0VBQ3ZCO0lBQ0UsZUFBZSxFQUFFO0VBQ25CO0lBQ0UsaUJBQWlCLEVBQUU7RUFFdkI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSwwSkFBMEo7RUFDMUosc0JBQXNCLEVBQUU7RUFFMUI7RUFDRSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFFO0VBQ3JCO0lBQ0UsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsVUFBVSxFQUFFO0VBRWhCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCwwQkFBMEI7RUFDMUIsZUFBZSxFQUFFO0VBRW5CO0VBQ0UsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUSxFQUFFO0VBRVo7RUFDRTtJQUNFLFVBQVUsRUFBRTtFQUNkO0lBQ0UsVUFBVSxFQUFFO0VBQ2Q7SUFDRSxlQUFlO0lBQ2YsV0FBVztJQUNYLGlCQUFpQixFQUFFO0VBQ3JCO0lBQ0UsV0FBVztJQUNYLGdCQUFnQixFQUFFO0lBQ2xCO01BQ0UsUUFBUTtNQUNSLFVBQVUsRUFBRTtJQUNkO01BQ0UsZ0JBQWdCLEVBQUU7RUFDdEI7SUFDRSxnQkFBZ0IsRUFBRTtJQUNsQjtNQUNFLGdCQUFnQjtNQUNoQixlQUFlLEVBQUU7SUFDbkI7TUFDRSxXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixRQUFRO01BQ1IsU0FBUztNQUNULFNBQVM7TUFDVCxXQUFXO01BQ1gsOEJBQThCO01BQzlCLHlEQUF5RCxFQUFFLEVBQUU7RUFFbkU7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUU7RUFFdkI7RUFDRSxvQkFBb0I7RUFDcEIsNEJBQW9CO1VBQXBCLG9CQUFvQixFQUFFIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90aW1lbGluZS90aW1lbGluZTEvdGltZWxpbmUxLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpbWVsaW5lIHtcbiAgcGFkZGluZzogNSU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxuICAudGltZWxpbmU6OmJlZm9yZSB7XG4gICAgY29udGVudDogJyc7XG4gICAgYmFja2dyb3VuZDogI0M1Q0FFOTtcbiAgICB3aWR0aDogNXB4O1xuICAgIGhlaWdodDogOTUlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpOyB9XG5cbi50aW1lbGluZS1pdGVtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1ib3R0b206IDcwcHg7IH1cbiAgLnRpbWVsaW5lLWl0ZW06bnRoLWNoaWxkKG9kZCkgLnRpbWVsaW5lLWNvbnRlbnQgLmlubmVyLWNvbnRlbnQge1xuICAgIG1hcmdpbi1yaWdodDogMTA1cHg7IH1cbiAgLnRpbWVsaW5lLWl0ZW06bnRoLWNoaWxkKGV2ZW4pIC50aW1lbGluZS1jb250ZW50IHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgcGFkZGluZzogNDBweCAzMHB4IDEwcHggMzBweDsgfVxuICAgIC50aW1lbGluZS1pdGVtOm50aC1jaGlsZChldmVuKSAudGltZWxpbmUtY29udGVudCAuZGF0ZSB7XG4gICAgICByaWdodDogYXV0bztcbiAgICAgIGxlZnQ6IDA7IH1cbiAgICAudGltZWxpbmUtaXRlbTpudGgtY2hpbGQoZXZlbikgLnRpbWVsaW5lLWNvbnRlbnQ6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICAgIHdpZHRoOiAwO1xuICAgICAgaGVpZ2h0OiAwO1xuICAgICAgdG9wOiAzMHB4O1xuICAgICAgbGVmdDogLTE1cHg7XG4gICAgICBib3JkZXItd2lkdGg6IDEwcHggMTVweCAxMHB4IDA7XG4gICAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50ICNmNWY1ZjUgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQ7IH1cbiAgLnRpbWVsaW5lLWl0ZW06OmFmdGVyIHtcbiAgICBjb250ZW50OiAnJztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBjbGVhcjogYm90aDsgfVxuXG4udGltZWxpbmUtY29udGVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDQwJTtcbiAgcGFkZGluZzogMTBweCAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIGJveC1zaGFkb3c6IDAgMjBweCAyNXB4IC0xNXB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcbiAgbWluLWhlaWdodDogMTBlbTsgfVxuICAudGltZWxpbmUtY29udGVudDo6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogMDtcbiAgICB0b3A6IDMwcHg7XG4gICAgcmlnaHQ6IC0xNXB4O1xuICAgIGJvcmRlci13aWR0aDogMTBweCAwIDEwcHggMTVweDtcbiAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50ICNmNWY1ZjU7IH1cblxuLnRpbWVsaW5lLWltZyB7XG4gIGJhY2tncm91bmQ6ICMzRjUxQjU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHdpZHRoOiA2MHB4O1xuICBoZWlnaHQ6IDYwcHg7XG4gIG1hcmdpbi1sZWZ0OiAtMzBweDtcbiAgbWFyZ2luLXRvcDogNDVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cbiAgLnRpbWVsaW5lLWltZyBwIHtcbiAgICBoZWlnaHQ6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgY29sb3I6ICNGRkZGRkY7IH1cblxuLnRpbWVsaW5lLWNhcmQge1xuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7IH1cbiAgLnRpbWVsaW5lLWNhcmQgcCB7XG4gICAgcGFkZGluZzogMCAyMHB4OyB9XG4gIC50aW1lbGluZS1jYXJkIGEge1xuICAgIG1hcmdpbi1sZWZ0OiAyMHB4OyB9XG5cbi50aW1lbGluZS1pdGVtOm50aC1jaGlsZCgyKSAudGltZWxpbmUtaW1nLWhlYWRlciB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDAsIDAsIDAsIDApLCByZ2JhKDAsIDAsIDAsIDAuNCkpLCB1cmwoXCJodHRwczovL2hkLnVuc3BsYXNoLmNvbS9waG90by0xNDU4NTMwOTcwODY3LWFhYTM3MDBlOTY2ZFwiKSBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgfVxuXG4udGltZWxpbmUtaXRlbTpudGgtY2hpbGQoNSkgLnRpbWVsaW5lLWltZy1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLCAwLCAwLCAwKSwgcmdiYSgwLCAwLCAwLCAwLjQpKSwgdXJsKFwiaHR0cHM6Ly9oZC51bnNwbGFzaC5jb20vcGhvdG8tMTQ0NDA5MzgyNjM0OS05Y2U4YzYyMmY0ZjNcIikgY2VudGVyIGNlbnRlciBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IH1cblxuLnRpbWVsaW5lLWl0ZW06bnRoLWNoaWxkKDYpIC50aW1lbGluZS1pbWctaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMCwgMCwgMCwgMCksIHJnYmEoMCwgMCwgMCwgMC40KSksIHVybChcImh0dHBzOi8vaGQudW5zcGxhc2guY29tL3Bob3RvLTE0NzE0Nzk5MTcxOTMtZjAwOTU1MjU2MjU3XCIpIGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyOyB9XG5cbi50aW1lbGluZS1pdGVtOm50aC1jaGlsZCg4KSAudGltZWxpbmUtaW1nLWhlYWRlciB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDAsIDAsIDAsIDApLCByZ2JhKDAsIDAsIDAsIDAuNCkpLCB1cmwoXCJodHRwczovL2hkLnVuc3BsYXNoLmNvbS9waG90by0xNDY2ODQwNzg3MDIyLTQ4ZTBlYzA0OGM4YVwiKSBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgfVxuXG4udGltZWxpbmUtaXRlbTpudGgtY2hpbGQoMTApIC50aW1lbGluZS1pbWctaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoMCwgMCwgMCwgMCksIHJnYmEoMCwgMCwgMCwgMC40KSksIHVybChcImh0dHBzOi8vaGQudW5zcGxhc2guY29tL3Bob3RvLTE0NDc2Mzk3MDM3NTgtZjUyNWYzNjQ1NmJmXCIpIGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyOyB9XG5cbi50aW1lbGluZS1pdGVtOm50aC1jaGlsZCgxMSkgLnRpbWVsaW5lLWltZy1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLCAwLCAwLCAwKSwgcmdiYSgwLCAwLCAwLCAwLjQpKSwgdXJsKFwiaHR0cHM6Ly9oZC51bnNwbGFzaC5jb20vcGhvdG8tMTQ2OTQyOTk3ODQwMC0wODJlZWM3MjVhZDVcIikgY2VudGVyIGNlbnRlciBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IH1cblxuLnRpbWVsaW5lLWltZy1oZWFkZXIge1xuICBoZWlnaHQ6IDIwMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7IH1cbiAgLnRpbWVsaW5lLWltZy1oZWFkZXIgaDIge1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDVweDtcbiAgICBsZWZ0OiAyMHB4OyB9XG5cbmJsb2NrcXVvdGUge1xuICBtYXJnaW4tdG9wOiAzMHB4O1xuICBjb2xvcjogIzc1NzU3NTtcbiAgYm9yZGVyLWxlZnQtY29sb3I6ICMzRjUxQjU7XG4gIHBhZGRpbmc6IDAgMjBweDsgfVxuXG4uZGF0ZSB7XG4gIGJhY2tncm91bmQ6ICMzRjUxQjU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29sb3I6ICNGRkZGRkY7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDsgfVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgLnRpbWVsaW5lOjpiZWZvcmUge1xuICAgIGxlZnQ6IDUwcHg7IH1cbiAgLnRpbWVsaW5lIC50aW1lbGluZS1pbWcge1xuICAgIGxlZnQ6IDUwcHg7IH1cbiAgLnRpbWVsaW5lIC50aW1lbGluZS1jb250ZW50IHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDcwcHg7IH1cbiAgLnRpbWVsaW5lIC50aW1lbGluZS1pdGVtOm50aC1jaGlsZChldmVuKSAudGltZWxpbmUtY29udGVudCB7XG4gICAgZmxvYXQ6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogMzVweDsgfVxuICAgIC50aW1lbGluZSAudGltZWxpbmUtaXRlbTpudGgtY2hpbGQoZXZlbikgLnRpbWVsaW5lLWNvbnRlbnQgLmRhdGUge1xuICAgICAgcmlnaHQ6IDA7XG4gICAgICBsZWZ0OiBhdXRvOyB9XG4gICAgLnRpbWVsaW5lIC50aW1lbGluZS1pdGVtOm50aC1jaGlsZChldmVuKSAudGltZWxpbmUtY29udGVudCAuaW5uZXItY29udGVudCB7XG4gICAgICBtYXJnaW4tdG9wOiAzNXB4OyB9XG4gIC50aW1lbGluZSAudGltZWxpbmUtaXRlbTpudGgtY2hpbGQob2RkKSAudGltZWxpbmUtY29udGVudCB7XG4gICAgbWFyZ2luLXRvcDogMzVweDsgfVxuICAgIC50aW1lbGluZSAudGltZWxpbmUtaXRlbTpudGgtY2hpbGQob2RkKSAudGltZWxpbmUtY29udGVudCAuaW5uZXItY29udGVudCB7XG4gICAgICBtYXJnaW4tdG9wOiA2NXB4O1xuICAgICAgbWFyZ2luLXJpZ2h0OiAwOyB9XG4gICAgLnRpbWVsaW5lIC50aW1lbGluZS1pdGVtOm50aC1jaGlsZChvZGQpIC50aW1lbGluZS1jb250ZW50OjphZnRlciB7XG4gICAgICBjb250ZW50OiAnJztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHRvcDogMzBweDtcbiAgICAgIGxlZnQ6IC0xNXB4O1xuICAgICAgYm9yZGVyLXdpZHRoOiAxMHB4IDE1cHggMTBweCAwO1xuICAgICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCAjZjVmNWY1IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50OyB9IH1cblxuLmlubmVyLWNvbnRlbnQge1xuICBtYXJnaW4tdG9wOiAzNXB4O1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5OyB9XG5cbi5kaXNhYmxlZC1lbGVtZW50IHtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gIGZpbHRlcjogb3BhY2l0eSgzNSUpOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/timeline/timeline1/timeline1.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/timeline/timeline1/timeline1.component.ts ***!
  \**********************************************************************/
/*! exports provided: Timeline1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Timeline1Component", function() { return Timeline1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Timeline1Component = /** @class */ (function () {
    function Timeline1Component() {
    }
    Timeline1Component.prototype.ngOnInit = function () { };
    Timeline1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timeline1',
            template: __webpack_require__(/*! raw-loader!./timeline1.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/timeline/timeline1/timeline1.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./timeline1.component.scss */ "./src/app/components/timeline/timeline1/timeline1.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Timeline1Component);
    return Timeline1Component;
}());



/***/ }),

/***/ "./src/app/components/timeline/timeline2/timeline2.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/components/timeline/timeline2/timeline2.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGltZWxpbmUvdGltZWxpbmUyL3RpbWVsaW5lMi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/timeline/timeline2/timeline2.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/components/timeline/timeline2/timeline2.component.ts ***!
  \**********************************************************************/
/*! exports provided: Timeline2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Timeline2Component", function() { return Timeline2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Timeline2Component = /** @class */ (function () {
    function Timeline2Component() {
    }
    Timeline2Component.prototype.ngOnInit = function () { };
    Timeline2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timeline2',
            template: __webpack_require__(/*! raw-loader!./timeline2.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/timeline/timeline2/timeline2.component.html"),
            styles: [__webpack_require__(/*! ./timeline2.component.scss */ "./src/app/components/timeline/timeline2/timeline2.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Timeline2Component);
    return Timeline2Component;
}());



/***/ })

}]);
//# sourceMappingURL=components-timeline-timeline-module.js.map