(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-dashboard-dashboard-module"],{

/***/ "./node_modules/browser-split/index.js":
/*!*********************************************!*\
  !*** ./node_modules/browser-split/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Cross-Browser Split 1.1.1
 * Copyright 2007-2012 Steven Levithan <stevenlevithan.com>
 * Available under the MIT License
 * ECMAScript compliant, uniform cross-browser split method
 */

/**
 * Splits a string into an array of strings using a regex or string separator. Matches of the
 * separator are not included in the result array. However, if `separator` is a regex that contains
 * capturing groups, backreferences are spliced into the result each time `separator` is matched.
 * Fixes browser bugs compared to the native `String.prototype.split` and can be used reliably
 * cross-browser.
 * @param {String} str String to split.
 * @param {RegExp|String} separator Regex or string to use for separating the string.
 * @param {Number} [limit] Maximum number of items to include in the result array.
 * @returns {Array} Array of substrings.
 * @example
 *
 * // Basic use
 * split('a b c d', ' ');
 * // -> ['a', 'b', 'c', 'd']
 *
 * // With limit
 * split('a b c d', ' ', 2);
 * // -> ['a', 'b']
 *
 * // Backreferences in result array
 * split('..word1 word2..', /([a-z]+)(\d+)/i);
 * // -> ['..', 'word', '1', ' ', 'word', '2', '..']
 */
module.exports = (function split(undef) {

  var nativeSplit = String.prototype.split,
    compliantExecNpcg = /()??/.exec("")[1] === undef,
    // NPCG: nonparticipating capturing group
    self;

  self = function(str, separator, limit) {
    // If `separator` is not a regex, use `nativeSplit`
    if (Object.prototype.toString.call(separator) !== "[object RegExp]") {
      return nativeSplit.call(str, separator, limit);
    }
    var output = [],
      flags = (separator.ignoreCase ? "i" : "") + (separator.multiline ? "m" : "") + (separator.extended ? "x" : "") + // Proposed for ES6
      (separator.sticky ? "y" : ""),
      // Firefox 3+
      lastLastIndex = 0,
      // Make `global` and avoid `lastIndex` issues by working with a copy
      separator = new RegExp(separator.source, flags + "g"),
      separator2, match, lastIndex, lastLength;
    str += ""; // Type-convert
    if (!compliantExecNpcg) {
      // Doesn't need flags gy, but they don't hurt
      separator2 = new RegExp("^" + separator.source + "$(?!\\s)", flags);
    }
    /* Values for `limit`, per the spec:
     * If undefined: 4294967295 // Math.pow(2, 32) - 1
     * If 0, Infinity, or NaN: 0
     * If positive number: limit = Math.floor(limit); if (limit > 4294967295) limit -= 4294967296;
     * If negative number: 4294967296 - Math.floor(Math.abs(limit))
     * If other: Type-convert, then use the above rules
     */
    limit = limit === undef ? -1 >>> 0 : // Math.pow(2, 32) - 1
    limit >>> 0; // ToUint32(limit)
    while (match = separator.exec(str)) {
      // `separator.lastIndex` is not reliable cross-browser
      lastIndex = match.index + match[0].length;
      if (lastIndex > lastLastIndex) {
        output.push(str.slice(lastLastIndex, match.index));
        // Fix browsers whose `exec` methods don't consistently return `undefined` for
        // nonparticipating capturing groups
        if (!compliantExecNpcg && match.length > 1) {
          match[0].replace(separator2, function() {
            for (var i = 1; i < arguments.length - 2; i++) {
              if (arguments[i] === undef) {
                match[i] = undef;
              }
            }
          });
        }
        if (match.length > 1 && match.index < str.length) {
          Array.prototype.push.apply(output, match.slice(1));
        }
        lastLength = match[0].length;
        lastLastIndex = lastIndex;
        if (output.length >= limit) {
          break;
        }
      }
      if (separator.lastIndex === match.index) {
        separator.lastIndex++; // Avoid an infinite loop
      }
    }
    if (lastLastIndex === str.length) {
      if (lastLength || !separator.test("")) {
        output.push("");
      }
    } else {
      output.push(str.slice(lastLastIndex));
    }
    return output.length > limit ? output.slice(0, limit) : output;
  };

  return self;
})();


/***/ }),

/***/ "./node_modules/chartjs-plugin-streaming/dist/chartjs-plugin-streaming.js":
/*!********************************************************************************!*\
  !*** ./node_modules/chartjs-plugin-streaming/dist/chartjs-plugin-streaming.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*!
 * chartjs-plugin-streaming v1.8.0
 * https://nagix.github.io/chartjs-plugin-streaming
 * (c) 2019 Akihiko Kusanagi
 * Released under the MIT license
 */
(function (global, factory) {
 true ? module.exports = factory(__webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js"), __webpack_require__(/*! moment */ "./node_modules/moment/moment.js")) :
undefined;
}(this, function (Chart, moment) { 'use strict';

Chart = Chart && Chart.hasOwnProperty('default') ? Chart['default'] : Chart;
moment = moment && moment.hasOwnProperty('default') ? moment['default'] : moment;

var helpers = Chart.helpers;

var cancelAnimFrame = (function() {
	if (typeof window !== 'undefined') {
		return window.cancelAnimationFrame ||
			window.webkitCancelAnimationFrame ||
			window.mozCancelAnimationFrame ||
			window.oCancelAnimationFrame ||
			window.msCancelAnimationFrame ||
			function(id) {
				return window.clearTimeout(id);
			};
	}
}());

var StreamingHelper = {

	startFrameRefreshTimer: function(context, func) {
		if (!context.frameRequestID) {
			var frameRefresh = function() {
				func();
				context.frameRequestID = helpers.requestAnimFrame.call(window, frameRefresh);
			};
			context.frameRequestID = helpers.requestAnimFrame.call(window, frameRefresh);
		}
	},

	stopFrameRefreshTimer: function(context) {
		var frameRequestID = context.frameRequestID;

		if (frameRequestID) {
			cancelAnimFrame.call(window, frameRequestID);
			delete context.frameRequestID;
		}
	}

};

var helpers$1 = Chart.helpers;
var canvasHelpers = helpers$1.canvas;
var scaleService = Chart.scaleService;
var TimeScale = scaleService.getScaleConstructor('time');

scaleService.getScaleConstructor = function(type) {
	// For backwards compatibility
	if (type === 'time') {
		type = 'realtime';
	}
	return this.constructors.hasOwnProperty(type) ? this.constructors[type] : undefined;
};

// For Chart.js 2.7.x backward compatibility
var defaultAdapter = {
	// Ported from Chart.js 2.8.0-rc.1 35273ee
	parse: function(value, format) {
		if (typeof value === 'string' && typeof format === 'string') {
			value = moment(value, format);
		} else if (!(value instanceof moment)) {
			value = moment(value);
		}
		return value.isValid() ? value.valueOf() : null;
	}
};

// Ported from Chart.js 2.8.0-rc.1 35273ee. Modified for Chart.js 2.7.x backward compatibility.
function toTimestamp(scale, input) {
	var adapter = scale._adapter || defaultAdapter;
	var options = scale.options.time;
	var parser = options.parser;
	var format = parser || options.format;
	var value = input;

	if (typeof parser === 'function') {
		value = parser(value);
	}

	// Only parse if its not a timestamp already
	if (typeof value !== 'number' && !(value instanceof Number) || !isFinite(value)) {
		value = typeof format === 'string'
			? adapter.parse(value, format)
			: adapter.parse(value);
	}

	if (value !== null) {
		return +value;
	}

	// Labels are in an incompatible format and no `parser` has been provided.
	// The user might still use the deprecated `format` option for parsing.
	if (!parser && typeof format === 'function') {
		value = format(input);

		// `format` could return something else than a timestamp, if so, parse it
		if (typeof value !== 'number' && !(value instanceof Number) || !isFinite(value)) {
			value = adapter.parse(value);
		}
	}

	return value;
}

// Ported from Chart.js 2.8.0-rc.1 35273ee
function parse(scale, input) {
	if (helpers$1.isNullOrUndef(input)) {
		return null;
	}

	var options = scale.options.time;
	var value = toTimestamp(scale, scale.getRightValue(input));
	if (value === null) {
		return value;
	}

	if (options.round) {
		value = +scale._adapter.startOf(value, options.round);
	}

	return value;
}

function resolveOption(scale, key) {
	var realtimeOpts = scale.options.realtime;
	var streamingOpts = scale.chart.options.plugins.streaming;
	return helpers$1.valueOrDefault(realtimeOpts[key], streamingOpts[key]);
}

var datasetPropertyKeys = [
	'pointBackgroundColor',
	'pointBorderColor',
	'pointBorderWidth',
	'pointRadius',
	'pointRotation',
	'pointStyle',
	'pointHitRadius',
	'pointHoverBackgroundColor',
	'pointHoverBorderColor',
	'pointHoverBorderWidth',
	'pointHoverRadius',
	'backgroundColor',
	'borderColor',
	'borderSkipped',
	'borderWidth',
	'hoverBackgroundColor',
	'hoverBorderColor',
	'hoverBorderWidth',
	'hoverRadius',
	'hitRadius',
	'radius',
	'rotation'
];

function refreshData(scale) {
	var chart = scale.chart;
	var id = scale.id;
	var duration = resolveOption(scale, 'duration');
	var delay = resolveOption(scale, 'delay');
	var ttl = resolveOption(scale, 'ttl');
	var pause = resolveOption(scale, 'pause');
	var onRefresh = resolveOption(scale, 'onRefresh');
	var max = scale.max;
	var min = Date.now() - (isNaN(ttl) ? duration + delay : ttl);
	var meta, data, length, i, start, count, removalRange;

	if (onRefresh) {
		onRefresh(chart);
	}

	// Remove old data
	chart.data.datasets.forEach(function(dataset, datasetIndex) {
		meta = chart.getDatasetMeta(datasetIndex);
		if (id === meta.xAxisID || id === meta.yAxisID) {
			data = dataset.data;
			length = data.length;

			if (pause) {
				// If the scale is paused, preserve the visible data points
				for (i = 0; i < length; ++i) {
					if (!(scale._getTimeForIndex(i, datasetIndex) < max)) {
						break;
					}
				}
				start = i + 2;
			} else {
				start = 0;
			}

			for (i = start; i < length; ++i) {
				if (!(scale._getTimeForIndex(i, datasetIndex) <= min)) {
					break;
				}
			}
			count = i - start;
			if (isNaN(ttl)) {
				// Keep the last two data points outside the range not to affect the existing bezier curve
				count = Math.max(count - 2, 0);
			}

			data.splice(start, count);
			datasetPropertyKeys.forEach(function(key) {
				if (dataset.hasOwnProperty(key) && helpers$1.isArray(dataset[key])) {
					dataset[key].splice(start, count);
				}
			});
			helpers$1.each(dataset.datalabels, function(value) {
				if (helpers$1.isArray(value)) {
					value.splice(start, count);
				}
			});
			if (typeof data[0] !== 'object') {
				removalRange = {
					start: start,
					count: count
				};
			}
		}
	});
	if (removalRange) {
		chart.data.labels.splice(removalRange.start, removalRange.count);
	}

	chart.update({
		preservation: true
	});
}

function stopDataRefreshTimer(scale) {
	var realtime = scale.realtime;
	var refreshTimerID = realtime.refreshTimerID;

	if (refreshTimerID) {
		clearInterval(refreshTimerID);
		delete realtime.refreshTimerID;
		delete realtime.refreshInterval;
	}
}

function startDataRefreshTimer(scale) {
	var realtime = scale.realtime;
	var interval = resolveOption(scale, 'refresh');

	realtime.refreshTimerID = setInterval(function() {
		var newInterval = resolveOption(scale, 'refresh');

		refreshData(scale);
		if (realtime.refreshInterval !== newInterval && !isNaN(newInterval)) {
			stopDataRefreshTimer(scale);
			startDataRefreshTimer(scale);
		}
	}, interval);
	realtime.refreshInterval = interval;
}

var transitionKeys = {
	x: {
		data: ['x', 'controlPointPreviousX', 'controlPointNextX'],
		dataset: ['x'],
		tooltip: ['x', 'caretX']
	},
	y: {
		data: ['y', 'controlPointPreviousY', 'controlPointNextY'],
		dataset: ['y'],
		tooltip: ['y', 'caretY']
	}
};

function transition(element, keys, translate) {
	var start = element._start || {};
	var view = element._view || {};
	var model = element._model || {};
	var i, ilen;

	for (i = 0, ilen = keys.length; i < ilen; ++i) {
		var key = keys[i];
		if (start.hasOwnProperty(key)) {
			start[key] -= translate;
		}
		if (view.hasOwnProperty(key) && view !== start) {
			view[key] -= translate;
		}
		if (model.hasOwnProperty(key) && model !== view) {
			model[key] -= translate;
		}
	}
}

function scroll(scale) {
	var chart = scale.chart;
	var realtime = scale.realtime;
	var duration = resolveOption(scale, 'duration');
	var delay = resolveOption(scale, 'delay');
	var id = scale.id;
	var tooltip = chart.tooltip;
	var activeTooltip = tooltip._active;
	var now = Date.now();
	var length, keys, offset, meta, elements, i, ilen;

	if (scale.isHorizontal()) {
		length = scale.width;
		keys = transitionKeys.x;
	} else {
		length = scale.height;
		keys = transitionKeys.y;
	}
	offset = length * (now - realtime.head) / duration;

	if (scale.options.ticks.reverse) {
		offset = -offset;
	}

	// Shift all the elements leftward or upward
	helpers$1.each(chart.data.datasets, function(dataset, datasetIndex) {
		meta = chart.getDatasetMeta(datasetIndex);
		if (id === meta.xAxisID || id === meta.yAxisID) {
			elements = meta.data || [];

			for (i = 0, ilen = elements.length; i < ilen; ++i) {
				transition(elements[i], keys.data, offset);
			}

			if (meta.dataset) {
				transition(meta.dataset, keys.dataset, offset);
			}
		}
	});

	// Shift tooltip leftward or upward
	if (activeTooltip && activeTooltip[0]) {
		meta = chart.getDatasetMeta(activeTooltip[0]._datasetIndex);
		if (id === meta.xAxisID || id === meta.yAxisID) {
			transition(tooltip, keys.tooltip, offset);
		}
	}

	scale.max = scale._table[1].time = now - delay;
	scale.min = scale._table[0].time = scale.max - duration;

	realtime.head = now;
}

var defaultConfig = {
	position: 'bottom',
	distribution: 'linear',
	bounds: 'data',
	adapters: {},
	time: {
		parser: false, // false == a pattern string from http://momentjs.com/docs/#/parsing/string-format/ or a custom callback that converts its argument to a moment
		format: false, // DEPRECATED false == date objects, moment object, callback or a pattern string from http://momentjs.com/docs/#/parsing/string-format/
		unit: false, // false == automatic or override with week, month, year, etc.
		round: false, // none, or override with week, month, year, etc.
		displayFormat: false, // DEPRECATED
		isoWeekday: false, // override week start day - see http://momentjs.com/docs/#/get-set/iso-weekday/
		minUnit: 'millisecond',

		// defaults to unit's corresponding unitFormat below or override using pattern string from http://momentjs.com/docs/#/displaying/format/
		displayFormats: {
			millisecond: 'h:mm:ss.SSS a',
			second: 'h:mm:ss a',
			minute: 'h:mm a',
			hour: 'hA',
			day: 'MMM D',
			week: 'll',
			month: 'MMM YYYY',
			quarter: '[Q]Q - YYYY',
			year: 'YYYY'
		},
	},
	realtime: {},
	ticks: {
		autoSkip: false,
		source: 'auto',
		major: {
			enabled: true
		}
	}
};

var RealTimeScale = TimeScale.extend({
	initialize: function() {
		var me = this;

		TimeScale.prototype.initialize.apply(me, arguments);

		// For backwards compatibility
		if (me.options.type === 'time' && !me.chart.options.plugins.streaming) {
			return;
		}

		me.realtime = me.realtime || {};

		startDataRefreshTimer(me);
	},

	update: function() {
		var me = this;
		var realtime = me.realtime;

		// For backwards compatibility
		if (me.options.type === 'time' && !me.chart.options.plugins.streaming) {
			return TimeScale.prototype.update.apply(me, arguments);
		}

		if (resolveOption(me, 'pause')) {
			StreamingHelper.stopFrameRefreshTimer(realtime);
		} else {
			StreamingHelper.startFrameRefreshTimer(realtime, function() {
				scroll(me);
			});
			realtime.head = Date.now();
		}

		return TimeScale.prototype.update.apply(me, arguments);
	},

	buildTicks: function() {
		var me = this;
		var options = me.options;

		// For backwards compatibility
		if (options.type === 'time' && !me.chart.options.plugins.streaming) {
			return TimeScale.prototype.buildTicks.apply(me, arguments);
		}

		var timeOpts = options.time;
		var majorTicksOpts = options.ticks.major;
		var duration = resolveOption(me, 'duration');
		var delay = resolveOption(me, 'delay');
		var refresh = resolveOption(me, 'refresh');
		var bounds = options.bounds;
		var distribution = options.distribution;
		var offset = options.offset;
		var minTime = timeOpts.min;
		var maxTime = timeOpts.max;
		var majorEnabled = majorTicksOpts.enabled;
		var max = me.realtime.head - delay;
		var min = max - duration;
		var maxArray = [max + refresh, max];
		var ticks;

		options.bounds = undefined;
		options.distribution = 'linear';
		options.offset = false;
		timeOpts.min = -1e15;
		timeOpts.max = 1e15;
		majorTicksOpts.enabled = true;

		Object.defineProperty(me, 'min', {
			get: function() {
				return min;
			},
			set: helpers$1.noop
		});
		Object.defineProperty(me, 'max', {
			get: function() {
				return maxArray.shift();
			},
			set: helpers$1.noop
		});

		ticks = TimeScale.prototype.buildTicks.apply(me, arguments);

		delete me.min;
		delete me.max;

		me.min = min;
		me.max = max;
		options.bounds = bounds;
		options.distribution = distribution;
		options.offset = offset;
		timeOpts.min = minTime;
		timeOpts.max = maxTime;
		majorTicksOpts.enabled = majorEnabled;
		me._table = [{time: min, pos: 0}, {time: max, pos: 1}];

		return ticks;
	},

	fit: function() {
		var me = this;
		var options = me.options;

		TimeScale.prototype.fit.apply(me, arguments);

		// For backwards compatibility
		if (options.type === 'time' && !me.chart.options.plugins.streaming) {
			return;
		}

		if (options.ticks.display && options.display && me.isHorizontal()) {
			me.paddingLeft = 3;
			me.paddingRight = 3;
			me.handleMargins();
		}
	},

	draw: function(chartArea) {
		var me = this;
		var chart = me.chart;

		// For backwards compatibility
		if (me.options.type === 'time' && !chart.options.plugins.streaming) {
			TimeScale.prototype.draw.apply(me, arguments);
			return;
		}

		var context = me.ctx;
		var	clipArea = me.isHorizontal() ?
			{
				left: chartArea.left,
				top: 0,
				right: chartArea.right,
				bottom: chart.height
			} : {
				left: 0,
				top: chartArea.top,
				right: chart.width,
				bottom: chartArea.bottom
			};

		// Clip and draw the scale
		canvasHelpers.clipArea(context, clipArea);
		TimeScale.prototype.draw.apply(me, arguments);
		canvasHelpers.unclipArea(context);
	},

	destroy: function() {
		var me = this;

		// For backwards compatibility
		if (me.options.type === 'time' && !me.chart.options.plugins.streaming) {
			return;
		}

		StreamingHelper.stopFrameRefreshTimer(me.realtime);
		stopDataRefreshTimer(me);
	},

	/*
	 * @private
	 */
	_getTimeForIndex: function(index, datasetIndex) {
		var me = this;
		var timestamps = me._timestamps;
		var time = timestamps.datasets[datasetIndex][index];
		var value;

		if (helpers$1.isNullOrUndef(time)) {
			value = me.chart.data.datasets[datasetIndex].data[index];
			if (helpers$1.isObject(value)) {
				time = parse(me, value);
			} else {
				time = parse(me, timestamps.labels[index]);
			}
		}

		return time;
	}
});

scaleService.registerScaleType('realtime', RealTimeScale, defaultConfig);

var helpers$2 = Chart.helpers;
var canvasHelpers$1 = helpers$2.canvas;

Chart.defaults.global.plugins.streaming = {
	duration: 10000,
	delay: 0,
	frameRate: 30,
	refresh: 1000,
	onRefresh: null,
	pause: false,
	ttl: undefined
};

/**
 * Update the chart keeping the current animation but suppressing a new one
 * @param {object} config - animation options
 */
function update(config) {
	var me = this;
	var preservation = config && config.preservation;
	var tooltip, lastActive, tooltipLastActive, lastMouseEvent;

	if (preservation) {
		tooltip = me.tooltip;
		lastActive = me.lastActive;
		tooltipLastActive = tooltip._lastActive;
		me._bufferedRender = true;
	}

	Chart.prototype.update.apply(me, arguments);

	if (preservation) {
		me._bufferedRender = false;
		me._bufferedRequest = null;
		me.lastActive = lastActive;
		tooltip._lastActive = tooltipLastActive;

		if (me.animating) {
			// If the chart is animating, keep it until the duration is over
			Chart.animationService.animations.forEach(function(animation) {
				if (animation.chart === me) {
					me.render({
						duration: (animation.numSteps - animation.currentStep) * 16.66
					});
				}
			});
		} else {
			// If the chart is not animating, make sure that all elements are at the final positions
			me.data.datasets.forEach(function(dataset, datasetIndex) {
				me.getDatasetMeta(datasetIndex).controller.transition(1);
			});
		}

		if (tooltip._active) {
			tooltip.update(true);
		}

		lastMouseEvent = me.streaming.lastMouseEvent;
		if (lastMouseEvent) {
			me.eventHandler(lastMouseEvent);
		}
	}
}

// Draw chart at frameRate
function drawChart(chart) {
	var streaming = chart.streaming;
	var frameRate = chart.options.plugins.streaming.frameRate;
	var frameDuration = 1000 / (Math.max(frameRate, 0) || 30);
	var next = streaming.lastDrawn + frameDuration || 0;
	var now = Date.now();
	var lastMouseEvent = streaming.lastMouseEvent;

	if (next <= now) {
		// Draw only when animation is inactive
		if (!chart.animating && !chart.tooltip._start) {
			chart.draw();
		}
		if (lastMouseEvent) {
			chart.eventHandler(lastMouseEvent);
		}
		streaming.lastDrawn = (next + frameDuration > now) ? next : now;
	}
}

var StreamingPlugin = {
	id: 'streaming',

	beforeInit: function(chart) {
		var streaming = chart.streaming = chart.streaming || {};
		var canvas = streaming.canvas = chart.canvas;
		var mouseEventListener = streaming.mouseEventListener = function(event) {
			var pos = helpers$2.getRelativePosition(event, chart);
			streaming.lastMouseEvent = {
				type: 'mousemove',
				chart: chart,
				native: event,
				x: pos.x,
				y: pos.y
			};
		};

		canvas.addEventListener('mousedown', mouseEventListener);
		canvas.addEventListener('mouseup', mouseEventListener);
	},

	afterInit: function(chart) {
		chart.update = update;

		if (chart.resetZoom) {
			Chart.Zoom.updateResetZoom(chart);
		}
	},

	beforeUpdate: function(chart) {
		var chartOpts = chart.options;
		var scalesOpts = chartOpts.scales;

		if (scalesOpts) {
			scalesOpts.xAxes.concat(scalesOpts.yAxes).forEach(function(scaleOpts) {
				if (scaleOpts.type === 'realtime' || scaleOpts.type === 'time') {
					// Allow Bézier control to be outside the chart
					chartOpts.elements.line.capBezierPoints = false;
				}
			});
		}
		return true;
	},

	afterUpdate: function(chart, options) {
		var streaming = chart.streaming;
		var pause = true;

		// if all scales are paused, stop refreshing frames
		helpers$2.each(chart.scales, function(scale) {
			if (scale instanceof RealTimeScale) {
				pause &= helpers$2.valueOrDefault(scale.options.realtime.pause, options.pause);
			}
		});
		if (pause) {
			StreamingHelper.stopFrameRefreshTimer(streaming);
		} else {
			StreamingHelper.startFrameRefreshTimer(streaming, function() {
				drawChart(chart);
			});
		}
	},

	beforeDatasetDraw: function(chart, args) {
		var meta = args.meta;
		var chartArea = chart.chartArea;
		var clipArea = {
			left: 0,
			top: 0,
			right: chart.width,
			bottom: chart.height
		};
		if (meta.xAxisID && meta.controller.getScaleForId(meta.xAxisID) instanceof RealTimeScale) {
			clipArea.left = chartArea.left;
			clipArea.right = chartArea.right;
		}
		if (meta.yAxisID && meta.controller.getScaleForId(meta.yAxisID) instanceof RealTimeScale) {
			clipArea.top = chartArea.top;
			clipArea.bottom = chartArea.bottom;
		}
		canvasHelpers$1.clipArea(chart.ctx, clipArea);
		return true;
	},

	afterDatasetDraw: function(chart) {
		canvasHelpers$1.unclipArea(chart.ctx);
	},

	beforeEvent: function(chart, event) {
		var streaming = chart.streaming;

		if (event.type === 'mousemove') {
			// Save mousemove event for reuse
			streaming.lastMouseEvent = event;
		} else if (event.type === 'mouseout') {
			// Remove mousemove event
			delete streaming.lastMouseEvent;
		}
		return true;
	},

	destroy: function(chart) {
		var streaming = chart.streaming;
		var canvas = streaming.canvas;
		var mouseEventListener = streaming.mouseEventListener;

		StreamingHelper.stopFrameRefreshTimer(streaming);

		canvas.removeEventListener('mousedown', mouseEventListener);
		canvas.removeEventListener('mouseup', mouseEventListener);

		helpers$2.each(chart.scales, function(scale) {
			if (scale instanceof RealTimeScale) {
				scale.destroy();
			}
		});
	}
};

var helpers$3 = Chart.helpers;

// Ported from chartjs-plugin-zoom 0.7.0 3c187b7
var zoomNS = Chart.Zoom = Chart.Zoom || {};

// Ported from chartjs-plugin-zoom 0.7.0 3c187b7
zoomNS.zoomFunctions = zoomNS.zoomFunctions || {};
zoomNS.panFunctions = zoomNS.panFunctions || {};

// Ported from chartjs-plugin-zoom 0.7.0 3c187b7
function rangeMaxLimiter(zoomPanOptions, newMax) {
	if (zoomPanOptions.scaleAxes && zoomPanOptions.rangeMax &&
			!helpers$3.isNullOrUndef(zoomPanOptions.rangeMax[zoomPanOptions.scaleAxes])) {
		var rangeMax = zoomPanOptions.rangeMax[zoomPanOptions.scaleAxes];
		if (newMax > rangeMax) {
			newMax = rangeMax;
		}
	}
	return newMax;
}

// Ported from chartjs-plugin-zoom 0.7.0 3c187b7
function rangeMinLimiter(zoomPanOptions, newMin) {
	if (zoomPanOptions.scaleAxes && zoomPanOptions.rangeMin &&
			!helpers$3.isNullOrUndef(zoomPanOptions.rangeMin[zoomPanOptions.scaleAxes])) {
		var rangeMin = zoomPanOptions.rangeMin[zoomPanOptions.scaleAxes];
		if (newMin < rangeMin) {
			newMin = rangeMin;
		}
	}
	return newMin;
}

function zoomRealTimeScale(scale, zoom, center, zoomOptions) {
	var realtimeOpts = scale.options.realtime;
	var streamingOpts = scale.chart.options.plugins.streaming;
	var duration = helpers$3.valueOrDefault(realtimeOpts.duration, streamingOpts.duration);
	var delay = helpers$3.valueOrDefault(realtimeOpts.delay, streamingOpts.delay);
	var newDuration = duration * (2 - zoom);
	var maxPercent, limitedDuration;

	if (scale.isHorizontal()) {
		maxPercent = (scale.right - center.x) / (scale.right - scale.left);
	} else {
		maxPercent = (scale.bottom - center.y) / (scale.bottom - scale.top);
	}
	if (zoom < 1) {
		limitedDuration = rangeMaxLimiter(zoomOptions, newDuration);
	} else {
		limitedDuration = rangeMinLimiter(zoomOptions, newDuration);
	}
	realtimeOpts.duration = limitedDuration;
	realtimeOpts.delay = delay + maxPercent * (duration - limitedDuration);
}

function panRealTimeScale(scale, delta, panOptions) {
	var realtimeOpts = scale.options.realtime;
	var streamingOpts = scale.chart.options.plugins.streaming;
	var delay = helpers$3.valueOrDefault(realtimeOpts.delay, streamingOpts.delay);
	var newDelay = delay + (scale.getValueForPixel(delta) - scale.getValueForPixel(0));

	if (delta > 0) {
		realtimeOpts.delay = rangeMaxLimiter(panOptions, newDelay);
	} else {
		realtimeOpts.delay = rangeMinLimiter(panOptions, newDelay);
	}
}

zoomNS.zoomFunctions.realtime = zoomRealTimeScale;
zoomNS.panFunctions.realtime = panRealTimeScale;

function updateResetZoom(chart) {
	// For chartjs-plugin-zoom 0.6.6 backward compatibility
	var zoom = chart.$zoom || {_originalOptions: {}};

	var resetZoom = chart.resetZoom;
	var update = chart.update;
	var resetZoomAndUpdate = function() {
		helpers$3.each(chart.scales, function(scale) {
			var realtimeOptions = scale.options.realtime;
			var originalOptions = zoom._originalOptions[scale.id] || scale.originalOptions;

			if (realtimeOptions) {
				if (originalOptions) {
					realtimeOptions.duration = originalOptions.realtime.duration;
					realtimeOptions.delay = originalOptions.realtime.delay;
				} else {
					delete realtimeOptions.duration;
					delete realtimeOptions.delay;
				}
			}
		});

		update.call(chart, {
			duration: 0
		});
	};

	chart.resetZoom = function() {
		chart.update = resetZoomAndUpdate;
		resetZoom();
		chart.update = update;
	};
}

zoomNS.updateResetZoom = updateResetZoom;

Chart.helpers.streaming = StreamingHelper;

Chart.plugins.register(StreamingPlugin);

return StreamingPlugin;

}));


/***/ }),

/***/ "./node_modules/class-list/index.js":
/*!******************************************!*\
  !*** ./node_modules/class-list/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// contains, add, remove, toggle
var indexof = __webpack_require__(/*! indexof */ "./node_modules/indexof/index.js")

module.exports = ClassList

function ClassList(elem) {
    var cl = elem.classList

    if (cl) {
        return cl
    }

    var classList = {
        add: add
        , remove: remove
        , contains: contains
        , toggle: toggle
        , toString: $toString
        , length: 0
        , item: item
    }

    return classList

    function add(token) {
        var list = getTokens()
        if (indexof(list, token) > -1) {
            return
        }
        list.push(token)
        setTokens(list)
    }

    function remove(token) {
        var list = getTokens()
            , index = indexof(list, token)

        if (index === -1) {
            return
        }

        list.splice(index, 1)
        setTokens(list)
    }

    function contains(token) {
        return indexof(getTokens(), token) > -1
    }

    function toggle(token) {
        if (contains(token)) {
            remove(token)
            return false
        } else {
            add(token)
            return true
        }
    }

    function $toString() {
        return elem.className
    }

    function item(index) {
        var tokens = getTokens()
        return tokens[index] || null
    }

    function getTokens() {
        var className = elem.className

        return filter(className.split(" "), isTruthy)
    }

    function setTokens(list) {
        var length = list.length

        elem.className = list.join(" ")
        classList.length = length

        for (var i = 0; i < list.length; i++) {
            classList[i] = list[i]
        }

        delete list[length]
    }
}

function filter (arr, fn) {
    var ret = []
    for (var i = 0; i < arr.length; i++) {
        if (fn(arr[i])) ret.push(arr[i])
    }
    return ret
}

function isTruthy(value) {
    return !!value
}


/***/ }),

/***/ "./node_modules/data-set/index.js":
/*!****************************************!*\
  !*** ./node_modules/data-set/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Weakmap = __webpack_require__(/*! weakmap */ "./node_modules/weakmap/weakmap.js")
var Individual = __webpack_require__(/*! individual */ "./node_modules/individual/index.js")

var datasetMap = Individual("__DATA_SET_WEAKMAP", Weakmap())

module.exports = DataSet

function DataSet(elem) {
    if (elem.dataset) {
        return elem.dataset
    }

    var hash = datasetMap.get(elem)

    if (!hash) {
        hash = createHash(elem)
        datasetMap.set(elem, hash)
    }

    return hash
}

function createHash(elem) {
    var attributes = elem.attributes
    var hash = {}

    if (attributes === null || attributes === undefined) {
        return hash
    }

    for (var i = 0; i < attributes.length; i++) {
        var attr = attributes[i]

        if (attr.name.substr(0,5) !== "data-") {
            continue
        }

        hash[attr.name.substr(5)] = attr.value
    }

    return hash
}


/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function $getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return $getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = $getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  var args = [];
  for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    ReflectApply(this.listener, this.target, args);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      if (typeof listener !== 'function') {
        throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
      }
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      if (typeof listener !== 'function') {
        throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
      }

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/ever/index.js":
/*!************************************!*\
  !*** ./node_modules/ever/index.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var EventEmitter = __webpack_require__(/*! events */ "./node_modules/events/events.js").EventEmitter;

module.exports = function (elem) {
    return new Ever(elem);
};

function Ever (elem) {
    this.element = elem;
}

Ever.prototype = new EventEmitter;

Ever.prototype.on = function (name, cb, useCapture) {
    if (!this._events) this._events = {};
    if (!this._events[name]) this._events[name] = [];
    this._events[name].push(cb);
    this.element.addEventListener(name, cb, useCapture || false);

    return this;
};
Ever.prototype.addListener = Ever.prototype.on;

Ever.prototype.removeListener = function (type, listener, useCapture) {
    if (!this._events) this._events = {};
    this.element.removeEventListener(type, listener, useCapture || false);
    
    var xs = this.listeners(type);
    var ix = xs.indexOf(listener);
    if (ix >= 0) xs.splice(ix, 1);

    return this;
};

Ever.prototype.removeAllListeners = function (type) {
    var self = this;
    function removeAll (t) {
        var xs = self.listeners(t);
        for (var i = 0; i < xs.length; i++) {
            self.removeListener(t, xs[i]);
        }
    }
    
    if (type) {
        removeAll(type)
    }
    else if (self._events) {
        for (var key in self._events) {
            if (key) removeAll(key);
        }
    }
    return EventEmitter.prototype.removeAllListeners.apply(self, arguments);
}

var initSignatures = __webpack_require__(/*! ./init.json */ "./node_modules/ever/init.json");

Ever.prototype.emit = function (name, ev) {
    if (typeof name === 'object') {
        ev = name;
        name = ev.type;
    }
    
    if (!isEvent(ev)) {
        var type = Ever.typeOf(name);
        
        var opts = ev || {};
        if (opts.type === undefined) opts.type = name;
        
        ev = document.createEvent(type + 's');
        var init = typeof ev['init' + type] === 'function'
            ? 'init' + type : 'initEvent'
        ;
        
        var sig = initSignatures[init];
        var used = {};
        var args = [];
        
        for (var i = 0; i < sig.length; i++) {
            var key = sig[i];
            args.push(opts[key]);
            used[key] = true;
        }
        ev[init].apply(ev, args);
        
        // attach remaining unused options to the object
        for (var key in opts) {
            if (!used[key]) ev[key] = opts[key];
        }
    }
    return this.element.dispatchEvent(ev);
};

function isEvent (ev) {
    var s = Object.prototype.toString.call(ev);
    return /\[object \S+Event\]/.test(s);
}

Ever.types = __webpack_require__(/*! ./types.json */ "./node_modules/ever/types.json");
Ever.typeOf = (function () {
    var types = {};
    for (var key in Ever.types) {
        var ts = Ever.types[key];
        for (var i = 0; i < ts.length; i++) {
            types[ts[i]] = key;
        }
    }
    
    return function (name) {
        return types[name] || 'Event';
    };
})();;


/***/ }),

/***/ "./node_modules/ever/init.json":
/*!*************************************!*\
  !*** ./node_modules/ever/init.json ***!
  \*************************************/
/*! exports provided: initEvent, initUIEvent, initMouseEvent, initMutationEvent, default */
/***/ (function(module) {

module.exports = {"initEvent":["type","canBubble","cancelable"],"initUIEvent":["type","canBubble","cancelable","view","detail"],"initMouseEvent":["type","canBubble","cancelable","view","detail","screenX","screenY","clientX","clientY","ctrlKey","altKey","shiftKey","metaKey","button","relatedTarget"],"initMutationEvent":["type","canBubble","cancelable","relatedNode","prevValue","newValue","attrName","attrChange"]};

/***/ }),

/***/ "./node_modules/ever/types.json":
/*!**************************************!*\
  !*** ./node_modules/ever/types.json ***!
  \**************************************/
/*! exports provided: MouseEvent, KeyBoardEvent, MutationEvent, HTMLEvent, UIEvent, default */
/***/ (function(module) {

module.exports = {"MouseEvent":["click","mousedown","mouseup","mouseover","mousemove","mouseout"],"KeyBoardEvent":["keydown","keyup","keypress"],"MutationEvent":["DOMSubtreeModified","DOMNodeInserted","DOMNodeRemoved","DOMNodeRemovedFromDocument","DOMNodeInsertedIntoDocument","DOMAttrModified","DOMCharacterDataModified"],"HTMLEvent":["load","unload","abort","error","select","change","submit","reset","focus","blur","resize","scroll"],"UIEvent":["DOMFocusIn","DOMFocusOut","DOMActivate"]};

/***/ }),

/***/ "./node_modules/foreach/index.js":
/*!***************************************!*\
  !*** ./node_modules/foreach/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {


var hasOwn = Object.prototype.hasOwnProperty;
var toString = Object.prototype.toString;

module.exports = function forEach (obj, fn, ctx) {
    if (toString.call(fn) !== '[object Function]') {
        throw new TypeError('iterator must be a function');
    }
    var l = obj.length;
    if (l === +l) {
        for (var i = 0; i < l; i++) {
            fn.call(ctx, obj[i], i, obj);
        }
    } else {
        for (var k in obj) {
            if (hasOwn.call(obj, k)) {
                fn.call(ctx, obj[k], k, obj);
            }
        }
    }
};



/***/ }),

/***/ "./node_modules/hyperscript/index.js":
/*!*******************************************!*\
  !*** ./node_modules/hyperscript/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var split = __webpack_require__(/*! browser-split */ "./node_modules/browser-split/index.js")
var ClassList = __webpack_require__(/*! class-list */ "./node_modules/class-list/index.js")
var DataSet = __webpack_require__(/*! data-set */ "./node_modules/data-set/index.js")

module.exports = h

function h() {
  var args = [].slice.call(arguments), e = null
  function item (l) {
    var r
    function parseClass (string) {
      var m = split(string, /([\.#]?[a-zA-Z0-9_-]+)/)
      forEach(m, function (v) {
        var s = v.substring(1,v.length)
        if(!v) return
        if(!e)
          e = document.createElement(v)
        else if (v[0] === '.')
          ClassList(e).add(s)
        else if (v[0] === '#')
          e.setAttribute('id', s)
      })
    }

    if(l == null)
      ;
    else if('string' === typeof l) {
      if(!e)
        parseClass(l)
      else
        e.appendChild(r = document.createTextNode(l))
    }
    else if('number' === typeof l
      || 'boolean' === typeof l
      || l instanceof Date
      || l instanceof RegExp ) {
        e.appendChild(r = document.createTextNode(l.toString()))
    }
    //there might be a better way to handle this...
    else if (isArray(l))
      forEach(l, item)
    else if(isNode(l))
      e.appendChild(r = l)
    else if(l instanceof Text)
      e.appendChild(r = l)
    else if ('object' === typeof l) {
      for (var k in l) {
        if('function' === typeof l[k]) {
          if(/^on\w+/.test(k)) {
            e.addEventListener
              ? e.addEventListener(k.substring(2), l[k])
              : e.attachEvent(k, l[k])
          } else {
            e[k] = l[k]()
            l[k](function (v) {
              e[k] = v
            })
          }
        }
        else if(k === 'style') {
          for (var s in l[k]) (function(s, v) {
            if('function' === typeof v) {
              e.style.setProperty(s, v())
              v(function (val) {
                e.style.setProperty(s, val)
              })
            } else
              e.style.setProperty(s, l[k][s])
          })(s, l[k][s])
        } else if (k.substr(0, 5) === "data-") {
          DataSet(e)[k.substr(5)] = l[k]
        } else {
          e[k] = l[k]
        }
      }
    } else if ('function' === typeof l) {
      //assume it's an observable!
      var v = l()
      e.appendChild(r = isNode(v) ? v : document.createTextNode(v))

      l(function (v) {
        if(isNode(v) && r.parentElement)
          r.parentElement.replaceChild(v, r), r = v
        else
          r.textContent = v
      })

    }

    return r
  }
  while(args.length)
    item(args.shift())

  return e
}

function isNode (el) {
  return el && el.nodeName && el.nodeType
}

function isText (el) {
  return el && el.nodeName === '#text' && el.nodeType == 3
}

function forEach (arr, fn) {
  if (arr.forEach) return arr.forEach(fn)
  for (var i = 0; i < arr.length; i++) fn(arr[i], i)
}

function isArray (arr) {
  return Object.prototype.toString.call(arr) == '[object Array]'
}


/***/ }),

/***/ "./node_modules/indexof/index.js":
/*!***************************************!*\
  !*** ./node_modules/indexof/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {


var indexOf = [].indexOf;

module.exports = function(arr, obj){
  if (indexOf) return arr.indexOf(obj);
  for (var i = 0; i < arr.length; ++i) {
    if (arr[i] === obj) return i;
  }
  return -1;
};

/***/ }),

/***/ "./node_modules/individual/index.js":
/*!******************************************!*\
  !*** ./node_modules/individual/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(/*! global */ "./node_modules/individual/node_modules/global/index.js")

module.exports = Individual

function Individual(key, value) {
    if (root[key]) {
        return root[key]
    }

    Object.defineProperty(root, key, {
        value: value
        , configurable: true
    })

    return value
}


/***/ }),

/***/ "./node_modules/individual/node_modules/global/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/individual/node_modules/global/index.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*global window, global*/
if (typeof global !== "undefined") {
    module.exports = global
} else if (typeof window !== "undefined") {
    module.exports = window
}


/***/ }),

/***/ "./node_modules/is/index.js":
/*!**********************************!*\
  !*** ./node_modules/is/index.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**!
 * is
 * the definitive JavaScript type testing library
 * 
 * @copyright 2013 Enrico Marino
 * @license MIT
 */

var objProto = Object.prototype;
var owns = objProto.hasOwnProperty;
var toString = objProto.toString;
var isActualNaN = function (value) {
  return value !== value;
};
var NON_HOST_TYPES = {
  "boolean": 1,
  "number": 1,
  "string": 1,
  "undefined": 1
};

/**
 * Expose `is`
 */

var is = module.exports = {};

/**
 * Test general.
 */

/**
 * is.type
 * Test if `value` is a type of `type`.
 *
 * @param {Mixed} value value to test
 * @param {String} type type
 * @return {Boolean} true if `value` is a type of `type`, false otherwise
 * @api public
 */

is.a =
is.type = function (value, type) {
  return typeof value === type;
};

/**
 * is.defined
 * Test if `value` is defined.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if 'value' is defined, false otherwise
 * @api public
 */

is.defined = function (value) {
  return value !== undefined;
};

/**
 * is.empty
 * Test if `value` is empty.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is empty, false otherwise
 * @api public
 */

is.empty = function (value) {
  var type = toString.call(value);
  var key;

  if ('[object Array]' === type || '[object Arguments]' === type) {
    return value.length === 0;
  }

  if ('[object Object]' === type) {
    for (key in value) if (owns.call(value, key)) return false;
    return true;
  }

  if ('[object String]' === type) {
    return '' === value;
  }

  return false;
};

/**
 * is.equal
 * Test if `value` is equal to `other`.
 *
 * @param {Mixed} value value to test
 * @param {Mixed} other value to compare with
 * @return {Boolean} true if `value` is equal to `other`, false otherwise
 */

is.equal = function (value, other) {
  var type = toString.call(value)
  var key;

  if (type !== toString.call(other)) {
    return false;
  }

  if ('[object Object]' === type) {
    for (key in value) {
      if (!is.equal(value[key], other[key])) {
        return false;
      }
    }
    return true;
  }

  if ('[object Array]' === type) {
    key = value.length;
    if (key !== other.length) {
      return false;
    }
    while (--key) {
      if (!is.equal(value[key], other[key])) {
        return false;
      }
    }
    return true;
  }

  if ('[object Function]' === type) {
    return value.prototype === other.prototype;
  }

  if ('[object Date]' === type) {
    return value.getTime() === other.getTime();
  }

  return value === other;
};

/**
 * is.hosted
 * Test if `value` is hosted by `host`.
 *
 * @param {Mixed} value to test
 * @param {Mixed} host host to test with
 * @return {Boolean} true if `value` is hosted by `host`, false otherwise
 * @api public
 */

is.hosted = function (value, host) {
  var type = typeof host[value];
  return type === 'object' ? !!host[value] : !NON_HOST_TYPES[type];
};

/**
 * is.instance
 * Test if `value` is an instance of `constructor`.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an instance of `constructor`
 * @api public
 */

is.instance = is['instanceof'] = function (value, constructor) {
  return value instanceof constructor;
};

/**
 * is.null
 * Test if `value` is null.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is null, false otherwise
 * @api public
 */

is['null'] = function (value) {
  return value === null;
};

/**
 * is.undefined
 * Test if `value` is undefined.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is undefined, false otherwise
 * @api public
 */

is.undefined = function (value) {
  return value === undefined;
};

/**
 * Test arguments.
 */

/**
 * is.arguments
 * Test if `value` is an arguments object.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an arguments object, false otherwise
 * @api public
 */

is.arguments = function (value) {
  var isStandardArguments = '[object Arguments]' === toString.call(value);
  var isOldArguments = !is.array(value) && is.arraylike(value) && is.object(value) && is.fn(value.callee);
  return isStandardArguments || isOldArguments;
};

/**
 * Test array.
 */

/**
 * is.array
 * Test if 'value' is an array.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an array, false otherwise
 * @api public
 */

is.array = function (value) {
  return '[object Array]' === toString.call(value);
};

/**
 * is.arguments.empty
 * Test if `value` is an empty arguments object.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an empty arguments object, false otherwise
 * @api public
 */
is.arguments.empty = function (value) {
  return is.arguments(value) && value.length === 0;
};

/**
 * is.array.empty
 * Test if `value` is an empty array.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an empty array, false otherwise
 * @api public
 */
is.array.empty = function (value) {
  return is.array(value) && value.length === 0;
};

/**
 * is.arraylike
 * Test if `value` is an arraylike object.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an arguments object, false otherwise
 * @api public
 */

is.arraylike = function (value) {
  return !!value && !is.boolean(value)
    && owns.call(value, 'length')
    && isFinite(value.length)
    && is.number(value.length)
    && value.length >= 0;
};

/**
 * Test boolean.
 */

/**
 * is.boolean
 * Test if `value` is a boolean.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a boolean, false otherwise
 * @api public
 */

is.boolean = function (value) {
  return '[object Boolean]' === toString.call(value);
};

/**
 * is.false
 * Test if `value` is false.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is false, false otherwise
 * @api public
 */

is['false'] = function (value) {
  return is.boolean(value) && (value === false || value.valueOf() === false);
};

/**
 * is.true
 * Test if `value` is true.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is true, false otherwise
 * @api public
 */

is['true'] = function (value) {
  return is.boolean(value) && (value === true || value.valueOf() === true);
};

/**
 * Test date.
 */

/**
 * is.date
 * Test if `value` is a date.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a date, false otherwise
 * @api public
 */

is.date = function (value) {
  return '[object Date]' === toString.call(value);
};

/**
 * Test element.
 */

/**
 * is.element
 * Test if `value` is an html element.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an HTML Element, false otherwise
 * @api public
 */

is.element = function (value) {
  return value !== undefined
    && typeof HTMLElement !== 'undefined'
    && value instanceof HTMLElement
    && value.nodeType === 1;
};

/**
 * Test error.
 */

/**
 * is.error
 * Test if `value` is an error object.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an error object, false otherwise
 * @api public
 */

is.error = function (value) {
  return '[object Error]' === toString.call(value);
};

/**
 * Test function.
 */

/**
 * is.fn / is.function (deprecated)
 * Test if `value` is a function.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a function, false otherwise
 * @api public
 */

is.fn = is['function'] = function (value) {
  var isAlert = typeof window !== 'undefined' && value === window.alert;
  return isAlert || '[object Function]' === toString.call(value);
};

/**
 * Test number.
 */

/**
 * is.number
 * Test if `value` is a number.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a number, false otherwise
 * @api public
 */

is.number = function (value) {
  return '[object Number]' === toString.call(value);
};

/**
 * is.infinite
 * Test if `value` is positive or negative infinity.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is positive or negative Infinity, false otherwise
 * @api public
 */
is.infinite = function (value) {
  return value === Infinity || value === -Infinity;
};

/**
 * is.decimal
 * Test if `value` is a decimal number.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a decimal number, false otherwise
 * @api public
 */

is.decimal = function (value) {
  return is.number(value) && !isActualNaN(value) && !is.infinite(value) && value % 1 !== 0;
};

/**
 * is.divisibleBy
 * Test if `value` is divisible by `n`.
 *
 * @param {Number} value value to test
 * @param {Number} n dividend
 * @return {Boolean} true if `value` is divisible by `n`, false otherwise
 * @api public
 */

is.divisibleBy = function (value, n) {
  var isDividendInfinite = is.infinite(value);
  var isDivisorInfinite = is.infinite(n);
  var isNonZeroNumber = is.number(value) && !isActualNaN(value) && is.number(n) && !isActualNaN(n) && n !== 0;
  return isDividendInfinite || isDivisorInfinite || (isNonZeroNumber && value % n === 0);
};

/**
 * is.int
 * Test if `value` is an integer.
 *
 * @param value to test
 * @return {Boolean} true if `value` is an integer, false otherwise
 * @api public
 */

is.int = function (value) {
  return is.number(value) && !isActualNaN(value) && value % 1 === 0;
};

/**
 * is.maximum
 * Test if `value` is greater than 'others' values.
 *
 * @param {Number} value value to test
 * @param {Array} others values to compare with
 * @return {Boolean} true if `value` is greater than `others` values
 * @api public
 */

is.maximum = function (value, others) {
  if (isActualNaN(value)) {
    throw new TypeError('NaN is not a valid value');
  } else if (!is.arraylike(others)) {
    throw new TypeError('second argument must be array-like');
  }
  var len = others.length;

  while (--len >= 0) {
    if (value < others[len]) {
      return false;
    }
  }

  return true;
};

/**
 * is.minimum
 * Test if `value` is less than `others` values.
 *
 * @param {Number} value value to test
 * @param {Array} others values to compare with
 * @return {Boolean} true if `value` is less than `others` values
 * @api public
 */

is.minimum = function (value, others) {
  if (isActualNaN(value)) {
    throw new TypeError('NaN is not a valid value');
  } else if (!is.arraylike(others)) {
    throw new TypeError('second argument must be array-like');
  }
  var len = others.length;

  while (--len >= 0) {
    if (value > others[len]) {
      return false;
    }
  }

  return true;
};

/**
 * is.nan
 * Test if `value` is not a number.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is not a number, false otherwise
 * @api public
 */

is.nan = function (value) {
  return !is.number(value) || value !== value;
};

/**
 * is.even
 * Test if `value` is an even number.
 *
 * @param {Number} value value to test
 * @return {Boolean} true if `value` is an even number, false otherwise
 * @api public
 */

is.even = function (value) {
  return is.infinite(value) || (is.number(value) && value === value && value % 2 === 0);
};

/**
 * is.odd
 * Test if `value` is an odd number.
 *
 * @param {Number} value value to test
 * @return {Boolean} true if `value` is an odd number, false otherwise
 * @api public
 */

is.odd = function (value) {
  return is.infinite(value) || (is.number(value) && value === value && value % 2 !== 0);
};

/**
 * is.ge
 * Test if `value` is greater than or equal to `other`.
 *
 * @param {Number} value value to test
 * @param {Number} other value to compare with
 * @return {Boolean}
 * @api public
 */

is.ge = function (value, other) {
  if (isActualNaN(value) || isActualNaN(other)) {
    throw new TypeError('NaN is not a valid value');
  }
  return !is.infinite(value) && !is.infinite(other) && value >= other;
};

/**
 * is.gt
 * Test if `value` is greater than `other`.
 *
 * @param {Number} value value to test
 * @param {Number} other value to compare with
 * @return {Boolean}
 * @api public
 */

is.gt = function (value, other) {
  if (isActualNaN(value) || isActualNaN(other)) {
    throw new TypeError('NaN is not a valid value');
  }
  return !is.infinite(value) && !is.infinite(other) && value > other;
};

/**
 * is.le
 * Test if `value` is less than or equal to `other`.
 *
 * @param {Number} value value to test
 * @param {Number} other value to compare with
 * @return {Boolean} if 'value' is less than or equal to 'other'
 * @api public
 */

is.le = function (value, other) {
  if (isActualNaN(value) || isActualNaN(other)) {
    throw new TypeError('NaN is not a valid value');
  }
  return !is.infinite(value) && !is.infinite(other) && value <= other;
};

/**
 * is.lt
 * Test if `value` is less than `other`.
 *
 * @param {Number} value value to test
 * @param {Number} other value to compare with
 * @return {Boolean} if `value` is less than `other`
 * @api public
 */

is.lt = function (value, other) {
  if (isActualNaN(value) || isActualNaN(other)) {
    throw new TypeError('NaN is not a valid value');
  }
  return !is.infinite(value) && !is.infinite(other) && value < other;
};

/**
 * is.within
 * Test if `value` is within `start` and `finish`.
 *
 * @param {Number} value value to test
 * @param {Number} start lower bound
 * @param {Number} finish upper bound
 * @return {Boolean} true if 'value' is is within 'start' and 'finish'
 * @api public
 */
is.within = function (value, start, finish) {
  if (isActualNaN(value) || isActualNaN(start) || isActualNaN(finish)) {
    throw new TypeError('NaN is not a valid value');
  } else if (!is.number(value) || !is.number(start) || !is.number(finish)) {
    throw new TypeError('all arguments must be numbers');
  }
  var isAnyInfinite = is.infinite(value) || is.infinite(start) || is.infinite(finish);
  return isAnyInfinite || (value >= start && value <= finish);
};

/**
 * Test object.
 */

/**
 * is.object
 * Test if `value` is an object.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is an object, false otherwise
 * @api public
 */

is.object = function (value) {
  return value && '[object Object]' === toString.call(value);
};

/**
 * is.hash
 * Test if `value` is a hash - a plain object literal.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a hash, false otherwise
 * @api public
 */

is.hash = function (value) {
  return is.object(value) && value.constructor === Object && !value.nodeType && !value.setInterval;
};

/**
 * Test regexp.
 */

/**
 * is.regexp
 * Test if `value` is a regular expression.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if `value` is a regexp, false otherwise
 * @api public
 */

is.regexp = function (value) {
  return '[object RegExp]' === toString.call(value);
};

/**
 * Test string.
 */

/**
 * is.string
 * Test if `value` is a string.
 *
 * @param {Mixed} value value to test
 * @return {Boolean} true if 'value' is a string, false otherwise
 * @api public
 */

is.string = function (value) {
  return '[object String]' === toString.call(value);
};



/***/ }),

/***/ "./node_modules/knob/handle_change.js":
/*!********************************************!*\
  !*** ./node_modules/knob/handle_change.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var ever = __webpack_require__(/*! ever */ "./node_modules/ever/index.js")

module.exports = function(knob){
  var documentEvents = ever(window.document)

  ever(knob.canvas).on('mousedown', function(e){
    e.preventDefault()

    var offset = getOffset(knob.canvas)

    function mouseMove(e) {
      knob.setValue(xyToValue(knob.options, e.pageX, e.pageY, offset), true)
    }

    function mouseUp(e){
      knob.options.activeClass && knob.classList.remove(knob.options.activeClass)
      documentEvents.removeListener('mousemove', mouseMove)
      documentEvents.removeListener('mouseup', mouseUp)
    }

    knob.options.activeClass && knob.classList.add(knob.options.activeClass)

    documentEvents.on('mousemove', mouseMove).on('mouseup', mouseUp)

    mouseMove(e)

  }).on('touchstart', function(e){
    e.preventDefault()

    var touchIndex = e.touches.length - 1
    var offset = getOffset(knob.canvas)

    function touchMove(e){
      knob.setValue(
        xyToValue(knob.options, e.touches[touchIndex].pageX, e.touches[touchIndex].pageY, offset), true
      )
    }

    function touchEnd(){
      knob.options.activeClass && knob.classList.remove(knob.options.activeClass)
      documentEvents.removeListener('touchmove', touchMove)
      documentEvents.removeListener('touchend', touchEnd)
    }

    knob.options.activeClass && knob.classList.add(knob.options.activeClass)

    documentEvents.on('touchmove', touchMove).on('touchend', touchEnd)

    touchMove(e)
  })
}

function xyToValue(options, x, y, offset){
  var PI2 = 2*Math.PI
  var w2 = options.width / 2
  var angleArc = options.angleArc * Math.PI / 180
  var angleOffset = options.angleOffset * Math.PI / 180;

  var angle = Math.atan2(x - (offset.x + w2), - (y - offset.y - w2)) - angleOffset

  if(angleArc != PI2 && (angle < 0) && (angle > -0.5)) {
    angle = 0
  } else if (angle < 0) {
    angle += PI2
  }

  var result = ~~ (0.5 + (angle * (options.max - options.min) / angleArc)) + options.min;
  return Math.max(Math.min(result, options.max), options.min)
}

function getOffset(element){
  var result = {x: 0, y: 0};
  while (element) {
    result.x += element.offsetLeft
    result.y += element.offsetTop
    element = element.offsetParent
  } 

  return result
}

/***/ }),

/***/ "./node_modules/knob/index.js":
/*!************************************!*\
  !*** ./node_modules/knob/index.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var extend = __webpack_require__(/*! xtend */ "./node_modules/knob/node_modules/xtend/index.js")
var h = __webpack_require__(/*! hyperscript */ "./node_modules/hyperscript/index.js")

var handleChange = __webpack_require__(/*! ./handle_change */ "./node_modules/knob/handle_change.js")

module.exports = function(options){

  options = extend({
    value: 50,

    min: 0,
    max: 100,
    step: 1,

    cursor: false,
    thickness: 0.35,
    lineCap: 'butt',
    readOnly: false,
    displayInput: true,

    width: 200,
    height: options.width || 200,

    bgColor: '#EEEEEE',
    fgColor: '#87CEEB',
    labelColor: '#888',

    angleOffset: 0,
    angleArc: 360,

    className: null,
    activeClass: null

  }, options)

  var canvas = h('canvas', {
    height: options.height,
    width: options.width,
    style: {'position': 'absolute'}
  })

  var context2d = canvas.getContext("2d")

  var fontScale = Math.max(String(Math.abs(options.max)).length, String(Math.abs(options.min)).length, 2) + 2

  var inputOptions = {value: options.value, style: {
    'position' : 'absolute',
    'top': (options.width / 2) - (options.width / 7) + 'px',
    'left': getLineWidth(options) + 'px',
    'width': options.width - (getLineWidth(options)*2) + 'px',
    'vertical-align' : 'middle',
    'border' : 0,
    'background' : 'none',
    'font' : 'bold ' + ((options.width / fontScale) >> 0) + 'px Arial',
    'text-align' : 'center',
    'color' : options.fgColor,
    'padding' : '0px',
    '-webkit-appearance': 'none'
  }}

  if(options.readOnly){
    inputOptions['disabled'] = true
  }

  if(options.displayInput === false){
    inputOptions.style['display'] = 'none';
  }

  var input = h('input', inputOptions);

  var label = h('span', {style: {
    'color': options.labelColor,
    'position': 'absolute',
    'bottom': 0,
    'font-size': '80%',
    'text-align': 'center',
    'pointer-events': 'none',
    'top': (options.width / 2) + (options.width / 8) - 3 + 'px',
    'left': 0,
    'right': 0
  }}, options.label)

  var element = h('div', { className: options.className,
    style: {'display': 'inline-block', 'position': 'relative', 'height': options.height + 'px', 'width': options.width + 'px'}
  }, canvas, input, label)

  element.canvas = canvas
  element.options = options

  var renderedValue = options.value
  var animating = false

  element.setValue = function(value, event){
    value = Math.min(options.max, Math.max(options.min, value))
    options.value = value
    element.value = value
    if (!animating){
      refreshCanvas()
    }
    if (event === true && element.onchange){
      element.onchange()
    }
  }

  element.getValue = function(){
    return options.value
  }

  input.onchange = function(){
    element.setValue(this.value)
  }

  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame

  function refreshCanvas(){
    if (renderedValue === options.value){
      animating = false
    } else {
      animating = true
      renderedValue = options.value
      context2d.clearRect(0, 0, canvas.width, canvas.height)
      draw(context2d, options)
      input.value = options.value
      requestAnimationFrame.call(window, refreshCanvas)
    }
  }

  draw(context2d, options)

  if(!options.readOnly){
    handleChange(element)
  }

  return element
}

function getLineWidth(options){
  var xy = options.width / 2
  return xy * options.thickness
}

function draw(context2d, options){
  // deg to rad
  var angleOffset = options.angleOffset * Math.PI / 180
  var angleArc = options.angleArc * Math.PI / 180

  var angle = (options.value - options.min) * angleArc / (options.max - options.min)

  var xy = options.width / 2
  var lineWidth = xy * options.thickness
  var radius = xy - lineWidth / 2;

  var startAngle = 1.5 * Math.PI + angleOffset;
  var endAngle = 1.5 * Math.PI + angleOffset + angleArc;

  var startAt = startAngle
  var endAt = startAt + angle

  if (options.cursor){
    var cursorExt = (options.cursor / 100) || 1;
    startAt = endAt - cursorExt
    endAt = endAt + cursorExt
  }

  context2d.lineWidth = lineWidth
  context2d.lineCap = options.lineCap;

  context2d.beginPath()
    context2d.strokeStyle = options.bgColor
    context2d.arc(xy, xy, radius, endAngle, startAngle, true)
  context2d.stroke()

  context2d.beginPath()
    context2d.strokeStyle = options.fgColor
    context2d.arc(xy, xy, radius, startAt, endAt, false)
  context2d.stroke()
}



/***/ }),

/***/ "./node_modules/knob/node_modules/xtend/has-keys.js":
/*!**********************************************************!*\
  !*** ./node_modules/knob/node_modules/xtend/has-keys.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = hasKeys

function hasKeys(source) {
    return source !== null &&
        (typeof source === "object" ||
        typeof source === "function")
}


/***/ }),

/***/ "./node_modules/knob/node_modules/xtend/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/knob/node_modules/xtend/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Keys = __webpack_require__(/*! object-keys */ "./node_modules/object-keys/index.js")
var hasKeys = __webpack_require__(/*! ./has-keys */ "./node_modules/knob/node_modules/xtend/has-keys.js")

module.exports = extend

function extend() {
    var target = {}

    for (var i = 0; i < arguments.length; i++) {
        var source = arguments[i]

        if (!hasKeys(source)) {
            continue
        }

        var keys = Keys(source)

        for (var j = 0; j < keys.length; j++) {
            var name = keys[j]
            target[name] = source[name]
        }
    }

    return target
}


/***/ }),

/***/ "./node_modules/object-keys/index.js":
/*!*******************************************!*\
  !*** ./node_modules/object-keys/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = Object.keys || __webpack_require__(/*! ./shim */ "./node_modules/object-keys/shim.js");



/***/ }),

/***/ "./node_modules/object-keys/shim.js":
/*!******************************************!*\
  !*** ./node_modules/object-keys/shim.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function () {
	"use strict";

	// modified from https://github.com/kriskowal/es5-shim
	var has = Object.prototype.hasOwnProperty,
		is = __webpack_require__(/*! is */ "./node_modules/is/index.js"),
		forEach = __webpack_require__(/*! foreach */ "./node_modules/foreach/index.js"),
		hasDontEnumBug = !({'toString': null}).propertyIsEnumerable('toString'),
		dontEnums = [
			"toString",
			"toLocaleString",
			"valueOf",
			"hasOwnProperty",
			"isPrototypeOf",
			"propertyIsEnumerable",
			"constructor"
		],
		keysShim;

	keysShim = function keys(object) {
		if (!is.object(object) && !is.array(object)) {
			throw new TypeError("Object.keys called on a non-object");
		}

		var name, theKeys = [];
		for (name in object) {
			if (has.call(object, name)) {
				theKeys.push(name);
			}
		}

		if (hasDontEnumBug) {
			forEach(dontEnums, function (dontEnum) {
				if (has.call(object, dontEnum)) {
					theKeys.push(dontEnum);
				}
			});
		}
		return theKeys;
	};

	module.exports = keysShim;
}());



/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/bitcoin/bitcoin.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/bitcoin/bitcoin.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media bitcoin-graph\"><img src=\"assets/images/bitcoin/graph-icon-1.png\" alt=\"\">\r\n            <div class=\"top-bitcoin\">\r\n              <h5>BTC</h5><span>Bitcoin</span>\r\n            </div>\r\n            <div class=\"media-body\">\r\n              <div class=\"bitcoin-content text-right\">\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 1h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span> 0.12\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 24h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                    </span> +0.30\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 7d</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span>\r\n                    -0.20</h6>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"dashboard-chart-container\">\r\n          <x-chartist class=\"bitcoinchart-1\" [data]=\"chart1.data\" [type]=\"chart1.type\" [options]=\"chart1.options\"\r\n            [events]=\"chart1.events\">\r\n          </x-chartist>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media bitcoin-graph\"><img src=\"assets/images/bitcoin/graph-icon-2.png\" alt=\"\">\r\n            <div class=\"top-bitcoin\">\r\n              <h5>DASH</h5><span>Tranding</span>\r\n            </div>\r\n            <div class=\"media-body\">\r\n              <div class=\"bitcoin-content text-right\">\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 1h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span> 0.12\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 24h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                    </span> +0.30\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 7d</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span>\r\n                    -0.20</h6>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"dashboard-chart-container\">\r\n          <x-chartist class=\"bitcoinchart-2\" [data]=\"chart2.data\" [type]=\"chart2.type\" [options]=\"chart2.options\"\r\n            [events]=\"chart2.events\">\r\n          </x-chartist>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media bitcoin-graph\"><img src=\"assets/images/bitcoin/graph-icon-3.png\" alt=\"\">\r\n            <div class=\"top-bitcoin\">\r\n              <h5>ETH</h5><span>Ethereum</span>\r\n            </div>\r\n            <div class=\"media-body\">\r\n              <div class=\"bitcoin-content text-right\">\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 1h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span> 0.12\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 24h</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                    </span> +0.30\r\n                  </h6>\r\n                </div>\r\n                <div class=\"bitcoin-numbers d-inline-block\">\r\n                  <h6 class=\"mb-0\">% 7d</h6>\r\n                  <h6 class=\"mb-0 font-primary\"><span>\r\n                      <app-feather-icons class=\"arrow-down\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                    </span>\r\n                    -0.20</h6>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"dashboard-chart-container\">\r\n          <x-chartist class=\"bitcoinchart-3\" [data]=\"chart3.data\" [type]=\"chart3.type\" [options]=\"chart3.options\"\r\n            [events]=\"chart3.events\">\r\n          </x-chartist>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Market Value</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <x-chartist class=\"market-chart\" [data]=\"chart4.data\" [type]=\"chart4.type\" [options]=\"chart4.options\"\r\n            [events]=\"chart4.events\">\r\n          </x-chartist>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h5 class=\"bitcoin-header\">Buy</h5>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"text-right right-header-color\">\r\n                <p class=\"mb-0\">USD Balance: $ 5000.00</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"bitcoin-form\">\r\n            <div class=\"form-row\">\r\n              <div class=\"col-xl-4 mb-3 col-sm-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustom01\">Parchase</label>\r\n                <div class=\"bitcoin-form-dropdown\">\r\n                  <div class=\"onhover-dropdown\" id=\"validationCustom01\">\r\n                    <button class=\"btn f-12\" type=\"button\">Amount <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div\"><a class=\"d-block\"  href=\"javascript:void(0)\">Link 1</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link\r\n                        2</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link 3</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-8 mb-3 col-sm-9\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Units</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-4 mb-3 col-sm-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustom02\">Bid</label>\r\n                <div class=\"bitcoin-form-dropdown\">\r\n                  <div class=\"onhover-dropdown\" id=\"validationCustom02\">\r\n                    <button class=\"btn f-12\" type=\"button\">Limit <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div\"><a class=\"d-block\"  href=\"javascript:void(0)\">Link 1</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link\r\n                        2</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link 3</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-8 mb-3 col-sm-9\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Bid</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend1\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername1\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12 mb-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Total</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend2\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername2\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12\">\r\n                <div class=\"btn-bottom\">\r\n                  <button class=\"btn btn-primary\" type=\"button\">Buy Now!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h5 class=\"bitcoin-header\">Sell</h5>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"text-right right-header-color\">\r\n                <p class=\"mb-0\">BTC Balance: $ 5000.00</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"bitcoin-form\">\r\n            <div class=\"form-row\">\r\n              <div class=\"col-xl-4 mb-3 col-sm-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustom03\">Sell</label>\r\n                <div class=\"bitcoin-form-dropdown\">\r\n                  <div class=\"onhover-dropdown\" id=\"validationCustom03\">\r\n                    <button class=\"btn f-12\" type=\"button\">Amount <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div\"><a class=\"d-block\"  href=\"javascript:void(0)\">Link 1</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link\r\n                        2</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link 3</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-8 mb-3 col-sm-9\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Units</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend3\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername3\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-4 mb-3 col-sm-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustom04\">Bid</label>\r\n                <div class=\"bitcoin-form-dropdown\">\r\n                  <div class=\"onhover-dropdown\" id=\"validationCustom04\">\r\n                    <button class=\"btn f-12\" type=\"button\">Limit <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div\"><a class=\"d-block\"  href=\"javascript:void(0)\">Link 1</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link\r\n                        2</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link 3</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xl-8 mb-3 col-sm-9\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Bid</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend5\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername5\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12 mb-3\">\r\n                <label class=\"f-w-600\" for=\"validationCustomUsername\">Total</label>\r\n                <div class=\"input-group\">\r\n                  <div class=\"input-group-prepend\"><span class=\"input-group-text\" id=\"inputGroupPrepend6\"><i\r\n                        class=\"fa fa-btc font-primary\"></i></span></div>\r\n                  <input class=\"form-control\" id=\"validationCustomUsername6\" type=\"text\"\r\n                    aria-describedby=\"inputGroupPrepend\" required=\"\">\r\n                  <div class=\"invalid-feedback\">Please choose a username.</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-12\">\r\n                <div class=\"btn-bottom\">\r\n                  <button class=\"btn btn-primary\" type=\"button\">Sell Now!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Active Order</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"table-responsive active-order-table\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Data</th>\r\n                  <th scope=\"col\">Type</th>\r\n                  <th scope=\"col\">Customer</th>\r\n                  <th scope=\"col\">Price</th>\r\n                  <th scope=\"col\">USD</th>\r\n                  <th scope=\"col\">Fee(%)</th>\r\n                  <th scope=\"col\">Status</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td>\r\n                    <p>2018-01-31</p>\r\n                    <p class=\"mb-0\">6:51:51</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Buy</button>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"customers\">\r\n                      <ul>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/3.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/5.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/1.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\">\r\n                          <p class=\"f-12\">+10 More</p>\r\n                        </li>\r\n                      </ul>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>11900.12</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>$ 6979.78</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>0.2</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Status</button>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <p>2018-01-31</p>\r\n                    <p class=\"mb-0\">06:50:50</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-secondary btn-pill\" type=\"button\">Sell</button>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"customers\">\r\n                      <ul>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/3.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/5.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/1.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\">\r\n                          <p class=\"f-12\">+10 More</p>\r\n                        </li>\r\n                      </ul>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>11900.12</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>$ 6979.78</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>0.2</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Status</button>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <p>2018-01-31</p>\r\n                    <p class=\"mb-0\">06:49:51</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Buy</button>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"customers\">\r\n                      <ul>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/3.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/5.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/1.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\">\r\n                          <p class=\"f-12\">+10 More</p>\r\n                        </li>\r\n                      </ul>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>11900.12</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>$ 6979.78</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>0.2</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Status</button>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <p>2018-01-31</p>\r\n                    <p class=\"mb-0\">06:50:50</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-secondary btn-pill\" type=\"button\">Sell</button>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"customers\">\r\n                      <ul>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/3.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/5.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\"><img class=\"img-40 rounded-circle\" src=\"assets/images/user/1.jpg\"\r\n                            alt=\"\"></li>\r\n                        <li class=\"d-inline-block\">\r\n                          <p class=\"f-12\">+10 More</p>\r\n                        </li>\r\n                      </ul>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>11900.12</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>$ 6979.78</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>0.2</p>\r\n                  </td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">Status</button>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Market News</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"media markets\"><img src=\"assets/images/bitcoin/market-1.jpg\">\r\n            <div class=\"media-body\">\r\n              <h5 class=\"font-primary\">03 Jan</h5>\r\n              <p>\r\n                Lorem Ipsum is simply dummy text of the printing\r\n                and typesetting industry. Lorem Ipsum\r\n              </p>\r\n            </div>\r\n          </div>\r\n          <div class=\"media markets\"><img src=\"assets/images/bitcoin/market-2.jpg\">\r\n            <div class=\"media-body\">\r\n              <h5 class=\"font-primary\">03 Jan</h5>\r\n              <p>\r\n                Lorem Ipsum is simply dummy text of the printing\r\n                and typesetting industry. Lorem Ipsum\r\n              </p>\r\n            </div>\r\n          </div>\r\n          <div class=\"media markets\"><img src=\"assets/images/bitcoin/market-3.jpg\">\r\n            <div class=\"media-body\">\r\n              <h5 class=\"font-primary\">03 Jan</h5>\r\n              <p>\r\n                Lorem Ipsum is simply dummy text of the printing\r\n                and typesetting industry. Lorem Ipsum\r\n              </p>\r\n            </div>\r\n          </div>\r\n          <div class=\"media markets mb-0\"><img src=\"assets/images/bitcoin/market-4.jpg\">\r\n            <div class=\"media-body\">\r\n              <h5 class=\"font-primary\">03 Jan</h5>\r\n              <p>\r\n                Lorem Ipsum is simply dummy text of the printing\r\n                and typesetting industry. Lorem Ipsum\r\n              </p>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Sales Statistics</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"show-value-top d-flex pull-right\">\r\n            <div class=\"value-left d-inline-block\">\r\n              <div class=\"circle-graph bg-primary d-inline-block m-r-5\"></div><span>Total Earning</span>\r\n            </div>\r\n            <div class=\"value-right d-inline-block\">\r\n              <div class=\"circle-graph d-inline-block bg-secondary m-r-5\"></div><span>Total Tax</span>\r\n            </div>\r\n          </div>\r\n          <div class=\"chart-block\">\r\n            <canvas baseChart class=\"chart flot-chart-placeholder\" id=\"linecharts-bitcoin\" [legend]=\"saleChartLegend\"\r\n              [datasets]=\"saleChartData\" [labels]=\"saleChartLable\" [options]=\"saleChartOption\"\r\n              [chartType]=\"saleChartType\" [colors]=\"saleChartColor\"></canvas>\r\n          </div>\r\n          <div class=\"row chart-bottom\">\r\n            <div class=\"col text-center\">\r\n              <div class=\"d-inline-block\">\r\n                <h5 class=\"font-primary counter\" [CountTo]=\"75000\" [from]=\"0\" [duration]=\"2\"></h5>\r\n                <h6 class=\"mb-0\">Total Sale</h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"col text-center\">\r\n              <div class=\"d-inline-block\">\r\n                <h5 class=\"font-primary counter\" [CountTo]=\"40000\" [from]=\"0\" [duration]=\"2\"></h5>\r\n                <h6 class=\"mb-0\">Bitcoin Sale</h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"col text-center\">\r\n              <div class=\"d-inline-block\">\r\n                <h5 class=\"font-primary counter\" [CountTo]=\"35000\" [from]=\"0\" [duration]=\"2\"></h5>\r\n                <h6 class=\"mb-0\">Ethereum Sale</h6>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Datatables Market</h5>\r\n        </div>\r\n        <div class=\"card-body tab-content\">\r\n\r\n          <ngb-tabset class=\"card-footer b-t-primary market-tabs pb-0 pl-0 pr-0 bit-coin-tab\">\r\n            <ngb-tab>\r\n              <ng-template ngbTabContent class=\"tab-content\">\r\n                <div class=\"market-table table-responsive tab-pane active\" id=\"htc\">\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th scope=\"col\">Market</th>\r\n                        <th scope=\"col\">Price</th>\r\n                        <th scope=\"col\">Change</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 13458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548248</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 022157</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.025486854</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-05.15%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 11458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-18.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbTabTitle class=\"nav-item nav nav-pills\">Users</ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabContent class=\"tab-content\">\r\n                <div class=\"market-table table-responsive tab-pane active\" id=\"htc\">\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th scope=\"col\">Market</th>\r\n                        <th scope=\"col\">Price</th>\r\n                        <th scope=\"col\">Change</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 02357</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.025486854</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-05.15%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 15458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12468</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-18.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbTabTitle class=\"nav-item\">Email</ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabContent class=\"tab-content\">\r\n                <div class=\"market-table table-responsive tab-pane active\" id=\"htc\">\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th scope=\"col\">Market</th>\r\n                        <th scope=\"col\">Price</th>\r\n                        <th scope=\"col\">Change</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 10458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.0025453248</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 02157</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.025486854</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-05.25%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.93%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12498</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-18.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbTabTitle class=\"nav-item\">Blog</ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabContent class=\"tab-content\">\r\n                <div class=\"market-table table-responsive tab-pane active\" id=\"htc\">\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th scope=\"col\">Market</th>\r\n                        <th scope=\"col\">Price</th>\r\n                        <th scope=\"col\">Change</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 02157</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.025486854</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-05.15%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-18.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbTabTitle class=\"nav-item\">Chat</ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabContent class=\"tab-content\">\r\n                <div class=\"market-table table-responsive tab-pane active\" id=\"htc\">\r\n                  <table class=\"table\">\r\n                    <thead>\r\n                      <tr>\r\n                        <th scope=\"col\">Market</th>\r\n                        <th scope=\"col\">Price</th>\r\n                        <th scope=\"col\">Change</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 02157</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.025486854</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-05.15%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>+16.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-primary\" [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td>\r\n                          <p class=\"font-primary\">BTC - 12458</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>0.002548548</p>\r\n                        </td>\r\n                        <td>\r\n                          <p>-18.23%</p>\r\n                          <div class=\"text-center\">\r\n                            <app-feather-icons class=\"font-secondary\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbTabTitle class=\"nav-item\">Ecommerce</ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-12 xl-100\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body bg-primary\">\r\n              <div class=\"icons-section text-center\"><img src=\"assets/images/bitcoin/1.png\" alt=\"\">\r\n                <h6>Bitcoin</h6>\r\n                <h5><span>\r\n                    <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons>\r\n                  </span>760.03 <span>\r\n                    <app-feather-icons [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                  </span></h5>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body bg-secondary\">\r\n              <div class=\"icons-section text-center\"><img src=\"assets/images/bitcoin/2.png\" alt=\"\">\r\n                <h6>Ethereum</h6>\r\n                <h5><span>\r\n                    <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons>\r\n                  </span>750.03 <span>\r\n                    <app-feather-icons [icon]=\"'arrow-up'\"></app-feather-icons>\r\n                  </span></h5>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body bg-primary\">\r\n              <div class=\"icons-section text-center\"><img src=\"assets/images/bitcoin/3.png\" alt=\"\">\r\n                <h6>Balance</h6>\r\n                <h5><span>\r\n                    <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons>\r\n                  </span>9,980 <span>\r\n                    <app-feather-icons [icon]=\"'arrow-down'\"></app-feather-icons>\r\n                  </span></h5>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Chat</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body chat-box\">\r\n          <div class=\"chat-right-aside bitcoin-chat\">\r\n            <div class=\"chat\">\r\n              <div class=\"chat-history chat-msg-box custom-scrollbar\">\r\n                <ul>\r\n                  <li>\r\n                    <div class=\"message my-message\"><img class=\"rounded-circle float-left chat-user-img\"\r\n                        src=\"assets/images/bitcoin/chat-1.jpg\" alt=\"\">\r\n                      <div class=\"message-data text-right\"><span class=\"message-data-time\">1:00 pm</span></div> Project\r\n                      has been already finished and\r\n                      I have results to show you.\r\n                    </div>\r\n                  </li>\r\n                  <li class=\"clearfix\">\r\n                    <div class=\"message other-message pull-right\"><img class=\"rounded-circle float-right chat-user-img\"\r\n                        src=\"assets/images/bitcoin/chat-2.jpg\" alt=\"\">\r\n                      <div class=\"message-data\"><span class=\"message-data-time\">1:08 pm</span></div> Well I am not sure.\r\n                      The rest of the\r\n                      team is not here yet.\r\n                    </div>\r\n                  </li>\r\n                  <li>\r\n                    <div class=\"message my-message mb-0\"><img class=\"rounded-circle float-left chat-user-img\"\r\n                        src=\"assets/images/bitcoin/chat-1.jpg\" alt=\"\">\r\n                      <div class=\"message-data text-right\"><span class=\"message-data-time\">1:12 pm</span></div> Actually\r\n                      everything was fine. I'm very\r\n                      excited to show this to our team.\r\n                    </div>\r\n                  </li>\r\n                </ul>\r\n              </div>\r\n              <div class=\"bitcoin-message clearfix\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-xl-12 d-flex\">\r\n                    <div class=\"smiley-box bg-primary\">\r\n                      <div class=\"picker\"><img src=\"assets/images/smiley.png\" alt=\"\"></div>\r\n                    </div>\r\n                    <div class=\"input-group text-box\">\r\n                      <input class=\"form-control input-txt-bx\" id=\"message-to-send\" type=\"text\" name=\"message-to-send\"\r\n                        placeholder=\"Type a message......\">\r\n                      <div class=\"input-group-append\">\r\n                        <button class=\"btn btn-primary\" type=\"button\">SEND</button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"default-according style-1 bitcoin-accordion\" id=\"accordionoc\">\r\n          <ngb-accordion [closeOthers]=\"true\" activeIds=\"static-1\">\r\n            <ngb-panel id=\"static-1\">\r\n              <ng-template ngbPanelTitle>\r\n                <div class=\"card-header bg-primary\">\r\n                  <h5 class=\"mb-0\">\r\n                    <button class=\"btn btn-link txt-white p-0\">BTC</button>\r\n                  </h5>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbPanelContent>\r\n                <div class=\"collapse p-0\" id=\"collapse\" [ngbCollapse]=\"isBTC\">\r\n                  <div class=\"media-accordion\">\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/USD</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>11916.9</p>\r\n                        <p class=\"font-primary\">283.1 USD (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1029.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/EUR</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>9213.9</p>\r\n                        <p class=\"font-primary\">200.1 EUR (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1599.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/GBP</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>1459.9</p>\r\n                        <p class=\"font-primary\">-283.1 USD (-2.33%)</p>\r\n                        <p class=\"font-secondary\">350.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"static-2\">\r\n              <ng-template ngbPanelTitle>\r\n                <div class=\"card-header bg-primary\">\r\n                  <h5 class=\"mb-0\">\r\n                    <button class=\"btn btn-link txt-white p-0\">ETH</button>\r\n                  </h5>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbPanelContent>\r\n                <div class=\"collapse\" id=\"collapse\" [ngbCollapse]=\"isBTC\">\r\n                  <div class=\"media-accordion\">\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/USD</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>11916.9</p>\r\n                        <p class=\"font-primary\">283.1 USD (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1029.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/EUR</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>9213.9</p>\r\n                        <p class=\"font-primary\">200.1 EUR (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1599.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/GBP</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>1459.9</p>\r\n                        <p class=\"font-primary\">-283.1 USD (-2.33%)</p>\r\n                        <p class=\"font-secondary\">350.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-panel>\r\n            <ngb-panel id=\"static-3\">\r\n              <ng-template ngbPanelTitle>\r\n                <div class=\"card-header bg-primary\">\r\n                  <h5 class=\"mb-0\">\r\n                    <button class=\"btn btn-link txt-white p-0\">DASH</button>\r\n                  </h5>\r\n                </div>\r\n              </ng-template>\r\n              <ng-template ngbPanelContent>\r\n                <div class=\"collapse\" id=\"collapse\" [ngbCollapse]=\"isBTC\">\r\n                  <div class=\"media-accordion\">\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/USD</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>11916.9</p>\r\n                        <p class=\"font-primary\">283.1 USD (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1029.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/EUR</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>9213.9</p>\r\n                        <p class=\"font-primary\">200.1 EUR (+2.33%)</p>\r\n                        <p class=\"font-secondary\">1599.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"media\">\r\n                      <div>\r\n                        <h6>BTC/GBP</h6>\r\n                        <p>24h Change</p>\r\n                        <p>24h Volume</p>\r\n                      </div>\r\n                      <div class=\"media-body text-right\">\r\n                        <p>1459.9</p>\r\n                        <p class=\"font-primary\">-283.1 USD (-2.33%)</p>\r\n                        <p class=\"font-secondary\">350.1906 BTC</p>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-panel>\r\n          </ngb-accordion>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header b-l-primary\">\r\n          <h5>Invest</h5>\r\n        </div>\r\n        <div class=\"card-body chart-block\">\r\n          <div class=\"pull-right right-header invest-dropdown\">\r\n            <div class=\"onhover-dropdown\">\r\n              <button class=\"btn\" type=\"button\">Today <span class=\"pr-0\"><i\r\n                    class=\"fa fa-angle-down\"></i></span></button>\r\n              <div class=\"onhover-show-div right-header-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">Link 1</a><a\r\n                  class=\"d-block\"  href=\"javascript:void(0)\">Link 2</a><a class=\"d-block\"  href=\"javascript:void(0)\">Link 3</a></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"flot-chart-container\">\r\n            <div class=\"flot-chart-placeholder\" id=\"bitcoin-morris\">\r\n              <canvas baseChart [legend]=\"dailyChartLegend\" [data]=\"dailyChartData\" [labels]=\"dailyChartLabels\"\r\n                [options]=\"dailyChartOptions\" [chartType]=\"dailyChartType\" [colors]=\"dailyChartColors\">\r\n              </canvas>\r\n            </div>\r\n          </div>\r\n          <div class=\"show-value-top d-flex mb-0 bottom-morris-chart\">\r\n            <div class=\"value-left d-inline-block\">\r\n              <div class=\"circle-graph bg-primary d-inline-block m-r-5\"></div><span>Bitcoin</span>\r\n            </div>\r\n            <div class=\"value-right d-inline-block\">\r\n              <div class=\"circle-graph d-inline-block bg-secondary m-r-5\"></div><span>Ripple</span>\r\n            </div>\r\n            <div class=\"value-third d-inline-block\">\r\n              <div class=\"circle-graph d-inline-block bg-light m-r-5\"></div><span>Invest</span>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/default/default.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/default/default.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <!-- Container-fluid starts-->\r\n  <div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-8 xl-100\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"card\">\r\n              <div class=\"card-body\">\r\n                <div class=\"chart-widget-dashboard\">\r\n                  <div class=\"media\">\r\n                    <div class=\"media-body\">\r\n                      <h5 class=\"mt-0 mb-0 f-w-600\"><app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"5789\" [from]=\"0\" [duration]=\"2\"></span></h5>\r\n                      <p>Total Visits</p>\r\n                    </div><app-feather-icons [icon]=\"'tag'\"></app-feather-icons>\r\n                  </div>\r\n                  <div class=\"dashboard-chart-container\">\r\n                    <x-chartist class=\"small-chart-gradient-1\" [data]=\"chart1.data\" [type]=\"chart1.type\" [options]=\"chart1.options\" [events]=\"chart1.events\">\r\n                    </x-chartist>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"card\">\r\n              <div class=\"card-body\">\r\n                <div class=\"chart-widget-dashboard\">\r\n                  <div class=\"media\">\r\n                    <div class=\"media-body\">\r\n                      <h5 class=\"mt-0 mb-0 f-w-600\"><app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"4986\" [from]=\"0\" [duration]=\"2\"></span></h5>\r\n                      <p>Total Sale</p>\r\n                    </div><app-feather-icons [icon]=\"'shopping-cart'\"></app-feather-icons>\r\n                  </div>\r\n                  <div class=\"dashboard-chart-container\">\r\n                    <x-chartist class=\"small-chart-gradient-2\" [data]=\"chart2.data\" [type]=\"chart2.type\" [options]=\"chart2.options\" [events]=\"chart2.events\">\r\n                    </x-chartist>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"card\">\r\n              <div class=\"card-body\">\r\n                <div class=\"chart-widget-dashboard\">\r\n                  <div class=\"media\">\r\n                    <div class=\"media-body\">\r\n                      <h5 class=\"mt-0 mb-0 f-w-600\"><app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"8568\" [from]=\"0\" [duration]=\"2\"></span></h5>\r\n                      <p>Total Value</p>\r\n                    </div><app-feather-icons [icon]=\"'sun'\"></app-feather-icons>\r\n                  </div>\r\n                  <div class=\"dashboard-chart-container\">\r\n                    <x-chartist class=\"small-chart-gradient-3\" [data]=\"chart3.data\" [type]=\"chart3.type\" [options]=\"chart3.options\" [events]=\"chart3.events\">\r\n                    </x-chartist>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-lg-12\">\r\n            <div class=\"card\">\r\n              <div class=\"card-header\">\r\n                <h5>Production Valuation</h5>\r\n              </div>\r\n              <div class=\"card-body\">\r\n                <div class=\"show-value-top d-flex\">\r\n                  <div class=\"value-left d-inline-block\">\r\n                    <div class=\"square bg-primary d-inline-block\"></div><span>Total Earning</span>\r\n                  </div>\r\n                  <div class=\"value-right d-inline-block\">\r\n                    <div class=\"square d-inline-block bg-secondary\"></div><span>Total Tax</span>\r\n                  </div>\r\n                </div>\r\n                <x-chartist class=\"smooth-chart flot-chart-container\" [data]=\"chart4.data\" [type]=\"chart4.type\" [options]=\"chart4.options\" [events]=\"chart4.events\">\r\n                </x-chartist>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 xl-100\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Activity</h5>\r\n          </div>\r\n          <div class=\"card-body activity-scroll\">\r\n            <div class=\"activity\">\r\n              <div class=\"media\">\r\n                <div class=\"gradient-round m-r-30 gradient-line-1\"><app-feather-icons [icon]=\"'shopping-cart'\"></app-feather-icons></div>\r\n                <div class=\"media-body\">\r\n                  <h6>New Sale <span class=\"pull-right f-14\">New</span></h6>\r\n                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"media\">\r\n                <div class=\"gradient-round m-r-30 gradient-line-1\"><app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons></div>\r\n                <div class=\"media-body\">\r\n                  <h6>New Message <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"media\">\r\n                <div class=\"gradient-round m-r-30 small-line\"><app-feather-icons [icon]=\"'minus-circle'\"></app-feather-icons></div>\r\n                <div class=\"media-body\">\r\n                  <h6>New Report <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                  <p>Lorem Ipsum is simply dummy text.</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"media\">\r\n                <div class=\"gradient-round m-r-30 gradient-line-1\"><app-feather-icons [icon]=\"'shopping-bag'\"></app-feather-icons></div>\r\n                <div class=\"media-body\">\r\n                  <h6>New Sale <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"media\">\r\n                <div class=\"gradient-round m-r-30 medium-line\"><app-feather-icons [icon]=\"'tag'\"></app-feather-icons></div>\r\n                <div class=\"media-body\">\r\n                  <h6>New Visits <span class=\"pull-right f-14\">14m Ago</span></h6>\r\n                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.</p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 xl-50 col-md-6\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Total Profit</h5><span class=\"d-block fonts-dashboard\">All Custom Income</span>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"knob-block text-center\">\r\n               <div class=\"knob\" id=\"profit\"></div>\r\n            </div>\r\n            <div class=\"show-value d-flex\">\r\n              <div class=\"value-left\">\r\n                <div class=\"square bg-primary d-inline-block\"></div><span>Total Profit</span>\r\n              </div>\r\n              <div class=\"value-right\">\r\n                <div class=\"square bg-light d-inline-block\"></div><span>Total Investment</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 xl-50 col-md-6\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <div class=\"card\">\r\n              <div class=\"card-body progress-media\">\r\n                <div class=\"media\">\r\n                  <div class=\"media-body\">\r\n                    <h5>Total Review</h5><span class=\"mb-0 d-block\">Customer Review</span>\r\n                  </div><app-feather-icons [icon]=\"'thumbs-up'\"></app-feather-icons>\r\n                </div>\r\n                <div class=\"progress-bar-showcase\">\r\n                  <div class=\"progress sm-progress-bar\">\r\n                    <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 90%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"progress-change\"><span>Change</span><span class=\"pull-right\">95%</span></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-12\">\r\n            <div class=\"card\">\r\n              <div class=\"card-body progress-media\">\r\n                <div class=\"media\">\r\n                  <div class=\"media-body\">\r\n                    <h5>Total Feedback</h5><span class=\"mb-0 d-block\">Feedback</span>\r\n                  </div><app-feather-icons [icon]=\"'message-circle'\"></app-feather-icons>\r\n                </div>\r\n                <div class=\"progress-bar-showcase\">\r\n                  <div class=\"progress sm-progress-bar\">\r\n                    <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 85%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"progress-change\"><span>Change</span><span class=\"pull-right\">85%</span></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 xl-100\">\r\n        <div class=\"card user-card\">\r\n          <div class=\"card-body\">\r\n            <div class=\"online-user\">\r\n              <h5 class=\"font-primary f-18 mb-0\">Online</h5>\r\n            </div>\r\n            <div class=\"user-card-image\"><img class=\"img-fluid rounded-circle image-radius\" src=\"assets/images/dashboard/designer.jpg\" alt=\"\"></div>\r\n            <div class=\"user-deatils text-center\">\r\n              <h5>Marshi Kisteen</h5>\r\n              <h6 class=\"mb-0\">marshikisteen@gmail.com</h6>\r\n            </div>\r\n            <div class=\"user-badge text-center\"><a class=\"badge badge-pill badge-light\"  href=\"javascript:void(0)\">Dashboard</a><a class=\"badge badge-pill badge-light\"  href=\"javascript:void(0)\">Ui</a><a class=\"badge badge-pill badge-light\"  href=\"javascript:void(0)\">Xi</a><a  href=\"javascript:void(0)\"><span class=\"badge badge-pill badge-light active\">2+</span></a></div>\r\n            <div class=\"card-footer row pb-0 text-center\">\r\n              <div class=\"col-6\">\r\n                <div class=\"d-flex user-footer\"><app-feather-icons class=\"m-r-10\"  [icon]=\"'message-square'\"></app-feather-icons>\r\n                  <h6 class=\"f-18 mb-0\">Message</h6>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-6\">\r\n                <div class=\"d-flex user-footer\"><app-feather-icons class=\"m-r-10\"  [icon]=\"'briefcase'\"></app-feather-icons>\r\n                  <h6 class=\"f-18 mb-0\">Portfolio</h6>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-6\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header card-header-border\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-6\">\r\n                <h5>New User</h5>\r\n              </div>\r\n              <div class=\"col-sm-6\">\r\n                <div class=\"pull-right right-header\"><span>Month</span><span>\r\n                    <button class=\"btn btn-primary btn-pill\">Today</button></span></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"new-users\">\r\n              <div class=\"media\"><img class=\"rounded-circle image-radius m-r-15\" src=\"assets/images/user/2.png\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <h6 class=\"mb-0 f-w-700\">Nick Stone</h6>\r\n                  <p>Visual Designer, Github Inc</p>\r\n                </div><span class=\"pull-right\">\r\n                  <button class=\"btn btn-pill btn-outline-light\">Follow</button></span>\r\n              </div>\r\n              <div class=\"media\"><img class=\"rounded-circle image-radius m-r-15\" src=\"assets/images/user/5.jpg\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <h6 class=\"mb-0 f-w-700\">Milano Esco</h6>\r\n                  <p>Visual Designer, Github Inc</p>\r\n                </div><span class=\"pull-right\">\r\n                  <button class=\"btn btn-pill btn-outline-light\">Follow</button></span>\r\n              </div>\r\n              <div class=\"media\"><img class=\"rounded-circle image-radius m-r-15\" src=\"assets/images/user/3.jpg\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <h6 class=\"mb-0 f-w-700\">Wiltor Noice</h6>\r\n                  <p>Visual Designer, Github Inc</p>\r\n                </div><span class=\"pull-right\">\r\n                  <button class=\"btn btn-pill btn-outline-light\">Follow</button></span>\r\n              </div>\r\n              <div class=\"media\"><img class=\"rounded-circle image-radius m-r-15\" src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <h6 class=\"mb-0 f-w-700\">Anna Strong</h6>\r\n                  <p>Visual Designer, Github Inc</p>\r\n                </div><span class=\"pull-right\">\r\n                  <button class=\"btn btn-pill btn-outline-light\">Follow</button></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-6\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header card-header-border\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-6\">\r\n                <h5>Recent Notification</h5>\r\n              </div>\r\n              <div class=\"col-sm-6\">\r\n                <div class=\"pull-right right-header\">\r\n                  <div class=\"onhover-dropdown\">\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">All <span class=\"pr-0\"><i class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div right-header-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">Order</a><a class=\"d-block\"  href=\"javascript:void(0)\">Download</a><a class=\"d-block\"  href=\"javascript:void(0)\">Trash</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body recent-notification\">\r\n            <div class=\"media\">\r\n              <h6>09:00</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum dolor sit amit,consectetur eiusmdd.</span>\r\n                <p class=\"f-12\">By Kan</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <h6>10:50</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum.</span>\r\n                <p class=\"f-12\">By Tailer</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <h6>01:00</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum dolor sit amit,consectetur eiusmdd.</span>\r\n                <p class=\"f-12\">By Kaint</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <h6>05:00</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum dolor sit amit.</span>\r\n                <p class=\"f-12\">By call</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <h6>12:00</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum dolor sit.</span>\r\n                <p class=\"f-12\">By Waiter</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <h6>08:20</h6>\r\n              <div class=\"media-body\"><span>Lorem ipsum dolor sit amit,consectetur eiusmdd dolor sit amit.</span>\r\n                <p class=\"f-12\">By Comman</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-12\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <h5>Calculation</h5>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"show-value-top d-flex\">\r\n              <div class=\"value-left d-inline-block\">\r\n                <div class=\"square bg-primary d-inline-block\"></div><span>Total Income</span>\r\n              </div>\r\n              <div class=\"value-right d-inline-block\">\r\n                <div class=\"square d-inline-block bg-smooth-chart\"></div><span>Total Loss</span>\r\n              </div>\r\n            </div>\r\n            <x-chartist class=\"ct-svg flot-chart-container\" [data]=\"chart5.data\" [type]=\"chart5.type\" [options]=\"chart5.options\" [events]=\"chart5.events\">\r\n            </x-chartist>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-7 xl-100\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header card-header-border\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-6\">\r\n                <h5>Conversations</h5>\r\n              </div>\r\n              <div class=\"col-sm-6\">\r\n                <div class=\"right-header pull-right m-t-5\">\r\n                  <div class=\"onhover-dropdown\"><app-feather-icons [icon]=\"'more-horizontal'\"></app-feather-icons>\r\n                    <div class=\"onhover-show-div right-header-dropdown more-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">View</a><a class=\"d-block\"  href=\"javascript:void(0)\">Media</a><a class=\"d-block\"  href=\"javascript:void(0)\">Search</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body chat-box dashboard-chat\">\r\n            <div class=\"chat\">\r\n              <div class=\"media left-side-chat\"><img class=\"rounded-circle chat-user-img img-60 m-r-20\" src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"message-main\">\r\n                    <p class=\"mb-0\">Lorem Ipsum is simply dummy text of the </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"media right-side-chat\">\r\n                <div class=\"media-body text-right\">\r\n                  <div class=\"message-main\">\r\n                    <p class=\"pull-right\">Lorem Ipsum is simply dummy text of the printing and Lorem Ipsum has been the industry's</p>\r\n                    <div class=\"clearfix\"></div>\r\n                  </div>\r\n                  <div class=\"sub-message message-main\">\r\n                    <p class=\"pull-right mb-0\">Lorem Ipsum is simply dummy text of the </p>\r\n                    <div class=\"clearfix\"></div>\r\n                  </div>\r\n                </div><img class=\"rounded-circle chat-user-img img-60 m-l-20\" src=\"assets/images/user/5.jpg\" alt=\"\">\r\n              </div>\r\n              <div class=\"media left-side-chat\"><img class=\"rounded-circle chat-user-img img-60 m-r-20\" src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                <div class=\"media-body\">\r\n                  <div class=\"message-main\">\r\n                    <p>Lorem Ipsum is simply dummy text of the printing</p>\r\n                  </div>\r\n                  <div class=\"sub-message message-main smiley-bg\"><img src=\"assets/images/dashboard/smily.png\" alt=\"\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"media chat-footer bg-primary\"><i class=\"icon-face-smile\"></i>\r\n            <div class=\"media-body\">\r\n              <input class=\"form-control\" type=\"text\" placeholder=\"Type your message\" required=\"\">\r\n            </div><app-feather-icons [icon]=\"'send'\"></app-feather-icons>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-5 xl-100\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header card-header-border\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-6\">\r\n                <h5>Selling Update</h5>\r\n              </div>\r\n              <div class=\"col-sm-6\">\r\n                <div class=\"pull-right right-header\">\r\n                  <div class=\"onhover-dropdown\">\r\n                    <button class=\"btn btn-primary btn-pill\" type=\"button\">All <span class=\"pr-0\"><i class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div right-header-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">shipping</a><a class=\"d-block\"  href=\"javascript:void(0)\">Purchase</a><a class=\"d-block\"  href=\"javascript:void(0)\">Total Sell</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <div class=\"row\">\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'activity'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+500</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'anchor'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+120</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'box'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">-410</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'book'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">-155</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'compass'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+920</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center\"><app-feather-icons [icon]=\"'cpu'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+500</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center mb-0 xs-mb-selling\"><app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+500</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center mb-0 xs-mb-selling\"><app-feather-icons [icon]=\"'slack'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">+120</h5>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-sm-4\">\r\n                <div class=\"selling-update text-center mb-0\"><app-feather-icons [icon]=\"'umbrella'\"></app-feather-icons>\r\n                  <h5 class=\"mb-0 f-18\">-410</h5>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Container-fluid Ends-->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/e-commerce/e-commerce.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/e-commerce/e-commerce.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-7 xl-100\">\r\n      <div class=\"row\">\r\n        <div class=\"owl-carousel owl-theme owl-loaded owl-drag\" id=\"owl-carousel-14\">\r\n          <owl-carousel-o [options]=\"customOptions\">\r\n            <ng-container *ngFor=\"let slide of slidesStore\">\r\n              <ng-template carouselSlide [id]=\"slide.id\">\r\n                <div class=\"item\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-body ecommerce-icons text-center\">\r\n                      <app-feather-icons [icon]=\"slide.icon\"></app-feather-icons>\r\n                      <div><span>{{slide.title}}</span></div>\r\n                      <h4 class=\"font-primary mb-0 counter\" [CountTo]=slide.number [from]=\"0\" [duration]=\"0.5\"></h4>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ng-container>\r\n          </owl-carousel-o>\r\n        </div>\r\n\r\n        <div class=\"col-md-12\">\r\n          <div class=\"card\">\r\n            <div class=\"card-header\">\r\n              <h5>Total Sale</h5>\r\n            </div>\r\n            <div class=\"card-body charts-box\">\r\n              <div class=\"flot-chart-container\">\r\n                <canvas baseChart class=\"chart flot-chart-placeholder\" id=\"graph123\" [legend]=\"saleChartLegend\"\r\n                  [data]=\"saleChartData\" [labels]=\"saleChartLable\" [options]=\"saleChartOption\"\r\n                  [chartType]=\"saleChartType\" [colors]=\"saleChartColor\" height=\"80px\"></canvas>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-5 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Best Sellers</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"table-responsive sellers\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">Name</th>\r\n                  <th scope=\"col\">Sale</th>\r\n                  <th scope=\"col\">Stock</th>\r\n                  <th scope=\"col\">Categories</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/6.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/2.png\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/3.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/5.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-middle\"><img\r\n                        class=\"img-radius img-30 align-top m-r-15 rounded-circle\" src=\"assets/images/user/15.png\"\r\n                        alt=\"\">\r\n                      <div class=\"d-inline-block\">\r\n                        <p>Alana Slacker</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <p>8956</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>54</p>\r\n                  </td>\r\n                  <td>\r\n                    <p>Product No: 1</p>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"number-widgets\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body align-self-center\">\r\n                <h6 class=\"mb-0\">Payment Status</h6>\r\n              </div>\r\n              <div class=\"radial-bar radial-bar-95 radial-bar-primary\" data-label=\"95%\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"number-widgets\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body align-self-center\">\r\n                <h6 class=\"mb-0\">Work Process</h6>\r\n              </div>\r\n              <div class=\"radial-bar radial-bar-75 radial-bar-primary\" data-label=\"75%\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"number-widgets\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body align-self-center\">\r\n                <h6 class=\"mb-0\">Sale Completed</h6>\r\n              </div>\r\n              <div class=\"radial-bar radial-bar-90 radial-bar-primary\" data-label=\"90%\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"number-widgets\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body align-self-center\">\r\n                <h6 class=\"mb-0\">Payment Done</h6>\r\n              </div>\r\n              <div class=\"radial-bar radial-bar-80 radial-bar-primary\" data-label=\"80%\"></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Total Income</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body chart-block\">\r\n          <canvas baseChart class=\"chart\" id=\"myLineCharts\" [legend]=\"chartLegend1\" [data]=\"chartData1\"\r\n            [labels]=\"chartLable1\" [options]=\"chartOption1\" [chartType]=\"chartType1\" [colors]=\"chartColor1\"></canvas>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Total Profit</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body chart-block\">\r\n          <canvas baseChart class=\"chart\" id=\"profitchart\" [legend]=\"chartLegend2\" [data]=\"chartData2\"\r\n            [labels]=\"chartLable2\" [options]=\"chartOption2\" [chartType]=\"chartType2\" [colors]=\"chartColor2\"></canvas>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50 col-md-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Profile Status</h5>\r\n        </div>\r\n        <div class=\"card-body height-equal\">\r\n          <div class=\"progress-block\">\r\n            <div class=\"progress-title\"><span>Basic Information</span><span class=\"pull-right\">15/18</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 90%\" aria-valuenow=\"90\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-block\">\r\n            <div class=\"progress-title\"><span>Portfolio</span><span class=\"pull-right\">5/6</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 85%\" aria-valuenow=\"85\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-block\">\r\n            <div class=\"progress-title\"><span>Legal Document</span><span class=\"pull-right\">12/20</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 80%\" aria-valuenow=\"80\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-block\">\r\n            <div class=\"progress-title\"><span>Interest</span><span class=\"pull-right\">5/10</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"95\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-block\">\r\n            <div class=\"progress-title\"><span>Product Info</span><span class=\"pull-right\">15/17</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 75%\" aria-valuenow=\"75\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress-block mb-0\">\r\n            <div class=\"progress-title\"><span>Billing Details</span><span class=\"pull-right\">2/5</span></div>\r\n            <div class=\"progress\" style=\"height: 3px;\">\r\n              <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"95\"\r\n                aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50 col-md-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Logs</h5>\r\n        </div>\r\n        <div class=\"card-body height-equal log-content\">\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-odd\"></div><span>New User Registration</span><span class=\"pull-right\">14:12</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-even\"></div><span>New sale: souffle.</span><span class=\"pull-right\">19:00</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-odd\"></div><span>14 products added.</span><span class=\"pull-right\">05:22</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-even\"></div><span>New sale: Napole.</span><span class=\"pull-right\">08:45</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-odd\"></div><span>New User Registration</span><span class=\"pull-right\">06:51</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-even\"></div><span>New User Registration</span><span\r\n              class=\"pull-right\">09:42</span>\r\n          </div>\r\n          <div class=\"logs-element\">\r\n            <div class=\"circle-double-odd\"></div><span>New User Registration</span><span class=\"pull-right\">10:45</span>\r\n          </div>\r\n          <div class=\"logs-element mb-0\">\r\n            <div class=\"circle-double-even\"></div><span>New User Registration</span><span\r\n              class=\"pull-right\">02:05</span>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>statics</h5>      \r\n        </div>\r\n        <div class=\"card-body updating-chart height-equal\">\r\n          <div class=\"upadates text-center\">\r\n            <h2 class=\"font-primary\">\r\n              <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"89.68\"\r\n                [from]=\"0\" [duration]=\"2\"> </span><app-feather-icons [icon]=\"'arrow-up'\"></app-feather-icons>\r\n            </h2>\r\n            <p>Week2 +<span class=\"counter\" [CountTo]=\"15.44\" [from]=\"0\" [duration]=\"2\"></span></p>\r\n          </div>\r\n          <div class=\"flot-chart-container\">\r\n            <div class=\"flot-chart-placeholder\" id=\"updating-data-morris-chart\"> \r\n                <canvas baseChart class=\"chart flot-chart-placeholder\" id=\"graph123\" [legend]=\"staticChartLegend\"\r\n                  [data]=\"staticChartData\" [labels]=\"staticChartLable\" [options]=\"staticChartOption\"\r\n                  [chartType]=\"staticChartType\" [colors]=\"staticChartColor\"></canvas>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Shopping Cart</h5>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"table-responsive shopping-table text-center\">\r\n            <table class=\"table table-bordernone\">\r\n              <thead>\r\n                <tr>\r\n                  <th scope=\"col\">No</th>\r\n                  <th scope=\"col\">Product</th>\r\n                  <th scope=\"col\">Quantity</th>\r\n                  <th scope=\"col\">Status</th>\r\n                  <th scope=\"col\">Amount</th>\r\n                  <th scope=\"col\">Delete</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td>1</td>\r\n                  <td>Computer</td>\r\n                  <td>5</td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\">Active</button>\r\n                  </td>\r\n                  <td>15000</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'x'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>2</td>\r\n                  <td>Headphone</td>\r\n                  <td>8</td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\">Disable</button>\r\n                  </td>\r\n                  <td>8000</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'x'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>3</td>\r\n                  <td>Furniture</td>\r\n                  <td>3</td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\">Paused</button>\r\n                  </td>\r\n                  <td>12000</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'x'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>4</td>\r\n                  <td>shoes</td>\r\n                  <td>9</td>\r\n                  <td>\r\n                    <button class=\"btn btn-primary btn-pill\">On Way</button>\r\n                  </td>\r\n                  <td>5500</td>\r\n                  <td>\r\n                    <app-feather-icons [icon]=\"'x'\"></app-feather-icons>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Review</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"text-center ecommerce-knob\">\r\n           <div class=\"knob knob-block\" id=\"review\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/project/project.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/project/project.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\" id=\"project\">\r\n    <div class=\"col-sm-12\">\r\n      <ngb-tabset>\r\n        <ngb-tab>\r\n          <ng-template ngbTabTitle class=\"nav-item\">\r\n            <app-feather-icons [icon]=\"'home'\" class=\"custom-ic\"></app-feather-icons>Home\r\n            <div class=\"material-border\"></div>\r\n          </ng-template>\r\n          <ng-template ngbTabContent class=\"tab-content\">\r\n            <div class=\"tab-pane fade show\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home\">\r\n              <div class=\"row\">\r\n                <div class=\"col-xl-3 col-sm-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                      <div class=\"media\">\r\n                        <div class=\"select2-drpdwn-project select-options\">\r\n                          <select class=\"form-control form-control-primary btn-square\" name=\"select\">\r\n                            <option value=\"opt1\">Today</option>\r\n                            <option value=\"opt2\">Yesterday</option>\r\n                            <option value=\"opt3\">Tomorrow</option>\r\n                            <option value=\"opt4\">Monthly</option>\r\n                            <option value=\"opt5\">Weekly</option>\r\n                          </select>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"project-widgets text-center\">\r\n                        <h1 class=\"font-primary counter\" [CountTo]=\"45\" [from]=\"0\" [duration]=\"1\"></h1>\r\n                        <h6 class=\"mb-0\">Due Tasks</h6>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-footer project-footer\">\r\n                      <h6 class=\"mb-0\">Completed: <span class=\"counter\" [CountTo]=\"14\" [from]=\"0\" [duration]=\"1\"></span>\r\n                      </h6>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-sm-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                      <div class=\"media\">\r\n                        <h5 class=\"mb-0\">Features</h5>\r\n                      </div>\r\n                      <div class=\"project-widgets text-center\">\r\n                        <h1 class=\"font-primary counter\" [CountTo]=\"80\" [from]=\"0\" [duration]=\"1\"></h1>\r\n                        <h6 class=\"mb-0\">Proposals</h6>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-footer project-footer\">\r\n                      <h6 class=\"mb-0\">Implemented: <span class=\"counter\" [CountTo]=\"14\" [from]=\"0\"\r\n                          [duration]=\"1\"></span></h6>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-sm-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                      <div class=\"media\">\r\n                        <h5 class=\"mb-0\">Issues</h5>\r\n                      </div>\r\n                      <div class=\"project-widgets text-center\">\r\n                        <h1 class=\"font-primary counter\" [CountTo]=\"34\" [from]=\"0\" [duration]=\"1\"></h1>\r\n                        <h6 class=\"mb-0\">Open</h6>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-footer project-footer\">\r\n                      <h6 class=\"mb-0\">Closed today: <span class=\"counter\" [CountTo]=\"10\" [from]=\"0\"\r\n                          [duration]=\"1\"></span></h6>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-sm-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-body\">\r\n                      <div class=\"media\">\r\n                        <h5 class=\"mb-0\">Overdue</h5>\r\n                      </div>\r\n                      <div class=\"project-widgets text-center\">\r\n                        <h1 class=\"font-primary counter\" [CountTo]=\"7\" [from]=\"0\" [duration]=\"1\"></h1>\r\n                        <h6 class=\"mb-0\">Tasks</h6>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-footer project-footer\">\r\n                      <h6 class=\"mb-0\">Task Solved: <span class=\"counter\" [CountTo]=\"4\" [from]=\"0\"\r\n                          [duration]=\"1\"></span></h6>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header project-header\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-8\">\r\n                          <h5>Task Distribution</h5>\r\n                        </div>\r\n                        <div class=\"col-sm-4\">\r\n                          <div class=\"select2-drpdwn-project select-options\">\r\n                            <select class=\"form-control form-control-primary btn-square\" name=\"select\">\r\n                              <option value=\"opt1\">Today</option>\r\n                              <option value=\"opt2\">Yesterday</option>\r\n                              <option value=\"opt3\">Tomorrow</option>\r\n                              <option value=\"opt4\">Monthly</option>\r\n                              <option value=\"opt5\">Weekly</option>\r\n                            </select>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-body chart-block chart-vertical-center project-charts\">\r\n                      <ngx-charts-pie-chart [view]=\"view\" [scheme]=\"doughnutChartColorScheme\" [results]=\"doughnutData\"\r\n                        [explodeSlices]=\"true\" [labels]=\"doughnutChartShowLabels\" [arcWidth]=0.50 [doughnut]=\"true\"\r\n                        [gradient]=\"doughnutChartGradient\" (select)=\"onSelect($event)\">\r\n                      </ngx-charts-pie-chart>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-6\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header project-header\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-8\">\r\n                          <h5>Schedule</h5>\r\n                        </div>\r\n                        <div class=\"col-sm-4\">\r\n                          <div class=\"select2-drpdwn-project select-options\">\r\n                            <select class=\"form-control form-control-primary btn-square\" name=\"select\">\r\n                              <option value=\"opt1\">Today</option>\r\n                              <option value=\"opt2\">Yesterday</option>\r\n                              <option value=\"opt3\">Tomorrow</option>\r\n                              <option value=\"opt4\">Monthly</option>\r\n                              <option value=\"opt5\">Weekly</option>\r\n                            </select>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                      <div class=\"schedule\">\r\n                        <div class=\"media\">\r\n                          <div class=\"media-body\">\r\n                            <h6>Group Meeting</h6>\r\n                            <p>30 minutes</p>\r\n                          </div>\r\n                          <div ngbDropdown class=\"dropdown schedule-dropdown d-inline-block\">\r\n                            <button class=\"btn\" id=\"dropdownMenuButton\" ngbDropdownToggle>\r\n                              <app-feather-icons [icon]=\"'more-horizontal'\"></app-feather-icons>\r\n                            </button>\r\n                            <div ngbDropdownMenu aria-labelledby=\"dropdownBasic1\">\r\n                              <button class=\"dropdown-item\">Project</button>\r\n                              <button class=\"dropdown-item\">Requirements</button>\r\n                              <button class=\"dropdown-item\">Discussion</button>\r\n                              <button class=\"dropdown-item\">Planning</button>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"media\">\r\n                          <div class=\"media-body\">\r\n                            <h6>Public Beta Release</h6>\r\n                            <p>10:00 PM</p>\r\n                          </div>\r\n                          <app-feather-icons [icon]=\"'more-horizontal'\"></app-feather-icons>\r\n                        </div>\r\n                        <div class=\"media\">\r\n                          <div class=\"media-body\">\r\n                            <h6>Lunch</h6>\r\n                            <p>12:30 PM</p>\r\n                          </div>\r\n                          <app-feather-icons [icon]=\"'more-horizontal'\"></app-feather-icons>\r\n                        </div>\r\n                        <div class=\"media\">\r\n                          <div class=\"media-body\">\r\n                            <h6>Clients Timing</h6>\r\n                            <p>2:00 PM to 6:00 PM</p>\r\n                          </div>\r\n                          <app-feather-icons [icon]=\"'more-horizontal'\"></app-feather-icons>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header\">\r\n                      <h5>Github Issues</h5>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-xl-6 xl-100\">\r\n                          <div class=\"row more-projects\">\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Created</h6>\r\n                                  <h5 class=\"counter mb-0\">27</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-1 project-small\" [data]=\"chart1.data\"\r\n                                  [type]=\"chart1.type\" [options]=\"chart1.options\" [events]=\"chart1.events\">\r\n                                </x-chartist>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Fixed</h6>\r\n                                  <h5 class=\"counter mb-0\">8</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-2 project-small\" [type]=\"chart2.type\"\r\n                                  [data]=\"chart2.data\" [options]=\"chart2.options\" [events]=\"chart2.events\"></x-chartist>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Re-opened</h6>\r\n                                  <h5 class=\"counter mb-0\">2</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-3 project-small\" [type]=\"chart3.type\"\r\n                                  [data]=\"chart3.data\" [options]=\"chart3.options\" [events]=\"chart3.events\"></x-chartist>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Closed</h6>\r\n                                  <h5 class=\"counter mb-0\">10</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-4 project-small\" [type]=\"chart4.type\"\r\n                                  [data]=\"chart4.data\" [options]=\"chart4.options\" [events]=\"chart4.events\"></x-chartist>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Wont'fix</h6>\r\n                                  <h5 class=\"counter mb-0\">25</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-5 project-small\" [type]=\"chart5.type\"\r\n                                  [data]=\"chart5.data\" [options]=\"chart5.options\" [events]=\"chart5.events\"></x-chartist>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6 xl-4\">\r\n                              <div class=\"projects-main\">\r\n                                <div class=\"project-content\">\r\n                                  <h6>Need's test</h6>\r\n                                  <h5 class=\"counter mb-0\">5</h5>\r\n                                </div>\r\n                                <x-chartist class=\"project-small-chart-6 project-small\" [type]=\"chart6.type\"\r\n                                  [data]=\"chart6.data\" [options]=\"chart6.options\" [events]=\"chart6.events\"></x-chartist>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col-xl-6 xl-100 github-lg\">\r\n                          <div class=\"show-value-top d-flex\">\r\n                            <div class=\"value-left d-inline-block\">\r\n                              <div class=\"square bg-primary d-inline-block\"></div><span>Closed Issues</span>\r\n                            </div>\r\n                            <div class=\"value-right d-inline-block\">\r\n                              <div class=\"square d-inline-block bg-secondary\"></div><span>Issues</span>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"flot-chart-container\">\r\n                            <div class=\"flot-chart-placeholder\" id=\"github-issues\">\r\n                              <ngx-charts-bar-vertical-stacked [scheme]=\"verticalStackChartColorScheme\"\r\n                                [results]=\"multiData\" [gradient]=\"verticalStackChartgradient\"\r\n                                [xAxis]=\"verticalStackChartshowXAxis\" [yAxis]=\"verticalStackChartshowYAxis\"\r\n                                [legend]=\"verticalStackChartshowLegend\" [showXAxisLabel]=\"showXAxisLabel\"\r\n                                [showYAxisLabel]=\"showYAxisLabel\" [barPadding]=20 [yScaleMax]=8\r\n                                (select)=\"onSelect($event)\">\r\n                              </ngx-charts-bar-vertical-stacked>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n        </ngb-tab>\r\n        <ngb-tab>\r\n          <ng-template ngbTabTitle class=\"nav-item\">\r\n            <app-feather-icons [icon]=\"'activity'\" class=\"custom-ic\"></app-feather-icons>Budget Summary\r\n            <div class=\"material-border\"></div>\r\n          </ng-template>\r\n          <ng-template ngbTabContent class=\"tab-content\">\r\n            <div class=\"tab-pane fade show\" id=\"budget\" role=\"tabpanel\" aria-labelledby=\"budget\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header\">\r\n                      <h5>Current Progress</h5>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                      <div class=\"table-responsive current-progress\">\r\n                        <table class=\"table table-bordernone\">\r\n                          <tbody>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/1.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <h6>Web application</h6>\r\n                                    <p>Design & development</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><span>Latest Updated Today at 1:30\r\n                                    PM</span><span class=\"ml-current\"><i class=\"fa fa-clock-o\"></i>10:32</span><span\r\n                                    class=\"ml-current\"><i class=\"fa fa-comment\"></i>540</span></div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"progress sm-progress-bar\">\r\n                                  <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 60%\"\r\n                                    aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                                </div>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <h6>Login module</h6>\r\n                                    <p>Development</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><span>Latest Updated Today at 4:00\r\n                                    PM</span><span class=\"ml-current\"><i class=\"fa fa-clock-o\"></i>1:32</span><span\r\n                                    class=\"ml-current\"><i class=\"fa fa-comment\"></i>700</span></div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"progress sm-progress-bar\">\r\n                                  <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 50%\"\r\n                                    aria-valuenow=\"50\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                                </div>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/7.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <h6>Module testing</h6>\r\n                                    <p>Testing</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><span>Latest Updated Today at 5:45\r\n                                    PM</span><span class=\"ml-current\"><i class=\"fa fa-clock-o\"></i>11:40</span><span\r\n                                    class=\"ml-current\"><i class=\"fa fa-comment\"></i>425</span></div>\r\n                              </td>\r\n                              <td>\r\n                                <div class=\"progress sm-progress-bar\">\r\n                                  <div class=\"progress-bar bg-primary\" role=\"progressbar\" style=\"width: 90%\"\r\n                                    aria-valuenow=\"90\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                                </div>\r\n                              </td>\r\n                            </tr>\r\n                          </tbody>\r\n                        </table>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-6 xl-100\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header\">\r\n                      <h5>Budget Distribution</h5>\r\n                    </div>\r\n                    <div class=\"card-body chart-block\">\r\n                      <div class=\"flot-chart-container budget-chart\">\r\n                        <google-chart [data]=\"pieChart1\"></google-chart>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-xl-6 xl-100\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header project-header\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-8\">\r\n                          <h5>Spent</h5>\r\n                        </div>\r\n                        <div class=\"col-sm-4\">\r\n                          <div class=\"select2-drpdwn-project select-options\">\r\n                            <select class=\"form-control form-control-primary btn-square\" name=\"select\">\r\n                              <option value=\"opt1\">Today</option>\r\n                              <option value=\"opt2\">Yesterday</option>\r\n                              <option value=\"opt3\">Tomorrow</option>\r\n                              <option value=\"opt4\">Monthly</option>\r\n                              <option value=\"opt5\">Weekly</option>\r\n                            </select>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-body spent\">\r\n                      <div class=\"spent-graph\">\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"project-budget\">\r\n                            <h6>Weekly spent</h6>\r\n                            <h2 class=\"mb-0\"><span><i data-feather=\"dollar-sign\"></i>12,5000</span></h2>\r\n                          </div>\r\n                          <div class=\"projects-main mb-0\">\r\n                            <x-chartist [data]=\"barChartSingle1.data\" [type]=\"barChartSingle1.type\"\r\n                              [options]=\"barChartSingle1.options\">\r\n                            </x-chartist>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-body spent\">\r\n                      <div class=\"spent-graph\">\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"project-budget\">\r\n                            <h6>Total spent</h6>\r\n                            <h2 class=\"mb-0\"><span><i data-feather=\"dollar-sign\"></i>15,7452</span></h2>\r\n                          </div>\r\n                          <div class=\"projects-main mb-0\">\r\n                            <x-chartist [data]=\"barChartSingle2.data\" [type]=\"barChartSingle2.type\"\r\n                              [options]=\"barChartSingle2.options\">\r\n                            </x-chartist>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-body spent\">\r\n                      <div class=\"spent-graph\">\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"project-budget\">\r\n                            <h6>Remaining</h6>\r\n                            <h2 class=\"mb-0\"><span><i data-feather=\"dollar-sign\"></i>18,5438</span></h2>\r\n                          </div>\r\n                          <div class=\"projects-main mb-0\">\r\n                            <x-chartist [data]=\"barChartSingle3.data\" [type]=\"barChartSingle3.type\"\r\n                              [options]=\"barChartSingle3.options\">\r\n                            </x-chartist>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"card-footer spent\">\r\n                      <div class=\"spent-graph\">\r\n                        <div class=\"d-flex\">\r\n                          <div class=\"project-budget m-0\">\r\n                            <h6>Total Budget</h6>\r\n                            <h2 class=\"mb-0\"><span><i data-feather=\"dollar-sign\"></i>34,5812</span></h2>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n        </ngb-tab>\r\n        <ngb-tab>\r\n          <ng-template ngbTabTitle class=\"nav-item\">\r\n            <app-feather-icons [icon]=\"'users'\" class=\"custom-ic\"></app-feather-icons>Team Members\r\n            <div class=\"material-border\"></div>\r\n          </ng-template>\r\n          <ng-template ngbTabContent class=\"tab-content\">\r\n            <div class=\"tab-pane fade show\" id=\"team-members\" role=\"tabpanel\" aria-labelledby=\"team-members\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"card\">\r\n                    <div class=\"card-header\">\r\n                      <h5>Team Members</h5>\r\n\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                      <div class=\"table-responsive sellers team-members\">\r\n                        <table class=\"table table-bordernone\">\r\n                          <thead>\r\n                            <tr>\r\n                              <th scope=\"col\">Name</th>\r\n                              <th scope=\"col\">Position</th>\r\n                              <th scope=\"col\">Office</th>\r\n                              <th scope=\"col\">E-Mail</th>\r\n                              <th scope=\"col\">Phone</th>\r\n                            </tr>\r\n                          </thead>\r\n                          <tbody>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/6.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Jerry Patterson</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Design Manager</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Integer</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>jerry13@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+91 264 570 4611</p>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/2.png\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Rosa Matthews</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Director of Sales</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Ipsum</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>ros456@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+01 967 487 1873</p>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/3.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Alvaro Aguirre</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Office Assistant</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Praesent</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>alvar76@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+48 724 585 0012</p>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/15.png\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Jerry Patterson</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Programmer Analyst</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Ipsum</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>jerry13@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+91 264 570 4611</p>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Anne Snyder</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Social Worker</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Donec</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>annsny@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+61 480 087 1175</p>\r\n                              </td>\r\n                            </tr>\r\n                            <tr>\r\n                              <td>\r\n                                <div class=\"d-inline-block align-middle\"><img\r\n                                    class=\"img-radius img-50 align-top m-r-15 rounded-circle\"\r\n                                    src=\"assets/images/user/5.jpg\" alt=\"\">\r\n                                  <div class=\"d-inline-block\">\r\n                                    <p>Alana Slacker</p>\r\n                                  </div>\r\n                                </div>\r\n                              </td>\r\n                              <td>\r\n                                <p>Systems Engineer</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>Etiam</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>alana82@gmail.com</p>\r\n                              </td>\r\n                              <td>\r\n                                <p>+75 483 761 4680</p>\r\n                              </td>\r\n                            </tr>\r\n                          </tbody>\r\n                        </table>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ng-template>\r\n        </ngb-tab>\r\n      </ngb-tabset>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/server/server.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/server/server.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card server-card-bg\">\r\n        <div class=\"card-body server-widgets\">\r\n          <div class=\"media\">\r\n            <app-feather-icons [icon]=\"'codepen'\"></app-feather-icons>\r\n            <div class=\"top-server\">\r\n              <h6 class=\"mb-0\">Storage</h6>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-server\">\r\n            <h5 class=\"mb-0\">85GB / <span>100Gb</span></h5>\r\n          </div>\r\n          <div class=\"progress\">\r\n            <div class=\"progress-bar-animated bg-primary progress-bar-striped\" role=\"progressbar\" style=\"width: 25%\"\r\n              aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card server-card-bg\">\r\n        <div class=\"card-body server-widgets\">\r\n          <div class=\"media\">\r\n            <app-feather-icons [icon]=\"'clock'\"></app-feather-icons>\r\n            <div class=\"top-server\">\r\n              <h6 class=\"mb-0\">Latency</h6>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-server\">\r\n            <h5><span class=\"second-color counter mb-0\" [CountTo]=\"40\" [from]=\"0\" [duration]=\"2\"></span><span>/ms</span>\r\n            </h5>\r\n          </div>\r\n          <div class=\"progress\">\r\n            <div class=\"progress-bar-animated bg-primary progress-bar-striped\" role=\"progressbar\" style=\"width: 85%\"\r\n              aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card server-card-bg\">\r\n        <div class=\"card-body server-widgets\">\r\n          <div class=\"media\">\r\n            <app-feather-icons [icon]=\"'server'\"></app-feather-icons>\r\n            <div class=\"top-server\">\r\n              <h6 class=\"mb-0\">Bandwidth</h6>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-server\">\r\n            <h5 class=\"mb-0\">75GB / <span>100Gb</span></h5>\r\n          </div>\r\n          <div class=\"progress\">\r\n            <div class=\"progress-bar-animated bg-primary progress-bar-striped\" role=\"progressbar\" style=\"width: 75%\"\r\n              aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-3 xl-50 col-sm-6\">\r\n      <div class=\"card server-card-bg\">\r\n        <div class=\"card-body server-widgets\">\r\n          <div class=\"media\">\r\n            <app-feather-icons [icon]=\"'anchor'\"></app-feather-icons>\r\n            <div class=\"top-server\">\r\n              <h6 class=\"mb-0\">Cluster Load</h6>\r\n            </div>\r\n          </div>\r\n          <div class=\"bottom-server\">\r\n            <h5 class=\"mb-0\">70% <span>\r\n                <app-feather-icons [icon]=\"'trending-down'\"></app-feather-icons>\r\n              </span></h5>\r\n          </div>\r\n          <div class=\"progress\">\r\n            <div class=\"progress-bar-animated bg-primary progress-bar-striped\" role=\"progressbar\" style=\"width: 50%\"\r\n              aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Live CPU Usage</h5>\r\n        </div>\r\n        <div class=\"card-body chart-block\">\r\n          <div class=\"server-chart-container\">\r\n            <canvas baseChart class=\"chart flot-chart-placeholder\" id=\"cpu-updating\" [legend]=\"cpuChartLegend\"\r\n              [data]=\"cpuChartData\" [labels]=\"cpuChartLabels\" [options]=\"cpuChartOptions\" [chartType]=\"cpuChartType\"\r\n              [colors]=\"cpuChartColors\"></canvas>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Memory Usage</h5>\r\n        </div>\r\n        <div class=\"card-body server-canvas\">\r\n          <canvas baseChart class=\"chart\" id=\"myGraph\" [legend]=\"memoryChartLegend\" [datasets]=\"memoryChartData\"\r\n            [labels]=\"memoryChartLabels\" [options]=\"memoryChartOptions\" [chartType]=\"memoryChartType\"\r\n            [colors]=\"memoryChartColors\"></canvas>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header server-header\">\r\n          <h5 class=\"d-inline-block\">Process Explorer</h5><span\r\n            class=\"badge badge-primary m-l-10 d-inline-block mt-0\">57 Process</span>\r\n        </div>\r\n        <div class=\"card-body server-datatable\">\r\n          <div class=\"table-responsive\">\r\n            <ng2-smart-table [settings]=\"settings\" [source]=\"explorer\"></ng2-smart-table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header server-header\">\r\n          <h5 class=\"d-inline-block\">I/O Activity</h5><span class=\"badge badge-primary m-l-10 d-inline-block mt-0\">Last\r\n            10 Activity</span>\r\n\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"server-activity\">\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n              <span>Brandley</span>\r\n              <div class=\"media-body text-right\"><span>14 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons><span>Cara</span>\r\n              <div class=\"media-body text-right\"><span>5 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-secondary m-r-10\" [icon]=\"'arrow-up'\"></app-feather-icons><span>Airi</span>\r\n              <div class=\"media-body text-right\"><span>15 MB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-up'\"></app-feather-icons><span>Cedric</span>\r\n              <div class=\"media-body text-right\"><span>4 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-secondary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n              <span>Cara</span>\r\n              <div class=\"media-body text-right\"><span>20 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-up'\"></app-feather-icons><span>Airi</span>\r\n              <div class=\"media-body text-right\"><span>25 MB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n              <span>Brandley</span>\r\n              <div class=\"media-body text-right\"><span>14 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons><span>Cara</span>\r\n              <div class=\"media-body text-right\"><span>5 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-secondary m-r-10\" [icon]=\"'arrow-up'\"></app-feather-icons><span>Airi</span>\r\n              <div class=\"media-body text-right\"><span>15 MB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-primary m-r-10\" [icon]=\"'arrow-up'\"></app-feather-icons><span>Cedric</span>\r\n              <div class=\"media-body text-right\"><span>4 KB</span></div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <app-feather-icons class=\"font-secondary m-r-10\" [icon]=\"'arrow-down'\"></app-feather-icons>\r\n              <span>Cara</span>\r\n              <div class=\"media-body text-right\"><span>20 KB</span></div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/university/university.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/dashboard/university/university.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"card\" data-intro=\"This is University Earning Chart\">\r\n        <div class=\"card-header university-header\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h5>University Earning</h5>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"pull-right d-flex buttons-right\">\r\n                <div class=\"right-header\">\r\n                  <div class=\"onhover-dropdown\">\r\n                    <button class=\"btn btn-primary\" type=\"button\">Monthly <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div right-header-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">Average</a><a\r\n                        class=\"d-block\"  href=\"javascript:void(0)\">Maximum</a><a class=\"d-block\"  href=\"javascript:void(0)\">Minimum</a></div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"right-header\">\r\n                  <div class=\"onhover-dropdown\">\r\n                    <button class=\"btn btn-light\" type=\"button\">yearly <span class=\"pr-0\"><i\r\n                          class=\"fa fa-angle-down\"></i></span></button>\r\n                    <div class=\"onhover-show-div right-header-dropdown\"><a class=\"d-block\"  href=\"javascript:void(0)\">Average</a><a\r\n                        class=\"d-block\"  href=\"javascript:void(0)\">Maximum</a><a class=\"d-block\"  href=\"javascript:void(0)\">Minimum</a></div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body height-curves\">\r\n          <div class=\"curves-2\">\r\n            <x-chartist class=\"animate-curve ct-golden-section\" [data]=\"chart1.data\" [type]=\"chart1.type\"\r\n              [options]=\"chart1.options\" [events]=\"chart1.events\">\r\n            </x-chartist>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6 xl-100\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"media feather-main\">\r\n                <div class=\"feather-icon-block\">\r\n                  <app-feather-icons [icon]=\"'command'\"></app-feather-icons>\r\n                </div>\r\n                <div class=\"media-body align-self-center\">\r\n                  <h6>Total Admission</h6>\r\n                  <p>5678</p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"media feather-main\">\r\n                <div class=\"feather-icon-block\">\r\n                  <app-feather-icons [icon]=\"'navigation'\"></app-feather-icons>\r\n                </div>\r\n                <div class=\"media-body align-self-center\">\r\n                  <h6>Total University Visit</h6>\r\n                  <p>8569</p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"media chart-university\">\r\n                <div class=\"media-body\">\r\n                  <h3 class=\"mb-0\">\r\n                    <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"5683\"\r\n                      [from]=\"0\" [duration]=\"2\"></span>\r\n                  </h3>\r\n                  <p>Html Course</p>\r\n                </div>\r\n                <div class=\"small-bar\">\r\n                  <x-chartist class=\"ct-small-left flot-chart-container\" [data]=\"chart4.data\" [type]=\"chart4.type\"\r\n                    [options]=\"chart4.options\" [events]=\"chart4.events\">\r\n                  </x-chartist>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-body\">\r\n              <div class=\"media chart-university\">\r\n                <div class=\"media-body\">\r\n                  <h3 class=\"mb-0\">\r\n                    <app-feather-icons [icon]=\"'dollar-sign'\"></app-feather-icons><span class=\"counter\" [CountTo]=\"7243\"\r\n                      [from]=\"0\" [duration]=\"2\"></span>\r\n                  </h3>\r\n                  <p>PHP Course</p>\r\n                </div>\r\n                <div class=\"small-bar\">\r\n                  <x-chartist class=\"ct-small-right flot-chart-container\" [data]=\"chart5.data\" [type]=\"chart5.type\"\r\n                    [options]=\"chart5.options\" [events]=\"chart5.events\">\r\n                  </x-chartist>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-header\">\r\n              <h5>Math Professors</h5>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <div class=\"table-responsive professor-table\">\r\n                <table class=\"table table-bordernone\">\r\n                  <tbody>\r\n                    <tr>\r\n                      <td><img class=\"img-radius img-35 align-top m-r-15 rounded-circle\"\r\n                          src=\"assets/images/university/math-1.jpg\" alt=\"\">\r\n                        <div class=\"professor-block d-inline-block\">luson keter\r\n                          <p>Math Professors</p>\r\n                        </div>\r\n                      </td>\r\n                      <td>\r\n                        <label class=\"pull-right mb-0\" for=\"edo-ani\">\r\n                          <input class=\"radio_animated\" id=\"edo-ani\" type=\"radio\" name=\"rdo-ani\">\r\n                        </label>\r\n                      </td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td><img class=\"img-radius img-25 align-top m-r-15 rounded-circle\"\r\n                          src=\"assets/images/university/math-2.jpg\" alt=\"\">\r\n                        <div class=\"professor-block d-inline-block\">Elan hormas\r\n                          <p>Bio Professors</p>\r\n                        </div>\r\n                      </td>\r\n                      <td>\r\n                        <label class=\"pull-right mb-0\" for=\"edo-ani1\">\r\n                          <input class=\"radio_animated\" id=\"edo-ani1\" type=\"radio\" name=\"rdo-ani\" checked=\"\">\r\n                        </label>\r\n                      </td>\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"card\">\r\n            <div class=\"card-header\">\r\n              <h5>Bio Professors</h5>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <div class=\"table-responsive professor-table\">\r\n                <table class=\"table table-bordernone\">\r\n                  <tbody>\r\n                    <tr>\r\n                      <td><img class=\"img-radius img-25 align-top m-r-15 rounded-circle\"\r\n                          src=\"assets/images/university/bio-1.jpg\" alt=\"\">\r\n                        <div class=\"professor-block d-inline-block\">Erana siddy\r\n                          <p>Director</p>\r\n                        </div>\r\n                      </td>\r\n                      <td>\r\n                        <label class=\"pull-right mb-0\" for=\"edo-ani2\">\r\n                          <input class=\"radio_animated\" id=\"edo-ani2\" type=\"radio\" name=\"rdo-ani\" checked=\"\">\r\n                        </label>\r\n                      </td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td><img class=\"img-radius img-35 align-top m-r-15 rounded-circle\"\r\n                          src=\"assets/images/university/bio-2.jpg\" alt=\"\">\r\n                        <div class=\"professor-block d-inline-block\">Tom kerrly\r\n                          <p>Director</p>\r\n                        </div>\r\n                      </td>\r\n                      <td>\r\n                        <label class=\"pull-right mb-0\" for=\"edo-ani3\">\r\n                          <input class=\"radio_animated\" id=\"edo-ani3\" type=\"radio\" name=\"rdo-ani\">\r\n                        </label>\r\n                      </td>\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card height-equal\">\r\n        <div class=\"card-header\">\r\n          <h5>Upcoming Event</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"upcoming-event\">\r\n            <div class=\"upcoming-innner media\">\r\n              <div class=\"bg-primary left m-r-20\">\r\n                <app-feather-icons [icon]=\"'help-circle'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <p class=\"mb-0\">Demo Content <span class=\"pull-right\">Mar 18</span></p>\r\n                <h6 class=\"f-w-600\">Quiz Compition</h6>\r\n                <p class=\"mb-0\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem ipsum\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"upcoming-innner media\">\r\n              <div class=\"bg-primary left m-r-20\">\r\n                <app-feather-icons [icon]=\"'mic'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <p class=\"mb-0\">Demo Content <span class=\"pull-right\">Sep 18</span></p>\r\n                <h6 class=\"f-w-600\">Singing Compition</h6>\r\n                <p class=\"mb-0\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem ipsum\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"upcoming-innner media\">\r\n              <div class=\"bg-primary left m-r-20\">\r\n                <app-feather-icons [icon]=\"'zap'\"></app-feather-icons>\r\n              </div>\r\n              <div class=\"media-body\">\r\n                <p class=\"mb-0\">Demo Content <span class=\"pull-right\">Dec 18</span></p>\r\n                <h6 class=\"f-w-600\">Diwali Celebration</h6>\r\n                <p class=\"mb-0\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem ipsum\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card height-equal\" data-intro=\"This is Ranker Ratio\">\r\n        <div class=\"card-header\">\r\n          <h5>Ranker Ratio</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"knob-block text-center knob-sm\">\r\n            <div class=\"knob\" id=\"ranker\"></div>\r\n          </div>\r\n          <div class=\"ranker text-center\">\r\n            <h6>Student</h6>\r\n            <h5 class=\"mb-0\">New Ranker 2018</h5>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card height-equal\">\r\n        <div class=\"card-header\">\r\n          <h5>Notification</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"notifiaction-media\">\r\n            <div class=\"media\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>You are confirmation visit..<span class=\"pull-right f-12\">1 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>Lorem Ipsum has been the..<span class=\"pull-right f-12\">5 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>Standard dummy text ever..<span class=\"pull-right f-12\">7 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>When an unknown printer..<span class=\"pull-right f-12\">9 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"media\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>Took a gallery of type..<span class=\"pull-right f-12\">6 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n            <div class=\"media mb-0\">\r\n              <div class=\"media-body\">\r\n                <div class=\"circle-left\"></div>\r\n                <h6>Scrambled it to make a type..<span class=\"pull-right f-12\">2 Day Ago</span></h6>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-footer btn-more text-center\"><a  href=\"javascript:void(0)\">MORE...</a></div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h5>Statistics</h5>\r\n              <button class=\"btn btn-primary btn-sm header-btn btn-pill\">2017</button>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"pull-right statistics\">\r\n                <h5 class=\"counter\" [CountTo]=\"85\" [from]=\"0\" [duration]=\"2\"></h5>\r\n                <p class=\"f-12 mb-0\">Statistics 2017</p>\r\n                <div class=\"font-primary font-weight-bold d-flex f-11 pull-right\"><i\r\n                    class=\"fa fa-sort-up mr-2\"></i><span class=\"number\"><span class=\"counter\" [CountTo]=\"100\" [from]=\"0\"\r\n                      [duration]=\"2\"></span>%</span></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"curves-2\">\r\n            <x-chartist class=\"animate-curve2 ct-golden-section\" [data]=\"chart2.data\" [type]=\"chart2.type\"\r\n              [options]=\"chart2.options\" [events]=\"chart2.events\">\r\n            </x-chartist>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h5>Last 5 Year Board</h5>\r\n              <p class=\"f-12 header-small mb-0\">18 september, 2018</p>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"pull-right statistics\">\r\n                <h5 class=\"counter\" [CountTo]=\"50\" [from]=\"0\" [duration]=\"2\"></h5>\r\n                <p class=\"f-12 mb-0\">Board 2018</p>\r\n                <div class=\"font-primary font-weight-bold d-flex f-11 pull-right\"><i\r\n                    class=\"fa fa-sort-up mr-2\"></i><span class=\"number\"><span class=\"counter\" [CountTo]=\"78\" [from]=\"0\"\r\n                      [duration]=\"2\"></span>%</span></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <x-chartist class=\"board-chart ct-golden-section\" [data]=\"chart3.data\" [type]=\"chart3.type\"\r\n            [options]=\"chart3.options\" [events]=\"chart3.events\">\r\n          </x-chartist>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 xl-50\">\r\n      <div class=\"card\" data-intro=\"This is Our Topper List\">\r\n        <div class=\"card-header\">\r\n          <h5>Our Topper List</h5>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"table-responsive topper-lists\">\r\n            <table class=\"table table-bordernone\">\r\n              <tbody>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-self-center\">\r\n                      <div class=\"form-group d-inline-block\">\r\n                        <div class=\"checkbox\">\r\n                          <input id=\"checkbox1\" type=\"checkbox\">\r\n                          <label for=\"checkbox1\"></label>\r\n                        </div>\r\n                      </div><img class=\"img-radius img-40 align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/4.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\"><span class=\"f-w-600\">Ossim Keter</span>\r\n                        <p>1<sup>st</sup> year</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">+48 605 562 215</span>\r\n                      <p>Phone Number</p>\r\n                    </div>\r\n                  </td>\r\n                  <td><img class=\"align-top\" src=\"assets/images/university/chart-1.png\" alt=\"\"></td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">1</span>\r\n                      <p>Rank</p>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">590/600</span>\r\n                      <p>Total marks</p>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-self-center\">\r\n                      <div class=\"form-group d-inline-block\">\r\n                        <div class=\"checkbox\">\r\n                          <input id=\"checkbox2\" type=\"checkbox\" checked=\"\">\r\n                          <label for=\"checkbox2\"></label>\r\n                        </div>\r\n                      </div><img class=\"img-radius img-40 align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/university/math-1.jpg\" alt=\"\">\r\n                      <div class=\"check-dot d-inline-block\"></div>\r\n                      <div class=\"d-inline-block\"><span class=\"f-w-600\">Venter Loren</span>\r\n                        <p>1<sup>st</sup> year</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">+25 598 559 368</span>\r\n                      <p>Phone Number</p>\r\n                    </div>\r\n                  </td>\r\n                  <td><img class=\"align-top\" src=\"assets/images/university/chart-2.png\" alt=\"\"></td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">2</span>\r\n                      <p>Rank</p>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">570/600</span>\r\n                      <p>Total marks</p>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-self-center\">\r\n                      <div class=\"form-group d-inline-block\">\r\n                        <div class=\"checkbox\">\r\n                          <input id=\"checkbox3\" type=\"checkbox\">\r\n                          <label for=\"checkbox3\"></label>\r\n                        </div>\r\n                      </div><img class=\"img-radius img-40 align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/2.png\" alt=\"\">\r\n                      <div class=\"d-inline-block\"><span class=\"f-w-600\">Fran Loain</span>\r\n                        <p>1<sup>st</sup> year</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">+65 659 145 235</span>\r\n                      <p>Phone Number</p>\r\n                    </div>\r\n                  </td>\r\n                  <td><img class=\"align-top\" src=\"assets/images/university/chart-3.png\" alt=\"\"></td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">3</span>\r\n                      <p>Rank</p>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">565/600</span>\r\n                      <p>Total marks</p>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td>\r\n                    <div class=\"d-inline-block align-self-center\">\r\n                      <div class=\"form-group d-inline-block\">\r\n                        <div class=\"checkbox\">\r\n                          <input id=\"checkbox4\" type=\"checkbox\">\r\n                          <label for=\"checkbox4\"></label>\r\n                        </div>\r\n                      </div><img class=\"img-radius img-40 align-top m-r-15 rounded-circle\"\r\n                        src=\"assets/images/user/5.jpg\" alt=\"\">\r\n                      <div class=\"d-inline-block\"><span class=\"f-w-600\">Loften Horen</span>\r\n                        <p>1<sup>st</sup> year</p>\r\n                      </div>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">+37 595 367 368</span>\r\n                      <p>Phone Number</p>\r\n                    </div>\r\n                  </td>\r\n                  <td><img class=\"align-top\" src=\"assets/images/university/chart-4.png\" alt=\"\"></td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">4</span>\r\n                      <p>Rank</p>\r\n                    </div>\r\n                  </td>\r\n                  <td>\r\n                    <div class=\"d-inline-block text-center\"><span class=\"f-w-600\">540/600</span>\r\n                      <p>Total marks</p>\r\n                    </div>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <div class=\"card card-gradient\">\r\n        <div class=\"card-body text-center o-hidden\">\r\n          <div class=\"knob-header\">\r\n            <h5>Total Student</h5>\r\n            <div class=\"d-inline-block pull-right f-16\">120 / <span>130</span></div>\r\n          </div>\r\n          <div class=\"knob-block text-center knob-center university-knob\">\r\n            <div class=\"knob\" id=\"student\"></div>\r\n          </div><img class=\"round-image\" src=\"assets/images/university/round.png\" alt=\"\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50\">\r\n      <ngb-datepicker #dp class=\"custom-datepicker university-datepicker\" (navigate)=\"date = $event.next\">\r\n      </ngb-datepicker>\r\n    </div>\r\n    <div class=\"col-xl-8 xl-50\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <h5>Admission Ratio</h5>\r\n        </div>\r\n        <div class=\"card-body chart-block admission-chart\">\r\n          <canvas baseChart class=\"chart flot-chart-placeholder\" id=\"myLineCharts\" [legend]=\"admissionChartLegend\"\r\n            [data]=\"admissionChartData\" [labels]=\"admissionChartLabels\" [options]=\"admissionChartOptions\"\r\n            [chartType]=\"admissionChartType\" [colors]=\"admissionChartColors\"></canvas>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./node_modules/weakmap/weakmap.js":
/*!*****************************************!*\
  !*** ./node_modules/weakmap/weakmap.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* (The MIT License)
 *
 * Copyright (c) 2012 Brandon Benvie <http://bbenvie.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the 'Software'), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included with all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// Original WeakMap implementation by Gozala @ https://gist.github.com/1269991
// Updated and bugfixed by Raynos @ https://gist.github.com/1638059
// Expanded by Benvie @ https://github.com/Benvie/harmony-collections

void function(global, undefined_, undefined){
  var getProps = Object.getOwnPropertyNames,
      defProp  = Object.defineProperty,
      toSource = Function.prototype.toString,
      create   = Object.create,
      hasOwn   = Object.prototype.hasOwnProperty,
      funcName = /^\n?function\s?(\w*)?_?\(/;


  function define(object, key, value){
    if (typeof key === 'function') {
      value = key;
      key = nameOf(value).replace(/_$/, '');
    }
    return defProp(object, key, { configurable: true, writable: true, value: value });
  }

  function nameOf(func){
    return typeof func !== 'function'
          ? '' : 'name' in func
          ? func.name : toSource.call(func).match(funcName)[1];
  }

  // ############
  // ### Data ###
  // ############

  var Data = (function(){
    var dataDesc = { value: { writable: true, value: undefined } },
        datalock = 'return function(k){if(k===s)return l}',
        uids     = create(null),

        createUID = function(){
          var key = Math.random().toString(36).slice(2);
          return key in uids ? createUID() : uids[key] = key;
        },

        globalID = createUID(),

        storage = function(obj){
          if (hasOwn.call(obj, globalID))
            return obj[globalID];

          if (!Object.isExtensible(obj))
            throw new TypeError("Object must be extensible");

          var store = create(null);
          defProp(obj, globalID, { value: store });
          return store;
        };

    // common per-object storage area made visible by patching getOwnPropertyNames'
    define(Object, function getOwnPropertyNames(obj){
      var props = getProps(obj);
      if (hasOwn.call(obj, globalID))
        props.splice(props.indexOf(globalID), 1);
      return props;
    });

    function Data(){
      var puid = createUID(),
          secret = {};

      this.unlock = function(obj){
        var store = storage(obj);
        if (hasOwn.call(store, puid))
          return store[puid](secret);

        var data = create(null, dataDesc);
        defProp(store, puid, {
          value: new Function('s', 'l', datalock)(secret, data)
        });
        return data;
      }
    }

    define(Data.prototype, function get(o){ return this.unlock(o).value });
    define(Data.prototype, function set(o, v){ this.unlock(o).value = v });

    return Data;
  }());


  var WM = (function(data){
    var validate = function(key){
      if (key == null || typeof key !== 'object' && typeof key !== 'function')
        throw new TypeError("Invalid WeakMap key");
    }

    var wrap = function(collection, value){
      var store = data.unlock(collection);
      if (store.value)
        throw new TypeError("Object is already a WeakMap");
      store.value = value;
    }

    var unwrap = function(collection){
      var storage = data.unlock(collection).value;
      if (!storage)
        throw new TypeError("WeakMap is not generic");
      return storage;
    }

    var initialize = function(weakmap, iterable){
      if (iterable !== null && typeof iterable === 'object' && typeof iterable.forEach === 'function') {
        iterable.forEach(function(item, i){
          if (item instanceof Array && item.length === 2)
            set.call(weakmap, iterable[i][0], iterable[i][1]);
        });
      }
    }


    function WeakMap(iterable){
      if (this === global || this == null || this === WeakMap.prototype)
        return new WeakMap(iterable);

      wrap(this, new Data);
      initialize(this, iterable);
    }

    function get(key){
      validate(key);
      var value = unwrap(this).get(key);
      return value === undefined_ ? undefined : value;
    }

    function set(key, value){
      validate(key);
      // store a token for explicit undefined so that "has" works correctly
      unwrap(this).set(key, value === undefined ? undefined_ : value);
    }

    function has(key){
      validate(key);
      return unwrap(this).get(key) !== undefined;
    }

    function delete_(key){
      validate(key);
      var data = unwrap(this),
          had = data.get(key) !== undefined;
      data.set(key, undefined);
      return had;
    }

    function toString(){
      unwrap(this);
      return '[object WeakMap]';
    }

    try {
      var src = ('return '+delete_).replace('e_', '\\u0065'),
          del = new Function('unwrap', 'validate', src)(unwrap, validate);
    } catch (e) {
      var del = delete_;
    }

    var src = (''+Object).split('Object');
    var stringifier = function toString(){
      return src[0] + nameOf(this) + src[1];
    };

    define(stringifier, stringifier);

    var prep = { __proto__: [] } instanceof Array
      ? function(f){ f.__proto__ = stringifier }
      : function(f){ define(f, stringifier) };

    prep(WeakMap);

    [toString, get, set, has, del].forEach(function(method){
      define(WeakMap.prototype, method);
      prep(method);
    });

    return WeakMap;
  }(new Data));

  var defaultCreator = Object.create
    ? function(){ return Object.create(null) }
    : function(){ return {} };

  function createStorage(creator){
    var weakmap = new WM;
    creator || (creator = defaultCreator);

    function storage(object, value){
      if (value || arguments.length === 2) {
        weakmap.set(object, value);
      } else {
        value = weakmap.get(object);
        if (value === undefined) {
          value = creator(object);
          weakmap.set(object, value);
        }
      }
      return value;
    }

    return storage;
  }


  if (true) {
    module.exports = WM;
  } else {}

  WM.createStorage = createStorage;
  if (global.WeakMap)
    global.WeakMap.createStorage = createStorage;
}((0, eval)('this'));


/***/ }),

/***/ "./src/app/components/dashboard/bitcoin/bitcoin.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/dashboard/bitcoin/bitcoin.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2JpdGNvaW4vYml0Y29pbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/bitcoin/bitcoin.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/dashboard/bitcoin/bitcoin.component.ts ***!
  \*******************************************************************/
/*! exports provided: BitcoinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BitcoinComponent", function() { return BitcoinComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/data/dashboard/crypto */ "./src/app/shared/data/dashboard/crypto.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BitcoinComponent = /** @class */ (function () {
    function BitcoinComponent() {
        this.isBTC = false;
        this.isETH = false;
        this.isDASH = false;
        this.chart1 = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["chart1"];
        this.chart2 = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["chart2"];
        this.chart3 = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["chart3"];
        this.chart4 = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["chart4"];
        this.saleChartType = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartType"];
        this.saleChartLable = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartLabels"];
        this.saleChartData = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartData"];
        this.saleChartOption = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartOptions"];
        this.saleChartColor = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartColors"];
        this.saleChartLegend = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["saleChartLegend"];
        //Invest Chart data and options
        this.dailyChartLabels = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartLabels"];
        this.dailyChartData = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartData"];
        this.dailyChartColors = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartColors"];
        this.dailyChartType = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartType"];
        this.dailyChartLegend = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartLegend"];
        this.dailyChartOptions = _shared_data_dashboard_crypto__WEBPACK_IMPORTED_MODULE_1__["dailyChartOptions"];
    }
    BitcoinComponent.prototype.ngOnInit = function () { };
    BitcoinComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bitcoin',
            template: __webpack_require__(/*! raw-loader!./bitcoin.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/bitcoin/bitcoin.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./bitcoin.component.scss */ "./src/app/components/dashboard/bitcoin/bitcoin.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BitcoinComponent);
    return BitcoinComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _default_default_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./default/default.component */ "./src/app/components/dashboard/default/default.component.ts");
/* harmony import */ var _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./e-commerce/e-commerce.component */ "./src/app/components/dashboard/e-commerce/e-commerce.component.ts");
/* harmony import */ var _university_university_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./university/university.component */ "./src/app/components/dashboard/university/university.component.ts");
/* harmony import */ var _bitcoin_bitcoin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bitcoin/bitcoin.component */ "./src/app/components/dashboard/bitcoin/bitcoin.component.ts");
/* harmony import */ var _server_server_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./server/server.component */ "./src/app/components/dashboard/server/server.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./project/project.component */ "./src/app/components/dashboard/project/project.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '',
        children: [
            {
                path: 'default',
                component: _default_default_component__WEBPACK_IMPORTED_MODULE_2__["DefaultComponent"],
                data: {
                    title: "Default",
                    breadcrumb: "Default"
                }
            },
            {
                path: 'e-commerce',
                component: _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_3__["ECommerceComponent"],
                data: {
                    title: "E-commerce",
                    breadcrumb: "E-commerce"
                }
            },
            {
                path: 'university',
                component: _university_university_component__WEBPACK_IMPORTED_MODULE_4__["UniversityComponent"],
                data: {
                    title: "University",
                    breadcrumb: "University"
                }
            },
            {
                path: 'bitcoin',
                component: _bitcoin_bitcoin_component__WEBPACK_IMPORTED_MODULE_5__["BitcoinComponent"],
                data: {
                    title: "Crypto",
                    breadcrumb: "Crypto"
                }
            },
            {
                path: 'server',
                component: _server_server_component__WEBPACK_IMPORTED_MODULE_6__["ServerComponent"],
                data: {
                    title: "Server",
                    breadcrumb: "Server"
                }
            },
            {
                path: 'project',
                component: _project_project_component__WEBPACK_IMPORTED_MODULE_7__["ProjectComponent"],
                data: {
                    title: "Project",
                    breadcrumb: "Project"
                }
            }
        ]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.module.ts ***!
  \**********************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-owl-carousel-o */ "./node_modules/ngx-owl-carousel-o/fesm5/ngx-owl-carousel-o.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/fesm5/ng-chartist.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var angular_count_to__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-count-to */ "./node_modules/angular-count-to/modules/angular-count-to.es5.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var ng2_google_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-google-charts */ "./node_modules/ng2-google-charts/index.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/components/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _default_default_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./default/default.component */ "./src/app/components/dashboard/default/default.component.ts");
/* harmony import */ var _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./e-commerce/e-commerce.component */ "./src/app/components/dashboard/e-commerce/e-commerce.component.ts");
/* harmony import */ var _university_university_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./university/university.component */ "./src/app/components/dashboard/university/university.component.ts");
/* harmony import */ var _bitcoin_bitcoin_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./bitcoin/bitcoin.component */ "./src/app/components/dashboard/bitcoin/bitcoin.component.ts");
/* harmony import */ var _server_server_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./server/server.component */ "./src/app/components/dashboard/server/server.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./project/project.component */ "./src/app/components/dashboard/project/project.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _default_default_component__WEBPACK_IMPORTED_MODULE_13__["DefaultComponent"],
                _e_commerce_e_commerce_component__WEBPACK_IMPORTED_MODULE_14__["ECommerceComponent"],
                _university_university_component__WEBPACK_IMPORTED_MODULE_15__["UniversityComponent"],
                _bitcoin_bitcoin_component__WEBPACK_IMPORTED_MODULE_16__["BitcoinComponent"],
                _server_server_component__WEBPACK_IMPORTED_MODULE_17__["ServerComponent"],
                _project_project_component__WEBPACK_IMPORTED_MODULE_18__["ProjectComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ngx_owl_carousel_o__WEBPACK_IMPORTED_MODULE_3__["CarouselModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_5__["ChartistModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_6__["ChartsModule"],
                angular_count_to__WEBPACK_IMPORTED_MODULE_7__["CountToModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_12__["DashboardRoutingModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_8__["NgxChartsModule"],
                ng2_google_charts__WEBPACK_IMPORTED_MODULE_9__["Ng2GoogleChartsModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_11__["SharedModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__["Ng2SmartTableModule"]
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/components/dashboard/default/default.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/dashboard/default/default.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2RlZmF1bHQvZGVmYXVsdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/default/default.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/dashboard/default/default.component.ts ***!
  \*******************************************************************/
/*! exports provided: DefaultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultComponent", function() { return DefaultComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/data/dashboard/default */ "./src/app/shared/data/dashboard/default.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Knob = __webpack_require__(/*! knob */ "./node_modules/knob/index.js"); // browserify require
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
var DefaultComponent = /** @class */ (function () {
    function DefaultComponent() {
        // Chart Data  
        this.chart1 = _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__["chartBox1"];
        this.chart2 = _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__["chartBox2"];
        this.chart3 = _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__["chartBox3"];
        this.chart4 = _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__["chartProduction"];
        this.chart5 = _shared_data_dashboard_default__WEBPACK_IMPORTED_MODULE_1__["chartCalculation"];
    }
    DefaultComponent.prototype.ngOnInit = function () {
        var profit = Knob({
            value: 35,
            angleOffset: 90,
            className: "review",
            thickness: 0.2,
            width: 270,
            height: 270,
            fgColor: primary,
            bgColor: '#f6f7fb',
            lineCap: 'round'
        });
        document.getElementById('profit').append(profit);
    };
    DefaultComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-default',
            template: __webpack_require__(/*! raw-loader!./default.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/default/default.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./default.component.scss */ "./src/app/components/dashboard/default/default.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DefaultComponent);
    return DefaultComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/e-commerce/e-commerce.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/dashboard/e-commerce/e-commerce.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2UtY29tbWVyY2UvZS1jb21tZXJjZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/e-commerce/e-commerce.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/dashboard/e-commerce/e-commerce.component.ts ***!
  \*************************************************************************/
/*! exports provided: ECommerceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECommerceComponent", function() { return ECommerceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/data/dashboard/e-commerce */ "./src/app/shared/data/dashboard/e-commerce.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Knob = __webpack_require__(/*! knob */ "./node_modules/knob/index.js"); // browserify require
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
var ECommerceComponent = /** @class */ (function () {
    function ECommerceComponent() {
        this.slidesStore = [{
                id: 1,
                icon: 'dollar-sign',
                title: 'Total Earning',
                number: 72
            },
            {
                id: 2,
                icon: 'map-pin',
                title: 'Total Web Visitor',
                number: 65
            },
            {
                id: 3,
                icon: 'shopping-cart',
                title: 'Total Sale Product',
                number: 96
            },
            {
                id: 4,
                icon: 'trending-down',
                title: 'Company Loss',
                number: 89
            },
            {
                id: 5,
                icon: 'dollar-sign',
                title: 'Total Earning',
                number: 72
            }];
        this.customOptions = {
            margin: 10,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            loop: true,
            dots: false,
            nav: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                420: {
                    items: 2,
                    nav: false
                },
                600: {
                    items: 3,
                    nav: false
                },
                932: {
                    items: 4,
                    nav: false
                }
            }
        };
        // Charts1
        this.saleChartType = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartType"];
        this.saleChartLable = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartLabels"];
        this.saleChartData = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartData"];
        this.saleChartOption = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartOptions"];
        this.saleChartColor = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartColors"];
        this.saleChartLegend = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["saleChartLegend"];
        // Charts1
        this.chartType1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartType1"];
        this.chartLable1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartLabels1"];
        this.chartData1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartData1"];
        this.chartOption1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartOptions1"];
        this.chartColor1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartColors1"];
        this.chartLegend1 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartLegend1"];
        // Chart2
        this.chartType2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartType2"];
        this.chartLable2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartLabels2"];
        this.chartData2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartData2"];
        this.chartOption2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartOptions2"];
        this.chartColor2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartColors2"];
        this.chartLegend2 = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["lineChartLegend2"];
        //Static chart
        this.staticChartType = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartType"];
        this.staticChartLable = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartLabels"];
        this.staticChartData = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartData"];
        this.staticChartOption = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartOptions"];
        this.staticChartColor = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartColors"];
        this.staticChartLegend = _shared_data_dashboard_e_commerce__WEBPACK_IMPORTED_MODULE_1__["staticChartLegend"];
    }
    ECommerceComponent.prototype.ngOnInit = function () {
        var review = Knob({
            value: 35,
            angleOffset: 180,
            className: "review",
            thickness: 0.1,
            width: 290,
            height: 290,
            fgColor: primary
        });
        document.getElementById('review').append(review);
    };
    ECommerceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-e-commerce',
            template: __webpack_require__(/*! raw-loader!./e-commerce.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/e-commerce/e-commerce.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./e-commerce.component.scss */ "./src/app/components/dashboard/e-commerce/e-commerce.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ECommerceComponent);
    return ECommerceComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/project/project.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/dashboard/project/project.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL3Byb2plY3QvcHJvamVjdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/project/project.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/dashboard/project/project.component.ts ***!
  \*******************************************************************/
/*! exports provided: ProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectComponent", function() { return ProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/data/dashboard/project */ "./src/app/shared/data/dashboard/project.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProjectComponent = /** @class */ (function () {
    function ProjectComponent() {
        this.doughnutData = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["doughnutData"];
        this.vertical_stack_chart = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["vertical_stack_chart"];
        // doughnut
        this.view = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["view"];
        this.doughnutChartColorScheme = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["doughnutChartcolorScheme"];
        this.doughnutChartShowLabels = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["doughnutChartShowLabels"];
        this.doughnutChartGradient = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["doughnutChartGradient"];
        //vertical_stack_chart
        this.verticalStackChartColorScheme = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["colorScheme"];
        this.verticalStackChartshowXAxis = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["showXAxis"];
        this.verticalStackChartshowYAxis = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["showYAxis"];
        this.verticalStackChartgradient = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["gradient"];
        this.verticalStackChartshowLegend = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["showLegend"];
        this.verticalStackChartshowXAxisLabel = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["showXAxisLabel"];
        this.verticalStackChartshowYAxisLabel = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["showYAxisLabel"];
        this.chart1 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart1"];
        this.chart2 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart2"];
        this.chart3 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart3"];
        this.chart4 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart4"];
        this.chart5 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart5"];
        this.chart6 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["chart6"];
        this.pieChart1 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["pieChart1"];
        this.barChartSingle1 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["barChartSingle1"];
        this.barChartSingle2 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["barChartSingle2"];
        this.barChartSingle3 = _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["barChartSingle3"];
        Object.assign(this, { doughnutData: _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["doughnutData"], vertical_stack_chart: _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["vertical_stack_chart"], multiData: _shared_data_dashboard_project__WEBPACK_IMPORTED_MODULE_1__["multiData"], });
    }
    ProjectComponent.prototype.ngOnInit = function () { };
    ProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project',
            template: __webpack_require__(/*! raw-loader!./project.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/project/project.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./project.component.scss */ "./src/app/components/dashboard/project/project.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProjectComponent);
    return ProjectComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/server/server.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/dashboard/server/server.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL3NlcnZlci9zZXJ2ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/dashboard/server/server.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/dashboard/server/server.component.ts ***!
  \*****************************************************************/
/*! exports provided: ServerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerComponent", function() { return ServerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/data/dashboard/server */ "./src/app/shared/data/dashboard/server.ts");
/* harmony import */ var _shared_data_tables_server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/data/tables/server */ "./src/app/shared/data/tables/server.ts");
/* harmony import */ var chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! chartjs-plugin-streaming */ "./node_modules/chartjs-plugin-streaming/dist/chartjs-plugin-streaming.js");
/* harmony import */ var chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerComponent = /** @class */ (function () {
    function ServerComponent() {
        this.explorer = [];
        this.settings = {
            columns: {
                name: {
                    title: 'Name'
                },
                user: {
                    title: 'User Name',
                    type: 'html',
                },
                IO: {
                    title: 'IO'
                },
                cpu: {
                    title: 'CPU'
                },
                mem: {
                    title: 'MEM'
                }
            }
        };
        this.latencyChartType = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartType"];
        this.latencyChartLabels = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["latencyChartLabels"];
        this.latencyChartData = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["latencyChartData"];
        this.latencyChartOptions = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["latencyChartOptions"];
        this.latencyChartColors = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["latencyChartColors"];
        this.latencyChartLegend = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["latencyChartLegend"];
        this.memoryChartType = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartType"];
        this.memoryChartLabels = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartLabels"];
        this.memoryChartData = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartData"];
        this.memoryChartOptions = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartOptions"];
        this.memoryChartColors = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartColors"];
        this.memoryChartLegend = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["memoryChartLegend"];
        this.cpuChartType = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartType"];
        this.cpuChartLabels = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartLabels"];
        this.cpuChartData = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartData"];
        this.cpuChartOptions = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartOptions"];
        this.cpuChartColors = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartColors"];
        this.cpuChartLegend = _shared_data_dashboard_server__WEBPACK_IMPORTED_MODULE_1__["cpuChartLegend"];
        this.explorer = _shared_data_tables_server__WEBPACK_IMPORTED_MODULE_2__["serverDB"].explorer;
    }
    ServerComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', 'assets/data/tables/explorer.json');
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    ServerComponent.prototype.ngAfterViewChecked = function () { };
    ServerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-server',
            template: __webpack_require__(/*! raw-loader!./server.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/server/server.component.html"),
            styles: [__webpack_require__(/*! ./server.component.scss */ "./src/app/components/dashboard/server/server.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ServerComponent);
    return ServerComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboard/university/university.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/dashboard/university/university.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.review canvas {\n  position: relative !important; }\n\n:focus {\n  outline-color: transparent !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvdW5pdmVyc2l0eS9DOlxceGFtcHBcXGh0ZG9jc1xcYWRtaW4yLjAvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGRhc2hib2FyZFxcdW5pdmVyc2l0eVxcdW5pdmVyc2l0eS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDZCQUE2QixFQUFBOztBQUcvQjtFQUNFLHFDQUFxQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvdW5pdmVyc2l0eS91bml2ZXJzaXR5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2LnJldmlldyBjYW52YXMge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Zm9jdXMge1xyXG4gIG91dGxpbmUtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/dashboard/university/university.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/dashboard/university/university.component.ts ***!
  \*************************************************************************/
/*! exports provided: UniversityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UniversityComponent", function() { return UniversityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/data/dashboard/university */ "./src/app/shared/data/dashboard/university.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Knob = __webpack_require__(/*! knob */ "./node_modules/knob/index.js"); // browserify require
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
var UniversityComponent = /** @class */ (function () {
    function UniversityComponent() {
        this.chart1 = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["chart1"];
        this.chart2 = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["chart2"];
        this.chart3 = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["chart3"];
        this.chart4 = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["chart4"];
        this.chart5 = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["chart5"];
        this.admissionChartType = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartType"];
        this.admissionChartLabels = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartLabels"];
        this.admissionChartData = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartData"];
        this.admissionChartOptions = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartOptions"];
        this.admissionChartColors = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartColors"];
        this.admissionChartLegend = _shared_data_dashboard_university__WEBPACK_IMPORTED_MODULE_1__["admissionChartLegend"];
    }
    UniversityComponent.prototype.ngOnInit = function () {
        var ranker = Knob({
            value: 25,
            angleOffset: -125,
            angleArc: 250,
            className: "review",
            lineCap: "round",
            thickness: 0.2,
            width: 295,
            height: 295,
            fgColor: primary
        });
        document.getElementById('ranker').append(ranker);
        var student = Knob({
            value: 85,
            angleOffset: 80,
            angleArc: 360,
            className: "review",
            lineCap: "round",
            thickness: 0.1,
            width: 180,
            height: 180,
            fgColor: '#fff',
            bgColor: primary
        });
        document.getElementById('student').append(student);
    };
    UniversityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-university',
            template: __webpack_require__(/*! raw-loader!./university.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/dashboard/university/university.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./university.component.scss */ "./src/app/components/dashboard/university/university.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UniversityComponent);
    return UniversityComponent;
}());



/***/ }),

/***/ "./src/app/shared/data/dashboard/crypto.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/data/dashboard/crypto.ts ***!
  \*************************************************/
/*! exports provided: chart1, chart2, chart3, chart4, saleChartType, saleChartLabels, saleChartData, saleChartOptions, saleChartColors, saleChartLegend, dailyChartLabels, dailyChartData, dailyChartColors, dailyChartType, dailyChartLegend, dailyChartOptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart1", function() { return chart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart2", function() { return chart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart3", function() { return chart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart4", function() { return chart4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartType", function() { return saleChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartLabels", function() { return saleChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartData", function() { return saleChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartOptions", function() { return saleChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartColors", function() { return saleChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartLegend", function() { return saleChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartLabels", function() { return dailyChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartData", function() { return dailyChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartColors", function() { return dailyChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartType", function() { return dailyChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartLegend", function() { return dailyChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyChartOptions", function() { return dailyChartOptions; });
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_0__);

var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
// Chart 1
var chart1 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06'],
        series: [
            [8, 3, 7.5, 4, 7, 4]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 3
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            top: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 2
var chart2 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06'],
        series: [
            [8, 3, 7.5, 4, 7, 4]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 3
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            top: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 3
var chart3 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06'],
        series: [
            [8, 3, 7.5, 4, 7, 4]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 3
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            top: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 4
var chart4 = {
    type: 'Bar',
    data: {
        labels: ['100', '200', '300', '400', '500', '600', '700', '800'],
        series: [
            [2.5, 3, 3, 0.9, 1.3, 1.8, 3.8, 1.5],
            [3.8, 1.8, 4.3, 2.3, 3.6, 2.8, 2.8, 2.8]
        ]
    },
    options: {
        seriesBarDistance: 12,
        chartPadding: {
            left: 0,
            right: 0,
            bottom: 0,
        },
        axisX: {
            showGrid: false,
            labelInterpolationFnc: function (value) {
                return value[0];
            }
        }
    },
    events: {
        created: function (data) {
        }
    }
};
//Sale chart
var saleChartType = 'line';
var saleChartLabels = ["2010", "2011", "2012", "2013", "2014", "2015", "2016"];
var saleChartData = [
    { data: [1, 2.5, 1.5, 3, 1.3, 2, 4, 4.5] },
    { data: [0, 1, 0.5, 1, 0.3, 1.6, 1.4, 2] }
];
var saleChartOptions = {
    responsive: true,
    animation: false,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                }
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    precision: 0
                }
            }]
    }
};
var saleChartColors = [
    {
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary,
        lineTension: 0
    },
    {
        fill: false,
        borderColor: secondary,
        borderWidth: 2.5,
        pointBackgroundColor: secondary,
        pointBorderColor: secondary,
        lineTension: 0
    },
];
var saleChartLegend = false;
//Invest Chart data and options
var dailyChartLabels = ['Bitcoin', 'Ripple', 'Invest'];
var dailyChartData = [40, 8, 10];
var dailyChartColors = [{
        backgroundColor: [primary, "#f6f6f6", secondary],
        borderAlign: 'center',
        borderColor: primary,
        weight: 1,
        borderWidth: 2
    }];
var dailyChartType = 'doughnut';
var dailyChartLegend = true;
var dailyChartOptions = {
    animation: {
        easing: 'easeOutBounce',
    },
    cutoutPercentage: 70,
    tooltips: {
        mode: 'index',
        intersect: true,
    },
    responsive: true,
    height: 500,
    maintainAspectRatio: false,
    legend: {
        display: false,
        fullWidth: true,
        onClick: true,
        position: 'center'
    }
};


/***/ }),

/***/ "./src/app/shared/data/dashboard/default.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/data/dashboard/default.ts ***!
  \**************************************************/
/*! exports provided: chartBox1, chartBox2, chartBox3, chartCalculation, chartProduction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartBox1", function() { return chartBox1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartBox2", function() { return chartBox2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartBox3", function() { return chartBox3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartCalculation", function() { return chartCalculation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chartProduction", function() { return chartProduction; });
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_0__);

var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
// Chart 1
var chartBox1 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06', '07'],
        series: [
            [0, 2, 1.2, 4, 2, 3, 1.5, 0]
        ],
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 2
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient2',
                x1: 1,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
// Chart 2
var chartBox2 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06'],
        series: [
            [0, 2, 1.2, 4, 2, 3, 0]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 2
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient3',
                x1: 1,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
// Chart 3
var chartBox3 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06', '07'],
        series: [
            [0, 2, 1.2, 4, 2, 3, 1.5, 2, 0]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 2
        }),
        fullWidth: !0,
        showArea: !0,
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient4',
                x1: 1,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
// Chart 4
var chartCalculation = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06', '07', '08'],
        series: [
            [0, 2, 1.2, 4, 2, 3, 1.5, 0],
            [0, 1, 2.2, 1.5, 3, 1.5, 2.25, 0]
        ]
    },
    options: {
        low: 0,
        showArea: true,
        fullWidth: true,
        onlyInteger: true,
        chartPadding: {
            left: 0,
            right: 0,
        },
        axisY: {
            low: 0,
            scaleMinSpace: 50,
        },
        axisX: {
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient1',
                x1: 0,
                y1: 0,
                x2: 1,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
// Chart 5
var chartProduction = {
    type: 'Line',
    data: {
        labels: ['2009', '2010', '2011', '2012'],
        series: [
            [0, 6, 2, 6],
            [0, 7, 1, 8]
        ]
    },
    options: {
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    },
    events: {
        created: function (data) {
        }
    }
};


/***/ }),

/***/ "./src/app/shared/data/dashboard/e-commerce.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/data/dashboard/e-commerce.ts ***!
  \*****************************************************/
/*! exports provided: saleChartType, saleChartLabels, saleChartData, saleChartOptions, saleChartColors, saleChartLegend, lineChartType1, lineChartLabels1, lineChartData1, lineChartOptions1, lineChartColors1, lineChartLegend1, lineChartType2, lineChartLabels2, lineChartData2, lineChartOptions2, lineChartColors2, lineChartLegend2, staticChartType, staticChartLabels, staticChartData, staticChartOptions, staticChartColors, staticChartLegend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartType", function() { return saleChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartLabels", function() { return saleChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartData", function() { return saleChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartOptions", function() { return saleChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartColors", function() { return saleChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saleChartLegend", function() { return saleChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartType1", function() { return lineChartType1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLabels1", function() { return lineChartLabels1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartData1", function() { return lineChartData1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartOptions1", function() { return lineChartOptions1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartColors1", function() { return lineChartColors1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLegend1", function() { return lineChartLegend1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartType2", function() { return lineChartType2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLabels2", function() { return lineChartLabels2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartData2", function() { return lineChartData2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartOptions2", function() { return lineChartOptions2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartColors2", function() { return lineChartColors2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lineChartLegend2", function() { return lineChartLegend2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartType", function() { return staticChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartLabels", function() { return staticChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartData", function() { return staticChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartOptions", function() { return staticChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartColors", function() { return staticChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticChartLegend", function() { return staticChartLegend; });
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
//Sale chart
var saleChartType = 'line';
var saleChartLabels = ["2009", "2010", "2011", "2012", "2013", "2014", "2015"];
var saleChartData = [0, 2.25, 1.25, 3, 1.25, 2.25, 0];
var saleChartOptions = {
    responsive: true,
    animation: false,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                }
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true
                }
            }]
    }
};
var saleChartColors = [{
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary
    }];
var saleChartLegend = false;
//Line chart
var lineChartType1 = 'line';
var lineChartLabels1 = ["", "2009", "2010", "2011", "2012", "2013", "2014"];
var lineChartData1 = [20, 33, 20, 50, 20, 33, 20, 0];
var lineChartOptions1 = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                }
            }],
        yAxes: [{
                display: true,
            }]
    }
};
var lineChartColors1 = [{
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary,
        lineTension: 0,
    }];
var lineChartLegend1 = false;
//Line chart 2
var lineChartType2 = 'line';
var lineChartLabels2 = ["", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016"];
var lineChartData2 = [5, 0, 5, 0, 15, 0, 5, 0, 5];
var lineChartOptions2 = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                }
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    fixedStepSize: 5,
                    precision: 0
                }
            }]
    }
};
var lineChartColors2 = [{
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBordereWidth: 5,
        pointBorderColor: primary,
        lineTension: 0,
    }];
var lineChartLegend2 = false;
var staticChartType = 'line';
var staticChartLabels = ["0", "50", "100", "150", "200", "250", "300", "350"];
var staticChartData = [1.000000000, 0.642787610, -0.173648178, -0.866025404, -0.939692621, -0.342020143, 0.500000000, 0.984807753];
var staticChartOptions = {
    responsive: true,
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    animation: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                },
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true
                },
            }]
    }
};
var staticChartColors = [{
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary
    }];
var staticChartLegend = false;


/***/ }),

/***/ "./src/app/shared/data/dashboard/project.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/data/dashboard/project.ts ***!
  \**************************************************/
/*! exports provided: doughnutData, view, doughnutChartShowLabels, doughnutChartGradient, doughnutChartcolorScheme, multiData, vertical_stack_chart, showXAxis, showYAxis, gradient, showLegend, showXAxisLabel, showYAxisLabel, colorScheme, chart1, chart2, chart3, chart4, chart5, chart6, pieChart1, barChartSingle1, barChartSingle2, barChartSingle3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutData", function() { return doughnutData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view", function() { return view; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartShowLabels", function() { return doughnutChartShowLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartGradient", function() { return doughnutChartGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doughnutChartcolorScheme", function() { return doughnutChartcolorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "multiData", function() { return multiData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vertical_stack_chart", function() { return vertical_stack_chart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showXAxis", function() { return showXAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showYAxis", function() { return showYAxis; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "gradient", function() { return gradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showLegend", function() { return showLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showXAxisLabel", function() { return showXAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showYAxisLabel", function() { return showYAxisLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "colorScheme", function() { return colorScheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart1", function() { return chart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart2", function() { return chart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart3", function() { return chart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart4", function() { return chart4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart5", function() { return chart5; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart6", function() { return chart6; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pieChart1", function() { return pieChart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartSingle1", function() { return barChartSingle1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartSingle2", function() { return barChartSingle2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "barChartSingle3", function() { return barChartSingle3; });
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_0__);

var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
var doughnutData = [
    {
        value: 300,
        name: "Frontend"
    },
    {
        value: 50,
        name: "Backend"
    },
    {
        value: 30,
        name: "Api"
    },
    {
        value: 100,
        name: "Issues"
    }
];
//doughnut-Chart
var view = [285, 285];
//Options
var doughnutChartShowLabels = false;
var doughnutChartGradient = true;
var doughnutChartcolorScheme = {
    domain: [primary, secondary, secondary, secondary]
};
var multiData = [
    {
        "name": "Mon",
        "series": [
            {
                "name": "y",
                "value": 3
            },
            {
                "name": "z",
                "value": 2
            }
        ]
    },
    {
        "name": "Tue",
        "series": [
            {
                "name": "y",
                "value": 3
            },
            {
                "name": "z",
                "value": 0
            }
        ]
    },
    {
        "name": "Wen",
        "series": [
            {
                "name": "y",
                "value": 0
            },
            {
                "name": "z",
                "value": 1.5
            }
        ]
    },
    {
        "name": "Thu",
        "series": [
            {
                "name": "y",
                "value": 2
            },
            {
                "name": "z",
                "value": 0
            }
        ]
    },
    {
        "name": "Fri",
        "series": [
            {
                "name": "y",
                "value": 0
            },
            {
                "name": "z",
                "value": 3.5
            }
        ]
    },
    {
        "name": "Sat",
        "series": [
            {
                "name": "y",
                "value": 3
            },
            {
                "name": "z",
                "value": 2
            }
        ]
    },
    {
        "name": "sun",
        "series": [
            {
                "name": "y",
                "value": 0
            },
            {
                "name": "z",
                "value": 2
            }
        ]
    }
];
//vertical-stack chart
var vertical_stack_chart = [
    {
        x: "Mon",
        y: 3,
        z: 2
    },
    {
        x: "Tue",
        y: 3,
        z: null
    },
    {
        x: "Wed",
        y: 0,
        z: 1.5
    },
    {
        x: "Thu",
        y: 2,
        z: null
    },
    {
        x: "Fri",
        y: 0,
        z: 3.5
    },
    {
        x: "Sat",
        y: 3,
        z: 2
    },
    {
        x: "Sun",
        y: 0,
        z: 2
    }
];
//Vertical stacked chart option
var showXAxis = true;
var showYAxis = true;
var gradient = false;
var showLegend = false;
var showXAxisLabel = true;
var showYAxisLabel = true;
var colorScheme = {
    domain: [primary, secondary, primary, secondary]
};
var chart1 = {
    type: 'Line',
    data: {
        labels: ['01', '02', '03', '04', '05', '06'],
        series: [
            [1, 5, 2, 5, 4, 3]
        ]
    },
    options: {
        lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_0__["Interpolation"].simple({
            divisor: 2
        }),
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient5',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var chart2 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [[5, 2, 3, 1, 3, 2]]
    },
    options: {
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisX: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        },
        axisY: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient6',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var chart3 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [[1, 2, 5, 1, 4, 3]]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisX: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        },
        axisY: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient7',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var chart4 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [[1, 2, 4, 3, 2, 3]]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisX: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        },
        axisY: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient8',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var chart5 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [[0, 5, 2, 3, 1, 3]]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisX: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        },
        axisY: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient9',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var chart6 = {
    type: 'Line',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        series: [[1, 2, 3, 1, 2, 3]]
    },
    options: {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        chartpadding: {
            bottom: 0,
            left: 0,
            right: 0
        },
        axisX: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        },
        axisY: {
            low: 0,
            offset: -5,
            showLabel: false,
            showGrid: false
        }
    },
    events: {
        created: function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient10',
                x1: 1,
                y1: 0,
                x2: 0,
                y2: 1
            }).elem('stop', {
                offset: 0,
                'stop-color': primary
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': secondary
            });
        }
    }
};
var pieChart1 = {
    chartType: 'PieChart',
    dataTable: [
        ['Week', 'Day in week'],
        ['Mon', 15],
        ['Tue', 10],
        ['Wed', 15],
        ['Thu', 20],
        ['Fri', 25],
        ['sat', 20],
        ['Sun', 10]
    ],
    options: {
        title: 'My Daily Activities',
        height: 400,
        colors: [primary, secondary, "#22af47", "#007bff", "#FF5370", "#22af47", "#ff9f40"],
        labels: false,
        backgroundColor: 'transparent'
    },
};
var barChartSingle1 = {
    type: 'Bar',
    data: {
        series: [
            [5, 7, 3, 5, 2, 3, 9, 6, 5, 9],
            [5, 3, 5, 2, 5, 3, 3, 9, 6, 5],
            [9, 2, 9, 6, 5, 9, 7, 3, 5, 2]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            offset: 0
        },
        responsive: true,
        height: 70,
        width: 450,
        fill: [primary, secondary, "#22af47"],
    }
};
var barChartSingle2 = {
    type: 'Bar',
    data: {
        series: [
            [5, 7, 3, 5, 2, 3, 9, 6, 5, 9],
            [5, 3, 5, 2, 5, 3, 3, 9, 6, 5],
            [9, 7, 9, 6, 5, 9, 7, 3, 5, 2]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            offset: 0
        },
        responsive: true,
        height: 70,
        width: 450,
        fill: [primary, secondary, "#22af47"]
    }
};
var barChartSingle3 = {
    type: 'Bar',
    data: {
        series: [
            [9, 7, 3, 5, 2, 5, 3, 5, 3, 9],
            [6, 5, 9, 3, 5, 2, 5, 3, 6, 5],
            [9, 7, 9, 2, 5, 3, 7, 9, 5, 6]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            offset: 0
        },
        responsive: true,
        height: 70,
        width: 450,
        fill: [primary, secondary, "#22af47"]
    }
};


/***/ }),

/***/ "./src/app/shared/data/dashboard/server.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/data/dashboard/server.ts ***!
  \*************************************************/
/*! exports provided: memoryChartType, memoryChartLabels, memoryChartData, memoryChartOptions, memoryChartColors, memoryChartLegend, latencyChartType, latencyChartLabels, latencyChartData, latencyChartOptions, latencyChartColors, latencyChartLegend, cpuChartType, cpuChartLabels, cpuChartData, cpuChartOptions, cpuChartColors, cpuChartLegend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartType", function() { return memoryChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartLabels", function() { return memoryChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartData", function() { return memoryChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartOptions", function() { return memoryChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartColors", function() { return memoryChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "memoryChartLegend", function() { return memoryChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartType", function() { return latencyChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartLabels", function() { return latencyChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartData", function() { return latencyChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartOptions", function() { return latencyChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartColors", function() { return latencyChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latencyChartLegend", function() { return latencyChartLegend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartType", function() { return cpuChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartLabels", function() { return cpuChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartData", function() { return cpuChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartOptions", function() { return cpuChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartColors", function() { return cpuChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cpuChartLegend", function() { return cpuChartLegend; });
/* harmony import */ var chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chartjs-plugin-streaming */ "./node_modules/chartjs-plugin-streaming/dist/chartjs-plugin-streaming.js");
/* harmony import */ var chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chartjs_plugin_streaming__WEBPACK_IMPORTED_MODULE_0__);

var moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
moment.suppressDeprecationWarnings = true;
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
//Sale chart
var memoryChartType = 'line';
var memoryChartLabels = ["1 min.", "10 min.", "20 min.", "30 min.", "40 min.", "50 min.", "60 min.", "70 min.", "80 min.", "90 min.", "100 min"];
var memoryChartData = [
    { data: [0, 59, 80, 40, 100, 60, 95, 20, 70, 40, 70] },
    { data: [0, 48, 30, 19, 86, 27, 90, 60, 30, 70, 40] },
    { data: [0, 30, 40, 10, 60, 40, 70, 30, 20, 80, 50] }
];
// export var memoryChartData: Array<any> = [{ data: getRandomData() }];
var memoryChartOptions = {
    responsive: true,
    animation: false,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    precision: 0
                }
            }]
    }
};
var memoryChartColors = [{
        fill: false,
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary
    }, {
        fill: false,
        borderColor: secondary,
        borderWidth: 2.5,
        pointBackgroundColor: secondary,
        pointBorderColor: secondary
    },
    {
        fill: false,
        borderColor: "#22af47",
        borderWidth: 2.5,
        pointBackgroundColor: '#22af47',
        pointBorderColor: "#22af47"
    }];
var memoryChartLegend = false;
// Line chart
var latencyChartType = 'line';
var latencyChartLabels = ["", "2009", "2010", "2011", "2012", "2013", "2014"];
var latencyChartData = [{
        data: []
    }, {
        data: []
    }];
var latencyChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
        point: {
            radius: 0
        }
    },
    scales: {
        xAxes: [{
                display: false,
                type: 'realtime',
                realtime: {
                    onRefresh: function (chart) {
                        chart.data.datasets.forEach(function (dataset) {
                            dataset.data.push({
                                x: Date.now(),
                                y: (Math.random() > 0.1 ? 1.0 : 1.0) * Math.round(Math.random() * 100)
                            });
                        });
                    },
                    delay: 1000,
                    duration: 50000,
                    refresh: 1000,
                },
            }],
        yAxes: [{
                display: false,
                ticks: {
                    beginAtZero: true,
                    fixedStepSize: 10,
                    precision: 0
                }
            }]
    }
};
var latencyChartColors = [{
        fill: true,
        backgroundColor: "rgb(183, 196, 246)",
        borderColor: "#4466f2",
        borderWidth: 1.5,
        pointBackgroundColor: '#4466f2',
        pointBorderColor: "#4466f2",
        pointBorderWidth: 0,
        lineTension: 0,
    }];
var latencyChartLegend = false;
// Line chart
var cpuChartType = 'line';
var cpuChartLabels = ["", "2009", "2010", "2011", "2012", "2013", "2014"];
var cpuChartData = [{
        data: []
    }, {
        data: []
    }];
var cpuChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
        point: {
            radius: 0
        }
    },
    scales: {
        xAxes: [{
                display: false,
                type: 'realtime',
                realtime: {
                    onRefresh: function (chart) {
                        chart.data.datasets.forEach(function (dataset) {
                            dataset.data.push({
                                x: Date.now(),
                                y: (Math.random() > 0.1 ? 1.0 : 1.0) * Math.round(Math.random() * 100)
                            });
                        });
                    },
                    delay: 1000,
                    duration: 50000,
                    refresh: 1000,
                },
            }],
        yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    fixedStepSize: 10,
                    precision: 0
                }
            }]
    }
};
var cpuChartColors = [{
        fill: true,
        backgroundColor: "rgb(183, 196, 246)",
        borderColor: primary,
        borderWidth: 1.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary,
        pointBorderWidth: 0,
        lineTension: 0,
    }];
var cpuChartLegend = false;


/***/ }),

/***/ "./src/app/shared/data/dashboard/university.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/data/dashboard/university.ts ***!
  \*****************************************************/
/*! exports provided: chart1, chart2, chart3, chart4, chart5, admissionChartType, admissionChartLabels, admissionChartData, admissionChartOptions, admissionChartColors, admissionChartLegend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart1", function() { return chart1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart2", function() { return chart2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart3", function() { return chart3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart4", function() { return chart4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chart5", function() { return chart5; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartType", function() { return admissionChartType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartLabels", function() { return admissionChartLabels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartData", function() { return admissionChartData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartOptions", function() { return admissionChartOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartColors", function() { return admissionChartColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admissionChartLegend", function() { return admissionChartLegend; });
var primary = localStorage.getItem('primary_color') || '#4466f2';
var secondary = localStorage.getItem('secondary_color') || '#1ea6ec';
// Chart 1
var chart1 = {
    type: 'Line',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
        series: [
            [2, 3, 2.5, 5, 1.5, 4.5, 3, 3.1],
            [3, 3.5, 6, 1.1, 5, 2.5, 3.2, 2]
        ]
    },
    options: {
        low: 0,
        showPoint: false,
        chartPadding: {
            left: 0,
            right: 0,
        },
        axisY: {
            scaleMinSpace: 40
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 2
var chart2 = {
    type: 'Line',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
        series: [
            [1.5, 3, 2, 1, 4, 1, 4, 2, 3, 2.5],
            [5, 4.7, 4, 3, 3.3, 3.7, 3, 3.8, 3, 2]
        ]
    },
    options: {
        low: 0,
        showPoint: false,
        chartPadding: {
            left: 0,
            right: 0,
        },
        axisY: {
            scaleMinSpace: 40
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 3
var chart3 = {
    type: 'Bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
        series: [
            [1, 2, 1.5, 3, 1.5, 0.8, 1.5, 2],
            [6, 4, 5, 6.5, 3, 2, 5.5, 7]
        ]
    },
    options: {
        seriesBarDistance: 12,
        chartPadding: {
            left: 0,
            right: 0,
        },
        axisX: {
            showGrid: false,
            labelInterpolationFnc: function (value) {
                return value[0];
            }
        }
    },
    events: {
        created: function (data) {
        }
    }
};
// Chart 4
var chart4 = {
    type: 'Bar',
    data: {
        labels: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6'],
        series: [
            [50, 200, 150, 400, 300, 600, 700]
        ]
    },
    options: {
        stackBars: true,
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        draw: function (data) {
            if (data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 2px'
                });
            }
        }
    }
};
// Chart 5
var chart5 = {
    type: 'Bar',
    data: {
        labels: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6'],
        series: [
            [50, 200, 150, 400, 300, 600, 700]
        ]
    },
    options: {
        stackBars: true,
        axisY: {
            low: 0,
            showGrid: false,
            showLabel: false,
            offset: 0
        },
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0
        }
    },
    events: {
        draw: function (data) {
            if (data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 2px'
                });
            }
        }
    }
};
//Admission chart
var admissionChartType = 'line';
var admissionChartLabels = ["", "1000", "2000", "3000", "4000", "5000", "6000"];
var admissionChartData = [20, 25, 22, 25, 35, 30, 38, 35, 20];
var admissionChartOptions = {
    responsive: true,
    animation: false,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{
                display: true,
                gridLines: {
                    color: "#fff",
                    drawTicks: true,
                }
            }],
        yAxes: [{
                display: true,
            }]
    }
};
var admissionChartColors = [{
        fill: true,
        backgroundColor: "rgba(68, 102, 242,0.1)",
        borderColor: primary,
        borderWidth: 2.5,
        pointBackgroundColor: primary,
        pointBorderColor: primary,
        lineTension: 0,
    }];
var admissionChartLegend = false;


/***/ }),

/***/ "./src/app/shared/data/tables/server.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/data/tables/server.ts ***!
  \**********************************************/
/*! exports provided: serverDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "serverDB", function() { return serverDB; });
var serverDB = /** @class */ (function () {
    function serverDB() {
    }
    serverDB.explorer = [
        {
            name: "Tiger",
            user: "<span class='badge badge-primary'>Tig</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "50 MB"
        },
        {
            name: "Garrett",
            user: "<span class='badge badge-primary'>Great</span>",
            IO: "0.54 KB/s",
            cpu: "0.20 %",
            mem: "1 MB"
        },
        {
            name: "Ashton",
            user: "<span class='badge badge-primary'>Root</span>",
            IO: "20 KB/s",
            cpu: "0.50 %",
            mem: "13 MB"
        },
        {
            name: "Cedric",
            user: "<span class='badge badge-primary'>Ced</span>",
            IO: "50 KB/s",
            cpu: "0.40 %",
            mem: "25 MB"
        },
        {
            name: "Airi",
            user: "<span class='badge badge-secondary'>Airi</span>",
            IO: "0.40 KB/s",
            cpu: "0 %",
            mem: "13 MB"
        },
        {
            name: "Brielle",
            user: "<span class='badge badge-secondary'>Brie</span>",
            IO: "25 KB/s",
            cpu: "0.60 %",
            mem: "25 MB"
        },
        {
            name: "Herrod",
            user: "<span class='badge badge-primary'>Herr</span>",
            IO: "57 KB/s",
            cpu: "0 %",
            mem: "1 MB"
        },
        {
            name: "Rhona",
            user: "<span class='badge badge-primary'>Rho</span>",
            IO: "24 KB/s",
            cpu: "0 %",
            mem: "1 MB"
        },
        {
            name: "Colleen",
            user: "<span class='badge badge-secondary'>Coll</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "14 MB"
        },
        {
            name: "Sonya",
            user: "<span class='badge badge-primary'>Sony</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "13 MB"
        },
        {
            name: "Jena",
            user: "<span class='badge badge-secondary'>Jen</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "1 MB"
        },
        {
            name: "Quinn",
            user: "<span class='badge badge-primary'>Quin</span>",
            IO: "75 KB/s",
            cpu: "0.50 %",
            mem: "42 MB"
        },
        {
            name: "Charde",
            user: "<span class='badge badge-secondary'>Char</span>",
            IO: "30 KB/s",
            cpu: "0.20 %",
            mem: "10 MB"
        },
        {
            name: "Haley",
            user: "<span class='badge badge-secondary'>Hal</span>",
            IO: "56 KB/s",
            cpu: "0.10 %",
            mem: "20 MB"
        },
        {
            name: "Tatyana",
            user: "<span class='badge badge-primary'>Taty</span>",
            IO: "20 KB/s",
            cpu: "0.80 %",
            mem: "77 MB"
        },
        {
            name: "Michael",
            user: "<span class='badge badge-secondary'>Micha</span>",
            IO: "0 KB/s",
            cpu: "0.70 %",
            mem: "30 MB"
        },
        {
            name: "Paul",
            user: "<span class='badge badge-primary'>Paul</span>",
            IO: "0 KB/s",
            cpu: "0.50 %",
            mem: "11 MB"
        },
        {
            name: "Gloria",
            user: "<span class='badge badge-primary'>Glori</span>",
            IO: "65 KB/s",
            cpu: "0.20 %",
            mem: "1 MB"
        },
        {
            name: "Bradley",
            user: "<span class='badge badge-secondary'>Bradl</span>",
            IO: "40 KB/s",
            cpu: "0 %",
            mem: "15 MB"
        },
        {
            name: "Dai",
            user: "<span class='badge badge-primary'>Da</span>",
            IO: "10 KB/s",
            cpu: "0 %",
            mem: "15 MB"
        },
        {
            name: "Jenette",
            user: "<span class='badge badge-primary'>Jene</span>",
            IO: "37 KB/s",
            cpu: "0.30 %",
            mem: "20 MB"
        },
        {
            name: "Yuri",
            user: "<span class='badge badge-primary'>Yur</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "25 MB"
        },
        {
            name: "Caesar",
            user: "<span class='badge badge-secondary'>Caes</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "20 MB"
        },
        {
            name: "Doris",
            user: "<span class='badge badge-secondary'>Dori</span>",
            IO: "38 KB/s",
            cpu: "0.10 %",
            mem: "14 MB"
        },
        {
            name: "Angelica",
            user: "<span class='badge badge-primary'>Angel</span>",
            IO: "40 KB/s",
            cpu: "0.50 %",
            mem: "1 MB"
        },
        {
            name: "Gavin",
            user: "<span class='badge badge-primary'>Gavi</span>",
            IO: "0 KB/s",
            cpu: "0.20 %",
            mem: "1 MB"
        },
        {
            name: "Jennifer",
            user: "<span class='badge badge-primary'>Jeni</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "20 MB"
        },
        {
            name: "Brenden",
            user: "<span class='badge badge-secondary'>Bren</span>",
            IO: "16 KB/s",
            cpu: "0 %",
            mem: "23 MB"
        },
        {
            name: "Fiona",
            user: "<span class='badge badge-primary'>Fio</span>",
            IO: "50 KB/s",
            cpu: "0.30 %",
            mem: "75 MB"
        },
        {
            name: "Shou",
            user: "<span class='badge badge-primary'>Sho</span>",
            IO: "37 KB/s",
            cpu: "0.20 %",
            mem: "22 MB"
        },
        {
            name: "Michelle",
            user: "<span class='badge badge-primary'>Mich</span>",
            IO: "0 KB/s",
            cpu: "0.50 %",
            mem: "16 MB"
        },
        {
            name: "Suki",
            user: "<span class='badge badge-secondary'>Suk</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "42 MB"
        },
        {
            name: "Prescott",
            user: "<span class='badge badge-primary'>Pres</span>",
            IO: "56 KB/s",
            cpu: "0.27 %",
            mem: "1 MB"
        },
        {
            name: "Gavin",
            user: "<span class='badge badge-primary'>Gavi</span>",
            IO: "0 KB/s",
            cpu: "0.22 %",
            mem: "70 MB"
        },
        {
            name: "Martena",
            user: "<span class='badge badge-primary'>Mart</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "20 MB"
        },
        {
            name: "Unity",
            user: "<span class='badge badge-primary'>Unity</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "1 MB"
        },
        {
            name: "Howard",
            user: "<span class='badge badge-primary'>Howa</span>",
            IO: "65 KB/s",
            cpu: "0.20 %",
            mem: "1 MB"
        },
        {
            name: "Hope",
            user: "<span class='badge badge-secondary'>Hop</span>",
            IO: "5 KB/s",
            cpu: "0.04 %",
            mem: "145 MB"
        },
        {
            name: "Vivian",
            user: "<span class='badge badge-secondary'>Vivi</span>",
            IO: "80 KB/s",
            cpu: "0.50 %",
            mem: "25 MB"
        },
        {
            name: "Timothy",
            user: "<span class='badge badge-primary'>Timo</span>",
            IO: "50 KB/s",
            cpu: "0.30 %",
            mem: "13 MB"
        },
        {
            name: "Jackson",
            user: "<span class='badge badge-secondary'>Jack</span>",
            IO: "37 KB/s",
            cpu: "0.10 %",
            mem: "10 MB"
        },
        {
            name: "Olivia",
            user: "<span class='badge badge-primary'>Oliv</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "7"
        },
        {
            name: "Bruno",
            user: "<span class='badge badge-secondary'>Brun</span>",
            IO: "37 KB/s",
            cpu: "0.15 %",
            mem: "5 MB"
        },
        {
            name: "Sakura",
            user: "<span class='badge badge-primary'>Saku</span>",
            IO: "50 KB/s",
            cpu: "0.50 %",
            mem: "15 MB"
        },
        {
            name: "Thor",
            user: "<span class='badge badge-primary'>Tho</span>",
            IO: "25 KB/s",
            cpu: "0.10 %",
            mem: "80 MB"
        },
        {
            name: "Finn",
            user: "<span class='badge badge-primary'>Fin</span>",
            IO: "40 KB/s",
            cpu: "0.30 %",
            mem: "7 MB"
        },
        {
            name: "Serge",
            user: "<span class='badge badge-secondary'>Ser</span>",
            IO: "60 KB/s",
            cpu: "0.10 %",
            mem: "45 MB"
        },
        {
            name: "Zenaida",
            user: "<span class='badge badge-secondary'>Zen</span>",
            IO: "37 KB/s",
            cpu: "0.50 %",
            mem: "60 MB"
        },
        {
            name: "Zorita",
            user: "<span class='badge badge-primary'>Zor</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "57 MB"
        },
        {
            name: "Jennifer",
            user: "<span class='badge badge-primary'>Jenni</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "180 MB"
        },
        {
            name: "Cara",
            user: "<span class='badge badge-primary'>Car</span>",
            IO: "50 KB/s",
            cpu: "0.30 %",
            mem: "20 MB"
        },
        {
            name: "Hermione",
            user: "<span class='badge badge-secondary'>Hermi</span>",
            IO: "37 KB/s",
            cpu: "0.20 %",
            mem: "1 MB"
        },
        {
            name: "Lael",
            user: "<span class='badge badge-primary'>Lae</span>",
            IO: "0 KB/s",
            cpu: "0.10 %",
            mem: "50 MB"
        },
        {
            name: "Jonas",
            user: "<span class='badge badge-primary'>Jon</span>",
            IO: "12 KB/s",
            cpu: "0.05 %",
            mem: "2 MB"
        },
        {
            name: "Shad",
            user: "<span class='badge badge-primary'>Sha</span>",
            IO: "42 KB/s",
            cpu: "020 %",
            mem: "40 MB"
        },
        {
            name: "Michael",
            user: "<span class='badge badge-primary'>Mich</span>",
            IO: "0 KB/s",
            cpu: "0 %",
            mem: "25 MB"
        },
        {
            name: "Donna",
            user: "<span class='badge badge-primary'>Donn</span>",
            IO: "37 KB/s",
            cpu: "0.10 %",
            mem: "13 MB"
        }
    ];
    return serverDB;
}());



/***/ })

}]);
//# sourceMappingURL=components-dashboard-dashboard-module.js.map