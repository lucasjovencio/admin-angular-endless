(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-coming-soon-coming-soon-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/page-with-image/page-with-image.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/coming-soon/page-with-image/page-with-image.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid p-0 m-0\">\r\n  <div class=\"comingsoon comingsoon-bgimg\">\r\n    <div class=\"comingsoon-inner text-center\"><img src=\"assets/images/other-images/coming-soon-Logo.png\" alt=\"\">\r\n      <h5>WE ARE COMING SOON</h5>\r\n      <div class=\"countdown\" id=\"clockdiv\">\r\n        <ul>\r\n          <li><span class=\"time digits\" id=\"days\"></span><span class=\"title\">days</span></li>\r\n          <li><span class=\"time digits\" id=\"hours\"></span><span class=\"title\">Hours</span></li>\r\n          <li><span class=\"time digits\" id=\"minutes\"></span><span class=\"title\">Minutes</span></li>\r\n          <li><span class=\"time digits\" id=\"seconds\"></span><span class=\"title\">Seconds</span></li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/page-with-video/page-with-video.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/coming-soon/page-with-video/page-with-video.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid p-0\">\r\n  <div class=\"comingsoon auth-bg-video\">\r\n    <video class=\"bgvideo-comingsoon\" id=\"bgvid\" poster=\"assets/images/other-images/coming-soon-bg.jpg\" playsinline=\"\"\r\n      autoplay=\"\" muted=\"\" loop=\"\">\r\n      <source src=\"assets/video/auth-bg.mp4\" type=\"video/mp4\">\r\n    </video>\r\n    <div class=\"comingsoon-inner text-center\"><img src=\"assets/images/other-images/coming-soon-Logo.png\" alt=\"\">\r\n      <h5>WE ARE COMING SOON</h5>\r\n      <div class=\"countdown\" id=\"clockdiv\">\r\n        <ul>\r\n          <li><span class=\"time digits\" id=\"days\"></span><span class=\"title\">days</span></li>\r\n          <li><span class=\"time digits\" id=\"hours\"></span><span class=\"title\">Hours</span></li>\r\n          <li><span class=\"time digits\" id=\"minutes\"></span><span class=\"title\">Minutes</span></li>\r\n          <li><span class=\"time digits\" id=\"seconds\"></span><span class=\"title\">Seconds</span></li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/simple/simple.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/coming-soon/simple/simple.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid p-0\">\r\n  <div class=\"comingsoon\">\r\n    <div class=\"comingsoon-inner text-center\"><img src=\"assets/images/other-images/coming-soon-Logo.png\" alt=\"\">\r\n      <h5>WE ARE COMING SOON</h5>\r\n      <div class=\"countdown\" id=\"clockdiv\">\r\n        <ul>\r\n          <li><span class=\"time digits\" id=\"days\"></span><span class=\"title\">days</span></li>\r\n          <li><span class=\"time digits\" id=\"hours\"></span><span class=\"title\">Hours</span></li>\r\n          <li><span class=\"time digits\" id=\"minutes\"></span><span class=\"title\">Minutes</span></li>\r\n          <li><span class=\"time digits\" id=\"seconds\"></span><span class=\"title\">Seconds</span></li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/coming-soon/coming-soon-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/coming-soon/coming-soon-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ComingSoonRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComingSoonRoutingModule", function() { return ComingSoonRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _simple_simple_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./simple/simple.component */ "./src/app/pages/coming-soon/simple/simple.component.ts");
/* harmony import */ var _page_with_video_page_with_video_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page-with-video/page-with-video.component */ "./src/app/pages/coming-soon/page-with-video/page-with-video.component.ts");
/* harmony import */ var _page_with_image_page_with_image_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-with-image/page-with-image.component */ "./src/app/pages/coming-soon/page-with-image/page-with-image.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '',
        children: [
            {
                path: 'page',
                component: _simple_simple_component__WEBPACK_IMPORTED_MODULE_2__["SimpleComponent"]
            },
            {
                path: 'page/image',
                component: _page_with_image_page_with_image_component__WEBPACK_IMPORTED_MODULE_4__["PageWithImageComponent"]
            },
            {
                path: 'page/video',
                component: _page_with_video_page_with_video_component__WEBPACK_IMPORTED_MODULE_3__["PageWithVideoComponent"]
            }
        ]
    }
];
var ComingSoonRoutingModule = /** @class */ (function () {
    function ComingSoonRoutingModule() {
    }
    ComingSoonRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ComingSoonRoutingModule);
    return ComingSoonRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/coming-soon/coming-soon.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/coming-soon/coming-soon.module.ts ***!
  \*********************************************************/
/*! exports provided: ComingSoonModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComingSoonModule", function() { return ComingSoonModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _coming_soon_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./coming-soon-routing.module */ "./src/app/pages/coming-soon/coming-soon-routing.module.ts");
/* harmony import */ var _simple_simple_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./simple/simple.component */ "./src/app/pages/coming-soon/simple/simple.component.ts");
/* harmony import */ var _page_with_image_page_with_image_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-with-image/page-with-image.component */ "./src/app/pages/coming-soon/page-with-image/page-with-image.component.ts");
/* harmony import */ var _page_with_video_page_with_video_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page-with-video/page-with-video.component */ "./src/app/pages/coming-soon/page-with-video/page-with-video.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComingSoonModule = /** @class */ (function () {
    function ComingSoonModule() {
    }
    ComingSoonModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_simple_simple_component__WEBPACK_IMPORTED_MODULE_3__["SimpleComponent"], _page_with_image_page_with_image_component__WEBPACK_IMPORTED_MODULE_4__["PageWithImageComponent"], _page_with_video_page_with_video_component__WEBPACK_IMPORTED_MODULE_5__["PageWithVideoComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _coming_soon_routing_module__WEBPACK_IMPORTED_MODULE_2__["ComingSoonRoutingModule"]
            ]
        })
    ], ComingSoonModule);
    return ComingSoonModule;
}());



/***/ }),

/***/ "./src/app/pages/coming-soon/page-with-image/page-with-image.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/coming-soon/page-with-image/page-with-image.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbWluZy1zb29uL3BhZ2Utd2l0aC1pbWFnZS9wYWdlLXdpdGgtaW1hZ2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/coming-soon/page-with-image/page-with-image.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/coming-soon/page-with-image/page-with-image.component.ts ***!
  \********************************************************************************/
/*! exports provided: PageWithImageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageWithImageComponent", function() { return PageWithImageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageWithImageComponent = /** @class */ (function () {
    function PageWithImageComponent() {
        this.setTime();
    }
    PageWithImageComponent.prototype.setTime = function () {
        setInterval(function () {
            var countDown = new Date('Sep 30, 2019 00:00:00').getTime();
            var now = new Date().getTime();
            var distance = countDown - now;
            this.document.getElementById('days').innerHTML = Math.floor(distance / (1000 * 60 * 60 * 24));
            this.document.getElementById('hours').innerHTML = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            this.document.getElementById('minutes').innerHTML = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            this.document.getElementById('seconds').innerHTML = Math.floor((distance % (1000 * 60)) / 1000);
        }, this.seconds);
    };
    PageWithImageComponent.prototype.ngOnInit = function () { };
    PageWithImageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-with-image',
            template: __webpack_require__(/*! raw-loader!./page-with-image.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/page-with-image/page-with-image.component.html"),
            styles: [__webpack_require__(/*! ./page-with-image.component.scss */ "./src/app/pages/coming-soon/page-with-image/page-with-image.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PageWithImageComponent);
    return PageWithImageComponent;
}());



/***/ }),

/***/ "./src/app/pages/coming-soon/page-with-video/page-with-video.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/coming-soon/page-with-video/page-with-video.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbWluZy1zb29uL3BhZ2Utd2l0aC12aWRlby9wYWdlLXdpdGgtdmlkZW8uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/coming-soon/page-with-video/page-with-video.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/coming-soon/page-with-video/page-with-video.component.ts ***!
  \********************************************************************************/
/*! exports provided: PageWithVideoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageWithVideoComponent", function() { return PageWithVideoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageWithVideoComponent = /** @class */ (function () {
    function PageWithVideoComponent() {
        this.setTime();
    }
    PageWithVideoComponent.prototype.setTime = function () {
        setInterval(function () {
            var countDown = new Date('Sep 30, 2019 00:00:00').getTime();
            var now = new Date().getTime();
            var distance = countDown - now;
            this.document.getElementById('days').innerHTML = Math.floor(distance / (1000 * 60 * 60 * 24));
            this.document.getElementById('hours').innerHTML = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            this.document.getElementById('minutes').innerHTML = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            this.document.getElementById('seconds').innerHTML = Math.floor((distance % (1000 * 60)) / 1000);
        }, this.seconds);
    };
    PageWithVideoComponent.prototype.ngOnInit = function () { };
    PageWithVideoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-with-video',
            template: __webpack_require__(/*! raw-loader!./page-with-video.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/page-with-video/page-with-video.component.html"),
            styles: [__webpack_require__(/*! ./page-with-video.component.scss */ "./src/app/pages/coming-soon/page-with-video/page-with-video.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PageWithVideoComponent);
    return PageWithVideoComponent;
}());



/***/ }),

/***/ "./src/app/pages/coming-soon/simple/simple.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/pages/coming-soon/simple/simple.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbWluZy1zb29uL3NpbXBsZS9zaW1wbGUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/coming-soon/simple/simple.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/coming-soon/simple/simple.component.ts ***!
  \**************************************************************/
/*! exports provided: SimpleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimpleComponent", function() { return SimpleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { count } from 'rxjs/operators';
// export interface Time {
// }
var SimpleComponent = /** @class */ (function () {
    function SimpleComponent() {
        this.setTime();
    }
    SimpleComponent.prototype.setTime = function () {
        setInterval(function () {
            var countDown = new Date('Sep 30, 2019 00:00:00').getTime();
            var now = new Date().getTime();
            var distance = countDown - now;
            this.document.getElementById('days').innerHTML = Math.floor(distance / (1000 * 60 * 60 * 24));
            this.document.getElementById('hours').innerHTML = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            this.document.getElementById('minutes').innerHTML = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            this.document.getElementById('seconds').innerHTML = Math.floor((distance % (1000 * 60)) / 1000);
        }, this.seconds);
    };
    SimpleComponent.prototype.ngOnInit = function () { };
    SimpleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-simple',
            template: __webpack_require__(/*! raw-loader!./simple.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/coming-soon/simple/simple.component.html"),
            styles: [__webpack_require__(/*! ./simple.component.scss */ "./src/app/pages/coming-soon/simple/simple.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SimpleComponent);
    return SimpleComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-coming-soon-coming-soon-module.js.map