(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-knowledge-base-knowledge-base-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/knowledge-base/knowledge-base.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/knowledge-base/knowledge-base.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 xl-50 col-sm-6\">\r\n      <div class=\"card bg-primary\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media faq-widgets\">\r\n            <div class=\"media-body\">\r\n              <h5>Knowledge Base</h5>\r\n              <p>Ouch found swore much dear conductively hid submissively hatchet vexed far inanimately alongside\r\n                candidly</p>\r\n            </div>\r\n            <app-feather-icons [icon]=\"'book-open'\"></app-feather-icons>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-50 col-sm-6\">\r\n      <div class=\"card bg-primary\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media faq-widgets\">\r\n            <div class=\"media-body\">\r\n              <h5>Support center</h5>\r\n              <p>Dear spryly growled much far jeepers vigilantly less and far hideous and some mannishly less jeepers\r\n                less and and crud</p>\r\n            </div>\r\n            <app-feather-icons [icon]=\"'aperture'\"></app-feather-icons>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-4 xl-100\">\r\n      <div class=\"card bg-primary\">\r\n        <div class=\"card-body\">\r\n          <div class=\"media faq-widgets\">\r\n            <div class=\"media-body\">\r\n              <h5>Articles and news</h5>\r\n              <p>Diabolically somberly astride crass one endearingly blatant depending peculiar antelope piquantly\r\n                popularly adept</p>\r\n            </div>\r\n            <app-feather-icons [icon]=\"'file-text'\"></app-feather-icons>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"header-faq\">\r\n            <h5 class=\"mb-0\">Browse articles by category</h5>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6\">\r\n          <form class=\"form-inline search-form pull-right search-knowledge\">\r\n            <div class=\"form-group mr-0\">\r\n              <input class=\"form-control-plaintext\" name=\"search\" [(ngModel)]=\"term\" type=\"text\" placeholder=\"Search..\">\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n          <div class=\"card\">\r\n            <div class=\"card-header\">\r\n              <h5>Browse Articles</h5>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <div class=\"row browse\">\r\n                <div class=\"col-xl-4 xl-50\" *ngFor=\"let kb of Kb | filter:term\">\r\n                  <div class=\"browse-articles browse-bottom\">\r\n                    <h6><span>\r\n                        <app-feather-icons [icon]=\"'archive'\"></app-feather-icons>\r\n                      </span>{{kb.title}}</h6>\r\n                    <ul [innerHTML]=\"kb.desc\">\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-sm-12\">\r\n                  <div *ngIf=\"!(Kb | filter:term).length\">\r\n                    <div class=\"search-not-found knowledge-search-height text-center\">\r\n                      <div class=\"\">\r\n                        <img src=\"assets/images/search-not-found.png\" class=\"second-search\">\r\n                        <h6>Sorry, We didn't find any articles matching this search</h6>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"header-faq\">\r\n        <h5 class=\"mb-0\">Featured articles and tutorials</h5>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-3 xl-50\">\r\n          <div class=\"card features-faq product-box\">\r\n            <div class=\"faq-image product-img\"><img class=\"img-fluid\" src=\"assets/images/faq/1.jpg\" alt=\"\">\r\n              <div class=\"product-hover\">\r\n                <ul>\r\n                  <li><i class=\"icon-link\"></i></li>\r\n                  <li><i class=\"icon-import\"></i></li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <h6>For ostrich much</h6>\r\n              <p>Some various less crept gecko the jeepers dear forewent far the ouch far a incompetent saucy wherever\r\n                towards</p>\r\n            </div>\r\n            <div class=\"card-footer\"><span>Dec 15, 2018</span><span class=\"pull-right\"><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i></span></div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-3 xl-50\">\r\n          <div class=\"card features-faq product-box\">\r\n            <div class=\"faq-image product-img\"><img class=\"img-fluid\" src=\"assets/images/faq/2.jpg\" alt=\"\">\r\n              <div class=\"product-hover\">\r\n                <ul>\r\n                  <li><i class=\"icon-link\"></i></li>\r\n                  <li><i class=\"icon-import\"></i></li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <h6>Considering far</h6>\r\n              <p>Kookaburra so hey a less tritely far congratulated this winked some under had unblushing beyond\r\n                sympathetic</p>\r\n            </div>\r\n            <div class=\"card-footer\"><span>Dec 15, 2018</span><span class=\"pull-right\"><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star-o font-primary\"></i></span></div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-3 xl-50\">\r\n          <div class=\"card features-faq product-box\">\r\n            <div class=\"faq-image product-img\"><img class=\"img-fluid\" src=\"assets/images/faq/3.jpg\" alt=\"\">\r\n              <div class=\"product-hover\">\r\n                <ul>\r\n                  <li><i class=\"icon-link\"></i></li>\r\n                  <li><i class=\"icon-import\"></i></li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <h6>Despite perversely</h6>\r\n              <p>Coming merits and was talent enough far. Sir joy northward sportsmen education. Put still any about\r\n                manor heard</p>\r\n            </div>\r\n            <div class=\"card-footer\"><span>Dec 15, 2018</span><span class=\"pull-right\"><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i></span></div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-3 xl-50\">\r\n          <div class=\"card features-faq product-box\">\r\n            <div class=\"faq-image product-img\"><img class=\"img-fluid\" src=\"assets/images/faq/4.jpg\" alt=\"\">\r\n              <div class=\"product-hover\">\r\n                <ul>\r\n                  <li><i class=\"icon-link\"></i></li>\r\n                  <li><i class=\"icon-import\"></i></li>\r\n                </ul>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <h6>Helpfully stolidly</h6>\r\n              <p>Hippopotamus aside while a shrewdly this after kookaburra wow in haphazardly much salmon buoyantly\r\n                sullen gosh</p>\r\n            </div>\r\n            <div class=\"card-footer\"><span>Dec 15, 2018</span><span class=\"pull-right\"><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star font-primary\"></i><i class=\"fa fa-star font-primary\"></i><i\r\n                  class=\"fa fa-star-half-o font-primary\"></i></span></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"header-faq\">\r\n        <h5 class=\"mb-0\">Latest articles and videos</h5>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-4 col-md-6\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'codepen'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Contemptibly bleakly</h6>\r\n                      <p>Speechless far goodness bent as boa crud because vague far koala the that lantern alas sold</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'codepen'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Stretched flamboyant</h6>\r\n                      <p>Some when foolhardy dangerous so less less aimlessly along hazardously oversaw much</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'codepen'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Bound befell well</h6>\r\n                      <p>Since left before understandably much groomed along burped through dear</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-4 col-md-6\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'file-text'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Blamelessly wow hence</h6>\r\n                      <p>A without walking some objective hiccupped some this overlay immorally crud and gosh</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'file-text'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Walking away fallaciously</h6>\r\n                      <p>Ouch licentiously lackadaisical crud together began gregarious below near darn goodness</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'file-text'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Porcupine strict nodded</h6>\r\n                      <p>Left extravagant leered crab repaid outgrew said knelt hello astride within oh disbanded</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xl-12 col-md-6\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'youtube'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Manatee broadcast</h6>\r\n                      <p>Some where indecently manta floated raptly youthful unlike swept dragonfly pulled moth</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-12 col-md-6\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'youtube'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Ducked ravenously dear</h6>\r\n                      <p>Reran sincere said monkey one slapped jeepers rubbed fleetly incongruously due yet llama</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-xl-12\">\r\n              <div class=\"card\">\r\n                <div class=\"card-body\">\r\n                  <div class=\"media\">\r\n                    <app-feather-icons class=\"m-r-30\" [icon]=\"'youtube'\"></app-feather-icons>\r\n                    <div class=\"media-body\">\r\n                      <h6 class=\"f-w-600\">Porcupine strict nodded</h6>\r\n                      <p>Left extravagant leered crab repaid outgrew said knelt hello astride within oh disbanded</p>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->\r\n"

/***/ }),

/***/ "./src/app/components/knowledge-base/knowledge-base-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/knowledge-base/knowledge-base-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: KnowledgeBaseRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KnowledgeBaseRoutingModule", function() { return KnowledgeBaseRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _knowledge_base_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./knowledge-base.component */ "./src/app/components/knowledge-base/knowledge-base.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _knowledge_base_component__WEBPACK_IMPORTED_MODULE_2__["KnowledgeBaseComponent"],
                data: {
                    title: "Knowledge Base",
                    breadcrumb: ""
                }
            },
        ]
    }
];
var KnowledgeBaseRoutingModule = /** @class */ (function () {
    function KnowledgeBaseRoutingModule() {
    }
    KnowledgeBaseRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], KnowledgeBaseRoutingModule);
    return KnowledgeBaseRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/knowledge-base/knowledge-base.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/knowledge-base/knowledge-base.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMva25vd2xlZGdlLWJhc2Uva25vd2xlZGdlLWJhc2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/knowledge-base/knowledge-base.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/knowledge-base/knowledge-base.component.ts ***!
  \***********************************************************************/
/*! exports provided: KnowledgeBaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KnowledgeBaseComponent", function() { return KnowledgeBaseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_knowledge_base_knowledge_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/data/knowledge-base/knowledge-base */ "./src/app/shared/data/knowledge-base/knowledge-base.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var KnowledgeBaseComponent = /** @class */ (function () {
    function KnowledgeBaseComponent(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.Kb = _shared_data_knowledge_base_knowledge_base__WEBPACK_IMPORTED_MODULE_1__["KB_DB"].Kb_Category;
    }
    KnowledgeBaseComponent.prototype.ngOnInit = function () { };
    KnowledgeBaseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-knowledge-base',
            template: __webpack_require__(/*! raw-loader!./knowledge-base.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/knowledge-base/knowledge-base.component.html"),
            styles: [__webpack_require__(/*! ./knowledge-base.component.scss */ "./src/app/components/knowledge-base/knowledge-base.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], KnowledgeBaseComponent);
    return KnowledgeBaseComponent;
}());



/***/ }),

/***/ "./src/app/components/knowledge-base/knowledge-base.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/knowledge-base/knowledge-base.module.ts ***!
  \********************************************************************/
/*! exports provided: KnowledgeBaseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KnowledgeBaseModule", function() { return KnowledgeBaseModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _knowledge_base_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./knowledge-base-routing.module */ "./src/app/components/knowledge-base/knowledge-base-routing.module.ts");
/* harmony import */ var _knowledge_base_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./knowledge-base.component */ "./src/app/components/knowledge-base/knowledge-base.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var KnowledgeBaseModule = /** @class */ (function () {
    function KnowledgeBaseModule() {
    }
    KnowledgeBaseModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_knowledge_base_component__WEBPACK_IMPORTED_MODULE_6__["KnowledgeBaseComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _knowledge_base_routing_module__WEBPACK_IMPORTED_MODULE_5__["KnowledgeBaseRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_3__["Ng2SearchPipeModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ]
        })
    ], KnowledgeBaseModule);
    return KnowledgeBaseModule;
}());



/***/ }),

/***/ "./src/app/shared/data/knowledge-base/knowledge-base.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/data/knowledge-base/knowledge-base.ts ***!
  \**************************************************************/
/*! exports provided: KB_DB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KB_DB", function() { return KB_DB; });
var KB_DB = /** @class */ (function () {
    function KB_DB(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    KB_DB.Kb_Category = [
        {
            title: 'Quick Questions are answered',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Lorem Ipsum is simply dummy text of the printing</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Lorem Ipsum has been the industry's standard dummy </span><span class=\"badge badge-primary pull-right\">New</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>When an unknown printer took a galley </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>But also the leap into electronic typesetting, </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (40)</span></a></li>",
        },
        {
            title: 'Integrating WordPress with Your Website',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>It was popularised in the 1960s with the release</span><span class=\"badge badge-primary pull-right\">Review</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Etraset sheets containing Lorem Ipsum passages</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Desktop publishing software like Aldus PageMaker </span><span class=\"badge badge-primary pull-right\">Articles</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Making this the first true generator on the Internet.</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (90)</span></a></li>",
        },
        {
            title: 'WordPress Site Maintenance',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>The point of using Lorem Ipsum is that it has a more-or-less </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Normal distribution of letters, as opposed to using </span><span class=\"badge badge-primary pull-right\">Closed</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Lorem Ipsum, you need to be sure there isn't anything  </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Repetition, injected humour, or non-characteristic words etc</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (50)</span></a></li>"
        },
        {
            title: ' Meta Tags in WordPress',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Nemo enim ipsam voluptatem quia voluptas sit  </span><span class=\"badge badge-primary pull-right\">Popular</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Ipsum quia dolor sit amet, consectetur</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Sed quia non numquam eius modi tempora incidunt</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Lorem eum fugiat quo voluptas nulla pariatu</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (90)</span></a></li>"
        },
        {
            title: 'WordPress in Your Language',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Desktop publishing software like Aldus PageMaker</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Etraset sheets containing Lorem Ipsum passages</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>It was popularised in the 1960s with the release</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Making this the first true generator on the Internet</span><span class=\"badge badge-primary pull-right\">Closed</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (50)</span></a></li>"
        },
        {
            title: 'Know Your Sources',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>The point of using Lorem Ipsum </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>It has a more-or-less normal distribution of letters</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Et harum quidem rerum facilis est et expedita</span><span class=\"badge badge-primary pull-right\">Article</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Nam libero tempore, cum soluta nobis est eligendi </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (26)</span></a></li>"
        },
        {
            title: 'Validating a Website',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>When our power of choice is untrammelled </span><span class=\"badge badge-primary pull-right\">Review</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>It will frequently occur that pleasures  </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>who fail in their duty through weakness </span><span class=\"badge badge-primary pull-right\">Closed</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span> At vero eos et accusamus et iusto </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (10)</span></a></li>"
        },
        {
            title: 'Quick Questions are answered',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Quis autem vel eum iure reprehenderit  </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Ducimus  blanditiis praesentium voluptatum</span><span class=\"badge badge-primary pull-right\">Popular</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Omnis voluptas assumenda est</span><span class=\"badge badge-primary pull-right\">Review</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Produces no resultant pleasure</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (21)</span></a></li>"
        },
        {
            title: 'Integrating WordPress with Your Website',
            desc: "\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Lorem Ipsum passage, and going through</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>The first line of Lorem Ipsum,  Lorem ipsum </span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>Thus comes from a line in section</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"file-text\"></i></span><span>First true generator on the Internet</span><span class=\"badge badge-primary pull-right\">On hold</span></a></li>\n                <li><a  href=\"javascript:void(0)\"><span><i data-feather=\"arrow-right\"></i></span><span>See More (34)</span></a></li>"
        }
    ];
    return KB_DB;
}());



/***/ })

}]);
//# sourceMappingURL=components-knowledge-base-knowledge-base-module.js.map