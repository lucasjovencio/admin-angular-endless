(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-search-result-search-result-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/components/search-result/image-gallery/image-gallery.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/search-result/image-gallery/image-gallery.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"my-gallery row gallery-with-description\" id=\"aniimated-thumbnials\">\r\n  <div class=\"my-app-custom-plain-container-with-desc row\">\r\n    <ng-container *ngFor=\"let img of imagesRect\">\r\n      <figure class=\"my-app-custom-image-with-desc col-xl-3 col-sm-6\">\r\n        <img [src]=\"img.modal.img\" (click)=\"openImageModalRowDescription(img)\" />\r\n        <div class=\"caption\">\r\n          <h4>Portfolio Title</h4>\r\n          <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has\r\n            been the industry's standard dummy.</p>\r\n        </div>\r\n      </figure>\r\n    </ng-container>\r\n  </div>\r\n  <ks-modal-gallery [id]=\"1\" [modalImages]=\"imagesRect\" [plainGalleryConfig]=\"customPlainGalleryRowDescConfig\"\r\n    [currentImageConfig]=\"{downloadable: true}\" [buttonsConfig]=\"buttonsConfigCustom\">\r\n  </ks-modal-gallery>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/search-result/search-result.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/search-result/search-result.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Container-fluid starts-->\r\n<div class=\"container-fluid search-page\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <form class=\"search-form\">\r\n            <div class=\"form-group m-0\">\r\n              <input class=\"form-control-plaintext\" type=\"search\" placeholder=\"Search..\">\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"card-body\" id=\"search-custom\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n              <div class=\"nav nav-tabs nav-material\" id=\"top-tab\" role=\"tablist\">\r\n                <ngb-tabset id=\"tabset\">\r\n                  <ngb-tab>\r\n                    <ng-template ngbTabTitle class=\"nav-item\"><i class=\"icon-target\"></i>All </ng-template>\r\n                    <ng-template ngbTabContent class=\"tab-content\">\r\n                      <div class=\"search-links tab-pane fade show\" id=\"all-links\" role=\"tabpanel\"\r\n                        aria-labelledby=\"all-link\">\r\n                        <div class=\"row\">\r\n                          <div class=\"col-xl-6\">\r\n                            <p class=\"pb-4\">About 6,000 results (0.60 seconds)</p>\r\n                            <div class=\"info-block\">\r\n                              <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry</h6><a\r\n                                href=\"\">endlesseducation.info/</a>\r\n                              <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                Coaching in Surat.</p>\r\n                              <div class=\"star-ratings\">\r\n                                <ul class=\"search-info\">\r\n                                  <li>2.5 stars</li>\r\n                                  <li>590 votes</li>\r\n                                  <li>Music</li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <h6>Proin eleifend metus vel erat faucibus, ut bibendum nulla iaculis.</h6><a\r\n                                href=\"\">endlesseducation.info/</a>\r\n                              <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                Coaching in Surat.</p>\r\n                              <div class=\"star-ratings\">\r\n                                <ul class=\"search-info\">\r\n                                  <li>2.5 stars</li>\r\n                                  <li>590 votes</li>\r\n                                  <li>Music</li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <h6>Fusce rutrum elit aliquet nisi malesuada cursus.</h6><a\r\n                                href=\"\">endlesseducation.info/</a>\r\n                              <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                Coaching in Surat.</p>\r\n                              <div class=\"star-ratings\">\r\n                                <ul class=\"search-info\">\r\n                                  <li><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rating\"></i><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i></li>\r\n                                  <li>2.5 stars</li>\r\n                                  <li>590 votes</li>\r\n                                  <li>Music</li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <h6>Morbi feugiat mauris vel semper fringilla.</h6><a href=\"\">endlesseducation.info/</a>\r\n                              <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                Coaching in Surat.</p>\r\n                              <div class=\"star-ratings\">\r\n                                <ul class=\"search-info\">\r\n                                  <li><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rating\"></i><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i></li>\r\n                                  <li>2.5 stars</li>\r\n                                  <li>590 votes</li>\r\n                                  <li>Music</li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <h6>Morbi feugiat mauris vel semper fringilla.</h6><a href=\"\">endlesseducation.info/</a>\r\n                              <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                Coaching in Surat.</p>\r\n                              <div class=\"star-ratings\">\r\n                                <ul class=\"search-info\">\r\n                                  <li><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rating\"></i><i class=\"icofont icofont-ui-rating\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i><i\r\n                                      class=\"icofont icofont-ui-rate-blank\"></i></li>\r\n                                  <li>2.5 stars</li>\r\n                                  <li>590 votes</li>\r\n                                  <li>Music</li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <nav aria-label=\"...\">\r\n                                <ul class=\"pagination pagination-primary\">\r\n                                  <li class=\"page-item disabled\"><a class=\"page-link\"  href=\"javascript:void(0)\"\r\n                                      tabindex=\"-1\">Previous</a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">1</a></li>\r\n                                  <li class=\"page-item active\"><a class=\"page-link\"  href=\"javascript:void(0)\">2 <span\r\n                                        class=\"sr-only\">(current)</span></a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">3</a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">Next</a></li>\r\n                                </ul>\r\n                              </nav>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col-xl-6\">\r\n                            <div class=\"card lg-mt mb-0\">\r\n                              <div class=\"blog-box blog-shadow\"><img class=\"img-fluid\" src=\"assets/images/blog/blog.jpg\"\r\n                                  alt=\"\">\r\n                                <div class=\"blog-details\">\r\n                                  <p class=\"digits\">25 July 2018</p>\r\n                                  <h4>Accusamus et iusto odio dignissimos ducimus qui blanditiis.</h4>\r\n                                  <ul class=\"blog-social digits\">\r\n                                    <li><i class=\"icofont icofont-user\"></i>Mark Jecno</li>\r\n                                    <li><i class=\"icofont icofont-thumbs-up\"></i>02 Hits</li>\r\n                                  </ul>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </ng-template>\r\n                  </ngb-tab>\r\n                  <ngb-tab>\r\n                    <ng-template ngbTabTitle class=\"nav-item\"><i class=\"icon-image\"></i>Images </ng-template>\r\n                    <ng-template ngbTabContent class=\"tab-content\">\r\n                      <div class=\"tab-pane fade show\" id=\"image-links\" role=\"tabpanel\" aria-labelledby=\"image-link\">\r\n                        <div class=\"info-block m-t-30\">\r\n                          <p class=\"pb-4 col-sm-12 digits\">About 12,120 results (0.50 seconds)</p>\r\n                          <app-image-gallery></app-image-gallery>\r\n                        </div>\r\n                        <div class=\"info-block m-t-30\">\r\n                          <nav aria-label=\"...\">\r\n                            <ul class=\"pagination pagination-primary\">\r\n                              <li class=\"page-item disabled\"><a class=\"page-link\"  href=\"javascript:void(0)\" tabindex=\"-1\">Previous</a>\r\n                              </li>\r\n                              <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">1</a></li>\r\n                              <li class=\"page-item active\"><a class=\"page-link\"  href=\"javascript:void(0)\">2 <span\r\n                                    class=\"sr-only\">(current)</span></a></li>\r\n                              <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">3</a></li>\r\n                              <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">Next</a></li>\r\n                            </ul>\r\n                          </nav>\r\n                        </div>\r\n                      </div>\r\n                    </ng-template>\r\n                  </ngb-tab>\r\n                  <ngb-tab>\r\n                    <ng-template ngbTabTitle class=\"nav-item\"><i class=\"icon-video-clapper\"></i>Videos </ng-template>\r\n                    <ng-template ngbTabContent class=\"tab-content\">\r\n                      <div class=\"tab-pane fade show\" id=\"video-links\" role=\"tabpanel\" aria-labelledby=\"video-link\">\r\n                        <div class=\"row\">\r\n                          <div class=\"col-xl-6\">\r\n                            <p class=\"pb-4\">About 6,000 results (0.60 seconds)</p>\r\n                            <div class=\"media info-block\">\r\n                              <iframe class=\"mr-3 mb-3\" width=\"200\" height=\"100\"\r\n                                src=\"https://www.youtube.com/embed/CJnfAXlBRTE\"></iframe>\r\n                              <div class=\"media-body\">\r\n                                <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry</h6><a\r\n                                  href=\"\">endlesseducation.info/</a>\r\n                                <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                  Coaching in Surat.</p>\r\n                                <div class=\"star-ratings\">\r\n                                  <ul class=\"search-info\">\r\n                                    <li>2.5 stars</li>\r\n                                    <li>590 votes</li>\r\n                                    <li>Music</li>\r\n                                  </ul>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"media info-block\">\r\n                              <iframe class=\"mr-3 mb-3\" width=\"200\" height=\"100\"\r\n                                src=\"https://www.youtube.com/embed/-L4gEk7cOfk\"></iframe>\r\n                              <div class=\"media-body\">\r\n                                <h6>Morbi eget quam et purus commodo dapibus.</h6><a href=\"\">endlesseducation.info/</a>\r\n                                <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                  Coaching in Surat.</p>\r\n                                <div class=\"star-ratings\">\r\n                                  <ul class=\"search-info\">\r\n                                    <li>2.5 stars</li>\r\n                                    <li>590 votes</li>\r\n                                    <li>Music</li>\r\n                                  </ul>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"media info-block\">\r\n                              <iframe class=\"mr-3 mb-3\" width=\"200\" height=\"100\"\r\n                                src=\"https://www.youtube.com/embed/FZzWGr3ruVw\"></iframe>\r\n                              <div class=\"media-body\">\r\n                                <h6>Etiam eget ligula at ante eleifend rutrum.</h6><a href=\"\">endlesseducation.info/</a>\r\n                                <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                  Coaching in Surat.</p>\r\n                                <div class=\"star-ratings\">\r\n                                  <ul class=\"search-info\">\r\n                                    <li>2.5 stars</li>\r\n                                    <li>590 votes</li>\r\n                                    <li>Music</li>\r\n                                  </ul>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"media info-block\">\r\n                              <iframe class=\"mr-3 mb-3\" width=\"200\" height=\"100\"\r\n                                src=\"https://www.youtube.com/embed/wpmHZspl4EM\"></iframe>\r\n                              <div class=\"media-body\">\r\n                                <h6>Lorem Ipsum is simply dummy text of the printing.</h6><a\r\n                                  href=\"\">endlesseducation.info/</a>\r\n                                <p>endless introduces a IELTS Coaching, TOEFL Coaching, GRE Coaching, GMAT Coaching, SAT\r\n                                  Coaching in Surat.</p>\r\n                                <div class=\"star-ratings\">\r\n                                  <ul class=\"search-info\">\r\n                                    <li>2.5 stars</li>\r\n                                    <li>590 votes</li>\r\n                                    <li>Music</li>\r\n                                  </ul>\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"info-block\">\r\n                              <nav aria-label=\"...\">\r\n                                <ul class=\"pagination pagination-primary\">\r\n                                  <li class=\"page-item disabled\"><a class=\"page-link\"  href=\"javascript:void(0)\"\r\n                                      tabindex=\"-1\">Previous</a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">1</a></li>\r\n                                  <li class=\"page-item active\"><a class=\"page-link\"  href=\"javascript:void(0)\">2 <span\r\n                                        class=\"sr-only\">(current)</span></a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">3</a></li>\r\n                                  <li class=\"page-item\"><a class=\"page-link\"  href=\"javascript:void(0)\">Next</a></li>\r\n                                </ul>\r\n                              </nav>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col-xl-6\">\r\n                            <div>\r\n                              <div class=\"embed-responsive embed-responsive-21by9 lg-mt\">\r\n                                <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/wpmHZspl4EM\"\r\n                                  allowfullscreen=\"\"></iframe>\r\n                              </div>\r\n                              <div class=\"embed-responsive embed-responsive-21by9\">\r\n                                <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/I0-vBdh4sZ8\"\r\n                                  allowfullscreen=\"\"></iframe>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </ng-template>\r\n                  </ngb-tab>\r\n                </ngb-tabset>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Container-fluid Ends-->"

/***/ }),

/***/ "./src/app/components/search-result/image-gallery/image-gallery.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/components/search-result/image-gallery/image-gallery.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VhcmNoLXJlc3VsdC9pbWFnZS1nYWxsZXJ5L2ltYWdlLWdhbGxlcnkuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/search-result/image-gallery/image-gallery.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/search-result/image-gallery/image-gallery.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ImageGalleryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageGalleryComponent", function() { return ImageGalleryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ImageGalleryComponent = /** @class */ (function () {
    function ImageGalleryComponent(galleryService) {
        this.galleryService = galleryService;
        this.imagesRect = [
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](0, {
                img: 'assets/images/big-lightgallry/013.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](1, {
                img: 'assets/images/big-lightgallry/014.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](2, {
                img: 'assets/images/big-lightgallry/015.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](3, {
                img: 'assets/images/big-lightgallry/016.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](4, {
                img: 'assets/images/big-lightgallry/012.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](5, {
                img: 'assets/images/big-lightgallry/01.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](6, {
                img: 'assets/images/big-lightgallry/02.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](7, {
                img: 'assets/images/big-lightgallry/03.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](8, {
                img: 'assets/images/big-lightgallry/04.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](9, {
                img: 'assets/images/big-lightgallry/05.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](10, {
                img: 'assets/images/big-lightgallry/06.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
            new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](11, {
                img: 'assets/images/big-lightgallry/07.jpg',
                extUrl: 'http://www.google.com',
                title: 'Portfolio Title',
                description: '<h4>Portfolio Title</h4><p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.</p>'
            }),
        ];
        this.buttonsConfigDefault = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].DEFAULT
        };
        this.buttonsConfigSimple = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].SIMPLE
        };
        this.buttonsConfigAdvanced = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].ADVANCED
        };
        this.buttonsConfigFull = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].FULL
        };
        this.buttonsConfigCustom = {
            visible: true,
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonsStrategy"].CUSTOM,
            buttons: [
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_FULL_SCREEN"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_DELETE"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_EXTURL"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_DOWNLOAD"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["KS_DEFAULT_BTN_CLOSE"]
            ]
        };
        this.customPlainGalleryRowDescConfig = {
            strategy: _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["PlainGalleryStrategy"].CUSTOM,
            layout: new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["AdvancedLayout"](-1, true)
        };
    }
    ImageGalleryComponent.prototype.openImageModalRowDescription = function (image) {
        var index = this.getCurrentIndexCustomLayout(image, this.imagesRect);
        this.customPlainGalleryRowDescConfig = Object.assign({}, this.customPlainGalleryRowDescConfig, { layout: new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["AdvancedLayout"](index, true) });
    };
    ImageGalleryComponent.prototype.getCurrentIndexCustomLayout = function (image, images) {
        return image ? images.indexOf(image) : -1;
    };
    ;
    ImageGalleryComponent.prototype.onButtonBeforeHook = function (event) {
        if (!event || !event.button) {
            return;
        }
        if (event.button.type === _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonType"].DELETE) {
            this.imagesRect = this.imagesRect.filter(function (val) { return event.image && val.id !== event.image.id; });
        }
    };
    ImageGalleryComponent.prototype.onButtonAfterHook = function (event) {
        if (!event || !event.button) {
            return;
        }
    };
    ImageGalleryComponent.prototype.onCustomButtonBeforeHook = function (event, galleryId) {
        var _this = this;
        if (!event || !event.button) {
            return;
        }
        if (event.button.type === _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["ButtonType"].CUSTOM) {
            this.addRandomImage();
            setTimeout(function () {
                _this.galleryService.openGallery(galleryId, _this.imagesRect.length - 1);
            }, 0);
        }
    };
    ImageGalleryComponent.prototype.onCustomButtonAfterHook = function (event, galleryId) {
        if (!event || !event.button) {
            return;
        }
    };
    ImageGalleryComponent.prototype.addRandomImage = function () {
        var imageToCopy = this.imagesRect[Math.floor(Math.random() * this.imagesRect.length)];
        var newImage = new _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["Image"](this.imagesRect.length - 1 + 1, imageToCopy.modal, imageToCopy.plain);
        this.imagesRect = this.imagesRect.concat([newImage]);
    };
    ImageGalleryComponent.prototype.ngOnInit = function () { };
    ImageGalleryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-image-gallery',
            template: __webpack_require__(/*! raw-loader!./image-gallery.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/search-result/image-gallery/image-gallery.component.html"),
            styles: [__webpack_require__(/*! ./image-gallery.component.scss */ "./src/app/components/search-result/image-gallery/image-gallery.component.scss")]
        }),
        __metadata("design:paramtypes", [_ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_1__["GalleryService"]])
    ], ImageGalleryComponent);
    return ImageGalleryComponent;
}());



/***/ }),

/***/ "./src/app/components/search-result/search-result-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/components/search-result/search-result-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: SearchResultRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchResultRoutingModule", function() { return SearchResultRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _search_result_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-result.component */ "./src/app/components/search-result/search-result.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: _search_result_component__WEBPACK_IMPORTED_MODULE_2__["SearchResultComponent"],
                data: {
                    title: "Search Result",
                    breadcrumb: ""
                }
            }
        ]
    }
];
var SearchResultRoutingModule = /** @class */ (function () {
    function SearchResultRoutingModule() {
    }
    SearchResultRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SearchResultRoutingModule);
    return SearchResultRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/search-result/search-result.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/search-result/search-result.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VhcmNoLXJlc3VsdC9zZWFyY2gtcmVzdWx0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/search-result/search-result.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/search-result/search-result.component.ts ***!
  \*********************************************************************/
/*! exports provided: SearchResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchResultComponent", function() { return SearchResultComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchResultComponent = /** @class */ (function () {
    function SearchResultComponent() {
    }
    SearchResultComponent.prototype.ngOnInit = function () { };
    SearchResultComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search-result',
            template: __webpack_require__(/*! raw-loader!./search-result.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/search-result/search-result.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./search-result.component.scss */ "./src/app/components/search-result/search-result.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchResultComponent);
    return SearchResultComponent;
}());



/***/ }),

/***/ "./src/app/components/search-result/search-result.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/search-result/search-result.module.ts ***!
  \******************************************************************/
/*! exports provided: SearchResultModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchResultModule", function() { return SearchResultModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ks89/angular-modal-gallery */ "./node_modules/@ks89/angular-modal-gallery/fesm5/ks89-angular-modal-gallery.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! mousetrap */ "./node_modules/mousetrap/mousetrap.js");
/* harmony import */ var mousetrap__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(mousetrap__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _search_result_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search-result-routing.module */ "./src/app/components/search-result/search-result-routing.module.ts");
/* harmony import */ var _search_result_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./search-result.component */ "./src/app/components/search-result/search-result.component.ts");
/* harmony import */ var _image_gallery_image_gallery_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./image-gallery/image-gallery.component */ "./src/app/components/search-result/image-gallery/image-gallery.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var SearchResultModule = /** @class */ (function () {
    function SearchResultModule() {
    }
    SearchResultModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_search_result_component__WEBPACK_IMPORTED_MODULE_7__["SearchResultComponent"], _image_gallery_image_gallery_component__WEBPACK_IMPORTED_MODULE_8__["ImageGalleryComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _search_result_routing_module__WEBPACK_IMPORTED_MODULE_6__["SearchResultRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
                _ks89_angular_modal_gallery__WEBPACK_IMPORTED_MODULE_3__["GalleryModule"].forRoot()
            ]
        })
    ], SearchResultModule);
    return SearchResultModule;
}());



/***/ })

}]);
//# sourceMappingURL=components-search-result-search-result-module.js.map